let coordinates_data = [];

let cards = [
	{
		'title' : 'Surveys',
		'icon'   : 'fas fa-user-tie',
		'color'  : 'bg-warning',
		'class_num' : 'total-surveys',
	},
	{
		'title' : 'Active Users',
		'icon'   : 'fas fa-users',
		'color'  : 'bg-primary',
		'class_num' : 'total-active',
	},
	{
		'title' : 'Issues',
		'icon'   : 'fas fa-newspaper',
		'color'  : 'bg-danger',
		'class_num' : 'total-issues',
	}
];

$(function () {
	get_coordinates();
	get_dashboard(true);
	get_users();
	get_numbers();
	set_cards();
	// timeout();
	$('.nav-menu li:first').addClass('active');
});

function timeout() {
	setTimeout(function () {
		get_dashboard(false);
		timeout();
		$('[data-toggle="tooltip"]').tooltip('dispose');
	}, 5000);
}

function get_dashboard(timing) {
	$.ajax({
		url: url + 'get_dashboard',
		dataType: 'json',
		type: 'GET',
		beforeSend() {
			if (timing) {
				let html = '<h5 class="text-center text-white mg-top-135">' +
					'<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>' +
					'<h4 class="text-dark text-center">Loading</h4>' +
					'</h5>';
				$('.act-list').html(html);

				$('.loader').removeClass('d-none');
				$('#tracks').addClass('h-145');
			}
		},
		success: function (data) {
			getActivities(data.activities);
			set_values(data);

			if (timing) {
				$('#tracks').removeClass('h-145');
				$('.loader').addClass('d-none');
				get_projects(data.surveys);
			}
			$('.c_tracking').removeClass('d-none');
			// getSurveyGraphTracks(data.surveys);

		},
		error: function (xhr, status, error) {
			errorSwal();
		}
	});
}

function get_coordinates(type = 'all', id = '') {
	$.ajax({
		url: url + 'get_coordinates',
		dataType: 'json',
		data: {type: type, id: id},
		type: 'GET',
		beforeSend() {
			let html = '<h5 class="text-center text-white mg-top-60">' +
				'<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>' +
				'<h4 class="text-dark text-center">Loading</h4>' +
				'</h5>';
			$('#main_map').html(html);
		},
		success: function (data) {
			$('#main_map').html('');
			setLocations(data.coordinates, 'main_map');
			coordinates_data = [];
			if (data.coordinates.length > 0) {
				for (i in data.coordinates) {
					coordinates_data.push({
						'Site Name': data.coordinates[i].SITE_NAME,
						'Latitude': data.coordinates[i].latitude,
						'Longtitude': data.coordinates[i].longtitude,
						'Survey Date': data.coordinates[i].Survey_Date,
						'Project': data.coordinates[i].Project,
						'RRE Representative': data.coordinates[i].rre,
						'RFE Representative': data.coordinates[i].RFE_Representative,
						'SAQ Representative': data.coordinates[i].SAQ_Representative,
						'Category': data.coordinates[i].Category,
						'Vendor': data.coordinates[i].Vendor,
						'Site Status': data.coordinates[i].Site_Status,
						'Remarks': data.coordinates[i].Remarks,
					});
				}

			}
		},
		error: function (xhr, status, error) {
			Swal.fire({
				title: 'Opps!',
				text: "Something went wrong please try again.",
				type: 'error',
				showCancelButton: false,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Ok'
			}).then((result) => {
				if (result.value) {
					location.reload(true);
				}
			});
		}
	});
}

function getActivities(data) {
	let html = '';
	let ctr = 1, count = 0;
	let total = 0;
	if (data.length > 0) {
		for (i in data) {
			for (o in data[i].names) {
				let counts = data[i].names[o].subject.length;
				let more = parseInt(counts - 1) === 0 ? '' : ' +' + parseInt(counts - 1) + ' more <i class="fas fa-calendar-plus"></i>';

				let day = data[i].names[o].subject[0].day;
				let month = data[i].names[o].subject[0].month;

				let subs = "";
				let num = 1;
				for (a = 0; a < data[i].names[o].subject.length; a++) {
					subs += num + '.' + data[i].names[o].subject[a].subject_name + '\n';
					num++;
					total++;
				}

				if (ctr <= 4) {
					html +=
						'<div class="card mb-2">' +
							'<div class="row no-gutters">' +
							'<div class="col-md-4 bg-warning text-center">' +
							'<div class="card-body">' +
							'<p class="card-text">' +
							'<b>'+day+'</b><br>' +
							'<small class="text-bold">'+month+'</small>' +
							'</p>' +
						'</div>' +
						'</div>' +
						'<div class="col-md-8">' +
						'<p class="card-text container text-truncate mg-top-10">' +
						'<b data-toggle="tooltip" data-placement="top" title="' + data[i].names[o].subject[0].subject_name + '">'+data[i].names[o].subject[0].subject_name+'</b><br>' +
						'<small class="text-muted">'+data[i].names[o].subject[0].name+'</small><br>';
					if (parseInt(counts - 1) > 0) {
						html += '<small class="text-muted" data-toggle="tooltip" data-placement="top" title="' + subs + '">'+ more +'</small>';
					}
					html += '</p>' +
						'</div>' +
						'</div>' +
						'</div>';
					count++;
				}
				ctr++;
			}
		}
		let ctrs = data.length - count;
		if (ctrs > 0) {
			html += '<div class="card">' +
				'<b class="card-text text-center"> +' +  ctrs +  ' more' + '</b>' +
				'</div>';
		}
	} else {
		html = '<div class="empty mg-top-135">' +
			'<h5 class="text-center text-white">' +
			'<span class="far fa-calendar-alt display-3 text-dark"></span><br>' +
			'<h5 class="text-dark text-center">No events.</h5>' +
			'<label class="text-muted small text-center ">No list has been displayed yet.</label>' +
			'</h5>' +
			'</div>';
	}
	$('.surveys').text(data.length);
	$('.total_rings').text(total);
	$('.act-list').html(html);
	$('[data-toggle="tooltip"]').tooltip();
}

function get_users() {
	doAjax().then((data) => {
		let html = '';
		if (data.length > 0) {
			html = '<option value="" selected>All</option>';
			for (i in data) {
				html += '<option value=' + data[i].globe_id + '>' + data[i].fname + '</option>';
			}
		}
		$('#dash_names').html(html);
	});
}

function set_values(data) {
	$('.total-surveys').text(data.surveys.surveyed);
	$('.total-active').text(data.users.active);
	$('.u-total').text(data.users.users);
	$('.u-activate').text(data.users.activated);
	$('.u-deactivate').text(data.users.deactivated);
}

// function set_DashCards() {
// 	let html = '';
// 	for (i in dash_datas) {
// 		html += '<div class="card border-secondary">' +
// 			'    <div class="card-body">' +
// 			'      <h3 class="card-title t-dash-cards counter" id=' + dash_datas[i].id + '>0</h3>' +
// 			'      <p class="card-text"><small class="text-muted">' + dash_datas[i].name + '</small></p>' +
// 			'    </div>' +
// 			'  </div>';
// 	}
//
// 	$('.dash-blocks').html(html);
// 	$('.counter').countUp();
// }

function set_cards() {
	let current = moment().format("MMMM YYYY");
	let html = '';
	for (i in cards) {
		html += '<div class="card shadow">\n' +
			'    <div class="card-body">\n' +
			'\t\t\t\t\t\t\t\t<div class="row">\n' +
			'\t\t\t\t\t\t\t\t\t<div class="col">\n' +
			'\t\t\t\t\t\t\t\t\t\t<h6 class="card-title text-uppercase text-muted mb-0">'+cards[i].title+'</h6>\n' +
			'\t\t\t\t\t\t\t\t\t\t<span class="h4 font-weight-bold mg-top-5 '+cards[i].class_num+' counter">0</span>\n' +
			'\t\t\t\t\t\t\t\t\t</div>\n' +
			'\t\t\t\t\t\t\t\t\t<div class="col-auto">\n' +
			'\t\t\t\t\t\t\t\t\t\t<div class="pad-font icon icon-shape '+cards[i].color+' text-white rounded-circle shadow">\n' +
			'\t\t\t\t\t\t\t\t\t\t\t<i class="'+cards[i].icon+'"></i>\n' +
			'\t\t\t\t\t\t\t\t\t\t</div>\n' +
			'\t\t\t\t\t\t\t\t\t</div>\n' +
			'\t\t\t\t\t\t\t\t</div>';

		if (parseInt(i)  === 0) {
			html += '<p class="mt-3 mb-0 text-muted text-sm">\n' +
				'<span class="text-warning mr-2" data-toggle="tooltip" data-placement="top" title="Total number of Sites to be surveyed."><b class="total_rings">0 </b> <i>Sites</i></span>\n' +
				'<span class="text-nowrap">| '+current+'</span\n' +
				'</p>';
		} else if (parseInt(i)  === 1) {
			html += '\t\t\t\t\t\t\t\t<p class="mt-3 mb-0 text-muted text-sm text-center">\n' +
				'\t\t\t\t\t\t\t\t\t<span class="text-success mr-2" data-toggle="tooltip" data-placement="top" title="Activated"><i class="fas fa-user"></i> | <b class="u-activate counter">0</b></span>\n' +
				'\t\t\t\t\t\t\t\t\t<span class="text-danger mr-2" data-toggle="tooltip" data-placement="top" title="Deactivated"><i class="fas fa-user"></i> | <b class="u-deactivate counter">0</b></span>\n' +
				'\t\t\t\t\t\t\t\t</p>';
		} else if (parseInt(i)  === 2) {
			html += '\t\t\t\t\t\t\t\t<p class="mt-3 mb-0 text-muted text-sm">\n' +
				'\t\t\t\t\t\t\t\t\t<span class="text-danger mr-2"><i class="fas fa-arrow-down"></i> 0</span>\n' +
				'\t\t\t\t\t\t\t\t\t<span class="text-nowrap"></span>\n' +
				'\t\t\t\t\t\t\t\t</p>';
		}

		html += '</div>\n' +
			'  </div>';
	}

	$('#cards-dash').html(html);
	$('.counter').countUp();
}

function get_numbers() {
	$.ajax({
		type: 'GET',
		url: url + 'getnumber',
		dataType: 'json',
		async: false,
		success: function (data) {
			$("#g900").html(data.g900);
			$("#g1800").html(data.g1800);
			$("#u900").html(data.u900);
			$("#u2100").html(data.u2100);
			$("#l700").html(data.l700);
			$("#l1800").html(data.l1800);
			$("#l2300").html(data.l2300);
			$("#l2600").html(data.l2600);

		}
	});
}

function get_projects(data) {
	let canvasBar = document.getElementById("projects_chart");
	let ctxBar = canvasBar.getContext('2d');
	ctxBar.height = 100;

	let nom = 0;
	let awe = 0;
	let pmo = 0;
	let surveys = data.surveyed;
	let ddd = 0;
	let ibs = 0;
	let tssr = 0;
	let clp = 0;
	let sales_map = 0;
	let complaints = 0;
	let events = 0;

	let datas = {
		labels: ["Nominations", "AWE", "PMO", "Surveys", "DDD", "IBS", "TSSR", "CLP", "Sales Map", "Complaints", "Events"],
		datasets: [
			{
				label: "Projects",
				fill: true,
				backgroundColor: ['#2b580c', '#aeefec', '#f78259', '#162447', '#888888', '#dd7631', '#f6d743', '#06623b', '#442727', '#c70039', '#efa8e4' ],
				data: [
					getWholePercent(0, 0),
					getWholePercent(0, 0),
					getWholePercent(0, 0),
					getWholePercent(surveys, data.total),
					getWholePercent(0, 0),
					getWholePercent(0, 0),
					getWholePercent(0, 0),
					getWholePercent(0, 0),
					getWholePercent(0, 0),
					getWholePercent(0, 0),
					getWholePercent(0, 0),
				],
			}
		]
	};

	optionsBar.title = setTitle("Projects");

	build_chart(datas, optionsBar, ctxBar, 0, 'bar', 'barBal');
}

$(document).on('change', '.radio_survey', function () {
	let type = $(this).val();
	let id = $('#dash_names').val();
	get_coordinates(type, id);
});

$(document).on('change', '#dash_names', function () {
	let id = $(this).val();
	let type = $('input[name="radio_survey"]:checked').val();
	get_coordinates(type, id);
});

$(document).on('click', '#download_as_excel', function () {
	let today = moment().format("DD/MM/YYYY");
	if (coordinates_data.length > 0) {
		JSONToCSVConvertor(coordinates_data, "Survey_Tracker" + "(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

async function doAjax(param = '') {
	let result;

	try {
		result = await $.ajax({
			url: url + 'main_dash_counts',
			type: 'GET',
			data: param,
			dataType: 'json',
		});

		return result;
	} catch (error) {
		errorSwal();
	}
}
