let tssrData = [];
let tssr_id = [];
let del_tssr_status = false;
let charTssr = ['Coverage', 'Clutter_Type'];
$(function () {
  let html = '';
  for (i in charTssr) {
    html += '<div class="row border-top">' +
        				'<div class="col-sm-6 col-md-6 col-lg-6 border-right">' +
        					'<div class="chart-container" style="position: relative;">' +
        						'<canvas id="line_'+charTssr[i]+'" class="mg-top-10"></canvas>' +
        				'	</div>' +
        				'</div>' +
                '<div class="col-sm-6 col-md-6 col-lg-6">' +
                  '<div class="chart-container" style="position: relative;">' +
                      '<canvas id="pie_'+charTssr[i]+'" class="mg-top-10"></canvas>' +
                  '</div>' +
                '</div>' +
              '</div>';
  }

  $('#chartsTssr').html(html);
});

function setTssr(data) {
  if (data.details.length > 0) {
    $('.emptyTssrChartTrack, .emptyTssrTrack').addClass('d-none');
    $('.tssrChart, #tssrSearch, .tssrListsTrack').removeClass('d-none');
    setToCoverage(data.Coverage, data.total);
    setToClutterType(data.Clutter_Type, data.total);
    setToTssrDetails(data.details, data.pagination, data.total);
  } else {
    $('.emptyTssrChartTrack, .emptyTssrTrack').removeClass('d-none');
    $('.tssrChart, .tssrListsTrack').addClass('d-none');
  }
}

function setToCoverage(data, total = 0) {
  let canvasPie = document.getElementById("pie_Coverage");
  let ctxPie = canvasPie.getContext('2d');
  let canvasLine = document.getElementById("line_Coverage");
  let ctxLine = canvasLine.getContext('2d');

  let Indoor = data.Indoor;
  let Indoor_Outdoor = data.Indoor_Outdoor;
  let Outdoor = data.Outdoor;
  let NA = data.NA;

  let total_per = getWholePercent(Indoor, total) +
									getWholePercent(Indoor_Outdoor, total) +
									getWholePercent(Outdoor, total) +
									getWholePercent(NA, total)
									;

  let dataCoverage = {
		labels: [Indoor + ":Indoor",
						Indoor_Outdoor + ":Indoor-Outdoor",
						Outdoor + ": Outdoor",
						NA + ": NA"
						],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#a35638', '#8b2f97', '#e25822', '#ffd800'],
				data: [getWholePercent(Indoor, total),
							getWholePercent(Indoor_Outdoor, total),
							getWholePercent(Outdoor, total),
							getWholePercent(NA, total)],
			}
		]
	};

  optionsPie.elements = centerText('Coverage');
  optionsPie.title = setTitle("Pie Chart");
  optionsBar.title = setTitle("Bar Graph");

	build_chart(dataCoverage, optionsPie, ctxPie, total_per, 'doughnut', 'pieCoverage');
	build_chart(dataCoverage, optionsBar, ctxLine, total_per, 'horizontalBar', 'barCoverage');
}

function setToClutterType(data, total = 0) {
  let canvasPie = document.getElementById("pie_Clutter_Type");
  let ctxPie = canvasPie.getContext('2d');
  let canvasLine = document.getElementById("line_Clutter_Type");
  let ctxLine = canvasLine.getContext('2d');

  let Rural   	 = data.Rural;
  let Urban   	 = data.Urban;

  let total_per = getWholePercent(Rural, total) +
									getWholePercent(Urban, total);

  let dataCType = {
		labels: [Rural + ":Airport",
						Urban + ":Condominium"
						],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#a35638', '#8b2f97'],
				data: [getWholePercent(Rural, total),
							getWholePercent(Urban, total),
							],
			}
		]
	};

  optionsPie.elements = centerText('Clutter Type');
  optionsPie.title = setTitle("Pie Chart");
  optionsBar.title = setTitle("Bar Graph");

	build_chart(dataCType, optionsPie, ctxPie, total_per, 'doughnut', 'pieCType');
	build_chart(dataCType, optionsBar, ctxLine, total_per, 'horizontalBar', 'barCType');
}

$(document).on('keyup', '#'+track_key[5]+'Search', function(e) {
	e.preventDefault();
	get_TSSR_Track(0, row_page = 10, $(this).val());
});

$(document).on('change', '#'+track_key[5]+'_sort', function() {
		get_TSSR_Track(0, $(this).val());
});

$(document).on('click', '#'+track_key[5]+'_pagination a', function(e) {
	e.preventDefault();
	let row_num = $(this).attr('data-ci-pagination-page');
  get_TSSR_Track(row_num);
});

function get_TSSR_Track(row_num = 0, row_page = 10, search = '') {
	$.ajax({
		url: url + 'FileTrack/get_TSSR_Track/'+row_num,
		dataType: 'json',
		data: {row_num: row_num, row_page : row_page, search: search},
		type: 'GET',
		beforeSend() {
      let html = '<div class="content">' +
        '<h5 class="text-center text-white">' +
        '<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>' +
        '<h4 class="text-dark text-center">Loading</h4>' +
        '</h5>' +
        '</div>';
      $('#'+track_key[5]+'_datas').html(html);
    },
		success: function (data) {
				setTssr(data);
		},
		error: function (xhr, status, error) {
			errorSwal();
		}
	});
}

function setToTssrDetails(data, pagination, total) {
  let html = '';
  let count = 1;
  let ctr = 1;
  if (data.length > 0) {
    for (i in data) {
      html += '<tr class="tssr_id-'+ctr+'">' +
        '<td style="width:5% !important;" data-tssr-id='+data[i].id+'>' +
          '<div class="custom-control pad-left-rem custom-checkbox chk-tssr-'+i+'">' +
            '<input type="checkbox" name="table2_lists" class="custom-control-input check-tbl-tssr-item" id="check-tssr-item-'+i+'">' +
            '<label class="custom-control-label" for="check-tssr-item-'+i+'"></label>' +
          '</div>' +
        '</td>' +
        '<td class="text-center" style="width:10% !important;" data-id='+data[i].id+'>'+data[i].id+'.</td>' +
        '<td class="text-truncate" data-toggle="tooltip" data-placement="top" title="'+data[i].site_name+'">'+data[i].site_name+'</td>' +
        '<td class="text-center">'+data[i].region+'</td>' +
        '<td class="text-truncate" data-toggle="tooltip" data-placement="top" title="'+data[i].pla_id+'">'+data[i].pla_id+'</td>' +
        '<td class="text-truncate" data-toggle="tooltip" data-placement="top" title="'+data[i].coverage+'">'+data[i].coverage+'</td>' +
        '<td class="text-truncate" data-toggle="tooltip" data-placement="top" title="'+data[i].location+'">'+data[i].location+'</td>' +
        '<td class="text-truncate text-center" data-toggle="tooltip" data-placement="top" title="'+data[i].province+'">'+data[i].province+'</td>' +
        '<td class="text-truncate text-center" data-toggle="tooltip" data-placement="top" title="'+data[i].town+'">'+data[i].town+'</td>' +
        '<td class="text-truncate text-center" data-toggle="tooltip" data-placement="top" title="'+data[i].brgy+'">'+data[i].brgy+'</td>' +
        '<td class="text-truncate text-center" data-toggle="tooltip" data-placement="top" title="'+data[i].town_psgc+'">'+data[i].town_psgc+'</td>' +
        '<td class="text-truncate text-center" data-toggle="tooltip" data-placement="top" title="'+data[i].longitude+'">'+data[i].longitude  +'</td>' +
        '<td class="text-truncate text-center" data-toggle="tooltip" data-placement="top" title="'+data[i].latitude+'">'+data[i].latitude+'</td>' +
        '<td class="text-truncate text-center" data-toggle="tooltip" data-placement="top" title="'+data[i].baluarte_tagging+'">'+data[i].baluarte_tagging+'</td>' +
        '<td class="text-truncate text-center" data-toggle="tooltip" data-placement="top" title="'+data[i].clutter_type+'">'+data[i].clutter_type+'</td>';
        '<tr>';
      ctr++;
    }

    $('#emptytssrTrack').find('form-control').val('');
    $('#tssr_datas').html(html);
    $('#tssr_pagination').html(pagination);
    $('#' + track_key[5] + 'EditTrack, #' + track_key[5] + 'DelTrack,  #' + track_key[5] + 'ViewTrack').addClass('d-none');
    $('.' + track_key[5] + 'ListsTrack, #' + track_key[5] + 'AddTrack, #' + track_key[5] + 'ImportTrack').removeClass('d-none');
    $('.emptyTssrTrack').addClass('d-none');
    $('#chk_' + track_key[5] + '_item').prop('checked', false);
  } else {
    $('.emptyListsTracks, .emptyTssrTrack').removeClass('d-none');
    $('#chk_' + track_key[5] + '_item').prop('checked', false);
  }

  $('[data-toggle="tooltip"]').tooltip();
  $('.total_tssr_details').text(nFormatter(total));
}

$(document).on('click', '#'+track_key[5]+'ImportTrack', function() {
  $('#import_'+track_key[5]+'_modal').modal('show');
});

$(document).on('change', '#chk_' + track_key[5] + '_item', function() {
	tssrData = [];
	tssr_id = [];
	$('.check-tbl-' + track_key[5] + '-item').prop('checked', !!$(this).is(":checked"));
	let total_length = $('#' + track_key[5] + '_datas tr').length;
	if ($(this).is(":checked")) {
		for (i = 1; i <= total_length; i++) {
			let row = $('.' + track_key[5] + '_id-' + i);
			let id = row.closest("tr").find('td:eq(0)').attr('data-' + track_key[5] + '-id');
			let site_name = row.closest("tr").closest("tr").find('td:eq(2)').text();
      let region = row.closest("tr").find('td:eq(3)').text();
      let pla_id = row.closest("tr").find('td:eq(4)').text();
      let coverage = row.closest("tr").find('td:eq(5)').text();
      let address = row.closest("tr").find('td:eq(6)').text();
      let province = row.closest("tr").find('td:eq(7)').text();
      let town = row.closest("tr").find('td:eq(8)').text();
      let brgy = row.closest("tr").find('td:eq(9)').text();
      let psgc = row.closest("tr").find('td:eq(10)').text();
      let latitude = row.closest("tr").find('td:eq(11)').text();
      let longtitude = row.closest("tr").find('td:eq(12)').text();
    	let baluarte_tagging = row.closest("tr").find('td:eq(13)').text();
      let clutter_type = row.closest("tr").find('td:eq(14)').text();

			tssrData.push({
				id: id,
				site_name: site_name,
        region: region,
        pla_id: pla_id,
				coverage: coverage,
        address: address,
        province: province,
        town: town,
				brgy: brgy,
        psgc: psgc,
				longtitude: longtitude,
				latitude: latitude,
				baluarte_tagging: baluarte_tagging,
        clutter_type: clutter_type
			});
			tssr_id.push(id);
		}
		$('#' + track_key[5] + 'AddTrack, #' + track_key[5] + 'ImportTrack').addClass('d-none');
		$('#' + track_key[5] + 'EditTrack, #' + track_key[5] + 'DelTrack,  #' + track_key[5] + 'ViewTrack').removeClass('d-none');
		del_tssr_status = true;
	} else {
		$('#' + track_key[5] + 'AddTrack, #' + track_key[5] + 'ImportTrack').removeClass('d-none');
		$('#' + track_key[5] + 'EditTrack, #' + track_key[5] + 'DelTrack, #' + track_key[5] + 'ViewTrack').addClass('d-none');
		$('.' + track_key[5] + '_site_name, .' + track_key[5] + '_region, .' + track_key[4] + '_pla_id, .' + track_key[5] + '_coverage, .' + track_key[5] + '_address, .' + track_key[5] + '_province, .' + track_key[5] + '_brgy, .' + track_key[5] + '_town, .' + track_key[5] + '_psgc, .' + track_key[5] + '_latitude, .' + track_key[5] + '_longtitude, .' + track_key[5] + '_baluarte_tagging, .'  + track_key[5] + '_clutter_type').val('');
		del_tssr_status = false;
	}
});

$(document).on('click', '.check-tbl-' + track_key[5] + '-item', function() {
	let chk = $('#chk_' + track_key[5] + '_item');
	let total_checked = $('#' + track_key[5] + '_datas').find('.check-tbl-' + track_key[5] + '-item:checked').length;
	let total_length = $('#' + track_key[5] + '_datas tr').length;
	chk.prop('checked', total_checked === total_length);

	if (total_checked === total_length) {
		$('#' + track_key[5] + 'AddTrack, #' + track_key[5] + 'ImportTrack').addClass('d-none');
		$('#' + track_key[5] + 'EditTrack, #' + track_key[5] + 'DelTrack, #' + track_key[5] + 'ViewTrack').removeClass('d-none');
	} else {
		if (total_checked === 0) {
			$('#' + track_key[5] + 'AddTrack, #' + track_key[5] + 'ImportTrack').removeClass('d-none');
			$('#' + track_key[5] + 'EditTrack, #' + track_key[5] + 'DelTrack, #' + track_key[5] + 'ViewTrack').addClass('d-none');
		  $('.' + track_key[5] + '_site_name, .' + track_key[5] + '_region, .' + track_key[4] + '_pla_id, .' + track_key[5] + '_coverage, .' + track_key[5] + '_address, .' + track_key[5] + '_province, .' + track_key[5] + '_brgy, .' + track_key[5] + '_town, .' + track_key[5] + '_psgc, .' + track_key[5] + '_latitude, .' + track_key[5] + '_longtitude, .' + track_key[5] + '_baluarte_tagging, .'  + track_key[5] + '_clutter_type').val('');
		}
	}

  let id = $(this).closest("tr").find('td:eq(0)').attr('data-' + track_key[5] + '-id');
  let site_name = $(this).closest("tr").closest("tr").find('td:eq(2)').text();
  let region = $(this).closest("tr").find('td:eq(3)').text();
  let pla_id = $(this).closest("tr").find('td:eq(4)').text();
  let coverage = $(this).closest("tr").find('td:eq(5)').text();
  let address = $(this).closest("tr").find('td:eq(6)').text();
  let province = $(this).closest("tr").find('td:eq(7)').text();
  let town = $(this).closest("tr").find('td:eq(8)').text();
  let brgy = $(this).closest("tr").find('td:eq(9)').text();
  let psgc = $(this).closest("tr").find('td:eq(10)').text();
  let latitude = $(this).closest("tr").find('td:eq(11)').text();
  let longtitude = $(this).closest("tr").find('td:eq(12)').text();
  let baluarte_tagging = $(this).closest("tr").find('td:eq(13)').text();
  let clutter_type = $(this).closest("tr").find('td:eq(14)').text();

	if ($(this).is(":checked")) {
		tssrData.push({
      id: id,
      site_name: site_name,
      region: region,
      pla_id: pla_id,
      coverage: coverage,
      address: address,
      province: province,
      town: town,
      brgy: brgy,
      psgc: psgc,
      longtitude: longtitude,
      latitude: latitude,
      baluarte_tagging: baluarte_tagging,
      clutter_type: clutter_type
		});
		tssr_id.push(id);
		$('#' + track_key[5] + 'AddTrack, #' + track_key[5] + 'ImportTrack').addClass('d-none');
		$('#' + track_key[5] + 'EditTrack, #' + track_key[5] + 'DelTrack, #' + track_key[5] + 'ViewTrack').removeClass('d-none');
	} else {
		_.remove(tssrData, function(e) {
			return e.id === id
		});
		tssr_id.pop(id);
	}
});

$(document).on('click', function() {
	$('#file-Tssrs-upload').ajaxForm({
		url:  url + 'tssr_upload',
		contentType: false,
		cache:false,
		processData: false,
		beforeSubmit: function () {
			$('#submitTssrButton, #clear_data_file').prop("disabled", true);
			$('#progressTssrId').fadeIn('slow');
			$("#progressTssrId").css("display", "block");
			var percentValue = '0%';

			$('#progressBarTssr').width(percentValue);
			$('#percentTssr').html(percentValue);
		},
		uploadProgress: function (event, position, total, percentComplete) {
			var percentValue = percentComplete + '%';
			$("#progressBarTssr").animate({
				width: '' + percentValue + ''
			}, {
				duration: 5000,
				easing: "linear",
				step: function (x) {
					percentText = Math.round(x * 100 / percentComplete);
					$("#percentTssr").text(percentText + "%");
					if(percentText == "100") {
						setTimeout(function(){
							SWAL('success', 'Success!', 'Successfully uploaded.');
              $('#import_tssr_modal').modal('hide');
              clearTssr();
						}, 1000);
						setTimeout(function(){
							$('#progressTssrId').fadeOut('slow');
						}, 3000);
					}
				}
			});
		},
		error: function (response, status, e) {
			SWAL('error', 'Opps!', 'Something went wrong.');
			setTimeout(function(){
				$('#progressTssrId').fadeOut('slow');
			}, 3000);
		},
		complete: function (xhr) {
			if (xhr.responseText && xhr.responseText != "error")
			{
				let json = JSON.parse(xhr.responseText);

				if (json.type == 'error') {
					SWAL(json.type, json.title, json.msg);
					clearTssr();
					$('#progressTssrId').fadeOut('fast');
				} else {
          get_TSSR_Track();
				}
			}
			else{
				SWAL('error', 'Opps!', 'Problem in uploading file.');
				clearTssr();
				$("#progressBarTssr").stop();
				setTimeout(function(){
					$('#progressTssrId').fadeOut('slow');
				}, 3000);
			}
		}
	});
});

$(document).on('click', '#'+track_key[5]+'AddTrack', function() {
  $('#add_tssr_modal').modal('show');
});

$(document).on('click', '.append_'+track_key[5]+'_track', function(e){
	e.preventDefault();
	let length = $('#'+track_key[5]+'_table tr').length;
	setTssrToAppend(length);
});

function setTssrToAppend(length) {
	let html = "<tr class="+track_key[5]+"_rows-"+length+">" +
		"<td><textarea name=\"tssr_site_name[]\" id=\"tssr_site_name\" cols=\"30\" rows=\"1\" class=\"form-control tssr_site_name\" placeholder=\"Site Name\" required></textarea></td>" +
		"<td><input type=\"text\" name=\"tssr_region[]\" class=\"form-control tssr_region\" id=\"tssr_region\" placeholder=\"Building Name  \" required></td>" +
		"<td><input type=\"text\" name=\"tssr_pla_id[]\" class=\"form-control tssr_pla_id\" id=\"tssr_pla_id\" placeholder=\"Building Type\" required></td>" +
    "<td><input type=\"text\" name=\"tssr_coverage[]\" class=\"form-control tssr_coverage\" id=\"tssr_coverage\" placeholder=\"PLA ID\" required></td>" +
    "<td><input type=\"text\" name=\"tssr_address[]\" class=\"form-control tssr_address\" id=\"tssr_address\" placeholder=\"Address\" required></td>" +
    "<td><input type=\"text\" name=\"tssr_province[]\" class=\"form-control tssr_province\" id=\"tssr_province\" placeholder=\"Province\" required></td>" +
		"<td><input type=\"text\" name=\"tssr_town[]\" class=\"form-control tssr_town\" id=\"tssr_town\" placeholder=\"Municipality City\" required></td>" +
    "<td><input type=\"text\" name=\"tssr_brgy[]\" class=\"form-control tssr_brgy\" id=\"tssr_brgy\" placeholder=\"Barangay\" required></td>" +
    "<td><input type=\"text\" name=\"tssr_psgc[]\" class=\"form-control tssr_psgc\" id=\"tssr_psgc\" placeholder=\"PSGC\" required></td>" +
    "<td><input type=\"text\" name=\"tssr_latitude[]\" class=\"form-control tssr_latitude\" id=\"tssr_latitude\" placeholder=\"Latitude\" required></td>" +
    "<td><input type=\"text\" name=\"tssr_longtitude[]\" class=\"form-control tssr_longtitude\" id=\"tssr_longtitude\" placeholder=\"Longtitude\" required></td>" +
    "<td><input type=\"text\" name=\"tssr_baluarte_tagging[]\" class=\"form-control tssr_baluarte_tagging\" id=\"tssr_baluarte_tagging\" placeholder=\"Balaurte Tagging\" required></td>" +
    "<td><input type=\"text\" name=\"tssr_clutter_type[]\" class=\"form-control tssr_clutter_type\" id=\"tssr_clutter_type\" placeholder=\"Clutter Type\" required></td>" +
    "<td class='text-center'><button type=\"button\" class=\"btn btn-danger btn-sm mg-left-3 remove_"+track_key[5]+"_track\"><i class=\"fas fa-minus\"></i></button></td>" +
    "</tr>";
	  $('#'+track_key[5]+'_track_tables').append(html);
}

$(document).on('click', '.remove_'+track_key[5]+'_track', function(e){
	e.preventDefault();
	$(this).closest('tr').remove();
});

$(document).on('submit', '#edd_'+track_key[5]+'_Track', function(e){
	e.preventDefault();

	let values = $('input[name="'+track_key[5]+'_site_name[]"]').map(function() {
		return this.value;
	}).toArray();

	let checkExists = !values.every(function(v,i) {
		return values.indexOf(v) == i;
	});

	if (checkExists) {
		SWAL('error', 'Invalid!', 'Duplicate subject name.');
	} else {
		$.ajax({
			url: url + 'save_tssr_track',
			dataType: 'json',
			data: $(this).serialize(),
			type: 'POST',
			success: function(data) {
				SWAL(data.type, data.title,  data.msg);
				if (data.type === 'success') {
          $('#chk_tssr_item').prop('checked', false);
					get_TSSR_Track();
					SWAL(data.type, data.title,  data.msg);
					$('.selected').removeClass('selected');
					$('#'+track_key[5]+'AddTrack, #'+track_key[5]+'ImportTrack').removeClass('d-none');
					$('#'+track_key[5]+'EditTrack, #'+track_key[5]+'DelTrack, #'+track_key[5]+'ViewTrack').addClass('d-none');
					$('#tssrTitleTrack').text('Add TSSR Track');
					$('#add_'+track_key[5]+'_modal').modal('hide');
          clearTssr();
				}
			},
			error: function(xhr, status, error) {
				errorSwal();
			}
		});
	}
});

$(document).on('click', '#'+track_key[5]+'DelTrack', function() {
  Swal.fire({
		title: 'Are you sure?',
		text: "You want to remove this data.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Delete'
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: url + 'del_tssr_track',
				dataType: 'json',
				data:{id:tssr_id, status : del_tssr_status},
				type: 'POST',
				success: function(data) {
					clearTssr();
					SWAL(data.type, data.title,  data.msg);
					get_TSSR_Track();
          $('#'+track_key[5]+'AddTrack, #'+track_key[5]+'ImportTrack').removeClass('d-none');
          $('#'+track_key[5]+'EditTrack, #'+track_key[5]+'DelTrack, #'+track_key[5]+'ViewTrack').addClass('d-none');
				},
				error: function(xhr, status, error) {
					errorSwal();
				}
			});
		}
	});
});

$(document).on('click', '#'+track_key[5]+'EditTrack', function() {
  let html = '';
  let ctr = 0;
  if (tssrData.length > 0) {
    $('#'+track_key[5]+'_status').val(1);
    for (i in tssrData) {
      if (ctr <= 0) {
        $('#'+track_key[5]+'-iid').val(tssrData[i].id);
        $('#'+track_key[5]+'_site_name').val(tssrData[i].site_name);
        $('#'+track_key[5]+'_region').val(tssrData[i].region);
        $('#'+track_key[5]+'_pla_id').val(tssrData[i].pla_id);
        $('#'+track_key[5]+'_coverage').val(tssrData[i].coverage);
        $('#'+track_key[5]+'_address').val(tssrData[i].address);
        $('#'+track_key[5]+'_province').val(tssrData[i].province);
        $('#'+track_key[5]+'_town').val(tssrData[i].town);
        $('#'+track_key[5]+'_brgy').val(tssrData[i].brgy);
        $('#'+track_key[5]+'_psgc').val(tssrData[i].psgc);
        $('#'+track_key[5]+'_latitude').val(tssrData[i].latitude);
        $('#'+track_key[5]+'_longtitude').val(tssrData[i].longtitude);
        $('#'+track_key[5]+'_baluarte_tagging').val(tssrData[i].baluarte_tagging);
        $('#'+track_key[5]+'_clutter_type').val(tssrData[i].clutter_type);
      } else {
        html +=
        "<tr class="+track_key[5]+"_rows-"+ctr+">" +
         "<td>" +
          "<input type=\"hidden\" name=\"tssr_iid[]\" class=\"form-control\" value="+tssrData[i].id+">" +
          	"<input type=\"text\" name=\"tssr_site_name[]\" class=\"form-control tssr_site_name\" id=\"tssr_site_name\" placeholder=\"Site Name\" value="+tssrData[i].site_name+" required>" +
          "</td>" +
       		"<td><input type=\"text\" name=\"tssr_region[]\" class=\"form-control tssr_region\" id=\"tssr_region\" placeholder=\"Region\" value="+tssrData[i].region+" required></td>" +
       		"<td><input type=\"text\" name=\"tssr_pla_id[]\" class=\"form-control tssr_pla_id\" id=\"tssr_pla_id\" placeholder=\"PLA ID\" value="+tssrData[i].pla_id+" required></td>" +
          "<td><input type=\"text\" name=\"tssr_coverage[]\" class=\"form-control tssr_coverage\" id=\"tssr_coverage\" placeholder=\"Coverage\"  value="+tssrData[i].coverage+" required></td>" +
          "<td><input type=\"text\" name=\"tssr_address[]\" class=\"form-control tssr_address\" id=\"tssr_address\" value="+tssrData[i].address+" placeholder=\"Address\" required></td>" +
          "<td><input type=\"text\" name=\"tssr_province[]\" class=\"form-control tssr_province\" id=\"tssr_province\" placeholder=\"Province\" value="+tssrData[i].province+" required></td>" +
          "<td><input type=\"text\" name=\"tssr_town[]\" class=\"form-control tssr_town\" id=\"tssr_town\" placeholder=\"Town\" value="+tssrData[i].town+" required></td>" +
       		"<td><input type=\"text\" name=\"tssr_brgy[]\" class=\"form-control tssr_brgy\" id=\"tssr_brgy\" value='"+tssrData[i].brgy+"' placeholder=\"Barangay\" required></td>" +
       		"<td><input type=\"text\" name=\"tssr_psgc[]\" class=\"form-control tssr_psgc\" id=\"tssr_psgc\" placeholder=\"PSGC\" required value='"+tssrData[i].psgc+"'></td>" +
          "<td><input type=\"text\" name=\"tssr_latitude[]\" class=\"form-control tssr_latitude\" id=\"tssr_latitude\" placeholder=\"Latitude\"  value='"+tssrData[i].latitude+"'></td>" +
          "<td><input type=\"text\" name=\"tssr_longtitude[]\" class=\"form-control tssr_longtitude\" id=\"tssr_longtitude\" placeholder=\"Longtitude\"  value='"+tssrData[i].longtitude+"'></td>" +
          "<td><input type=\"text\" name=\"tssr_baluarte_tagging[]\" class=\"form-control tssr_baluarte_tagging\" id=\"tssr_baluarte_tagging\" placeholder=\"Baluarte Tagging\"  value='"+tssrData[i].baluarte_tagging+"'></td>" +
          "<td><input type=\"text\" name=\"tssr_clutter_type[]\" class=\"form-control tssr_clutter_type\" id=\"tssr_clutter_type\" placeholder=\"Clutter Type\"  value='"+tssrData[i].clutter_type+"'></td>" +
          "<td class='text-center'><button type=\"button\" class=\"btn btn-danger btn-sm mg-left-3 remove_"+track_key[5]+"_track\"><i class=\"fas fa-minus\"></i></button></td>" +
         "</tr>";
      }
      ctr++;
    }
    $('#'+track_key[5]+'_track_tables').append(html);
    $('#add_'+track_key[5]+'_modal').modal('show');
  } else {
    SWAL('error', 'Opps!', 'Please check on the data listed.');
  }
});

$(document).on('click', '.'+track_key[5]+'_close', function() {
  let ctr = 1;
  for (i in tssrData) {
    $('.'+track_key[5]+'_rows-'+ctr).remove();
    ctr++;
  }
});

$(document).on('click', '#' + track_key[5] + 'ViewTrack', function() {
	let html = '';
	let ctr = 1;
	for (i in tssrData) {
		let col = (ctr <= 1) ? 4 : 1;
		html += "<tr>" +
			"<td><small>" + ctr + ".</small></td>" +
			"<td><small>" + tssrData[i].site_name + "</small></td>" +
			"<td><small>" + tssrData[i].region + "</small></td>" +
			"<td><small>" + tssrData[i].pla_id + "</small></td>" +
			"<td><small>" + tssrData[i].coverage + "</small></td>" +
			"<td><small>" + tssrData[i].address + "</small></td>" +
			"<td><small>" + tssrData[i].province + "</small></td>" +
			"<td><small>" + tssrData[i].town + "</small></td>" +
      "<td><small>" + tssrData[i].brgy + "</small></td>" +
			"<td><small>" + tssrData[i].psgc + "</small></td>" +
			"<td><small>" + tssrData[i].latitude + "</small></td>" +
			"<td><small>" + tssrData[i].longtitude + "</small></td>" +
      "<td><small>" + tssrData[i].baluarte_tagging + "</small></td>" +
      "<td><small>" + tssrData[i].clutter_type + "</small></td>" +
			"</tr>";
		ctr++;
	}

	$('#view_' + track_key[5] + '_build').html(html);
	$('#viewTssrModal').modal('show');
});

function clearTssr() {
	$('#progressBarTssr').width(0);
	$('#submit'+track_key[5]+'Button, #clear_data_file').prop("disabled", false);
	$('#'+track_key[5]+'_file').val('');
	$('#chk_'+track_key[5]+'_item').prop('checked', false);
	let ctr = 1;
  for (i in tssrData) {
    $('.'+track_key[5]+'_rows-'+ctr).remove();
		ctr++;
  }
  $('.' + track_key[5] + '_site_name, .' + track_key[5] + '_region, .' + track_key[5] + '_pla_id, .' + track_key[5] + '_coverage, .' + track_key[5] + '_address, .' + track_key[5] + '_province, .' + track_key[5] + '_brgy, .' + track_key[5] + '_town, .' + track_key[5] + '_psgc, .' + track_key[5] + '_latitude, .' + track_key[5] + '_longtitude, .' + track_key[5] + '_baluarte_tagging, .'  + track_key[5] + '_clutter_type').val('');
  tssrData = [];
}
