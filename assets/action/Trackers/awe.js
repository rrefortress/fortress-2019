let aweData = [];
let awe_id = [];
let del_awe_status = false;
let charAwe = ['Program_Tag', 'Baluarte_Tagging', 'Priority_Tag'];

$(function () {
  let html = '';
  for (i in charAwe) {
    html += '<div class="row border-top">' +
        				'<div class="col-sm-6 col-md-6 col-lg-6 border-right">' +
        					'<div class="chart-container" style="position: relative;">' +
        						'<canvas id="line'+charAwe[i]+'" class="mg-top-10"></canvas>' +
        				'	</div>' +
        				'</div>' +
                '<div class="col-sm-6 col-md-6 col-lg-6">' +
                  '<div class="chart-container" style="position: relative;">' +
                      '<canvas id="pie'+charAwe[i]+'" class="mg-top-10"></canvas>' +
                  '</div>' +
                '</div>' +
              '</div>';
  }

  $('#chartsAWE').html(html);
});

function setAWE(data) {
  let html = '';
  if (data.details.length > 0) {
    $('.emptyAweChartTrack, .emptyAweTrack').addClass('d-none');
    $('.aweFilter, .aweChart, #aweSearch, .aweListsTrack').removeClass('d-none');
    setBaluarteTagging(data.Baluarte_Tagging, data.total);
    setPriority_Tag(data.Priority_Tag, data.total);
    setProgram_Tag(data.Program_Tag, data.total);
    setTOAweDetails(data.details, data.pagination, data.total);
    if (data.pname.length > 0) {
      html += '<option value="" selected>All</option>';
      for (i in data.pname) {
        html += '<option>'+data.pname[i].name+'</option>';
      }
    }
    $('#aweProvince').html(html);
  } else {
    $('.emptyAweChartTrack, .emptyAweTrack').removeClass('d-none');
    $('.aweFilter, .aweChart, .aweListsTrack').addClass('d-none');
  }
}

$(document).on('change', '#aweProvince', function() {
  	$.ajax({
  		url: url + 'get_AWE_Track',
  		dataType: 'json',
  		data: {name: $(this).val()},
  		type: 'POST',
  		cache:false,
  		success: function (data) {
        setBaluarteTagging(data.Baluarte_Tagging, data.total);
        setPriority_Tag(data.Priority_Tag, data.total);
        setProgram_Tag(data.Program_Tag, data.total);
  		},
  		error: function (xhr, status, error) {
  			errorSwal();
  		}
  	});
});

$(document).on('click', '#'+track_key[1]+'ImportTrack', function() {
  $('#import_'+track_key[1]+'_modal').modal('show');
});

$(document).on('keyup', '#'+track_key[1]+'Search', function(e) {
	e.preventDefault();
	get_AWE_Track(0, row_page = 10, $(this).val());
});

$(document).on('change', '#'+track_key[1]+'_sort', function() {
		get_AWE_Track(0, $(this).val());
});

$(document).on('click', '#'+track_key[1]+'_pagination a', function(e) {
	e.preventDefault();
	let row_num = $(this).attr('data-ci-pagination-page');
  get_AWE_Track(row_num);
});

function get_AWE_Track(row_num = 0, row_page = 10, search = '') {
	$.ajax({
		url: url + 'FileTrack/get_AWE_Track/'+row_num,
		dataType: 'json',
		data: {row_num: row_num, row_page : row_page, search: search},
		type: 'GET',
		beforeSend() {
      let html = '<div class="content">' +
        '<h5 class="text-center text-white">' +
        '<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>' +
        '<h4 class="text-dark text-center">Loading</h4>' +
        '</h5>' +
        '</div>';
      $('#'+track_key[1]+'_datas').html(html);
    },
		success: function (data) {
				setAWE(data);
		},
		error: function (xhr, status, error) {
			errorSwal();
		}
	});
}

function setTOAweDetails(data, pagination, total) {
  let html = '';
  let count = 1;
  let ctr = 1;
  if (data.length > 0) {
    for (i in data) {
      html += '<tr class="awe_id-'+ctr+'">' +
        '<td style="width:5% !important;" data-awe-id='+data[i].id+'>' +
          '<div class="custom-control pad-left-rem custom-checkbox chk-awe-'+i+'">' +
            '<input type="checkbox" name="table2_lists" class="custom-control-input check-tbl-awe-item" id="check-awe-item-'+i+'">' +
            '<label class="custom-control-label" for="check-awe-item-'+i+'"></label>' +
          '</div>' +
        '</td>' +
				'<td class="text-center" style="width:7% !important;">'+data[i].id+'.</td>' +
        '<td class="text-truncate" data-toggle="tooltip" data-placement="top" title='+data[i].SERIAL_NUMBER+'>'+data[i].SERIAL_NUMBER+'</td>' +
        '<td class="text-truncate" data-toggle="tooltip" data-placement="top" title='+data[i].SITE_NAME+'>'+data[i].SITE_NAME+'</td>' +

				'<td class="text-center text-truncate" data-toggle="tooltip" data-placement="top" title="'+data[i].REGION+'">'+data[i].REGION+'</td>' +
				'<td class="text-center text-truncate" data-toggle="tooltip" data-placement="top" title="'+data[i].PROVINCE+'">'+data[i].PROVINCE+'</td>' +
        '<td class="text-center text-truncate" data-toggle="tooltip" data-placement="top" title="'+data[i].TOWN_CITY+'">'+data[i].TOWN_CITY+'</td>' +
        // '<td class="text-center text-truncate">'+data[i].Baluarte_Tagging+'</td>' +
        // '<td class="text-center text-truncate">'+data[i].Priority_Tag+'</td>' +
        '<td class="text-center text-truncate" data-toggle="tooltip" data-placement="top" title='+data[i].Town_Priority+'>'+data[i].Town_Priority+'</td>' +
        '<td class="text-center text-truncate" data-toggle="tooltip" data-placement="top" title='+data[i].Latitude+'>'+data[i].Latitude+'</td>' +
        '<td class="text-center text-truncate" data-toggle="tooltip" data-placement="top" title='+data[i].Longitude+'>'+data[i].Longitude+'</td>' +
        '<td class="text-center text-truncate" data-toggle="tooltip" data-placement="top" title='+data[i].VENDOR+'>'+data[i].VENDOR+'</td>' +
        '<td class="text-center text-truncate" data-toggle="tooltip" data-placement="top" title='+data[i].Program_Tag+'>'+data[i].Program_Tag+'</td>' +
        '<td class="text-center text-truncate" data-toggle="tooltip" data-placement="top" title='+data[i].PLANNED_TECH+'>'+data[i].PLANNED_TECH+'</td>' +
        '<td class="text-center text-truncate" data-toggle="tooltip" data-placement="top" title='+data[i].Tagging+'>'+data[i].Tagging+'</td>' +
        '<td class="text-center text-truncate" data-toggle="tooltip" data-placement="top" title='+data[i].Site_Type+'>'+data[i].Site_Type+'</td>';
        // '<td class="text-center text-truncate">'+data[i].Final_Rank_Town_Ranking+'</td>';
			'<tr>';
      ctr++;
    }
    $('#emptyAweTrack').find('form-control').val('');
    $('#awe_datas').html(html);
    $('#awe_pagination').html(pagination);
    $('.btnTracks').show();
    $('#' + track_key[1] + 'EditTrack, #' + track_key[1] + 'DelTrack,  #' + track_key[1] + 'ViewTrack').addClass('d-none');
    $('.' + track_key[1] + 'ListsTrack, #' + track_key[1] + 'AddTrack, #' + track_key[1] + 'ImportTrack').removeClass('d-none');
    $('.emptyAweTrack').addClass('d-none');
    $('#chk_' + track_key[1] + '_item').prop('checked', false);
  } else {
    $('.emptyaweTrack').removeClass('d-none');
    $('.' + track_key[1] + 'ListsTrack, #' + track_key[1] + 'AddTrack, #' + track_key[1] + 'ImportTrack').addClass('d-none');
    $('#chk_' + track_key[1] + '_item').prop('checked', false);
  }
  $('[data-toggle="tooltip"]').tooltip();
  $('.total_awe_details').text(total);
}

function setBaluarteTagging(data, total = 0) {
  let canvasPie = document.getElementById("pieBaluarte_Tagging");
  let ctxPie = canvasPie.getContext('2d');
  let canvasLine = document.getElementById("lineBaluarte_Tagging");
  let ctxLine = canvasLine.getContext('2d');

  let Baluarte      = data.Baluarte;
  let Baluarte_2018 = data.Baluarte_2018;
  let Non           = data.Non;
  let total_per = getWholePercent(Baluarte, total) + getWholePercent(Baluarte_2018, total) + getWholePercent(Non, total);

  let dataAwe = {
		labels: [Baluarte + ":Baluarte", Baluarte_2018 + ":Baluarte - 2018", Non + ":Non-baluarte"],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#00897b', '#263238', '#e53935'],
				data: [getWholePercent(Baluarte, total), getWholePercent(Baluarte_2018, total), getWholePercent(Non, total)],
			}
		]
  };

  optionsPie.elements = centerText('Baluarte Tag');
  optionsPie.title = setTitle("Pie Chart");
  optionsBar.title = setTitle("Bar Graph");

  build_chart(dataAwe, optionsPie, ctxPie, total_per, 'doughnut', 'pieBal');
  build_chart(dataAwe, optionsBar, ctxLine, total_per, 'horizontalBar', 'barBal');
}

function setPriority_Tag(data, total = 0) {
  let canvasPie = document.getElementById("piePriority_Tag");
  let ctxPie = canvasPie.getContext('2d');
  let canvasLine = document.getElementById("linePriority_Tag");
  let ctxLine = canvasLine.getContext('2d');

  let low_investment = data.low_investment;
  let no_tag         = data.no_tag;
  let no_build_sell  = data.no_build_sell;
  let prio_1_build   = data.prio_1_build;
  let prio_2_build   = data.prio_2_build;
  let prio_3_build   = data.prio_3_build;
  let total_per = getWholePercent(low_investment, total) +
                  getWholePercent(no_tag, total) +
                  getWholePercent(no_build_sell, total) +
                  getWholePercent(prio_1_build, total) +
                  getWholePercent(prio_2_build, total) +
                  getWholePercent(prio_3_build, total);

  let dataAwe = {
    labels: [low_investment + ":Low Investment", no_tag + ":No Tag", no_build_sell + ":No Build-Sell", prio_1_build + ":Prio 1 Build", prio_2_build + ":Prio 2 Build", prio_3_build + ":Prio 3 Build"],
    datasets: [
      {
        label: "",
        fill: true,
        backgroundColor: ['#00897b', '#263238', '#e53935', '#8e24aa', '#e65100', '#ff4081'],
        data: [getWholePercent(low_investment, total),
              getWholePercent(no_tag, total),
              getWholePercent(no_build_sell, total),
              getWholePercent(prio_1_build, total),
              getWholePercent(prio_2_build, total),
              getWholePercent(prio_3_build, total),
            ],
      }
    ]
  };

  optionsPie.elements = centerText('Priority Tag');
  optionsPie.title = setTitle("Pie Chart");
  optionsBar.title = setTitle("Bar Graph");

  build_chart(dataAwe, optionsPie, ctxPie, total_per, 'doughnut', 'piePTag');
  build_chart(dataAwe, optionsBar, ctxLine, total_per, 'horizontalBar', 'barPTag');
}

function setProgram_Tag(data, total = 0) {
  let canvasPie = document.getElementById("pieProgram_Tag");
  let ctxPie = canvasPie.getContext('2d');
  let canvasLine = document.getElementById("lineProgram_Tag");
  let ctxLine = canvasLine.getContext('2d');

  let baymax = data.baymax;
  let baymax_refresh_add = data.baymax_refresh_add;
  let capacity  = data.capacity;
  let capacity_sites   = data.capacity_sites;
  let eg   = data.eg;
  let lgu_vip   = data.lgu_vip;
  let mass   = data.mass;
  let ntg_driven   = data.ntg_driven;
  let rre_additionals   = data.rre_additionals;
  let rre_nominations   = data.rre_nominations;
  let victory_list   = data.victory_list;

  let total_per = getWholePercent(baymax, total) +
                  getWholePercent(baymax_refresh_add, total) +
                  getWholePercent(capacity, total) +
                  getWholePercent(capacity_sites, total) +
                  getWholePercent(eg, total) +
                  getWholePercent(lgu_vip, total) +
                  getWholePercent(ntg_driven, total) +
                  getWholePercent(mass, total) +
                  getWholePercent(rre_additionals, total) +
                  getWholePercent(rre_nominations, total) +
                  getWholePercent(victory_list, total)
                  ;

  let dataAwe = {
    labels: [baymax + ":BayMax",
             baymax_refresh_add + ":BAYMAX REFRESHED Add",
             capacity + ":Capacity",
             capacity_sites + ":Capacity Sites",
             eg + ":EG",
             lgu_vip + ":LGU/VIP",
             ntg_driven + ":NTG Driven",
             mass + ":Mass",
             rre_additionals + ":RRE Additional Capacity AdditionalS",
             rre_nominations + ":RRE Nominations",
             victory_list + ":Victory List",
           ],
    datasets: [
      {
        label: "",
        fill: true,
        backgroundColor: [
                          '#00897b',
                          '#263238',
                          '#e53935',
                          '#8e24aa',
                          '#6d4c41',
                          '#ff4081',
                          '#3949ab',
                          '#e65100',
                          '#ffff00',
                          '#5e35b1',
                          '#43a047',
                        ],
        data: [         getWholePercent(baymax, total),
                        getWholePercent(baymax_refresh_add, total),
                        getWholePercent(capacity, total),
                        getWholePercent(capacity_sites, total),
                        getWholePercent(eg, total),
                        getWholePercent(lgu_vip, total),
                        getWholePercent(ntg_driven, total),
                        getWholePercent(mass, total),
                        getWholePercent(rre_additionals, total),
                        getWholePercent(rre_nominations, total),
                        getWholePercent(victory_list, total)
            ],
      }
    ]
  };

  optionsPie.elements = centerText(total_per + '%');
  optionsPie.title = setTitle("Pie Chart - Program Tag");
  optionsBar.title = setTitle("Bar Graph - Program Tag");

  build_chart(dataAwe, optionsPie, ctxPie, total_per, 'doughnut', 'pieProg');
  build_chart(dataAwe, optionsBar, ctxLine, total_per, 'horizontalBar', 'barProg');
}

$(document).on('click', function() {
	$('#file-awe-upload').ajaxForm({
		url:  url + 'awe_upload',
		contentType: false,
		cache:false,
		processData: false,
		beforeSubmit: function () {
			$('#submitaweButton, #clear_data_file').prop("disabled", true);
			$('#progressAweId').fadeIn('slow');
			$("#progressAweId").css("display", "block");
			var percentValue = '0%';

			$('#progressBarAwe').width(percentValue);
			$('#percentAwe').html(percentValue);
		},
		uploadProgress: function (event, position, total, percentComplete) {
			var percentValue = percentComplete + '%';
			$("#progressBarAwe").animate({
				width: '' + percentValue + ''
			}, {
				duration: 5000,
				easing: "linear",
				step: function (x) {
					percentText = Math.round(x * 100 / percentComplete);
					$("#percentAwe").text(percentText + "%");
					if(percentText == "100") {
						setTimeout(function(){
							SWAL('success', 'Success!', 'Successfully uploaded.');
              $('#import_awe_modal').modal('hide');
              clearAwe();
						}, 1000);
						setTimeout(function(){
							$('#progressAweId').fadeOut('slow');
						}, 3000);
					}
				}
			});
		},
		error: function (response, status, e) {
			SWAL('error', 'Opps!', 'Something went wrong.');
			setTimeout(function(){
				$('#progressAweId').fadeOut('slow');
			}, 3000);
		},
		complete: function (xhr) {
			if (xhr.responseText && xhr.responseText != "error")
			{
				let json = JSON.parse(xhr.responseText);

				if (json.type == 'error') {
					SWAL(json.type, json.title, json.msg);
					clearAwe();
					$('#progressAweId').fadeOut('fast');
				} else {
					get_AWE_Track();
				}
			}
			else{
				SWAL('error', 'Opps!', 'Problem in uploading file.');
				clearAwe();
				$("#progressBarAwe").stop();
				setTimeout(function(){
					$('#progressAweId').fadeOut('slow');
				}, 3000);
			}
		}
	});
});

$(document).on('click', '#'+track_key[1]+'AddTrack', function() {
  $('#add_awes_modal').modal('show');
});

$(document).on('click', '#'+track_key[1]+'EditTrack', function() {
	console.log(aweData)
  let html = '';
  let ctr = 0;
  if (aweData.length > 0) {
    $('#'+track_key[1]+'_status').val(1);
    for (i in aweData) {
      if (ctr <= 0) {
        $('#'+track_key[1]+'-iid').val(aweData[i].id);
        $('#'+track_key[1]+'_serial_number').val(aweData[i].serial_number);
        $('#'+track_key[1]+'_site_name').val(aweData[i].site_name);
        $('#'+track_key[1]+'_region').val(aweData[i].region);
        $('#'+track_key[1]+'_province').val(aweData[i].province);
        $('#'+track_key[1]+'_town_city').val(aweData[i].town_city);
        $('#'+track_key[1]+'_town_priority').val(aweData[i].priority_town);
        $('#'+track_key[1]+'_latitude').val(aweData[i].latitude);
        $('#'+track_key[1]+'_longtitude').val(aweData[i].longtitude);
        $('#'+track_key[1]+'_vendor').val(aweData[i].vendor);
        $('#'+track_key[1]+'_program_tag').val(aweData[i].program_tag);
        $('#'+track_key[1]+'_planned_tech').val(aweData[i].planned_tech);
        $('#'+track_key[1]+'_tagging').val(aweData[i].tagging);
        $('#'+track_key[1]+'_site_type').val(aweData[i].site_type);
      } else {
        html +=
        "<tr class="+track_key[1]+"_rows-"+ctr+">" +
         "<td>" +
          "<input type=\"hidden\" name=\"awe_iid[]\" class=\"form-control\" value="+aweData[i].id+">" +
          	"<input type=\"text\" name=\"awe_serial_number[]\" class=\"form-control awe_serial_number\" id=\"awe_serial_number\" placeholder=\"Serial Number\" value="+aweData[i].serial_number+" required>" +
        "</td>" +
       		"<td><input type=\"text\" name=\"awe_site_name[]\" id=\"awe_site_name\" cols=\"30\" rows=\"1\" class=\"form-control awe_site_name\" placeholder=\"Site Name\" value="+aweData[i].site_name+" required></td>" +
       		"<td><input type=\"text\" name=\"awe_region[]\" class=\"form-control awe_region\" id=\"awe_region\" placeholder=\"Region\" value="+aweData[i].region+" required></td>" +
       		"<td><input type=\"text\" name=\"awe_province[]\" class=\"form-control awe_province\" id=\"awe_province\" placeholder=\"Province\" value="+aweData[i].province+" required></td>" +
          "<td><input type=\"text\" name=\"awe_town_city[]\" class=\"form-control awe_town_city\" id=\"awe_town_city\" placeholder=\"Town City\"  value="+aweData[i].town_city+" required></td>" +

          "<td><input type=\"text\" name=\"awe_town_priority[]\" class=\"form-control awe_town_priority\" id=\"awe_town_priority\" value="+aweData[i].priority_town+" placeholder=\"Town Priority\" required></td>" +
          "<td><input type=\"text\" name=\"awe_latitude[]\" class=\"form-control awe_latitude\" id=\"awe_latitude\" placeholder=\"Latitude\" value="+aweData[i].latitude+" required></td>" +
          "<td><input type=\"text\" name=\"awe_longtitude[]\" class=\"form-control awe_longtitude\" id=\"awe_longtitude\" placeholder=\"Longtitude\" value="+aweData[i].longtitude+" required></td>" +
       		"<td><input type=\"text\" name=\"awe_vendor[]\" class=\"form-control awe_vendor\" id=\"awe_vendor\" placeholder=\"Vendor\" value="+aweData[i].vendor+" required></td>" +
       		"<td><input type=\"text\" name=\"awe_program_tag[]\" class=\"form-control awe_program_tag\" id=\"awe_program_tag\" value='"+aweData[i].program_tag+"' placeholder=\"Program Tag\" required></td>" +
       		"<td><input type=\"text\" name=\"awe_planned_tech[]\" class=\"form-control awe_planned_tech\" id=\"awe_planned_tech\" placeholder=\"Planned Tech\" required value='"+aweData[i].planned_tech+"'></td>" +
          "<td><input type=\"text\" name=\"awe_tagging[]\" class=\"form-control awe_tagging\" id=\"awe_tagging\" placeholder=\"Tagging\"  value='"+aweData[i].tagging+"'></td>" +
          "<td><input type=\"text\" name=\"awe_site_type[]\" class=\"form-control awe_site_type\" id=\"awe_site_type\" placeholder=\"Site Type\" value='"+aweData[i].site_type+"'></td>" +
          "<td class='text-center'><button type=\"button\" class=\"btn btn-danger btn-sm mg-left-3 remove_"+track_key[1]+"_track\"><i class=\"fas fa-minus\"></i></button></td>" +
         "</tr>";
      }
      ctr++;
    }
    $('#'+track_key[1]+'_track_tables').append(html);
    $('#add_'+track_key[1]+'s_modal').modal('show');

  } else {
    SWAL('error', 'Opps!', 'Please check on the data listed.');
  }

});

$(document).on('click', '#'+track_key[1]+'DelTrack', function() {
  Swal.fire({
		title: 'Are you sure?',
		text: "You want to remove this data.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Delete'
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: url + 'del_awe_track',
				dataType: 'json',
				data:{id:awe_id, status : del_awe_status},
				type: 'POST',
				success: function(data) {
					clearAwe();
					SWAL(data.type, data.title,  data.msg);
					get_AWE_Track();
          $('#'+track_key[1]+'AddTrack, #'+track_key[1]+'ImportTrack').removeClass('d-none');
          $('#'+track_key[1]+'EditTrack, #'+track_key[1]+'DelTrack, #'+track_key[1]+'ViewTrack').addClass('d-none');
				},
				error: function(xhr, status, error) {
					errorSwal();
				}
			});
		}
	});
});

$(document).on('click', '.'+track_key[1]+'_close', function() {
  let ctr = 1;
  for (i in aweData) {
    $('.'+track_key[1]+'_rows-'+ctr).remove();
    ctr++;
  }
});

$(document).on('change', '#chk_'+track_key[1]+'_item', function () {
  aweData = [];
  awe_id = [];
	$('.check-tbl-'+track_key[1]+'-item').prop('checked', !!$(this).is(":checked"));
	let total_length = $('#'+track_key[1]+'_datas tr').length;
	if ($(this).is(":checked")) {
		for (i = 1; i <= total_length; i++) {
			let row = $('.'+track_key[1]+'_id-'+i);
      let id = row.closest("tr").find('td:eq(0)').attr('data-'+track_key[1]+'-id');
      let serial_number = row.closest("tr").closest("tr").find('td:eq(2)').text();
      let site_name = row.closest("tr").closest("tr").find('td:eq(3)').text();
      let region = row.closest("tr").find('td:eq(4)').text();
      let province = row.closest("tr").find('td:eq(5)').text();
      let town_city = row.closest("tr").find('td:eq(6)').text();
      let priority_town = row.closest("tr").find('td:eq(7)').text();
      let latitude = row.closest("tr").find('td:eq(8)').text();
      let longtitude = row.closest("tr").find('td:eq(9)').text();
      let vendor = row.closest("tr").find('td:eq(10)').text();
      let program_tag = row.closest("tr").find('td:eq(11)').text();
      let planned_tech = row.closest("tr").find('td:eq(12)').text();
      let tagging = row.closest("tr").find('td:eq(13)').text();
      let site_type = row.closest("tr").find('td:eq(14)').text();

      aweData.push({id:id,
                serial_number: serial_number,
                site_name: site_name,
                region: region,
                province: province,
                town_city: town_city,
                priority_town: priority_town,
                latitude: latitude,
                longtitude: longtitude,
                vendor: vendor,
                program_tag: program_tag,
                planned_tech: planned_tech,
                tagging: tagging,
                site_type: site_type,
              });
      awe_id.push(id);
		}
    $('#'+track_key[1]+'AddTrack, #'+track_key[1]+'ImportTrack').addClass('d-none');
    $('#'+track_key[1]+'EditTrack, #'+track_key[1]+'DelTrack, #'+track_key[1]+'ViewTrack').removeClass('d-none');
		del_awe_status = true;
	} else {
    $('#'+track_key[1]+'AddTrack, #'+track_key[1]+'ImportTrack').removeClass('d-none');
    $('#'+track_key[1]+'EditTrack, #'+track_key[1]+'DelTrack, #'+track_key[1]+'ViewTrack').addClass('d-none');
          $('.'+track_key[1]+'serial_number, .'+track_key[1]+'_site_name, .'+track_key[1]+'_region, .'+track_key[1]+'_province, .'+track_key[1]+'_town_city, .'+track_key[1]+'awe_baluarte_tagging, .'+track_key[1]+'awe_priority_tag, .'+track_key[1]+'awe_town_priority, .'+track_key[1]+'_latitude, .'+track_key[1]+'_longtitude, .'+track_key[1]+'_vendor, .'+track_key[1]+'_program_tag, .'+track_key[1]+'_tagging, .'+track_key[1]+'_planned_tech, .'+track_key[1]+'_site_type').val('');
		del_awe_status = false;
	}
});

$(document).on('click', '.check-tbl-'+track_key[1]+'-item', function () {
	let chk = $('#chk_'+track_key[1]+'_item');
	let total_checked = $('#'+track_key[1]+'_datas').find('.check-tbl-'+track_key[1]+'-item:checked').length;
	let total_length = $('#'+track_key[1]+'_datas tr').length;
	chk.prop('checked', total_checked === total_length);

	if (total_checked === total_length) {
    $('#'+track_key[1]+'AddTrack, #'+track_key[1]+'ImportTrack').addClass('d-none');
    $('#'+track_key[1]+'EditTrack, #'+track_key[1]+'DelTrack, #'+track_key[1]+'ViewTrack').removeClass('d-none');
	} else {
    if ( total_checked === 0) {
      $('#'+track_key[1]+'AddTrack, #'+track_key[1]+'ImportTrack').removeClass('d-none');
      $('#'+track_key[1]+'EditTrack, #'+track_key[1]+'DelTrack, #'+track_key[1]+'ViewTrack').addClass('d-none');
      $('.'+track_key[1]+'serial_number, .'+track_key[1]+'_site_name, .'+track_key[1]+'_region, .'+track_key[1]+'_province, .'+track_key[1]+'_town_city, .'+track_key[1]+'awe_baluarte_tagging, .'+track_key[1]+'awe_priority_tag, .'+track_key[1]+'awe_town_priority, .'+track_key[1]+'_latitude, .'+track_key[1]+'_longtitude, .'+track_key[1]+'_vendor, .'+track_key[1]+'_program_tag, .'+track_key[1]+'_tagging, .'+track_key[1]+'_planned_tech, .'+track_key[1]+'_site_type').val('');
    }
	}

  let id = $(this).closest("tr").find('td:eq(0)').attr('data-'+track_key[1]+'-id');
  let serial_number = $(this).closest("tr").closest("tr").find('td:eq(2)').text();
  let site_name = $(this).closest("tr").closest("tr").find('td:eq(3)').text();
  let region = $(this).closest("tr").find('td:eq(4)').text();
  let province = $(this).closest("tr").find('td:eq(5)').text();
  let town_city = $(this).closest("tr").find('td:eq(6)').text();
  let priority_town = $(this).closest("tr").find('td:eq(7)').text();
  let latitude = $(this).closest("tr").find('td:eq(8)').text();
  let longtitude = $(this).closest("tr").find('td:eq(9)').text();
  let vendor = $(this).closest("tr").find('td:eq(10)').text();
  let program_tag = $(this).closest("tr").find('td:eq(11)').text();
  let planned_tech = $(this).closest("tr").find('td:eq(12)').text();
  let tagging = $(this).closest("tr").find('td:eq(13)').text();
  let site_type = $(this).closest("tr").find('td:eq(14)').text();

	if ($(this).is(":checked")) {
    aweData.push({
              id:id,
              serial_number: serial_number,
              site_name: site_name,
              region: region,
              province: province,
              town_city: town_city,
              priority_town: priority_town,
              latitude: latitude,
              longtitude: longtitude,
              vendor: vendor,
              program_tag: program_tag,
              planned_tech: planned_tech,
              tagging: tagging,
              site_type: site_type,
            });
    awe_id.push(id);
    $('#'+track_key[1]+'AddTrack, #'+track_key[1]+'ImportTrack').addClass('d-none');
    $('#'+track_key[1]+'EditTrack, #'+track_key[1]+'DelTrack, #'+track_key[1]+'ViewTrack').removeClass('d-none');
	} else {
    _.remove(aweData, function(e) {
      return e.id === id
    });
    awe_id.pop(id);
	}
});

$(document).on('click', '.append_'+track_key[1]+'_track', function(e){
	e.preventDefault();
	let length = $('#'+track_key[1]+'_table tr').length;
	setAweToAppend(length);
});

function setAweToAppend(length) {
	let html = "<tr class="+track_key[1]+"_rows-"+length+">" +
  	"<td><input type=\"text\" name=\"awe_serial_number[]\" class=\"form-control awe_serial_number\" id=\"awe_serial_number\" placeholder=\"Serial Number\" required></td>" +
		"<td><textarea name=\"awe_site_name[]\" id=\"awe_site_name\" cols=\"30\" rows=\"1\" class=\"form-control awe_site_name\" placeholder=\"Site Name\" required></textarea></td>" +
		"<td><input type=\"text\" name=\"awe_region[]\" class=\"form-control awe_region\" id=\"awe_region\" placeholder=\"Region\" required></td>" +
		"<td><input type=\"text\" name=\"awe_province[]\" class=\"form-control awe_province\" id=\"awe_province\" placeholder=\"Province\" required></td>" +
    "<td><input type=\"text\" name=\"awe_town_city[]\" class=\"form-control awe_town_city\" id=\"awe_town_city\" placeholder=\"Town City\" required></td>" +
    "<td><input type=\"text\" name=\"awe_baluarte_tagging[]\" class=\"form-control awe_baluarte_tagging\" id=\"awe_baluarte_tagging\" placeholder=\"Baluarte Tagging\" required></td>" +
    // "<td><input type=\"text\" name=\"awe_priority_tag[]\" class=\"form-control awe_priority_tag\" id=\"awe_priority_tag\" placeholder=\"Priority Tag\" required></td>" +
    // "<td><input type=\"text\" name=\"awe_town_priority[]\" class=\"form-control awe_town_priority\" id=\"awe_town_priority\" placeholder=\"Town Priority\" required></td>" +
    "<td><input type=\"text\" name=\"awe_latitude[]\" class=\"form-control awe_latitude\" id=\"awe_latitude\" placeholder=\"Latitude\" required></td>" +
    "<td><input type=\"text\" name=\"awe_longtitude[]\" class=\"form-control awe_longtitude\" id=\"awe_longtitude\" placeholder=\"Longtitude\" required></td>" +
		"<td><input type=\"text\" name=\"awe_vendor[]\" class=\"form-control awe_vendor\" id=\"awe_vendor\" placeholder=\"Vendor\" required></td>" +
		"<td><input type=\"text\" name=\"awe_program_tag[]\" class=\"form-control awe_program_tag\" id=\"awe_program_tag\" placeholder=\"Program Tag\" required></td>" +
		"<td><input type=\"text\" name=\"awe_planned_tech[]\" class=\"form-control awe_planned_tech\" id=\"awe_planned_tech\" placeholder=\"Planned Tech\" required></td>" +
    "<td><input type=\"text\" name=\"awe_tagging[]\" class=\"form-control awe_tagging\" id=\"awe_tagging\" placeholder=\"Tagging\" required></td>" +
    "<td><input type=\"text\" name=\"awe_site_type[]\" class=\"form-control awe_site_type\" id=\"awe_site_type\" placeholder=\"Site Type\" required></td>" +
    "<td class='text-center'><button type=\"button\" class=\"btn btn-danger btn-sm mg-left-3 remove_"+track_key[1]+"_track\"><i class=\"fas fa-minus\"></i></button></td>" +
    "</tr>";
	  $('#'+track_key[1]+'_track_tables').append(html);
}

$(document).on('click', '.remove_'+track_key[1]+'_track', function(e){
	e.preventDefault();
	$(this).closest('tr').remove();
});

$(document).on('submit', '#edd_'+track_key[1]+'_Track', function(e){
	e.preventDefault();

	let values = $('input[name="'+track_key[1]+'_site_name[]"]').map(function() {
		return this.value;
	}).toArray();

	let checkExists = !values.every(function(v,i) {
		return values.indexOf(v) == i;
	});

	if (checkExists) {
		SWAL('error', 'Invalid!', 'Duplicate subject name.');
	} else {
		$.ajax({
			url: url + 'save_awe_track',
			dataType: 'json',
			data: $(this).serialize(),
			type: 'POST',
			success: function(data) {
				SWAL(data.type, data.title,  data.msg);
				if (data.type === 'success') {
          $('#chk_awe_item').prop('checked', false);
					get_AWE_Track();
					SWAL(data.type, data.title,  data.msg);
					$('.selected').removeClass('selected');
					$('#'+track_key[1]+'AddTrack, #'+track_key[1]+'ImportTrack').removeClass('d-none');
					$('#'+track_key[1]+'EditTrack, #'+track_key[1]+'DelTrack, #'+track_key[1]+'ViewTrack').addClass('d-none');
					$('#AweTitleTrack').text('Add AWE Track');
					$('#add_'+track_key[1]+'s_modal').modal('hide');
          clearAwe();
				}
			},
			error: function(xhr, status, error) {
				errorSwal();
			}
		});
	}
});

$(document).on('click', '#'+track_key[1]+'ViewTrack', function() {
  let html = '';
  let ctr = 1;
  for (i in aweData) {
    let col = (ctr <= 1) ? 4 : 1;
		html += "<tr>" +
		"<td><small>"+ctr+".</small></td>" +
    "<td><small>"+aweData[i].serial_number+"</small></td>" +
		"<td><small>"+aweData[i].site_name+"</small></td>" +
		"<td><small>"+aweData[i].province+"</small></td>" +
		"<td><small>"+aweData[i].region+"</small></td>" +
		"<td><small>"+aweData[i].town_city+"</small></td>" +
		"<td><small>"+aweData[i].latitude+"</small></td>" +
		"<td><small>"+aweData[i].longtitude+"</small></td>" +
    "<td><small>"+aweData[i].vendor+"</small></td>" +
    "<td><small>"+aweData[i].program_tag+"</small></td>" +
    "<td><small>"+aweData[i].planned_tech+"</small></td>" +
		"<td><small>"+aweData[i].tagging+"</small></td>" +
		"<td><small>"+aweData[i].site_type+"</small></td>" +
		"</tr>";
    ctr++;
  }

  $('#view_'+track_key[1]+'_build').html(html);
  $('#viewAweModal').modal('show');
});

function clearAwe() {
	$('#progressBarAwe').width(0);
	$('#submitAweButton, #clear_data_file').prop("disabled", false);
	$('#'+track_key[1]+'_file').val('');
	$('#chk_'+track_key[1]+'_item').prop('checked', false);
  let ctr = 1;
  for (i in nomData) {
    $('.'+track_key[1]+'_rows-'+ctr).remove();
		ctr++;
  }
  $('.'+track_key[1]+'_serial_number, .'+track_key[1]+'_site_name, .'+track_key[1]+'_region, .'+track_key[1]+'_province, .'+track_key[1]+'_town_city, .'+track_key[1]+'_baluarte_tagging, .'+track_key[1]+'_priority_tag, .'+track_key[1]+'_town_priority, .'+track_key[1]+'_latitude, .'+track_key[1]+'_longtitude, .'+track_key[1]+'_vendor, .'+track_key[1]+'_program_tag, .'+track_key[1]+'_tagging, .'+track_key[1]+'_planned_tech, .'+track_key[1]+'_site_type').val('');
  aweData = [];
}
