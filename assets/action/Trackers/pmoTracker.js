let pmoData = [];
let pmo_id = [];
let del_pmo_status = false;
let charPmo = ['Program', 'Solution', 'Scope', 'Lead_Vendor', 'Budget_Tagging', 'Clutter_Type'];
$(function () {
  let html = '';
  for (i in charPmo) {
    html += '<div class="row border-top">' +
        				'<div class="col-sm-6 col-md-6 col-lg-6 border-right">' +
        					'<div class="chart-container" style="position: relative;">' +
        						'<canvas id="line'+charPmo[i]+'" class="mg-top-10"></canvas>' +
        				'	</div>' +
        				'</div>' +
                '<div class="col-sm-6 col-md-6 col-lg-6">' +
                  '<div class="chart-container" style="position: relative;">' +
                      '<canvas id="pie'+charPmo[i]+'" class="mg-top-10"></canvas>' +
                  '</div>' +
                '</div>' +
              '</div>';
  }

  $('#chartsPMO').html(html);
});

$(document).on('keyup', '#'+track_key[2]+'Search', function(e) {
	e.preventDefault();
	get_PMO_Track(0, row_page = 10, $(this).val());
});

$(document).on('change', '#'+track_key[2]+'_sort', function() {
		get_PMO_Track(0, $(this).val());
});

$(document).on('click', '#'+track_key[2]+'_pagination a', function(e) {
	e.preventDefault();
	let row_num = $(this).attr('data-ci-pagination-page');
  get_PMO_Track(row_num);
});

function get_PMO_Track(row_num = 0, row_page = 10, search = '') {
	$.ajax({
		url: url + 'FileTrack/get_PMO_TRACK/'+row_num,
		dataType: 'json',
		data: {row_num: row_num, row_page : row_page, search: search},
		type: 'GET',
		beforeSend() {
      let html = '<div class="content">' +
        '<h5 class="text-center text-white">' +
        '<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>' +
        '<h4 class="text-dark text-center">Loading</h4>' +
        '</h5>' +
        '</div>';
      $('#'+track_key[2]+'_datas').html(html);
    },
		success: function (data) {
				setPMO(data);
		},
		error: function (xhr, status, error) {
			errorSwal();
		}
	});
}

function setPMO(data) {
  if (data.details.length > 0) {
    $('.emptyPmoChartTrack, .emptyPmoTrack').addClass('d-none');
    $('.pmoChart, #pmoSearch, .pmoListsTrack').removeClass('d-none');
    setBudget_Tagging(data.Budget_Tagging, data.total);
    setClutter_Type(data.Clutter_Type, data.total);
    setLead_Vendor(data.Lead_Vendor, data.total);
    setProgram(data.Program, data.total);
    setScope(data.Scope, data.total);
    setSolution(data.Solution, data.total);
    setTOPmoDetails(data.details, data.pagination, data.total);
  } else {
    $('.emptyPmoChartTrack, .emptyPmoTrack').removeClass('d-none');
    $('.pmoChart, .pmoListsTrack').addClass('d-none');
  }
}

function setTOPmoDetails(data, pagination, total) {
  let html = '';
  let count = 1;
  let ctr = 1;
  if (data.length > 0) {
    for (i in data) {
      html += '<tr class="pmo_id-'+ctr+'">' +
        '<td style="width:5% !important;" data-pmo-id='+data[i].id+'>' +
          '<div class="custom-control pad-left-rem custom-checkbox chk-pmo-'+i+'">' +
            '<input type="checkbox" name="table2_lists" class="custom-control-input check-tbl-pmo-item" id="check-pmo-item-'+i+'">' +
            '<label class="custom-control-label" for="check-pmo-item-'+i+'"></label>' +
          '</div>' +
        '</td>' +
				'<td class="text-center" style="width:7% !important;">'+data[i].id+'.</td>' +
        	'<td class="text-truncate" data-toggle="tooltip" data-placement="top" title='+data[i].Serial_Number+'>'+data[i].Serial_Number+'</td>' +
        '<td class="text-truncate" data-toggle="tooltip" data-placement="top" title='+data[i].SITE_NAME+'>'+data[i].SITE_NAME+'</td>' +
				'<td class="text-center text-truncate">'+data[i].REGION+'</td>' +
				'<td class="text-center text-truncate">'+data[i].Budget_Tagging+'</td>' +
        '<td class="text-center text-truncate" data-toggle="tooltip" data-placement="top" title='+data[i].Program+'>'+data[i].Program+'</td>' +
        '<td class="text-center text-truncate">'+data[i].Project_Phase+'</td>' +
        '<td class="text-center text-truncate">'+data[i].Lead_Vendor+'</td>' +
        '<td class="text-center text-truncate" data-toggle="tooltip" data-placement="top" title='+data[i].Solution+'>'+data[i].Solution+'</td>' +
        '<td class="text-center text-truncate">'+data[i].Coverage+'</td>' +
        '<td class="text-center text-truncate">'+data[i].Scope+'</td>' +
        '<td class="text-center text-truncate">'+data[i].Wireless_Plan+'</td>' +
        '<td class="text-center text-truncate">'+data[i].No_of_Sectors+'</td>';
			'<tr>';
      ctr++;
    }
    $('#emptyPmoTrack').find('form-control').val('');
    $('#pmo_datas').html(html);
    $('#pmo_pagination').html(pagination);
    $('#' + track_key[2] + 'EditTrack, #' + track_key[2] + 'DelTrack,  #' + track_key[2] + 'ViewTrack').addClass('d-none');
    $('.' + track_key[2] + 'ListsTrack, #' + track_key[2] + 'AddTrack, #' + track_key[2] + 'ImportTrack').removeClass('d-none');
    $('.emptypmoTrack').addClass('d-none');
    $('#chk_' + track_key[2] + '_item').prop('checked', false);
  } else {
  $('.'+track_key[2]+'ListsTrack, #'+track_key[2]+'AddTrack, #'+track_key[2]+'ImportTrack').addClass('d-none');
    $('#chk_'+track_key[2]+'_item').prop('disabled', true);
  }
  $('[data-toggle="tooltip"]').tooltip();
  $('.total_pmo_details').text(total);
}

function setBudget_Tagging(data, total = 0) {
  let canvasPiePMOBT = document.getElementById("pieBudget_Tagging");
  let ctxPiePMOBT = canvasPiePMOBT.getContext('2d');
  let canvasLinePMOBT = document.getElementById("lineBudget_Tagging");
  let ctxLinePMOBT = canvasLinePMOBT.getContext('2d');

  let Full_Build   = data.Full_Build;
  let tf           = data.TF;
  let total_per = getWholePercent(Full_Build, total) + getWholePercent(tf, total);

  let dataPMO = {
		labels: [Full_Build + ":Full Build", tf + ":T & F"],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#00897b', '#263238'],
				data: [getWholePercent(Full_Build, total), getWholePercent(tf, total)],
			}
		]
	};

  optionsPie.elements = centerText(total_per + '%');
  optionsPie.title = setTitle("Pie Chart - Budget");
  optionsBar.title = setTitle("Bar Graph - Budget");


  setChartPiePMOBT(dataPMO, optionsPie, ctxPiePMOBT, total_per, 'doughnut');
  setChartBarPMOBT(dataPMO, optionsBar, ctxLinePMOBT, total_per, 'horizontalBar');
}

function setChartPiePMOBT(data = "", option = "", ctx = "", total_per = "", type = "") {
	if(window.pieBT != undefined) {
		window.pieBT.destroy();
	}

	window.pieBT = new Chart(ctx, {
		type: type,
		data: data,
		options: option,
	});
}

function setChartBarPMOBT(data = "", option = "", ctx = "", total_per = "", type = "") {
	if(window.barBT != undefined) {
		window.barBT.destroy();
	}

	window.barBT = new Chart(ctx, {
		type: type,
		data: data,
		options: option,
	});
}

function setClutter_Type(data, total = 0) {
  let canvasPieCT = document.getElementById("pieClutter_Type");
  let ctxPieCT = canvasPieCT.getContext('2d');
  let canvasLineCT = document.getElementById("lineClutter_Type");
  let ctxLineCT = canvasLineCT.getContext('2d');

  let Dense_Urban   = data.Dense_Urban;
  let Urban         = data.Rural;
  let na            = data.NA
  let total_per = getWholePercent(Dense_Urban, total) + getWholePercent(Urban, total) + getWholePercent(na, total);

  let dataPMO = {
		labels: [Dense_Urban + ":Dense Urban", Urban + ":Urban", na + ":NA"],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#ff7315', '#464159', "#232020"],
				data: [getWholePercent(Dense_Urban, total), getWholePercent(Urban, total), getWholePercent(na, total)],
			}
		]
	};

  optionsPie.elements = centerText(total_per + '%');
  optionsPie.title = setTitle("Pie Chart - Clutter Type");
  optionsBar.title = setTitle("Bar Graph - Clutter Type");

  setChartPieCT(dataPMO, optionsPie, canvasPieCT, total_per, 'doughnut');
  setChartBarCT(dataPMO, optionsBar, canvasLineCT, total_per, 'horizontalBar');
}

function setChartPieCT(data = "", option = "", ctx = "", total_per = "", type = "") {
	if(window.pieCT != undefined) {
		window.pieCT.destroy();
	}

	window.pieBT = new Chart(ctx, {
		type: type,
		data: data,
		options: option,
	});
}

function setChartBarCT(data = "", option = "", ctx = "", total_per = "", type = "") {
	if(window.barCT != undefined) {
		window.barCT.destroy();
	}

	window.barCT = new Chart(ctx, {
		type: type,
		data: data,
		options: option,
	});
}

function setLead_Vendor(data, total = 0) {
  let canvasPieCT = document.getElementById("pieLead_Vendor");
  let ctxPieCT = canvasPieCT.getContext('2d');
  let canvasLineCT = document.getElementById("lineLead_Vendor");
  let ctxLineCT = canvasLineCT.getContext('2d');

  let Guevent = data.Guevent
  let ht   = data.HT;
  let Nokia = data.Nokia;

  let total_per = getWholePercent(Guevent, total) + getWholePercent(ht, total) + getWholePercent(Nokia, total);

  let dataPMO = {
    labels: [Guevent + ":Guevent", ht + ":Hupmoi", Nokia + ":Nokia"],
    datasets: [
      {
        label: "",
        fill: true,
        backgroundColor: ['#3a3535', '#d1274b', "#b1bd5d"],
        data: [getWholePercent(Guevent, total), getWholePercent(ht, total), getWholePercent(Nokia, total)],
      }
    ]
  };

  optionsPie.elements = centerText(total_per + '%');
  optionsPie.title = setTitle("Pie Chart - Lead Vendor");
  optionsBar.title = setTitle("Bar Graph - Lead Vendor");

  setChartPieLV(dataPMO, optionsPie, canvasPieCT, total_per, 'doughnut');
  setChartBarLV(dataPMO, optionsBar, canvasLineCT, total_per, 'horizontalBar');
}

function setChartPieLV(data = "", option = "", ctx = "", total_per = "", type = "") {
	if(window.pieLV != undefined) {
		window.pieLV.destroy();
	}

	window.pieLV = new Chart(ctx, {
		type: type,
		data: data,
		options: option,
	});
}

function setChartBarLV(data = "", option = "", ctx = "", total_per = "", type = "") {
	if(window.barLV != undefined) {
		window.barLV.destroy();
	}

	window.barLV = new Chart(ctx, {
		type: type,
		data: data,
		options: option,
	});
}

function setProgram(data, total = 0) {
  let canvasPieProgram = document.getElementById("pieProgram");
  let ctxPieProgram = canvasPieProgram.getContext('2d');
  let canvasLineProgram = document.getElementById("lineProgram");
  let ctxLineProgram = canvasLineProgram.getContext('2d');

  let Masterplan    = data.Masterplan;
  let npd           = data.NPD;
  let NTG_Driven    = data.NTG_Driven;
  let Project_Fly_1 = data.Project_Fly_1;
  let capacity_2019 = data.capacity_2019;
  let total_per     = getWholePercent(Masterplan, total) +
                      getWholePercent(npd, total) +
                      getWholePercent(NTG_Driven, total) +
                      getWholePercent(Project_Fly_1, total) +
                      getWholePercent(capacity_2019, total);

  let dataPMO = {
		labels: [Masterplan + ":Masterplan",
            npd + ":NPD",
            NTG_Driven + ":NTG Driven",
            Project_Fly_1 + ":Project Fly 1",
            capacity_2019 + ":capacity_2019"],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#110133', '#bd574e', '#ffdc34', '#0c9463', '#400082'],
				data: [ getWholePercent(Masterplan, total),
                getWholePercent(npd, total),
                getWholePercent(NTG_Driven, total),
                getWholePercent(Project_Fly_1, total),
                getWholePercent(capacity_2019, total)]
			}
		]
	};

  optionsPie.elements = centerText(total_per + '%');
  optionsPie.title = setTitle("Pie Chart - Program");
  optionsBar.title = setTitle("Bar Graph - Program");

  setChartPieP(dataPMO, optionsPie, ctxPieProgram, total_per, 'doughnut');
  setChartBarP(dataPMO, optionsBar, ctxLineProgram, total_per, 'horizontalBar');
}

function setChartPieP(data = "", option = "", ctx = "", total_per = "", type = "") {
	if(window.pieP != undefined) {
		window.pieP.destroy();
	}

	window.pieP = new Chart(ctx, {
		type: type,
		data: data,
		options: option,
	});
}

function setChartBarP(data = "", option = "", ctx = "", total_per = "", type = "") {
	if(window.barP != undefined) {
		window.barP.destroy();
	}

	window.barP = new Chart(ctx, {
		type: type,
		data: data,
		options: option,
	});
}

function setScope(data, total = 0) {
  let canvasPieScope = document.getElementById("pieScope");
  let ctxPieScope = canvasPieScope.getContext('2d');
  let canvasLineScope = document.getElementById("lineScope");
  let ctxLineScope = canvasLineScope.getContext('2d');

  let sec1   = data.sec1;
  let sec2   = data.sec2;
  let sec3   = data.sec3;
  let sec4   = data.sec4;

  let total_per = getWholePercent(sec1, total) + getWholePercent(sec2, total) + getWholePercent(sec3, total) + getWholePercent(sec4, total);

  let dataPMO = {
    labels: [sec1 + ":Sector 1", sec2 + ":Sector 2", sec3 + ':Sector 3', sec4 + ':Sector4'],
    datasets: [
      {
        label: "",
        fill: true,
        backgroundColor: ['#46b5d1', '#515585' , '#32407b', '#151965'],
        data: [getWholePercent(sec1, total),
               getWholePercent(sec2, total),
               getWholePercent(sec3, total),
               getWholePercent(sec4, total)],
      }
    ]
  };

  optionsPie.elements = centerText(total_per + '%');
  optionsPie.title = setTitle("Pie Chart - Scope");
  optionsBar.title = setTitle("Bar Graph - Scope");

  setChartPieSC(dataPMO, optionsPie, ctxPieScope, total_per, 'doughnut');
  setChartBarSC(dataPMO, optionsBar, ctxLineScope, total_per, 'horizontalBar');
}

function setChartPieSC(data = "", option = "", ctx = "", total_per = "", type = "") {
	if(window.pieSC != undefined) {
		window.pieSC.destroy();
	}

	window.pieSC = new Chart(ctx, {
		type: type,
		data: data,
		options: option,
	});
}

function setChartBarSC(data = "", option = "", ctx = "", total_per = "", type = "") {
	if(window.barSC != undefined) {
		window.barSC.destroy();
	}

	window.barSC = new Chart(ctx, {
		type: type,
		data: data,
		options: option,
	});
}

function setSolution(data, total = 0) {
  let canvasPieSolution = document.getElementById("pieSolution");
  let ctxPieSolution = canvasPieSolution.getContext('2d');
  let canvasLineSolution = document.getElementById("lineSolution");
  let ctxLineSolution = canvasLineSolution.getContext('2d');

  let Easy_Macro   = data.Easy_Macro;
  let macro        = data.MACRO;
  let Micro_RRH    = data.Micro_RRH;
  let Mini_Macro   = data.Mini_Macro;

  let total_per = getWholePercent(Easy_Macro, total) +
                  getWholePercent(macro, total) +
                  getWholePercent(Micro_RRH, total) +
                  getWholePercent(Mini_Macro, total);

  let dataPMO = {
		labels: [Easy_Macro + ":Easy Macro", macro + ":Macro", Micro_RRH + "Micro RRH", Mini_Macro + "Mini Macro"],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#fcc169', '#9b45e4', '#e13a9d', '#fa697c'],
				data: [ getWholePercent(Easy_Macro, total),
                getWholePercent(macro, total),
                getWholePercent(Micro_RRH, total),
                getWholePercent(Mini_Macro, total)],
			}
		]
	};

  optionsPie.elements = centerText(total_per + '%');
  optionsPie.title = setTitle("Pie Chart - Solution");
  optionsBar.title = setTitle("Bar Graph - Solution");

  setChartPieSol(dataPMO, optionsPie, ctxPieSolution, total_per, 'doughnut');
  setChartBarSol(dataPMO, optionsBar, ctxLineSolution, total_per, 'horizontalBar');
}

function setChartPieSol(data = "", option = "", ctx = "", total_per = "", type = "") {
	if(window.pieSol != undefined) {
		window.pieSol.destroy();
	}

	window.pieSol = new Chart(ctx, {
		type: type,
		data: data,
		options: option,
	});
}

function setChartBarSol(data = "", option = "", ctx = "", total_per = "", type = "") {
	if(window.barSol != undefined) {
		window.barSol.destroy();
	}

	window.barSol = new Chart(ctx, {
		type: type,
		data: data,
		options: option,
	});
}

$(document).on('click', '#'+track_key[2]+'ImportTrack', function() {
  $('#import_'+track_key[2]+'_modal').modal('show');
});

$(document).on('change', '#chk_'+track_key[2]+'_item', function () {
  pmoData = [];
  pmo_id = [];
	$('.check-tbl-'+track_key[2]+'-item').prop('checked', !!$(this).is(":checked"));
	let total_length = $('#'+track_key[2]+'_datas tr').length;
	if ($(this).is(":checked")) {
		for (i = 1; i <= total_length; i++) {
      let row = $('.'+track_key[2]+'_id-'+i);
      let id = row.closest("tr").find('td:eq(0)').attr('data-'+track_key[2]+'-id');
      let serial_number = row.closest("tr").closest("tr").find('td:eq(2)').text();
      let site_name = row.closest("tr").closest("tr").find('td:eq(3)').text();
      let region = row.closest("tr").find('td:eq(4)').text();
      let budget_tagging = row.closest("tr").find('td:eq(5)').text();
      let program = row.closest("tr").find('td:eq(6)').text();
      let project_phase = row.closest("tr").find('td:eq(7)').text();
      let lead_vendor = row.closest("tr").find('td:eq(8)').text();
      let solution = row.closest("tr").find('td:eq(9)').text();
      let coverage = row.closest("tr").find('td:eq(10)').text();
      let scope = row.closest("tr").find('td:eq(11)').text();
      let wireless_plan = row.closest("tr").find('td:eq(12)').text();
      let sectors = row.closest("tr").find('td:eq(13)').text();

      pmoData.push({id:id,
                serial_number: serial_number, site_name: site_name,
                region: region, budget_tagging: budget_tagging,
                program: program, project_phase: project_phase,
                lead_vendor: lead_vendor, solution: solution,
                coverage: coverage, scope: scope,
                wireless_plan: wireless_plan, sectors: sectors,
              });
      pmo_id.push(id);
		}
    $('#'+track_key[2]+'AddTrack, #'+track_key[2]+'ImportTrack').addClass('d-none');
    $('#'+track_key[2]+'EditTrack, #'+track_key[2]+'DelTrack,  #'+track_key[2]+'ViewTrack').removeClass('d-none');
		del_nom_status = true;
	} else {
    $('#'+track_key[2]+'AddTrack, #'+track_key[2]+'ImportTrack').removeClass('d-none');
    $('#'+track_key[2]+'EditTrack, #'+track_key[2]+'DelTrack, #'+track_key[2]+'ViewTrack').addClass('d-none');
    $('.'+track_key[2]+'_serial_number, .'+track_key[2]+'_site_name, .'+track_key[2]+'_region,  .'+track_key[2]+'_budget_tagging, .'+track_key[2]+'_program, .'+track_key[2]+'_project_phase, .'+track_key[2]+'_lead_vendor, .'+track_key[2]+'_solution, .'+track_key[2]+'_coverage, .'+track_key[2]+'_scope, .'+track_key[2]+'_wireless_plan, .'+track_key[2]+'_sectors').val('');
		del_pmo_status = false;
	}
});

$(document).on('click', '.check-tbl-'+track_key[2]+'-item', function () {
	let chk = $('#chk_'+track_key[2]+'_item');
	let total_checked = $('#'+track_key[2]+'_datas').find('.check-tbl-'+track_key[2]+'-item:checked').length;
	let total_length = $('#'+track_key[2]+'_datas tr').length;
	chk.prop('checked', total_checked === total_length);

	if (total_checked === total_length) {
    $('#'+track_key[2]+'AddTrack, #'+track_key[2]+'ImportTrack').addClass('d-none');
    $('#'+track_key[2]+'EditTrack, #'+track_key[2]+'DelTrack, #'+track_key[2]+'ViewTrack').removeClass('d-none');
	} else {
    if ( total_checked === 0) {
      $('#'+track_key[2]+'AddTrack, #'+track_key[2]+'ImportTrack').removeClass('d-none');
      $('#'+track_key[2]+'EditTrack, #'+track_key[2]+'DelTrack, #'+track_key[2]+'ViewTrack').addClass('d-none');
      $('.'+track_key[2]+'serial_number, .'+track_key[2]+'_site_name, .'+track_key[2]+'_region, .'+track_key[2]+'_budget_tagging, .'+track_key[2]+'_program, .'+track_key[2]+'_project_phase, .'+track_key[2]+'_lead_vendor, .'+track_key[2]+'_solution, .'+track_key[2]+'_coverage, .'+track_key[2]+'_scope, .'+track_key[2]+'_wireless_plan, .'+track_key[2]+'_sectors').val('');
    }
	}

  let id = $(this).closest("tr").find('td:eq(0)').attr('data-'+track_key[2]+'-id');
  let serial_number = $(this).closest("tr").closest("tr").find('td:eq(2)').text();
  let site_name = $(this).closest("tr").closest("tr").find('td:eq(3)').text();
  let region = $(this).closest("tr").find('td:eq(4)').text();
  let budget_tagging = $(this).closest("tr").find('td:eq(5)').text();
  let program = $(this).closest("tr").find('td:eq(6)').text();
  let project_phase = $(this).closest("tr").find('td:eq(7)').text();
  let lead_vendor = $(this).closest("tr").find('td:eq(8)').text();
  let solution = $(this).closest("tr").find('td:eq(9)').text();
  let coverage = $(this).closest("tr").find('td:eq(10)').text();
  let scope = $(this).closest("tr").find('td:eq(11)').text();
  let wireless_plan = $(this).closest("tr").find('td:eq(12)').text();
  let sectors = $(this).closest("tr").find('td:eq(13)').text();

	if ($(this).is(":checked")) {
    pmoData.push({id:id,
              serial_number: serial_number, site_name: site_name,
              region: region, budget_tagging: budget_tagging,
              program: program, project_phase: project_phase,
              lead_vendor: lead_vendor, solution: solution,
              coverage: coverage, scope: scope,
              wireless_plan: wireless_plan, sectors: sectors,
            });
    pmo_id.push(id);
    $('#'+track_key[2]+'AddTrack, #'+track_key[2]+'ImportTrack').addClass('d-none');
    $('#'+track_key[2]+'EditTrack, #'+track_key[2]+'DelTrack, #'+track_key[2]+'ViewTrack').removeClass('d-none');
	} else {
    _.remove(pmoData, function(e) {
      return e.id === id
    });
    pmo_id.pop(id);
	}
});

$(document).on('click', function() {
	$('#file-pmo-upload').ajaxForm({
		url:  url + 'pmo_upload',
		contentType: false,
		cache:false,
		processData: false,
		beforeSubmit: function () {
			$('#submitpmoButton, #clear_data_file').prop("disabled", true);
			$('#progressPmoId').fadeIn('slow');
			$("#progressPmoId").css("display", "block");
			var percentValue = '0%';

			$('#progressBarPmo').width(percentValue);
			$('#percentPmo').html(percentValue);
		},
		uploadProgress: function (event, position, total, percentComplete) {
			var percentValue = percentComplete + '%';
			$("#progressBarPmo").animate({
				width: '' + percentValue + ''
			}, {
				duration: 5000,
				easing: "linear",
				step: function (x) {
					percentText = Math.round(x * 100 / percentComplete);
					$("#percentPmo").text(percentText + "%");
					if(percentText == "100") {
						setTimeout(function(){
							SWAL('success', 'Success!', 'Successfully uploaded.');
              $('#import_pmo_modal').modal('hide');
              clearPMO();
						}, 1000);
						setTimeout(function(){
							$('#progressPmoId').fadeOut('slow');
						}, 3000);
					}
				}
			});
		},
		error: function (response, status, e) {
			SWAL('error', 'Opps!', 'Something went wrong.');
			setTimeout(function(){
				$('#progressPmoId').fadeOut('slow');
			}, 3000);
		},
		complete: function (xhr) {
			if (xhr.responseText && xhr.responseText != "error")
			{
				let json = JSON.parse(xhr.responseText);

				if (json.type == 'error') {
					SWAL(json.type, json.title, json.msg);
					clearPMO();
					$('#progressPmoId').fadeOut('fast');
				} else {
				  get_PMO_Track();
				}
			}
			else{
				SWAL('error', 'Opps!', 'Problem in uploading file.');
				clearPMO();
				$("#progressBarPmo").stop();
				setTimeout(function(){
					$('#progressPmoId').fadeOut('slow');
				}, 3000);
			}
		}
	});
});

$(document).on('click', '#'+track_key[2]+'AddTrack', function() {
  $('#add_'+track_key[2]+'_modal').modal('show');
});

$(document).on('click', '.append_'+track_key[2]+'_track', function(e){
	e.preventDefault();
	let length = $('#'+track_key[2]+'_table tr').length;
	setPmoToAppend(length);
});

function setPmoToAppend(length) {
	let html = "<tr class="+track_key[2]+"_rows-"+length+">" +
  	"<td><input type=\"text\" name=\"pmo_serial_number[]\" class=\"form-control pmo_serial_number\" id=\"pmo_serial_number\" placeholder=\"Serial Number\" required></td>" +
		"<td><textarea name=\"pmo_site_name[]\" id=\"pmo_site_name\" cols=\"30\" rows=\"1\" class=\"form-control pmo_site_name\" placeholder=\"Site Name\" required></textarea></td>" +
		"<td><input type=\"text\" name=\"pmo_region[]\" class=\"form-control pmo_region\" id=\"pmo_region\" placeholder=\"Region\" required></td>" +
		"<td><input type=\"text\" name=\"pmo_budget_tagging[]\" class=\"form-control pmo_budget_tagging\" id=\"pmo_budget_tagging\" placeholder=\"Budget Tagging\" required></td>" +
    "<td><input type=\"text\" name=\"pmo_program[]\" class=\"form-control pmo_program\" id=\"pmo_program\" placeholder=\"Program\" required></td>" +
    "<td><input type=\"text\" name=\"pmo_project_phase[]\" class=\"form-control pmo_project_phase\" id=\"pmo_project_phase\" placeholder=\"Project Phase\" required></td>" +
    "<td><input type=\"text\" name=\"pmo_lead_vendor[]\" class=\"form-control pmo_lead_vendor\" id=\"pmo_lead_vendor\" placeholder=\"Lead Vendor\" required></td>" +
    "<td><input type=\"text\" name=\"pmo_solution[]\" class=\"form-control pmo_solution\" id=\"pmo_solution\" placeholder=\"Solution\" required></td>" +
		"<td><input type=\"text\" name=\"pmo_coverage[]\" class=\"form-control pmo_coverage\" id=\"pmo_coverage\" placeholder=\"Coverage\" required></td>" +
		"<td><input type=\"text\" name=\"pmo_scope[]\" class=\"form-control pmo_scope\" id=\"pmo_scope\" placeholder=\"Scope\" required></td>" +
		"<td><input type=\"text\" name=\"pmo_wireless_plan[]\" class=\"form-control pmo_wireless_plan\" id=\"pmo_wireless_plan\" placeholder=\"Wireless Plan\" required></td>" +
    "<td><input type=\"text\" name=\"pmo_sectors[]\" class=\"form-control pmo_sectors\" id=\"pmo_sectors\" placeholder=\"Sectors\" required></td>" +
    "<td class='text-center'><button type=\"button\" class=\"btn btn-danger btn-sm mg-left-3 remove_"+track_key[2]+"_track\"><i class=\"fas fa-minus\"></i></button></td>" +
    "</tr>";
	  $('#'+track_key[2]+'_track_tables').append(html);
}

$(document).on('click', '.remove_'+track_key[2]+'_track', function(e){
	e.preventDefault();
	$(this).closest('tr').remove();
});

$(document).on('submit', '#edd_'+track_key[2]+'_Track', function(e){
	e.preventDefault();

	let values = $('input[name="'+track_key[2]+'_site_name[]"]').map(function() {
		return this.value;
	}).toArray();

	let checkExists = !values.every(function(v,i) {
		return values.indexOf(v) == i;
	});

	if (checkExists) {
		SWAL('error', 'Invalid!', 'Duplicate subject name.');
	} else {
		$.ajax({
			url: url + 'save_pmo_track',
			dataType: 'json',
			data: $(this).serialize(),
			type: 'POST',
			success: function(data) {
				SWAL(data.type, data.title,  data.msg);
				if (data.type === 'success') {
          $('#chk_pmo_item').prop('checked', false);
					get_PMO_Track();
					SWAL(data.type, data.title,  data.msg);
					$('.selected').removeClass('selected');
					$('#'+track_key[2]+'AddTrack, #'+track_key[2]+'ImportTrack').removeClass('d-none');
					$('#'+track_key[2]+'EditTrack, #'+track_key[2]+'DelTrack, #'+track_key[2]+'ViewTrack').addClass('d-none');
					$('#pmoTitleTrack').text('Add PMO Track');
					$('#add_'+track_key[2]+'_modal').modal('hide');
          clearPMO();
				}
			},
			error: function(xhr, status, error) {
				errorSwal();
			}
		});
	}
});

$(document).on('click', '#'+track_key[2]+'DelTrack', function() {
  Swal.fire({
		title: 'Are you sure?',
		text: "You want to remove this data.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Delete'
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: url + 'del_pmo_track',
				dataType: 'json',
				data:{id:pmo_id, status : del_pmo_status},
				type: 'POST',
				success: function(data) {
					clearPMO();
					SWAL(data.type, data.title,  data.msg);
					get_PMO_Track();
          $('#'+track_key[2]+'AddTrack, #'+track_key[2]+'ImportTrack').removeClass('d-none');
          $('#'+track_key[2]+'EditTrack, #'+track_key[2]+'DelTrack, #'+track_key[2]+'ViewTrack').addClass('d-none');
				},
				error: function(xhr, status, error) {
					errorSwal();
				}
			});
		}
	});
});

$(document).on('click', '#'+track_key[2]+'EditTrack', function() {
  let html = '';
  let ctr = 0;
  if (pmoData.length > 0) {
    $('#'+track_key[2]+'_status').val(1);
    for (i in pmoData) {
      if (ctr <= 0) {
        $('#'+track_key[2]+'-iid').val(pmoData[i].id);
        $('#'+track_key[2]+'_serial_number').val(pmoData[i].serial_number);
        $('#'+track_key[2]+'_site_name').val(pmoData[i].site_name);
        $('#'+track_key[2]+'_region').val(pmoData[i].region);
        $('#'+track_key[2]+'_budget_tagging').val(pmoData[i].budget_tagging);
        $('#'+track_key[2]+'_program').val(pmoData[i].program);
        $('#'+track_key[2]+'_project_phase').val(pmoData[i].project_phase);
        $('#'+track_key[2]+'_lead_vendor').val(pmoData[i].lead_vendor);
        $('#'+track_key[2]+'_solution').val(pmoData[i].solution);
        $('#'+track_key[2]+'_coverage').val(pmoData[i].coverage);
        $('#'+track_key[2]+'_scope').val(pmoData[i].scope);
        $('#'+track_key[2]+'_wireless_plan').val(pmoData[i].wireless_plan);
        $('#'+track_key[2]+'_sectors').val(pmoData[i].sectors);
      } else {
        html +=
        "<tr class="+track_key[2]+"_rows-"+ctr+">" +
         "<td>" +
          "<input type=\"hidden\" name=\"pmo_iid[]\" class=\"form-control\" value="+pmoData[i].id+">" +
          	"<input type=\"text\" name=\"pmo_serial_number[]\" class=\"form-control pmo_serial_number\" id=\"pmo_serial_number\" placeholder=\"Serial Number\" value="+pmoData[i].serial_number+" required>" +
          "</td>" +
       		"<td><input type=\"text\" name=\"pmo_site_name[]\" id=\"pmo_site_name\" cols=\"30\" rows=\"1\" class=\"form-control pmo_site_name\" placeholder=\"Site Name\" value="+pmoData[i].site_name+" required></td>" +
       		"<td><input type=\"text\" name=\"pmo_region[]\" class=\"form-control pmo_region\" id=\"pmo_region\" placeholder=\"Region\" value="+pmoData[i].region+" required></td>" +
       		"<td><input type=\"text\" name=\"pmo_budget_tagging[]\" class=\"form-control pmo_province\" id=\"pmo_budget_tagging\" placeholder=\"Province\" value="+pmoData[i].budget_tagging+" required></td>" +
          "<td><input type=\"text\" name=\"pmo_program[]\" class=\"form-control pmo_town_city\" id=\"pmo_program\" placeholder=\"Program\"  value="+pmoData[i].program+" required></td>" +
          "<td><input type=\"text\" name=\"pmo_project_phase[]\" class=\"form-control pmo_project_phase\" id=\"pmo_project_phase\" value="+pmoData[i].project_phase+" placeholder=\"Town Priority\" required></td>" +
          "<td><input type=\"text\" name=\"pmo_lead_vendor[]\" class=\"form-control pmo_lead_vendor\" id=\"pmo_lead_vendor\" placeholder=\"lead Vendor\" value="+pmoData[i].lead_vendor+" required></td>" +
          "<td><input type=\"text\" name=\"pmo_solution[]\" class=\"form-control pmo_solution\" id=\"pmo_solution\" placeholder=\"Solution\" value="+pmoData[i].solution+" required></td>" +
       		"<td><input type=\"text\" name=\"pmo_coverage[]\" class=\"form-control pmo_coverage\" id=\"pmo_coverage\" value='"+pmoData[i].coverage+"' placeholder=\"Program Tag\" required></td>" +
       		"<td><input type=\"text\" name=\"pmo_scope[]\" class=\"form-control pmo_scope\" id=\"pmo_scope\" placeholder=\"Planned Tech\" required value='"+pmoData[i].scope+"'></td>" +
          "<td><input type=\"text\" name=\"pmo_wireless_plan[]\" class=\"form-control pmo_wireless_plan\" id=\"pmo_wireless_plan\" placeholder=\"Wireless Plan\"  value='"+pmoData[i].wireless_plan+"'></td>" +
          "<td><input type=\"text\" name=\"pmo_sectors[]\" class=\"form-control pmo_sectors\" id=\"pmo_sectors\" placeholder=\"Sectors\" value='"+pmoData[i].sectors+"'></td>" +
          "<td class='text-center'><button type=\"button\" class=\"btn btn-danger btn-sm mg-left-3 remove_"+track_key[2]+"_track\"><i class=\"fas fa-minus\"></i></button></td>" +
         "</tr>";
      }
      ctr++;
    }
    $('#'+track_key[2]+'_track_tables').append(html);
    $('#add_'+track_key[2]+'_modal').modal('show');
  } else {
    SWAL('error', 'Opps!', 'Please check on the data listed.');
  }
});

$(document).on('click', '.'+track_key[2]+'_close', function() {
  let ctr = 1;
  for (i in pmoData) {
    $('.'+track_key[2]+'_rows-'+ctr).remove();
    ctr++;
  }
});

$(document).on('click', '#'+track_key[2]+'ViewTrack', function() {
  let html = '';
  let ctr = 1;
  for (i in pmoData) {
    let col = (ctr <= 1) ? 4 : 1;
		html += "<tr>" +
		"<td><small>"+ctr+".</small></td>" +
    "<td><small>"+pmoData[i].serial_number+"</small></td>" +
		"<td><small>"+pmoData[i].site_name+"</small></td>" +
    "<td><small>"+pmoData[i].region+"</small></td>" +
		"<td><small>"+pmoData[i].budget_tagging+"</small></td>" +
		"<td><small>"+pmoData[i].program+"</small></td>" +
		"<td><small>"+pmoData[i].project_phase+"</small></td>" +
		"<td><small>"+pmoData[i].lead_vendor+"</small></td>" +
    "<td><small>"+pmoData[i].solution+"</small></td>" +
    "<td><small>"+pmoData[i].coverage+"</small></td>" +
    "<td><small>"+pmoData[i].scope+"</small></td>" +
		"<td><small>"+pmoData[i].wireless_plan+"</small></td>" +
		"<td><small>"+pmoData[i].sectors+"</small></td>" +
		"</tr>";
    ctr++;
  }

  $('#view_'+track_key[2]+'_build').html(html);
  $('#viewPmoModal').modal('show');
});

function clearPMO() {
	$('#progressBarPmo').width(0);
	$('#submit'+track_key[2]+'Button, #clear_data_file').prop("disabled", false);
	$('#'+track_key[2]+'_file').val('');
	$('#chk_'+track_key[2]+'_item').prop('checked', false);
	let ctr = 1;
  for (i in pmoData) {
    $('.'+track_key[2]+'_rows-'+ctr).remove();
		ctr++;
  }
  $('.'+track_key[2]+'_serial_number, .'+track_key[2]+'_site_name, .'+track_key[2]+'_region,  .'+track_key[2]+'_budget_tagging, .'+track_key[2]+'_program, .'+track_key[2]+'_project_phase, .'+track_key[2]+'_lead_vendor, .'+track_key[2]+'_solution, .'+track_key[2]+'_coverage, .'+track_key[2]+'_scope, .'+track_key[2]+'_wireless_plan, .'+track_key[2]+'_sectors').val('');
  pmoData = [];
}
