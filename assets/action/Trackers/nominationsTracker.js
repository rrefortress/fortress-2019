let nomData = [];
let nom_id = [];
let charNom = ['site_range', 'site_type'];
let del_nom_status = false;

let viewData = [
	{
		'title' : 'Site Name',
		'name'    : 'site_name',
	},
  {
    'title' : 'Province',
    'name'    : 'province',
  },
	{
    'title' : 'Region',
		'name'    : 'region',
	},
	{
    'title' : 'TOWN/CITY',
		'name'    : 'town_City',
	},
  {
    'title' : 'Latitude',
    'name'    : 'latitude',
  },
	{
    'title' : 'Longitude',
		'name'    : 'longitude',
	},
	{
    'title' : 'Tagging',
    'name'    : 'tagging',
	},
	{
    'title' : 'Site Type',
    'name'    : 'site_type',
 	},
	{
    'title' : 'Remarks',
    'name'    : 'remarks',
	},
];

$(function () {
	let html = '';
	for (i in charNom) {
		html += '<div class="row border-top">' +
								'<div class="col-6 border-right">' +
									'<div class="chart-container" style="position: relative;">' +
										'<canvas id="lineChart_'+charNom[i]+'" class="mg-top-10"></canvas>' +
								'	</div>' +
								'</div>' +
								'<div class="col-6">' +
									'<div class="chart-container" style="position: relative;">' +
											'<canvas id="pieChart_'+charNom[i]+'" class="mg-top-10"></canvas>' +
									'</div>' +
								'</div>' +
							'</div>';
	}

	$('#chartsNom').html(html);
});

$(document).on('change', '#'+track_key[0]+'Province', function() {
  	$.ajax({
  		url: url + 'get_Nominations_Track',
  		dataType: 'json',
  		data: {name: $(this).val()},
  		type: 'POST',
  		cache:false,
  		success: function (data) {
  			if (data.data.length > 0) {
          setToNomGraph(data.data[0]);
  			}
  		},
  		error: function (xhr, status, error) {
  			errorSwal();
  		}
  	});
});

$(document).on('click', '#'+track_key[0]+'AddTrack', function() {
  $('#add_'+track_key[0]+'_modal').modal('show');
});

$(document).on('click', '#'+track_key[0]+'EditTrack', function() {
  let html = '';
  let ctr = 0;
  if (nomData.length > 0) {
    $('#nom_status').val(1);
    for (i in nomData) {
      if (ctr <= 0) {
        $('#'+track_key[0]+'-iid').val(nomData[i].id);
        $('#'+track_key[0]+'_site_name').val(nomData[i].site_name);
        $('#'+track_key[0]+'_region').val(nomData[i].region);
        $('#'+track_key[0]+'_province').val(nomData[i].province);
        $('#'+track_key[0]+'_town_city').val(nomData[i].town_city);
        $('#'+track_key[0]+'_latitude').val(nomData[i].latitude);
        $('#'+track_key[0]+'_longtitude').val(nomData[i].longtitude);
        $('#'+track_key[0]+'_tagging').val(nomData[i].tagging);
        $('#'+track_key[0]+'_site_type').val(nomData[i].site_type);
        $('#'+track_key[0]+'_remarks').val(nomData[i].remarks);
      } else {
        html +=
        "<tr class="+track_key[0]+"_rows-"+ctr+">" +
         "<td>" +
          "<input type=\"hidden\" name=\"nom_iid[]\" class=\"form-control\" value="+nomData[i].id+">" +
          "<input type=\"text\" name=\"nom_site_name[]\" class=\"form-control nom_site_name\" placeholder=\"Site Name\" value="+nomData[i].site_name+">" +
        "</td>" +
         "<td><input type=\"text\" name=\"nom_region[]\" class=\"form-control nom_region\" placeholder=\"Region\" value="+nomData[i].region+" required></td>" +
         "<td><input type=\"text\" name=\"nom_province[]\" class=\"form-control nom_province\" placeholder=\"Province\" value="+nomData[i].province+" required></td>" +
         "<td><input type=\"text\" name=\"nom_town_city[]\" class=\"form-control nom_town_city\" placeholder=\"Town/City\" value="+nomData[i].town_city+" required></td>" +
         "<td><input type=\"text\" name=\"nom_latitude[]\" class=\"form-control nom_latitude\" placeholder=\"Latitude\" value="+nomData[i].latitude+" required></td>" +
         "<td><input type=\"text\" name=\"nom_longtitude[]\" class=\"form-control nom_longtitude\" placeholder=\"Longtitude\" value="+nomData[i].longtitude+" required></td>" +
         "<td><input type=\"text\" name=\"nom_tagging[]\" class=\"form-control nom_tagging\" placeholder=\"Tagging\" value="+nomData[i].tagging+" required></td>" +
         "<td><input type=\"text\" name=\"nom_site_type[]\" class=\"form-control nom_site_type\"  placeholder=\"Site Type\" value="+nomData[i].site_type+" required></td>" +
         "<td><input type=\"text\" name=\"nom_remarks[]\" class=\"form-control nom_remarks\" placeholder=\"Remarks\" value="+nomData[i].remarks+" required></td>" +
         "<td class='text-center'><button type=\"button\" class=\"btn btn-danger btn-sm mg-left-3 remove_"+track_key[0]+"_track\"><i class=\"fas fa-minus\"></i></button></td>" +
         "</tr>";
      }
      ctr++;
    }
    $('#'+track_key[0]+'_track_tables').append(html);
    $('#add_'+track_key[0]+'_modal').modal('show');

  } else {
    SWAL('error', 'Opps!', 'Please check on the data listed.');
  }

});

$(document).on('click', '#'+track_key[0]+'ImportTrack', function() {
  $('#import_'+track_key[0]+'_modal').modal('show');
});

$(document).on('change', '#chk_'+track_key[0]+'_item', function () {
  nomData = [];
  nom_id = [];
	$('.check-tbl-'+track_key[0]+'-item').prop('checked', !!$(this).is(":checked"));
	let total_length = $('#'+track_key[0]+'_datas tr').length;
	if ($(this).is(":checked")) {
		for (i = 1; i <= total_length; i++) {
			let row = $('.'+track_key[0]+'_id-'+i);
      let id = row.closest("tr").find('td:eq(0)').attr('data-'+track_key[0]+'-id');
      let site_name = row.closest("tr").closest("tr").find('td:eq(2)').text();
      let region = row.closest("tr").find('td:eq(3)').text();
      let province = row.closest("tr").find('td:eq(4)').text();
      let town_city = row.closest("tr").find('td:eq(5)').text();
      let latitude = row.closest("tr").find('td:eq(6)').text();
      let longtitude = row.closest("tr").find('td:eq(7)').text();
      let tagging = row.closest("tr").find('td:eq(8)').text();
      let site_type = row.closest("tr").find('td:eq(9)').text();
      let remarks = row.closest("tr").find('td:eq(10)').text();

      nomData.push({id:id, site_name: site_name,
                region: region, province: province,
                town_city: town_city, latitude: latitude,
                longtitude: longtitude, tagging: tagging,
                site_type: site_type, remarks: remarks,
              });
      nom_id.push(id);
		}
    $('#'+track_key[0]+'AddTrack, #'+track_key[0]+'ImportTrack').addClass('d-none');
    $('#'+track_key[0]+'EditTrack, #'+track_key[0]+'DelTrack, #'+track_key[0]+'ViewTrack').removeClass('d-none');
		del_nom_status = true;
	} else {
    $('#'+track_key[0]+'AddTrack, #'+track_key[0]+'ImportTrack').removeClass('d-none');
    $('#'+track_key[0]+'EditTrack, #'+track_key[0]+'DelTrack, #'+track_key[0]+'ViewTrack').addClass('d-none');
    $('.'+track_key[0]+'_site_name, .'+track_key[0]+'_region, .'+track_key[0]+'_province, .'+track_key[0]+'_town_city, .'+track_key[0]+'_latitude, .'+track_key[0]+'_longtitude, .'+track_key[0]+'_tagging, .'+track_key[0]+'_site_type, .'+track_key[0]+'_remarks').val('');
		del_nom_status = false;
	}
});

$(document).on('click', '.check-tbl-'+track_key[0]+'-item', function () {
	let chk = $('#chk_'+track_key[0]+'_item');
	let total_checked = $('#'+track_key[0]+'_datas').find('.check-tbl-'+track_key[0]+'-item:checked').length;
	let total_length = $('#'+track_key[0]+'_datas tr').length;
	chk.prop('checked', total_checked === total_length);

	if (total_checked === total_length) {
    $('#'+track_key[0]+'AddTrack, #'+track_key[0]+'ImportTrack').addClass('d-none');
    $('#'+track_key[0]+'EditTrack, #'+track_key[0]+'DelTrack, #'+track_key[0]+'ViewTrack').removeClass('d-none');
	} else {
    if ( total_checked === 0) {
      $('#'+track_key[0]+'AddTrack, #'+track_key[0]+'ImportTrack').removeClass('d-none');
      $('#'+track_key[0]+'EditTrack, #'+track_key[0]+'DelTrack, #'+track_key[0]+'ViewTrack').addClass('d-none');
      $('.'+track_key[0]+'_site_name, .'+track_key[0]+'_region, .'+track_key[0]+'_province, .'+track_key[0]+'_town_city, .'+track_key[0]+'_latitude, .'+track_key[0]+'_longtitude, .'+track_key[0]+'_tagging, .'+track_key[0]+'_site_type, .'+track_key[0]+'_remarks').val('');
    }
	}

  let id = $(this).closest("tr").find('td:eq(0)').attr('data-'+track_key[0]+'-id');
  let site_name = $(this).closest("tr").closest("tr").find('td:eq(2)').text();
  let region = $(this).closest("tr").find('td:eq(3)').text();
  let province = $(this).closest("tr").find('td:eq(4)').text();
  let town_city = $(this).closest("tr").find('td:eq(5)').text();
  let latitude = $(this).closest("tr").find('td:eq(6)').text();
  let longtitude = $(this).closest("tr").find('td:eq(7)').text();
  let tagging = $(this).closest("tr").find('td:eq(8)').text();
  let site_type = $(this).closest("tr").find('td:eq(9)').text();
  let remarks = $(this).closest("tr").find('td:eq(10)').text();

	if ($(this).is(":checked")) {
    nomData.push({id:id, site_name: site_name,
              region: region, province: province,
              town_city: town_city, latitude: latitude,
              longtitude: longtitude, tagging: tagging,
              site_type: site_type, remarks: remarks,
            });
    nom_id.push(id);
    $('#'+track_key[0]+'AddTrack, #'+track_key[0]+'ImportTrack').addClass('d-none');
    $('#'+track_key[0]+'EditTrack, #'+track_key[0]+'DelTrack, #'+track_key[0]+'ViewTrack').removeClass('d-none');
	} else {
    _.remove(nomData, function(e) {
      return e.id === id
    });
    nom_id.pop(id);
	}
});

$(document).on('click', '.append_'+track_key[0]+'_track', function(e){
	e.preventDefault();
	let length = $('#'+track_key[0]+'_table tr').length;
	setNomToAppend(length);
});

$(document).on('click', '#remove_'+track_key[0]+'_Button', function(e){
	e.preventDefault();
	$('#'+track_key[0]+'_file').val('');
});

$(document).on('click', '#'+track_key[0]+'DelTrack', function() {
  Swal.fire({
		title: 'Are you sure?',
		text: "You want to remove this data.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Delete'
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: url + 'del_nom_track',
				dataType: 'json',
				data:{id:nom_id, status : del_nom_status},
				type: 'POST',
				success: function(data) {
					clearNom();
					SWAL(data.type, data.title,  data.msg);
					get_Nominations_Track();
          $$('.total_'+track_key[0]+'_details').text(data.data.length);
          $('#'+track_key[0]+'AddTrack, #'+track_key[0]+'ImportTrack').removeClass('d-none');
          $('#'+track_key[0]+'EditTrack, #'+track_key[0]+'DelTrack, #'+track_key[0]+'ViewTrack').addClass('d-none');
				},
				error: function(xhr, status, error) {
					errorSwal();
				}
			});
		}
	});
});

$(document).on('click', '#'+track_key[0]+'ViewTrack', function() {
  let html = '';
  let ctr = 1;
  for (i in nomData) {
    let col = (ctr <= 1) ? 4 : 1;
		html += "<tr>" +
		"<td><small>"+ctr+".</small></td>" +
		"<td><small>"+nomData[i].site_name+"</small></td>" +
		"<td><small>"+nomData[i].province+"</small></td>" +
		"<td><small>"+nomData[i].region+"</small></td>" +
		"<td><small>"+nomData[i].town_city+"</small></td>" +
		"<td><small>"+nomData[i].latitude+"</small></td>" +
		"<td><small>"+nomData[i].longtitude+"</small></td>" +
		"<td><small>"+nomData[i].tagging+"</small></td>" +
		"<td><small>"+nomData[i].site_type+"</small></td>" +
		"<td><small>"+nomData[i].remarks+"</small></td>" +
		"</tr>";
    ctr++;
  }

  $('#view_'+track_key[0]+'_build').html(html);
  $('#viewNomModal').modal('show');
});

$(document).on('click', '.remove_'+track_key[0]+'_track', function(e){
	e.preventDefault();
	$(this).closest('tr').remove();
});

$(document).on('submit', '#edd_'+track_key[0]+'_Track', function(e){
	e.preventDefault();

	let values = $('input[name="'+track_key[0]+'_site_name[]"]').map(function() {
		return this.value;
	}).toArray();

	let checkExists = !values.every(function(v,i) {
		return values.indexOf(v) == i;
	});

	if (checkExists) {
		SWAL('error', 'Invalid!', 'Duplicate subject name.');
	} else {
		$.ajax({
			url: url + 'save_nomination_track',
			dataType: 'json',
			data: $(this).serialize(),
			type: 'POST',
			success: function(data) {
				SWAL(data.type, data.title,  data.msg);
				if (data.type === 'success') {
					get_Nominations_Track();
					SWAL(data.type, data.title,  data.msg);
					$('.'+track_key[0]+'_region, .'+track_key[0]+'_province, .'+track_key[0]+'_town_city, .'+track_key[0]+'_latitude, .'+track_key[0]+'_longtitude, .'+track_key[0]+'_tagging, .'+track_key[0]+'_site_type, .'+track_key[0]+'_remarks').val('');
					$('.selected').removeClass('selected');
					$('#'+track_key[0]+'AddTrack, #'+track_key[0]+'ImportTrack').removeClass('d-none');
					$('#'+track_key[0]+'EditTrack, #'+track_key[0]+'DelTrack, #'+track_key[0]+'ViewTrack').addClass('d-none');
					$('#NomTitleTrack').text('Add Nomination Track');
					$('#add_'+track_key[0]+'_modal').modal('hide');
          clearNom();
				}
			},
			error: function(xhr, status, error) {
				errorSwal();
			}
		});
	}
});

$(document).on('click', function() {
	$('#file-Nom-upload').ajaxForm({
		url:  url + 'nom_upload',
		contentType: false,
		cache:false,
		processData: false,
		beforeSubmit: function () {
			$('#submitNomButton, #clear_data_file').prop("disabled", true);
			$('#progressNomId').fadeIn('slow');
			$("#progressNomId").css("display", "block");
			var percentValue = '0%';

			$('#progressBarNom').width(percentValue);
			$('#percentNom').html(percentValue);
		},
		uploadProgress: function (event, position, total, percentComplete) {
			var percentValue = percentComplete + '%';
			$("#progressBarNom").animate({
				width: '' + percentValue + ''
			}, {
				duration: 5000,
				easing: "linear",
				step: function (x) {
					percentText = Math.round(x * 100 / percentComplete);
					$("#percentNom").text(percentText + "%");
					if(percentText == "100") {
						SWAL('success', 'Success!', 'Successfully uploaded.');
						setTimeout(function(){
              $('#import_nom_modal').modal('hide');
              clearNom();
						}, 1000);
						setTimeout(function(){
							$('#progressNomId').fadeOut('slow');
						}, 3000);
					}
				}
			});
		},
		error: function (response, status, e) {
			SWAL('error', 'Opps!', 'Something went wrong.');
			setTimeout(function(){
				$('#progressNomId').fadeOut('slow');
			}, 3000);
		},
		complete: function (xhr) {
			if (xhr.responseText && xhr.responseText != "error")
			{
				let json = JSON.parse(xhr.responseText);

				if (json.type == 'error') {
					SWAL(json.type, json.title, json.msg);
					clearNom();
					$('#progressNomId').fadeOut('fast');
				} else {
					get_Nominations_Track();
				}
			}
			else{
				SWAL('error', 'Opps!', 'Problem in uploading file.');
				clearNom();
				$("#progressBarNom").stop();
				setTimeout(function(){
					$('#progressNomId').fadeOut('slow');
				}, 3000);
			}
		}
	});
});

$(document).on('click', '.'+track_key[0]+'_close', function() {
  let ctr = 1;
  for (i in nomData) {
    $('.'+track_key[0]+'_rows-'+ctr).remove();
		ctr++;
  }
});

function clearNom() {
	$('#progressBarNom').width(0);
	$('#submitNomButton, #clear_data_file').prop("disabled", false);
	$('#nom_file').val('');
	$('#chk_'+track_key[0]+'_item').prop('checked', false);
	let ctr = 1;
  for (i in nomData) {
    $('.'+track_key[0]+'_rows-'+ctr).remove();
		ctr++;
  }
  $('.'+track_key[0]+'_site_name, .'+track_key[0]+'_region, .'+track_key[0]+'_province, .'+track_key[0]+'_town_city, .'+track_key[0]+'_latitude, .'+track_key[0]+'_longtitude, .'+track_key[0]+'_tagging, .'+track_key[0]+'_site_type, .'+track_key[0]+'_remarks').val('');
  nomData = [];
}

function setNomToAppend(length) {
	let html = "<tr class="+track_key[0]+"_rows-"+length+">" +
		"<td><textarea name=\"nom_site_name[]\" id=\"nom_site_name\" cols=\"30\" rows=\"1\" class=\"form-control nom_site_name\" placeholder=\"Site Name\" required></textarea></td>" +
		"<td><input type=\"text\" name=\"nom_region[]\" class=\"form-control nom_region\" id=\"nom_region\" placeholder=\"Region\" required></td>" +
		"<td><input type=\"text\" name=\"nom_province[]\" class=\"form-control nom_province\" id=\"nom_province\" placeholder=\"Province\" required></td>" +
    "<td><input type=\"text\" name=\"nom_town_city[]\" class=\"form-control nom_town_city\" id=\"nom_town_city\" placeholder=\"Town/City\" required></td>" +
    "<td><input type=\"text\" name=\"nom_latitude[]\" class=\"form-control nom_latitude\" id=\"nom_latitude\" placeholder=\"Latitude\" required></td>" +
    "<td><input type=\"text\" name=\"nom_longtitude[]\" class=\"form-control nom_longtitude\" id=\"nom_longtitude\" placeholder=\"longtitude\" required></td>" +
    "<td><input type=\"text\" name=\"nom_tagging[]\" class=\"form-control nom_tagging\" id=\"nom_tagging\" placeholder=\"Tagging\" required></td>" +
    "<td><input type=\"text\" name=\"nom_site_type[]\" class=\"form-control nom_site_type\" id=\"nom_site_type\" placeholder=\"Site Type\" required></td>" +
    "<td><textarea name=\"nom_remarks[]\" id=\"nom_remarks\" cols=\"30\" rows=\"1\" class=\"form-control nom_remarks\" placeholder=\"Remarks\"></textarea></td>" +
    "<td class='text-center'><button type=\"button\" class=\"btn btn-danger btn-sm mg-left-3 remove_"+track_key[0]+"_track\"><i class=\"fas fa-minus\"></i></button></td>" +
    "</tr>";
		$('#'+track_key[0]+'_track_tables').append(html);
}

$(document).on('keyup', '#'+track_key[0]+'Search', function(e) {
	e.preventDefault();
	get_Nominations_Track(0, row_page = 10, $(this).val());
});

$(document).on('change', '#'+track_key[0]+'_sort', function() {
		get_Nominations_Track(0, $(this).val());
});

$(document).on('click', '#'+track_key[0]+'_pagination a', function(e) {
	e.preventDefault();
	let row_num = $(this).attr('data-ci-pagination-page');
  get_Nominations_Track(row_num);
});

function get_Nominations_Track(row_num = 0, row_page = 10, search = '') {
	$.ajax({
		url: url + 'FileTrack/get_Nominations_Track/'+row_num,
		dataType: 'json',
		data: {row_num: row_num, row_page : row_page, search: search},
		type: 'GET',
		beforeSend() {
      let html = '<div class="content">' +
        '<h5 class="text-center text-white">' +
        '<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>' +
        '<h4 class="text-dark text-center">Loading</h4>' +
        '</h5>' +
        '</div>';
      $('#'+track_key[0]+'_datas').html(html);
    },
		success: function (data) {
				setNominations(data);
		},
		error: function (xhr, status, error) {
			errorSwal();
		}
	});
}

function setNominations(data) {
  let html = '';
  if (data.details.length > 0) {
    $('.emptyNomChartTrack, .emptyNomTrack').addClass('d-none');
    $('.nomFilter, .nomChart, #nomSearch, .nomListsTrack').removeClass('d-none');
    setToNomGraph(data.data[0]);
    setTONomDetails(data.details, 0, data.pagination, data.total);
    if (data.pname.length > 0) {
      html += '<option value="" selected>All</option>';
      for (i in data.pname) {
        html += '<option>'+data.pname[i].name+'</option>';
      }
    }
    $('#nomProvince').html(html);
  } else {
    $('.emptyNomChartTrack, .emptyNomTrack').removeClass('d-none');
    $('.nomFilter, .nomChart, .nomListsTrack').addClass('d-none');
  }
}

function setTONomDetails(data, status = 0, pagination, total) {
  let html = '';
  let count = 1;
  let ctr = 1;
  if (data.length > 0) {
    for (i in data) {
      html += '<tr class="nom_id-'+ctr+'">' +
        '<td style="width:5% !important;" data-nom-id='+data[i].id+'>' +
          '<div class="custom-control pad-left-rem custom-checkbox chk-nom-'+i+'">' +
            '<input type="checkbox" name="table2_lists" class="custom-control-input check-tbl-nom-item" id="check-nom-item-'+i+'">' +
            '<label class="custom-control-label" for="check-nom-item-'+i+'"></label>' +
          '</div>' +
        '</td>' +
				'<td class="text-center" style="width:7% !important;">'+data[i].id+'.</td>' +
				'<td class="text-truncate" data-toggle="tooltip" data-placement="top" title="'+data[i].SITE_NAME+'">'+data[i].SITE_NAME+'</td>' +
				'<td class="text-center text-truncate">'+data[i].REGION+'</td>' +
				'<td class="text-center text-truncate">'+data[i].PROVINCE+'</td>' +
				'<td class="text-center text-truncate">'+data[i].TOWN_CITY+'</td>' +
        '<td class="text-center text-truncate">'+data[i].Latitude+'</td>' +
        '<td class="text-center text-truncate">'+data[i].Longitude+'</td>' +
        '<td class="text-center text-truncate">'+data[i].Tagging+'</td>' +
        '<td class="text-center text-truncate">'+data[i].Site_Type+'</td>' +
				'<td class="text-center text-truncate">'+data[i].Remarks+'</td>';
			'<tr>';
      ctr++;
    }
    $('#emptyNomTrack').find('form-control').val('');
    $('#nom_datas').html(html);
		$('#nom_pagination').html(pagination);
    $('.btnTracks').show();
		$('#' + track_key[0] + 'EditTrack, #' + track_key[0] + 'DelTrack,  #' + track_key[0] + 'ViewTrack').addClass('d-none');
    $('.' + track_key[0] + 'ListsTrack, #' + track_key[0] + 'AddTrack, #' + track_key[0] + 'ImportTrack').removeClass('d-none');
    $('.emptyNomTrack').addClass('d-none');
		$('#chk_' + track_key[0] + '_item').prop('checked', false);
  } else {
    $('.emptyNomTrack').removeClass('d-none');
    $('.' + track_key[0] + 'ListsTrack, #' + track_key[0] + 'AddTrack, #' + track_key[0] + 'ImportTrack').addClass('d-none');
    $('#chk_' + track_key[0] + '_item').prop('disabled', true);
  }
	$('[data-toggle="tooltip"]').tooltip();
	$('.total_nom_details').text(total);
}

function setToNomGraph(data) {
  let canvasPie1 = document.getElementById("pieChart_site_range");
  let ctxPie1 = canvasPie1.getContext('2d');
  let canvasLine1 = document.getElementById("lineChart_site_range");
  let ctxLine1 = canvasLine1.getContext('2d');

  let canvasPie2 = document.getElementById("pieChart_site_type");
  let ctxPie2 = canvasPie2.getContext('2d');
  let canvasLine2 = document.getElementById("lineChart_site_type");
  let ctxLine2 = canvasLine2.getContext('2d');

	let capacity = data.Capacity;
	let coverage = data.Coverage;
  let macro = data.MACRO;
  let smallcell = data.SMALLCELL;
  let total = data.total;
	let total_per = getWholePercent(capacity, total) + getWholePercent(coverage, total);

	let dataNom1 = {
		labels: [capacity + ":Capacity", coverage + ":Coverage"],
		datasets: [
			{
				fill: true,
				backgroundColor: ['#d81b60', '#5e35b1'],
				data: [getWholePercent(capacity, total), getWholePercent(coverage, total)],
			}
		]
	};

	let optionsPie1 = {
		title    : setTitle("Pie Chart"),
		plugins  : setPlugins(),
		elements : centerText("Site Range"),
		rotation : -0.7 * Math.PI
	};

	let optionsBar1 = {
		title    : setTitle("Bar Graph"),
		plugins  : setPlugins(),
		rotation : -0.7 * Math.PI,
    legend: {
        display: false
    },
	};

  let dataNom2 = {
    labels: [macro + ":MACRO", smallcell + ":SMALLCELL"],
    datasets: [
      {
        label: "",
        fill: true,
        backgroundColor: ['#007bff', '#dc3545'],
        data: [getWholePercent(macro, total), getWholePercent(smallcell, total)],
      }
    ]
  };

  let optionsPie2 = {
    title    : setTitle("Pie Chart"),
    plugins  : setPlugins(),
    elements : centerText("Site Type"),
    rotation : -0.7 * Math.PI,
		responsive: true,
    maintainAspectRatio: true
  };

  let optionsBar2 = {
    title    : setTitle("Bar Graph"),
    plugins  : setPlugins(),
    rotation : -0.7 * Math.PI,
		responsive: true,
    maintainAspectRatio: true,
    legend: {
        display: false
    },
  };

	build_chart(dataNom1, optionsPie1, ctxPie1, total_per, 'doughnut', 'pieRange');
	build_chart(dataNom1, optionsBar1, ctxLine1, total_per, 'horizontalBar', 'barRange');

	build_chart(dataNom2, optionsPie2, ctxPie2, total_per, 'doughnut', 'pieType');
	build_chart(dataNom2, optionsBar2, ctxLine2, total_per, 'horizontalBar', 'barType');
}
