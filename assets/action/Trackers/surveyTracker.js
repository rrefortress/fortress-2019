let surveyData = [];
let survey_id = [];
// let surData = ['site_status', 'category', 'vendor'];
let surData = ['category', 'vendor'];
let del_survey_status = false;
$(function () {
	let html = '';
	for (i in surData) {
		html += '<div class="row border-top">' +
			'<div class="col-sm-6 col-md-6 col-lg-6 border-right">' +
			'<div class="chart-container" style="position: relative;">' +
			'<canvas id="line_' + surData[i] + '" class="mg-top-10"></canvas>' +
			'	</div>' +
			'</div>' +
			'<div class="col-sm-6 col-md-6 col-lg-6">' +
			'<div class="chart-container" style="position: relative;">' +
			'<canvas id="pie_' + surData[i] + '" class="mg-top-10"></canvas>' +
			'</div>' +
			'</div>' +
			'</div>';
	}

	$('#chartsSurvey').html(html);
});


$(document).on('change', '#chk_' + track_key[3] + '_item', function () {
	surveyData = [];
	survey_id = [];
	$('.check-tbl-' + track_key[3] + '-item').prop('checked', !!$(this).is(":checked"));
	let total_length = $('#' + track_key[3] + '_datas tr').length;
	if ($(this).is(":checked")) {
		for (i = 1; i <= total_length; i++) {
			let row = $('.' + track_key[3] + '_id-' + i);
			let id = row.closest("tr").find('td:eq(0)').attr('data-' + track_key[3] + '-id');
			let site_name = row.closest("tr").closest("tr").find('td:eq(2)').text();
			let date = row.closest("tr").find('td:eq(3)').text();
			let latitude = row.closest("tr").find('td:eq(4)').text();
			let longtitude = row.closest("tr").find('td:eq(5)').text();
			let project = row.closest("tr").find('td:eq(6)').text();
			let rre = row.closest("tr").find('td:eq(7)').attr('data-RRE_Representative');
			let rre_name = row.closest("tr").find('td:eq(7)').text();
			let rfe = row.closest("tr").find('td:eq(8)').text();
			let saq = row.closest("tr").find('td:eq(9)').text();
			let category = row.closest("tr").find('td:eq(10)').text();
			let vendor = row.closest("tr").find('td:eq(11)').text();
			let status = row.closest("tr").find('td:eq(12)').text();
			let remarks = row.closest("tr").find('td:eq(13)').text();

			surveyData.push({
				id: id, site_name: site_name,
				date: date, latitude: latitude, longtitude: longtitude,
				project: project, rre: rre,
				rre_name: rre_name, rfe: rfe, saq: saq,
				category: category, vendor: vendor,
				status: status, remarks: remarks,
			});
			survey_id.push(id);
		}

		$('#' + track_key[3] + 'AddTrack, #' + track_key[3] + 'ImportTrack').addClass('d-none');
		$('#' + track_key[3] + 'EditTrack, #' + track_key[3] + 'DelTrack, #' + track_key[3] + 'ViewTrack').removeClass('d-none');
		del_survey_status = true;
	} else {
		$('#' + track_key[3] + 'AddTrack, #' + track_key[3] + 'ImportTrack').removeClass('d-none');
		$('#' + track_key[3] + 'EditTrack, #' + track_key[3] + 'DelTrack, #' + track_key[3] + 'ViewTrack').addClass('d-none');

		del_survey_status = false;
	}
});

$(document).on('click', '.check-tbl-' + track_key[3] + '-item', function () {
	let chk = $('#chk_' + track_key[3] + '_item');
	let total_checked = $('#' + track_key[3] + '_datas').find('.check-tbl-' + track_key[3] + '-item:checked').length;
	let total_length = $('#' + track_key[3] + '_datas tr').length;
	chk.prop('checked', total_checked === total_length);

	if (total_checked === total_length) {
		$('#' + track_key[3] + 'AddTrack, #' + track_key[3] + 'ImportTrack').addClass('d-none');
		$('#' + track_key[3] + 'EditTrack, #' + track_key[3] + 'DelTrack, #' + track_key[3] + 'ViewTrack').removeClass('d-none');
	} else {
		if (total_checked === 0) {
			$('#' + track_key[3] + 'AddTrack, #' + track_key[3] + 'ImportTrack').removeClass('d-none');
			$('#' + track_key[3] + 'EditTrack, #' + track_key[3] + 'DelTrack, #' + track_key[3] + 'ViewTrack').addClass('d-none');
			clearSurvey();
		}
	}

	let id = $(this).closest("tr").find('td:eq(0)').attr('data-' + track_key[3] + '-id');
	let site_name = $(this).closest("tr").closest("tr").find('td:eq(2)').text();
	let date = $(this).closest("tr").find('td:eq(3)').text();
	let latitude = $(this).closest("tr").find('td:eq(4)').text();
	let longtitude = $(this).closest("tr").find('td:eq(5)').text();
	let project = $(this).closest("tr").find('td:eq(6)').text();
	let rre = $(this).closest("tr").find('td:eq(7)').attr('data-RRE_Representative');
	let rre_name = $(this).closest("tr").find('td:eq(7)').text();
	let rfe = $(this).closest("tr").find('td:eq(8)').text();
	let saq = $(this).closest("tr").find('td:eq(9)').text();
	let category = $(this).closest("tr").find('td:eq(10)').text();
	let vendor = $(this).closest("tr").find('td:eq(11)').text();
	let status = $(this).closest("tr").find('td:eq(12)').text();
	let remarks = $(this).closest("tr").find('td:eq(13)').text();

	if ($(this).is(":checked")) {
		surveyData.push({
			id: id, site_name: site_name,
			date: date, latitude: latitude, longtitude: longtitude,
			project: project, rre: rre,
			rre_name: rre_name, rfe: rfe, saq: saq,
			category: category, vendor: vendor,
			status: status, remarks: remarks,
		});
		survey_id.push(id);
		$('#' + track_key[3] + 'AddTrack, #' + track_key[3] + 'ImportTrack').addClass('d-none');
		$('#' + track_key[3] + 'EditTrack, #' + track_key[3] + 'DelTrack, #' + track_key[3] + 'ViewTrack').removeClass('d-none');
	} else {
		_.remove(surveyData, function (e) {
			return e.id === id
		});
		survey_id.pop(id);
	}
});

function get_Surveys_Track(row_num = 0, row_page = 10, search = '', name = '') {
	$.ajax({
		url: url + 'FileTrack/get_Surveys_Track/' + row_num,
		dataType: 'json',
		data: {row_num: row_num, row_page: row_page, search: search, name: name},
		type: 'GET',
		beforeSend() {
			let html = '<div class="content">' +
				'<h5 class="text-center text-white">' +
				'<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>' +
				'<h4 class="text-dark text-center">Loading</h4>' +
				'</h5>' +
				'</div>';
			$('#' + track_key[3] + '_datas').html(html);
		},
		success: function (data) {
			$('#survey_datas').html('');
			setSurvey(data);
		},
		error: function (xhr, status, error) {
			errorSwal();
		}
	});
}

function setSurvey(data) {
	if (data.details.length > 0) {
		$('.emptySurveyChartTrack, .emptySurveyTrack').addClass('d-none');
		$('.surveyChart, #surveySearch, .surveyListsTrack, #surveyProject, #surveyName, #chartsSurvey').removeClass('d-none');
		// setToSiteStatus(data.site_status, data.total);
		setToSiteCategory(data.category, data.total);
		setToVendor(data.vendor, data.total);
		setToSurveyDetails(data.details, data.pagination, data.total);
		let name = '';
		if (data.user_name.length > 0) {
			name += '<option value="" selected>All</option>';
			for (i in data.user_name) {
				name += '<option value=' + data.user_name[i].globe_id + '>' + data.user_name[i].fname + ' ' + data.user_name[i].lname + '</option>';
			}
		}
		$('#surveyName').html(name);
		let survey_details = $('#surveyNameDetails').val();
		if (survey_details == null) {
			$('#surveyNameDetails').html(name);
		}

		let projects = '';
		if (data.project.length > 0) {
			for (i in data.project) {
				let project = !data.project[i].project || data.project[i].project == '#N/A' ? 'All' : data.project[i].project;
				projects += '<option value="' + data.project[i].project + '">' + project + '</option>';
			}
		}
		$('#surveyProject').html(projects);
	} else {
		$('.emptySurveyChartTrack, .emptySurveyTrack').removeClass('d-none');
		$('.surveyChart, .surveyListsTrack, #surveyProject, #surveyName, #chartsSurvey').addClass('d-none');
	}
}

$(document).on('change', '#surveyProject', function () {
	let project = $('#surveyProject').val() == 'null' ? '' : $('#surveyProject').val();
	$.ajax({
		url: url + 'get_Surveys_Track',
		dataType: 'json',
		data: {project: project},
		type: 'POST',
		cache: false,
		success: function (data) {
			// setToSiteStatus(data.site_status, data.total);
			setToSiteCategory(data.category, data.total);
			setToVendor(data.vendor, data.total);
			$('#surveyName').val('');
		},
		error: function (xhr, status, error) {
			errorSwal();
		}
	});
});

$(document).on('change', '#surveyName', function () {
	let name = $(this).val();
	let project = $('#surveyProject').val() == 'null' ? '' : $('#surveyProject').val();
	$.ajax({
		url: url + 'get_Surveys_Track_name',
		dataType: 'json',
		data: {name: $(this).val(), project: project},
		type: 'POST',
		cache: false,
		success: function (data) {
			// setToSiteStatus(data.site_status, data.total);
			setToSiteCategory(data.category, data.total);
			setToVendor(data.vendor, data.total);
		},
		error: function (xhr, status, error) {
			errorSwal();
		}
	});
});

$(document).on('change', '#surveyNameDetails', function (e) {
	e.preventDefault();
	let row_num = $(this).attr('data-ci-pagination-page');
	let search = $('#surveySearch').val();
	let name = $(this).val();
	get_Surveys_Track(0, row_num, search, name);
});

$(document).on('keyup', '#' + track_key[3] + 'Search', function (e) {
	e.preventDefault();
	let row_num = $(this).attr('data-ci-pagination-page');
	let name = $('#surveyNameDetails').val();
	let search = $(this).val();
	get_Surveys_Track(0, row_num, search, name);
});

$(document).on('change', '#' + track_key[3] + '_sort', function (e) {
	e.preventDefault();
	let name = $('#surveyNameDetails').val();
	let search = $('#surveySearch').val();
	let row_num = $(this).val();
	get_Surveys_Track(0, row_num, search, name);
});

$(document).on('click', '#' + track_key[3] + '_pagination a', function (e) {
	e.preventDefault();
	let row_num = $(this).attr('data-ci-pagination-page');
	let name = $('#surveyNameDetails').val();
	let search = $('#surveySearch').val();
	// get_Surveys_Track(row_num);

	get_Surveys_Track(row_num, 10, search, name);
});

function setToSurveyDetails(data = 0, pagination = 0, total = 0) {
	let html = '';
	let count = 1;
	let ls = 0;
	let ctr = 1;
	if (data.length > 0) {
		total_length = data.length;
		for (i in data) {
			html += '<tr class="survey_id-' + ctr + '">' +
				'<td style="width:5% !important;" data-survey-id=' + data[i].id + '>' +
				'<div class="custom-control pad-left-rem custom-checkbox chk-survey-' + i + '">' +
				'<input type="checkbox" name="table2_lists" class="custom-control-input check-tbl-survey-item" id="check-survey-item-' + i + '">' +
				'<label class="custom-control-label" for="check-survey-item-' + i + '"></label>' +
				'</div>' +
				'</td>' +
				'<td class="text-center" style="width:10% !important;" data-id=' + data[i].id + '>' + ctr + '.</td>' +
				'<td class="text-truncate" data-toggle="tooltip" data-placement="top" title=' + data[i].SITE_NAME + '>' + data[i].SITE_NAME + '</td>' +
				'<td class="text-center text-truncate" data-toggle="tooltip" data-placement="top" title="' + data[i].Survey_Date + '">' + data[i].Survey_Date + '</td>' +
				'<td class="text-center text-truncate" data-toggle="tooltip" data-placement="top" title="' + data[i].latitude + '">' + data[i].latitude + '</td>' +
				'<td class="text-center text-truncate" data-toggle="tooltip" data-placement="top" title="' + data[i].longtitude + '">' + data[i].longtitude + '</td>' +
				'<td class="text-center text-truncate" data-toggle="tooltip" data-placement="top" title="' + data[i].Project + '">' + data[i].Project + '</td>' +
				'<td class="text-center" data-RRE_Representative=' + data[i].RRE_Representative + '>' + data[i].rre_name + '</td>' +
				'<td class="text-center">' + data[i].RFE_Representative + '</td>' +
				'<td class="text-center">' + data[i].SAQ_Representative + '</td>' +
				'<td class="text-center">' + data[i].Category + '</td>' +
				'<td class="text-center">' + data[i].Vendor + '</td>' +
				'<td class="text-center">' + data[i].Site_Status + '</td>' +
				'<td class="text-center">' + data[i].Remarks + '</td>';
			'<tr>';
			ctr++;
		}

		$('#emptySurveyTrack').find('form-control').val('');
		$('#survey_datas').html(html);
		$('#survey_pagination').html(pagination);
		$('.btnSurveyTracks').show();
		$('.aweListsTrack, #aweAddTrack, #surveyImportTrack').removeClass('d-none');
		$('#' + track_key[3] + 'EditTrack, #' + track_key[3] + 'DelTrack,  #' + track_key[3] + 'ViewTrack').addClass('d-none');
		$('.' + track_key[3] + 'ListsTrack, #' + track_key[3] + 'AddTrack, #' + track_key[3] + 'ImportTrack').removeClass('d-none');
		$('.emptySurveyTrack').addClass('d-none');
		$('#chk_' + track_key[3] + '_item').prop('checked', false);
	} else {
		$('.btnSurveyTracks').hide();
		$('.tableListsTrack, #addSurveyTrack').addClass('d-none');
		$('.emptyListsTracks').removeClass('d-none');

		$('.emptySurveyTrack').removeClass('d-none');
		$('.' + track_key[3] + 'ListsTrack, #' + track_key[3] + 'AddTrack, #' + track_key[3] + 'ImportTrack').addClass('d-none');
		$('#chk_' + track_key[3] + '_item').prop('disabled', true);
	}
	$('[data-toggle="tooltip"]').tooltip();
	$('.total_survey_details').text(total);
}

// function setToSiteStatus(data, total = 0) {
//   let canvasPie = document.getElementById("pie_site_status");
//   let ctxPie = canvasPie.getContext('2d');
//   let canvasLine = document.getElementById("line_site_status");
//   let ctxLine = canvasLine.getContext('2d');
//
//   let artb        = data.artb;
//   let cancel      = data.cancel;
//   let for_build   = data.for_build;
//   let implemented = data.implemented;
//   let rtb         = data.rtb;
//   let surveyed    = data.surveyed;
//   let sNA         = data.sNA;
//   let total_per = getWholePercent(artb, total) +
//                   getWholePercent(cancel, total) +
//                   getWholePercent(for_build, total) +
//                   getWholePercent(implemented, total) +
//                   getWholePercent(rtb, total) +
//                   getWholePercent(surveyed, total) +
//                   getWholePercent(sNA, total);
//
//   let dataSurvey = {
// 		labels: [artb + ":ARTB'd",
//             cancel + ":Cancelled",
//             for_build + ":For Build",
//             implemented + ":Implemented",
//             rtb + ":RTB'd",
//             surveyed + ":Surveyed",
//             sNA + ":N/A"
//             ],
// 		datasets: [
// 			{
// 				label: "",
// 				fill: true,
// 				backgroundColor: ['#a35638', '#8b2f97', '#e25822', '#ffd800', '#46b3e6', '#0c9463', '#c70d3a'],
// 				data: [ getWholePercent(artb, total) ,
//                         getWholePercent(cancel, total) ,
//                         getWholePercent(for_build, total) ,
//                         getWholePercent(implemented, total) ,
//                         getWholePercent(rtb, total) ,
//                         getWholePercent(surveyed, total) ,
//                         getWholePercent(sNA, total)],
// 			}
// 		]
// 	};
//
//   optionsPie.elements = centerText('Site Status');
//   optionsPie.title = setTitle("Pie Chart");
//   optionsBar.title = setTitle("Bar Graphs");
//
//   build_chart(dataSurvey, optionsPie, ctxPie, total_per, 'doughnut', 'pieSite');
//   build_chart(dataSurvey, optionsBar, ctxLine, total_per, 'horizontalBar', 'barSite');
// }

function setToSiteCategory(data, total = 0) {
	let canvasPie = document.getElementById("pie_category");
	let ctxPie = canvasPie.getContext('2d');
	let canvasLine = document.getElementById("line_category");
	let ctxLine = canvasLine.getContext('2d');

	let ewo = data.ewo;
	let master_plan = data.master_plan;
	let new_site = data.new_site;
	let owo = data.owo;
	let re_survey = data.re_survey;
	let cNA = data.cNA;

	let total_per = getWholePercent(ewo, total) +
		getWholePercent(master_plan, total) +
		getWholePercent(new_site, total) +
		getWholePercent(owo, total) +
		getWholePercent(re_survey, total) +
		getWholePercent(cNA, total);

	let dataSurvey = {
		labels: [
			ewo + ":EWO",
			master_plan + ":Master Plan",
			new_site + ":New Site",
			owo + ":OWO",
			re_survey + ":Re-survey",
			cNA + ":N/A"
		],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#10316b', '#ffd800', '#27aa80', '#9d0b0b', '#ff502f', '#393e46'],
				data: [getWholePercent(ewo, total),
					getWholePercent(master_plan, total),
					getWholePercent(new_site, total),
					getWholePercent(owo, total),
					getWholePercent(re_survey, total),
					getWholePercent(cNA, total)
				],
			}
		]
	};

	optionsPie.elements = centerText('Category');
	optionsPie.title = setTitle("Pie Chart");
	optionsBar.title = setTitle("Bar Graph");

	build_chart(dataSurvey, optionsPie, ctxPie, total_per, 'doughnut', 'pieCat');
	build_chart(dataSurvey, optionsBar, ctxLine, total_per, 'horizontalBar', 'barCat');
}

function setToVendor(data, total = 0) {
	let canvasPie = document.getElementById("pie_vendor");
	let ctxPie = canvasPie.getContext('2d');
	let canvasLine = document.getElementById("line_vendor");
	let ctxLine = canvasLine.getContext('2d');

	let argus = data.argus;
	let bspt = data.bspt;
	let gk = data.gk;
	let huawei = data.huawei;
	let mohave = data.mohave;
	let power_acts = data.power_acts;
	let richworld = data.richworld;
	let vNA = data.vNA;

	let total_per = getWholePercent(argus, total) +
		getWholePercent(bspt, total) +
		getWholePercent(gk, total) +
		getWholePercent(huawei, total) +
		getWholePercent(mohave, total) +
		getWholePercent(power_acts, total) +
		getWholePercent(richworld, total) +
		getWholePercent(vNA, total);

	let dataSurvey = {
		labels: [argus + ":Argus",
			bspt + ":BSPT",
			gk + ":GK",
			huawei + ":Huawei",
			mohave + ":Mohave",
			power_acts + ":Power acts",
			richworld + ":Richworld  ",
			vNA + ":N/A"
		],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#801336', '#46b3e6', '#a35638', '#c70d3a', '#ffdc34', '#ff4081', '#1f6650', '#3c4245'],
				data: [getWholePercent(argus, total),
					getWholePercent(bspt, total),
					getWholePercent(gk, total),
					getWholePercent(huawei, total),
					getWholePercent(mohave, total),
					getWholePercent(power_acts, total),
					getWholePercent(richworld, total),
					getWholePercent(vNA, total)
				],
			}
		]
	};

	optionsPie.elements = centerText('Vendor');
	optionsPie.title = setTitle("Pie Chart");
	optionsBar.title = setTitle("Bar Graph");

	build_chart(dataSurvey, optionsPie, ctxPie, total_per, 'doughnut', 'pieVendor');
	build_chart(dataSurvey, optionsBar, ctxLine, total_per, 'horizontalBar', 'barVendor');
}

$(document).on('click', '#' + track_key[3] + 'ImportTrack', function () {
	$('#import_' + track_key[3] + '_modal').modal('show');
});

$(document).on('click', function () {
	$('#file-survey-upload').ajaxForm({
		url: url + 'survey_upload',
		contentType: false,
		cache: false,
		processData: false,
		beforeSubmit: function () {
			$('#submitSurveyButton, #clear_data_file').prop("disabled", true);
			$('#progresSurveyId').fadeIn('slow');
			$("#progresSurveyId").css("display", "block");
			var percentValue = '0%';

			$('#progressBarSurvey').width(percentValue);
			$('#percentSurvey').html(percentValue);
		},
		uploadProgress: function (event, position, total, percentComplete) {
			var percentValue = percentComplete + '%';
			$("#progressBarSurvey").animate({
				width: '' + percentValue + ''
			}, {
				duration: 5000,
				easing: "linear",
				step: function (x) {
					percentText = Math.round(x * 100 / percentComplete);
					$("#percentSurvey").text(percentText + "%");
					if (percentText == "100") {
						setTimeout(function () {
							SWAL('success', 'Success!', 'Successfully uploaded.');
							$('#import_survey_modal').modal('hide');
							$('.' + track_key[3] + '-iid').val(0);
						}, 1000);
						setTimeout(function () {
							$('#progresSurveyId').fadeOut('slow');
						}, 3000);
					}
				}
			});
		},
		error: function (response, status, e) {
			SWAL('error', 'Opps!', 'Something went wrong.');
			setTimeout(function () {
				$('#progresSurveyId').fadeOut('slow');
			}, 3000);
		},
		complete: function (xhr) {
			if (xhr.responseText && xhr.responseText != "error") {
				let json = JSON.parse(xhr.responseText);

				if (json.type == 'error') {
					SWAL(json.type, json.title, json.msg);
					clearsurvey();
					$('#progresSurveyId').fadeOut('fast');
				} else {
					get_Surveys_Track();
					callCalendar();
				}
			} else {
				SWAL('error', 'Opps!', 'Problem in uploading file.');
				clearsurvey();
				$("#progressBarSurvey").stop();
				setTimeout(function () {
					$('#progresSurveyId').fadeOut('slow');
				}, 3000);
			}
		}
	});
});

$(document).on('click', '#' + track_key[3] + 'AddTrack', function () {
	setTrackRRE(0, 0);
	clearSurvey();
	$('#add_survey_modal').modal('show');
});

function setTrackRRE(length, val = 0) {
	$.ajax({
		url: url + 'getTrackUsers',
		dataType: 'json',
		type: 'GET',
		success: function (data) {
			let html = '';
			let gid = [];
			if (data.length > 0) {
				html = '<option disabled selected>Select RRE</option>';
				html += '<option value="0">Null</option>';
				for (i in data) {
					html += '<option value=' + data[i].globe_id + '>' + data[i].fname + '</option>';
					gid.push(data[i].globe_id);
				}
				// val == '' ? NULL : val;
				$("." + track_key[3] + "_rows-" + length).find('.' + track_key[3] + '_rre').html(html).val(val == 'null' ? 0 : val);
			} else {
				$("." + track_key[3] + "_rows-" + length).find('.' + track_key[3] + '_rre').hide();
			}
		},
		error: function (xhr, status, error) {
			SWAL('error', 'Opps!', 'Something went wrong please try again.');
		}
	});
}

$(document).on('click', '.' + track_key[3] + '_close', function () {
	let ctr = 1;
	for (i in surveyData) {
		$('.' + track_key[3] + '_rows-' + ctr).remove();
		ctr++;
	}
});

$(document).on('click', '#' + track_key[3] + 'EditTrack', function () {
	let html = '';
	let ctr = 0;
	let rre = '';
	if (surveyData.length > 0) {
		$('#s_status').val(1);
		for (i in surveyData) {
			if (ctr <= 0) {
				$('#' + track_key[3] + '-iid').val(surveyData[i].id);
				$('#' + track_key[3] + '_site_name').val(surveyData[i].site_name);
				$('#' + track_key[3] + '_date').val(surveyData[i].date);
				$('#' + track_key[3] + '_latitude').val(surveyData[i].latitude);
				$('#' + track_key[3] + '_longtitude').val(surveyData[i].longtitude);
				$('#' + track_key[3] + '_project').val(surveyData[i].project);
				$('#' + track_key[3] + '_rre').val(surveyData[i].rre);
				$('#' + track_key[3] + '_rfe').val(surveyData[i].rfe);
				$('#' + track_key[3] + '_saq').val(surveyData[i].saq);
				$('#' + track_key[3] + '_category').val(surveyData[i].category);
				$('#' + track_key[3] + '_vendor').val(surveyData[i].vendor);
				$('#' + track_key[3] + '_status').val(surveyData[i].status);
				$('#' + track_key[3] + '_remarks').val(surveyData[i].remarks);
				rre = surveyData[i].rre;
			} else {
				html +=
					"<tr class=" + track_key[3] + "_rows-" + ctr + ">" +
					"<td>" +
					"<input type=\"hidden\" name=\"" + track_key[3] + "_iid[]\" class=\"form-control\" value=" + surveyData[i].id + ">" +
					"<input type=\"text\" name=\"" + track_key[3] + "_site_name[]\" class=\"form-control " + track_key[3] + "_site_name\" placeholder=\"Site Name\" value=" + surveyData[i].site_name + ">" +
					"</td>" +
					"<td><input type=\"date\" name=\"" + track_key[3] + "_date[]\" class=\"form-control " + track_key[3] + "_date\" placeholder=\"Date\" value='" + surveyData[i].date + "' required></td>" +
					"<td><input type=\"text\" name=\"" + track_key[3] + "_latitude[]\" class=\"form-control " + track_key[3] + "_latitude\" id=\"survey_latitude\" placeholder=\"Latitude\" value='" + surveyData[i].latitude + "' required></td>" +
					"<td><input type=\"text\" name=\"" + track_key[3] + "_longtitude[]\" class=\"form-control " + track_key[3] + "_longtitude\" id=\"survey_longtitude\" placeholder=\"Longtitude\" value='" + surveyData[i].longtitude + "' required></td>" +
					"<td><input type=\"text\" name=\"" + track_key[3] + "_project[]\" class=\"form-control " + track_key[3] + "_project\" placeholder=\"Project\" value='" + surveyData[i].project + "' required></td>" +
					"<td><select name=\"" + track_key[3] + "_rre[]\" class=\"form-control " + track_key[3] + "_rre\"></select></td>" +
					"<td><input type=\"text\" name=\"" + track_key[3] + "_rfe[]\" class=\"form-control " + track_key[3] + "_rfe\" placeholder=\"RFE\" value='" + surveyData[i].rfe + "' required></td>" +
					"<td><input type=\"text\" name=\"" + track_key[3] + "_saq[]\" class=\"form-control " + track_key[3] + "_saq\" placeholder=\"SAQ\" value='" + surveyData[i].saq + "' required></td>" +
					"<td><input type=\"text\" name=\"" + track_key[3] + "_category[]\" class=\"form-control " + track_key[3] + "_category\" placeholder=\"Category\" value='" + surveyData[i].category + "' required></td>" +
					"<td><input type=\"text\" name=\"" + track_key[3] + "_vendor[]\" class=\"form-control " + track_key[3] + "_vendor\" placeholder=\"Tagging\" value='" + surveyData[i].vendor + "' required></td>" +
					"<td><input type=\"text\" name=\"" + track_key[3] + "_status[]\" class=\"form-control " + track_key[3] + "status\"  placeholder=\"Status\" value='" + surveyData[i].status + "' required></td>" +
					"<td><input type=\"text\" name=\"" + track_key[3] + "_remarks[]\" class=\"form-control " + track_key[3] + "_remarks\" placeholder=\"Remarks\" value='" + surveyData[i].remarks + "' required></td>" +
					"<td class='text-center'><button type=\"button\" class=\"btn btn-danger btn-sm mg-left-3 remove_" + track_key[3] + "_track\"><i class=\"fas fa-minus\"></i></button></td>" +
					"</tr>";
			}
			ctr++;
		}
		$('#' + track_key[3] + '_track_tables').append(html);
		$('#add_' + track_key[3] + '_modal').modal('show');
		for (i in surveyData) {
			setTrackRRE(i, surveyData[i].rre);
		}
	} else {
		SWAL('error', 'Opps!', 'Please check on the data listed.');
	}
});

$(document).on('click', '.remove_' + track_key[3] + '_track', function (e) {
	e.preventDefault();
	$(this).closest('tr').remove();
});

$(document).on('submit', '#edd_' + track_key[3] + '_Track', function (e) {
	e.preventDefault();

	let values = $('input[name="' + track_key[3] + '_site_name[]"]').map(function () {
		return this.value;
	}).toArray();

	let checkExists = !values.every(function (v, i) {
		return values.indexOf(v) == i;
	});

	if (checkExists) {
		SWAL('error', 'Invalid!', 'Duplicate subject name.');
	} else {
		$.ajax({
			url: url + 'save_survey_track',
			dataType: 'json',
			data: $(this).serialize(),
			type: 'POST',
			success: function (data) {
				SWAL(data.type, data.title, data.msg);
				if (data.type === 'success') {
					get_Surveys_Track();
					callCalendar();
					SWAL(data.type, data.title, data.msg);
					emit_activities(data.activities);
					$('.' + track_key[3] + '-iid').val(0);
					$('.selected').removeClass('selected');
					$('#' + track_key[3] + 'AddTrack, #' + track_key[3] + 'ImportTrack').removeClass('d-none');
					$('#' + track_key[3] + 'EditTrack, #' + track_key[3] + 'DelTrack, #' + track_key[3] + 'ViewTrack').addClass('d-none');
					$('#SurveyTitleTrack').text('Add Survey Track');
					$('#add_' + track_key[3] + '_modal').modal('hide');
					clearSurvey();
				}
			},
			error: function (xhr, status, error) {
				errorSwal();
			}
		});
	}
});

$(document).on('click', '.append_' + track_key[3] + '_track', function (e) {
	e.preventDefault();
	let length = $('#survey_track_tables tr').length;
	setTrackRRE(length, 0);
	setSurveyToAppend(length);
});

function setSurveyToAppend(length) {
	let html = "<tr class=" + track_key[3] + "_rows-" + length + ">" +
		"<td>" +
		"<input type=\"hidden\" name=\"" + track_key[3] + "_iid[]\" class=\"form-control\" value='0'>" +
		"<input type=\"text\" name=\"" + track_key[3] + "_site_name[]\" class=\"form-control " + track_key[3] + "_site_name\" placeholder=\"Site Name\">" +
		"</td>" +
		"<td><input type=\"date\" name=\"" + track_key[3] + "_date[]\" class=\"form-control " + track_key[3] + "_date\" placeholder=\"Date\" required></td>" +
		"<td><input type=\"text\" name=\"survey_latitude[]\" class=\"form-control " + track_key[3] + "_latitude\" id=\"survey_latitude\" placeholder=\"Latitude\" required></td>" +
		"<td><input type=\"text\" name=\"survey_longtitude[]\" class=\"form-control " + track_key[3] + "_longtitude\" id=\"survey_longtitude\" placeholder=\"Longtitude\" required></td>" +
		"<td><input type=\"text\" name=\"" + track_key[3] + "_project[]\" class=\"form-control " + track_key[3] + "_project\" placeholder=\"Project\" required></td>" +
		"<td><select name=\"" + track_key[3] + "_rre[]\" class=\"form-control " + track_key[3] + "_rre\"></select></td>" +
		"<td><input type=\"text\" name=\"" + track_key[3] + "_rfe[]\" class=\"form-control " + track_key[3] + "_rfe\" placeholder=\"RFE\" required></td>" +
		"<td><input type=\"text\" name=\"" + track_key[3] + "_saq[]\" class=\"form-control " + track_key[3] + "_saq\" placeholder=\"SAQ\" required></td>" +
		"<td><input type=\"text\" name=\"" + track_key[3] + "_category[]\" class=\"form-control " + track_key[3] + "_category\" placeholder=\"Category\" required></td>" +
		"<td><input type=\"text\" name=\"" + track_key[3] + "_vendor[]\" class=\"form-control " + track_key[3] + "_vendor\" placeholder=\"Vendor\" required></td>" +
		"<td><input type=\"text\" name=\"" + track_key[3] + "_status[]\" class=\"form-control " + track_key[3] + "status\"  placeholder=\"Status\" required></td>" +
		"<td><input type=\"text\" name=\"" + track_key[3] + "_remarks[]\" class=\"form-control " + track_key[3] + "_remarks\" placeholder=\"Remarks\" required></td>" +
		"<td class='text-center'><button type=\"button\" class=\"btn btn-danger btn-sm mg-left-3 remove_" + track_key[3] + "_track\"><i class=\"fas fa-minus\"></i></button></td>" +
		"</tr>";
	$('#' + track_key[3] + '_track_tables').append(html);
}

$(document).on('click', '#' + track_key[3] + 'ViewTrack', function () {
	let html = '';
	let ctr = 1;
	for (i in surveyData) {
		let col = (ctr <= 1) ? 4 : 1;
		html += "<tr>" +
			"<td><small>" + ctr + ".</small></td>" +
			"<td><small>" + surveyData[i].site_name + "</small></td>" +
			"<td><small>" + surveyData[i].date + "</small></td>" +
			"<td><small>" + surveyData[i].latitude + "</small></td>" +
			"<td><small>" + surveyData[i].longtitude + "</small></td>" +
			"<td><small>" + surveyData[i].project + "</small></td>" +
			"<td><small>" + surveyData[i].rre_name + "</small></td>" +
			"<td><small>" + surveyData[i].rfe + "</small></td>" +
			"<td><small>" + surveyData[i].saq + "</small></td>" +
			"<td><small>" + surveyData[i].category + "</small></td>" +
			"<td><small>" + surveyData[i].vendor + "</small></td>" +
			"<td><small>" + surveyData[i].status + "</small></td>" +
			"<td><small>" + surveyData[i].remarks + "</small></td>" +
			"</tr>";
		ctr++;
	}

	$('#view_' + track_key[3] + '_build').html(html);
	$('#view' + track_key[3] + 'Modal').modal('show');
});

$(document).on('click', '#' + track_key[3] + 'DelTrack', function () {
	Swal.fire({
		title: 'Are you sure?',
		text: "You want to remove this data.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Delete'
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: url + 'del_survey_track',
				dataType: 'json',
				data: {id: survey_id, status: del_survey_status},
				type: 'POST',
				success: function (data) {
					clearSurvey();
					SWAL(data.type, data.title, data.msg);
					get_Surveys_Track();
					$('.total_' + track_key[3] + '_details').text(data.length);
					$('#' + track_key[3] + 'AddTrack, #' + track_key[3] + 'ImportTrack').removeClass('d-none');
					$('#' + track_key[3] + 'EditTrack, #' + track_key[3] + 'DelTrack, #' + track_key[3] + 'ViewTrack').addClass('d-none');
				},
				error: function (xhr, status, error) {
					errorSwal();
				}
			});
		}
	});
});

function clearSurvey() {
	$('#surveySearch').val('');
	$('#progressBarNom').width(0);
	$('#submitSurveyButton, #clear_data_file').prop("disabled", false);
	$('#survey_file').val('');
	$('#chk_' + track_key[3] + '_item').prop('checked', false);
	let ctr = 1;
	for (i in surveyData) {
		$('.' + track_key[3] + '_rows-' + ctr).remove();
		ctr++;
	}
	$('.' + track_key[3] + '-iid').val(0);
	$('.' + track_key[3] + '_rre').val(0);
	$('.' + track_key[3] + '_site_name,.' + track_key[3] + '_project, .' + track_key[3] + '_latitude, .' + track_key[3] + '_longtitude, .' + track_key[3] + '_rre,.' + track_key[3] + '_rfe,.' + track_key[3] + '_saq,.' + track_key[3] + '_category,.' + track_key[3] + '_vendor,.' + track_key[3] + '_status,.' + track_key[3] + '_remarks').val('');
	surveyData = [];
}
