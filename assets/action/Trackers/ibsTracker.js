let ibsData = [];
let ibs_id = [];
let del_ibs_status = false;
let charIbs = ['Building_Type'];
$(function() {
	let html = '';
	for (i in charIbs) {
		html += '<div class="row border-top border-bottom">' +
			'<div class="col-sm-6 col-md-6 col-lg-6 border-right">' +
			'<div class="chart-container" style="position: relative;">' +
			'<canvas id="line_' + charIbs[i] + '" class="mg-top-10"></canvas>' +
			'	</div>' +
			'</div>' +
			'<div class="col-sm-6 col-md-6 col-lg-6">' +
			'<div class="chart-container" style="position: relative;">' +
			'<canvas id="pie_' + charIbs[i] + '" class="mg-top-10"></canvas>' +
			'</div>' +
			'</div>' +
			'</div>';
	}

	$('#chartsIBS').html(html);
});

$(document).on('change', '#ibsProvince', function() {
	$.ajax({
		url: url + 'get_IBS_Track',
		dataType: 'json',
		data: {
			name: $(this).val()
		},
		type: 'POST',
		cache: false,
		success: function(data) {
			setBuilding_Type(data.Building_Type, data.total);
			setToIBSDetails(data.details);
		},
		error: function(xhr, status, error) {
			errorSwal();
		}
	});
});

function setIBS(data) {
	let html = '';
	if (data.details.length > 0) {
		$('.emptyibsChartTrack, .emptyibsTrack').addClass('d-none');
		$('.ibsChart, #ibsSearch, .ibsListsTrack').removeClass('d-none');
		setBuilding_Type(data.Building_Type, data.total);
		setToIBSDetails(data.details, data.pagination, data.total);
		if (data.pname.length > 0) {
			html += '<option value="" selected>All</option>';
			for (i in data.pname) {
				html += '<option>' + data.pname[i].name + '</option>';
			}
		}
		$('#ibsProvince').html(html);
	} else {
		$('.emptyibsChartTrack, .emptyibsTrack').removeClass('d-none');
		$('.ibsChart, .ibsListsTrack').addClass('d-none');
	}
}

function setBuilding_Type(data, total = 0) {
	let canvasPie = document.getElementById("pie_Building_Type");
	let ctxPie = canvasPie.getContext('2d');
	let canvasLine = document.getElementById("line_Building_Type");
	let ctxLine = canvasLine.getContext('2d');

	let airport = data.airport;
	let condominium = data.condominium;
	let corporate_office = data.corporate_office;
	let globe_store = data.globe_store;
	let hospital = data.hospital;
	let hotel = data.hotel;
	let mall = data.mall;
	let office = data.office;
	let resort = data.resort;


	let total_per = getWholePercent(airport, total) +
		getWholePercent(condominium, total) +
		getWholePercent(corporate_office, total) +
		getWholePercent(globe_store, total) +
		getWholePercent(hospital, total) +
		getWholePercent(hotel, total) +
		getWholePercent(mall, total) +
		getWholePercent(office, total) +
		getWholePercent(resort, total);

	let dataIBS = {
		labels: [airport + ":Airport",
			condominium + ":Condominium",
			corporate_office + ": Corporate Office",
			globe_store + ": Globe Store",
			hospital + ": Hospital",
			hotel + ": Hotel",
			mall + ": Mall",
			office + ": Office",
			resort + ": Resort "
		],
		datasets: [{
			label: "",
			fill: true,
			backgroundColor: ['#a35638', '#8b2f97', '#e25822', '#ffd800', '#46b3e6', '#0c9463', '#c70d3a', '#00897b', '#263238'],
			data: [getWholePercent(airport, total),
				getWholePercent(condominium, total),
				getWholePercent(corporate_office, total),
				getWholePercent(globe_store, total),
				getWholePercent(hospital, total),
				getWholePercent(hotel, total),
				getWholePercent(mall, total),
				getWholePercent(office, total),
				getWholePercent(resort, total)
			],
		}]
	};

	optionsPie.elements = centerText('Building Type');
	optionsPie.title = setTitle("Pie Chart");
	optionsBar.title = setTitle("Bar Graph");

	build_chart(dataIBS, optionsPie, ctxPie, total_per, 'doughnut', 'pieIBS');
	build_chart(dataIBS, optionsBar, ctxLine, total_per, 'horizontalBar', 'barIBS');
}

$(document).on('keyup', '#' + track_key[4] + 'Search', function(e) {
	e.preventDefault();
	get_IBS_Track(0, row_page = 10, $(this).val());
});

$(document).on('change', '#' + track_key[4] + '_sort', function() {
	get_IBS_Track(0, $(this).val());
});

$(document).on('click', '#' + track_key[4] + '_pagination a', function(e) {
	e.preventDefault();
	let row_num = $(this).attr('data-ci-pagination-page');
	get_IBS_Track(row_num);
});

function get_IBS_Track(row_num = 0, row_page = 10, search = '') {
	$.ajax({
		url: url + 'FileTrack/get_IBS_Track/' + row_num,
		dataType: 'json',
		data: {
			row_num: row_num,
			row_page: row_page,
			search: search
		},
		type: 'GET',
		beforeSend() {
			let html = '<div class="content">' +
				'<h5 class="text-center text-white">' +
				'<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>' +
				'<h4 class="text-dark text-center">Loading</h4>' +
				'</h5>' +
				'</div>';
			$('#' + track_key[4] + '_datas').html(html);
		},
		success: function(data) {
			setIBS(data);
		},
		error: function(xhr, status, error) {
			errorSwal();
		}
	});
}

function setToIBSDetails(data, pagination, total) {
	let html = '';
	let count = 1;
	let ctr = 1;
	if (data.length > 0) {
		for (i in data) {
			html += '<tr class="ibs_id-'+ctr+'">' +
				'<td style="width:5% !important;" data-ibs-id=' + data[i].id + '>' +
				'<div class="custom-control pad-left-rem custom-checkbox chk-ibs-' + i + '">' +
				'<input type="checkbox" name="table2_lists" class="custom-control-input check-tbl-ibs-item" id="check-ibs-item-' + i + '">' +
				'<label class="custom-control-label" for="check-ibs-item-' + i + '"></label>' +
				'</div>' +
				'</td>' +
				'<td class="text-center" style="width:10% !important;" data-id=' + data[i].id + '>' + data[i].id + '.</td>' +
				'<td class="text-truncate" data-toggle="tooltip" data-placement="top" title="' + data[i].SITE_NAME + '">' + data[i].SITE_NAME + '</td>' +
				'<td class="text-truncate" data-toggle="tooltip" data-placement="top" title="' + data[i].Building_Name + '">' + data[i].Building_Name + '</td>' +
				'<td class="text-truncate" data-toggle="tooltip" data-placement="top" title="' + data[i].Building_Type + '">' + data[i].Building_Type + '</td>' +
				'<td class="text-truncate" data-toggle="tooltip" data-placement="top" title="' + data[i].PLA_ID + '">' + data[i].PLA_ID + '</td>' +
				'<td class="text-truncate" data-toggle="tooltip" data-placement="top" title="' + data[i].Longitude + '">' + data[i].Longitude + '</td>' +
				'<td class="text-truncate" data-toggle="tooltip" data-placement="top" title="' + data[i].Latitude + '">' + data[i].Latitude + '</td>' +
				'<td class="text-truncate" data-toggle="tooltip" data-placement="top" title="' + data[i].Region + '">' + data[i].Region + '</td>' +
				'<td class="text-truncate" data-toggle="tooltip" data-placement="top" title="' + data[i].Province + '">' + data[i].Province + '</td>' +
				'<td class="text-truncate" data-toggle="tooltip" data-placement="top" title="' + data[i].Municipality_City + '">' + data[i].Municipality_City + '</td>' +
				'<td class="text-truncate" data-toggle="tooltip" data-placement="top" title="' + data[i].address + '">' + data[i].address + '</td>';
			'<tr>';
      ctr++;
		}

		$('#emptyibsTrack').find('form-control').val('');
		$('#ibs_datas').html(html);
		$('#ibs_pagination').html(pagination);
		$('.emptyIBSTrack').addClass('d-none');
		$('#' + track_key[4] + 'EditTrack, #' + track_key[4] + 'DelTrack,  #' + track_key[4] + 'ViewTrack').addClass('d-none');
    $('.' + track_key[4] + 'ListsTrack, #' + track_key[4] + 'AddTrack, #' + track_key[4] + 'ImportTrack').removeClass('d-none');
    $('#chk_' + track_key[4] + '_item').prop('checked', false);
	} else {
		$('.' + track_key[4] + 'ListsTrack, #' + track_key[4] + 'AddTrack, #' + track_key[4] + 'ImportTrack').addClass('d-none');
		$('#chk_' + track_key[4] + '_item').prop('disabled', true);
	}

	$('[data-toggle="tooltip"]').tooltip();
	$('.total_ibs_details').text(total);
}

$(document).on('click', '#' + track_key[4] + 'ImportTrack', function() {
	$('#import_' + track_key[4] + '_modal').modal('show');
});

$(document).on('change', '#chk_' + track_key[4] + '_item', function() {
	ibsData = [];
	ibs_id = [];
	$('.check-tbl-' + track_key[4] + '-item').prop('checked', !!$(this).is(":checked"));
	let total_length = $('#' + track_key[4] + '_datas tr').length;
	if ($(this).is(":checked")) {
		for (i = 1; i <= total_length; i++) {
			let row = $('.' + track_key[4] + '_id-' + i);
			let id = row.closest("tr").find('td:eq(0)').attr('data-' + track_key[4] + '-id');
			let site_name = row.closest("tr").closest("tr").find('td:eq(2)').text();
			let bldg_type = row.closest("tr").find('td:eq(3)').text();
			let bldg_name = row.closest("tr").find('td:eq(4)').text();
			let pla_id = row.closest("tr").find('td:eq(5)').text();
			let longtitude = row.closest("tr").find('td:eq(6)').text();
			let latitude = row.closest("tr").find('td:eq(7)').text();
			let region = row.closest("tr").find('td:eq(8)').text();
			let province = row.closest("tr").find('td:eq(9)').text();
			let mun_city = row.closest("tr").find('td:eq(10)').text();
			let address = row.closest("tr").find('td:eq(11)').text();
			ibsData.push({
				id: id,
				site_name: site_name,
				bldg_type: bldg_type,
				bldg_name: bldg_name,
				pla_id: pla_id,
				longtitude: longtitude,
				latitude: latitude,
				region: region,
				province: province,
				mun_city: mun_city,
				address: address
			});
			ibs_id.push(id);
		}
		$('#' + track_key[4] + 'AddTrack, #' + track_key[4] + 'ImportTrack').addClass('d-none');
		$('#' + track_key[4] + 'EditTrack, #' + track_key[4] + 'DelTrack,  #' + track_key[4] + 'ViewTrack').removeClass('d-none');
		del_ibs_status = true;
	} else {
		$('#' + track_key[4] + 'AddTrack, #' + track_key[4] + 'ImportTrack').removeClass('d-none');
		$('#' + track_key[4] + 'EditTrack, #' + track_key[4] + 'DelTrack, #' + track_key[4] + 'ViewTrack').addClass('d-none');
		$('.' + track_key[4] + '_site_name, .' + track_key[4] + '_bldg_name, .' + track_key[4] + '_bldg_type, .' + track_key[4] + '_pla_id, .' + track_key[4] + '_latitude, .' + track_key[4] + '_longtitude, .' + track_key[4] + '_region, .' + track_key[4] + '_province, .' + track_key[4] + '_mun_city, .' + track_key[4] + '_address').val('');
		del_ibs_status = false;
	}
});

$(document).on('click', '.check-tbl-' + track_key[4] + '-item', function() {
	let chk = $('#chk_' + track_key[4] + '_item');
	let total_checked = $('#' + track_key[4] + '_datas').find('.check-tbl-' + track_key[4] + '-item:checked').length;
	let total_length = $('#' + track_key[4] + '_datas tr').length;
	chk.prop('checked', total_checked === total_length);

	if (total_checked === total_length) {
		$('#' + track_key[4] + 'AddTrack, #' + track_key[4] + 'ImportTrack').addClass('d-none');
		$('#' + track_key[4] + 'EditTrack, #' + track_key[4] + 'DelTrack, #' + track_key[4] + 'ViewTrack').removeClass('d-none');
	} else {
		if (total_checked === 0) {
			$('#' + track_key[4] + 'AddTrack, #' + track_key[4] + 'ImportTrack').removeClass('d-none');
			$('#' + track_key[4] + 'EditTrack, #' + track_key[4] + 'DelTrack, #' + track_key[4] + 'ViewTrack').addClass('d-none');
			$('.' + track_key[4] + '_site_name, .' + track_key[4] + '_bldg_name, .' + track_key[4] + '_bldg_type, .' + track_key[4] + '_pla_id, .' + track_key[4] + '_latitude, .' + track_key[4] + '_longtitude, .' + track_key[4] + '_region, .' + track_key[4] + '_province, .' + track_key[4] + '_mun_city, .' + track_key[4] + '_address').val('');
		}
	}

	let id = $(this).closest("tr").find('td:eq(0)').attr('data-' + track_key[4] + '-id');
	let site_name = $(this).closest("tr").closest("tr").find('td:eq(2)').text();
	let bldg_type = $(this).closest("tr").find('td:eq(3)').text();
	let bldg_name = $(this).closest("tr").find('td:eq(4)').text();
	let pla_id = $(this).closest("tr").find('td:eq(5)').text();
	let longtitude = $(this).closest("tr").find('td:eq(6)').text();
	let latitude = $(this).closest("tr").find('td:eq(7)').text();
	let region = $(this).closest("tr").find('td:eq(8)').text();
	let province = $(this).closest("tr").find('td:eq(9)').text();
	let mun_city = $(this).closest("tr").find('td:eq(10)').text();
	let address = $(this).closest("tr").find('td:eq(11)').text();

	if ($(this).is(":checked")) {
		ibsData.push({
			id: id,
			site_name: site_name,
			bldg_type: bldg_type,
			bldg_name: bldg_name,
			pla_id: pla_id,
			longtitude: longtitude,
			latitude: latitude,
			region: region,
			province: province,
			mun_city: mun_city,
			address: address
		});
		ibs_id.push(id);
		$('#' + track_key[4] + 'AddTrack, #' + track_key[4] + 'ImportTrack').addClass('d-none');
		$('#' + track_key[4] + 'EditTrack, #' + track_key[4] + 'DelTrack, #' + track_key[4] + 'ViewTrack').removeClass('d-none');
	} else {
		_.remove(ibsData, function(e) {
			return e.id === id
		});
		ibs_id.pop(id);
	}
});

$(document).on('click', function() {
	$('#file-ibs-upload').ajaxForm({
		url: url + 'ibs_upload',
		contentType: false,
		cache: false,
		processData: false,
		beforeSubmit: function() {
			$('#submitibsButton, #clear_data_file').prop("disabled", true);
			$('#progressIbsId').fadeIn('slow');
			$("#progressIbsId").css("display", "block");
			var percentValue = '0%';
			$('#progressBarIbs').width(percentValue);
			$('#percentIbs').html(percentValue);
		},
		uploadProgress: function(event, position, total, percentComplete) {
			var percentValue = percentComplete + '%';
			$("#progressBarIbs").animate({
				width: '' + percentValue + ''
			}, {
				duration: 5000,
				easing: "linear",
				step: function(x) {
					percentText = Math.round(x * 100 / percentComplete);
					$("#percentIbs").text(percentText + "%");
					if (percentText == "100") {
						setTimeout(function() {
							SWAL('success', 'Success!', 'Successfully uploaded.');
							$('#import_ibs_modal').modal('hide');
							clearIBS();
						}, 1000);
						setTimeout(function() {
							$('#progressIbsId').fadeOut('slow');
						}, 3000);
					}
				}
			});
		},
		error: function(response, status, e) {
			SWAL('error', 'Opps!', 'Something went wrong.');
			setTimeout(function() {
				$('#progressIbsId').fadeOut('slow');
			}, 3000);
		},
		complete: function(xhr) {
			if (xhr.responseText && xhr.responseText != "error") {
				let json = JSON.parse(xhr.responseText);

				if (json.type == 'error') {
					SWAL(json.type, json.title, json.msg);
					clearIBS();
					$('#progressIbsId').fadeOut('fast');
				} else {
					get_IBS_Track();
				}
			} else {
				SWAL('error', 'Opps!', 'Problem in uploading file.');
				clearIBS();
				$("#progressIbsId").stop();
				setTimeout(function() {
					$('#progressIbsId').fadeOut('slow');
				}, 3000);
			}
		}
	});
});

$(document).on('click', '#' + track_key[4] + 'AddTrack', function() {
	$('#add_' + track_key[4] + '_modal').modal('show');
});

function clearibs() {
	$('#progressBarIbs').width(0);
	$('#submit' + track_key[4] + 'Button, #clear_data_file').prop("disabled", false);
	$('#' + track_key[4] + '_file').val('');
	$('#chk_' + track_key[4] + '_item').prop('checked', false);
	let ctr = 1;
	for (i in ibsData) {
		$('.' + track_key[4] + '_rows-' + ctr).remove();
		ctr++;
	}
	$('.' + track_key[4] + '_site_name, .' + track_key[4] + '_bldg_name, .' + track_key[4] + '_bldg_type, .' + track_key[4] + '_pla_id, .' + track_key[4] + '_latitude, .' + track_key[4] + '_longtitude, .' + track_key[4] + '_region, .' + track_key[4] + '_province, .' + track_key[4] + '_mun_city, .' + track_key[4] + '_address').val('');
	ibsData = [];
}

$(document).on('click', '.append_'+track_key[4]+'_track', function(e){
	e.preventDefault();
	let length = $('#'+track_key[4]+'_table tr').length;
	setIbsToAppend(length);
});

function setIbsToAppend(length) {
	let html = "<tr class="+track_key[4]+"_rows-"+length+">" +
		"<td><textarea name=\"ibs_site_name[]\" id=\"ibs_site_name\" cols=\"30\" rows=\"1\" class=\"form-control ibs_site_name\" placeholder=\"Site Name\" required></textarea></td>" +
		"<td><input type=\"text\" name=\"ibs_bldg_name[]\" class=\"form-control ibs_bldg_name\" id=\"ibs_bldg_name\" placeholder=\"Building Name  \" required></td>" +
		"<td><input type=\"text\" name=\"ibs_bldg_type[]\" class=\"form-control ibs_bldg_type\" id=\"ibs_bldg_type\" placeholder=\"Building Type\" required></td>" +
    "<td><input type=\"text\" name=\"ibs_pla_id[]\" class=\"form-control ibs_pla_id\" id=\"ibs_pla_id\" placeholder=\"PLA ID\" required></td>" +
    "<td><input type=\"text\" name=\"ibs_latitude[]\" class=\"form-control ibs_latitude\" id=\"ibs_latitude\" placeholder=\"Latitude\" required></td>" +
    "<td><input type=\"text\" name=\"ibs_longtitude[]\" class=\"form-control ibs_longtitude\" id=\"ibs_longtitude\" placeholder=\"Longtitude\" required></td>" +
    "<td><input type=\"text\" name=\"ibs_region[]\" class=\"form-control ibs_region\" id=\"ibs_region\" placeholder=\"Region\" required></td>" +
		"<td><input type=\"text\" name=\"ibs_province[]\" class=\"form-control ibs_province\" id=\"ibs_province\" placeholder=\"Province\" required></td>" +
		"<td><input type=\"text\" name=\"ibs_mun_city[]\" class=\"form-control ibs_mun_city\" id=\"ibs_mun_city\" placeholder=\"Municipality City\" required></td>" +
    "<td><input type=\"text\" name=\"ibs_address[]\" class=\"form-control ibs_address\" id=\"ibs_address\" placeholder=\"Address\" required></td>" +
    "<td class='text-center'><button type=\"button\" class=\"btn btn-danger btn-sm mg-left-3 remove_"+track_key[4]+"_track\"><i class=\"fas fa-minus\"></i></button></td>" +
    "</tr>";
	  $('#'+track_key[4]+'_track_tables').append(html);
}

$(document).on('click', '.remove_'+track_key[4]+'_track', function(e){
	e.preventDefault();
	$(this).closest('tr').remove();
});

$(document).on('submit', '#edd_'+track_key[4]+'_Track', function(e){
	e.preventDefault();

	let values = $('input[name="'+track_key[4]+'_site_name[]"]').map(function() {
		return this.value;
	}).toArray();

	let checkExists = !values.every(function(v,i) {
		return values.indexOf(v) == i;
	});

	if (checkExists) {
		SWAL('error', 'Invalid!', 'Duplicate subject name.');
	} else {
		$.ajax({
			url: url + 'save_ibs_track',
			dataType: 'json',
			data: $(this).serialize(),
			type: 'POST',
			success: function(data) {
				SWAL(data.type, data.title,  data.msg);
				if (data.type === 'success') {
          $('#chk_ibs_item').prop('checked', false);
					get_IBS_Track();
					SWAL(data.type, data.title,  data.msg);
					$('.selected').removeClass('selected');
					$('#'+track_key[4]+'AddTrack, #'+track_key[2]+'ImportTrack').removeClass('d-none');
					$('#'+track_key[4]+'EditTrack, #'+track_key[2]+'DelTrack, #'+track_key[2]+'ViewTrack').addClass('d-none');
					$('#ibsTitleTrack').text('Add IBS Track');
					$('#add_'+track_key[4]+'_modal').modal('hide');
          clearIBS();
				}
			},
			error: function(xhr, status, error) {
				errorSwal();
			}
		});
	}
});

$(document).on('click', '#'+track_key[4]+'DelTrack', function() {
  Swal.fire({
		title: 'Are you sure?',
		text: "You want to remove this data.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Delete'
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: url + 'del_ibs_track',
				dataType: 'json',
				data:{id:ibs_id, status : del_ibs_status},
				type: 'POST',
				success: function(data) {
					clearibs();
					SWAL(data.type, data.title,  data.msg);
					get_IBS_Track();
          $('#'+track_key[4]+'AddTrack, #'+track_key[4]+'ImportTrack').removeClass('d-none');
          $('#'+track_key[4]+'EditTrack, #'+track_key[4]+'DelTrack, #'+track_key[4]+'ViewTrack').addClass('d-none');
				},
				error: function(xhr, status, error) {
					errorSwal();
				}
			});
		}
	});
});

$(document).on('click', '#'+track_key[4]+'EditTrack', function() {
  let html = '';
  let ctr = 0;
  if (ibsData.length > 0) {
    $('#'+track_key[4]+'_status').val(1);
    for (i in ibsData) {
      if (ctr <= 0) {
        $('#'+track_key[4]+'-iid').val(ibsData[i].id);
        $('#'+track_key[4]+'_site_name').val(ibsData[i].site_name);
        $('#'+track_key[4]+'_bldg_name').val(ibsData[i].bldg_name);
        $('#'+track_key[4]+'_bldg_type').val(ibsData[i].bldg_type);
        $('#'+track_key[4]+'_pla_id').val(ibsData[i].pla_id);
        $('#'+track_key[4]+'_longtitude').val(ibsData[i].longtitude);
        $('#'+track_key[4]+'_latitude').val(ibsData[i].latitude);
        $('#'+track_key[4]+'_region').val(ibsData[i].region);
        $('#'+track_key[4]+'_province').val(ibsData[i].province);
        $('#'+track_key[4]+'_mun_city').val(ibsData[i].mun_city);
        $('#'+track_key[4]+'_address').val(ibsData[i].address);
      } else {
        html +=
        "<tr class="+track_key[4]+"_rows-"+ctr+">" +
         "<td>" +
          "<input type=\"hidden\" name=\"ibs_iid[]\" class=\"form-control\" value="+ibsData[i].id+">" +
          	"<input type=\"text\" name=\"ibs_site_name[]\" class=\"form-control ibs_site_name\" id=\"ibs_site_name\" placeholder=\"Site Name\" value="+ibsData[i].site_name+" required>" +
          "</td>" +
       		"<td><input type=\"text\" name=\"ibs_bldg_name[]\" class=\"form-control ibs_bldg_name\" id=\"ibs_bldg_name\" placeholder=\"Building Name\" value="+ibsData[i].bldg_name+" required></td>" +
       		"<td><input type=\"text\" name=\"ibs_bldg_type[]\" class=\"form-control ibs_bldg_type\" id=\"ibs_bldg_type\" placeholder=\"Building Type\" value="+ibsData[i].bldg_type+" required></td>" +
          "<td><input type=\"text\" name=\"ibs_pla_id[]\" class=\"form-control ibs_pla_id\" id=\"ibs_pla_id\" placeholder=\"PLA ID\"  value="+ibsData[i].pla_id+" required></td>" +
          "<td><input type=\"text\" name=\"ibs_longtitude[]\" class=\"form-control ibs_longtitude\" id=\"ibs_longtitude\" value="+ibsData[i].longtitude+" placeholder=\"Longtitude\" required></td>" +
          "<td><input type=\"text\" name=\"ibs_latitude[]\" class=\"form-control ibs_latitude\" id=\"ibs_latitude\" placeholder=\"latitude\" value="+ibsData[i].latitude+" required></td>" +
          "<td><input type=\"text\" name=\"ibs_region[]\" class=\"form-control ibs_region\" id=\"ibs_region\" placeholder=\"Region\" value="+ibsData[i].region+" required></td>" +
       		"<td><input type=\"text\" name=\"ibs_province[]\" class=\"form-control ibs_province\" id=\"ibs_province\" value='"+ibsData[i].province+"' placeholder=\"Province\" required></td>" +
       		"<td><input type=\"text\" name=\"ibs_mun_city[]\" class=\"form-control ibs_mun_city\" id=\"ibs_mun_city\" placeholder=\"Municipality City\" required value='"+ibsData[i].mun_city+"'></td>" +
          "<td><input type=\"text\" name=\"ibs_address[]\" class=\"form-control ibs_address\" id=\"ibs_address\" placeholder=\"Address\"  value='"+ibsData[i].address+"'></td>" +
          "<td class='text-center'><button type=\"button\" class=\"btn btn-danger btn-sm mg-left-3 remove_"+track_key[4]+"_track\"><i class=\"fas fa-minus\"></i></button></td>" +
         "</tr>";
      }
      ctr++;
    }
    $('#'+track_key[4]+'_track_tables').append(html);
    $('#add_'+track_key[4]+'_modal').modal('show');
  } else {
    SWAL('error', 'Opps!', 'Please check on the data listed.');
  }
});

$(document).on('click', '.'+track_key[4]+'_close', function() {
  let ctr = 1;
  for (i in ibsData) {
    $('.'+track_key[4]+'_rows-'+ctr).remove();
    ctr++;
  }
});

$(document).on('click', '#' + track_key[4] + 'ViewTrack', function() {
	let html = '';
	let ctr = 1;
	for (i in ibsData) {
		let col = (ctr <= 1) ? 4 : 1;
		html += "<tr>" +
			"<td><small>" + ctr + ".</small></td>" +
			"<td><small>" + ibsData[i].site_name + "</small></td>" +
			"<td><small>" + ibsData[i].bldg_name + "</small></td>" +
			"<td><small>" + ibsData[i].bldg_type + "</small></td>" +
			"<td><small>" + ibsData[i].pla_id + "</small></td>" +
			"<td><small>" + ibsData[i].latitude + "</small></td>" +
			"<td><small>" + ibsData[i].longtitude + "</small></td>" +
			"<td><small>" + ibsData[i].region + "</small></td>" +
			"<td><small>" + ibsData[i].province + "</small></td>" +
			"<td><small>" + ibsData[i].mun_city + "</small></td>" +
			"<td><small>" + ibsData[i].address + "</small></td>" +
			"</tr>";
		ctr++;
	}

	$('#view_' + track_key[4] + '_build').html(html);
	$('#viewIbsModal').modal('show');
});

function clearIBS() {
	$('#progressBarIbs').width(0);
	$('#submit'+track_key[4]+'Button, #clear_data_file').prop("disabled", false);
	$('#'+track_key[4]+'_file').val('');
	$('#chk_'+track_key[4]+'_item').prop('checked', false);
	let ctr = 1;
  for (i in ibsData) {
    $('.'+track_key[4]+'_rows-'+ctr).remove();
		ctr++;
  }
  $('.' + track_key[4] + '_site_name, .' + track_key[4] + '_bldg_name, .' + track_key[4] + '_bldg_type, .' + track_key[4] + '_pla_id, .' + track_key[4] + '_latitude, .' + track_key[4] + '_longtitude, .' + track_key[4] + '_region, .' + track_key[4] + '_province, .' + track_key[4] + '_mun_city, .' + track_key[4] + '_address').val('');
  ibsData = [];
}
