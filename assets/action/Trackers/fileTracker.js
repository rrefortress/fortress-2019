let id = 0, length = 0, total_length = 0;
let subject_name = "", date_end = "", date_ass = "", track_name = "", status = "", remarks = "", assignee = "", name = "";
let bgcolors = ['#43a047', '#fdd835', '#e53935', '#8e24aa', '#6d4c41', '#f4511e', '#fdd835'];
let view_data = [];

$(document).ready(function () {
	$('[data-toggle="tooltip"]').tooltip();
	getTracks();
});

function getTracks() {
	$.ajax({
		url: url + 'get_tracks',
		dataType: 'json',
		data: {row_page: 10},
		type: 'GET',
		beforeSend : function (){
			$('.loader').removeClass('d-none');
		},
		success: function(data) {
			$('.'+track_key[0]+'Tracks .row').removeClass('d-none');
			$('.loader').addClass('d-none');
			$('.t-tracks').removeClass('d-none');

			setNominations(data.nominations);
			setAWE(data.awe);
			setPMO(data.pmo);
			setSurvey(data.surveys);
			setIBS(data.IBS);
			setTssr(data.TSSR);

			setLocations(data.surveys.coordinates, 'track_map');
		},
		error: function(xhr, status, error) {
			errorSwal();
		}
	});
}
