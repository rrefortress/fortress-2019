let data = [];
let ids = [];
let serial_data = []
let item_data = [];

$(function () {
	$('.datepicker').datepicker({
			todayHighlight: true,
			"setDate": new Date(),
      "autoclose": true
	});

	get_items();
	get_requested_lists();
});

$(document).on('click', '#btnAddInventory', function () {
	// e.preventDefault();
	$('.modal_title ').text('Add Item');
	let year = new Date().getFullYear();
	let random = Math.floor(Math.random()*90000) + 10000;
	$('#name, #quantity, .uploadFile, #id').val('');
	$('.imagePreview').attr('src', url + './style/images/no_product.png');
	$('#item_code').val(year + '-' + random + '-' + 'ITEM');
	$('#inventory_modal').modal('show');
});

$(document).on('click', '#btnImportInventory', function (e) {
	e.preventDefault();
	$('#import_view_modal').modal('show');
});

$(document).one("change",".uploadFile", function() {
	$('.imagePreview').attr("src", "");
	let uploadFile = $(this);
	let files = !!this.files ? this.files : [];
	if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

	if (/^image/.test( files[0].type)){ // only image file
		let reader = new FileReader(); // instance of the FileReader
		reader.readAsDataURL(files[0]); // read the local file

		reader.onloadend = function(){ // set image data as background of div
			$('.imagePreview').attr("src", this.result);
			$('#image_profile').val(this.result);
		}
	}
});

$(document).on('submit', '#save_item', function (e) {
	$.ajax({
		url: url + 'save_item',
		type: "post",
		data:new FormData(this),
		processData:false,
		contentType:false,
		cache:false,
		async:false,
		dataType: 'json',
		success: function (response) {
			get_items();
			SWAL('success', "SUCCESS!", "account successfully updated.");
			$('#inventory_modal').modal('hide');
			$('#name, #quantity, .uploadFile, #id').val('');
			$('.imagePreview').attr('src', url + './style/images/no_product.png');
			clearItems();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			SWAL('error', "Oops!", "something went wrong.");
		}
	});
	e.preventDefault();
});

$(document).on('change', '#chk_item', function () {
	$(".check-tbl-item").prop('checked', !!$(this).is(":checked"));
	$('#save_to_cart').prop('disabled', !$(this).is(":checked"));
	let total_length = $('#table_item_lists tr').length;
	ids = [];
	data = [];
	if ($(this).is(":checked")) {
		$('.btn-options').removeClass('d-none');
		$('.btn-add').addClass('d-none');
		for (i = 1; i <= total_length; i++) {
			let row = $('.row-items-'+i);
			let id = row.closest("tr").find('td:eq(0)').attr('row_id');
			let item_code = row.closest("tr").closest("tr").find('td:eq(2)').text();
			let image = row.closest("tr").find('td:eq(3)').attr('row_image');
			let name = row.closest("tr").find('td:eq(4)').text();
			let qty = row.closest("tr").find('td:eq(5)').text();
			let row_serials = row.closest("tr").find('td:eq(0)').attr('row_serials');
			data.push({id:id, item_code: item_code, image: image, name: name, qty: qty});
			ids.push(id);
		}
	} else {
		$('.btn-add').removeClass('d-none');
		$('.btn-options').addClass('d-none');
	}
});

$(document).on('click', '.check-tbl-item', function () {
	let chk = $('#chk_item');
	let total_checked = $('#table_item_lists').find('.check-tbl-item:checked').length;
	let total_length = $('#table_item_lists tr').length;
	chk.prop('checked', total_checked === total_length);

	if (total_checked === total_length) {
		$('#save_to_cart').prop('disabled', !chk.is(":checked"));
	} else {
		$('#save_to_cart').prop('disabled', total_checked === 0);
	}

	if (total_checked === 0) {
		$('.btn-add').removeClass('d-none');
		$('.btn-options').addClass('d-none');
	} else {
		$('.btn-options').removeClass('d-none');
		$('.btn-add').addClass('d-none');
	}

	let id = $(this).closest("tr").find('td:eq(0)').attr('row_id');
	let item_code = $(this).closest("tr").find('td:eq(2)').text();
	let image = $(this).closest("tr").find('td:eq(3)').attr('row_image');
	let name = $(this).closest("tr").find('td:eq(4)').text();
	let qty = $(this).closest("tr").find('td:eq(5)').text();
	let row_serials = $(this).closest("tr").find('td:eq(0)').attr('row_serials');

	if ($(this).is(":checked")) {
		data.push({id:id, item_code: item_code, image: image, name: name, qty: qty, row_serials: row_serials});
	} else {
		let id = $(this).closest('tr').attr('row-id');
		_.remove(data, function(e) {
			return e.id === id
		});
	}

	if ($(this).is(":checked")) {
		ids.push(id);
	} else {
		// ids.pop($(this).closest('tr').attr('row-id'));
		let id = $(this).closest('tr').attr('row-id');
		_.remove(ids, function(e) {
			return e === id
		});
	}
});

$(document).on('change', '#chk_item_req', function () {
	$(".check-tbl-item-req").prop('checked', !!$(this).is(":checked"));
	$('#return_item').prop('disabled', !$(this).is(":checked"));
	let total_length = $('#table_requests_lists tr').length;
	ids = [];
	data = [];
	if ($(this).is(":checked")) {
		for (i = 1; i <= total_length; i++) {
			let row = $('.row-req-'+i);
			let id = row.closest("tr").find('td:eq(0)').attr('row_id-req');
			let qty = row.closest("tr").find('td:eq(0)').attr('row_qty');
			data.push({id:id, qty: qty});
		}
	}
});

$(document).on('click', '.check-tbl-item-req', function () {
	let chk = $('#chk_item_req');
	let total_checked = $('#table_requests_lists').find('.check-tbl-item-req:checked').length;
	let total_length = $('#table_requests_lists tr').length;
	chk.prop('checked', total_checked === total_length);

	if (total_checked === total_length) {
		$('#return_item').prop('disabled', !chk.is(":checked"));
	} else {
		$('#return_item').prop('disabled', total_checked === 0);
	}

	let id = $(this).closest("tr").find('td:eq(0)').attr('row_id-req');
	let qty = $(this).closest("tr").find('td:eq(0)').attr('row_qty');
	if ($(this).is(":checked")) {
		data.push({id:id, qty: qty});
	} else {
		let datas = $(this).closest('tr').attr('row-id');
		_.remove(data, function(e) {
			return e.id === datas
		});
	}
});

$(document).on('click', '#return_item', function () {
	Swal.fire({
		title: 'Return Request?',
		text: "You won't be able to revert this!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes',
		// reverseButtons: true
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: url + 'return_item',
				dataType: 'json',
				data:{data:data},
				type: 'POST',
				success: function(data) {
					SWAL(data.type, data.title,  data.msg);
					get_items();
					get_requested_lists();
					clearItems();
				},
				error: function(xhr, status, error) {
					errorSwal();
				}
			});
		}
	})
});

$(document).on('click', '#btnEditInventory', function () {
	$('.modal_title ').text('Edit Item');
	if (data.length > 0 ) {
		if (data.length === 1) {
			$('#id').val(data[0].id);
			$('.imagePreview').attr('src', data[0].image);
			//ERROR: UPDATE IMAGE NOT WORKING
			// $('.uploadFile').val(data[0].image);
			$('#item_code').val(data[0].item_code);
			$('#name').val(data[0].name);
			$('#quantity').val(data[0].qty);

			let sr = [];
			sr = data[0].row_serials.split(",");
			let ctr = 1;
			let html = '';
			for (a in sr ) {
					html += '<div class="form-group">' +
										'<input type="text" name="serial[]" class="form-control form-control-sm" placeholder="Enter Serial Number #'+ctr+'" value="'+sr[a]+'" required>' +
									'</div>';
					ctr++;
			}
			$('.serial_append').html(html);

			$('#inventory_modal').modal('show');
		} else {
			let msg = 'Please check only one item to edit.';
			SWAL('error', 'Invalid!', msg);
		}
	}
});

$(document).on('click', '#save_to_cart', function () {
	let html = '';
	if (data.length > 0) {
		for (i in data) {
			let image = data[i].image === '' ? url + './style/images/no_product.png' : data[i].image;
			html += '<div class="row selected_items" row-id='+i+'>' +
						'<div class="col-4">' +
							'<img src='+image+' height="105" width="120" class="border imagePreview vertical-center text-center">' +
						'</div>' +
						'<div class="col-8">' +
							// '<div class="form-group">' +
								// '<div class="form-group">' +
									'<label for="username" class="text-uppercase text-muted">'+data[i].item_code+'</label><br>' +
									'<label for="username" class="text-uppercase text-muted">'+$.trim(data[i].name)+'</label>' +

									'<div class="row">' +
										'<div class="col-9">' +
											'<input type="text" name="quantities[]" class="form-control quantities" id="qty-'+i+'" placeholder="Enter number of quantity" required>' +
											'<input type="hidden" name="id[]" class="form-control text-center" value='+data[i].id+' id="id-'+i+'">' +
										'</div>' +
										'<div class="col-3">' +
											'<input type="hidden" class="form-control text-center" value='+data[i].qty+' id="origStock-'+i+'">' +
											'<input type="number" name="stocks[]" class="form-control text-center" value='+data[i].qty+' id="stocks-'+i+'" placeholder="Enter item name" readonly>' +
										'</div>' +
									// '</div>' +

								// '</div>' +
							'</div>' +
						'</div>' +
					'</div>' +
					'<hr>';
		}
	}

	$('.appendItems').html(html);
	$('#cart_preview_modal').modal('show');
});
let status = false;
$(document).on('keyup', '.quantities:input', function() {
	let ctr = $(this).parent().parent().parent().parent().attr('row-id');
	let qty = $('#qty-'+ctr).val() == '' ? 0 : $('#qty-'+ctr).val();
	let stocks = $('#stocks-'+ctr).val();
	let origStock = $('#origStock-'+ctr).val();

	if (parseInt(qty) > parseInt(stocks)) {
		$('#qty-'+ctr).addClass('is-invalid');
		$('#stocks-'+ctr).val(origStock);
		status = true;
	} else {
		let total = parseInt(stocks) - parseInt(qty);
		qty = qty === 0 ? origStock : total;
		$('#qty-'+ctr).removeClass('is-invalid');
		$('#stocks-'+ctr).val(qty);
		status = false;
	}
});

$(document).on('click', '#btnDelInventory', function () {
		Swal.fire({
			title: 'Are you sure?',
			text: "You want to delete this item/s",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: url + 'del_items',
					type: "post",
					data:{id:ids},
					dataType: 'json',
					success: function (response) {
						get_items();
						SWAL('success', "SUCCESS!", "account successfully deleted.");
						clearItems();
					},
					error: function(jqXHR, textStatus, errorThrown) {
						errorSwal();
						clearItems()
					}
				});
			}
		});
});

$(document).on('submit', '#save_append_items', function (e) {
	if (status) {
		SWAL('error', "Invalid!", "# of input/s is greater total quantity.");
	} else {
		$.ajax({
			url: url + 'save_to_cart',
			dataType: 'json',
			data: $(this).serialize(),
			type: 'POST',
			success: function(data) {
				SWAL(data.type, data.title,  data.msg);
				if (data.type === 'success') {
					$('#cart_preview_modal').modal('hide');
					get_items();
					clearItems();
				}
			},
			error: function(xhr, status, error) {
				errorSwal();
				clearItems();
			}
		});
	}
	e.preventDefault();
});

$(document).on('keypress', '.quantities', function (e) {
	if(e.which < 46 || e.which >= 58 || e.which == 47) {
		event.preventDefault();
	}

	if(e.which == 46 && $(this).val().indexOf('.') != -1) {
		this.value = '' ;
	}
});

$(document).on('click', '#view_cart', function () {
	get_item_cart();
	$('#cart_view_modal').modal('show');
});

$(document).on('submit', '#save_requests_items', function (e) {
	e.preventDefault();
	Swal.fire({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes',
		// reverseButtons: true
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: url + 'save_to_requests',
				dataType: 'json',
				data: $(this).serialize(),
				type: 'POST',
				success: function(data) {
					$('#cart_view_modal').modal('hide');
					SWAL(data.type, data.title,  data.msg);
					get_items();
					get_requested_lists();
					clearItems();
				},
				error: function(xhr, status, error) {
					errorSwal();
					clearItems();
				}
			});
		}
	})
});

$(document).on('keyup', '#quantity', function(e) {
	e.preventDefault();
	append_serial($(this).val());
});

function  append_serial(num = 0, val = '') {
	let html = '';
	let ctr = 1;
	for (i = 0; i < num; i++) {
		html += '<div class="form-group">' +
							'<input type="text" name="serial[]" class="form-control form-control-sm" placeholder="Enter Serial Number #'+ctr+'" value="'+val+'" required>' +
						'</div>';
		ctr++;
	}
	$('.serial_append').html(html);
}

$(document).on('keyup', '#search_items', function(e) {
	e.preventDefault();
	get_items($(this).val());
});

$(document).on('change', '#inventory_status', function(e) {
	e.preventDefault();
	get_requested_lists($(this).val());
});

$(document).on('keyup', '#search_req_items', function(e) {
	e.preventDefault();
	get_requested_lists($(this).val());
});

$(function() {
		$('#date_select').datepicker({format: 'yyyy-mm-dd',
		}).on('changeDate', function(e) {
    		console.log($(this).val());
				get_requested_lists('', $(this).val());
		});
});

$(document).on('click', '#remove_cart_lists', function(e) {
	e.preventDefault();
	let id = $(this).attr('cart-id');

	Swal.fire({
		title: 'Remove from list?',
		text: "You won't be able to revert this!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes',
		// reverseButtons: true
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: url + 'remove_cart_lists',
				dataType: 'json',
				data: {id: id},
				type: 'POST',
				success: function(data) {
					get_items();
					get_item_cart();
				},
				error: function(xhr, status, error) {
					errorSwal();
					clearItems();
				}
			});
		}
	})
});

function get_items(search = "") {
	$.ajax({
		url: url + 'get_items',
		type: "post",
		async:false,
		data: {search: search},
		dataType: 'json',
		beforeSend() {
			let html = '<div class="content">' +
				'<h5 class="text-center text-white mg-top-135">' +
				'<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>' +
				'<h4 class="text-dark text-center">Loading</h4>' +
				'</h5>' +
				'</div>';
			$('#table_item_lists').html(html);
		},
		success: function (data) {
			let html = '';
			if (data.length > 0)
			{
				let ctr = 1;
				for (i in data)
				{
					let sr = [];
					let image = data[i].image === '' ? url + './style/images/no_product.png' : data[i].image;
					// let sr = JSON.stringify(data[i].serials);
					for (o in data[i].serials) {
						sr.push(data[i].serials[o].serial_number);
					}

					html += '<tr class="border-bottom" row-id='+data[i].id+'>' +
						'<td class="row-items-'+ctr+'" row_id='+data[i].id+' row_serials="'+sr+'">' +
						'<div class="custom-control pad-left-rem custom-checkbox chk-'+i+'">' +
						'<input type="checkbox" name="table2_lists" class="custom-control-input check-tbl-item" id="check-item-'+i+'">' +
						'<label class="custom-control-label" for="check-item-'+i+'"></label>' +
						'</div>' +
						'</td>' +
						'<td class="text-center">'+ctr+'.</td>' +
						'<td class="text-center wy-text-small"><small>'+data[i].item_code+'</small></td>' +
						'<td row_image='+image+' align="center"><img src='+image+' class="rounded shadow-sm border" height="50" width="50" ></td>' +
						'<td class="text-center">'+$.trim(data[i].name)+'</td>' +
						'<td class="text-right">'+data[i].quantity+'</td>' +
						'</tr>';
					ctr++;
				}


			}
			else
			{
				html = '<div class="content">' +
					'<h5 class="text-center text-white">' +
					'<i class="fas fa-shopping-cart display-1 text-dark"></i><br>' +
					'<h4 class="text-dark text-center">Empty Data.</h4>' +
					'<label class="text-dark text-center">No data has been added yet.</label>' +
					'</h5>' +
					'</div>';
			}

			$('#inventory_modal').modal('hide');
			$('#name, #quantity').val('');
			$('#table_item_lists').html(html);
			$('#chk_item').prop('checked', false).prop('disabled', data.length > 0 ? false : true);
			$('#save_to_cart').prop('disabled', true);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			SWAL('error', "Oops!", "something went wrong.");
		}
	});
}

function get_item_cart() {
	$.ajax({
		url: url + 'get_item_cart',
		type: "get",
		async:false,
		dataType: 'json',
		beforeSend() {
			let html = '<div class="content">' +
				'<h5 class="text-center text-white mg-top-135">' +
				'<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>' +
				'<h4 class="text-dark text-center">Loading</h4>' +
				'</h5>' +
				'</div>';
			$('#cart_items').html(html);
		},
		success: function (data) {
			let html = '';
			if (data.length > 0)
			{
				for (i in data) {
					let image = data[i].image === '' ? url + './style/images/no_product.png' : data[i].image;
					html += '<div class="row" >' +
								'<div class="col-3">' +
									'<img src='+image+' height="100" width="100" class="border vertical-center text-center shadow-sm rounded border">' +
								'</div>' +
								'<div class="col-8">' +
									// '<div class="form-group">' +
										// '<div class="form-group">' +
											'<label for="username" class="text-uppercase text-muted"><small>'+data[i].item_code+'</small></label><br>' +
											'<label for="username" class="text-uppercase text-muted"><small>'+$.trim(data[i].name)+'</small></label>' +
											'<input type="hidden" name="id[]" class="form-control text-center" value='+data[i].id+' id="id-'+i+'">' +
											'<div class="row">' +
												'<div class="col-9">' +
													'<label class="text-uppercase text-muted">Quantity: <b><small>'+data[i].qty+'</small></b></label>'+
												'</div>' +
												'<div class="col-3">' +
													'<button type="button" id="remove_cart_lists" cart-id='+data[i].id+' class="btn btn-danger btn-sm rounded-circle" data-toggle="tooltip" data-placement="top" title="Remove from list"><i class="fas fa-minus"></i></button>' +
												'</div>' +
											// '</div>' +

										// '</div>' +
									'</div>' +
								'</div>' +
							'</div>' +
							'<hr>';
				}

				$('.modal-cart-footer, #purpose').removeClass('d-none');
			}
			else
			{
				html = '<h5 class="text-center text-white">' +
						'<i class="fas fa-shopping-cart display-1 text-dark"></i><br>' +
						'<h4 class="text-dark text-center">Empty Cart</h4>' +
						'</h5>';
				$('.modal-cart-footer, #purpose').addClass('d-none');
			}
			$('#cart_items').html(html);
			$('[data-toggle="tooltip"]').tooltip();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			SWAL('error', "Oops!", "something went wrong.");
		}
	});
}

function get_requested_lists(search = "", date = "") {
	$.ajax({
		url: url + 'get_requested_lists',
		type: "post",
		async:false,
		dataType: 'json',
		data: {search: search, date: date},
		beforeSend() {
			let html = '<div class="content">' +
				'<h5 class="text-center text-white mg-top-135">' +
				'<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>' +
				'<h4 class="text-dark text-center">Loading</h4>' +
				'</h5>' +
				'</div>';
			$('#table_requests_lists').html(html);
		},
		success: function (data) {
			let html = '';
			let chkItems = [];
			let items = [];

			if (data.length > 0)
			{
				let ctr = 1;
				for (i in data)
				{
					let image = data[i].image === '' ? url + './style/images/no_product.png' : data[i].image;
					let status = data[i].item_status == 2 ? 'pending' : data[i].item_status == 3 ? 'accepted' : data[i].item_status == 5 ? 'returned' : 'rejected';
					let color  = data[i].item_status == 2 ? 'warning' : data[i].item_status == 3 ? 'success' : data[i].item_status == 5 ? 'info' : 'danger';

					chkItems.push({ctr: i, status: status});

					html += '<tr class="border-bottom" row-id='+data[i].id+'>' +
						'<td class="row-req-'+ctr+'" row_id-req='+data[i].id+' row_qty='+data[i].qty+'>';
							if (status !== 'pending') {
								html += '<div class="custom-control pad-left-rem custom-checkbox chk-'+i+'">' +
								'<input type="checkbox" name="table2_lists" class="custom-control-input check-tbl-item-req" id="check-item-req-'+i+'">' +
								'<label class="custom-control-label" for="check-item-req-'+i+'"></label>' +
								'</div>';
							}

						html += '</td>' +
						'<td class="text-center">'+ctr+'.</td>' +
						'<td class="row-'+ctr+'" row_id-req='+data[i].id+'>' +
							'<div class="col-12">' +
								'<div class="row" >' +
									'<div class="col-5">' +
										'<img src='+image+' height="90" width="90" class="border vertical-center text-center shadow-sm rounded border">' +
									'</div>' +
									'<div class="col-7">' +
										// '<div class="form-group ">' +
											// '<div class="form-group">' +
												'<h6 for="username" class="text-uppercase text-muted"><small>'+data[i].item_code+'</small></h6>' +
												'<label for="username" class="text-uppercase text-muted"><small>'+$.trim(data[i].name)+'</small></label>' +
												'<input type="hidden" name="id[]" class="form-control text-center" value='+data[i].id+' id="id-'+i+'">' +
												// '<div class="row">' +
												// 	'<div class="col-9">' +
														'<h6 class="text-uppercase text-muted mt-0"><small>Quantity:</small> <b><small>'+data[i].qty+'</small></b></h6>'+
												// 	'</div>' +
												// '</div>' +

											// '</div>' +
										// '</div>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</td>' +
						'<td class="text-right"><span class="badge badge-'+color+' text-uppercase text-white">'+status+'</span></td>' +
						'<td class="text-right"><small>'+data[i].request_date+'</small></td>' +
						'</tr>';
					ctr++;

					let item_serials = [];

					for (o in data[i].serials) {
						item_serials += data[i].serials[o].serial_number + ", ";
						// item_serials.push(serial);
					}

					item_data.push({
						'Name' 						   :  $.trim(data[i].name),
						'Item Status'        :  _.startCase(_.toLower(status)),
						'Item Code'       	 :  data[i].item_code,
						'Quantity'			 		 :  data[i].qty,
						'Requested Data'		 :  data[i].request_date,
						'Serial Number'      :  item_serials
					});
				}
			}
			else
			{
				html = '<div class="content">' +
					'<h5 class="text-center text-white">' +
					'<i class="fas fa-clipboard-list display-1 text-dark"></i><br>' +
					'<h4 class="text-dark text-center">Empty Data.</h4>' +
					'<label class="text-dark text-center">No data has been selected yet.</label>' +
					'</h5>' +
					'</div>';
			}

			$('#table_requests_lists').html(html);
			$('#chk_item_req').prop('checked', false).prop('disabled', data.length > 0 ? false : true);

			if (chkItems.length > 0) {
				for (i in chkItems) {
					$('#check-item-req').prop('disabled', chkItems[i].status == 'pending' ? true : false);
				}
			}

			// $('#chk_item_req').addClass('d-none');
		},
		error: function(jqXHR, textStatus, errorThrown) {
			SWAL('error', "Oops!", "something went wrong.");
		}
	});
}

$(document).on('click', '#export_as_excel', function() {
	let today = moment().format("DD/MM/YYYY");
	if (item_data.length > 0) {
		JSONToCSVConvertor(item_data, "Item_Invtory" + "("+today+")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

function clearItems() {
	ids = [];
	data = [];
	$('.btn-add').removeClass('d-none');
	$('.btn-options').addClass('d-none');
	$('.serial_append').html('');
}
