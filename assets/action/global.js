let loaders = 'assets/style/Spin-1s-200px.svg';
let port = ':3308';
let hostname = window.location.hostname;
let color = ['#4caf50', '#ffff00', '#e53935'];
let borderColor = ['#ffffff'];
let track_key = ['nom', 'awe', 'pmo', 'survey', 'ibs', 'tssr'];
let msg = $('#message');

Chart.defaults.global.defaultFontColor = '#fffcanvasSurvey!important';
Chart.defaults.global.defaultFontSize = 10;
Chart.defaults.scale.ticks.beginAtZero = true;
Chart.defaults.global.animation;

let optionsPie = {
  title    : setTitle("Pie Chart"),
  plugins  : setPlugins(),
  elements : centerText("Clutter Type"),
  rotation : -0.7 * Math.PI,
};

let optionsBar = {
  title    : setTitle("Bar Graph"),
  plugins  : setPlugins(),
  rotation : -0.7 * Math.PI,
  legend: {
      display: false
  },
};

$(function () {
	$('[data-toggle="tooltip"]').tooltip();
	sessionStorage.clear();
	read_cookies();
	let i = "Stop!",
		j = "This is a browser feature intended for Fortress developers only. ";

	if ((window.chrome || window.safari)) {
		let l = 'font-family:helvetica; font-size:20px; ';
		[
			[i, l + 'font-size:50px; font-weight:bold; ' + 'color:red; -webkit-text-stroke:1px black;'],
			[j, l],
		].map(function(r) {
			setTimeout(console.log.bind(console, '\n%c' + r[0], r[1]));
		});
	}
});

function renderButton() {
	gapi.signin2.render('gSignIn', {
		'scope': 'profile email',
		'height': 35,
		'longtitle': true,
		'theme': 'dark',
		'onsuccess': onSuccess,
		'onfailure': onFailure
	});
}

function onSuccess(googleUser) {
	gapi.client.load('oauth2', 'v2', function () {
		var request = gapi.client.oauth2.userinfo.get({
			'userId': 'me'
		});
		request.execute(function (resp) {
			let data = {
				auth_id : resp.id,
				firstname : resp.given_name,
				lastname : resp.family_name,
				email : resp.email,
				profile: resp.picture
			};
			social_login(data, googleUser);
		});
	});

}

// Sign-in failure callback
function onFailure(error) {
	// errorSwal();
	console.log('Cancel signin');
}

$(document).on('submit', '#login', function (e) {
	e.preventDefault();
	let user = $('#username').val();
	let pass = $('#password').val();
	let btn = $('.btn-login');
	let msg = $('#message');
	let param = {
		url: url + 'sign_in',
		dataType: 'json',
		type: 'POST',
		data: {username: user, password: pass},
		beforeSend: btn.prop('disabled', true).html('<i class="fas fa-spinner fa-pulse"></i> Signing in')
	};

	fortress(param).then( (data) => {
		msg.html('');
		btn.prop('disabled', false);
		if (data.type === 'success') {
			// if (data.logs !== null) {
			// 	emit_socket(data.logs);
			// 	emit_active(data);
			// }
			window.location.href = url + 'login';
		} else {
			msg.html('<div class="alert alert-danger mg-top-10 text-center small" role="alert">\n' +
				'\t\t\t\t\t\t\t\t\t<i class="fas fa-exclamation-triangle"></i> '+data.message+'\n' +
				'\t\t\t\t\t\t\t\t</div>');
		}
		btn.html('Submit');
	});
});

$(document).on('click', '#signout', function (e) {
	e.preventDefault();
	let param = {
		url: url + 'sign_out',
		dataType: 'json',
		type: 'GET',
	};

	fortress(param).then( (data) => {
		if (data.type === 'success') {
			// if (data.logs !== null) {
			// 	emit_socket(data.logs);
			// 	emit_active(data);
			// }
			gapi.auth.signOut();
			window.location.href = url + 'login';
		}
	});
});

function social_login(data, user) {
	let param = {
		url: url + 'social_login',
		dataType: 'json',
		type: 'POST',
		data: data,
	};

	fortress(param).then( (datas) => {
		msg.html('');
		if (datas.type === 'success') {
			// if (data.logs !== null) {
			// 	emit_active(data);
			// }

			msg.html('<div class="alert alert-success mg-top-10 text-center small" role="alert">\n' +
				'\t\t\t\t\t\t\t\t\t<i class="fas fa-check-circle"></i> Successfully login' +
				'\t\t\t\t\t\t\t\t</div>');
			user.disconnect();	
			window.location.href = url + 'login';
		} else {
			SWAL(datas.type, datas.title, datas.message);
		}
	});
}

function errorSwal() {
	Swal.fire({
		title: 'Opps!',
		text: "Something went wrong please try again.",
		type: 'error',
		showCancelButton: false,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Ok'
	}).then((result) => {
		if (result.value) {
			location.reload(true);
		}
	});
}

Chart.pluginService.register({
	beforeDraw: function (chart) {
		if (chart.config.options.elements.center) {
			//Get ctx from string
			var ctx = chart.chart.ctx;

			//Get options from the center object in options
			var centerConfig = chart.config.options.elements.center;
			var fontStyle = centerConfig.fontStyle || 'Arial';
			var txt = centerConfig.text;
			// var color = centerConfig.color || '#000';
			var sidePadding = centerConfig.sidePadding || 8;
			var sidePaddingCalculated = (sidePadding/100) * (chart.innerRadius * 2);
			//Start with a base font of 30px
			ctx.font = "35px " + fontStyle;

			//Get the width of the string and also the width of the element minus 10 to give it 5px side padding
			var stringWidth = ctx.measureText(txt).width;
			var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

			// Find out how much the font can grow in width.
			var widthRatio = elementWidth / stringWidth;
			var newFontSize = Math.floor(30 * widthRatio);
			var elementHeight = (chart.innerRadius * 2);

			// Pick a new font size so it will not be larger than the height of label.
			var fontSizeToUse = Math.min(newFontSize, elementHeight);

			//Set font settings to draw it correctly.
			ctx.textAlign = 'center';
			ctx.textBaseline = 'middle';
			var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
			var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
			ctx.font = fontSizeToUse+"px " + fontStyle;
			ctx.fillStyle = color;

			//Draw text in centera
			ctx.fillText(txt, centerX, centerY);
		}
	}
});

function clearform(value) {
	$(value).find('input[type=text], input[type=number], input[type=hidden]').val('').removeAttr("disabled");
}

function SWAL(type, title, text) {
	Swal.fire({
		title: title,
		type: type,
		html: text,
	})
}

function getWholePercent(percentFor, percentOf)  {
	return Math.floor(percentFor/percentOf *100);
}

function setTitle(text, subtitle = '') {
	let title = {
		display: true,
		text: [text, subtitle],
		position: 'bottom',
	};
	return title
}

function centerTextGraphs(done, total) {
	let centered = {
		center: {
			text: done + ' / ' + total,
			color: '#FF6384', // Default is #000000
			fontStyle: 'Arial', // Default is Arial
			sidePadding: 20 // Defualt is 20 (as a percentage)
		}
	};
	return centered
}

function centerText(text) {
	let centered = {
		center: {
			text: text,
			color: '#FF6384', // Default is #000000
			fontStyle: 'Arial', // Default is Arial
			sidePadding: 20 // Defualt is 20 (as a percentage)
		}
	};
	return centered
}

function setPlugins(title) {
	let plugins = {
		datalabels: {
			// labels: {
			// 	title: title
			// },
			color: '#fff',
			anchor: 'end',
			align: 'start',
			offset: -10,
			borderWidth: 2,
			borderColor: '#fff',
			borderRadius: 25,
			backgroundColor: (context) => {
				return context.dataset.backgroundColor;
			},
			font: {
				weight: 'bold',
				size: '10'
			},
			formatter: (value) => {
				return value+ '%';
			}
		}
	};
	return plugins;
}

function setPlugins_ssv(title) {
	let plugins = {
		datalabels: {
			color: '#fff',
			anchor: 'end',
			align: 'start',
			offset: -10,
			borderWidth: 2,
			borderColor: '#fff',
			borderRadius: 25,
			backgroundColor: (context) => {
				return context.dataset.backgroundColor;
			},
			font: {
				weight: 'bold',
				size: '12'
			},
			formatter: (value) => {
				return value;
			}
		}
	};
	return plugins;
}

function build_chart(data = "", option = "", ctx = "", total_per = "", type = "", site = "") {
	if(window[site] !== undefined) {
		window[site].destroy();
	}

	window[site] = new Chart(ctx, {
		type: type,
		data: data,
		options: option,
	});
}

function mixed_build_chart(data = "", option = "", ctx = "", total_per = "", type = "", site = "") {
	if(window[site] !== undefined) {
		window[site].destroy();
	}

	window[site] = new Chart(ctx, {
		type: type,
		data: data,
		options: option,
	});
}

function nFormatter(num) {
   if (num >= 1000000000) {
      return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
   }
   if (num >= 1000000) {
      return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
   }
   if (num >= 1000) {
      return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
   }
   return num;
}


// ===== Scroll to Top ====
$(window).scroll(function() {
	if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
		$('#return-to-top').fadeIn(200);    // Fade in the arrow
	} else {
		$('#return-to-top').fadeOut(200);   // Else fade out the arrow
	}
});

$('#return-to-top').click(function() {      // When arrow is clicked
	$('body,html').animate({
		scrollTop : 0                       // Scroll to top of body
	}, 500);
});

$(window).on('load', function(){
	$(".se-pre-con").fadeOut("slow");
});

$(document).on('click', '#remember', function () {
	if ($(this).is(':checked')) {
		let username = $('#username').val();
		let password = $('#password').val();
		$.cookie('username', username, { expires: 14 });
		$.cookie('password', password, { expires: 14 });
		$.cookie('remember', true, { expires: 14 });
	} else {
		$.cookie('username', null);
		$.cookie('password', null);
		$.cookie('remember', null);
	}
});

function read_cookies() {
	let remember = $.cookie('remember');
	if ( remember === 'true' ) {
		var username = $.cookie('username');
		var password = $.cookie('password');
		$('#username').attr("value", username);
		$('#password').attr("value", password);
		$('#remember').attr('checked', true);
	}
}

function AvoidSpace(event) {
	var k = event ? event.which : window.event.keyCode;
	if (k === 32) return false;
}

function formatDate(date) {
	if (date === null || date === undefined) {
		return null
	} else {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2)
			month = '0' + month;
		if (day.length < 2)
			day = '0' + day;

		return [month, day, year].join('-');
	}
}

async function fortress(data) {
	let result;

	try {
		result = await $.ajax({
			url: data.url,
			type: data.type,
			data: data.data,
			dataType: data.dataType,
			beforeSend() {
				data.beforeSend;
			}
		});

		return result;
	} catch (error) {
		errorSwal();
	}
}

// function emit_socket(data) {
// 	let socket = io.connect(hostname+port);
// 	socket.emit('logs', {
// 		id: data.id,
// 		action: data.action,
// 		ago: data.ago,
// 		department: data.department,
// 		geoarea: data.geoarea,
// 		name: data.name,
// 		profile: data.profile
// 	});
// }
//
// function emit_active(data) {
// 	let socket = io.connect(hostname+port);
// 	socket.emit('active_users', {
// 		active: data.active,
// 	});
// }

// function emit_activities(data) {
// 	let socket = io.connect(hostname+port);
// 	socket.emit('activities', {
// 		data: data
// 	});
// }

function commarize(min) {
	min = min || 1e3;
	// Alter numbers larger than 1k
	if (this >= min) {
		var units = ["k", "M", "B", "T"];

		var order = Math.floor(Math.log(this) / Math.log(1000));

		var unitname = units[(order - 1)];
		var num = Math.floor(this / 1000 ** order);

		// output number remainder + unitname
		return num + unitname
	}

	// return formatted original number
	return this.toLocaleString()
}

// Add method to prototype. this allows you to use this function on numbers and strings directly
Number.prototype.commarize = commarize
String.prototype.commarize = commarize