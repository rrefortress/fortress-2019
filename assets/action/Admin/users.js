let id = 0;
let username = "", globe_id = "", email = "", fname = "", lname = "", department = "", geoarea = "", access_level = "", status = "", vendor = "";

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
	pagerefresh();
});

$(document).on('submit', '#SaveUser', function (e) {
	e.preventDefault();
	let param = {
		url: url + 'adduser',
		dataType: 'json',
		data: $(this).serialize(),
		type: 'POST'
	};
	let html = '';
	fortress(param).then( (data) => {
		if (data.error === "error") {
			SWAL(data.type, data.title, data.msg);
		} else {
			$('#addUserModal').modal('hide');
			SWAL(data.type, data.title, data.msg);
			$('#editUser', '#delUser', '#resetUser').addClass('d-none');
			$('#addUser').removeClass('d-none');
			clearData();
			pagerefresh();
		}
		
	});
});

$(document).on('click', '#table_users tbody tr', function (e) {
	e.preventDefault();
	if ($(this).hasClass('selected')) {
		$(this).removeClass('selected');
		$('#editUser, #delUser, #resetUser').addClass('d-none');
		$('#addUser').removeClass('d-none');
		clearData();
	} else {
		$('.selected').removeClass('selected');
		$(this).toggleClass('selected');
		$('#editUser, #delUser, #resetUser').removeClass('d-none');
		$('#addUser').addClass('d-none');

		id = $(this).closest("tr").find('td:eq(0)').data('id');
		username = $(this).closest("tr").find('td:eq(1)').text();
		globe_id = $(this).closest("tr").find('td:eq(2)').text();
		email = $(this).closest("tr").find('td:eq(3)').text();
		fname = $(this).closest("tr").find('td:eq(4)').text();
		lname = $(this).closest("tr").find('td:eq(5)').text();
		department = $(this).closest("tr").find('td:eq(6)').text();
		geoarea = $(this).closest("tr").find('td:eq(7)').text();
		access_level = $(this).closest("tr").find('td:eq(8)').text();
		status = $(this).closest("tr").find('td:eq(9)').text();
		vendor = $(this).closest("tr").find('td:eq(10)').text();
	}
});

$(document).on('click', '#addUser', function (e) {
	e.preventDefault()
	$('.modal_title').text('Add User');
	$('#addUserModal').modal('show');
	$('#optionsEdit').hide();
});

$(document).on('click', '#editUser', function (e) {
	e.preventDefault()
	$('#addUserModal').modal('show');
	$('#optionsEdit').show();
	$('#uid').val(id);
	$('#username').val(username);
	$('#globe_id').val(globe_id);
	$('#email').val(email);
	$('#fname').val(fname);
	$('#lname').val(lname);
	$('#department').val(department);
	$('#geoarea').val(geoarea === 'nat' ? 'national' : geoarea);
	$('#access_level').val(access_level);
	$('#vendor').val(vendor);

	let toggle = status === 'activated' ? 'on' : 'off';
	$('#chk_status').bootstrapToggle(toggle, true);
	$('#status').val(status);

	$('.modal_title').text('Edit User');
});

$(document).on('click', '#delUser', function (e) {
	e.preventDefault();
	Swal.fire({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Delete'
	}).then((result) => {
		if (result.value) {
			let param = {id: id};

			doAjax(param, url + 'delete_user').then((data) => {
				SWAL(data.type, data.title, data.msg);
				pagerefresh();
				clearData();
			});
		}
	})
});
$(document).on('click', '#resetUser', function (e) {
	e.preventDefault();
	Swal.fire({
		title: 'Are you sure?',
		text: "This action cannot be undone.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Reset'
	}).then((result) => {
		if (result.value) {
			let param = {id: id};
			doAjax(param, url + 'reset_password').then((data) => {
				SWAL(data.type, data.title, data.msg);
				pagerefresh();
				clearData();
			});
		}
	})
});

$('#chk_status').change(function() {
	$('#status').val($(this).prop('checked') ? 'activated' : 'deactivated');
});

function clearData() {
	$('#editUser, #delUser, #resetUser').addClass('d-none');
	$('#addUser').removeClass('d-none');
	$('#username, #globe_id, #email, #fname, #lname, #uid, #status').val('');
	$('#department').val('RRE');
	$('#geoarea').val('national');
	$('#access_level').val('Standard');
}

function pagerefresh(){
	$.ajax({
		type: 'POST',
		url: url+'getusers',
		dataType: 'json',
		async: false,
		success: function(data){
			let html = '';
			let count = 1;
		
				html += '<table class="table table-striped table-bordered table-responsive small dt-responsive nowrap small" id="table_users">\n' +
					'\t\t\t<thead>\n' +
					'\t\t\t<tr>\n' +
					'\t\t\t\t<th scope="col" style="width:10% !important;">No.</th>\n' +
					'\t\t\t\t<th scope="col">Username</th>\n' +
					'\t\t\t\t<th scope="col">User ID</th>\n' +
					'\t\t\t\t<th scope="col">Email</th>\n' +
					'\t\t\t\t<th scope="col">Firstname</th>\n' +
					'\t\t\t\t<th scope="col">Lastname</th>\n' +
					'\t\t\t\t<th scope="col">Department</th>\n' +
					'\t\t\t\t<th scope="col">Area</th>\n' +
					'\t\t\t\t<th scope="col">Access Level</th>\n' +
					'\t\t\t\t<th scope="col" style="133px !important;" class="text-center">Status</th>\n' +
					'\t\t\t\t<th scope="col" class="text-center">Vendor</th>\n' +	
					'\t\t\t</tr>\n' +
					'\t\t\t</thead>' +
					'\t\t\t<tbody class="users_list">\n';

				for (i in data) {
					let status = data[i].status === 'activated' ? 'success' : 'danger';
					html += '<tr>' +
						'<td style="text-align: center" data-id='+data[i].id+'>'+count+++'.</td>' +
						'<td style="text-align: center">'+data[i].username+'</td>' +
						'<td style="text-align: center">'+data[i].globe_id+'</td>' +
						'<td style="text-align: center">'+data[i].email+'</td>' +
						'<td style="text-align: center">'+data[i].fname+'</td>' +
						'<td style="text-align: center">'+data[i].lname+'</td>' +
						'<td style="text-align: center">'+data[i].department+'</td>' +
						'<td style="text-align: center" class="text-uppercase">'+data[i].geoarea+'</td>' +
						'<td style="text-align: center">'+data[i].access_level+'</td>' +
						'<td style="text-align: center" class="text-uppercase"><span class="badge badge-'+status+'">'+data[i].status+'</span></td>' +
						'<td style="text-align: center">'+data[i].vendor+'</td>';
					'<tr>';
				}

			
			$('#maindiv').html(html);
			$('#table_users').DataTable({select: true});
			$("#table_users th").addClass('global_color text-white text-uppercase text-center');

			html += '</tbody>' +
			'</table>';
		}
	});

}

async function doAjax(param = '', urls) {
	let result;

	try {
		result = await $.ajax({
			url: urls,
			type: 'POST',
			data: param,
			dataType: 'json',
		});

		return result;
	} catch (error) {
		errorSwal();
	}
}
