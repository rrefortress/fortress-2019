	$(document).ready(function(){

    // $("h4").on('dblclick', function(){
    //     location.reload();
    // });
    //     var detailsafter;
    //     var detailsbefore;
    //     $.ajax({
    //         type: 'POST',
    //         url: url+'dbadjust',
    //         dataType: 'json',
    //         async: false,
    //         data:{
    //                 'updatetype':'add'
    //         },
    //         success: function(data){
    //             console.log('\n'+data);
    //             // detailsafter = JSON.parse(data.detailsafter);
    //             // detailsbefore = JSON.parse(data.detailsbefore);
    //             // console.log(detailsbefore);
    //             // console.log(detailsafter);
    //         }
    // });
    var mdbupdatesdatatable = $('#mdbupdates').DataTable({
        //"dom": "<"#sites_paginate.page-link" >",
        // "dom": '<"top"i>rt<"bottom"flp><"clear">',
        "processing":true,  
        "serverSide":true, 
        "autoWidth": false,
        "scrollX": true,
        "scrollY": true,
        "order":[],  
        "ajax":{  
            url: url+"generatedatatable/mdbupdates", 
            type:"POST"  
        },  
        // "columnDefs":[  
        // {  
        //     //"targets":[2,3,4,5],
        //     "orderable":false,  
        // }]
    });

    $("#mdbupdates").on('click','tbody tr', function(evt){
         // $('.bd-example-modal-lg').show();
        var updateid = (mdbupdatesdatatable.row(this).data())[0];
        console.log(updateid);
        // console.log((mdbupdatesdatatable.row(this).data()));
        // var updateid = ($(this).parent().siblings("#approvalid").text());
        var db = (mdbupdatesdatatable.row(this).data())[3];
        var modalth1 = "<tr><th scope=\"col\">Update Type</th><th scope=\"col\">Database</th><th scope=\"col\">Update Date</th><th scope=\"col\">User ID</th><th scope=\"col\">Approval ID</th></tr>";
        var modalth2 = "<tr><th scope=\"col\" align=\"right\" style=\"width:15%;\"></th><th scope=\"col\">Details Before</th><th scope=\"col\">Details After</th></tr>";
        const modalth3 = `<tr><th colspan="3"scope=\"col\" style="text-align:left;">RF Parameter Details</th></tr><tr><th scope=\"col\">Cellname</th><th scope=\"col\">Cell ID</th><th scope=\"col\">Azimuth</th></tr>`;
        $('#approvalmodal1 thead').empty();
        $('#approvalmodal2 thead').empty();
        $('#approvalmodal1 thead').append(modalth1);
        
        $('#mdbupdatesmodal').modal('show');
        //alert(db);
        $.ajax({
            url: url+'getmdbupdate',
            type: 'post',
            dataType: 'json',
            data:{
                "updateid":updateid
            },
            success: function(data){
                console.log(data);
                // var detailsafter = JSON.parse("{"+data.detailsafter+"}");
                // var detailsbefore = JSON.parse("{"+data.detailsbefore+"}");
                var fieldnames = JSON.parse(data.fieldnames);
                var detailsafter = JSON.parse(data.detailsafter);
                var detailsbefore = JSON.parse(data.detailsbefore);
                // console.log('----'+(detailsafter.length));
                console.log((fieldnames));
                // console.log((detailsbefore.length));
                console.log((detailsbefore));
                console.log((detailsafter));
                var header_array=[];
                var detailsbefore_array=[];
                var detailsafter_array=[];
                $('#approvalmodal1 tbody').empty();
                $('#approvalmodal2 tbody').empty();

                var tr1 = "<tr>";
                tr1 += "<td>"+ data.updatetype +"</td>";
                tr1 += "<td>"+ data.db +"</td>";
                tr1 += "<td>"+ data.datecreated +"</td>";
                tr1 += "<td>"+ data.userid +"</td>";
                tr1 += "<td>"+ data.id +"</td>";
                tr1 += "</tr>";
                $('#approvalmodal1 tbody').append(tr1);

                $("#remarks-dialog-p small").text(data.remarks);
                $("#cite").text(data.dateapproved);
                currentapprovalid = updateid;

                var detailsbefore_var=[];
                var detailsafter_var=[];
                // console.log(db);
                // console.log(detailsbefore.length);
                // console.log(detailsafter.length);

                switch(data.updatetype)
                {
                    case 'add':
                        var th=`<tr>`;
                        var tdafter;
                        var colspanlength=0;
                        { 
                            var i=0;
                            // console.log(fieldnames.length);

                            $.each( fieldnames[0] , function( key, value )
                            {
                                // if(value!="")
                                {   
                                    th+=`<th scope="col">`+value+`</th>`;
                                    colspanlength++;
                                }
                            });

                            $.each( detailsafter , function( key, value )
                            {
                                tdafter+=`<tr>`;
                                $.each( value , function( key2, value2 )
                                {
                                    if(value2!="")
                                    {   
                                        tdafter+=`<td scope="col">`+value2+`</td>`;
                                    }else
                                    {
                                        tdafter+=`<td scope="col">-</td>`;
                                    }
                                });
                                tdafter+=`</tr>`;
                            });
                        }
                        th+=`</tr>`;
                        tdafter+=`</tr>`;
                        $('#approvalmodal2 thead').append(`<tr><th scope="col" colspan="`+colspanlength+`"align="center">Details After</th></tr>`);
                        $('#approvalmodal2 tbody').append(th);
                        $('#approvalmodal2 tbody').append(tdafter);

                        break;
                    case 'edit':
                        var th;
                        var tdafter;
                        var tdbefore;
                        var colspanlength=0;
                        var i=0;
                        if(db=='tower')
                        { 
                            th=`<tr>`;
                            tdafter=`<tr>`;
                            tdbefore=`<tr>`;
                            th+=`<th scope="col"></th>`;
                            tdbefore+=`<th scope="col">BEFORE</th>`;
                            tdafter+=`<th scope="col">AFTER</th>`;
                            // for(i=0;i<fieldnames.length;i++)
                            $.each( fieldnames , function( key, value )
                            {
                                $.each( value , function( key2, value2 )
                                {
                                    if(value!="")
                                    {   
                                        th+=`<th scope="col">`+value2+`</th>`;
                                        tdbefore+=`<td scope="col">`+detailsbefore[key][key2]+`</td>`;
                                        tdafter+=`<td scope="col">`+detailsafter[key][key2]+`</td>`;
                                        colspanlength++;
                                    }
                                });
                            });
                            
                            th+=`</tr>`;
                            tdafter+=`</tr>`;
                            tdbefore+=`</tr>`;
                            colspanlength=0;
                            // $('#approvalmodal2 thead').append(`<tr><th scope="col" colspan="`+colspanlength+`"align="center">Details Before</th></tr>`);
                            $('#approvalmodal2 tbody').append(th);
                            $('#approvalmodal2 tbody').append(tdbefore);
                            $('#approvalmodal2 tbody').append(tdafter);
                        }
                        else
                        { 
                            $.each( fieldnames , function( key, value )
                            {
                                th=`<tr>`;
                                tdafter=`<tr>`;
                                tdbefore=`<tr>`;
                                th+=`<th scope="col"></th>`;
                                tdbefore+=`<th scope="col">BEFORE</th>`;
                                tdafter+=`<th scope="col">AFTER</th>`;
                                colspanlength=0;
                                $.each( value , function( key2, value2 )
                                {
                                    // if(value2!="")
                                    {   
                                        th+=`<th scope="col">`+value2+`</th>`;
                                        tdbefore+=`<td scope="col">`+detailsbefore[key][key2]+`</td>`;
                                        tdafter+=`<td scope="col">`+detailsafter[key][key2]+`</td>`;
                                        colspanlength=0;
                                    }
                                });
                                th+=`</tr>`;
                                tdafter+=`</tr>`;
                                tdbefore+=`</tr>`;
                                colspanlength=0;

                                // $('#approvalmodal2 thead').append(`<tr><th scope="col" colspan="`+colspanlength+`"align="center">Details Before</th></tr>`);
                                $('#approvalmodal2 tbody').append(th);
                                $('#approvalmodal2 tbody').append(tdbefore);
                                // $('#approvalmodal2 thead').append(`<tr><th scope="col" colspan="`+colspanlength+`"align="center">Details After</th></tr>`);
                                // $('#approvalmodal2 tbody').append(th);
                                $('#approvalmodal2 tbody').append(tdafter);
                            });
                        }
                        break;
                    case 'delete':
                        var th=`<tr>`;
                        var tdbefore=`<tr>`;
                        var colspanlength=0;

                        { 
                            $.each( fieldnames[0] , function( key, value )
                            {
                                // if(value!="")
                                {   
                                    th+=`<th scope="col">`+value+`</th>`;
                                    colspanlength++;
                                }
                            });

                            $.each( detailsbefore , function( key, value )
                            {
                                tdbefore+=`<tr>`;
                                $.each( value , function( key2, value2 )
                                {
                                    if(value2!="")
                                    {   
                                        tdbefore+=`<td scope="col">`+value2+`</td>`;
                                    }else
                                    {
                                        tdbefore+=`<td scope="col">-</td>`;
                                    }
                                });
                                tdbefore+=`</tr>`;
                            });
                            // $.each( fieldnames[0] , function( key, value )
                            // {
                            //     // if(value!="")
                            //     {   
                            //         th+=`<th scope="col">`+value+`</th>`;
                            //         colspanlength++;
                            //     }
                            // });
                            // $.each( detailsbefore , function( key, value )
                            // {
                                
                            //     $.each( value , function( key2, value2 )
                            //     {
                            //         // if(value2!="")
                            //         {   
                            //             tdbefore+=`<td scope="col">`+value2+`</td>`;
                            //         }
                            //     });
                            //     tdbefore+=`</tr>`;
                            // });
                        }
                        th+=`</tr>`;
                        
                        $('#approvalmodal2 thead').append(`<tr><th scope="col" colspan="`+colspanlength+`"align="center">Details Before</th></tr>`);
                        $('#approvalmodal2 tbody').append(th);
                        $('#approvalmodal2 tbody').append(tdbefore);
                        break;
                }
                
            }

        });


    });
        

}); 