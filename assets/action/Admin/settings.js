$(function () {
	get_menus();
});

function get_menus(sort = '') {
	$.ajax({
		url: url + 'get_all_menus',
		type: "get",
		data: {sort: sort},
		async:false,
		dataType: 'json',
		beforeSend() {
			let html = '<div class="">' +
				'<h5 class="text-center text-white">' +
				'<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>' +
				'<h4 class="text-dark text-center">Loading</h4>' +
				'</h5>' +
				'</div>';
			$('#tbl_admin_menus').html(html);
		},
		success: function (data) {
			let html = '';
			if (data.length > 0)
			{
				build_admin_tbl(data);
			}
			else
			{
				html =
					'<tr>' +
						'<td colspan="7" class="text-center">' +
						'<h5 class="text-center text-white">' +
						'<i class="fas fa-archive display-1 text-dark"></i><br>' +
						'<h4 class="text-dark text-center">Empty Data.</h4>' +
						'<label class="text-dark text-center">No data has been added yet.</label>' +
						'</h5>' +
						'</td>'+
				    '</tr>';

				$('#tbl_admin_menus').html('').html(html);
			}

		},
		error: function(jqXHR, textStatus, errorThrown) {
			SWAL('error', "Oops!", "something went wrong.");
		}
	});
}

function build_admin_tbl(data) {
	let html = '';
	for (i in data) {
			let icon = data[i].sub_menu.length > 0 ? 'plus' : 'minus';
			let color = data[i].sub_menu.length > 0 ? 'primary' : 'danger';
			html += ' <tr data-toggle="collapse" data-target="#menus-'+data[i].id+'" class="accordion-toggle" data-type="menu">' +
				'      <td class="text-center">';
					if (data[i].sub_menu.length > 0) {
						html +=  '<button id="btn_menu" type="button" data-action="off" class="btn btn-info btn-'+color+' btn-sm rounded-circle"><i class="fas fa-'+icon+'"></i></button>';
					}

			html += ' </td>' +
				'      <td class="text-center">'+data[i].order+'</td>' +
				'      <td>'+data[i].name+'</td>' +
				'      <td>'+data[i].icon+'</td>' +
				'      <td>'+data[i].base_url+'</td>' +
				'      <td>'+data[i].dep+'</td>' +
				'      <td>' +
				'			<button type="button" class="btn btn-info btn-sm edit_menu" data-id='+data[i].id+'><i class="fas fa-edit"></i></button>' +
				'			<button type="button" class="btn btn-danger btn-sm del_menu" data-id='+data[i].id+'><i class="fas fa-trash"></i></button>	' +
				'			<button type="button" class="btn btn-success btn-sm add_sub_menu" data-base_url='+data[i].base_url+' data-id='+data[i].id+'><i class="fas fa-plus"></i></button>' +
				'       </td>' +
				'    </tr>' +
				' 	 <tr>';
						if (data[i].sub_menu.length > 0) {
							html += ' <td colspan="6" class="hiddenRow">';
							for (o in data[i].sub_menu) {
								html += '<tr class="accordion-body collapse bg-light" id="menus-'+data[i].id+'" data-type="sub">' +
										'	   <td class="text-center"></td>' +
										'	   <td class="text-center">'+data[i].sub_menu[o].order+'</td>' +
										'      <td>'+data[i].sub_menu[o].name+'</td>' +
										'      <td>'+data[i].sub_menu[o].icon+'</td>' +
										'      <td>'+data[i].sub_menu[o].base_url+'</td>' +
										'      <td>'+data[i].sub_menu[o].dep+'</td>' +
										'      <td>' +
										'			<button type="button" class="btn btn-info btn-sm edit_menu" data-type="sub" data-id='+data[i].sub_menu[o].id+'><i class="fas fa-edit"></i></button>' +
										'			<button type="button" class="btn btn-danger btn-sm del_menu" data-id='+data[i].sub_menu[o].id+'><i class="fas fa-trash"></i></button>	' +
										'       </td>' +
									'</tr>';
							}
							html +=  '</td> ';
						}
				html += '</tr>';
	}
	$('#tbl_admin_menus').html(html);
}

$(document).on('click', '#btn_menu', function () {
	let action = $(this).data('action');
	if (action === 'off') {
		$(this).data('action', 'on');
		$(this).removeClass('btn-primary').addClass('btn-danger');
		$(this).html('').html('<i class="fas fa-minus"></i>');
	} else {
		$(this).data('action', 'off');
		$(this).removeClass('btn-danger').addClass('btn-primary');
		$(this).html('').html('<i class="fas fa-plus"></i>');
	}
});

$(document).on('click', '.accordion-toggle', function () {
	$('.hiddenRow').hide();
	$(this).next('tr').find('.hiddenRow').show().addClass('open');
});

$(document).on('change', '#sort_menu', function () {
	get_menus($(this).val());
});

$(document).on('click', '#add_new', function () {
	$('#add_edit_menu').modal('show');
});

$(document).on('submit', '#save_menu', function(e) {
	e.preventDefault();
	$.ajax({
		url: url + 'new_menu',
		type: "post",
		data: new FormData(this),
		processData:false,
		contentType:false,
		cache:false,
		async:false,
		dataType: 'json',
		success: function (data) {
			$("#order", "#name", "#icon", "#base_url", "#mid", "#type").val("");
			$("#department").val("RRE");
			SWAL(data.type, data.title,  data.msg);
			$('#add_edit_menu').modal('hide');
			get_menus();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			errorSwal();
		}
	});
});

$(document).on('click', '.edit_menu', function () {
	let id = $(this).data('id');
	let order = $(this).closest("tr").find('td:eq(1)').text();
	let name = $(this).closest("tr").find('td:eq(2)').text();
	let icon = $(this).closest("tr").find('td:eq(3)').text();
	let base_url = $(this).closest("tr").find('td:eq(4)').text();
	let dep = $(this).closest("tr").find('td:eq(5)').text();
	let sub = $(this).closest("tr").data('type');

	$('#sub_name').val(sub);
	$('#mid').val(id);
	$('#order').val(order);
	$('#name').val(name);
	$('#icon').val(icon);
	$('#base_url').val(base_url);
	$('#department').val(dep);
	$('#add_edit_menu').modal('show');
});

$(document).on('click', '.del_menu', function () {
	let id = $(this).data('id');
	Swal.fire({
		title: 'Are you sure?',
		text: "You want to remove this data.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Delete'
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: url + 'del_menu',
				dataType: 'json',
				data:{id:id},
				type: 'POST',
				success: function(data) {
					SWAL(data.type, data.title,  data.msg);
					get_menus();
				},
				error: function(xhr, status, error) {
					errorSwal();
				}
			});
		}
	});
});

$(document).on('click', '.add_sub_menu', function () {
	let base_url = $(this).data('base_url');
	$('#url').val(base_url);
	$('#add_sub_menu').modal('show');
});

let status = false;
$(document).on('keyup', '#no_sub_menu:input', function() {
	let no_sub_menu = $('#no_sub_menu').val();
	let html = '';
	for (i = 0; i < no_sub_menu; i++) {
		html +=
			'<div class="form-row mg-top-5">' +
				'<div class="col">\n' +
					'\t\t\t\t\t\t\t\t<input type="text" name="Name[]" class="subs form-control" placeholder="Enter Menu Name" required>\n' +
					'\t\t\t\t\t\t\t</div>\n' +
					'\t\t\t\t\t\t\t<div class="col">\n' +
					'\t\t\t\t\t\t\t\t<input type="text" name="Icon[]" class="subs form-control" placeholder="Enter Icon Name" required>\n' +
					'\t\t\t\t\t\t\t</div>\n' +
					'\t\t\t\t\t\t\t<div class="col">\n' +
					'\t\t\t\t\t\t\t\t<input type="text" name="Base_url[]" class="subs form-control" placeholder="Enter Base Url" required>\n' +
				'</div>' +
			'</div>';
	}
	$('#num_of_menus').html(html);
});

$(document).on('submit', '#save_sub_menu', function(e) {
	e.preventDefault();
	$.ajax({
		url: url + 'sub_menu',
		type: "post",
		data: new FormData(this),
		processData:false,
		contentType:false,
		cache:false,
		async:false,
		dataType: 'json',
		success: function (data) {
			$(".subs").val("");
			SWAL(data.type, data.title,  data.msg);
			$('#add_sub_menu').modal('hide');
			$('#base_url').val('');
			get_menus();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			errorSwal();
		}
	});
});

