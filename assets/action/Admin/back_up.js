let array = [];
$(function () {
	doAjax().then( (data) => {
		if (data.type === 'error') {
			SWAL(data.type, data.title,  data.msg);
		} else {
			get_datas(data);
			array = data;
		}

	});
});

$(document).on('keyup', '#search', function(e) {
	e.preventDefault();
	// doAjax($(this).val()).then( (data) => {

		// get_datas(data);
	// let search = _.mapValues($(this).val(), array);
	console.log(array);
	// let search = _.filter(array, (o) => _.includes(o, $(this).val()));
	// let result = _.filter(array, obj => _.some(obj, val => val === $(this).val()));
	// get_datas(search);
	// console.log(result);
	// });
	// _.forEach(data, function (value, key) {
	// 	let result = _.filter(array, obj => _.some(obj, val => val === $(this).val()));
	// 	console.log(result);
	// });

	for (i in array) {
		let result = _.filter(array[i], obj => _.some(obj, val => val === $(this).val()));
		console.log(result);
	}
});

$(document).on('click', '.btn-download', function(e) {
	e.preventDefault();
	let arr = {
		name : $(this).data('name'),
		type : $(this).data('type')
	};
	if ($(this).data('type') !== 'csv') {
		$("#download_loader").modal('show');
	}

	download_db(arr).then( (data) => {
		if ($(this).data('type') === 'csv') {
			let today = moment().format("DD/MM/YYYY");
			JSONToCSVConvertor(data,""+$(this).data('name')+"_(" + today + ")", true);
		} else {
			$("#download_loader").modal('hide');
			window.open(data, '_blank');
			delete_db(data).then( (datas) => {
				console.log(datas);
			});
		}
	});
});

$(document).on('click', '.btn-backup', function(e) {
	e.preventDefault();
	let type = $(this).data('type');
	$("#download_loader").modal('show');
	backup_db(type).then( (data) => {
		$("#download_loader").modal('hide');
		window.open(data, '_blank');
		delete_db(data).then( (datas) => {
			console.log(datas);
		});
	});
});

function get_datas(data) {
	let html = '';
	_.forEach(data, function (value, key) {
		html += ' <div class="col-sm-3 mg-top-10">\n' +
				' <div class="card shadow">\n' +

				'    <div class="card-body">\n' +
				'      <p class="card-title text-center h5">'+value.name+'</p>\n' +
						'<div class="dropdown text-center">\n' +
						'  <button class="btn btn-outline-primary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n' +
						'   <i class="fas fa-download"></i> Download' +
						'  </button>\n' +
						'  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">\n' +
						'    <button class="dropdown-item btn-download" data-type="sql" data-name='+value.name+'><i class="fas fa-database"></i>	as .SQL</button>\n' +
						'    <div class="dropdown-divider"></div>'+
						'    <button class="dropdown-item btn-download" data-type="csv" data-name='+value.name+'><i class="fas fa-file-csv"></i> as .CSV</button>\n' +
						'    <div class="dropdown-divider"></div>'+
						'    <button class="dropdown-item btn-download" data-type="gz" data-name='+value.name+'><i class="fas fa-box-open"></i> as .GZ</button>\n' +
						'  </div>\n' +
						'</div>'+
				'    </div>\n' +
					' <div class="card-footer text-muted text-center" style="padding: 5px">\n' +
							'<div class="col-12 small">' +
								'<div class="row">' +
									'<div class="col-6 border-right border-secondary">' +
									'Rows: '+ value.count +
									'</div>' +
									'<div class="col-6">' +
									'Fields: '+ value.fields +
									'</div>' +
							'</div>' +
						'</div>' +
					'  </div>' +
				'  </div>\n' +
				'</div>';
	});
	$('#count_tbl').text(data.length);
	$('#card_datas').html(html);
}

async function doAjax(param = []) {
	let result;

	try {
		result = await $.ajax({
			url: url + 'get_datas',
			type: 'GET',
			data: param,
			dataType: 'json',
			beforeSend() {
				let html = '<div class="content mg-top-135">' +
					'<h5 class="text-center text-white">' +
					'<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>' +
					'<h4 class="text-dark text-center">Loading</h4>' +
					'</h5>' +
					'</div>';
				$('#card_datas').html(html);
			},
		});

		return result;
	} catch (error) {
		errorSwal();
	}
}

async function download_db(param = []) {
	let result;
	try {
		result = await $.ajax({
			url: url + 'download_db',
			type: 'POST',
			data: param,
			dataType: param.type === 'csv' ? 'json' : 'text',
		});

		return result;
	} catch (error) {
		errorSwal();
	}
}

async function backup_db(type) {
	let result;

	try {
		result = await $.ajax({
			url: url + 'backup_db',
			data: {type: type},
			type: 'GET',
			dataType: 'text',
		});

		return result;
	} catch (error) {
		errorSwal();
	}
}

async function delete_db(file) {
	let result;

	try {
		result = await $.ajax({
			url: url + 'delete_db',
			type: 'POST',
			data: {file: file},
			dataType: 'text',
		});

		return result;
	} catch (error) {
		errorSwal();
	}
}
