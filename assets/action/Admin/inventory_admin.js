let data = [];
let ids = [];

$(function () {
	get_users();
	get_requested_item();
});

$(document).on('click', '#accept_request', function () {
	Swal.fire({
		title: 'Accept Request?',
		text: "You won't be able to revert this!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes',
		// reverseButtons: true
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: url + 'accept_request',
				dataType: 'json',
				data:{id:ids},
				type: 'POST',
				success: function(data) {
					SWAL(data.type, data.title,  data.msg);
					get_requested_item();
				},
				error: function(xhr, status, error) {
					errorSwal();
				}
			});
		}
	})
});

$(document).on('click', '#reject_request', function () {
	Swal.fire({
		title: 'Reject Request?',
		text: "You won't be able to revert this!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes',
		// reverseButtons: true
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: url + 'reject_request',
				dataType: 'json',
				data:{id:ids},
				type: 'POST',
				success: function(data) {
					SWAL(data.type, data.title,  data.msg);
					get_requested_item();
				},
				error: function(xhr, status, error) {
					errorSwal();
				}
			});
		}
	})
});

$(document).on('change', '#chk_item', function () {
	$(".check-tbl-item").prop('checked', !!$(this).is(":checked"));
	$('#accept_item, #accept_request, #reject_request').prop('disabled', !$(this).is(":checked"));
	let total_length = $('#table_requests_lists tr').length;
	ids = [];
	data = [];
	if ($(this).is(":checked")) {
		$('.btn-options').removeClass('d-none');
		$('.btn-add').addClass('d-none');
		for (i = 1; i <= total_length; i++) {
			let row = $('.row-'+i);
			let id = row.closest("tr").find('td:eq(0)').attr('row_id');
			data.push({id:id});
			ids.push(id);
		}
	}
});

$(document).on('click', '.check-tbl-item', function () {
	let chk = $('#chk_item');
	let total_checked = $('#table_requests_lists').find('.check-tbl-item:checked').length;
	let total_length = $('#table_requests_lists tr').length;
	chk.prop('checked', total_checked === total_length);

	if (total_checked === total_length) {
		$('#accept_item, #accept_request, #reject_request').prop('disabled', !chk.is(":checked"));
	} else {
		$('#accept_item, #accept_request, #reject_request').prop('disabled', total_checked === 0);
	}

	let id = $(this).closest("tr").find('td:eq(0)').attr('row_id');

	if ($(this).is(":checked")) {
		data.push({id:id});
	} else {
		data.pop($(this).closest('tr').attr('row_id'));
	}

	if ($(this).is(":checked")) {
		ids.push(id);
	} else {
		ids.pop($(this).closest('tr').attr('row_id'));
	}
});

$(document).on('keyup', '#search_req_items', function(e) {
	e.preventDefault();
	get_requested_item($(this).val());
});

$(document).on('change', '#name', function(e) {
	e.preventDefault();
	get_requested_item($(this).val());
});

$(function() {
		$('#date_select').datepicker({format: 'yyyy-mm-dd',
		}).on('changeDate', function(e) {
    		console.log($(this).val());
				get_requested_item('', $(this).val());
		});
});

function get_requested_item(search = "", date = "") {
	$.ajax({
		url: url + 'get_requested_item',
		type: "post",
		async:false,
		data: {search: search, date: date},
		dataType: 'json',
		beforeSend() {
			let html = '<div class="content">' +
				'<h5 class="text-center text-white mg-top-135">' +
				'<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>' +
				'<h4 class="text-dark text-center">Loading</h4>' +
				'</h5>' +
				'</div>';
			$('#table_requests_lists').html(html);
		},
		success: function (data) {
			let html = '';
			if (data.length > 0)
			{
				let ctr = 1;
				for (i in data)
				{
					let image = data[i].image === '' ? url + './style/images/no_product.png' : data[i].image;
					html += '<tr class="border-bottom">' +
							'<td class="row-'+ctr+'" row_id='+data[i].id+'>' +
							'<div class="custom-control pad-left-rem custom-checkbox chk-'+i+'">' +
							'<input type="checkbox" name="table2_lists" class="custom-control-input check-tbl-item" id="check-item-'+i+'">' +
							'<label class="custom-control-label" for="check-item-'+i+'"></label>' +
							'</div>' +
							'</td>' +
							'<td class="text-center">'+ctr+'.</td>' +
							'<td class="row-'+ctr+'" row_id='+data[i].id+'>' +
							'<div class="col-12">' +
							'<div class="row" >' +
							'<div class="col-3">' +
								'<img src='+image+' height="80" width="80" class="border vertical-center text-center shadow-sm rounded border">' +
							'</div>' +
							'<div class="col-9">' +
								'<div class="row small">' +
							    '<div class="col-sm">' +
										'<label for="username" class="text-uppercase text-muted">Item Code: <b>'+data[i].item_code+'</b></label><br>' +
										'<label for="username" class="text-uppercase text-muted">Name: <b>'+$.trim(data[i].name)+'</b></label><br>' +
										'<input type="hidden" name="id[]" class="form-control text-center" value='+data[i].id+' id="id-'+i+'">' +
										'<label class="text-uppercase text-muted">Quantity: <b>'+data[i].qty+'</b></label><br>'+
							    '</div>' +
							    '<div class="col-sm">' +
										'<label class="text-uppercase text-muted">Requested by: <b>'+data[i].fname+'</b></label>'+
										'<label class="text-uppercase text-muted">Date requested: <b>'+data[i].request_date+'</b></label>'+
										'<label class="text-uppercase text-muted">Purpose: <b>'+data[i].purpose+'</b></label>'+
							    '</div>' +
							  '</div>' +
							'</div>' +
							'</div>' +
							'</div>' +
							'</div>' +
							'</td>' +
							'</tr>';
					ctr++;
				}
			}
			else
			{
				html = '<div class="content">' +
					'<h5 class="text-center text-white">' +
					'<i class="fas fa-clipboard-list display-1 text-dark"></i><br>' +
					'<h4 class="text-dark text-center">Empty Data.</h4>' +
					'<label class="text-dark text-center">No data has been requested yet.</label>' +
					'</h5>' +
					'</div>';
			}
			$('#chk_item').prop('checked', false).prop('disabled', data.length > 0 ? false : true);
			$('#table_requests_lists').html(html);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			SWAL('error', "Oops!", "something went wrong.");
		}
	});
}

function get_users() {
	$.ajax({
		url: url + 'get_users',
		type: "get",
		async:false,
		dataType: 'json',
		success: function (data) {
			let html = '<option value="">All</option>';
			if (data.length > 0)
			{
				let ctr = 1;
				for (i in data)
				{
						html += '<option value='+data[i].id+'>'+data[i].fname+'</option>'
				}
			}
			$('#name').html(html);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			SWAL('error', "Oops!", "something went wrong.");
		}
	});
}

function clearItems() {
	ids = [];
	data = [];
	$('#accept_request, #reject_request').prop('disabled', true);
}
