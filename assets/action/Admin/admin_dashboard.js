let cards = [
	{
		'title' : 'Surveys',
		'icon'   : 'fas fa-user-tie',
		'color'  : 'bg-warning',
		'class_num' : 'total-surveys',
	},
	{
		'title' : 'Active Users',
		'icon'   : 'fas fa-users',
		'color'  : 'bg-primary',
		'class_num' : 'total-active',
	},
	{
		'title' : 'Issues',
		'icon'   : 'fas fa-newspaper',
		'color'  : 'bg-danger',
		'class_num' : 'total-issues',
	}
];

$(function () {
	get_dashboard(true);
	set_cards();

	$('#date_select').datepicker({format: 'yyyy-mm-dd',
	}).on('changeDate', function(e) {
		let arr = {
			row_num  : 0,
			row_page : 3,
			search   : $('#search').val(),
			date     : $(this).val()
		};
		getAllLogs(arr).then( (data) => {
			get_logs(data.data, data.pagination);
		});
	});
});

function get_dashboard(timing) {
	doAjax(timing).then( (data) => {
		getActivities(data.activities);
		set_values(data);
		get_logs(data.logs.data, data.logs.pagination);
		if (timing) {
			$('#tracks').removeClass('h-145');
			$('.loader').addClass('d-none');
		}
		$('.c_tracking').removeClass('d-none');
	});
}

$(document).on('click', '#logs-pagination a', function(e) {
	e.preventDefault();
	let row_num = $(this).attr('data-ci-pagination-page');
	let arr = {
		row_num  : row_num,
		row_page : 3,
		search   : $('#search').val(),
		date     : $('#date_select').val()
	};
	getAllLogs(arr).then( (data) => {
		get_logs(data.data, data.pagination);
	});
});

$(document).on('keyup', '#search', function(e) {
	e.preventDefault();
	let arr = {
		row_num  : 0,
		row_page : 3,
		search   : $(this).val(),
		date     : $('#date_select').val()
	};

	getAllLogs(arr).then( (data) => {
		get_logs(data.data, data.pagination);
	});
});


function getActivities(data) {
	let html = '';
	let ctr = 1, count = 0;
	let total = 0;
	if (data.length > 0) {
		for (i in data) {
			for (o in data[i].names) {
				let counts = data[i].names[o].subject.length;
				let more = parseInt(counts - 1) === 0 ? '' : ' +' + parseInt(counts - 1) + ' more <i class="fas fa-calendar-plus"></i>';

				let day = data[i].names[o].subject[0].day;
				let month = data[i].names[o].subject[0].month;

				let subs = "";
				let num = 1;
				for (a = 0; a < data[i].names[o].subject.length; a++) {
					subs += num + '.' + data[i].names[o].subject[a].subject_name + '\n';
					num++;
					total++;
				}

				if (ctr <= 4) {
					html +=
						'<div class="card mb-2">' +
							'<div class="row no-gutters">' +
								'<div class="col-md-4 bg-warning text-center">' +
									'<div class="card-body">' +
										'<p class="card-text">' +
											'<b>'+day+'</b><br>' +
											'<small class="text-bold">'+month+'</small>' +
										'</p>' +
									'</div>' +
								'</div>' +
								'<div class="col-md-8">' +
										'<p class="card-text container text-truncate mg-top-10">' +
											'<b data-toggle="tooltip" data-placement="top" title="' + data[i].names[o].subject[0].subject_name + '">'+data[i].names[o].subject[0].subject_name+'</b><br>' +
											'<small class="text-muted">'+data[i].names[o].subject[0].name+'</small><br>';
												if (parseInt(counts - 1) > 0) {
													html += '<small class="text-muted" data-toggle="tooltip" data-placement="top" title="' + subs + '">'+ more +'</small>';
												}
										html += '</p>' +
								'</div>' +
							'</div>' +
						'</div>';
					count++;
				}
				ctr++;
			}
		}
		let ctrs = data.length - count;
		if (ctrs > 0) {
			html += '<div class="card">' +
						'<b class="card-text text-center"> +' +  ctrs +  ' more' + '</b>' +
					'</div>';
		}
	} else {
		html = '<div class="empty mg-top-135">' +
			'<h5 class="text-center text-white">' +
			'<span class="far fa-calendar-alt display-3 text-dark"></span><br>' +
			'<h5 class="text-dark text-center">No events.</h5>' +
			'<label class="text-muted small text-center ">No list has been displayed yet.</label>' +
			'</h5>' +
			'</div>';
	}
	$('.surveys').text(data.length);
	$('.total_rings').text(total);
	$('.act-list').html(html);
	$('[data-toggle="tooltip"]').tooltip();
}

function set_values(data) {
	$('.total-surveys').text(data.surveys.surveyed);
	$('.total-active').text(data.users.active);
	$('.u-total').text(data.users.users);
	$('.u-activate').text(data.users.activated);
	$('.u-deactivate').text(data.users.deactivated);
}

function set_cards() {
	let current = moment().format("MMMM YYYY");
	let html = '';
	for (i in cards) {
		html += '<div class="card shadow">\n' +
			'    <div class="card-body">\n' +
			'\t\t\t\t\t\t\t\t<div class="row">\n' +
			'\t\t\t\t\t\t\t\t\t<div class="col">\n' +
			'\t\t\t\t\t\t\t\t\t\t<h6 class="card-title text-uppercase text-muted mb-0">'+cards[i].title+'</h6>\n' +
			'\t\t\t\t\t\t\t\t\t\t<span class="h4 font-weight-bold mg-top-5 '+cards[i].class_num+' counter">0</span>\n' +
			'\t\t\t\t\t\t\t\t\t</div>\n' +
			'\t\t\t\t\t\t\t\t\t<div class="col-auto">\n' +
			'\t\t\t\t\t\t\t\t\t\t<div class="pad-font icon icon-shape '+cards[i].color+' text-white rounded-circle shadow">\n' +
			'\t\t\t\t\t\t\t\t\t\t\t<i class="'+cards[i].icon+'"></i>\n' +
			'\t\t\t\t\t\t\t\t\t\t</div>\n' +
			'\t\t\t\t\t\t\t\t\t</div>\n' +
			'\t\t\t\t\t\t\t\t</div>';

			if (parseInt(i)  === 0) {
				html += '<p class="mt-3 mb-0 text-muted text-sm">\n' +
							'<span class="text-warning mr-2" data-toggle="tooltip" data-placement="top" title="Total number of Sites to be surveyed."><b class="total_rings">0 </b> <i>Sites</i></span>\n' +
							'<span class="text-nowrap">| '+current+'</span\n' +
						'</p>';
			} else if (parseInt(i)  === 1) {
				html += '\t\t\t\t\t\t\t\t<p class="mt-3 mb-0 text-muted text-sm text-center">\n' +
					'\t\t\t\t\t\t\t\t\t<span class="text-success mr-2" data-toggle="tooltip" data-placement="top" title="Activated"><i class="fas fa-user"></i> | <b class="u-activate counter">0</b></span>\n' +
					'\t\t\t\t\t\t\t\t\t<span class="text-danger mr-2" data-toggle="tooltip" data-placement="top" title="Deactivated"><i class="fas fa-user"></i> | <b class="u-deactivate counter">0</b></span>\n' +
					'\t\t\t\t\t\t\t\t</p>';
			} else if (parseInt(i)  === 2) {
				html += '\t\t\t\t\t\t\t\t<p class="mt-3 mb-0 text-muted text-sm">\n' +
					'\t\t\t\t\t\t\t\t\t<span class="text-danger mr-2"><i class="fas fa-arrow-down"></i> 0</span>\n' +
					'\t\t\t\t\t\t\t\t\t<span class="text-nowrap"></span>\n' +
					'\t\t\t\t\t\t\t\t</p>';
			}

			html += '</div>\n' +
			'  </div>';
	}

	$('#cards-dash').html(html);

}

function get_logs(data, pagination) {
	let html = '';
	if (data.length > 0) {
		for (i in data) {
			html += '<li class="list-group-item list-padding">\n' +
				'<div class="row">' +
				'<div class="col-2 text-center">' +
				'<img src='+data[i].profile+' height="73" width="73" class=" rounded-circle rounded-sm shadow" alt='+data[i].name+'>'+
				'</div>'+
				'<div class="col-10">' +
				'<div class="d-flex w-100 justify-content-between">\n' +
				'<small class="mb-0">'+data[i].name+'</small>\n' +
				'<small class="text-muted">'+data[i].ago+'</small>\n' +
				'</div>\n' +
				'<h6 class="mb-1">'+data[i].action+'</h6>\n' +
				'<em class="small text-muted">'+data[i].department+' - '+data[i].geoarea+'</em>\n' +
				'</div>'+
				'</div>' +
				'</li>';
		}
	} else {
		html = '<div class="empty mg-top-30 text-center">' +
			'<h5 class="text-center text-white">' +
			'<span class="fas fa-clipboard-list display-3 text-dark"></span><br>' +
			'<h5 class="text-dark text-center">Empty</h5>' +
			'<label class="text-muted small text-center ">No result found.</label>' +
			'</h5>' +
			'</div>';
	}


	$('.logs-lists').html(html);
	$('#logs-pagination').html(pagination);
}

async function getAllLogs(data) {
	let result;

	try {
		result = await $.ajax({
			url: url + 'Admin/get_logs/'+data.row_num,
			type: 'GET',
			data: data,
			dataType: 'json',
			beforeSend() {
				let html = '<h5 class="text-center text-white mg-top-30">' +
					'<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>' +
					'<h4 class="text-dark text-center">Loading</h4>' +
					'</h5>';
				$('.logs-lists').html(html);
			},
		});

		return result;
	} catch (error) {
		errorSwal();
	}
}

async function doAjax(timing) {
	let result;

	try {
		result = await $.ajax({
			url: url + 'get_dashboard',
			type: 'GET',
			dataType: 'json',
			data: {row_page: 3},
			beforeSend() {
				if (timing) {
					let html = '<h5 class="text-center text-white mg-top-135">' +
						'<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>' +
						'<h4 class="text-dark text-center">Loading</h4>' +
						'</h5>';
					$('.act-list').html(html);

					$('.loader').removeClass('d-none');
					$('#tracks').addClass('h-145');
				}
			},
		});

		return result;
	} catch (error) {
		errorSwal();
	}
}
