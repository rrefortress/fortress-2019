let calendar = $('.calendar');
$('#surveyNewNames').on('change', function () {
	calendar.fullCalendar('destroy');
	if ($(this).val() === 'All' || $(this).val() === 'NULL') {
		getNewChart();
	} else {
		$.ajax({
			url: url + 'get_new_track',
			dataType: 'json',
			data: {name: $(this).val()},
			type: 'POST',
			success: function (data) {
				if (data['data'].length > 0) {
					$("#pieChartNew").empty();
					// canvasPieNew.destroy();
					setToNewSiteGraph(data['data'][0]);
				}
			},
			error: function (xhr, status, error) {
				errorSwal();
			}
		});
	}
});

// $(window).load(function() {
//     $('.calendar').fullCalendar('render');
//  });

$(function () {
	$('#calendar').fullCalendar('today');
	$('.fc-today-button').trigger('click');
	calendar.fullCalendar('render');
	callCalendar();
});

$('#view_modal').on('click', function () {
	$('.fc-today-button').trigger('click');

	$('.view_sched').modal('show');
	let id = $('#surveyNewNames').val();
	callCalendar(id = '');
});

function callCalendar(id = '') {
	$.ajax({
		url: url + 'set_calendar',
		dataType: 'json',
		type: "POST",
		data: {id:id},
		beforeSend() {
			$('.loaders').removeClass('d-none');
			calendar.addClass('d-none');
		},
		success: function(data) {
			let datas = [];
			if (data.length > 0) {
				for (i in data) {
					for(o in data[i].names) {
						let counts = data[i].names[o].subject.length;
						let more = parseInt(counts - 1) === 0 ? '' : ' +' + parseInt(counts - 1)  + ' more';
						let name = data[i].names[o].subject[0].name + more;

						let Survey_Date = data[i].names[o].subject[0].Survey_Date;
						let status = data[i].names[o].subject[0].status;
						let category = data[i].names[o].subject[0].category;

						let subs = "";
						let num = 1;
						for (a = 0; a < data[i].names[o].subject.length; a++) {
							subs += num + '.' + data[i].names[o].subject[a].subject_name + '<br>' ;
							num++;
						}

						let current_date = new Date();
						current_date.setHours(0,0,0,0);
						let color = '#dedef0';

						if (new Date(Survey_Date) >= current_date) {
							color = '#f6da63';
						} else {
							if (category !== null) {
								switch (category.toLowerCase()) {
									case 'new site': color    = '#46b3e6'; break;
									case 're-survey': color   = '#ed8240'; break;
									case 'master plan': color = '#ea5e5e'; break;
									case 'owo': color         = '#d89cf6'; break;
									case 'ewo': color         = '#d89cf6'; break;
									default: color            = '#dedef0'; break;
								}
							}
						}
						let json = {
							title: name,
							start:Survey_Date,
							end: Survey_Date,
							description: subs,
							backgroundColor : color,
							textColor: '#000000',
							borderColor: '#000000',
						};

						datas.push(json);
					}
				}
			}
			$('.loaders').addClass('d-none');
			calendar.removeClass('d-none');
			setCalendar(datas);
			calendar.fullCalendar('refresh');
		},
		error: function(xhr, status, error) {
			errorSwal();
		}
	});
}

function setCalendar(data) {
	calendar.fullCalendar({
		height: $(window).height()*0.83,
		viewDisplay: resizeCalendar,
		timeZone: 'UTC',
		themeSystem: 'jquery-ui',
		displayEventTime: true,
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay,listMonth'
		},
		resourceGroupField: 'title',
		editable: false,
		events: data,
		resources: data,
		displayEventTime : false,
		eventRender: function(eventObj, el) {
			makeTodaybtnActive();
			$(el).popover({
				title: eventObj.title,
				content: eventObj.description,
				trigger: 'hover',
				html: true,
				placement: 'top',
				container: 'body'
			});
		},
		eventLimit: true,
		views: {
			timeGrid: {
				eventLimit: 6 // adjust to 6 only for timeGridWeek/timeGridDay
			}
		}
	});
}

function makeTodaybtnActive()
      {
         $('.calendar button.fc-today-button').removeAttr('disabled');
         $('.calendar button.fc-today-button').removeClass('fc-state-disabled');
       }

function resizeCalendar(calendarView) {
    if(calendarView.name === 'agendaWeek' || calendarView.name === 'agendaDay') {
        // if height is too big for these views, then scrollbars will be hidden
        calendarView.setHeight(9999);
    }
}
