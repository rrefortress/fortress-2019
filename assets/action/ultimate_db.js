let cards = [
	{
		title : 'RAN',
		'icon' : 'fas fa-brain',
		'color': 'bg-warning'
	},
	{
		title : 'FACILITIES',
		'icon': 'fas fa-building',
		'color': 'bg-danger'
	},
	{
		title : 'WIRELINE',
		'icon': 'fas fa-network-wired',
		'color': 'bg-success'
	},
	{
		title : 'RGPM',
		'icon': 'fas fa-ethernet',
		'color': 'bg-primary'
	}
];
let rre_data = [];
let rfe_data = [];
let rwe_data = [];
let rcne_data = [];
let rgpm_data = [];
let rre_tabs = [
	{
		name : '2G',
		id_name : 'twoG',
		status : 'active'
	},
	{
		name : '3G',
		id_name : 'threeG',
		status : ''
	},
	{
		name : '4G - <em>Nokia</em>',
		id_name : 'fourG_nokia',
		status : ''
	},

	{
		name : '4G - <em>Huawei</em>',
		id_name : 'fourG_ht',
		status : ''
	}
];
let wire_tabs = [
	{
		name : 'RWE',
		id_name : 'rwe',
		status : 'active'
	},
	{
		name : 'RCNE',
		id_name : 'rcne',
		status : ''
	},
];
let rfe_tabs_list = [
	{
		name : '<small><i class="fas fa-th-large"></i> CARDS</small>',
		id_name : 'cards',
		status : 'active'
	},
	{
		name : '<small><i class="fas fa-table"></i> TABLE</small>',
		id_name : 'tbls',
		status : ''
	},
];
let rwe_tabs_list = [
	{
		name : '<small><i class="fas fa-th-large"></i> CARDS</small>',
		id_name : 'rwe_cards',
		status : 'active'
	},
	{
		name : '<small><i class="fas fa-table"></i> TABLE</small>',
		id_name : 'rwe_tbls',
		status : ''
	},
];

$(document).ready(function () {
	empty_results();
});

$(document).on('search', '#search', function () {
	if('' === this.value) {
		$('#result_cards').addClass('d-none');
		$('#u_cards').html('');
		empty_results();
	}
});

$(document).on('keyup', '#search', function () {
	let res_id = $('#results');
	$('#result_cards').addClass('d-none');
	$('#u_cards').html('');
	let search = $.trim($('#search').val());
	if (search !== '') {
		let param = {
			url: url + 'search_keys',
			dataType: 'json',
			type: 'GET',
			data: {search: search},
			beforeSend: res_id.css({'overflow-y': 'hidden'}).html('<label class="list-keys list-group-item small text-muted"> ' +
				'<i class="fa fa-spinner fa-pulse fa-fw"></i> <em>Loading results</em>' +
				'</label>')
		};

		fortress(param).then( (data) => {
			res_id.empty();
			let html = '';
			if (data.length > 0) {
				_.forEach(data, function (val) {
					html += '<button data-plaid="'+val.PLAID+'"  class="list-keys list-group-item list-group-item-action btn-sm small"> ' +
								val.PLAID + ' | <em class="text-muted text-italic">'+val.SITE_NAME+'</em>' +
							'</button>';
				});
				if (data.length > 10) {
					res_id.css({'height': '400px','overflow-y': 'scroll'}).html(html);
				} else {
					res_id.css({'overflow-y': 'hidden'}).html(html);
				}
			} else {
				html += '<button class="list-keys list-group-item list-group-item-action btn-sm small" disabled> ' +
							'<span class="text-muted text-italic">No results found.</span>' +
						'</button>';
				res_id.css({'overflow-y': 'hidden'}).html(html);
			}
		});
	} else {
		res_id.css({'overflow-y': 'hidden'}).empty();
		empty_results();
	}
});

$(document).on('click', '.list-keys', function (e) {
	e.preventDefault();
	let id = $(this).data('plaid');
	$('#search').val(id);
	$('#results').css({'height': '','overflow-y': ''}).html('');
	$('#result_cards').removeClass('d-none');

	let param = {
		url: url + 'search_data',
		dataType: 'json',
		type: 'POST',
		data: {plaid: id},
		beforeSend: $('#u_cards').html('<div class="col-12 mg-top-135">' +
			'<h5 class="text-center text-white">' +
			'<i class="fas fa-spinner fa-spin display-4 text-muted"></i></br></br>' +
			'<small class="text-center text-muted mg-top-10"><em>Loading cards...</em></small>' +
			'</h5>' +
			'</div>')
	};

	fortress(param).then( (data) => {
		set_cards(data);
	});
});

function set_cards(data) {
	let html = '';
	for (i in cards) {
		rre_data = data.rre;
		rfe_data = data.rfe;
		rwe_data = data.rwe;
		rcne_data = data.rcne;
		rgpm_data = data.rgpm;
		html += '<div class="card">\n' +
			'\t\t\t\t\t<div class="card-header text-center text-white '+cards[i].color+' " ><i class="text-white '+cards[i].icon+'"></i> '+cards[i].title+'</div>\n' +
			'\t\t\t\t\t<div class="row no-gutters">\n' +

			'\t\t\t\t\t\t<div class="col">\n' +
			'\t\t\t\t\t\t\t<div class=" small">\n';

			 html += cards[i].title === "RAN"  ? set_card_detail_rre(data.rre, cards[i].title, cards[i].color)    : '';
			 html += cards[i].title === "FACILITIES"  ? set_card_detail_rfe(data.rfe[0], cards[i].title, cards[i].color)    : '';
			 html += cards[i].title === "WIRELINE" ? set_card_detail_wireline(data.rwe[0], data.rcne[0], cards[i].title, cards[i].color)  : '';
		     html += cards[i].title === "RGPM" ? set_card_detail_rgpm(data.rgpm, cards[i].title, cards[i].color)  : '';

		     html += '\t\t\t\t\t\t\t</div>\n' +
			'\t\t\t\t\t\t</div>\n' +
			'\t\t\t\t\t</div>\n' +
			'\t\t\t</div>';
	}
	$('#u_cards').html(html);
}

function set_card_detail_rre(data, title, color) {
	let html = '';
	if (data.rre_details !== null) {
		html = '\t\t\t\t\t\t\t\t<table class="table table-bordered border-right-0 border-left-0 table-sm">\n' +
			'\t\t\t\t\t\t\t\t\t<tbody>\n' +
			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">BTS Name</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.rre_details.BTS_Name+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Baranggay</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.rre_details.Baranggay+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Municipal</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.rre_details.Town+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Province</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.rre_details.Province+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light" >Region</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6" class="text-truncate">'+data.rre_details.Region+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Cluster</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.rre_details.Cluster+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row"  colspan="4" class="text-muted text-right bg-light" colspan="2">Site Coverage</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.rre_details.Site_Coverage+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +
			'\t\t\t\t\t\t\t\t\t<tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row"  colspan="4" class="text-muted text-right bg-light" colspan="2">Solution Type</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.rre_details.Solution_Type+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +
			'\t\t\t\t\t\t\t\t\t<tr>\n' +

			'\t\t\t\t\t\t\t\t\t\t<td scope="row"  colspan="4" class="text-muted text-right bg-light" colspan="2">Latitude</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.rre_details.Latitude+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +
			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row"  colspan="4" class="text-muted text-right bg-light" colspan="2">Longitude</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.rre_details.Longitude+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +
			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +
			'\t\t\t\t\t\t\t\t\t</tbody>\n' +
			'\t\t\t\t\t\t\t\t</table>\n';
			html += view_more(color, title);

	} else {
		html = empty_card_result();
	}

	return html;
}

function set_card_detail_rfe(data, title, color) {
	let html = '';
	if (data !== undefined) {
		 html = '<table class="table table-bordered table-sm">\n' +
			'\t\t\t\t\t\t\t\t\t<tbody>\n' +
			'\n' +
			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Site Name</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.SITE_NAME+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +
			'\n' +
			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Tower Type</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.TOWER_TYPE+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +
			'\n' +
			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Tower Height</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.TOWER_HEIGHT+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +
			'\n' +
			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">HUB</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.HUB+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +
			'\n' +
			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Site Type</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.SITE_TYPE+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +
			'\n' +
			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Site Class</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.SITE_CLASS+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +
			'\n' +
			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Category</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.CATEGORY+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +
			 '\t\t\t\t\t\t\t\t\t<tr>\n' +
			 '\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Solution</td>\n' +
			 '\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.SOLUTION+'</td>\n' +
			 '\t\t\t\t\t\t\t\t\t</tr>\n' +
			 '\t\t\t\t\t\t\t\t\t<tr>\n' +
			 '\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Cabin Type</td>\n' +
			 '\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.CABIN_TYPE+'</td>\n' +
			 '\t\t\t\t\t\t\t\t\t</tr>\n' +
			 '\t\t\t\t\t\t\t\t\t<tr>\n' +
			 '\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">SI RESULT</td>\n' +
			 '\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data.SI_RESULT+'</td>\n' +
			 '\t\t\t\t\t\t\t\t\t</tr>\n' +
			'\t\t\t\t\t\t\t\t\t</tbody>\n' +
			'\t\t\t\t\t\t\t\t</table>';
			 html += view_more(color, title);
	} else {
		html = empty_card_result();
	}

	return html;
}

function set_card_detail_wireline(data = null, data1, title, color) {
	let html = '';
	let site = data1 === undefined ? null : data1.SITE_NAME;
	let nap = data1 === undefined ? null : data1.NAP_NUMBER;
	let cabinet = data === null ? null : data.CABINET_NAME;
	let loop = data === null ? null : data.LOOP_NAME;
	let tech = data === null ? null : data.TECH;
	let OMIP = data === null ? null : data.OMIP;
	let OMGW = data === null ? null : data.OMGW;
	let OMNW = data === null ? null : data.OMNW;
	let MSANTECH = data === null ? null : data.MSANTECH;
	let TECH = data === null ? null : data.TECH;
	if (data !== null || data1 !== undefined) {
		 html = '<table class="table table-bordered table-sm">\n' +
			'\t\t\t\t\t\t\t\t\t<tbody>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Site Name</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+site+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Cabinet Name</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+cabinet+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Loop Name</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+loop+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Tech. Installed</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+tech+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">OM IP</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+OMIP+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">OM Gateway</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+OMGW+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">OM Network</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+OMNW+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			 '\t\t\t\t\t\t\t\t\t<tr>\n' +
			 '\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">TECH</td>\n' +
			 '\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+TECH+'</td>\n' +
			 '\t\t\t\t\t\t\t\t\t</tr>\n' +

			 '\t\t\t\t\t\t\t\t\t<tr>\n' +
			 '\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">MSANTECH</td>\n' +
			 '\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+MSANTECH+'</td>\n' +
			 '\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">NAP Number</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+nap+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t</tbody>\n' +
			'\t\t\t\t\t\t\t\t</table>';
			 html += view_more(color, title);
	} else {
		html = empty_card_result();
	}
	return html;
}

function set_card_detail_rgpm(data, title, color) {
	let html = '';
	if (data.length > 0) {
		html = '\t\t\t\t\t\t\t\t<table class="table table-bordered table-sm">\n' +
			'\t\t\t\t\t\t\t\t\t<tbody>\n' +
			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Site Name</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data[0].SITENAME+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Date Approved</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data[0].DATE_APPROVED+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Date Checked</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data[0].DATE_CHECKED+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Date Endorsed</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data[0].DATE_ENDORSED+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Quarter</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data[0].QUARTER+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Project Name</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data[0].PROJECT_NAME+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Status</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data[0].STATUS+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Vendor</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data[0].VENDOR+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">Technology</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data[0].TECHNOLOGY+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t<tr>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="4" class="text-muted text-right bg-light">TRFS</td>\n' +
			'\t\t\t\t\t\t\t\t\t\t<td colspan="6">'+data[0].TRFS+'</td>\n' +
			'\t\t\t\t\t\t\t\t\t</tr>\n' +

			'\t\t\t\t\t\t\t\t\t</tbody>\n' +
			'\t\t\t\t\t\t\t\t</table>\n';
			html += view_more(color, title);
	} else {
		html = empty_card_result();
	}


	return html;
}

function view_more(color, title) {
	return '<button type="button" data-color='+color+' data-name='+title+' class="card-footer view_all_details btn btn-outline-info border-0 btn-block">View more details</button>';
}

function empty_results() {
	html = '<div class="col-12 mg-top-135">' +
		'<h5 class="text-center text-white">' +
		'<i class="fas fa-table display-3 text-muted"></i></br></br>' +
		'<small class="text-center text-muted mg-top-10"><em>Results will be displayed here.</em></small>' +
		'</h5>' +
		'</div>';
	$('#results').html(html);
}

function empty_card_result() {
	return '<div class="col-12 text-center" style="padding: 54px">' +
				'<h5 class="text-center text-white">' +
					'<i class="fas fa-sad-tear display-3 text-muted"></i><br>' +
				'</h5>' +
				'<p	class="card-title text-muted" style="font-size:41px">Sorry,</p>' +
				'<p class="text-center text-muted mg-top-10">We couldn\'t  find what you\'re searching.</p>' +
			'</div>';
}

$(document).on('click', '.view_all_details', function (e) {
	e.preventDefault();
	let html = '';
	let section = $(this).data('name');
	let color = $(this).data('color');

	switch (section) {
		case "RAN"  : html += set_tbl_detail_rre(rre_data); break;
		case "FACILITIES"  : html += set_tbl_detail_rfe(rfe_data); break;
		case "WIRELINE" : html += set_tbl_detail_wireline(rwe_data, rcne_data); break;
		case "RGPM" : html += set_tbl_detail_rgpm(rgpm_data); break;
	}

	$('#all_details').html(html);
	$("#title_udb").text(section +' Details');
	$('.udb-header').removeClass('bg-primary').removeClass('bg-warning').removeClass('bg-success').removeClass('bg-danger').addClass(color);
	$('#view_all_details').modal('show');
	$('.rre_tbl').dataTable({ responsive: true,"autoWidth": false});
	if (section === "RAN") {
		let coordinates = {
			lat : rre_data.rre_details.Latitude,
			lng : rre_data.rre_details.Longitude
		};
		set_map(coordinates);
	}
});

$(document).on('click', '#close_modal', function (e) {
	e.preventDefault();
	$('#view_all_details').modal('hide');
});

function set_tbl_detail_rre(data) {
	let html = '<div class="col">' +
					'<div class="row">' +
						'<div class="col-8">' +
								'\t\t\t\t\t\t\t\t<table class="table table-bordered table-sm bg-white">\n' +
								'\t\t\t\t\t\t\t\t\t<tbody class="small">\n' +
								'\t\t\t\t\t\t\t\t\t<tr>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="2" class="text-muted text-center bg-light">BTS Name</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td colspan="5" class="text-center">'+data.rre_details.BTS_Name+'</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="2" class="text-muted text-center bg-light">PlA ID</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="2">'+data.rre_details.PLAID+'</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="2" class="text-muted text-center bg-light">Deniro Name</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="3">'+data.rre_details.Deniro_Name+'</td>\n' +
								'\t\t\t\t\t\t\t\t\t</tr>\n' +

								'\t\t\t\t\t\t\t\t\t<tr>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td colspan="2" scope="row" class="text-muted text-center bg-light">Building Name</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td colspan="3" class="text-center">'+data.rre_details.Building_Name+'</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td colspan="2" scope="row" class="text-muted text-center bg-light">Barangay</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td colspan="3" class="text-center">'+data.rre_details.Baranggay+'</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="3" class="text-muted text-center bg-light">Municipal / Town</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td colspan="3" class="text-center">'+data.rre_details.Town+'</td>\n' +
								'\t\t\t\t\t\t\t\t\t</tr>\n' +

								'\t\t\t\t\t\t\t\t\t</tr>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td colspan="2" scope="row" class="text-muted text-center bg-light">Province</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td colspan="4" class="text-center">'+data.rre_details.Province+'</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td colspan="2" scope="row" class="text-muted text-center bg-light">Region</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="3">'+data.rre_details.Region+'</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="2" class="text-muted text-center bg-light">Cluster</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="3">'+data.rre_details.Cluster+'</td>\n' +
								'\t\t\t\t\t\t\t\t\t</tr>'+
								'\t\t\t\t\t\t\t\t\t<tr>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="2" class="text-muted text-center bg-light">Site Coverage</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="2">'+data.rre_details.Site_Coverage+'</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="2" class="text-muted text-center bg-light">Solution Type</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="2">'+data.rre_details.Solution_Type+'</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="2" class="text-muted text-center bg-light">Site Type</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="2">'+data.rre_details.Site_Type+'</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="2" class="text-muted text-center bg-light">Tower Type</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="2">'+data.rre_details.Tower_Type+'</td>\n' +
								'\t\t\t\t\t\t\t\t\t</tr>\n' +

								'\t\t\t\t\t\t\t\t\t<tr>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td scope="row" class="text-muted text-center bg-light" colspan="2">Coordinates</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="3" class="text-muted text-center bg-light">Latitutde</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="4">'+data.rre_details.Latitude+'</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="3" class="text-muted text-center bg-light">Longtitude</td>\n' +
								'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="4">'+data.rre_details.Longitude+'</td>\n' +
								'\t\t\t\t\t\t\t\t\t</tr>\n' +
								'\t\t\t\t\t\t\t\t\t</tbody>\n' +
								'\t\t\t\t\t\t\t\t</table>\n'+
						'</div>' +
						'<div class="col-4">' +
							'<div id="rre_map" class="shadow-sm border border-light" style="height: 100%"></div>' +
						'</div>' +
					'</div>' +
				'</div>';

			html += '<ul class="nav nav-tabs" role="tablist">\n';
				_.forEach(rre_tabs, function (val) {
					html += ' <li class="nav-item" role="presentation">\n' +
						'    <a class="nav-link '+val.status+'" id="'+val.id_name+'-tab" data-toggle="tab" href="#'+val.id_name+'" role="tab" aria-controls='+val.id_name+' aria-selected="true">'+val.name+'</a>\n' +
						'  </li>\n';
				});

			html += '</ul>\n' +

			'<div class="tab-content" id="myTabContent">\n';
				_.forEach(rre_tabs, function (val, i) {
					html += '  <div class="tab-pane rre_tabs fade show '+val.status+'" id='+val.id_name+' role="tabpanel" aria-labelledby="'+val.id_name+'-tab">';
					switch (parseInt(i)) {
						case 0  : html += set_twog_tab(data.two_g); break;
						case 1  : html += set_threeg_tab(data.three_g); break;
						case 2  : html += set_fourg_tab(data.nokia_four_g); break;
						case 3  : html += set_fourg_tab(data.ht_four_g); break;
					}

					html += '</div>\n';
				});
			html += '	</div>\n';
	return html;
}

function set_tbl_detail_rfe(data) {
	let html = '<ul class="nav nav-pills justify-content-end" role="tablist">\n';
	_.forEach(rfe_tabs_list, function (val) {
		html += ' <li class="nav-item" role="presentation">\n' +
			'    	<a class="nav-link '+val.status+'" id="'+val.id_name+'-tab" data-toggle="tab" href="#'+val.id_name+'" role="tab" aria-controls='+val.id_name+' aria-selected="true">'+val.name+'</a>\n' +
			' 	 </li>\n';
	});

	html += '</ul>\n' +

		'<div class="tab-content mg-top-10" id="myTabContent">\n';
	_.forEach(rfe_tabs_list, function (val, i) {
		html += '  <div class="tab-pane rre_tabs fade show '+val.status+'" id='+val.id_name+' role="tabpanel" aria-labelledby="'+val.id_name+'-tab">';
		switch (parseInt(i)) {
			case 0  :
					html += '<div class="card-columns">\n' ;
					html += set_site_details_card(data[0]);
					html += set_site_profile_card(data[0]);
					html += set_power_card(data[0]);
					html += set_si_update_card(data[0]);
					html += set_retro_update_card(data[0]);
					html += set_tssr_ptpa_update_card(data[0]);
					html += '</div>';
					break;
			case 1  :
					html += set_rfe_tables(data);
					break;
		}

		html += '</div>\n';
	});
	html += '</div>\n';
	return html;
}

function set_rfe_tables(data) {
	let count = 1;
	let	html = '<div class="mg-top-5">' +
		'<table class="table table-bordered table-responsive dt-responsive nowrap rre_tbl">\n' +
		' <thead class="text-muted small text-center">\n' +
		'    <tr>\n' +
		'      <th class="bg-secondary text-white" scope="col">No.</th>\n'+
		'      <th class="u-lightpink" scope="col">PLAID</th>\n' +
		'      <th class="u-lightpink" scope="col">Area</th>\n' +
		'      <th class="u-lightpink" scope="col">Sitename</th>\n' +
		'      <th class="u-lightpink" scope="col">Latitude</th>\n' +
		'      <th class="u-lightpink" scope="col">Longitude</th>\n' +
		'      <th class="u-lightpink" scope="col">Address</th>\n' +
		'      <th class="u-lightpink" scope="col">Region</th>\n' +
		'      <th class="u-lightpink" scope="col">Province</th>\n' +
		'      <th class="u-lightpink" scope="col">Town</th>\n' +
		'      <th class="u-lightgreen" scope="col">Solution</th>\n' +
		'      <th class="u-lightgreen" scope="col">Site Type</th>\n' +
		'      <th class="u-lightgreen" scope="col">Tower</th>\n' +
		'      <th class="u-lightgreen" scope="col">Tower Type</th>\n' +
		'      <th class="u-lightgreen" scope="col">Tower Height</th>\n' +
		'      <th class="u-lightgreen" scope="col">Hub</th>\n' +
		'      <th class="u-lightgreen" scope="col">Site Class</th>\n' +
		'      <th class="u-lightgreen" scope="col">Engineer</th>\n' +
		'      <th class="u-lightgreen" scope="col">Category</th>\n' +
		'      <th class="u-lightgreen" scope="col">VIP</th>\n' +
		'      <th class="u-lightgreen" scope="col">Profile</th>\n' +
		'      <th class="u-purple" scope="col">Lightning Protection</th>\n' +
		'      <th class="u-purple" scope="col">Cabin Count</th>\n' +
		'      <th class="u-purple" scope="col">Cabin Type</th>\n' +
		'      <th class="u-purple" scope="col">Cabin Dimension</th>\n' +
		'      <th class="u-purple" scope="col">Back-up Power</th>\n' +
		'      <th class="u-purple" scope="col">Genset Aging</th>\n' +
		'      <th class="u-purple" scope="col">Genset ATS</th>\n' +
		'      <th class="u-purple" scope="col">Daytank Capacity</th>\n' +
		'      <th class="u-purple" scope="col">Batteries</th>\n' +
		'      <th class="u-purple" scope="col">Battery Type</th>\n' +
		'      <th class="u-purple" scope="col">Rectifier Brand</th>\n' +
		'      <th class="u-purple" scope="col">Rectifier Module</th>\n' +
		'      <th class="u-purple" scope="col">Rectifier Capacity</th>\n' +
		'      <th class="u-purple" scope="col">Rectifier Load</th>\n' +
		'      <th class="u-purple" scope="col">DC Load</th>\n' +
		'      <th class="u-purple" scope="col">MCB Rating</th>\n' +
		'      <th class="u-purple" scope="col">AC Load</th>\n' +
		'      <th class="u-purple" scope="col">ACU</th>\n' +
		'      <th class="u-purple" scope="col">ATS</th>\n' +
		'      <th class="u-purple" scope="col">Transformer</th>\n' +
		'      <th class="u-purple" scope="col">Genset</th>\n' +
		'      <th class="u-purple" scope="col">MCB ECB</th>\n' +
		'      <th class="u-purple" scope="col">ACPDB</th>\n' +
		'      <th class="u-purple" scope="col">FMS</th>\n' +
		'      <th class="u-purple" scope="col">Commercial Power Provider</th>\n' +
		'      <th class="u-purple" scope="col">Power System Phase</th>\n' +
		'      <th class="u-purple" scope="col">Power Account Number</th>\n' +
		'      <th class="u-purple" scope="col">Power Connection Type</th>\n' +
		'      <th class="u-purple" scope="col">Transformer Capacity</th>\n' +
		'      <th class="u-purple" scope="col">Grid Status</th>\n' +
		'      <th class="bg-success text-white" scope="col">Latest Covering FS Project</th>\n' +
		'      <th class="bg-success text-white" scope="col">Project RFS Date</th>\n' +
		'      <th class="bg-success text-white" scope="col">Facility Remarks</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Date Received</th>\n' +
		'      <th class="bg-primary text-white" scope="col">SI Update Project</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Vendor</th>\n' +
		'      <th class="bg-primary text-white" scope="col">NSCP Edition</th>\n' +
		'      <th class="bg-primary text-white" scope="col">SI Date</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Current UR PTA</th>\n' +
		'      <th class="bg-primary text-white" scope="col">SI UR</th>\n' +
		'      <th class="bg-primary text-white" scope="col">SI Result Existing Proposed</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Retro Details Existing Proposed</th>\n' +
		'      <th class="bg-primary text-white" scope="col">W/ Accessories Replacement</th>\n' +
		'      <th class="bg-primary text-white" scope="col">SI Result Proposed Future</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Retro Details Existing Proposed Future</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Existing UR</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Existing Pro UR</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Existing Pro Future UR</th>\n' +
		'      <th class="bg-primary text-white" scope="col">GT Comments</th>\n' +
		'      <th class="bg-danger text-white" scope="col">SI Result</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Approved for Rev</th>\n' +
		'      <th class="bg-primary text-white" scope="col">EWP Released</th>\n' +
		'      <th class="bg-primary text-white" scope="col">SI Update Remarks</th>\n' +
		'      <th class="u-lightpink" scope="col">Latest Retro Owner</th>\n' +
		'      <th class="bg-danger text-white" scope="col">Date of Last Retrofitting</th>\n' +
		'      <th class="bg-danger text-white" scope="col">Retro Status</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Retro Update Remarks</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Latest Tower Status SI Retro</th>\n' +
		'      <th class="bg-primary text-white" scope="col">NSCP 7 Compliance Governed by</th>\n' +
		'      <th class="bg-danger text-white" scope="col">Tower Capacity</th>\n' +
		'      <th class="bg-warning text-white" scope="col">TSSR PTA Update Project</th>\n' +
		'      <th class="bg-warning text-white" scope="col">Latest TSSR Approved Date</th>\n' +
		'      <th class="bg-warning text-white" scope="col">Latest PTA Tower Utilization Ration</th>\n' +
		'      <th class="bg-warning text-white" scope="col">TSSR PTA Remarks</th>\n' +
		'      <th class="bg-info text-white" scope="col">2019 Wireless Scope</th>\n' +
		'      <th class="bg-info text-white" scope="col">2020 Wireless Scope</th>\n' +
		'      <th class="bg-secondary text-white" scope="col">PTA SI Retro Summary</th>\n' +
		'      <th class="bg-secondary text-white" scope="col">Tower Status Summary Remarks</th>\n' +
		'      <th class="bg-secondary text-white" scope="col">Current Tower Status Capacity</th>\n' +
		'      <th class="u-lightpink" scope="col">Ongoing Tower Activity Future Req</th>\n' +
		'      <th class="u-orange" scope="col">Note</th>\n' +
		'      <th class="u-orange" scope="col">Typhoon Path</th>\n' +
		'      <th class="u-orange" scope="col">PTA Update Remarks</th>\n' +
		'      <th class="u-orange" scope="col">Date Updated</th>\n' +
		'      <th class="u-orange" scope="col">Additional Site</th>\n' +
		'      <th class="u-orange" scope="col">Site and Tower Info</th>\n' +
		'      <th class="u-orange" scope="col">PTA TSSR</th>\n' +
		'      <th class="u-orange" scope="col">SI</th>\n' +
		'      <th class="u-orange" scope="col">Retro</th>\n' +
		'      <th class="bg-warning text-white" scope="col">FE Remarks</th>\n' +
		'      <th class="u-lightpink" scope="col">SI Year</th>\n' +
		'      <th class="u-lightpink" scope="col">Retro Completion Year</th>\n' +
		'    </tr>\n' +
		'  </thead>' +
		'<tbody>';
	_.forEach(data,function (val) {
		html += ' <tr class="text-center small">\n' +
			'      <td scope="row">'+count+++'.</td>\n' +
			'      <td>'+val.PLAID+'</td>\n' +
			'      <td>'+val.AREA+'</td>\n' +
			'      <td>'+val.SITE_NAME+'</td>\n' +
			'      <td>'+val.LATITUDE+'</td>\n' +
			'      <td>'+val.LONGITUDE+'</td>\n' +
			'      <td>'+val.ADDRESS+'</td>\n' +
			'      <td>'+val.REGION+'</td>\n' +
			'      <td>'+val.PROVINCE+'</td>\n' +
			'      <td>'+val.TOWN+'</td>\n' +
			'      <td>'+val.SOLUTION+'</td>\n' +
			'      <td>'+val.SITE_TYPE+'</td>\n' +
			'      <td>'+val.TOWER+'</td>\n' +
			'      <td>'+val.TOWER_TYPE+'</td>\n' +
			'      <td>'+val.TOWER_HEIGHT+'</td>\n' +
			'      <td>'+val.HUB+'</td>\n' +
			'      <td>'+val.SITE_CLASS+'</td>\n' +
			'      <td>'+val.SITE_ENGINEER+'</td>\n' +
			'      <td>'+val.CATEGORY+'</td>\n' +
			'      <td>'+val.VIP+'</td>\n' +
			'      <td>'+val.PROFILE+'</td>\n' +
			'      <td>'+val.LIGHTNING_PROTECTION+'</td>\n' +
			'      <td>'+val.CABIN_COUNT+'</td>\n' +
			'      <td>'+val.CABIN_TYPE+'</td>\n' +
			'      <td>'+val.CABIN_DIMENSION+'</td>\n' +
			'      <td>'+val.BACK_UP_POWER+'</td>\n' +
			'      <td>'+val.GENSET_AGING+'</td>\n' +
			'      <td>'+val.GENSET_ATS+'</td>\n' +
			'      <td>'+val.DAYTANK_CAPACITY+'</td>\n' +
			'      <td>'+val.BATTERIES+'</td>\n' +
			'      <td>'+val.BATTERY_TYPE+'</td>\n' +
			'      <td>'+val.RECTIFIER_BRAND+'</td>\n' +
			'      <td>'+val.RECTIFIER_MODULE+'</td>\n' +
			'      <td>'+val.RECTIFIER_CAPACITY+'</td>\n' +
			'      <td>'+val.RECTIFIER_LOAD+'</td>\n' +
			'      <td>'+val.DC_LOAD+'</td>\n' +
			'      <td>'+val.MCB_RATING+'</td>\n' +
			'      <td>'+val.AC_LOAD+'</td>\n' +
			'      <td>'+val.ACU+'</td>\n' +
			'      <td>'+val.ACU_ATS+'</td>\n' +
			'      <td>'+val.TRANSFORMER_UR+'</td>\n' +
			'      <td>'+val.GENSET_UR+'</td>\n' +
			'      <td>'+val.MCB_ECB_UR+'</td>\n' +
			'      <td>'+val.ACPDB_UR+'</td>\n' +
			'      <td>'+val.FMS+'</td>\n' +
			'      <td>'+val.COMMERCIAL_POWER_PROVIDER+'</td>\n' +
			'      <td>'+val.POWER_SYSTEM_PHASE+'</td>\n' +
			'      <td>'+val.POWER_ACCOUNT_NUMBER+'</td>\n' +
			'      <td>'+val.POWER_CONNECTION_TYPE+'</td>\n' +
			'      <td>'+val.TRANSFORMER_CAPACITY+'</td>\n' +
			'      <td>'+val.GRID_STATUS+'</td>\n' +
			'      <td>'+val.LATEST_COVERING_FS_PROJECT+'</td>\n' +
			'      <td>'+val.PROJECT_RFS_DATE+'</td>\n' +
			'      <td>'+val.FACILITES_REMARKS+'</td>\n' +
			'      <td>'+val.DATE_RECEIVED+'</td>\n' +
			'      <td>'+val.SI_UPDATE_PROJECT+'</td>\n' +
			'      <td>'+val.VENDOR+'</td>\n' +
			'      <td>'+val.NSCP_EDITION+'</td>\n' +
			'      <td>'+val.SI_DATE+'</td>\n' +
			'      <td>'+val.CURRENT_UR_PTA+'</td>\n' +
			'      <td>'+val.SI_UR+'</td>\n' +
			'      <td>'+val.SI_RESULT_EXISTING_PROPOSED+'</td>\n' +
			'      <td>'+val.RETRO_DETAILS_EXISTING_PROPOSED+'</td>\n' +
			'      <td>'+val.WITH_ACCESSORIES_REPLACEMENT+'</td>\n' +
			'      <td>'+val.SI_RESULT_EXISTING_PROPOSED_FUTURE+'</td>\n' +
			'      <td>'+val.RETRO_DETAILS_EXISTING_PROPOSED_FUTURE+'</td>\n' +
			'      <td>'+val.EXISTING_UR+'</td>\n' +
			'      <td>'+val.EXISTING_PRO_UR+'</td>\n' +
			'      <td>'+val.EXT_PROP_FUT_UR+'</td>\n' +
			'      <td>'+val.GT_COMMENTS+'</td>\n' +
			'      <td>'+val.SI_RESULT+'</td>\n' +
			'      <td>'+val.APPROVED_FOR_REV+'</td>\n' +
			'      <td>'+val.EWP_RELEASED+'</td>\n' +
			'      <td>'+val.SI_UPDATE_REMARKS+'</td>\n' +
			'      <td>'+val.LATEST_RETRO_OWNER+'</td>\n' +
			'      <td>'+val.DATE_OF_LAST_RETROFITTING+'</td>\n' +
			'      <td>'+val.RETRO_STATUS+'</td>\n' +
			'      <td>'+val.RETRO_UPDATE_REMARKS+'</td>\n' +
			'      <td>'+val.LATEST_TOWER_STATUS_SI_RETRO+'</td>\n' +
			'      <td>'+val.NSCP_7_COMPLIANCE_GOVERNED_BY+'</td>\n' +
			'      <td>'+val.TOWER_CAPACITY+'</td>\n' +
			'      <td>'+val.TSSR_PTA_UPDATE_PROJECT+'</td>\n' +
			'      <td>'+val.LATEST_TSSR_APPROVED_DATE+'</td>\n' +
			'      <td>'+val.TSSR_PTA_TOWER_UTILIZATION_RATIO+'</td>\n' +
			'      <td>'+val.TSSR_PTA_REMARKS+'</td>\n' +
			'      <td>'+val.W_2019_WIRELESS_SCOPE+'</td>\n' +
			'      <td>'+val.W_2020_WIRELESS_SCOPE+'</td>\n' +
			'      <td>'+val.PTA_SI_RETRO_SUMMARY+'</td>\n' +
			'      <td>'+val.TOWER_STATUS_SUMMARY_REMARKS+'</td>\n' +
			'      <td>'+val.CURRENT_TOWER_STATUS_CAPACITY+'</td>\n' +
			'      <td>'+val.ONGOING_TOWER_ACTIVITY_FUTURE_REQ+'</td>\n' +
			'      <td>'+val.NOTE+'</td>\n' +
			'      <td>'+val.TYPHOON_PATH+'</td>\n' +
			'      <td>'+val.PTA_Updating_Remarks+'</td>\n' +
			'      <td>'+val.Date_Updated+'</td>\n' +
			'      <td>'+val.Additional_Site+'</td>\n' +
			'      <td>'+val.Site_and_Tower_Info+'</td>\n' +
			'      <td>'+val.PTA_TSSR+'</td>\n' +
			'      <td>'+val.SI+'</td>\n' +
			'      <td>'+val.Retro+'</td>\n' +
			'      <td>'+val.FE_Remarks+'</td>\n' +
			'      <td>'+val.SI_YEAR+'</td>\n' +
			'      <td>'+val.RETRO_COMPLETION_YEAR+'</td>\n' +
			'</tr>';
	});
	html += '</tbody>' +
		'\t\t\t\t\t\t\t\t</table>\n'+
		'</div>';

	return html;
}

function set_tbl_detail_wireline(wire = [], cable) {
	console.log(cable);
	let html = '<ul class="nav nav-tabs" role="tablist">\n';
	_.forEach(wire_tabs, function (val) {
		html += ' <li class="nav-item" role="presentation">\n' +
			'    <a class="nav-link '+val.status+'" id="'+val.id_name+'-tab" data-toggle="tab" href="#'+val.id_name+'" role="tab" aria-controls='+val.id_name+' aria-selected="true">'+val.name+'</a>\n' +
			'  </li>\n';
	});

	html += '</ul>\n' +

	'<div class="tab-content" id="myTabContent">\n';
		_.forEach(wire_tabs, function (val, i) {
		html += '  <div class="tab-pane rre_tabs fade show '+val.status+'" id='+val.id_name+' role="tabpanel" aria-labelledby="'+val.id_name+'-tab">';
		switch (parseInt(i)) {
			case 0  : html += set_rwe(wire); break;
			case 1  : html += set_cable(cable); break;
		}

		html += '</div>\n';
	});
	html += '	</div>\n';

	return html;
}

function set_rwe(data) {
	let html = '';
	if (data !== null && data !== undefined && data.length > 0) {
		html += '<ul class="nav nav-pills justify-content-end mg-top-10" role="tablist">\n';
		_.forEach(rwe_tabs_list, function (val) {
			html += ' <li class="nav-item" role="presentation">\n' +
				'    <a class="nav-link '+val.status+'" id="'+val.id_name+'-tab" data-toggle="tab" href="#'+val.id_name+'" role="tab" aria-controls='+val.id_name+' aria-selected="true">'+val.name+'</a>\n' +
				'  </li>\n';
		});

		html += '</ul>\n' +
			'<div class="tab-content" id="myTabContent">\n';
		_.forEach(rwe_tabs_list, function (val, i) {
			html += '  <div class="tab-pane rre_tabs fade show '+val.status+'" id='+val.id_name+' role="tabpanel" aria-labelledby="'+val.id_name+'-tab">';
			switch (parseInt(i)) {
				case 0  :
						html += '<div class="card-columns mg-top-10">\n' ;
						html += set_primary_details_card(data[0]);
						html += set_secondary_details_card(data[0]);
						html += set_om_details_card(data[0]);
						html += set_data_details_card(data[0]);
						html += set_voice_details_card(data[0]);
						html += set_sigip_details_card(data[0]);
						html += set_media_details_card(data[0]);
						html += set_cabinet_details_card(data[0]);
						html += '</div>';
						break;
				case 1  : html += set_rwe_tables(data); break;

			}

			html += '</div>\n';
		});
		html += '	</div>\n';
	} else {
		html = empty_card_result();
	}

	return html;
}

function set_cable(data) {
	let num = 1;
	let	html = '<div class="mg-top-5 col-12">' +
		'<table class="table table-bordered table-responsive dt-responsive nowrap rre_tbl">\n' +
		' <thead class="text-muted text-center">\n' +
		'    <tr>\n' +
		'      <th class="bg-success text-white" scope="col">No.</th>\n' +
		'      <th class="bg-success text-white" scope="col">Site Name</th>\n' +
		'      <th class="bg-success text-white" scope="col">PLAID</th>\n' +
		'      <th class="bg-success text-white" scope="col">NAP Number</th>\n' +
		'      <th class="bg-success text-white" scope="col">NAP Lines</th>\n' +
		'      <th class="bg-success text-white" scope="col">Location Address</th>\n' +
		'      <th class="bg-success text-white" scope="col">Latitude</th>\n' +
		'      <th class="bg-success text-white" scope="col">Longitude</th>\n' +
		'    </tr>\n' +
		'  </thead>' +
		'<tbody>';
	_.forEach(data, function (val) {
		html += ' <tr class="text-center">\n' +
			'      <td>'+num+++'</td>\n' +
			'      <td>'+val.SITE_NAME+'</td>\n' +
			'      <td>'+val.PLAID+'</td>\n' +
			'      <td>'+val.NAP_NUMBER+'</td>\n' +
			'      <td>'+val.NAP_LINES+'</td>\n' +
			'      <td>'+val.LOCATION_ADDRESS+'</td>\n' +
			'      <td>'+val.LATITUDE+'</td>\n' +
			'      <td>'+val.LONGITUDE+'</td>\n' +
			'</tr>';
	});
	html += '</tbody>' +
		'\t\t\t\t\t\t\t\t</table>\n'+
		'</div>';

	return html;
}

function set_tbl_detail_rgpm(data) {
	let num = 1;
	let	html = '<div class="mg-top-5">' +
		'\t\t\t\t\t\t\t\t<table class="table table-bordered table-responsive dt-responsive nowrap rre_tbl">\n' +
		' <thead class="text-muted small text-center thead-primary">\n' +
		'    <tr>\n' +
		'      <th class="bg-info text-white" scope="col">No.</th>\n' +
		'      <th class="bg-info text-white" scope="col">Site Name</th>\n' +
		'      <th class="bg-info text-white" scope="col">PLAID</th>\n' +
		'      <th class="bg-info text-white" scope="col">Endorsed</th>\n' +
		'      <th class="bg-info text-white" scope="col">Checked</th>\n' +
		'      <th class="bg-info text-white" scope="col">Quarter</th>\n' +
		'      <th class="bg-info text-white" scope="col">Resubmission</th>\n' +
		'      <th class="bg-info text-white" scope="col">Date</th>\n' +
		'      <th class="bg-info text-white" scope="col">Date Approv.</th>\n' +
		'      <th class="bg-info text-white" scope="col">Vendor</th>\n' +
		'      <th class="bg-info text-white" scope="col">Tech.</th>\n' +
		'      <th class="bg-info text-white" scope="col">Project</th>\n' +
		'      <th class="bg-info text-white" scope="col">Area</th>\n' +
		'      <th class="bg-info text-white" scope="col">Status</th>\n' +
		'      <th class="bg-info text-white" scope="col">Type </th>\n' +
		'      <th class="bg-info text-white" scope="col">TRFS</th>\n' +
		'      <th class="bg-info text-white" scope="col">CRFS</th>\n' +
		'    </tr>\n' +
		'  </thead>' +
		'<tbody>';
		_.forEach(data, function (val) {
			html += ' <tr class="text-center small">\n' +
				'      <td>'+num+++'</td>\n' +
				'      <td>'+val.SITENAME+'</td>\n' +
				'      <td>'+val.PLAID+'</td>\n' +
				'      <td>'+formatDate(val.DATE_ENDORSED)+'</td>\n' +
				'      <td>'+formatDate(val.DATE_CHECKED)+'</td>\n' +
				'      <td>'+val.QUARTER+'</td>\n' +
				'      <td>'+val.RESUBMISSION_NEEDED+'</td>\n' +
				'      <td>'+formatDate(val.RESUBMISSION_DATE)+'</td>\n' +
				'      <td>'+val.DATE_APPROVED+'</td>\n' +
				'      <td>'+val.VENDOR+'</td>\n' +
				'      <td>'+val.TECHNOLOGY+'</td>\n' +
				'      <td>'+val.PROJECT_NAME+'</td>\n' +
				'      <td>'+val.Area+'</td>\n' +
				'      <td>'+val.STATUS+'</td>\n' +
				'      <td>'+val.TYPE+'</td>\n' +
				'      <td>'+formatDate(val.TRFS)+'</td>\n' +
				'      <td>'+formatDate(val.CRFS)+'</td>\n' +
				'</tr>';
		});
	html += '</tbody>' +
		'\t\t\t\t\t\t\t\t</table>\n'+
		'</div>';

	return html;
}

function set_rwe_tables(data) {
	let count = 1;
	let	html = '<div class="mg-top-5">' +
		'\t\t\t\t\t\t\t\t<table class="table table-bordered table-responsive dt-responsive nowrap rre_tbl">\n' +
		' <thead class="text-muted small text-center">\n' +
		'    <tr>\n' +
		'      <th class="bg-secondary text-white" scope="col">No.</th>\n'+
		'      <th class="bg-primary text-white" scope="col">PLAID</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Loop Name</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Latitude</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Longitude</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Address</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Barangay</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Municipality</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Province</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Region</th>\n' +
		'      <th class="bg-primary text-white" scope="col">Geoarea</th>\n' +
		'      <th class="bg-success text-white" scope="col">AG Homing 1</th>\n' +
		'      <th class="bg-success text-white" scope="col">AG Homing 2</th>\n' +
		'      <th class="bg-success text-white" scope="col">AG Port 2</th>\n' +
		'      <th class="bg-success text-white" scope="col">ATN Port</th>\n' +
		'      <th class="bg-success text-white" scope="col">ATN Type</th>\n' +
		'      <th class="bg-success text-white" scope="col">Uplink Capacity</th>\n' +
		'      <th class="bg-success text-white" scope="col">Configuration Type</th>\n' +
		'      <th class="bg-success text-white" scope="col">Technology Installed</th>\n' +
		'      <th class="bg-success text-white" scope="col">MSAN Technology</th>\n' +
		'      <th class="bg-success text-white" scope="col">DSLAM Type</th>\n' +
		'      <th class="bg-success text-white" scope="col">Hardware Version</th>\n' +
		'      <th class="bg-warning text-white" scope="col">OM IP</th>\n' +
		'      <th class="bg-warning text-white" scope="col">OM Gateway</th>\n' +
		'      <th class="bg-warning text-white" scope="col">OM Network</th>\n' +
		'      <th class="bg-warning text-white" scope="col">OM Subnet</th>\n' +
		'      <th class="bg-warning text-white" scope="col">OM Vlan</th>\n' +
		'      <th class="u-purple text-white" scope="col">Data Vlan</th>\n' +
		'      <th class="u-purple text-white" scope="col">BNG Homing</th>\n' +
		'      <th class="u-purple text-white" scope="col">Data Leased Line</th>\n' +
		'      <th class="u-purple text-white" scope="col">Date Static</th>\n' +
		'      <th class="u-purple text-white" scope="col">IPOE VLAN</th>\n' +
		'      <th class="bg-danger text-white" scope="col">SIP VLAN</th>\n' +
		'      <th class="bg-danger text-white" scope="col">SIP From</th>\n' +
		'      <th class="bg-danger text-white" scope="col">SIP To</th>\n' +
		'      <th class="bg-danger text-white" scope="col">SIP Network</th>\n' +
		'      <th class="bg-danger text-white" scope="col">SIP Gateway</th>\n' +
		'      <th class="bg-danger text-white" scope="col">SIP Subnet</th>\n' +
		'      <th class="bg-danger text-white" scope="col">SIP Homing</th>\n' +
		'      <th class="u-brown text-white" scope="col">Signaling Gateway</th>\n' +
		'      <th class="u-brown text-white" scope="col">Signaling IP</th>\n' +
		'      <th class="u-brown text-white" scope="col">Signaling Network</th>\n' +
		'      <th class="u-brown text-white" scope="col">Signaling Subnet</th>\n' +
		'      <th class="u-brown text-white" scope="col">Signaling VLAN</th>\n' +
		'      <th class="u-pink text-white" scope="col">Media Gateway</th>\n' +
		'      <th class="u-pink text-white" scope="col">Media IP</th>\n' +
		'      <th class="u-pink text-white" scope="col">Media Network</th>\n' +
		'      <th class="u-pink text-white" scope="col">Media Subnet</th>\n' +
		'      <th class="u-pink text-white" scope="col">Media VLAN</th>\n' +
		'      <th class="bg-secondary text-white" scope="col">Cabinet Environment</th>\n' +
		'      <th class="bg-secondary text-white" scope="col">MSAN Uplink</th>\n' +
		'      <th class="bg-secondary text-white" scope="col">ZONEID</th>\n' +
		'      <th class="bg-secondary text-white" scope="col">TOPBOX</th>\n' +
		'      <th class="bg-secondary text-white" scope="col">Rectifier CO</th>\n' +
		'      <th class="bg-secondary text-white" scope="col">Project Name</th>\n' +
		'      <th class="bg-secondary text-white" scope="col">RFS Date</th>\n' +
		'      <th class="bg-secondary text-white" scope="col">Status</th>\n' +
		'      <th class="bg-secondary text-white" scope="col">Vendor</th>\n' +
		'    </tr>\n' +
		'  </thead>' +
		'<tbody>';
	_.forEach(data,function (val) {
		html += ' <tr class="text-center small">\n' +
			'      <td scope="row">'+count+++'.</td>\n' +
			'      <td>'+val.PLAID+'</td>\n' +
			'      <td>'+val.LOOP_NAME+'</td>\n' +
			'      <td>'+val.LATITUDE+'</td>\n' +
			'      <td>'+val.LONGITUDE+'</td>\n' +
			'      <td>'+val.SITE_ADDRESS+'</td>\n' +
			'      <td>'+val.BARANGGAY_DESC+'</td>\n' +
			'      <td>'+val.MUNICIPALITY_DESC+'</td>\n' +
			'      <td>'+val.PROVINCE_DESC+'</td>\n' +
			'      <td>'+val.REGION_DESC+'</td>\n' +
			'      <td>'+val.REGION+'</td>\n' +
			'      <td>'+val.AGHOM1+'</td>\n' +
			'      <td>'+val.AGHOM2+'</td>\n' +
			'      <td>'+val.AGPORT2+'</td>\n' +
			'      <td>'+val.ATNPORT+'</td>\n' +
			'      <td>'+val.ATNTYP+'</td>\n' +
			'      <td>'+val.UPCAP+'</td>\n' +
			'      <td>'+val.CONFTYPE+'</td>\n' +
			'      <td>'+val.TECH+'</td>\n' +
			'      <td>'+val.MSANTECH+'</td>\n' +
			'      <td>'+val.DSLAMTYP+'</td>\n' +
			'      <td>'+val.HARDWARE+'</td>\n' +
			'      <td>'+val.OMIP+'</td>\n' +
			'      <td>'+val.OMGW+'</td>\n' +
			'      <td>'+val.OMNW+'</td>\n' +
			'      <td>'+val.OMSN+'</td>\n' +
			'      <td>'+val.OMVLAN+'</td>\n' +
			'      <td>'+val.DATAVLAN+'</td>\n' +
			'      <td>'+val.DATABNG+'</td>\n' +
			'      <td>'+val.DATALL+'</td>\n' +
			'      <td>'+val.DATASTATIC+'</td>\n' +
			'      <td>'+val.DATASVLAN+'</td>\n' +
			'      <td>'+val.VOICEVLAN+'</td>\n' +
			'      <td>'+val.VOICEFROM+'</td>\n' +
			'      <td>'+val.VOICETO+'</td>\n' +
			'      <td>'+val.SIP_NETWORK+'</td>\n' +
			'      <td>'+val.SIP_GATEWAY+'</td>\n' +
			'      <td>'+val.VOICESN+'</td>\n' +
			'      <td>'+val.VOICEHOM+'</td>\n' +
			'      <td>'+val.SIGGW+'</td>\n' +
			'      <td>'+val.SIGIP+'</td>\n' +
			'      <td>'+val.SIGNW+'</td>\n' +
			'      <td>'+val.SIGSN+'</td>\n' +
			'      <td>'+val.SIGVLAN+'</td>\n' +
			'      <td>'+val.MEDIAGW+'</td>\n' +
			'      <td>'+val.MEDIAIP+'</td>\n' +
			'      <td>'+val.MEDIANW+'</td>\n' +
			'      <td>'+val.MEDIASN+'</td>\n' +
			'      <td>'+val.MEDIAVLAN+'</td>\n' +
			'      <td>'+val.CABINET+'</td>\n' +
			'      <td>'+val.MSAN+'</td>\n' +
			'      <td>'+val.ZONEID+'</td>\n' +
			'      <td>'+val.TOPBOX+'</td>\n' +
			'      <td>'+val.RECTIFIER+'</td>\n' +
			'      <td>'+val.PROJNAME+'</td>\n' +
			'      <td>'+val.RFS_DATE+'</td>\n' +
			'      <td>'+val.Status+'</td>\n' +
			'      <td>'+val.Vendor+'</td>\n' +
			'</tr>';
	});
	html += '</tbody>' +
		'\t\t\t\t\t\t\t\t</table>\n'+
		'</div>';

	return html;
}

//-----------------------------------------------//

function set_twog_tab(data) {
	let	html = '<div class="mg-top-5">' +
		'\t\t\t\t\t\t\t\t<table class="table table-bordered table-responsive dt-responsive nowrap rre_tbl">\n' +
		' <thead class="text-muted small text-center">\n' +
		'    <tr>\n' +
		'      <th scope="col">Cell Name</th>\n' +
		'      <th scope="col">4RFS</th>\n' +
		'      <th scope="col">Cell ID</th>\n' +
		'      <th scope="col">Sector</th>\n' +
		'      <th scope="col">BTS Name</th>\n' +
		'      <th scope="col">BCF Name</th>\n' +
		'      <th scope="col">BSC Name (Homing)</th>\n' +
		'      <th scope="col">Latitude</th>\n' +
		'      <th scope="col">Longitude</th>\n' +
		'      <th scope="col">Freq. Band</th>\n' +
		'      <th scope="col">Dual Beam</th>\n' +
		'      <th scope="col">MIMO</th>\n' +
		'      <th scope="col">BCCH Frequency</th>\n' +
		'      <th scope="col">No BCCH Freq list</th>\n' +
		'      <th scope="col">Antenna Center Line</th>\n' +
		'      <th scope="col">Azimuth</th>\n' +
		'      <th scope="col">Mechanical Tilt</th>\n' +
		'      <th scope="col">Electrical Tilt</th>\n' +
		'      <th scope="col">Cell Coverage</th>\n' +
		'      <th scope="col">Site Type</th>\n' +
		'      <th scope="col">Cell Type</th>\n' +
		'      <th scope="col">Location area code</th>\n' +
		'      <th scope="col">Routing Area Code</th>\n' +
		'      <th scope="col">Mobile network code</th>\n' +
		'      <th scope="col">Mobile country code</th>\n' +
		'      <th scope="col">BTS color code</th>\n' +
		'      <th scope="col">Network color code</th>\n' +
		'    </tr>\n' +
		'  </thead>' +
		'<tbody>';
		_.forEach(data,function (val) {
			html += ' <tr class="text-center small">\n' +
				'      <td scope="row">'+val.Cell_Name+'</td>\n' +
				'      <td>'+val.RFS+'</td>\n' +
				'      <td>'+val.Cell_ID+'</td>\n' +
				'      <td>'+val.Sector+'</td>\n' +
				'      <td>'+val.BTS_Name+'</td>\n' +
				'      <td>'+val.BCF_Name+'</td>\n' +
				'      <td>'+val.BSC_Name+'</td>\n' +
				'      <td>'+val.Latitude+'</td>\n' +
				'      <td>'+val.Latitude+'</td>\n' +
				'      <td>'+val.Freq_Band+'</td>\n' +
				'      <td>'+val.Dual_Beam+'</td>\n' +
				'      <td>'+val.MIMO+'</td>\n' +
				'      <td>'+val.BCCH_Frequency+'</td>\n' +
				'      <td>'+val.No_BCCH_Freq_list+'</td>\n' +
				'      <td>'+val.Antenna_Center_Line+'</td>\n' +
				'      <td>'+val.Azimuth+'</td>\n' +
				'      <td>'+val.Mechanical_Tilt+'</td>\n' +
				'      <td>'+val.Electrical_Tilt+'</td>\n' +
				'      <td>'+val.Cell_Coverage+'</td>\n' +
				'      <td>'+val.Site_Type+'</td>\n' +
				'      <td>'+val.Cell_Type+'</td>\n' +
				'      <td>'+val.Location_area_code+'</td>\n' +
				'      <td>'+val.Routing_Area_Code+'</td>\n' +
				'      <td>'+val.Mobile_network_code+'</td>\n' +
				'      <td>'+val.Mobile_country_code+'</td>\n' +
				'      <td>'+val.BTS_color_code+'</td>\n' +
				'      <td>'+val.Network_color_code+'</td>\n' +
				'</tr>';
		});
	 html += '</tbody>' +
	'\t\t\t\t\t\t\t\t</table>\n'+
	'</div>';

	return html;
}

function set_threeg_tab(data) {
	let	html = '<div class="mg-top-5">' +
		'\t\t\t\t\t\t\t\t<table class="table table-bordered table-responsive dt-responsive nowrap rre_tbl">\n' +
		' <thead class="text-muted small text-center">\n' +
		'    <tr>\n' +
		'      <th scope="col">Cell Name</th>\n' +
		'      <th scope="col">4RFS</th>\n' +
		'      <th scope="col">Cell ID</th>\n' +
		'      <th scope="col">Sector</th>\n' +
		'      <th scope="col">BTS Name</th>\n' +
		'      <th scope="col">NodeB Name</th>\n' +
		'      <th scope="col">RNC Name (Homing)</th>\n' +
		'      <th scope="col">Latitude</th>\n' +
		'      <th scope="col">Longitude</th>\n' +
		'      <th scope="col">Freq. Band</th>\n' +
		'      <th scope="col">Dual Beam</th>\n' +
		'      <th scope="col">MIMO</th>\n' +
		'      <th scope="col">Carrier</th>\n' +
		'      <th scope="col">Band</th>\n' +
		'      <th scope="col">Antenna Center Line</th>\n' +
		'      <th scope="col">Azimuth</th>\n' +
		'      <th scope="col">Mechanical Tilt</th>\n' +
		'      <th scope="col">Electrical Tilt</th>\n' +
		'      <th scope="col">Cell Coverage</th>\n' +
		'      <th scope="col">Site Type</th>\n' +
		'      <th scope="col">Cell Type</th>\n' +
		'      <th scope="col">Uplink UARFCN</th>\n' +
		'      <th scope="col">Downlink UARFCN</th>\n' +
		'      <th scope="col">DL Primary Scrambling Code</th>\n' +
		'      <th scope="col">Location area code</th>\n' +
		'      <th scope="col">Routing Area Code</th>\n' +
		'      <th scope="col">Service Area Code</th>\n' +
		'      <th scope="col">Local Cell ID</th>\n' +
		'    </tr>\n' +
		'  </thead>' +
		'<tbody>';
	_.forEach(data, function (val) {
		html += ' <tr class="text-center small">\n' +
			'      <td scope="row">'+val.Cell_Name+'</td>\n' +
			'      <td>'+val.RFS+'</td>\n' +
			'      <td>'+val.Cell_ID+'</td>\n' +
			'      <td>'+val.Sector+'</td>\n' +
			'      <td>'+val.BTS_Name+'</td>\n' +
			'      <td>'+val.NodeB_Name+'</td>\n' +
			'      <td>'+val.RNC_Name+'</td>\n' +
			'      <td>'+val.Latitude+'</td>\n' +
			'      <td>'+val.Longitude+'</td>\n' +
			'      <td>'+val.Freq_Band+'</td>\n' +
			'      <td>'+val.Dual_Beam+'</td>\n' +
			'      <td>'+val.MIMO+'</td>\n' +
			'      <td>'+val.Carrier+'</td>\n' +
			'      <td>'+val.Band+'</td>\n' +
			'      <td>'+val.Antenna_Center_Line+'</td>\n' +
			'      <td>'+val.Azimuth+'</td>\n' +
			'      <td>'+val.Mechanical_Tilt+'</td>\n' +
			'      <td>'+val.Electrical_Tilt+'</td>\n' +
			'      <td>'+val.Cell_Coverage+'</td>\n' +
			'      <td>'+val.Site_Type+'</td>\n' +
			'      <td>'+val.Cell_Type+'</td>\n' +
			'      <td>'+val.Uplink_UARFCN+'</td>\n' +
			'      <td>'+val.Downlink_UARFCN+'</td>\n' +
			'      <td>'+val.DL_Primary_Scrambling_Code+'</td>\n' +
			'      <td>'+val.Location_area_code+'</td>\n' +
			'      <td>'+val.Routing_Area_Code+'</td>\n' +
			'      <td>'+val.Service_Area_Code+'</td>\n' +
			'      <td>'+val.Local_Cell_ID+'</td>\n' +
			'</tr>';
	});

	html += '</tbody>' +
		'\t\t\t\t\t\t\t\t</table>\n'+
		'</div>';


	return html;
}

function set_fourg_tab(data) {
	let	html = '<div class="mg-top-5">' +
		'\t\t\t\t\t\t\t\t<table class="table table-bordered table-responsive dt-responsive nowrap rre_tbl">\n' +
		' <thead class="text-muted small text-center">\n' +
		'    <tr>\n' +
		'      <th scope="col">Cell Name</th>\n' +
		'      <th scope="col">4RFS</th>\n' +
		'      <th scope="col">Cell ID</th>\n' +
		'      <th scope="col">Sector</th>\n' +
		'      <th scope="col">BTS Name</th>\n' +
		'      <th scope="col">eNodeB Name</th>\n' +
		'      <th scope="col">MME Name (Homing)</th>\n' +
		'      <th scope="col">Latitude</th>\n' +
		'      <th scope="col">Longitude</th>\n' +
		'      <th scope="col">Freq. Band</th>\n' +
		'      <th scope="col">Dual Beam</th>\n' +
		'      <th scope="col">MIMO</th>\n' +
		'      <th scope="col">Carrier</th>\n' +
		'      <th scope="col">Band</th>\n' +
		'      <th scope="col">Antenna Center Line</th>\n' +
		'      <th scope="col">Azimuth</th>\n' +
		'      <th scope="col">Mechanical Tilt</th>\n' +
		'      <th scope="col">Electrical Tilt</th>\n' +
		'      <th scope="col">Cell Coverage</th>\n' +
		'      <th scope="col">Site Type</th>\n' +
		'      <th scope="col">Cell Type</th>\n' +
		'      <th scope="col">Uplink EARFCN</th>\n' +
		'      <th scope="col">Downlink EARFCN</th>\n' +
		'      <th scope="col">Physical Cell Identity</th>\n' +
		'      <th scope="col">Location area code</th>\n' +
		'      <th scope="col">Routing Area Code</th>\n' +
		'      <th scope="col">Tracking Area Code</th>\n' +
		'      <th scope="col">Duplex</th>\n' +
		'      <th scope="col">Local Cell ID</th>\n' +
		'      <th scope="col">EnodeB ID</th>\n' +
		'      <th scope="col">Status</th>\n' +
		'    </tr>\n' +
		'  </thead>' +
		'<tbody>';
	_.forEach(data, function (val) {
		html += ' <tr class="text-center small">\n' +
			'      <td>'+val.Cell_Name+'</td>\n' +
			'      <td>'+val.RFS+'</td>\n' +
			'      <td>'+val.Cell_ID+'</td>\n' +
			'      <td>'+val.Sector+'</td>\n' +
			'      <td>'+val.BTS_Name+'</td>\n' +
			'      <td>'+val.eNodeB_Name+'</td>\n' +
			'      <td>'+val.MME_Name+'</td>\n' +
			'      <td>'+val.Latitude+'</td>\n' +
			'      <td>'+val.Longitude+'</td>\n' +
			'      <td>'+val.Freq_Band+'</td>\n' +
			'      <td>'+val.Dual_Beam+'</td>\n' +
			'      <td>'+val.MIMO+'</td>\n' +
			'      <td>'+val.Carrier+'</td>\n' +
			'      <td>'+val.Band+'</td>\n' +
			'      <td>'+val.Antenna_Center_Line+'</td>\n' +
			'      <td>'+val.Azimuth+'</td>\n' +
			'      <td>'+val.Mechanical_Tilt+'</td>\n' +
			'      <td>'+val.Electrical_Tilt+'</td>\n' +
			'      <td>'+val.Cell_Coverage+'</td>\n' +
			'      <td>'+val.Site_Type+'</td>\n' +
			'      <td>'+val.Cell_Type+'</td>\n' +
			'      <td>'+val.Uplink_EARFCN+'</td>\n' +
			'      <td>'+val.Downlink_EARFCN+'</td>\n' +
			'      <td>'+val.Physical_Cell_Identity+'</td>\n' +
			'      <td>'+val.Location_area_code+'</td>\n' +
			'      <td>'+val.Routing_Area_Code+'</td>\n' +
			'      <td>'+val.Tracking_Area_Code+'</td>\n' +
			'      <td>'+val.Duplex+'</td>\n' +
			'      <td>'+val.Local_Cell_ID+'</td>\n' +
			'      <td>'+val.EnodeB_ID+'</td>\n' +
			'      <td>'+val.Status+'</td>\n' +
			'</tr>';
	});
	html += '</tbody>' +
		'\t\t\t\t\t\t\t\t</table>\n'+
		'</div>';

	return html;
}

//-----------------------------------------------//

function set_site_details_card(data) {
	let html = '  <div class="card border-primary shadow mb-3">\n' +
		'    <div class="card-header text-center bg-primary text-white border-primary">Site Info</div>' +
		'    <div class="card-body ">\n' +
		'<table class="table table-bordered table-sm small">\n' +
		'  <tbody>\n' +

		'    <tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">PLAID</td>\n' +
		'      <td>'+data.PLAID+'</td>\n' +
		'    </tr>\n' +
		'    <tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">Area</td>\n' +
		'      <td>'+data.AREA+'</td>\n' +
		'    </tr>\n' +


		'    </tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">Site name</td>\n' +
		'      <td>'+data.SITE_NAME+'</td>\n' +
		'    </tr>\n' +

		'    </tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">Latitude</td>\n' +
		'      <td>'+data.LATITUDE+'</td>\n' +
		'    </tr>\n' +
		'    </tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">Longtitude</td>\n' +
		'      <td>'+data.LONGITUDE+'</td>\n' +
		'    </tr>\n' +
		'    </tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">Address</td>\n' +
		'      <td>'+data.ADDRESS+'</td>\n' +
		'    </tr>\n' +

		'    </tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">Region</td>\n' +
		'      <td>'+data.REGION+'</td>\n' +
		'    </tr>\n' +

		'    </tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">Province</td>\n' +
		'      <td>'+data.PROVINCE+'</td>\n' +
		'    </tr>\n' +

		'    </tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">Town</td>\n' +
		'      <td>'+data.TOWN+'</td>\n' +
		'    </tr>\n' +

		'  </tbody>\n' +
		'</table>' +
		'    </div>\n' +
		'  </div>\n';
	return html;
}

function set_site_profile_card(data) {
	let html = '  <div class="card shadow border-danger">\n' +
		'    <div class="card-header bg-danger text-center text-white border-danger">Facilities</div>' +
		'    <div class="card-body ">\n' +
		'<table class="table table-bordered table-sm small">\n' +
		'  <tbody>\n' +

		'    <tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">Site Type</td>\n' +
		'      <td>'+data.SITE_TYPE+'</td>\n' +
		'    </tr>\n' +
		'    <tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">Tower Type</td>\n' +
		'      <td>'+data.TOWER_TYPE+'</td>\n' +
		'    </tr>\n' +
		'    </tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">Tower Height</td>\n' +
		'      <td>'+data.TOWER_HEIGHT+'</td>\n' +
		'    </tr>\n' +

		'    </tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">Hub</td>\n' +
		'      <td>'+data.HUB+'</td>\n' +
		'    </tr>\n' +
		'    </tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">Site Class</td>\n' +
		'      <td>'+data.SITE_CLASS+'</td>\n' +
		'    </tr>\n' +

		'    </tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">Profile</td>\n' +
		'      <td>'+data.PROFILE+'</td>\n' +
		'    </tr>\n' +

		'    </tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">Solution</td>\n' +
		'      <td>'+data.SOLUTION+'</td>\n' +
		'    </tr>\n' +
		'  </tbody>\n' +
		'</table>' +
		'    </div>\n' +
		'  </div>\n';
	return html;
}

function set_power_card(data) {
	let html = '<div class="card shadow border-success">\n' +
		'    <div class="card-header bg-success text-center text-white border-success">Power</div>' +
		'    <div class="card-body ">\n' +
				'<table class="table table-bordered table-sm small">\n' +
				'  <tbody>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Grid Status</td>\n' +
				'      <td>'+data.GRID_STATUS+'</td>\n' +
				'    </tr>\n' +
				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Cabin Count</td>\n' +
				'      <td>'+data.CABIN_COUNT+'</td>\n' +
				'    </tr>\n' +
				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Cabin Type</td>\n' +
				'      <td>'+data.CABIN_TYPE+'</td>\n' +
				'    </tr>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Back-up Power</td>\n' +
				'      <td>'+data.BACK_UP_POWER+'</td>\n' +
				'    </tr>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Batteries</td>\n' +
				'      <td>'+data.BATTERIES+'</td>\n' +
				'    </tr>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Rectifier Capacity</td>\n' +
				'      <td>'+data.RECTIFIER_CAPACITY+'</td>\n' +
				'    </tr>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Rectifier Load</td>\n' +
				'      <td>'+data.RECTIFIER_LOAD+'</td>\n' +
				'    </tr>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">DC Load</td>\n' +
				'      <td>'+data.DC_LOAD+'</td>\n' +
				'    </tr>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">MCB Rating</td>\n' +
				'      <td>'+data.MCB_RATING+'</td>\n' +
				'    </tr>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">AC Load</td>\n' +
				'      <td>'+data.AC_LOAD+'</td>\n' +
				'    </tr>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Transformer UR</td>\n' +
				'      <td>'+data.TRANSFORMER_UR+'</td>\n' +
				'    </tr>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Genset UR</td>\n' +
				'      <td>'+data.GENSET_UR+'</td>\n' +
				'    </tr>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">MCB/ECB UR</td>\n' +
				'      <td>'+data.MCB_ECB_UR+'</td>\n' +
				'    </tr>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">ACPDB UR</td>\n' +
				'      <td>'+data.ACPDB_UR+'</td>\n' +
				'    </tr>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Commercial Power Provider</td>\n' +
				'      <td>'+data.COMMERCIAL_POWER_PROVIDER+'</td>\n' +
				'    </tr>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Power Connection type</td>\n' +
				'      <td>'+data.POWER_CONNECTION_TYPE+'</td>\n' +
				'    </tr>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Transformer Capacity</td>\n' +
				'      <td>'+data.TRANSFORMER_CAPACITY+'</td>\n' +
				'    </tr>\n' +

				'  </tbody>\n' +
				'</table>' +
		'    </div>\n' +
		'  </div>\n';
	return html;
}

function set_si_update_card(data) {
	let html = '<div class="card shadow border-secondary">\n' +
		'    <div class="card-header bg-secondary text-white text-center border-secondary">SI Update</div>' +
		'    <div class="card-body ">\n' +
		'<table class="table table-bordered table-sm small">\n' +
		'  <tbody>\n' +
		'    <tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">NSCP Edition</td>\n' +
		'      <td>'+data.NSCP_EDITION+'</td>\n' +
		'    </tr>\n' +
		'    </tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">SI Date</td>\n' +
		'      <td>'+data.SI_DATE+'</td>\n' +
		'    </tr>\n' +

		'    </tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">SI UR</td>\n' +
		'      <td>'+data.SI_UR+'</td>\n' +
		'    </tr>\n' +

		'    </tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">SI RESULT</td>\n' +
		'      <td>'+data.SI_RESULT+'</td>\n' +
		'    </tr>\n' +

		'  </tbody>\n' +
		'</table>' +
		'    </div>\n' +
		'  </div>\n';
	return html;
}

function set_retro_update_card(data) {
	let html = '<div class="card shadow border-pink">\n' +
		'    <div class="card-header u-pink text-white border-pink text-center">Retro Update</div>' +
		'    <div class="card-body ">\n' +
			'<table class="table table-bordered table-sm small">\n' +
			'  <tbody>\n' +
			'    <tr>\n' +
			'      <td scope="row" class="text-muted bg-light text-right">Date of Last Retrofitting</td>\n' +
			'      <td>'+data.DATE_OF_LAST_RETROFITTING+'</td>\n' +
			'    </tr>\n' +
			'    </tr>\n' +
			'      <td scope="row" class="text-muted bg-light text-right">Retro Status</td>\n' +
			'      <td>'+data.RETRO_STATUS+'</td>\n' +
			'    </tr>\n' +
			'  </tbody>\n' +
			'</table>' +
		'    </div>\n' +
		'  </div>\n';
	return html;
}

function set_tssr_ptpa_update_card(data) {
	let html = '<div class="card shadow border-warning">\n' +
		'    <div class="card-header bg-warning text-center text-white border-warning">TSSR/PTA Update</div>' +
		'    <div class="card-body ">\n' +
		'<table class="table table-bordered table-sm small">\n' +
		'  <tbody>\n' +
		'    <tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">Latest TSSR Approved Date</td>\n' +
		'      <td>'+data.LATEST_TSSR_APPROVED_DATE+'</td>\n' +
		'    </tr>\n' +
		'    <tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">TSSR/PTA Tower Utilization Ratio</td>\n' +
		'      <td>'+data.TSSR_PTA_TOWER_UTILIZATION_RATIO+'</td>\n' +
		'    </tr>\n' +
		'      <td scope="row" class="text-muted bg-light text-right">TSSR/PTA Remarks</td>\n' +
		'      <td>'+data.TSSR_PTA_REMARKS+'</td>\n' +
		'    </tr>\n' +
		'  </tbody>\n' +
		'</table>' +
		'    </div>\n' +
		'  </div>\n';
	return html;
}

//-----------------------------------------------//

function set_primary_details_card(data) {
	let html = '  <div class="card border-primary shadow mb-3">\n' +
		'    <div class="card-header text-center bg-primary text-white border-primary">Primary Info</div>' +
		'    <div class="card-body ">\n';
			if (data !== null) {
				html += '<table class="table table-bordered table-sm small">\n' +
					'  <tbody>\n' +
					'    <tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">Loop Name</td>\n' +
					'      <td>'+data.LOOP_NAME+'</td>\n' +
					'    </tr>\n' +

					'    <tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">PLAID</td>\n' +
					'      <td>'+data.PLAID+'</td>\n' +
					'    </tr>\n' +

					'    </tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">Latitude</td>\n' +
					'      <td>'+data.LATITUDE+'</td>\n' +
					'    </tr>\n' +
					'    </tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">Longitude</td>\n' +
					'      <td>'+data.LONGITUDE+'</td>\n' +
					'    </tr>\n' +
					'    </tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">Address</td>\n' +
					'      <td>'+data.SITE_ADDRESS+'</td>\n' +
					'    </tr>\n' +

					'    </tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">Municipality</td>\n' +
					'      <td>'+data.MUNICIPALITY_DESC+'</td>\n' +
					'    </tr>\n' +

					'    </tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">Province</td>\n' +
					'      <td>'+data.PROVINCE_DESC+'</td>\n' +
					'    </tr>\n' +

					'    </tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">Region</td>\n' +
					'      <td>'+data.REGION_DESC+'</td>\n' +
					'    </tr>\n' +

					'    </tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">GeoArea</td>\n' +
					'      <td>'+data.REGION+'</td>\n' +
					'    </tr>\n' +

					'  </tbody>\n' +
					'</table>';
			} else {
				html += empty_card_result();
			}
		html += '</div>\n' +
		'  </div>\n';
	return html;
}

function set_secondary_details_card(data) {
	let html = '  <div class="card border-success shadow mb-3">\n' +
		'    <div class="card-header text-center bg-success text-white border-success">Technology</div>' +
		'    <div class="card-body ">\n';
			if (data !== null) {
				html += '<table class="table table-bordered table-sm small">\n' +
				'  <tbody>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">AG Homing 1</td>\n' +
				'      <td>'+data.AGHOM1+'</td>\n' +
				'    </tr>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">AG Homing 2</td>\n' +
				'      <td>'+data.AGHOM2+'</td>\n' +
				'    </tr>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">AG Port 2</td>\n' +
				'      <td>'+data.AGPORT2+'</td>\n' +
				'    </tr>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">ATN Type</td>\n' +
				'      <td>'+data.ATNTYP+'</td>\n' +
				'    </tr>\n' +
				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Uplink Capacity</td>\n' +
				'      <td>'+data.UPCAP+'</td>\n' +
				'    </tr>\n' +
				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Configuration Type</td>\n' +
				'      <td>'+data.CONFTYPE+'</td>\n' +
				'    </tr>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Technology Installed</td>\n' +
				'      <td>'+data.TECH+'</td>\n' +
				'    </tr>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">MSAN Technology</td>\n' +
				'      <td>'+data.MSANTECH+'</td>\n' +
				'    </tr>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">DSLAM Type</td>\n' +
				'      <td>'+data.DSLAMTYP+'</td>\n' +
				'    </tr>\n' +

				'    </tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Hardware Version</td>\n' +
				'      <td>'+data.HARDWARE+'</td>\n' +
				'    </tr>\n' +

				'  </tbody>\n' +
				'</table>';
			} else {
				html += empty_card_result();
			}

		html += '</div>\n' +
		'  </div>\n';
	return html;
}

function set_om_details_card(data) {
	let html = '  <div class="card border-warning shadow mb-3">\n' +
		'    <div class="card-header text-center bg-warning text-white border-warning">OM</div>' +
		'    <div class="card-body ">\n';
			if (data !== null) {
				html +=	'<table class="table table-bordered table-sm small">\n' +
					'  <tbody>\n' +

					'    <tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">OM IP</td>\n' +
					'      <td>'+data.OMIP+'</td>\n' +
					'    </tr>\n' +

					'    <tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">OM Gateway</td>\n' +
					'      <td>'+data.OMGW+'</td>\n' +
					'    </tr>\n' +

					'    <tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">OM Network</td>\n' +
					'      <td>'+data.OMNW+'</td>\n' +
					'    </tr>\n' +

					'    <tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">OM Subnet</td>\n' +
					'      <td>'+data.OMSN+'</td>\n' +
					'    </tr>\n' +

					'    <tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">OM VLAN</td>\n' +
					'      <td>'+data.OMVLAN+'</td>\n' +
					'    </tr>\n' +
					'  </tbody>\n' +
					'</table>';
			} else {
				html += empty_card_result();
			}

		html += '    </div>\n' +
		'  </div>\n';
	return html;
}

function set_data_details_card(data) {
	let html = '  <div class="card border-purple shadow mb-3">\n' +
		'    <div class="card-header text-center u-purple text-white border-purple">Data</div>' +
		'    <div class="card-body ">\n';
		if (data !== null) {
			html += '<table class="table table-bordered table-sm small">\n' +
			'  <tbody>\n' +

			'    <tr>\n' +
			'      <td scope="row" class="text-muted bg-light text-right">Data VLAN</td>\n' +
			'      <td>'+data.DATAVLAN+'</td>\n' +
			'    </tr>\n' +

			'    <tr>\n' +
			'      <td scope="row" class="text-muted bg-light text-right">BNG Homing</td>\n' +
			'      <td>'+data.DATABNG+'</td>\n' +
			'    </tr>\n' +

			'    <tr>\n' +
			'      <td scope="row" class="text-muted bg-light text-right">Data Leased Line</td>\n' +
			'      <td>'+data.DATALL+'</td>\n' +
			'    </tr>\n' +

			'    <tr>\n' +
			'      <td scope="row" class="text-muted bg-light text-right">Data Static</td>\n' +
			'      <td>'+data.DATASTATIC+'</td>\n' +
			'    </tr>\n' +

			'    <tr>\n' +
			'      <td scope="row" class="text-muted bg-light text-right">IPOE VLAN</td>\n' +
			'      <td>'+data.DATAVLAN+'</td>\n' +
			'    </tr>\n' +
			'  </tbody>\n' +
			'</table>';
		} else {
			html += empty_card_result();
		}

		html += '</div>\n' +
		'  </div>\n';
	return html;
}

function set_voice_details_card(data) {
	let html = '  <div class="card border-danger shadow mb-3">\n' +
		'    <div class="card-header text-center bg-danger text-white border-danger">Voice</div>' +
		'    <div class="card-body ">\n';
			if (data !== null) {
				html += '<table class="table table-bordered table-sm small">\n' +
				'  <tbody>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">SIP VLAN</td>\n' +
				'      <td>'+data.VOICEVLAN+'</td>\n' +
				'    </tr>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">SIP From</td>\n' +
				'      <td>'+data.VOICEFROM+'</td>\n' +
				'    </tr>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">SIP To</td>\n' +
				'      <td>'+data.VOICETO+'</td>\n' +
				'    </tr>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">SIP NETWORK</td>\n' +
				'      <td>'+data.SIP_NETWORK+'</td>\n' +
				'    </tr>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">SIP Gateway</td>\n' +
				'      <td>'+data.SIP_GATEWAY+'</td>\n' +
				'    </tr>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">SIP Subnet</td>\n' +
				'      <td>'+data.VOICESN+'</td>\n' +
				'    </tr>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">SIP Homing</td>\n' +
				'      <td>'+data.VOICEHOM+'</td>\n' +
				'    </tr>\n' +


				'  </tbody>\n' +
				'</table>';
			} else {
				html += empty_card_result();
			}

		html += '</div>\n' +
		'  </div>\n';
	return html;
}

function set_sigip_details_card(data) {
	let html = '  <div class="card border-brown shadow mb-3">\n' +
		'    <div class="card-header text-center u-brown text-white border-brown">Signaling</div>' +
		'    <div class="card-body ">\n';
				if (data !== null) {
					html += '<table class="table table-bordered table-sm small">\n' +
					'  <tbody>\n' +

					'    <tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">Signaling IP</td>\n' +
					'      <td>'+data.SIGIP+'</td>\n' +
					'    </tr>\n' +

					'    <tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">Signaling Network</td>\n' +
					'      <td>'+data.SIGNW+'</td>\n' +
					'    </tr>\n' +

					'    <tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">Signaling Subnet</td>\n' +
					'      <td>'+data.SIGSN+'</td>\n' +
					'    </tr>\n' +

					'    <tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">Signaling VLAN</td>\n' +
					'      <td>'+data.SIGVLAN+'</td>\n' +
					'    </tr>\n' +

					'  </tbody>\n' +
					'</table>';
				} else {
					html += empty_card_result();
				}

		html += '</div>\n' +
		'  </div>\n';
	return html;
}

function set_media_details_card(data) {
	let html = '  <div class="card border-pink shadow mb-3">\n' +
		'    <div class="card-header text-center u-pink text-white border-pink">Media</div>' +
		'    <div class="card-body ">\n';
				if (data !== null) {
					html += '<table class="table table-bordered table-sm small">\n' +
					'  <tbody>\n' +

					'    <tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">Media Gateway</td>\n' +
					'      <td>'+data.MEDIAGW+'</td>\n' +
					'    </tr>\n' +

					'    <tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">Media IP</td>\n' +
					'      <td>'+data.MEDIAIP+'</td>\n' +
					'    </tr>\n' +

					'    <tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">Media Network</td>\n' +
					'      <td>'+data.MEDIANW+'</td>\n' +
					'    </tr>\n' +

					'    <tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">Media Subnet</td>\n' +
					'      <td>'+data.MEDIASN+'</td>\n' +
					'    </tr>\n' +

					'    <tr>\n' +
					'      <td scope="row" class="text-muted bg-light text-right">Media VLAN</td>\n' +
					'      <td>'+data.MEDIAVLAN+'</td>\n' +
					'    </tr>\n' +

					'  </tbody>\n' +
					'</table>';
				} else {
					html += empty_card_result();
				}

		html += '    </div>\n' +
		'  </div>\n';
	return html;
}

function set_cabinet_details_card(data) {
	let html = '  <div class="card border-teal shadow mb-3">\n' +
		'    <div class="card-header text-center u-teal text-white border-teal">Indoor Cabinet</div>' +
		'    <div class="card-body ">\n';
			if (data !== null) {
				html += '<table class="table table-bordered table-sm small">\n' +
				'  <tbody>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Cabinet Name</td>\n' +
				'      <td>'+data.CABINET_NAME+'</td>\n' +
				'    </tr>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Cabinet Environment</td>\n' +
				'      <td>'+data.CABINET+'</td>\n' +
				'    </tr>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">MSAN Uplink</td>\n' +
				'      <td>'+data.MSAN+'</td>\n' +
				'    </tr>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">ZoneID</td>\n' +
				'      <td>'+data.CABINET+'</td>\n' +
				'    </tr>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Topbox</td>\n' +
				'      <td>'+data.TOPBOX+'</td>\n' +
				'    </tr>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Rectifier CO</td>\n' +
				'      <td>'+data.RECTIFIER+'</td>\n' +
				'    </tr>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Project Name</td>\n' +
				'      <td>'+data.PROJNAME+'</td>\n' +
				'    </tr>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">RFS Date</td>\n' +
				'      <td>'+data.RFS_DATE+'</td>\n' +
				'    </tr>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Status</td>\n' +
				'      <td>'+data.Status+'</td>\n' +
				'    </tr>\n' +

				'    <tr>\n' +
				'      <td scope="row" class="text-muted bg-light text-right">Vendor</td>\n' +
				'      <td>'+data.Vendor+'</td>\n' +
				'    </tr>\n' +

				'  </tbody>\n' +
				'</table>';
			} else {
				html += empty_card_result();
			}

		html += '</div>\n' +
		'  </div>\n';
	return html;
}

//-----------------------------------------------//
