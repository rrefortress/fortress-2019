Dropzone.autoDiscover = false;

let wireline = new Dropzone(".dropzone",{
	url: url + "Wireline/wire_upload",
	timeout: 180000,
	parallelUploads: 50,
	maxFiles: 50,
	method:"POST",
	acceptedFiles:".csv",
	paramName:"wire_file",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:true,
	init: function() {
		this.on("success", function(file, response) {
			let data = jQuery.parseJSON(response);
			SWAL(data.type, data.title, data.msg);
			table.ajax.reload();
		})
	}
});

wireline.on("queuecomplete", function () {
	this.removeAllFiles();
	$('#upload_wire').modal('hide');
});

wireline.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});

wireline.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});

