let table;

$(document).on('click', '.upload_trans', function (e) {
	e.preventDefault();
	$('#upload_trans').modal('show');
});

$(function () {
    let param = {
		url: url + 'get_trans_fkeys',
		dataType: 'json',
		type: 'GET',
        beforeSend: $('.d-dis').html('<div class="col-12 mg-top-100" style="min-height:49vh">' +
			'<h5 class="text-center text-white">' +
			'<img src='+loaders+' width="150" height="150" class="rounded-circle" alt="Loader"><br>' +
			'<small class="text-center text-muted pt-4"><em>Loading data...</em></small>' +
			'</h5>' +
			'</div>')
	};
	fortress(param).then((data) => {
        $('.d-dis').html('');
      
        let html = '<div class="table-responsive">' +
                        '<table id="d_transport" class="display small compact nowrap text-center table-sm responsive table-hover table-striped table-bordered mt-2" cellspacing="0" width="100%">' +
                            '<thead class="global_color text-white text-uppercase">' +
                                '<tr class="th_transport"></tr>' +
                            '</thead>' +
                            '<tbody"></tbody>' +
                        '</table>' + 
                        '</div>';

        $('.d-transport').html(html);  
       
        set_header(data, 'transport');
	});
});  

function set_header(data, id) {
    let html = '';
    _.forEach(data,function(val, key){
        html += '<th>'+val+'</th>'
    });

    $('.th_'+id).html(html);
    build_datatable(id);
}

function build_datatable(id) {
	table = $('#d_'+id).DataTable({
        "order": [],
        "select": true,
        "processing": true,
        "serverSide": true,
        "pageLength": 10,
        "paging":  true,
        "fixedHeader":true,
        "orderCellsTop": true,
        "lengthMenu": [[10, 30, 60, 100, -1], [10, 30, 60, 100, "All"]],
        "scrollY":"75vh",
        "scrollX":true,
        "ordering" : true,
        "info"     : true,
        "searching": true,
        "scrollCollapse": true,
        "ajax": {
            "url": url + 'Transport/get_'+id,
            "type": "POST"
        },
        "columnDefs": [
            { 
                "width": 200,
                "targets": [ 0 ], //first column / numbering column
                "orderable": false, //set not orderable
            },
        ],
    });
}

$(document).on('click', '.delete_trans', function (e) {
	e.preventDefault();
    Swal.fire({
		title: 'Are you sure?',
		text: "You want to delete all item/s?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	}).then((result) => {
		if (result.value) {
			let param = {
				url: url + 'trans_remove',
				type: "post",
				dataType: 'json'
			};

			fortress(param).then((data) => {
				SWAL(data.type, data.title, data.msg);
                table.ajax.reload();
			});
		}
	});
});