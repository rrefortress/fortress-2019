function set_on_chart(data) {
	let canvasBar = document.getElementById("on_aired_chart");
	let ctxBar = canvasBar.getContext('2d');

	let val1 = data['ON-AIRED'];
	let val2 = data['Not'];
	let total = val1 + val2;

	let total_per = getWholePercent(val1, total) + getWholePercent(val2, total);

	let datas = {
		labels: [val1 +": ON-AIRED", val2 +": NOT ON-AIRED"],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#448AFF', '#dc3545'],
				data: [
					getWholePercent(val1, total),
					getWholePercent(val2, total),
				],
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText(total),
		title : setTitle("ON-AIRED SITES"),
		rotation : -0.7 * Math.PI,
		legend: {
			position: 'left',
		},
		layout: {
			padding: {
				left: 5,
				right: 5,
				top: 5,
				bottom: 5
			}
		}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', 'oa');
}

function set_st_chart(data, elem, param) {
	let canvasBar = document.getElementById(elem);
	let ctxBar = canvasBar.getContext('2d');

	let gf = data['GREENFIELD'];
	let rf = data['ROOFTOP'];
	let swat = data['SWAT'];
	let cow = data['COW LEGO TEMPO'];
	let ibs = data['IBS'];
	let oibs = data['OD COLOC TO IBS'];
	let sc = data['SMALL CELL'];
	// let dis = data['DISMANTLED'];
	let mc = data['to be updated'];
	let total = gf +
				rf +
				swat +
				ibs +
				cow +
				oibs +
				sc +
				// dis +
				// macro +
				mc;

	let total_per = getWholePercent(gf, total) +
		getWholePercent(rf, total) +
		getWholePercent(swat, total) +
		getWholePercent(ibs, total) +
		getWholePercent(oibs, total) +
		getWholePercent(cow, total) +
		getWholePercent(sc, total) +
		// getWholePercent(dis, total) +
		// getWholePercent(macro, total) +
		getWholePercent(mc, total);

	let datas = {
		labels: [gf +": Greenfield",
			     rf +": Rooftop",
			     swat +": SWAT",
				 ibs+ ": IBS",
			     oibs+ ": OD Coloc to IBS",
			     cow+ ": Cow Lego Tempo",
			     sc+ ": Small Cell",
			     // dis+ ": Dismantled",
				 // macro+ ": MACRO",
			     mc+  ": To be Updated"],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#28a745',
					              '#448AFF',
								  '#dc3545',
								  '#f0a500',
					              '#93329e',
					              '#85603f',
					              '#fddb3a',
								  // '#252525',
					              '#646464'],
				data: [
					getWholePercent(gf, total),
					getWholePercent(rf, total),
					getWholePercent(swat, total),
					getWholePercent(ibs, total),
					getWholePercent(oibs, total),
					getWholePercent(cow, total),
					getWholePercent(sc, total),
					// getWholePercent(dis, total),
					getWholePercent(mc, total),
				],
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText(total),
		title : param.title,
		rotation : -0.7 * Math.PI,
		legend: {
			display: param.legend,
			position: param.pos,
			labels: {
				boxWidth: param.box,
				padding: param.pad
			},
		},
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', param.site);
}

function set_gf_chart(data, elem, param) {
	let canvasBar = document.getElementById(elem);
	let ctxBar = canvasBar.getContext('2d');

	let sst = data['SST'];
	let gt = data['GUYED TOWER'];
	let mp = data['MONOPOLE'];
	let tbu = data['to be updated'];
	// let cow = data['COW'];
	let other = data['OTHER GF TOWER'];

	let total = sst +
		        gt +
				mp +
				// leg +
				// cow +
				tbu +
				other;

	let total_per = getWholePercent(sst, total) +
		getWholePercent(gt, total) +
		getWholePercent(mp, total) +
		// getWholePercent(leg, total) +
		// getWholePercent(cow, total) +
		getWholePercent(other, total) +
		getWholePercent(tbu, total);

	let datas = {
		labels: [sst +": SST",
				 gt +": GUYED TOWER",
			     mp +": MONOPOLE",
				 // cow +": COW",
				 other + ": OTHER GF TOWER",
				 tbu +": To be updated",
				 ],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#28a745',
									'#0779e4',
									'#d8345f',
									// '#ffe227',
									// '#4a3933',
									'#7D5A50',
									'#686d76'
									],
				data: [
					getWholePercent(sst, total),
					getWholePercent(gt, total),
					getWholePercent(mp, total),
					// getWholePercent(cow, total),
					getWholePercent(other, total),
					getWholePercent(tbu, total),
				]
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText(total),
		title : param.title,
		rotation : -0.7 * Math.PI,
		legend: {
			position: param.pos,
			labels: {
				boxWidth: param.box,
				padding: param.pad
			},
		},
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', param.site);
}

function set_rt_chart(data, elem, param) {
	let canvasBar = document.getElementById(elem);
	let ctxBar = canvasBar.getContext('2d');

	let tri = data['TRIPOD'];
	let min = data['MINITOWER'];
	let rtp = data['ROOFTOP POLE'];
	let other = data['OTHER RT TOWER'];
	let tbu = data['to be updated'];

	let total = tri + min + rtp + other + tbu;

	let total_per = getWholePercent(tri, total) +
		getWholePercent(min, total) +
		getWholePercent(rtp, total) +
		getWholePercent(tbu, total) +
		getWholePercent(other, total);

	let datas = {
		labels: [tri +": TRIPOD",
			     min +": MINITOWER",
				 rtp +": ROOFTOP POLE",
				 other + ': OTHER RT TOWER',
				 tbu + ': To be updated'
		         ],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: [
					              '#f0a500',
					              '#28a745',
					              '#0779e4',
					              '#7D5A50',
								  '#686d76'
								],
				data: [
					getWholePercent(tri, total),
					getWholePercent(min, total),
					getWholePercent(rtp, total),
					getWholePercent(tbu, total),
					getWholePercent(other, total)
				]
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText(total),
		title : param.title,
		rotation : -0.7 * Math.PI,
		legend: {
			position: param.pos,
			labels: {
				boxWidth: param.box,
				padding: param.pad
			},
		},
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', param.site);
}

function set_tc_chart(data, elem, param) {
	let canvasBar = document.getElementById(elem);
	let ctxBar = canvasBar.getContext('2d');

	let bc = data['bc'];
	let fc = data['fc'];
	let wh = data['wh'];
	let tbu = data['tbu'];
	let na = data['none'];

	let total = bc + fc + wh + na + tbu;

	let total_per = getWholePercent(bc, total) +
		getWholePercent(fc, total) +
		getWholePercent(wh, total) +
		getWholePercent(tbu, total) +
		getWholePercent(na, total);

	let datas = {
		labels: [wh +": WITH HEADROOM", fc +": FULL CAPACITY (@105% UR)", bc +": FOR SI / UPGRADE", na +": None", tbu + ": To be updated"],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#28a745','#f0a500','#e53935', '#121013', '#686d76'],
				data: [
					getWholePercent(wh, total),
					getWholePercent(fc, total),
					getWholePercent(bc, total),
					getWholePercent(tbu, total),
					getWholePercent(na, total)
				]
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText(total),
		title : param.title,
		rotation : -0.7 * Math.PI,
		legend: {
			position: param.pos,
			labels: {
				boxWidth: param.box,
				padding: param.pad
			},
		},
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', param.site);
}

function set_nscp_chart(data, elem, param) {
	let canvasBar = document.getElementById(elem);
	let ctxBar = canvasBar.getContext('2d');

	let n7 = data['n7'];
	let n6 = data['n6'];
	let retro = data['retro'];

	let total = n7 + n6 + retro;

	let total_per = getWholePercent(n7, total) +
		getWholePercent(n6, total) +
		getWholePercent(retro, total);

	let datas = {
		labels: [n7 +": NSCP 7 Complaint", n6 +": For NSCP 6 Complaint", retro + ": Ongoing SI / Retro"],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#51adcf', '#de4463', '#ffe227'],
				data: [
					getWholePercent(n7, total),
					getWholePercent(n6, total),
					getWholePercent(retro, total)
				]
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText(total),
		title : param.title,
		rotation : -0.7 * Math.PI,
		legend: {
			position: param.pos,
			labels: {
				boxWidth: param.box,
				padding: param.pad
			},
		},
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', param.site);
}

$(document).on('change', '#geo_power', function(e) {
	e.preventDefault();
	geo_power = $(this).val();
	get_chart(geo_tower,geo_power, sc, status);
});

$(document).on('change', '#status', function(e) {
	e.preventDefault();
	status = $(this).val();
	get_chart(geo_tower,geo_power, sc, status);
});
