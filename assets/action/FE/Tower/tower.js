let inputs = [
	{
		basic : 
		[
			{
				'title' : 'PLA ID',
				'name' : 'PLAID',
				'type'   : 'text',
				'required'  : 'required',
				'col' : 5,
				'ons' : ''
			},
			{
				'title' : 'Site Name',
				'name' : 'SITENAME',
				'type'   : 'text',
				'required'  : 'required',
				'col' : 7,
				'ons' : ''
			},
			{
				'title' : 'Site Type',
				'name' : 'SITE_TYPE',
				'type'   : 'select',
				'required'  : 'required',
				'drowpdown' : {
					'GREENFIELD' : 'Greenfield',
					'ROOFTOP' : 'Rooftop',
					'IBS': 'IBS',
					'COW LEGO TEMPO' : 'Cow lego Tempo',
					'SMALL CELL': 'Small Cell',
					'OD COLOC TO IBS' : 'OD Coloc to IBS',
					'SWAT' : 'Swat',
					'MACRO' : 'Macro',
					'to be updated' : 'To be updated'
				},
				'col' : 6
			},
			{
				'title' : 'Tower Build Scope',
				'name' : 'TOWER_BUILD_SCOPE',
				'type'   : 'select',
				'required'  : 'required',
				'drowpdown' : {
					'SI' : 'SI',
					'SI & Tower Retro' : 'SI & Tower Retro',
					'Tower Conversion' : 'Tower Conversion',
					'to be updated' : 'To be updated'
				},
				'col' : 6
			},
			{
				'title' : 'Build Completion',
				'name' : 'BUILD_COMPLETION',
				'type'   : 'select',
				'required'  : 'required',
				'drowpdown' : {
					'Ongoing SI' : 'Ongoing SI',
					'SI Completed' : 'SI Completed',
					'SI RFI' : 'SI RFI',
					'For mobilization' : 'For mobilization',
					'Ongoing TR' : 'Ongoing TR',
					'RFTI' : 'RFTI',
					'PAT' : 'PAT',
					'PAC' : 'PAC',
					'FAC' : 'FAC',
					'to be updated' : 'To be updated'
				},
				'col' : 6
			},
		],

	}
];
let sitesdatatable;
let tblcolumns=[];
let nowblinking;
let clickedoperation="";
let activeoperation="";
let selectedtr = [];
let selectallflag=0;
let tdcolindex=0;
let section;
let textfinal='';
let textinitial=null;
let rowid = null;
let fieldid = null;
let tablecat = null;
let dataset=[];
let coldetails = [];
let tower_tbl = $("#tower_tbl");
let table_name = 'tower';

let profile = [];
let p_tech = [];

let antenna_epa_table = new Object();
let tower_epa_table = new Object();


$(function () {
	get_completion();
	get_tower_data();
});

function get_tower_data() {
	let param = {
		url: url + 'generatedatatable',
		dataType: 'json',
		type: 'POST',
		data: {displayto: table_name, request: "fields"},
	};

	$.each(inputs[0], function (index, value) {
			$.each(value, function (index, value2) {
					coldetails.push(value2);
			});
	});

	fortress(param).then((data) => {
		var temp = "";
		$.each(data, function (index, value) {
			$.each(coldetails, function (indexin, valuecol) {
				if (valuecol.name == value) {
					tblcolumns.push({"title": valuecol.title, "name": value});
					temp = valuecol.name;
					return;
				}
			});
			if (temp != value) {
				tblcolumns.push({"title": value, "name": value});
			}

			// tblcolumns.push({"title":value});
		});
		// console.log(tblcolumns);
		let reg_hdr=1;
		$.each(tblcolumns, function (ind, val) {
			if(val.name == "REGION" || val.name == "GEOAREA" )
			{
				reg_hdr = ind;
			}
		});
		let hiddencols = $('#geo').val() !== 'nat' ? [0,reg_hdr] : [0];
		sitesdatatable = tower_tbl.DataTable({
			"dom": 'lrt<"row"<"col"i><"#selectedtr_info.col"><"col"p>>',
			"processing": true,
			"serverSide": true,
			"pageLength": 10,
			"lengthMenu": [[10, 30, 60, 100, -1], [10, 30, 60, 100, "All"]],
			"columns": tblcolumns,
			"orderCellsTop": true,
			scrollY: "68vh",
			scrollX:        true,
			scrollCollapse: true,
			paging:         true,
			fixedColumns: {
				leftColumns: 3
			},
			"ajax": {
				url: url + "generatedatatable",
				data: {displayto: table_name, request: "data"},
				type: "POST",
			},
			fnCreatedRow: function (nRow, aData) {
				$(nRow).attr('id', aData[0]);
				$(nRow).attr('plaid', aData[2]);
				$(nRow).attr('region', aData[6]);
			},
			buttons:  [ 
	             {
		            extend : 'pdfHtml5',
		            title : function() {
		                return "ABCDE List";
		            },
		            orientation : 'landscape',
		            pageSize : 'A0',
		            text : '<i class="fa fa-file-pdf-o"> PDF</i>',
		            titleAttr : 'PDF'
		        } 
        	],
			// "colReorder": {
			// 	order: [70],
			// },
			// ordering: [[ 1, 'asc' ]],
	  //       colReorder: {
	  //           fixedColumnsLeft: 1,
	  //           fixedColumnsRight: 1
	  //       },
	        
			"columnDefs": [
				// { orderable: false, targets: 0 },
	   //          { orderable: false, targets: -1 },
				{
					"visible": false,
					"targets": hiddencols,
				},
				{
					targets: '_all',
					render: function (data, type, row) {
						var color = 'black';
						let value = data === null || data === undefined? 'to be updated' : data.toLowerCase();
						if (value === "to be updated") {
							color = 'red';
							return '<span style="color:' + color + '">' + data + '</span>';
						} else if (value === "none") {
							color = 'gray';
							return '<span style="color:' + color + '">' + data + '</span>';
						}else{
							return data;
						}
					}
				},
				// {
				// 	targets: fixedCols,
				// 	createdCell: function (td, data, rowData, row, col) {
				// 		$.each(fixedCols, function (index, value) {
				// 			if(value == col){
				// 				console.log(td+" \n "+data+" \n "+row+" \n "+col);
				// 				$(td).addClass('fixed-col');
				// 			}
				// 		});
				// 	}
				// },
				// {
				// 	targets: 6,
				// 	createdCell: function (td, cellData, rowData, row, col) {
				// 		// console.log(td+" \n "+cellData+" \n "+rowData+" \n "+row+" \n "+col);
				// 		$trout = $sel = $('<td></td>',{});;
				// 		$sel = $('<tr></tr>',{});
				// 		for($i=0;$i<5;$i++)
				// 		{
				// 			$sel.append($('<td></td>', {"text": $i}));
				// 		}
				// 		$sel.append(
				// 			$(`<td><button id="" type="button" class="btn btn-xxs"><span class="text-danger"><i class="fas fa-minus"></i></span></button>
				// 				<button id="" type="button" class="btn btn-xxs"><span class="text-success"><i class="fas fa-plus"></i></span></button></td>`, {}));
				// 		$trout.append($sel);
				// 		$trout.append($sel);
				// 		console.log($trout.prop("outerHTML"));
				// 		$(td).html($sel.prop("outerHTML"));
				// 		// $(td).text('id');
				// 	}
				// },
			],
			initComplete: async function () {

				let tower_length = $('#tower_tbl_length');
				$(tower_length).addClass('row mt-2');
				$('#tower_tbl_length label').addClass('col-2 m-0 p-0 ml-3');
				$(tower_length).append(`
				    <div class="col-3 form-group has-search m-0 p-0">
	                    <span class="fa fa-search form-control-feedback form-control-xs mt-2"></span>
	                    <input onsearch="OnSearch(this)" type="search" class="form-control form-control-sm rounded-pill" id="srchinput" placeholder="Search..." autofocus data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
	            	</div>`);
				$("#selectedtr_info").addClass('dataTables_info');
				if ($('#geo').val() !== 'nat' && $('#lvl').val() !== 'GUEST') {
					$(tower_length).append(`
								<div class="col-6 ml-5">
									<div class="">
										<h5 class="font-weight-normal m-0"></h5>
									</div>
									<div class="row d-flex justify-content-end">
										<div class="operationtoggle btn-group btn-group-toggle" data-toggle="buttons">
											<label id="edit" class="btn btn-outline-warning my-1 px-1 p-0">
												<input type="radio" name="options"  autocomplete="off"><i class="fas fa-edit"></i> EDIT
											</label>
											<label id="delete" class="btn btn-outline-danger my-1 px-1 p-0">
												<input type="radio" name="options" autocomplete="off"><i class="fas fa-trash"></i> DELETE
											</label>
										</div>
										<button id="select" type="button" class="btn btn-sm btn-link"><i class="fas fa-check-square"></i> SELECT / UNSELECT All</button>
										<button id="clear" type="button" class="btn btn-sm btn-link"><i class="fas fa-eraser"></i> CLEAR</button>
										<button id="proceed" type="button" class="btn btn-sm btn-link"><i class="fas fa-check-circle"></i> PROCEED</button>
									</div>
								</div>`);
					$("#select").attr('disabled', true);
					$("#proceed").attr('disabled', true);
					$("#clear").attr('disabled', true);
				}

				// $tr = $('<tr id="colfilter"></tr>',{});
				// $.each(tblcolumns, function (key1, val1) {
				// 	if(val1.title=="id"){return;}
				// 	// console.log(val1);
				// 	let $th = $('<th></th>', {});
		  //           let $inp = $('<input></input>', {
		  //           				"id":key1+1,
		  //           				"type": "search",
		  //           				"onsearch":"OnColSearch(this)",
		  //           				"class":"colsearch cellinputsize form-control form-control-sm",
		  //                           // "placeholder": val1.title,
		  //                        });
	   //              $th.append($inp);
    //                 $tr.append($th);
    //             });
				// $("#tower_tbl thead").append($tr.prop("outerHTML"));
				$(".tower_tbl th").addClass('global_color text-white text-uppercase text-center');
			},
		});

	});
	
	// yadcf.init(sitesdatatable, [
	//         {
	//         	column_number : 3,
 //                select_type: 'select2',
 //                // filter_container_id: prop_table_name+1,
 //                filter_type: "multi_select",
 //                filter_match_mode: "exact",
 //                select_type_options: {
 //                    width: '100%',
 //                    closeOnSelect : true,
 //                    'theme': 'bootstrap',
 //                    'placeholder': "Filter",

 //        			}
 //        	},
 //        	]);
}

function newexportaction(e, dt, button, config) {
         var self = this;
         var oldStart = dt.settings()[0]._iDisplayStart;
         dt.one('preXhr', function (e, s, data) {
             // Just this once, load all data from the server...
             data.start = 0;
             data.length = 2147483647;
             dt.one('preDraw', function (e, settings) {
                 // Call the original action function
                 if (button[0].className.indexOf('buttons-copy') >= 0) {
                     $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
                 } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                     $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                         $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                         $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                 } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                     $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                         $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                         $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
                 } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                     $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                         $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                         $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
                 } else if (button[0].className.indexOf('buttons-print') >= 0) {
                     $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
                 }
                 dt.one('preXhr', function (e, s, data) {
                     // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                     // Set the property to what it was before exporting.
                     settings._iDisplayStart = oldStart;
                     data.start = oldStart;
                 });
                 // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                 setTimeout(dt.ajax.reload, 0);
                 // Prevent rendering of the full data to the DOM
                 return false;
             });
         });
         // Requery the server with the new one-time export settings
         dt.ajax.reload();
     }

$( document ).ajaxComplete(function() {
 reHighlight();
});
$(document).on('keyup', '.colsearch',function () {
	drawAllSearch(this,"colsearch");
} );

$(document).on("keyup", "#srchinput", function () {
	drawAllSearch(this,"global");
});

$(document).on('click', '#proceed', function () {
	var updatedata = [];
	switch (activeoperation) {
		case 'edit':
			for (var i = 0; i < dataset.length; i++) {
				updatedata.push([dataset[i][1], dataset[i][2], dataset[i][4]]);
			}
			Swal.fire({
				title: 'Are you sure?',
				text: "Changes cannot be undone.",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes'
			}).then((result) => {
				if (result.value) {
					let param = {
						url: url + 'edit',
						dataType: 'json',
						type: 'POST',
						data: {"data": updatedata, "db": table_name},
					};
					fortress(param).then((data) => {
						console.log(url);
						SWAL('success', 'Success!', 'Data updated.');
						sitesdatatable.ajax.reload();
						get_completion();
						resetclear();
					});
				}
			});

			break;
		case 'delete':
			var selections = [];
			var delprint = "";
			if (selectallflag) {
				selections.push("all");
				delprint = "all";
			} else {
				selections = selectedtr;
				delprint = "selected";
			}

			Swal.fire({
				title: 'Delete ' +selectedtr.length+' row(s) '+ delprint + '?',
				text: "Changes cannot be undone.",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes'
			}).then((result) => {
				if (result.value) {
					let param = {
						url: url + 'delete',
						dataType: 'json',
						type: 'POST',
						data: {"selections": selections, "db": table_name, "fe": true},
					};
					fortress(param).then(() => {
						SWAL('success', 'Success!', 'Data removed.');
						sitesdatatable.ajax.reload();
						get_completion();
						resetclear();
					});
				}
			});
			break;
	}
});

$(document).on('click', '.operationtoggle label', function () {
	if (nowblinking === undefined) {
		nowblinking = this;
	}
	if (clickedoperation !== undefined && clickedoperation !== ($(this)).attr("id")) {
		nowblinking = this;
		resetclear();
	}
	blink(this);
	clickedoperation = ($(this)).attr("id");
	activeoperation = clickedoperation;
	$("#clear").attr('disabled', false);
	switch (activeoperation) {
		case "delete":
			$("#select").attr('disabled', false);
			break;
		case "edit":
			hidecolumns(1);
			break;
	}
});
function OnColSearch(input) {
	drawAllSearch(input,"colsearch");
}
function OnSearch(input) {
	drawAllSearch(input,"global");
}
function drawAllSearch(input,searchArea)
{
	if(searchArea=="global")
	{
		sitesdatatable.search(document.getElementById("srchinput").value).draw();
	}
	else
	{
		let colindex = $(input).attr('id')-1;
		for(let i=1; i<=tblcolumns.length; i++){
		    if ( sitesdatatable.column(colindex).search() !== input.value ) {
		        sitesdatatable.column(colindex).search(input.value).draw();
		    }
		}
	}
	
}
function resetclear() {
	hidecolumns(0);
	$(".operationtoggle label").removeClass('active');
	activeoperation = "";
	selectedtr = [];
	selectallflag = 0;
	dataset = [];
	reHighlight();
	$("#select").attr('disabled', true);
	$("#proceed").attr('disabled', true);
	$("#clear").attr('disabled', true);
	$('.tower-site, .plan-tagging').attr('disabled', true);
	sitesdatatable.ajax.reload();
}

function hidecolumns(bool) {
	let startcol = 103;
	let endcol = 103;
	if (bool == 1) {
		for (var i = startcol; i <= endcol; i++) {
			sitesdatatable.column(i).visible(false, false);
		}
	} else {
		for (var i = startcol; i <= endcol; i++) {
			sitesdatatable.column(i).visible(true, false);
		}
	}
}
function blink(tag) {
	$(tag).fadeOut(500, function () {
		$(tag).fadeIn(500, function () {
			// console.log(nowblinking+' '+tag+' '+activeoperation);
			if (nowblinking != tag || activeoperation == '') {
				// console.log(2);
				return;
			} else blink(tag);
		});
	});
}

function reHighlight() {
	let pTable = document.getElementById('tower_tbl');
	for (let i = 1, row; row = pTable.rows[i]; i++) {
		let rowid = $(pTable.rows[i].cells[0]).closest('tr').attr('plaid');
		selectedtr.includes(rowid) ? $(pTable.rows[i]).addClass('selected') : $(pTable.rows[i]).removeClass('selected');
	}
	if(selectedtr.length>0){
		$("#selectedtr_info").html(selectedtr.length+" row(s) selected.");
	}else{
		$("#selectedtr_info").html("");
	}
}
function getedited() {
	if (activeoperation === 'edit') {
		if (textinitial != null && textinitial !== textfinal) {
			var i = 0;
			for (i = 0; i < dataset.length; i++) {
				if (dataset[i][0] === tablecat && dataset[i][1] === rowid && dataset[i][2] === fieldid && dataset[i][4] !== textfinal) {
					dataset[i][4] = textfinal;
					return;
				}
			}
			dataset.push([tablecat, rowid, fieldid, textinitial, textfinal]);
		}
	}
}

$(document).on('click', '#clear', function () {
	resetclear();
});
$(document).on("keyup", '#tower_tbl td', function () {
	$("#proceed").attr('disabled', false);
});  
$(document).on('click', '#select', function () {
	if (selectallflag) {
		selectallflag = 0;
		selectedtr = [];
	} else {
		{
			selectallflag = 1;
			let pTable = document.getElementById('tower_tbl');
			for (let i = 1, row; row = pTable.rows[i]; i++) {
				let rowid = $(pTable.rows[i].cells[0]).closest('tr').attr('plaid');
				selectedtr.push(rowid);
			}
		}
	}
	if (selectedtr.length !== 0) $("#proceed").attr('disabled', false);
	else $("#proceed").attr('disabled', true);
	reHighlight();
});

$(document).on("click",'#tower_tbl td', function(e) {
	e.preventDefault();
	
		$('.tower-site, .plan-tagging').removeAttr("disabled");
		
		$(this).toggleClass('selected');

		let istd = (this.innerHTML.indexOf("dtableselect"));


		if(activeoperation==="edit") {
			let $sel;
			rowid = $(this).closest("tr").attr("id");
			tdcolindex = sitesdatatable.cell(this).index().column;
			$.each(tblcolumns, function(index,value){
				if(index == tdcolindex){	
					fieldid = value.name;
				}
			});
			tablecat = $(this).closest("table").attr("id");
			var textkeydown = sitesdatatable.cell(this).render('display');
			if (textinitial == null && textinitial !== textkeydown) {
				textinitial = textkeydown;
			}
			let menutype='';
			$.each(coldetails, function(index,value){
				if(value.name == fieldid){
					console.log(value.name + '====' + fieldid);
					if(value.type=='select' && istd == -1){
						let temp = 0;
						menutype='sel';
						$sel = $('<select class="'+fieldid+' dtableselect cellinputsize custom-select form-control custom-select-sm" name ='+value.name+'></select>',{});
						
						$.each(value.drowpdown, function (k1, v1) {
	                         var $opt = $('<option></option>', {
	                             "text": v1,
	                             "value": v1
	                         });
	                         if(v1.replace(/\s/g, '').toLowerCase()==textkeydown.replace(/\s/g, '').toLowerCase()) {
	                             $opt.attr("selected", "selected");
	                             temp = 1;
	                         }
	                         $sel.append($opt);
	                     });
						if(!temp) {
						let $opt = $('<option disabled selected value="disabled">'+textkeydown+'</option>', {});
	                    	$sel.append($opt);
	                    }
					}
				}
			});

			if(menutype=='sel'){
				sitesdatatable.cell(this).data($sel.prop("outerHTML"));
				$('.dtableselect').select2({ width: '100%' });
			}
			else if(menutype=='sel2'){

			}
			else{
				$(this).prop('contenteditable', true);
			}
		}	
});

$(document).on('blur', '#tower_tbl td', function (e) {
	e.preventDefault();
	let istd = (this.innerHTML.indexOf("dtableselect"));
	if(istd==-1)
	{
		textfinal = $(this).text();
		getedited();
		textinitial = null;
		textfinal = null;
		$(this).prop('contenteditable', false);
	}
});

$(document).on("change",'.dtableselect', function(e) {
	e.preventDefault();
	// console.log("changeeeeeee");
	if($(this).val()!=="disabled"){
		// rowid = $(this).closest("tr").attr("id");
		textfinal = $(this).find(":selected").val();
		console.log(textfinal);
		getedited();
		textinitial = null;
		textfinal = null;
		$("#proceed").attr('disabled', false);
	}
	// rowid = $(this).closest("tr").attr("id");
	// tdcolindex = sitesdatatable.cell(this).index().column;
	// $.each(tblcolumns, function(index,value){
	// 	if(index == tdcolindex) fieldid = value.name;
	// });

	// sitesdatatable.cell(rowid+1, tdcolindex+1).data("new");

});

$(document).on('click', '.gene-power', function (e) {
	e.preventDefault();
	let param = {
		url: url + 'get_power_reports',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then((data) => {
		let sub_tccp_ncr = 0;
		_.forEach(data.region.ncr, function(val, key) {

		});	

		

		let html = '<table class="table text-center table-bordered">' +
			'<thead class="bg-warning">' +
				'<tr>' +
					'<th scope="col" colspan="2">Power Guidelines</th>' +
					'<th scope="col" style="width:7%">NCR</th>' +
					'<th scope="col" style="width:7%">NLZ</th>' +
					'<th scope="col" style="width:7%">SLZ</th>' +
					'<th scope="col" style="width:7%">VIS</th>' +
					'<th scope="col" style="width:7%">MIN</th>' +
					'<th scope="col" style="width:7%">TOTAL</th>' +
				'</tr>' +
			'</thead>' +
			
			'<tbody>' +
				'<tr>' +
					'<th rowspan="5" class="bg-primary text-white" style="vertical-align: middle;">Permanent Power Solution</th>' +
				'</tr>' +

				'<tr>' +
					'<td>Tap to Commerial Power Provider</td>' +
					'<td>'+data.region.ncr.tcpp+'</td>' +
					'<td>'+data.region.nlz.tcpp+'</td>' +
					'<td>'+data.region.slz.tcpp+'</td>' +
					'<td>'+data.region.vis.tcpp+'</td>' +
					'<td>'+data.region.min.tcpp+'</td>' +
					'<td>'+data.total_tccp+'</td>' +
				'</tr>' +
				'<tr>' +
					'<td>Tap to Lessor via SPCA/Sub-meter</td>' +
					'<td>'+data.region.ncr.tlss+'</td>' +
					'<td>'+data.region.nlz.tlss+'</td>' +
					'<td>'+data.region.slz.tlss+'</td>' +
					'<td>'+data.region.vis.tlss+'</td>' +
					'<td>'+data.region.min.tlss+'</td>' +
					'<td>'+data.total_tlss+'</td>' +
				'</tr>' +
				'<tr>' +
					'<td>Tap to Genset by Design</td>' +
					'<td>'+data.region.ncr.tg+'</td>' +
					'<td>'+data.region.nlz.tg+'</td>' +
					'<td>'+data.region.slz.tg+'</td>' +
					'<td>'+data.region.vis.tg+'</td>' +
					'<td>'+data.region.min.tg+'</td>' +
					'<td>'+data.total_tg+'</td>' +
				'</tr>' +
				'<tr>' +
					'<th>Sub-total</th>' +
					'<th>'+data.region.ncr.sub_pps+'</th>' +
					'<th>'+data.region.nlz.sub_pps+'</th>' +
					'<th>'+data.region.slz.sub_pps+'</th>' +
					'<th>'+data.region.vis.sub_pps+'</th>' +
					'<th>'+data.region.min.sub_pps+'</th>' +
					'<th>'+data.total_sub_pps+'</th>' +
				'</tr>' +
				
				'<tr>' +
					'<th rowspan="5" class="bg-danger text-white" style="vertical-align: middle;">Tempo Power Solution</th>' +	
				'</tr>' +

				'<tr>' +
					'<td>Tempo tap to Commerial Power</td>' +
					'<td>'+data.region.ncr.ttcp+'</td>' +
					'<td>'+data.region.nlz.ttcp+'</td>' +
					'<td>'+data.region.slz.ttcp+'</td>' +
					'<td>'+data.region.vis.ttcp+'</td>' +
					'<td>'+data.region.min.ttcp+'</td>' +
					'<td>'+data.total_ttcp+'</td>' +
				'</tr>' +
				'<tr>' +
					'<td>Tap to Lessor/Building</td>' +
					'<td>'+data.region.ncr.tl+'</td>' +
					'<td>'+data.region.nlz.tl+'</td>' +
					'<td>'+data.region.slz.tl+'</td>' +
					'<td>'+data.region.vis.tl+'</td>' +
					'<td>'+data.region.min.tl+'</td>' +
					'<td>'+data.total_tl+'</td>' +
				'</tr>' +
				'<tr>' +
					'<td>Tap to Genset</td>' +
					'<td>'+data.region.ncr.tgp+'</td>' +
					'<td>'+data.region.nlz.tgp+'</td>' +
					'<td>'+data.region.slz.tgp+'</td>' +
					'<td>'+data.region.vis.tgp+'</td>' +
					'<td>'+data.region.min.tgp+'</td>' +
					'<td>'+data.total_tgp+'</td>' +
				'</tr>' +
			
				'<tr>' +
					'<th>Sub-total</th>' +
					'<th>'+data.region.ncr.sub_tps+'</th>' +
					'<th>'+data.region.nlz.sub_tps+'</th>' +
					'<th>'+data.region.slz.sub_tps+'</th>' +
					'<th>'+data.region.vis.sub_tps+'</th>' +
					'<th>'+data.region.min.sub_tps+'</th>' +
					'<th>'+data.total_sub_tps+'</th>' +
				'</tr>' +

			'</tbody>' +
			'<tfoot>' +
				'<tr>' +
					'<th scope="col" class="bg-success text-white" colspan="2">Total</th>' +
					'<th>'+data.region.ncr.total+'</th>' +
					'<th>'+data.region.nlz.total+'</th>' +
					'<th>'+data.region.slz.total+'</th>' +
					'<th>'+data.region.vis.total+'</th>' +
					'<th>'+data.region.min.total+'</th>' +
					'<th>'+data.total_total+'</th>' +
				'</tr>' +
			'</tfoot>' +
			'<tfoot>' +
				'<tr>' +
					'<th scope="col" class="bg-dark text-white" colspan="2">To be Updated (Progressive)</th>' +
					'<th>'+data.region.ncr.tbu+'</th>' +
					'<th>'+data.region.nlz.tbu+'</th>' +
					'<th>'+data.region.slz.tbu+'</th>' +
					'<th>'+data.region.vis.tbu+'</th>' +
					'<th>'+data.region.min.tbu+'</th>' +
					'<th>'+data.total_tbu+'</th>' +
				'</tr>' +
			'</tfoot>' +
		'</table>';

		$('.gene-tbl-report').html(html);
		$('#gene-report').modal('show');
		tinymce.init({
			selector: 'textarea#insights',
			skin: 'bootstrap',
			plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
			toolbar: 'h1 h2 bold italic strikethrough blockquote bullist undo redo | styleselect | fontselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent numlist backcolor | link image media | removeformat help',
			font_formats: "Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",
			toolbar_mode: 'floating',
			tinycomments_mode: 'embedded',
			tinycomments_author: 'Author name',
			menubar: false
		});
	});
});

$(document).on("click",'#tower_tbl td', function(e) {
	e.preventDefault();
	$('.tower-site, .plan-tagging').removeAttr("disabled");
	let istd = (this.innerHTML.indexOf("dtableselect"));

		if(activeoperation==="edit")
		{
			let $sel;
			rowid = $(this).closest("tr").attr("id");
			tdcolindex = sitesdatatable.cell(this).index().column;
			// console.log(tdcolindex);
			$.each(tblcolumns, function(index,value){
				if(index == tdcolindex){
					fieldid = value.name;
					// console.log(fieldid);
				}
			});
			tablecat = $(this).closest("table").attr("id");
			var textkeydown = sitesdatatable.cell(this).render('display');
			if (textinitial == null && textinitial !== textkeydown) {
				textinitial = textkeydown;
			}
			let menutype='';
			$.each(coldetails, function(index,value){
				if(value.name == fieldid){
					if(value.type=='select' && istd == -1){

						let temp = 0;
						menutype='sel';
						$sel = $('<select class="'+fieldid+' dtableselect cellinputsize custom-select form-control custom-select-sm" name ='+value.name+'></select>',{}).select2({ width: '100%' });
						$.each(value.drowpdown, function (k1, v1) {
	                         var $opt = $('<option></option>', {
	                             "text": v1,
	                             "value": v1
	                         });
	                         if(v1.replace(/\s/g, '').toLowerCase()==textkeydown.replace(/\s/g, '').toLowerCase()) {
	                             $opt.attr("selected", "selected");
	                             temp = 1;
	                         }
	                         $sel.append($opt);
	                     });
						if(!temp) {
						let $opt = $('<option disabled selected value="disabled">'+textkeydown+'</option>', {});
	                    	$sel.append($opt);
	                    }
					}
				}
			});

			if(menutype=='sel'){
				// console.log(8888);
				sitesdatatable.cell(this).data($sel.prop("outerHTML"));
				$('.dtableselect').select2({ width: '100%' });
			}
			else if(menutype=='sel2'){
				// console.log(9999);
				//do nothing
			}
			else{
				$(this).prop('contenteditable', true);
			}
		}
});

$(document).on("click", '#tower_tbl tr', function () {
	let ret = [];
	// const id = sitesdatatable.rows(this).data()[0][0];
	const id = $(this).closest("tr").attr("id");
	const plaid = $(this).closest("tr").attr("plaid");
	const reg = $(this).closest("tr").attr("region");

	get_site_profile(id, plaid, reg);

	// console.log(id);
	if(plaid!=="" || iplaidd!==undefined || plaid!==0){
		if (activeoperation === "delete") {
			selectedtr.push(plaid);
			for (var i = 0; i < selectedtr.length; i += 1) {
				if (ret.indexOf(selectedtr[i]) !== -1) {
					ret.splice(ret.indexOf(selectedtr[i]), 1);
				} else {
					ret.push(selectedtr[i]);
				}
			}
			selectedtr = ret;
			if (selectedtr.length !== 0) $("#proceed").attr('disabled', false);
			else $("#proceed").attr('disabled', true);
		}else{
			selectedtr = [];
			selectedtr.push(plaid);
		}
		reHighlight();
	}
	
});
// function get_all_db_dropdowns()
// {
// 	let param = {
// 		url: url + 'get_all_db_dropdowns',
// 		dataType: 'json',
// 		type: 'GET',
// 	};
// 	fortress(param).then((data) => {
// 		$.each(data, function (index, value) {
// 			let dropdown = [];

// 				switch(index)
// 				{
// 					case "acu":
// 						$.each(value, function (index2, val) {
// 							dropdown.push(val.model+'-'+val.power);
// 						});
// 						coldetails.push({"title": "ACU Model & Capacity(HP)", "name": "ACU_CAPACITY_HP", "type": "select", "drowpdown": dropdown});
// 						break;
// 					case "dropwire":
// 						$.each(value, function (index2, val) {
// 							dropdown.push(val.circuit_breaker+' ' +val.cable_mm+ 'mm');
// 						});
// 						coldetails.push({"title": "Dropwire", "name": "DROP_WIRES", "type": "select", "drowpdown": dropdown});
// 						break;
// 					case "rect":
// 						$.each(value, function (index2, val) {
// 							dropdown.push(val.name);
// 						});
// 						coldetails.push({"title": "Rectifier 1", "name": "RECTIFIER_1", "type": "select", "drowpdown": dropdown});
// 						coldetails.push({"title": "Rectifier 2", "name": "RECTIFIER_2", "type": "select", "drowpdown": dropdown});
// 						coldetails.push({"title": "Rectifier 3", "name": "RECTIFIER_3", "type": "select", "drowpdown": dropdown});
// 						break;
// 					case "batt":
// 						$.each(value, function (index2, val) {
// 							dropdown.push(val.name);
// 						});
// 						coldetails.push({"title": "RS 1 Battery Brand", "name": "RS1_BATTERY_BRAND", "type": "select", "drowpdown": dropdown});
// 						coldetails.push({"title": "RS 2 Battery Brand", "name": "RS2_BATTERY_BRAND", "type": "select", "drowpdown": dropdown});
// 						coldetails.push({"title": "RS 3 Battery Brand", "name": "RS3_BATTERY_BRAND", "type": "select", "drowpdown": dropdown});
// 						break;
// 				}
			
// 			// console.log({"title": index, "name": index, "type": "select", "drowpdown": drowpdown});
			
// 		});
		
// 	});
// 	console.log(coldetails);
// }


$(document).on('click', '.bulk-upload_tower', function (e) {
	e.preventDefault();
	$('#bulk-upload-tower').modal('show');
});


$(document).on("click", ".export_tower", function (e) {
	e.preventDefault();
	$(this).prop('disabled', true);

	let param = {
		url: url + 'export_tower',
		dataType: 'json',
		beforeSend: $(this).html('<em>Exporting <i class="fas fa-spinner fa-spin text-muted"></i></em>')
	};

	fortress(param).then((data) => {
		let today = moment().format("DD/MM/YYYY");
		if (data.result.length > 0) {
			JSONToCSVConvertor(data.result, "TOWER_" + data.geoarea + "(" + today + ")", true);
		} else {
			SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
		}
		$(this).html('<i class="fas fa-download"></i> EXPORT').prop('disabled', false);
	});

});

function get_completion() {
	let param = {
		url: url + 'get_tower_completion',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then((data) => {
		let percent = parseInt(data.total) === 0 ? 0 : getWholePercent(data.completed, data.total);

		$('.cmp').text(data.completed + ' / ' + data.total);
		$('.crate').text( percent + '%');
	});
}

$(document).on('click', '.tower-site', function (e) {
	e.preventDefault();
	let html = '';
	html += set_profile_cards(profile.basic_info, 'BASIC INFO', 60);
	html += set_profile_cards(profile.nscp, 'NSCP 7 COMPLIANCE', 60);
	html += set_profile_cards(profile.cabin, 'CABIN ODU & GENSET PAD', 70);
	html += set_profile_cards(profile.pat, 'PAT ACCEPTANCE');
	html += set_profile_cards(profile.rre, 'RRE TAGGING');
	html += set_profile_cards(profile.tssr, 'TSSR PTA UPDATE', 70);
	html += set_profile_cards(profile.si, 'SI UPDATE', 70);
	html += set_profile_cards(profile.retro, 'RETRO UPDATE', 70);
	html += set_profile_cards(profile.tc, 'TOWER CONVERSION', 70);
	html += set_profile_cards(profile.tu, 'TOWER UPDATE REMARKS', 70);
	html += set_profile_cards(profile.amb, 'AMB SOLUTIONS', 70);
	
	
	$('.c-profiles').html(html);
	$('#tower-item').modal('show');
});

function set_profile_cards(data, title = '', width = 50) {
	let html = '<div class="card rounded-0 shadow-sm border">' +
					'<div class="col-12 p-0 shadow-sm bg-danger display-5 text-uppercase text-white text-center">' +
						'<strong>' + title + '</strong>' +
					'</div>' +
					'<div class="card-body p-2">' +
						'<table class="table table-bordered table-sm mb-0 small table-hovered">'+
							'<tbody>';
								_.forEach(data, function(val, key) {
									html += '<tr>' +
												'<td class="text-right pr-2" scope="row" style="width:'+width+'%">'+key+'</td>' +
												'<th class="text-left pl-2">'+val+'</th>' +
											'</tr>';					
								});
						html += '</tbody>' +
							'</table>' +
					'</div>' +
				'</div>';
	return html;
}

function get_site_profile(id = 0, plaid = '', reg = '') {
	let param = {
		url: url + 'get_tower_profile',
		dataType: 'json',
		data: {id: id, plaid: plaid, region: reg},
		type: 'POST'
	};
	fortress(param).then( (data) => {
		profile = data.fe;	
		set_tbl_detail_rre(data.tech, data.tech.prof);
	});
	
}

$(document).on('click', '.gene-tower', function (e) {
	e.preventDefault();
	let param = {
		url: url + 'get_tower_reports',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then((data) => {
		let html = '';
		if (data.length > 0) {

				 html += '<table class="table text-center table-bordered">' +
					'<thead class="bg-warning">' +
						'<tr>' +
							'<th scope="col" colspan="2">Power Guidelines</th>' +
							'<th scope="col" style="width:7%">NCR</th>' +
							'<th scope="col" style="width:7%">NLZ</th>' +
							'<th scope="col" style="width:7%">SLZ</th>' +
							'<th scope="col" style="width:7%">VIS</th>' +
							'<th scope="col" style="width:7%">MIN</th>' +
							'<th scope="col" style="width:7%">TOTAL</th>' +
						'</tr>' +
					'</thead>' +
					
					'<tbody>' +
						'<tr>' +
							'<th rowspan="5" class="bg-primary text-white" style="vertical-align: middle;">Permanent Power Solution</th>' +
						'</tr>' +

						'<tr>' +
							'<td>Tap to Commerial Power Provider</td>' +
							'<td>'+data.region.ncr.tcpp+'</td>' +
							'<td>'+data.region.nlz.tcpp+'</td>' +
							'<td>'+data.region.slz.tcpp+'</td>' +
							'<td>'+data.region.vis.tcpp+'</td>' +
							'<td>'+data.region.min.tcpp+'</td>' +
							'<td>'+data.total_tccp+'</td>' +
						'</tr>' +
						'<tr>' +
							'<td>Tap to Lessor via SPCA/Sub-meter</td>' +
							'<td>'+data.region.ncr.tlss+'</td>' +
							'<td>'+data.region.nlz.tlss+'</td>' +
							'<td>'+data.region.slz.tlss+'</td>' +
							'<td>'+data.region.vis.tlss+'</td>' +
							'<td>'+data.region.min.tlss+'</td>' +
							'<td>'+data.total_tlss+'</td>' +
						'</tr>' +
						'<tr>' +
							'<td>Tap to Genset by Design</td>' +
							'<td>'+data.region.ncr.tg+'</td>' +
							'<td>'+data.region.nlz.tg+'</td>' +
							'<td>'+data.region.slz.tg+'</td>' +
							'<td>'+data.region.vis.tg+'</td>' +
							'<td>'+data.region.min.tg+'</td>' +
							'<td>'+data.total_tg+'</td>' +
						'</tr>' +
						'<tr>' +
							'<th>Sub-total</th>' +
							'<th>'+data.region.ncr.sub_pps+'</th>' +
							'<th>'+data.region.nlz.sub_pps+'</th>' +
							'<th>'+data.region.slz.sub_pps+'</th>' +
							'<th>'+data.region.vis.sub_pps+'</th>' +
							'<th>'+data.region.min.sub_pps+'</th>' +
							'<th>'+data.total_sub_pps+'</th>' +
						'</tr>' +
						
						'<tr>' +
							'<th rowspan="5" class="bg-danger text-white" style="vertical-align: middle;">Tempo Power Solution</th>' +	
						'</tr>' +

						'<tr>' +
							'<td>Tempo tap to Commerial Power</td>' +
							'<td>'+data.region.ncr.ttcp+'</td>' +
							'<td>'+data.region.nlz.ttcp+'</td>' +
							'<td>'+data.region.slz.ttcp+'</td>' +
							'<td>'+data.region.vis.ttcp+'</td>' +
							'<td>'+data.region.min.ttcp+'</td>' +
							'<td>'+data.total_ttcp+'</td>' +
						'</tr>' +
						'<tr>' +
							'<td>Tap to Lessor/Building</td>' +
							'<td>'+data.region.ncr.tl+'</td>' +
							'<td>'+data.region.nlz.tl+'</td>' +
							'<td>'+data.region.slz.tl+'</td>' +
							'<td>'+data.region.vis.tl+'</td>' +
							'<td>'+data.region.min.tl+'</td>' +
							'<td>'+data.total_tl+'</td>' +
						'</tr>' +
						'<tr>' +
							'<td>Tap to Genset</td>' +
							'<td>'+data.region.ncr.tgp+'</td>' +
							'<td>'+data.region.nlz.tgp+'</td>' +
							'<td>'+data.region.slz.tgp+'</td>' +
							'<td>'+data.region.vis.tgp+'</td>' +
							'<td>'+data.region.min.tgp+'</td>' +
							'<td>'+data.total_tgp+'</td>' +
						'</tr>' +
					
						'<tr>' +
							'<th>Sub-total</th>' +
							'<th>'+data.region.ncr.sub_tps+'</th>' +
							'<th>'+data.region.nlz.sub_tps+'</th>' +
							'<th>'+data.region.slz.sub_tps+'</th>' +
							'<th>'+data.region.vis.sub_tps+'</th>' +
							'<th>'+data.region.min.sub_tps+'</th>' +
							'<th>'+data.total_sub_tps+'</th>' +
						'</tr>' +

					'</tbody>' +
					'<tfoot>' +
						'<tr>' +
							'<th scope="col" class="bg-success text-white" colspan="2">Total</th>' +
							'<th>'+data.region.ncr.total+'</th>' +
							'<th>'+data.region.nlz.total+'</th>' +
							'<th>'+data.region.slz.total+'</th>' +
							'<th>'+data.region.vis.total+'</th>' +
							'<th>'+data.region.min.total+'</th>' +
							'<th>'+data.total_total+'</th>' +
						'</tr>' +
					'</tfoot>' +
					'<tfoot>' +
						'<tr>' +
							'<th scope="col" class="bg-dark text-white" colspan="2">To be Updated (Progressive)</th>' +
							'<th>'+data.region.ncr.tbu+'</th>' +
							'<th>'+data.region.nlz.tbu+'</th>' +
							'<th>'+data.region.slz.tbu+'</th>' +
							'<th>'+data.region.vis.tbu+'</th>' +
							'<th>'+data.region.min.tbu+'</th>' +
							'<th>'+data.total_tbu+'</th>' +
						'</tr>' +
					'</tfoot>' +
				'</table>';
		}
			

		$('.gene-tbl-report').html(html);
		$('#gene-report').modal('show');
		tinymce.init({
			selector: 'textarea#insights',
			skin: 'bootstrap',
			plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
			toolbar: 'h1 h2 bold italic strikethrough blockquote bullist undo redo | styleselect | fontselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent numlist backcolor | link image media | removeformat help',
			font_formats: "Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",
			toolbar_mode: 'floating',
			tinycomments_mode: 'embedded',
			tinycomments_author: 'Author name',
			menubar: false
		});
	});
});


