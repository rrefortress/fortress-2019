let prop_inputs = [
	{
		basic : 
		[
			{
				'title' : 'PLA ID',
				'name' : 'PLAID',
				'type'   : 'text',
				'required'  : 'required',
				'col' : 5,
				'ons' : ''
			},
			{
				'title' : 'Site Name',
				'name' : 'SITENAME',
				'type'   : 'text',
				'required'  : 'required',
				'col' : 7,
				'ons' : ''
			},
			{
				'title' : 'Proposed + Existing - For Dismantling',
				'name' : 'PROPOSED_EXISTING_DISMANTLED_UR',
				'type'   : 'text',
				'required'  : 'required',
				// 'col' : 7,
				// 'ons' : ''
			},
			
		],

	}
];
// let fields_list=["PLAID", "SITENAME", "REGION", "SITE_TYPE", "TOWER_TYPE", "TOWER_HEIGHT", "TOWER_HEIGHT_TYPE", "NSCP7_COMPLIANCE", "UTILIZATION_RATIO", "TRANSPORT_SOLUTION", "EPA_HEADROOM", "ANTENNA_MODEL", "ANTENNA_PORT", "ANTENNA_EPA", "ANTENNA_QTY", "HEADROOM_REMARKS", "TOTAL_ANTENNA_EPA", "TOWER_EPA", "MW_ANTENNA_SUMMARY", "RF_ANTENNA_SUMMARY", "RRU_SUMMARY", "TECHNOLOGY_SUMMARY", "RF_ANTENNA_PORT_SUMMARY", "TECHNOLOGY_PER_RRU_SUMMARY", "TOTAL_DESIGN_EPA", "TOTAL_EXT_EPA", "TOTAL_PROP_EXT_EPA", "TOTAL_PROP_EXT_DISTMANTLED_EPA", "EXISTING_UR", "PROPOSED_EXISTING_UR", "PROPOSED_EXISTING_DISMANTLED_UR", "TOWER_RECOMMENDATION"];
let fields_list=["PLAID", "SITENAME", "REGION", "SITE_TYPE", "TOWER_TYPE", "TOWER_HEIGHT", "TOWER_HEIGHT_TYPE", "NSCP7_COMPLIANCE", "UTILIZATION_RATIO", "TRANSPORT_SOLUTION", "EPA_HEADROOM", "ANTENNA_MODEL", "ANTENNA_PORT", "ANTENNA_EPA", "ANTENNA_QTY", "HEADROOM_REMARKS", "TOTAL_ANTENNA_EPA", "TOWER_EPA", "MW_ANTENNA_SUMMARY", "RF_ANTENNA_SUMMARY", "RRU_SUMMARY", "TECHNOLOGY_SUMMARY", "RF_ANTENNA_PORT_SUMMARY", "TECHNOLOGY_PER_RRU_SUMMARY", "TOTAL_DESIGN_EPA", "TOTAL_EXT_EPA", "TOTAL_PROP_EXT_EPA", "TOTAL_PROP_EXT_DISTMANTLED_EPA", "EXISTING_UR", "PROPOSED_EXISTING_UR", "PROPOSED_EXISTING_DISMANTLED_UR", "TOWER_RECOMMENDATION"];
let prop_sitesdatatable;
let prop_tblcolumns=[];
let prop_nowblinking;
let prop_clickedoperation="";
let prop_activeoperation="";
let prop_selectedtr = [];
let prop_selectallflag=0;
let prop_tdcolindex=0;
let prop_section;
let prop_textfinal='';
let prop_textinitial=null;
let prop_rowid = null;
let prop_fieldid = null;
let prop_tablecat = null;
let prop_dataset=[];
let prop_coldetails = [];
let prop_tower_tbl = $('#proposal_tower_tbl');
let prop_table_name = 'tower_proposal';
let current_data = [];
let result = [];
let cellheaders = ["Existing", "Proposed", "For Dismantling"];
let cellcolors = ["#AED6F1", "#A3E4D7", "#F5B7B1"];
let ind_hdrs = ["existing", "proposed", "dismantling"];
let antennaType = ["MW", "RF"];
let assesmentdata = [];
let alldata=[];
let proposed_sites = [];
let proposed_site_detials = [];
let not_found_sites = [];
let user_geoarea = $('#geo').val();
let user_id = $('#user_id').val();
let select_fields = "PLAID,SITENAME,REGION,SITE_TYPE,DETAILED_TOWER_TYPE,TOWER_HEIGHT,TOWER_HEIGHT_AND_TYPE,TRANSPORT_FROM_EFREN,RRE_ALL_TECH";

function set_proposal() {
	$('#pro_div').html('<div class="col-12 mg-top-135" style="height: 45vh;">\n' +
		'\t\t\t<h5 class="text-center text-white">\n' +
		'\t\t\t\t<i class="fas fa-table display-2 text-muted"></i></br>\n' +
		'\t\t\t\t<small class="text-center text-muted mg-top-10"><em>Please upload file first to display proposal lists.</em></small>\n' +
		'\t\t\t</h5>\n' +
		'\t\t</div>');
}

function get_proposal_list() {

	let param = {
		url: url + 'get_prop_list',
		dataType: 'json',
		type: 'GET',
	};
	// fortress(param).then((data) => {
	// 	let num = 1;
	// 	let html = '<table class="force-fit display nowrap text-center table-sm small table-responsive-sm table-hover table-striped table-bordered compact mt-3">\n' +
	// 		'\t\t\t\t\t<thead>\n' +
	// 		'\t\t\t\t\t<tr>\n' +
	// 		'\t\t\t\t\t\t<th scope="col" style="width: 10px">#</th>\n' +
	// 		'\t\t\t\t\t\t<th scope="col">NAME</th>\n' +
	// 		'\t\t\t\t\t\t<th scope="col" class="text-center">ACTION</th>\n' +
	// 		'\t\t\t\t\t</tr>\n' +
	// 		'\t\t\t\t\t</thead>\n' +
	// 		'\t\t\t\t\t<tbody>\n';
	// 		if (data.length > 0) {
	// 			_.forEach(data, function (val) {
	// 				html += '<tr>' +
	// 							'<td class="text-center">'+num+++'.</td>'+
	// 							'<td>'+val.name+'-'+val.id+'</td>'+
	// 							'<td>' +
	// 								'<button data-id="'+val.id+'" type="button" class="btn btn-sm btn-outline-primary view_prop-lists mr-1"><i class="fas fa-eye"></i> VIEW</button>' +
	// 								'<button data-id="'+val.id+'" type="button" class="btn btn-sm btn-outline-danger del_prop"><i class="fas fa-trash"></i> DELETE</button>' +
	// 							'</td>'+
	// 						 '</tr>';
	// 			});
	// 		}
	// 		html += '\t\t\t\t\t</tbody>\n' +
	// 		'\t\t\t\t</table>';
	// 		$('.prop-lists').html(html);
	// 		$('.prlists').dataTable();
	// });
	fortress(param).then((data) => {
		let num = 1;
		let html = '<table class="display nowrap text-center table-sm small table-responsive-sm table-hover table-striped table-bordered compact mt-3"></table>';
			$('.prop-lists').html(html);
			$('.prlists').dataTable();
	});
}

$(document).on('click', '.existing', function (e) {
	e.preventDefault();
	$('#existing').modal('show');
});

$(document).on('click', '.proposal', function (e) {
	e.preventDefault();
	// get_proposal_list();
	$('#proposal').modal('show'); 
});
$(document).on('onChange', '#fileDisplayAreaexisting', function (e){
    document.getElementById("fileInput").submit();
});

$(document).on("click", ".clear_pro", function (e) {
	e.preventDefault();
	set_proposal();
});

$(document).on("click", ".export_pro", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (result.length > 0) {
		JSONToCSVConvertor(result, "Proposal_(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

// $(document).on('keyup', '.prop_colsearch',function () {
// 	drawAllSearch(this,"colsearch");
// } );

// $(document).on("keyup", "#prop_srchinput", function () {
// 	drawAllSearch(this,"global");
// });
// function OnColSearch(input) {
// 	drawAllSearch(input,"colsearch");
// }
// function OnSearch(input) {
// 	drawAllSearch(input,"global");
// }
// function drawAllSearch(input,searchArea)
// {
// 	if(searchArea=="global")
// 	{
// 		prop_sitesdatatable.search(document.getElementById("prop_srchinput").value).draw();
// 	}
// 	else
// 	{
// 		let colindex = $(input).attr('id')-1;
// 		for(let i=1; i<=tblcolumns.length; i++){
// 		    if ( prop_sitesdatatable.column(colindex).search() !== input.value ) {
// 		        prop_sitesdatatable.column(colindex).search(input.value).draw();
// 		    }
// 		}
// 	}
	
// }



function colExtraction(){
	let rgxrep = new RegExp(/(\n[\D.]+[^\w. -@])+/g);
	let rgx1 = new RegExp(/(\d)-?([\w. ]+[^@])@([\w /.]+[^\n\s"])/gm);
	let rgx2 = new RegExp(/([A-Za-z0-9/-]+)/);

	let col_to_extract = [18,19];
	

	$.each(col_to_extract, function (index0, val0) {
		$.each(alldata, function (index0, value0) {
			let cdata = value0[val0].trim();
			cdata = cdata.replace(rgxrep, "|");
			cdata = cdata.split("|");
			$.each(cdata, function (index1, value1) {
				// console.log(value1);
				value1 = value1.split(/(\r?\n)/g);
				// console.log(value1);
				$.each(value1, function (index2, value2) {
					let match =rgx1.exec(value2); 
					if(match!==null){
						let pushflag = 0;
						// if(assesmentdata!=null){

						// 	$.each(assesmentdata, function (index3, value3) {
						// 		if(value3[3]==ind_hdrs[index1] && (value3[5].toLowerCase())==(match[2].toLowerCase())){
						// 			console.log(value3);
						// 			value3[4]+=parseInt(match[1]);
						// 			console.log(value3);
						// 			pushflag = 1;
						// 		}
						// 	});
						// }
						// if(!pushflag)
						{
							assesmentdata.push([value0[0],value0[1],antennaType[val0-18],ind_hdrs[index1],parseInt(match[1]),match[2],match[3],0]);
							// console.log(assesmentdata);
						}
						// console.log(assesmentdata);
					}
				});
			});
		});
	})
	// $.each(alldata, function (index0, value0) {
	// 		let cdata = value0[20].trim();
	// 		cdata = cdata.replace(rgxrep, "|");
	// 		cdata = cdata.split("|");
	// 		$.each(cdata, function (index1, value1) {
	// 			let rgxrep2 = new RegExp(/(,|\r?\n)/g);
	// 			value1 = value1.split(rgxrep2);
				
	// 			$.each(value1, function (index2, value2) {
	// 				let match =rgx2.exec(value2); 
	// 				// console.log(match);
	// 				if(match!==null){
	// 					// console.log(match);
	// 					// if(index1==4) index1=3;
	// 					assesmentdata.push([value0[0],value0[1],"RRU",ind_hdrs[index1],1,match[1],"",0]);
						
	// 				}
	// 			});
	// 			// console.log(assesmentdata);
	// 		});
	// 	});
	// console.log(assesmentdata);
	return;
}
function recalculate(){
	// colExtraction();
	
	$.each(assesmentdata, function (index0, value0) {
		// console.log(value0);
		$.each(antenna_epa_table, function (key, value) {
			
			// if((value.ANTENNA_MODEL).includes(value0[5])){

			if((value.ANTENNA_MODEL.toLowerCase()).includes(value0[5].toLowerCase())){
				// console.log(value0[5]+" - "+value.ANTENNA_MODEL+" - "+parseInt(value.ANTENNA_EPA));
				// assesmentdata[index0][7] = (value.ANTENNA_EPA);
				assesmentdata[index0][7] = parseFloat(value.ANTENNA_EPA);
			}
		});
	});

	let tmp=0;

	$.each(alldata, function (index0, value0) {
		value0[25]=0;
		value0[26]=0;
		value0[27]=0;

		$.each(tower_epa_table, function (key, value) {
				
			if(value0[6]!=null &&(value.TOWER_HEIGHT_AND_TYPE.toLowerCase()).includes(value0[6].toLowerCase())){
				value0[17] = parseFloat(value.TOWER_EPA);
				// console.log(value0[6] +" - "+value.TOWER_HEIGHT_AND_TYPE+" "+value0[17]);
				
			}
		});

		$.each(assesmentdata, function (index1, value1) {
			// console.log(value0);
			if(value0[0]==value1[0] && value1[3]=="existing"){
				// console.log(value1);
				value0[25]+= (parseInt(value1[4])*value1[7]);
				// console.log(value0[0] +" - "+ value0[25] +" "+(parseInt(value1[4])*value1[7]) +" += " +parseInt(value1[4])+" * "+value1[7] + " "+value1[5]);
			}
			if(value0[0]==value1[0] && value1[3]=="proposed"){
				// console.log(value1);
				value0[26]+= (parseInt(value1[4])*value1[7]);
				// console.log(value0[0] +" - "+ value0[25] +" "+(parseInt(value1[4])*value1[7]) +" += " +parseInt(value1[4])+" * "+value1[7] + " "+value1[5]);
			}
			if(value0[0]==value1[0] && value1[3]=="dismantling"){
				// console.log(value1);
				value0[27]+= (parseInt(value1[4])*value1[7]);
				// console.log(value0[0] +" - "+ value0[25] +" "+(parseInt(value1[4])*value1[7]) +" += " +parseInt(value1[4])+" * "+value1[7] + " "+value1[5]);
			}
		});
		value0[25]+=parseFloat(value0[17]);
		value0[26]+=value0[25];
		let tmpb = value0[26]-value0[27]
		value0[27]=tmpb;

		value0[28]=(value0[25]/parseFloat(value0[24]))*100;
		value0[29]=(value0[26]/parseFloat(value0[24]))*100;
		value0[30]=(value0[27]/parseFloat(value0[24]))*100;
		let tmpc = (value0[27]/parseFloat(value0[24]));
		value0[31] = tmpc>0.9&&tmpc<1.06 ? "Proceed_Tower @ Threshold" :  tmpc<0.9? "Proceed" : tmpc>1.05 ? "Requires Upgrade" : "" ;
		// ,IF(OH6<0.9,"Proceed",IF(OH6>1.05,"Requires Upgrade","-")))
	});
	// $.each(alldata, function (index0, value0) {

	// });

	return;
}
function get_proposal() {
	
	// $.each(prop_inputs[0], function (index, value) {
	// 	$.each(value, function (index, value2) {
	// 		// if(value2.name != "DROP_WIRES" && value2.name != "ACU_CAPACITY_HP" )
	// 		// {
	// 			prop_coldetails.push(value2);
	// 		// }
	// 	});
	// });

	// get_all_db_dropdowns();
	

	// fortress(param).then((data) => {
		console.log(alldata);
		// console.log(assesmentdata);
		var temp = "";
		$.each(fields_list, function (index, value) {
			$.each(prop_coldetails, function (indexin, valuecol) {
				if (valuecol.name == value) {
					prop_tblcolumns.push({"title": valuecol.title, "name": value});
					temp = valuecol.name;
					return;
				}
			});
			if (temp != value) {
				prop_tblcolumns.push({"title": value, "name": value});
			}

			tblcolumns.push({"title":value});
		});
		// console.log(tblcolumns.name);
		let reg_hdr=1;
		$.each(prop_tblcolumns, function (ind, val) {
			if(val.name == "REGION" || val.name == "GEOAREA" )
			{
				reg_hdr = ind;
			}
		});
		// let hiddencols = $('#geo').val() !== 'nat' ? [0,reg_hdr,27,28,29,30] : [0];
		let hiddencols = user_geoarea.toLowerCase() !== 'nat' ? [0,reg_hdr] : [0];
		prop_sitesdatatable = prop_tower_tbl.DataTable({
			"dom": 'lrt<"row"<"col"i><"col"p>>',
			"processing": true,
			stateSave: true,
			// "serverSide": true,
			"pageLength": 15,
			"lengthMenu": [[5, 15, 30, 60, 100, -1], [5, 15, 30, 60, 100, "All"]],
			"columns": prop_tblcolumns,
			scrollX:        true,
	        scrollCollapse: true,
	        paging:         true,
	        fixedColumns:   true,
	        fixedColumns: {
			    leftColumns: 3
			  },
			"data": alldata,
			// "ajax": {
			// 	url: url + "generatedatatable",
			// 	data: {displayto: prop_table_name, request: "data"},
			// 	type: "POST",
			// },
			// "bJQueryUI": true,
        	// "bStateSave": true,
			fnCreatedRow: function (nRow, aData) {
				$(nRow).attr('id', aData[0]);

			},
			buttons: [
				  {
				     extend: 'print',
				     text: '<i style="font-size:24px;color:#337ab7" class="fa fa fa-print fa-2x"></i>',
				     exportOptions: 
				      {
				            stripHtml: false
				      }
				    }
			],
			"columnDefs": [
				{
					"visible": false,
					"targets": [20,21,22,23],
					// "targets": hiddencols,
					// "targets": [0,reg_hdr,27,28,29,30],
				},
				// {
				// 	targets: '_all',
				// 	render: function (data, type, row) {
				// 		// console.log(row[0]);
				// 		var color = 'black';
				// 		let value = data === null || data === undefined? 'to be updated' : data.toLowerCase();
				// 		// if(data == null)
				// 		// {
				// 		// 	value = 
				// 		// }else{
				// 		// 	value = 
				// 		// }
				// 		if (value === "to be updated") {
				// 			color = 'red';
				// 			return '<span style="color:' + color + '">' + data + '</span>';
				// 		} else if (value === "none") {
				// 			color = 'gray';
				// 			return '<span style="color:' + color + '">' + data + '</span>';
				// 		}else{
				// 			return data;
				// 		}
				// 	}
				// },
				{
					targets: [24,25,26,27],
					createdCell: function (td, data, rowData, row, col) {
						$(td).html('<span style="font-weight:bold">' + ((parseFloat(data)).toFixed(2)) + '</span>');
						// $(td).css('background-color', color);

					}
				},
				{
					targets: [28,29,30],
					createdCell: function (td, data, rowData, row, col) {
						let dat = ((parseFloat(data)).toFixed(2));
						var color = cellcolors[0];
						if(dat>105){
							color= cellcolors[2];
						}
						$(td).html('<span style="font-weight:bold; background-color:'+color+';">' + dat + '</span>');
						// $(td).css('background-color', color);
						// return '<span style="font-weight:bold; background-color:'+color+';">' + dat + '%</span>';
					}
				},

				// {
				// 	targets: [28,29,30],
				// 	render: function (data, type, row) {
				// 		return '<span style="font-weight:bold">' + ((parseFloat(data)).toFixed(2)) + '</span>';
				// 	}
				// },
				{
					targets: [31],
					// render: function (data, type, row) {
					createdCell: function (td, data, rowData, row, col) {
						var color = 'red';

						if (data == "Proceed_Tower @ Threshold") {
							color = cellcolors[1];

						} else if (data == "Proceed") {
							color = cellcolors[0];
						}
						else if (data == "Requires Upgrade") {
							color = cellcolors[2];
						}
						$(td).html('<span style="font-weight:bold">' + data + '</span>');
						$(td).css('background-color', color);
						// return '<span style="font-weight:bold; background-color:'+color+';">' + data + '</span>';
					}
				},
				{
					targets: [18,19],
					// targets: [18,19,20,21,22,23],
					createdCell: function (td, cellData, rowData, row, col) {
						$celltr = $('<tr></tr>',{});
						$celltr.append($('<tr><th>QTY</th><th>Model</th><th>Elevation</th><th>EPA</th></tr>',{}));
						// console.log(td+" \n "+cellData+" \n "+rowData+" \n "+row+" \n "+col);
						// console.log(col);
						$.each(ind_hdrs, function (index0, value0) {
							$a = $('<tr></tr>',{});
							$a.append($('<th></th>',{"text":cellheaders[index0], "colspan":"4" ,"bgcolor":cellcolors[index0]}));
							$celltr.append($a);
							$.each(assesmentdata, function (index, value) {
								// console.log(value[2]);
								if(value[0]==rowData[0] && value[2]==antennaType[col-18]) {
									// console.log(value);
									if(value[3]==value0){
										$a = $('<tr></tr>',{});
										$a.append($("<td></td>", {"text": value[4], "bgcolor":cellcolors[index0]}));
										$a.append($("<td></td>", {"text": value[5], "bgcolor":cellcolors[index0]}));
										$a.append($("<td></td>", {"text": value[6], "bgcolor":cellcolors[index0]}));
										$a.append($("<td></td>", {"text": (value[4]*value[7]).toFixed(2), "bgcolor":cellcolors[index0]}));
										// $a.append($("<td></td>", {"text": value[7], "bgcolor":cellcolors[index0]}));
										$celltr.append($a);
									}
								}
								$(td).html($celltr.prop("outerHTML"));
							});
						});
							// console.log(value[0]);
							// $celltr.append($('<th></th>',{"text":cellheaders[index], "colspan":"3" ,"bgcolor":cellcolors[index]}));
							// value = value.split(/\r?\n/);
							// $.each(value, function (index2, value2) {
							// 	let re = new RegExp(/(\d)-?([\w. ]+[^@])@([\w /.]+[^\n\s"])/gm);
							// 	let match =re.exec(value2); 
							// 	if(match!==null){
							// 		if(index==0){
							// 			// console.log(cellheaders[index]);
							// 			$.each(antenna_epa_table, function (key, val) {
							// 					if(val["ANTENNA_MODEL"]==match[2]){
							// 						// console.log(val["ANTENNA_EPA"]);
							// 						TOTAL_EXT_EPA+=parseFloat(val["ANTENNA_EPA"]);
							// 					}
							// 			});
							// 			// console.log(TOTAL_EXT_EPA);
							// 		}else if(index == 1){
							// 			// console.log(cellheaders[index]);
							// 			$.each(antenna_epa_table, function (key, val) {
							// 				if(val["ANTENNA_MODEL"]==match[2]){
							// 					// console.log(val["ANTENNA_EPA"]);
							// 					TOTAL_EXT_EPA+=val["ANTENNA_EPA"];
							// 				}
							// 			});
							// 			// console.log(TOTAL_EXT_EPA);
							// 		}else if(index==2){
							// 			// console.log(cellheaders[index]);
							// 			$.each(antenna_epa_table, function (key, val) {
							// 				if(val["ANTENNA_MODEL"]==match[2]){
							// 					// console.log(val["ANTENNA_EPA"]);
							// 					TOTAL_EXT_EPA+=val["ANTENNA_EPA"];
							// 				}
							// 			});
							// 			// console.log(TOTAL_EXT_EPA);
							// 		}
							// 		$tbodytr = $('<tr></tr>',{});
							// 		$tbodytr.append($("<td></td>", {"text": match[1], "bgcolor":cellcolors[index]}));
							// 		$tbodytr.append($("<td></td>", {"text": match[2], "bgcolor":cellcolors[index]}));
							// 		$tbodytr.append($("<td></td>", {"text": match[3], "bgcolor":cellcolors[index]}));
							// 		$celltr.append($tbodytr);
							// 		$celltr.append($tbodytr);
							// 	}
							// });

						
						// console.log(TOTAL_EXT_EPA);
						// TOTAL_EXT_EPA = 0;
					}
				},
				{
					targets: [20,21,22,23],
					// targets: [0],
					createdCell: function (td, cellData, rowData, row, col) {
						// console.log(td+" \n "+cellData+" \n "+rowData+" \n "+row+" \n "+col);
						let cdata = cellData;
						cdata = cdata.trim();
						// console.log(cdata);
						// let re2 = RegExp(/([\D.|]+([^\w. -@\r\n])+)/);
						// cdata = cdata.replace(re2, "|");
						// console.log(cdata);

						cdata = cdata.split("|");
						$celltr = $('<tr></tr>',{});
						// $celltr.append($('<tr><td>QTY</td><td>Model</td><td>Elevation</td></tr>',{}));
						
						$.each(cdata, function (index, value) {
							$celltr.append($('<th></th>',{"text":cellheaders[index], "colspan":"1" ,"bgcolor":cellcolors[index]}));
							value = value.split(/\r?\n/);
							 // console.log(value);
							$.each(value, function (index2, value2) {
								// let re = new RegExp(/(\d)-?([\w. ]+[^@])@([\w /.]+[^\n\s"])/gm);
								// let match =re.exec(value2); 
								if(value2!==""){
									$tbodytr = $('<tr></tr>',{});
									$tbodytr.append($("<td></td>", {"text": value2, "bgcolor":cellcolors[index]}));
									// $tbodytr.append($("<td></td>", {"text": value}));
									// $celltr.append($tbodytr);
									$celltr.append($tbodytr);

								}
							});
						});
						$(td).html($celltr.prop("outerHTML"));
						
					}
				},
			],
			initComplete: async function () {
				let proposal_tower_length = $('#proposal_tower_tbl_length');
				$(proposal_tower_length).addClass('row mt-2');
				$('#proposal_tower_tbl_length label').addClass('col-2 m-0 p-0 ml-3');
				// $(proposal_tower_length).append(`
	   //              <div class="col-3 form-group has-search m-0 p-0">
	   //                  <span class="fa fa-search form-control-feedback form-control-xs mt-2"></span>
	   //                  <input onsearch="OnSearch(this)" type="search" class="form-control form-control-sm rounded-pill" id="prop_srchinput" placeholder="Search..." autofocus data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
	   //          	</div>`);
				// if ($('#geo').val() !== 'nat') {
				// 	$(proposal_tower_length).append(`
				// 				<div class="col-6 ml-5">
				// 					<div class="">
				// 						<h5 class="font-weight-normal m-0"></h5>
				// 					</div>
				// 					<div class="row d-flex justify-content-end">
				// 						<div class="operationtoggle btn-group btn-group-toggle" data-toggle="buttons">
				// 							<label id="edit" class="btn btn-outline-warning my-1 px-1 p-0">
				// 								<input type="radio" name="options"  autocomplete="off"><i class="fas fa-edit"></i> EDIT
				// 							</label>
				// 							<label id="delete" class="btn btn-outline-danger my-1 px-1 p-0">
				// 								<input type="radio" name="options" autocomplete="off"><i class="fas fa-trash"></i> DELETE
				// 							</label>
				// 						</div>
				// 						<button id="select" type="button" class="btn btn-sm btn-link"><i class="fas fa-check-square"></i> SELECT / UNSELECT All</button>
				// 						<button id="clear" type="button" class="btn btn-sm btn-link"><i class="fas fa-eraser"></i> CLEAR</button>
				// 						<button id="proceed" type="button" class="btn btn-sm btn-link"><i class="fas fa-check-circle"></i> PROCEED</button>
				// 					</div>
				// 				</div>`);
				// 	$("#select").attr('disabled', true);
				// 	$("#proceed").attr('disabled', true);
				// 	$("#clear").attr('disabled', true);
				// }

				// $tr = $('<tr id="colfilter"></tr>',{});
				// $.each(tblcolumns, function (key1, val1) {
				// 	if(val1.title=="id"){return;}
				// 	// console.log(val1);
				// 	let $th = $('<th></th>', {});
		  //           let $inp = $('<select></select>', {
		  //           				"id":prop_table_name+key1,
		  //           				"type": "search",
		  //           				"onsearch":"OnColSearch(this)",
		  //           				"class":"prop_colsearch cellinputsize form-control form-control-sm",
		  //                           // "placeholder": val1.title,
		  //                        });
	   //              $th.append($inp);
    //                 $tr.append($th);
    //             });
				// $("#proposal_tower_tbl thead").append($tr.prop("outerHTML"));
			},

		});

		// yadcf.init(prop_sitesdatatable, [
	 //        {
	 //        	column_number : 0,
  //               select_type: 'select2',
  //               // filter_container_id: prop_table_name+1,
  //               filter_type: "multi_select",
  //               filter_match_mode: "exact",
  //               select_type_options: {
  //                   width: '100%',
  //                   closeOnSelect : true,
  //                   'theme': 'bootstrap',
  //                   'placeholder': "Filter",

  //       			}
  //       	},
  //       	 {
	 //        	column_number : 1,
  //               select_type: 'select2',
  //               // filter_container_id: prop_table_name+1,
  //               filter_type: "multi_select",
  //               filter_match_mode: "exact",
  //               select_type_options: {
  //                   width: '100%',
  //                   closeOnSelect : true,
  //                   'theme': 'bootstrap',
  //                   'placeholder': "Filter",
  //       			}
  //       	},
  //       	{
	 //        	column_number : 2,
  //               select_type: 'select2',
  //               // filter_container_id: prop_table_name+1,
  //               filter_type: "multi_select",
  //               filter_match_mode: "exact",
  //               select_type_options: {
  //                   width: '100%',
  //                   closeOnSelect : true,
  //                   'theme': 'bootstrap',
  //                   'placeholder': "Filter",
  //       			}
  //       	},
  //       	{
	 //        	column_number : 3,
  //               select_type: 'select2',
  //               // filter_container_id: prop_table_name+1,
  //               filter_type: "multi_select",
  //               filter_match_mode: "exact",
  //               select_type_options: {
  //                   width: '100%',
  //                   closeOnSelect : true,
  //                   'theme': 'bootstrap',
  //                   'placeholder': "Filter",
  //       			}
  //       	},
  //       	{
	 //        	column_number : 4,
  //               select_type: 'select2',
  //               // filter_container_id: prop_table_name+1,
  //               filter_type: "multi_select",
  //               filter_match_mode: "exact",
  //               select_type_options: {
  //                   width: '100%',
  //                   closeOnSelect : true,
  //                   'theme': 'bootstrap',
  //                   'placeholder': "Filter",
  //       			}
  //       	},
  //       	{
	 //        	column_number : 5,
  //               select_type: 'select2',
  //               // filter_container_id: prop_table_name+1,
  //               filter_type: "multi_select",
  //               filter_match_mode: "exact",
  //               select_type_options: {
  //                   width: '100%',
  //                   closeOnSelect : true,
  //                   'theme': 'bootstrap',
  //                   'placeholder': "Filter",
  //       			}
  //       	},
  //       	{
	 //        	column_number : 6,
  //               select_type: 'select2',
  //               // filter_container_id: prop_table_name+1,
  //               filter_type: "multi_select",
  //               filter_match_mode: "exact",
  //               select_type_options: {
  //                   width: '100%',
  //                   closeOnSelect : true,
  //                   'theme': 'bootstrap',
  //                   'placeholder': "Filter",
  //       			}
  //       	},
  //       	{
	 //        	column_number : 7,
  //               select_type: 'select2',
  //               // filter_container_id: prop_table_name+1,
  //               filter_type: "multi_select",
  //               filter_match_mode: "exact",
  //               select_type_options: {
  //                   width: '100%',
  //                   closeOnSelect : true,
  //                   'theme': 'bootstrap',
  //                   'placeholder': "Filter",
  //       			}
  //       	},
  //       	{
	 //        	column_number : 8,
  //               select_type: 'select2',
  //               // filter_container_id: prop_table_name+1,
  //               filter_type: "multi_select",
  //               filter_match_mode: "exact",
  //               select_type_options: {
  //                   width: '100%',
  //                   closeOnSelect : true,
  //                   'theme': 'bootstrap',
  //                   'placeholder': "Filter",
  //       			}
  //       	},
  //       	{
	 //        	column_number : 28,
  //               select_type: 'select2',
  //               // filter_container_id: prop_table_name+1,
  //               filter_type: "multi_select",
  //               filter_match_mode: "exact",
  //               select_type_options: {
  //                   width: '100%',
  //                   closeOnSelect : true,
  //                   'theme': 'bootstrap',
  //                   'placeholder': "Filter",
  //       			}
  //       	},
  //       	{
	 //        	column_number : 29,
  //               select_type: 'select2',
  //               // filter_container_id: prop_table_name+1,
  //               filter_type: "multi_select",
  //               filter_match_mode: "exact",
  //               select_type_options: {
  //                   width: '100%',
  //                   closeOnSelect : true,
  //                   'theme': 'bootstrap',
  //                   'placeholder': "Filter",
  //       			}
  //       	},
  //       	{
	 //        	column_number : 30,
  //               select_type: 'select2',
  //               // filter_container_id: prop_table_name+1,
  //               filter_type: "multi_select",
  //               filter_match_mode: "exact",
  //               select_type_options: {
  //                   width: '100%',
  //                   closeOnSelect : true,
  //                   'theme': 'bootstrap',
  //                   'placeholder': "Filter",
  //       			}
  //       	},
  //       	{
	 //        	column_number : 31,
  //               select_type: 'select2',
  //               // filter_container_id: prop_table_name+1,
  //               filter_type: "multi_select",
  //               filter_match_mode: "exact",
  //               select_type_options: {
  //                   width: '100%',
  //                   closeOnSelect : true,
  //                   'theme': 'bootstrap',
  //                   'placeholder': "Filter",
  //       			}
  //       	},
		//     // {
		//     //     column_number: 3,
		//     //     filter_type: "multi_select",
		//     //     // filter_container_id: "external_filter_container"
		//     // },
		// 	        // {
	 //          //    column_number: 3,
	 //          //   'filter_type': "multi_select",
	 //          //   'select_type': "select2",
	 //            // 'select_type_options': {
	 //            // 	'theme': 'bootstrap',
	 //            // 	'placeholder': "Filter",
	 //            // },
	 //        // }
  //   	]);
	// });
	return;
}
// prop_tower_tbl.on( 'draw.dt', function (e,oSettings) {
//         var th = $('tr th',this);
//         $.each(oSettings.aoPreSearchCols, function( index, search ) {
//             if (th.length > index){
//                 if (search.sSearch != ""){
//                     $(th[index]).addClass('col-filtered');
//                 } else {
//                     $(th[index]).removeClass('col-filtered');
//                 }
//             }
//         });
//     });
$(document).on("dblclick",'#proposal_tower_tbl tr', function(e) {
	get_epa_table();
	$('#edit_proposal').modal('show');
});

function get_epa_table() {
	let param = {
		url: url + 'get_antenna_epa_table',
		dataType: 'json',
		type: 'POST',
		// data: $(this).serialize()
	};
	fortress(param).then((data) => {
		antenna_epa_table= data;
		// console.log(antenna_epa_table);
		// $('#save-pro').modal('show');
	});
	let param2 = {
		url: url + 'get_tower_epa_table',
		dataType: 'json',
		type: 'POST',
		// data: $(this).serialize()
	};
	fortress(param2).then((data) => {
		tower_epa_table= data;
		// console.log(antenna_epa_table);
		// $('#save-pro').modal('show');
	});
}

function upload_existing() {
  var file = fileInput.files[0];
  var textType = /text.*/;
  var csvType = 'text/csv';
  // if (file.type.match(csvType)) {
  var reader = new FileReader();

  new Promise(function(resolve, reject) {	
	
	  reader.onload = function(e) {
	      // console.log(csv_to_array(reader.result));
	      let data = csv_to_array(reader.result);
	      // assesmentdata.push([value0[0],value0[1],antennaType[val0-18],ind_hdrs[index1],parseInt(match[1]),match[2],match[3],0]);
	      assesmentdata=[];
	      // console.log(data);
	      proposed_sites=[];
	      proposed_site_detials=[];
	      alldata=[];
	      $.each(data, function (index, value) {
	      		if(index != 0 && value[0] != "" && value[0] != undefined && value.length>0)
		      	{
		      		// console.log(value);
		      		proposed_sites.push([value[0],value[1],value[2]]);
		      		proposed_site_detials.push([value[11],value[12],value[3],value[4],value[5],value[6],value[7],value[8],value[9],value[10]]);
		      		
		      	}
	    	});
	      // console.log(proposed_sites);
	      // console.log(proposed_site_detials);
	      // alldata.push(fields_list);
	      $.each(data, function (index0, value0) {
		      	// $.each(alldata, function (index1, value1) {
		      	if(value0[0] != "PLAID" && value0[0] != "" && value0[0] != undefined)
		      	{
		      		// if(value0[0]=="00001"){
		      		// 	// console.log(value0[0]+" - "+value1[0]);
		      		// 	value0[0]="1";
		      		// }
		      		// if(value0[0]==value1[0]){
		      			let i = 0;
		      			// while(i<=17)
		      			// {
		      			// 	value1[i]=value0[i];
		      			// 	i++;
		      			// }
		      			i = 13;
		      			while(i<=48)
		      			{
		      				if(value0[i+1]!=""){
		      					assesmentdata.push([value0[0],value0[1],"MW","existing",parseInt(value0[i+1]),value0[i],value0[i+2],0]);
		      				}
		      				i+=4;
		      			}
		      			i = 50;
		      			while(i<=175)
		      			{
		      				if(value0[i+3]!=""){
		      					assesmentdata.push([value0[0],value0[1],"RF","existing",parseInt(value0[i+3]),value0[i],value0[i+4],0]);
		      				}
		      				i+=21;
		      			}
		      			// console.log(alldata);
		      		// }
		      // });
		      	}
	      	});
	      // console.log(proposed_sites);
	       
	       // get_proposal_tower_data();
	       	$('#existing').modal('hide');
			$('#proposal').modal('show');
			}
			reader.readAsText(file);


		// if(get_proposal_tower_data()){
		// 	alert();
		// 	return;
		// }
			setTimeout(function(){ 
				get_epa_table();
				resolve()}, 
			500);

		}).then(function(result) { // (***)
			
			// console.log(proposed_sites);
			new Promise(function(resolve, reject) {
						$('.t-msg').addClass('d-none');
						let param = {
								url: url + "generatedatatable",
								dataType: 'json',
								type: 'POST',
								data: {displayto: 'tower', request: "select_sites",select_fields, proposed_sites},
							};
						fortress(param).then((data) => {
							console.log(proposed_sites);
							console.log(data);
							not_found_sites = proposed_sites;
							let snfArr = [];
							$.each(proposed_sites, function (index0, value0) {
								
									$.each(data, function (index, value) {
										
										if(value0!==undefined)
										{	
											console.log(value0+" - "+index0);
											// console.log(value0+" - "+value);
											if((((value0[0]).toLowerCase()).trim())==(((value.PLAID).toLowerCase()).trim()) && (((value0[1]).toLowerCase()).trim())==(((value.SITENAME).toLowerCase()).trim()))
											{
												console.log(value.PLAID);	
												alldata.push([value0[0],value0[1],value.REGION,value.SITE_TYPE,value.DETAILED_TOWER_TYPE,value.TOWER_HEIGHT,value.TOWER_HEIGHT_AND_TYPE,proposed_site_detials[index0][2],proposed_site_detials[index0][3],value.TRANSPORT_FROM_EFREN,proposed_site_detials[index0][4],proposed_site_detials[index0][5],proposed_site_detials[index0][6],proposed_site_detials[index0][7],proposed_site_detials[index0][8],proposed_site_detials[index0][9],proposed_site_detials[index0][0],'','','','','','','',proposed_site_detials[index0][1],'','','','','','','']);
													snfArr.push(index0);
											 }
										}
									});
							});
							console.log(alldata);
							$.each(not_found_sites, function (index1, value1) {
								if(snfArr.includes(index1))
								{
									not_found_sites.splice(index1)
								}
							});
							console.log(not_found_sites);
							// return;
						});
					setTimeout(function(){ resolve()}, 100);

				});

		// if(proposed_sites.length == alldata.length)
		// 			{
		// 				resolve();
		// 			}
	});
}
function upload_proposal() {
  var file = fileInputProp.files[0];
  var textType = /text.*/;
  var csvType = 'text/csv';
  // if (file.type.match(csvType)) {
  var reader = new FileReader();

   new Promise(function(resolve, reject) {	
		  reader.onload = function(e) {
		      // console.log(csv_to_array(reader.result));
		      let data = csv_to_array(reader.result);
		      // assesmentdata.push([value0[0],value0[1],antennaType[val0-18],ind_hdrs[index1],parseInt(match[1]),match[2],match[3],0]);
		      // assesmentdata=[];
		      // console.log(data);
		      $.each(data, function (index0, value0) {
			      	$.each(alldata, function (index1, value1) {

			      		// if(value0[0]=="00001"){
			      		// 	// console.log(value0[0]+" - "+value1[0]);
			      		// 	value0[0]="1";
			      		// }
			      		if(value0[0]==value1[0]){
			      			let i = 3;
			      			while(i<=22)
			      			{
			      				if(value0[i+1]!=""){
			      					assesmentdata.push([value0[0],value0[1],"MW","proposed",parseInt(value0[i+1]),value0[i],value0[i+2],0]);
			      				}
			      				i+=4;
			      			}
			      			i = 24;
			      			while(i<=128)
			      			{
			      				if(value0[i+3]!=""){
			      					assesmentdata.push([value0[0],value0[1],"RF","proposed",parseInt(value0[i+3]),value0[i],value0[i+4],0]);
			      				}
			      				i+=21;
			      			}
			      			i = 134;
			      			while(i<=145)
			      			{
			      				if(value0[i+1]!=""){
			      					assesmentdata.push([value0[0],value0[1],"MW","dismantling",parseInt(value0[i+1]),value0[i],value0[i+2],0]);
			      				}
			      				i+=4;
			      			}
			      			i = 147;
			      			while(i<=194)
			      			{
			      				if(value0[i+3]!=""){
			      					assesmentdata.push([value0[0],value0[1],"RF","dismantling",parseInt(value0[i+3]),value0[i],value0[i+4],0]);
			      				}
			      				i+=16;
			      			}
			      			// console.log(alldata);
			      		}
			      });
		      });
		       // console.log(assesmentdata);
		       
		       
		  }
		  reader.readAsText(file);

		  setTimeout(function(){ resolve()}, 300);
	}).then(function(result) { // (***)
		recalculate();
		setTimeout(function(){ return;}, 100);
		// return;
	}).then(function(result) { // (***)
		get_proposal();
		// setTimeout(function(){ return;}, 500);
		$('#proposal').modal('hide');
		return;
		

	});
	// get_proposal_tower_data();
}
function csv_to_array(text) {
    let p = '', row = [''], ret = [row], i = 0, r = 0, s = !0, l;
    for (l of text) {
        if ('"' === l) {
            if (s && l === p) row[i] += l;
            s = !s;
        } else if (',' === l && s) l = row[++i] = '';
        else if ('\n' === l && s) {
            if ('\r' === p) row[i] = row[i].slice(0, -1);
            row = ret[++r] = [l = '']; i = 0;
        } else row[i] += l;
        p = l;
    }
    return ret;
};

function fnExcelReport(elem)
{
    // var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
    // // var textRange; var j=0;
    // tab = document.getElementById('proposal_tower_tbl'); // id of table

    // for(j = 0 ; j < tab.rows.length ; j++) 
    // {     
    //     tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
    //     //tab_text=tab_text+"</tr>";
    // }

    // tab_text=tab_text+"</table>";
    // // tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    // // tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    // tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
    // tab_text= tab_text.replace(/<div id="yadcf.*>.*?<\/div>/gi, "");
    // console.log(tab_text);

    //     sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

    // return (sa);


    $(".dataTables_scroll").printThis();




}
$(document).on('click', '.view-proposal', function (e) {
	e.preventDefault();
	get_proposal_list();
	$('#view-pro-list').modal('show');
});

function get_proposal_list() {
	let param = {
		url: url + 'get_prop_list',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then((data) => {
		let num = 1;
		let html = '<ul class="list-group">';
		if (data.length > 0) {
			_.forEach(data, function (val) {
				html += '<li class="list-group-item p3">' +
							'<div class="row">' +
								'<div class="col-7">'+num+++'. | '+val.name+'</div>' +
								'<div class="col-5">' +
									'<button data-id="'+val.id+'" type="button" class="btn btn-sm btn-outline-success view_prop-lists mr-1"><i class="fas fa-eye"></i> VIEW</button>' +
									'<button data-id="'+val.id+'" type="button" class="btn btn-sm btn-outline-danger del_prop mr-1"><i class="fas fa-trash"></i> DELETE</button>' +
									'<button data-name="'+val.name+'" data-id="'+val.id+'" type="button" class="btn btn-sm btn-outline-warning edit_prop"><i class="fas fa-edit"></i> RENAME</button>' +
								'</div>' +
							'</div>' +
						'</li>';
			});
		} else {
			html = '<li class="list-group-item list-group-item-danger">No Available Proposal.</li>';
		}
		html += '</ul>';
		$('.prop-lists').html(html);
	});
}

function get_proposal_tower_data(){
	
	new Promise(function(resolve, reject) {

			setTimeout(function(){ 

				resolve()}, 0);
		
	}).then(function(result) { // (***)
		recalculate();
		return;
	}).then(function(result) {

	  	get_proposal();
	 	$('#proposal').modal('hide');
	  	return;
	});

}
function save_proposal(){
	let proposal_name = $("#tower_proposal_name").val();
	let batch_key = moment().format('MMM_D_YY_HHMMSS');
	batch_key = proposal_name == "" ? "Tower_Proposal_"+batch_key : proposal_name+"_"+batch_key;
	let dateNow = moment().format('YY-MM-DD HH:MM:SS');
	let prop_data = [];
	$.each(alldata, function (ind, val) {
		let temp={user_id:user_id,batch_key:batch_key,date:dateNow};
		$.each(prop_tblcolumns, function (ind1, val1) {
			temp[val1["title"]]=val[ind1];
		});
		prop_data.push(temp);
	});
	let param = {
		url: url + 'save_tower_proposal',
		dataType: 'json',
		type: 'POST',
		data: {displayto: prop_table_name, prop_data:prop_data},
	};
	fortress(param).then((data) => {
		SWAL('success', 'Success!', data+' Site proposal(s) saved.');
		// get_proposal(data);
	});
	
}

$(document).on("click", ".save-proposal", function (e) {
	e.preventDefault();
	let val = $('#tower_proposal_name').val();
	if (val === '') {
		SWAL('error', 'Oops!', 'Please input name for proposal');
		$('#tower_proposal_name').focus();
	} else {
		save_proposal();
	}
});

$(document).on("click", ".view_prop-lists", function (e) {
	e.preventDefault();
	let id = $(this).data('id');
	let param = {
		url: url + 'get_tower_proposal',
		dataType: 'json',
		type: 'POST',
		data: {batch_key: id}
	};
	fortress(param).then((data) => {
		$('#view-pro-list').modal('hide');
		get_proposal(data);
	});
});
$(document).on('click', '.view-proposal', function (e) {
	e.preventDefault();
	get_proposal_list();
	$('#view-pro-list').modal('show');
});

function get_proposal_list() {
	let param = {
		url: url + 'get_tower_proposals_list',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then((data) => {
		// data = _.uniq(dat, 'batch_key'); 
		// console.log(data);
		let num = 1;
		let html = '<ul class="list-group">';
		if (data.length > 0) {
			_.forEach(data, function (val) {
				html += '<li class="list-group-item p3">' +
							'<div class="row">' +
								'<div class="col-7">'+num+++'. | '+val.date+' | '+val.batch_key+'</div>' +
								'<div class="col-5">' +
									'<button data-id="'+val.id+'" type="button" class="btn btn-sm btn-outline-success view_prop-lists mr-1"><i class="fas fa-eye"></i> VIEW</button>' +
									'<button data-id="'+val.id+'" type="button" class="btn btn-sm btn-outline-danger del_prop mr-1"><i class="fas fa-trash"></i> DELETE</button>' +
									'<button data-name="'+val.name+'" data-id="'+val.id+'" type="button" class="btn btn-sm btn-outline-warning edit_prop"><i class="fas fa-edit"></i> RENAME</button>' +
								'</div>' +
							'</div>' +
						'</li>';
			});
		} else {
			html = '<li class="list-group-item list-group-item-danger">No Available Proposal.</li>';
		}
		html += '</ul>';
		$('.prop-lists').html(html);
	});
}