let region_name = '';
let site_name = '';
let content_name = '';
let sort = 'asc';
let filter = '';
let filetype = '';
let geo = $('#geo').val();
let lvl = $('#lvl').val();

$(function () {
	set_region_folder();
});

function set_region_folder() {
	let div = '<div class="col-12 mg-top-10 d-flex pad-left-0 justify-content-start">';
		if (geo !== 'nat' && lvl !== 'GUEST') {
			div += '<button type="button" class="btn btn-outline-primary lead upload_repo"><i class="fas fa-file-upload"></i> UPLOAD FILES</button>';
		}
			div += '<button type="button" class="btn btn-outline-secondary lead view-format ml-2"><i class="far fa-image"></i> VIEW FORMAT</button>' +
					'<button type="button" class="btn btn-outline-success lead view-chart ml-2"><i class="fas fa-chart-pie"></i> VIEW CHART</button>' +
				'</div>';
		$('.header_div').html(div);


	let param = {
		url: url + 'get_region_count',
		dataType: 'json',
		beforeSend: $('.folder').html('<div class="col-12 mg-top-135">' +
					'<h5 class="text-center text-white">' +
					'<i class="fas fa-spinner fa-spin display-4 text-muted"></i></br></br>' +
					'<small class="text-center text-muted mg-top-10"><em>Loading directory...</em></small>' +
					'</h5>' +

			'</div>')
	};
	let html = '';
	fortress(param).then( (data) => {
		_.forEach(data.size, function (val) {
			html += '<div class="col-4 text-center">\n' +
						'<div data-name="'+val.name+'" class="container rounded site-view shadow border mg-top-10">' +
							'<div class="row">' +
								'<div class="col-12 folder-view p-3">' +
									'<div class="col-12 display-1">' +val.name+ '</div>' +
									'<div class="col-12 display-5">'+val.label+'</div>' +
								'</div>' +
								'<div class="col-12 bg-light teal border-top text-muted">' +
									'SITES: <b class="counter">'+val.total+'</b> | SIZE: <b>'+val.size +'</b> | FILES: <b class="counter">'+val.count+'</b>' +
								'</div>' +
							'</div>' +
						'</div>' +
					'</div>';
		});
		$('.folder').html(html).addClass('justify-content-center');
		$('.storage').text(data.total);
		$('.total-files').text(data.count);
		$('.counter').countUp();
	});
}

function set_site_folder(name, data) {
	let html = '';
	_.forEach(data, function (val) {
		html += '<div class="col-2 text-center">\n' +
					'<div data-name="'+val.SITENAME+'" class="container sub-view shadow border mg-top-10">' +
						'<div class="row">' +
							'<div class="col-12 folder-view p-3">' +
								'<div class="row">' +
									'<div class="col-12">' +
										'<i class="far fa-folder display-4" ></i>' +
									'</div>' +
									'<div style="margin-bottom: -15px" data-toggle="tooltip" data-placement="top" title="'+val.SITENAME+'" class="col-12 text-truncate text-sm-center">' + val.SITENAME+ '</div>' +
								'</div>' +
							'</div>' +
							'<div class="col-12 bg-light text-center teal border-top">' +
								' <div class="row">\n';
								let col = 8;
								html += '<div class="col-sm-'+col+' border-left">'+ val.PLAID +'</div>' +
								'    <div class="col-sm-4 border-left">'+ val.files +'</div>\n' +
								'  </div>' +
							'</div>' +
						'</div>' +
					'</div>' +
				'</div>';
	});
	return html;
}

function set_content_folder(data) {
	let html = '';
	_.forEach(data, function (val) {
		let icon = val.file_type.toLowerCase() === 'pdf' ? 'fa-file-pdf' : 'fa-broadcast-tower';
		let color = val.file_type.toLowerCase() === 'pdf' ? 'danger' : 'success';
		html += '<div class="col-4 pad mg-top-10">\n' +
					'<div class="card shadow">\n' +
					'  <div class="row no-gutters">\n' +
					'    <div class="col-md-3 text-center bg-'+color+'">\n' +
							'<div class="col-12 mg-top-25">' +
								'<i class="fas '+icon+' text-white h1"></i>' +
							'</div>' +
							'<div class="col-12 small text-white">(<em>.' + _.upperCase(val.file_type) + '</em>)</div>' +
					'    </div>\n' +
					'    <div class="col-md-9">\n' +
					'      <div class="p-3">\n' +
					'        <div class="text-truncate h6 mb-0" data-toggle="tooltip" data-placement="top" title="'+val.file_name+'">' + val.file_name+ '</div>' +
					'		<div class="col-12 mb-0 pad-00 text-left">' +
								'<div class="row" style="line-height: 1;"> ' +
									'<div class="col-5">' +
										'<p class="card-text"><small class="text-muted"><label class="mb-0">File Size:</label> <label class="mb-0">'+val.file_size+'</label></small></p>\n' +
									'</div>' +
									'<div class="col-7">' +
										'<p class="card-text"><small class="text-muted"><label class="mb-0">Date Uploaded:</label> <label class="mb-0">'+val.date_upload+'</label></small></p>\n' +
									'</div>' +
								'</div>' +
					'		</div>' +
					'		<div class="col-12 mb-0 mg-top-5 pad-00 text-left">' +
								'<div class="btn-group btn-group-sm rounded-0 btn-block" role="group" aria-label="Basic example">\n';
									if (geo.toUpperCase() === region_name && lvl !== 'GUEST') {
										html += '  <button data-id="'+val.id+'" type="button" class="btn btn-sm btn-outline-danger remove_file" data-toggle="tooltip" data-placement="top" title="Remove"><i class="fas fa-trash-alt"></i></button>\n' +
												'  <a href="'+url+'Repo_files/'+region_name+'/'+site_name+'/'+ val.file_name+'" type="button" class="btn btn-sm btn-outline-success" data-toggle="tooltip" data-placement="top" title="Download" download><i class="fas fa-download"></i></a>\n';
									}

									if (val.file_type === 'pdf') {
										html += '  <button data-filetype="'+val.file_type+'" data-file="'+val.file_name+'" data-toggle="tooltip" data-placement="top" title="Preview" type="button" class="btn btn-sm btn-outline-primary preview_file"><i class="fas fa-eye"></i></button>\n';
									}
								html += '</div>' +
					'		</div>' +
					'      </div>\n' +

					'    </div>\n' +
					'  </div>\n' +
					'</div>' +
				'</div>';

	});
	return html;
}

function set_sub_folder(data) {
	let div = '<div class="col-12 mg-top-10 d-flex pad-left-0 justify-content-start">';
	if (geo !== 'nat' && geo.toUpperCase() === region_name && lvl !== 'GUEST') {
		div += '<button data-name="'+site_name+'" type="button" class="btn btn-outline-primary pr-lg-1 lead upload_repo"><i class="fas fa-file-upload"></i> UPLOAD FILES</button>';
	}

	div += '<button type="button" class="btn btn-outline-secondary lead view-format ml-2"><i class="far fa-image"></i> VIEW FORMAT</button>' +
		'<button type="button" class="btn btn-outline-success lead view-chart ml-2"><i class="fas fa-chart-pie"></i> VIEW CHART</button>' +
		'</div>';
	$('.header_div').html(div);

	let html = '';
	_.forEach(data, function (val) {
		let icon = val.name === 'ENGINEERING PLANS' ? 'fas fa-broadcast-tower' : 'fas fa-briefcase';
		let color = val.name === 'ENGINEERING PLANS' ? 'danger' : 'success';
		let list = val.name === 'ENGINEERING PLANS' ? 'APPROVED DDD | TSSR' : 'AS - BUILT PLANS | ACCEPTANCE CHECKLIST';
		
		html += '<div class="col-4 mg-top-100 text-center">\n' +
					'<div data-name="'+val.name+'" class="container content-view shadow border rounded">' +
						'<div class="row">' +
							'<div class="col-12 mb-0 folder-view bg-'+color+' text-white">' +
								'<div class="col-12 p-3">' +
									'<i class="'+icon+' display-1" ></i>' +
								'</div>' +
								'<div class="col-12">' +
									'<p class="h3 teal text-white mb-0">'+val.name+'</p>' +
								'</div>' +
								'<div class="col-12" style="margin-top: -7px">' +
									'<small class=" teal text-white mb-0">'+list+'</small>' +
								'</div>' +
							'</div>' +
							'<div class="col-12 bg-light teal border-top">TOTAL FILES: <b>'+val.count+'</b></div>' +
						'</div>' +
					'</div>' +
				'</div>';
	});

	return html;
}

function empty_content() {
	return  '<div class="col-12 mg-top-135">' +
					'<h5 class="text-center text-white">' +
						'<i class="far fa-folder-open display-2 text-muted"></i></br></br>' +
						'<small class="text-center text-muted mg-top-10"><em>No files has been uploaded yet</em></small>' +
					'</h5>' +
				'</div>';
}

function set_crumbs(data) {
	let num = 1;
	let html = '<div class="nav-container">\n' +
				'  <ol class="breadcrumb mb-0">\n' +
						'<li class="breadcrumb-item"><a href="#" class="region-view">REGION</a></li>\n';
					_.forEach(data, function (val) {
						if (data.length === num) {
							html += '<li class="breadcrumb-item active" aria-current="page">'+val.name+'</li>';
						} else {
							html += '<li class="breadcrumb-item"><a href="#" data-name="'+val.name+'" class="'+val.link+'">'+val.name+'</a></li>';
						}

						num++;
					});
					html += '  </ol>\n';
			'</div>';
	$('.crumbs').html(html);
}

// ---------------------------------------------------------------------------//
$(document).on('click', '.region-view', function (e) {
	e.preventDefault();
	sort = 'asc';
	site_name = '';
	region_name	 = '';
	$('.crumbs').html('');
	$('.header_div, #folder_paginate').html('');
	$('.paginate').removeClass('d-none');
	set_region_folder();
});

$(document).on('click', '.site-view', function (e) {
	e.preventDefault();
	region_name = $(this).data('name');
	$('.paginate').removeClass('d-none');

	let datas = [
			{
				'name': region_name,
				'link' : 'site-view',
			}
	];

	let header = '<div class="col-12 pad-00 mg-top-5 border-bottom">' +
					'<div class="row">' +
						'<div class="col-4 text-left text-muted small">TOTAL SITES: <b class="sites">0</b></div>'	+
						'<div class="col-4">' +
							'<div class="d-flex justify-content-center">' +
								'<div style="margin-bottom: 5px" class="col-12 form-group has-search">\n' +
									'<span class="fa fa-search form-control-filess"></span>\n' +
									'<input type="search" class="form-control form-control-sm rounded-pill" id="search" placeholder="Search Sitename / PLAID" onkeypress="return AvoidSpace(event)" autocomplete="off" autofocus>\n' +
								'</div>' +
							'</div>' +
						'</div>'+
						'<div class="col-4">' +
							'<div class="row">' +
								'<div class="col-6 pad-right-0 d-flex justify-content-end small text-muted">SORT BY: </div>' +
								'<div class="col-6">' +
									'<select class="custom-select text-dark custom-select-sm repo-sort">\n' +
									'  <option selected value="asc"><i class="fas fa-arrow-down text-dark"></i> A - Z</option>\n' +
									'  <option value="desc"><i class="fas fa-arrow-up text-dark"></i> Z - A</option>\n' +
									'</select>' +
								'</div>' +
							'</div>' +
						'</div>'	+
					'</div>' +
				'</div>';
	$(".header_div").html(header);
	load_site_folder(0, datas, sort);
});

$(document).on('click', '.sub-view', function (e) {
	e.preventDefault();
	$('.header_div, #folder_paginate').html('');
	filter = '';
	filetype = '';
	site_name = $(this).data('name');
	$('.upload').addClass('d-none');

	let data = [
		{
			'name': region_name,
			'link' : 'site-view',
		},
		{
			'name': site_name,
			'link' : 'sub-view'
		},
	];
	set_crumbs(data);
	let param = {
		url: url + 'get_file_count',
		dataType: 'json',
		data: {site_name: site_name},
		type: 'POST',

		beforeSend: $('.folder').html('<div class="col-12 mg-top-135">' +
			'<h5 class="text-center text-white">' +
			'<i class="fas fa-spinner fa-spin display-4 text-muted"></i></br></br>' +
			'<small class="text-center text-muted mg-top-10"><em>Loading directory...</em></small>' +
			'</h5>' +
			'</div>')
	};
	let html = '';
	fortress(param).then( (data) => {
		if (data.result.length > 0) {
			html = set_sub_folder(data.result);
		} else {
			html = empty_content();
		}
		$('.folder').html(html).addClass('justify-content-center');
		$('.storage').text(data.total);
		$('.total-files').text(data.count);
	});
});

$(document).on('click', '.content-view', function (e) {
	e.preventDefault();
	$('.header_div, #folder_paginate').html('');
	content_name = $(this).data('name');
	let data = [
		{
			'name': region_name,
			'link' : 'site-view'
		},
		{
			'name': site_name,
			'link' : 'sub-view'
		},
		{
			'name': content_name,
			'link' : 'engr-view'
		},
	];
	set_crumbs(data);
	let header = '<div class="col-12 pad-00 mg-top-5 border-bottom">' +
				 	'<div class="row">' +
						'<div class="col-4 text-left text-muted small">NO. OF FILES: <b class="total_files">0</b></div>'	+
						'<div class="col-2"></div>'+
						'<div class="col-6">' +
							'<div class="col-12 mb-1">' +
								'<div class="row">' +
									'<div class="col-6">' +
										'<div class="row">' +
											'<div class="col-3 pad-right-0 d-flex justify-content-end small text-muted">FILTER BY: </div>' +
											'<div class="col-9">' +
												'<select class="custom-select text-dark custom-select-sm filter">' +
												'  <option selected value="">ALL</option>\n';
												if (content_name === 'ENGINEERING PLANS') {
													header += '<option>DDD</option>\n' +
															'<option>TSSR</option>\n';
												} else {
													header += '<option>AS-BUILT</option>\n' +
															'<option>ACCEPTANCE-CHECKLIST</option>\n';
												}
												header += '</select>' +
											'</div>' +
										'</div>' +
									'</div>' +
									'<div class="col-6">' +
										'<div class="row">' +
											'<div class="col-3 pad-right-0 d-flex justify-content-end small text-muted">FILE TYPE: </div>' +
											'<div class="col-9">' +
											'<select class="custom-select text-dark custom-select-sm filetype">' +
											'  <option selected value="">ALL</option>\n' +
											'  <option value="pdf">PDF</option>\n' +
											'  <option value="dwg">CAD</option>\n' +
											'</select>' +
											'</div>' +
										'</div>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>'	+
					'</div>' +
				'</div>';
	$(".header_div").html(header);
	load_content_folder();
});

$(document).on('change', '.filter', function (e) {
	e.preventDefault();
	filter = $(this).val();
	load_content_folder();
});

$(document).on('change', '.filetype', function (e) {
	e.preventDefault();
	filetype = $(this).val();
	load_content_folder();
});

function load_content_folder() {
	let datas = {
		'sitename' : site_name,
		'folder_name' : content_name,
		'filter': filter,
		'filetype' : filetype
	};
	let param = {
		url: url + 'get_content',
		dataType: 'json',
		data: datas,
		type: 'POST',

		beforeSend: $('.folder').html('<div class="col-12 mg-top-135">' +
					'<h5 class="text-center text-white">' +
					'<i class="fas fa-spinner fa-spin display-4 text-muted"></i></br></br>' +
					'<small class="text-center text-muted mg-top-10"><em>Loading directory...</em></small>' +
					'</h5>' +
					'</div>')
	};
	let html = '';
	fortress(param).then( (data) => {
		if (data.result.length > 0) {
			html = set_content_folder(data.result);
		} else {
			html = empty_content();
		}

		$('.folder').html(html).removeClass('justify-content-center');
		$('.total_files').text(data.result.length);
		$('.storage').text(data.total);
		$('.total-files').text(data.count);
		$('[data-toggle="tooltip"]').tooltip();
	});
}
// ---------------------------------------------------------------------------//
$(document).on('click', '.upload_repo', function (e) {
	e.preventDefault();
	let text = site_name === '' ? '' : ' for <b>'+site_name+'</b>';
	$('.sname').html(text);
	$('#upload_repo').modal('show');
});

$(document).on('click', '.view-format', function (e) {
	e.preventDefault();
	$('#view_format').modal('show');
});
// ---------------------------------------------------------------------------//

$(document).on('click', '.page_site a', function(e) {
	e.preventDefault();
	let row_num = $(this).attr('data-ci-pagination-page');
	let datas = [
		{
			'name': region_name,
			'class_name' : 'region-view',
		},
	];

	load_site_folder(row_num, datas, sort);
});

$(document).on('search', '#search', function () {
	if('' === this.value) {
		let datas = [
			{
				'name': region_name,
				'class_name' : 'region-view',
			},
		];
		load_site_folder(0, datas, sort);
	}
});

$(document).on('keyup', '#search', function(e) {
	e.preventDefault();
	let datas = [
		{
			'name': region_name,
			'class_name' : 'region-view',
		},
	];

	load_site_folder(0, datas, sort);
});

$(document).on('change', '.repo-sort', function(e) {
	e.preventDefault();
	sort = $(this).val();

	let datas = [
		{
			'name': region_name,
			'class_name' : 'region-view',
		},
	];

	load_site_folder(0, datas, sort);
});

function load_site_folder(row_num = 0, datas, sort = 'asc') {
	let arr = {
		row_page : 18,
		search   : $('#search').val(),
		geo: region_name,
		sort: sort
	};

	let param = {
		url: url + 'Repo/get_sites/'+row_num,
		dataType: 'json',
		data: arr,
		type: 'POST',

		beforeSend: $('.folder').html('<div class="col-12 mg-top-135">' +
										'<h5 class="text-center text-white">' +
										'<i class="fas fa-spinner fa-spin display-4 text-muted"></i></br></br>' +
										'<small class="text-center text-muted mg-top-10"><em>Loading directory...</em></small>' +
										'</h5>' +

			'</div>')
	};
	let html = '';
	fortress(param).then( (data) => {
		set_crumbs(datas);
		if (data.result.length > 0) {
			html = set_site_folder(name, data.result);
			$('#folder_paginate').html(data.pagination);
		} else {
			$('#folder_paginate').html('');
			html = set_empty();
		}
		$('.sites').text(data.total);
		$('.folder').html(html).removeClass('justify-content-center');
		$('[data-toggle="tooltip"]').tooltip();
		$('.counter').countUp();
	});
}
// ---------------------------------------------------------------------------//
function set_empty() {
	return '<div class="col-12 mg-top-135">' +
				'<h5 class="text-center text-white">' +
					'<i class="far fa-folder-open display-2 text-muted"></i></br></br>' +
					'<small class="text-center text-muted mg-top-10"><em>No results found</em></small>' +
				'</h5>' +
			'</div>';
}
// ---------------------------------------------------------------------------//
$(document).on('click', '.preview_file', function(e) {
	e.preventDefault();
	let file = $(this).data('file');
	let type = $(this).data('filetype');
	if (type === 'pdf') {
		let base = url + 'Repo_files/'+region_name+'/'+site_name+'/' + file;
		window.open(base, '_blank');
	} else {

	}
});

$(document).on('click', '.remove_file', function () {
	let id = $(this).data('id');
	Swal.fire({
		title: 'Are you sure?',
		text: "You want to delete this file.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	}).then((result) => {
		if (result.value) {
			let param = {
				url: url + 'delete_content',
				dataType: 'json',
				data: {id:id},
				type: 'POST',
			};
			fortress(param).then( (data) => {
				if (data) {
					SWAL('success', 'Success', 'File successfully removed.');
					load_content_folder();
				} else {
					errorSwal();
				}
			});
		}
	});
});
// ---------------------------------------------------------------------------//
$(document).on('click', '.remove-site', function () {
	let id = $(this).data('id');
	let name = $(this).data('name');
	Swal.fire({
		title: "Are you sure you want to delete this "+name+" ? ",
		text: "All the contents inside will be erased.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	}).then((result) => {
		if (result.value) {
			let param = {
				url: url + 'delete_folder',
				dataType: 'json',
				data: {name:name},
				type: 'POST'
			};
			fortress(param).then( (data) => {
				if (data) {
					SWAL('success', 'Success', 'File successfully removed.');
					let datas = {
						'sitename' : site_name,
						'folder_name' : content_name
					};
					load_content_folder();
				} else {
					errorSwal();
				}
			});
		}
	});
});
