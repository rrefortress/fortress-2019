$(document).on('click', '.view-chart', function(e) {
	e.preventDefault();

	let param = {
		url: url + 'get_repo_chart',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then( (data) => {
		set_repo_chart(data.files);
		set_sites_chart(data.sites);
		set_plan_chart(data.plans);
	});
	$('.repo-chart').modal('show');
});

function set_repo_chart(data) {
		let canvasBar = document.getElementById("region_chart");
		let ctxBar = canvasBar.getContext('2d');
		let nlz = data.nlz;
		let slz = data.slz;
		let ncr = data.ncr;
		let min = data.min;
		let vis = data.vis;
		let total = nlz + slz + ncr + min + vis;

		let total_per = getWholePercent(nlz, total) +
			getWholePercent(slz, total) +
			getWholePercent(ncr, total) +
			getWholePercent(min, total) +
			getWholePercent(vis, total);

		let datas = {
			labels: [nlz +":NLZ", slz +": SLZ", ncr + ": NCR", min + ": MIN", vis +": VIS"],
			datasets: [
				{
					label: "",
					fill: true,
					backgroundColor: ['#f0a500', '#28a745', '#0779e4', '#d8345f','#52057b'],
					data: [
						getWholePercent(nlz, total),
						getWholePercent(slz, total),
						getWholePercent(ncr, total),
						getWholePercent(min, total),
						getWholePercent(vis, total)
					]
				}
			]
		};

		let optionsBar = {
			plugins  : setPlugins(),
			title : setTitle("Regional files upload chart : " + total),
			rotation : -0.7 * Math.PI,
			legend: {
				display: false
			},
			layout: {
				padding: {
					left: 10,
					right: 10,
					top: 10,
					bottom: 10
				}
			}
		};

		build_chart(datas, optionsBar, ctxBar, total_per, 'bar', 'region');
	}

function set_sites_chart(data) {
	let canvasBar = document.getElementById("sites_chart");
	let ctxBar = canvasBar.getContext('2d');

	let nlz = data.nlz;
	let slz = data.slz;
	let ncr = data.ncr;
	let min = data.min;
	let vis = data.vis;

	let total = nlz + slz + ncr + min + vis;

	let total_per = getWholePercent(nlz, total) +
		getWholePercent(slz, total) +
		getWholePercent(ncr, total) +
		getWholePercent(min, total) +
		getWholePercent(vis, total);

	let datas = {
		labels: [nlz +":NLZ", slz +": SLZ", ncr + ": NCR", min + ": MIN", vis +": VIS"],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#f0a500', '#28a745', '#0779e4', '#d8345f','#52057b'],
				data: [
					getWholePercent(nlz, total),
					getWholePercent(slz, total),
					getWholePercent(ncr, total),
					getWholePercent(min, total),
					getWholePercent(vis, total)
				]
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText("Sites"),
		rotation : -0.7 * Math.PI,
		legend: {
			position: 'left',
		},
		layout: {
			padding: {
				left: 30,
				right: 30,
				top: 30,
				bottom: 30
			}
		}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', 'sites');
}

function set_plan_chart(data) {
	let canvasBar = document.getElementById("plan_chart");
	let ctxBar = canvasBar.getContext('2d');

	let ep = data.ep;
	let im = data.im;

	let total = ep + im;

	let total_per = getWholePercent(ep, total) +
		getWholePercent(im, total);

	let datas = {
		labels: [ep +":Engineering Plans", im +": Implementations"],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#d8345f', '#28a745'],
				pointRadius: 0,  //<- set this
				data: [
					getWholePercent(ep, total),
					getWholePercent(im, total)
				]
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText("Plans"),
		rotation : -0.7 * Math.PI,
		legend: {
			position: 'left',
		},
		layout: {
			padding: {
				left: 30,
				right: 30,
				top: 30,
				bottom: 30
			}
		}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', 'plans');
}
