Dropzone.autoDiscover = false;

let repo_upload = new Dropzone(".dropzone",{
	url: url + "Repo/repo_upload",
	timeout: 180000,
	parallelUploads: 50,
	maxFiles: 50,
	method:"POST",
	acceptedFiles:".pdf,.dwg",
	paramName:"repo_file",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:true,
	init: function() {
		this.on("success", function(file, response) {
			let data = jQuery.parseJSON(response);
			SWAL(data.type, data.title, data.msg);
		})
	}
});

repo_upload.on("sending",function(a,b,c){
	a.token = site_name;
	c.append("site_name", site_name);
});

repo_upload.on("queuecomplete", function () {
	this.removeAllFiles();
	// SWAL('success', 'Success', 'All files are successfully imported.');
	if (site_name === '') {
		set_region_folder();
	} else {
		set_sub_folder();
	}
});

repo_upload.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});

repo_upload.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});

