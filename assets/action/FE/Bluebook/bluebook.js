let main_name = '';
let cat_name = '';
let lbl = '';
let yearly = '';
let sub_name = '';
let deep_name = '';

let sort = 'asc';
let filter = '';
let filetype = '';
let region = '';
let ids = [];
let geo = $('#geo').val();
let dep = $('#dep').val();
let datas = [];

$(function () {
	set_main_folder();
});

function set_main_folder() {
	let param = {
		url: url + 'get_main',
		dataType: 'json',
		type: 'post',
		beforeSend: $('.folder').html('<div class="col-12 mg-top-135">' +
											'<h5 class="text-center text-white">' +
											'<i class="fas fa-spinner fa-spin display-4 text-muted"></i></br></br>' +
											'<small class="text-center text-muted mg-top-10"><em>Loading directory...</em></small>' +
											'</h5>' +
									'</div>')
	};

	let html = '';
	fortress(param).then( (data) => {
		_.forEach(data.size, function (val) {
			html += '<div class="col-3 text-center" style="margin-top:10%">\n' +
							'<div class="container folder-view rounded border-dark p-5 border shadow-sm cat-view" data-name="'+val.name+'">' +
								'<div class="row">' +
									'<div class="col-12">' +
										'<div class="col-12 display-4 mt-2"><i class="'+val.icon+'"></i></div>' +
										'<div class="col-12 small lead"><strong>'+val.name+'</strong></div>' +
									'</div>' +
									'<div class="col-12 small">' +
										'SIZE: <b>'+val.size +'</b> | FILES: <b class="counter">'+val.count+'</b>' +
									'</div>' +
								'</div>' +
							'</div>' +			
					'</div>';
		});
		$('.folder').html(html).addClass('justify-content-center');
		$('.storage').text(data.total);
		$('.total-files').text(data.count);
		$('.counter').countUp();
	});
}

function empty_content() {
	return  '<div class="col-12 mg-top-135">' +
					'<h5 class="text-center text-white">' +
						'<i class="far fa-folder-open display-2 text-muted"></i></br></br>' +
						'<small class="text-center text-muted mg-top-10"><em>No files has been uploaded yet</em></small>' +
					'</h5>' +
				'</div>';
}

function set_crumbs(data) {
	let num = 1;	

	let html = '<div class="nav-container dash-bread">' +
						'  <ol class="breadcrumb mb-0">\n' +
							'<li class="breadcrumb-item"><a href="#" class="main-view">Main</a></li>\n';
						_.forEach(data, function (val) {
							if (data.length === num) {
								html += '<li class="breadcrumb-item active" aria-current="page">'+val.name+'</li>';
							} else {
								html += '<li class="breadcrumb-item"><a href="#" data-lbl="'+val.lbl+'" data-name="'+val.name+'" class="'+val.link+'">'+val.name+'</a></li>';
							}

							num++;
						});
						html += '</ol>\n' + 
						'<div class="blue-btn">' +
							'<button class="btn btn-sm btn-outline-success blue-upload rounded-pill"><i class="fas fa-file-upload"></i> Upload File</button>' +
							'<button class="btn btn-sm btn-outline-primary blue-add-folder ml-2 rounded-pill"><i class="fas fa-folder-plus"></i> Add Folder</button>' +
						'</div>'
					'</div>';
				
	
		$('.crumbs').html(html);
}

// ---------------------------------------------------------------------------//
$(document).on('click', '.main-view', function (e) {
	e.preventDefault();
	sort = 'asc';
	cat_name = '';
	main_name	 = '';
	$('.crumbs').html('');
	$('.header_div, #folder_paginate').html('');
	$('.paginate').removeClass('d-none');
	set_main_folder();
});

$(document).on('click', '.cat-view', function (e) {
	e.preventDefault();
	region = '';
	yearly = '';
	file_name = $(this).data('name');
	label = $(this).data('label');

	let data = [
		{
			'name': file_name,
			'link' : 'folder-view',
		},
	];

	let param = {
		url: url + 'get_categories',
		dataType: 'json',
		data: {label: label},
		type: 'POST',
		beforeSend: $('.folder').html('<div class="col-12 mg-top-135">' +
					'<h5 class="text-center text-white">' +
					'<i class="fas fa-spinner fa-spin display-4 text-muted"></i></br></br>' +
					'<small class="text-center text-muted mg-top-10"><em>Loading directory...</em></small>' +
					'</h5>' +

			'</div>')
	};
	fortress(param).then((datas) => {
		set_crumbs(data);
		load_category(data, datas);	
	});
});

function load_category(data, datas) {
	let html = '';

	html += '<div class="container-fluid mt-2">' +
			'<table class="table small table-hover bb-cat-table">' +
				'<thead>' +
				'<tr>' +
					'<th class="bb-th" scope="col">#</th>' +
					'<th scope="col">Name</th>' +
					'<th class="bb-ff" scope="col">No. of files</th>' +
					'<th class="bb-ff" scope="col">Size</th>' +
					'<th class="bb-name" scope="col">Uploaded by</th>' +
				'</tr>' +
				'</thead>' +
				'<tbody>' +
				'<tr>' +
					'<th class="bb-th" scope="row">1</th>' +
					'<td>Mark</td>' +
					'<td class="bb-ff">0</td>' +
					'<td class="bb-ff">0</td>' +
					'<td class="bb-name">Otto</td>' +
				'</tr>' +
				'<tr>' +
					'<th class="bb-th" scope="row">2</th>' +
					'<td>Jacob</td>' +
					'<td class="bb-ff">0</td>' +
					'<td class="bb-ff">0</td>' +
					'<td class="bb-name">Thornton</td>' +
				'</tr>' +
				'<tr>' +
					'<th class="bb-th" scope="row">3</th>' +
					'<td>Larry</td>' +
					'<td class="bb-ff">0</td>' +
					'<td class="bb-ff">0</td>' +
					'<td class="bb-name">the Bird</td>' +
				'</tr>' +
				'</tbody>' +
			'</table>' +
		'</div>';	
			

	$('.folder').html(html);
	$('.header_div').html('');
	$('.ccounts', '.ccounter').countUp();
}

$(document).on('click', '.blue-upload', function (e) {
	e.preventDefault();
	$('#upload_bb').modal('show');
});

$(document).on('click', '.blue-add-folder', function (e) {
	e.preventDefault();
	$('#bb-add-folder').modal('show');
});


