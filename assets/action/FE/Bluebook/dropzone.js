$(document).on('click', '.upload_acc', function (e) {
	e.preventDefault();
	$('#upload_acc').modal('show');
});


Dropzone.autoDiscover = false;

let acceptance_upload = new Dropzone(".dropzone",{
	url: url + "Acceptance/acceptance_upload",
	timeout: 180000,
	parallelUploads: 50,
	maxFiles: 50,
	method:"POST",
	acceptedFiles:".pdf",
	paramName:"acceptance_file",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:true,
	init: function() {
		this.on("success", function(file, response) {
			// let data = jQuery.parseJSON(response);
			// SWAL(data.type, data.title, data.msg);
		})
	}
});

acceptance_upload.on("sending",function(a,b,c){
	a.token = yearly;
	b.token = region;
	c.append("yearly", yearly);
	c.append("region", region);
});

acceptance_upload.on("queuecomplete", function () {
	this.removeAllFiles();
	get_vendor_files();
	SWAL('success', 'Success', 'All files are successfully imported.');
	$('#upload_acc').modal('hide');
});

acceptance_upload.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});

acceptance_upload.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});

