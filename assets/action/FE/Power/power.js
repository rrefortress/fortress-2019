let total_mc = 0;
let total_max = 0;
let table_name = 'powerdb';

let profile = [];

$(document).on('click', '.gene-power', function (e) {
	e.preventDefault();
	let param = {
		url: url + 'get_power_reports',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then((data) => {
		let html = '<table class="table text-center table-bordered">' +
			'<thead class="bg-warning">' +
				'<tr>' +
					'<th scope="col" colspan="2">Power Guidelines</th>' +
					'<th scope="col" style="width:7%">NCR</th>' +
					'<th scope="col" style="width:7%">NLZ</th>' +
					'<th scope="col" style="width:7%">SLZ</th>' +
					'<th scope="col" style="width:7%">VIS</th>' +
					'<th scope="col" style="width:7%">MIN</th>' +
					'<th scope="col" style="width:7%">TOTAL</th>' +
				'</tr>' +
			'</thead>' +
			
			'<tbody>' +
				'<tr>' +
					'<th rowspan="5" class="bg-primary text-white" style="vertical-align: middle;">Permanent Power Solution</th>' +
				'</tr>' +

				'<tr>' +
					'<td>Tap to Commerial Power Provider</td>' +
					'<td>'+data.region.ncr.tcpp+'</td>' +
					'<td>'+data.region.nlz.tcpp+'</td>' +
					'<td>'+data.region.slz.tcpp+'</td>' +
					'<td>'+data.region.vis.tcpp+'</td>' +
					'<td>'+data.region.min.tcpp+'</td>' +
					'<td>'+data.total_tccp+'</td>' +
				'</tr>' +
				'<tr>' +
					'<td>Tap to Lessor via SPCA/Sub-meter</td>' +
					'<td>'+data.region.ncr.tlss+'</td>' +
					'<td>'+data.region.nlz.tlss+'</td>' +
					'<td>'+data.region.slz.tlss+'</td>' +
					'<td>'+data.region.vis.tlss+'</td>' +
					'<td>'+data.region.min.tlss+'</td>' +
					'<td>'+data.total_tlss+'</td>' +
				'</tr>' +
				'<tr>' +
					'<td>Tap to Genset by Design</td>' +
					'<td>'+data.region.ncr.tg+'</td>' +
					'<td>'+data.region.nlz.tg+'</td>' +
					'<td>'+data.region.slz.tg+'</td>' +
					'<td>'+data.region.vis.tg+'</td>' +
					'<td>'+data.region.min.tg+'</td>' +
					'<td>'+data.total_tg+'</td>' +
				'</tr>' +
				'<tr>' +
					'<th>Sub-total</th>' +
					'<th>'+data.region.ncr.sub_pps+'</th>' +
					'<th>'+data.region.nlz.sub_pps+'</th>' +
					'<th>'+data.region.slz.sub_pps+'</th>' +
					'<th>'+data.region.vis.sub_pps+'</th>' +
					'<th>'+data.region.min.sub_pps+'</th>' +
					'<th>'+data.total_sub_pps+'</th>' +
				'</tr>' +
				
				'<tr>' +
					'<th rowspan="5" class="bg-danger text-white" style="vertical-align: middle;">Tempo Power Solution</th>' +	
				'</tr>' +

				'<tr>' +
					'<td>Tempo tap to Commerial Power</td>' +
					'<td>'+data.region.ncr.ttcp+'</td>' +
					'<td>'+data.region.nlz.ttcp+'</td>' +
					'<td>'+data.region.slz.ttcp+'</td>' +
					'<td>'+data.region.vis.ttcp+'</td>' +
					'<td>'+data.region.min.ttcp+'</td>' +
					'<td>'+data.total_ttcp+'</td>' +
				'</tr>' +
				'<tr>' +
					'<td>Tap to Lessor/Building</td>' +
					'<td>'+data.region.ncr.tl+'</td>' +
					'<td>'+data.region.nlz.tl+'</td>' +
					'<td>'+data.region.slz.tl+'</td>' +
					'<td>'+data.region.vis.tl+'</td>' +
					'<td>'+data.region.min.tl+'</td>' +
					'<td>'+data.total_tl+'</td>' +
				'</tr>' +
				'<tr>' +
					'<td>Tap to Genset</td>' +
					'<td>'+data.region.ncr.tgp+'</td>' +
					'<td>'+data.region.nlz.tgp+'</td>' +
					'<td>'+data.region.slz.tgp+'</td>' +
					'<td>'+data.region.vis.tgp+'</td>' +
					'<td>'+data.region.min.tgp+'</td>' +
					'<td>'+data.total_tgp+'</td>' +
				'</tr>' +
			
				'<tr>' +
					'<th>Sub-total</th>' +
					'<th>'+data.region.ncr.sub_tps+'</th>' +
					'<th>'+data.region.nlz.sub_tps+'</th>' +
					'<th>'+data.region.slz.sub_tps+'</th>' +
					'<th>'+data.region.vis.sub_tps+'</th>' +
					'<th>'+data.region.min.sub_tps+'</th>' +
					'<th>'+data.total_sub_tps+'</th>' +
				'</tr>' +

			'</tbody>' +
			'<tfoot>' +
				'<tr>' +
					'<th scope="col" class="bg-success text-white" colspan="2">Total</th>' +
					'<th>'+data.region.ncr.total+'</th>' +
					'<th>'+data.region.nlz.total+'</th>' +
					'<th>'+data.region.slz.total+'</th>' +
					'<th>'+data.region.vis.total+'</th>' +
					'<th>'+data.region.min.total+'</th>' +
					'<th>'+data.total_total+'</th>' +
				'</tr>' +
			'</tfoot>' +
			'<tfoot>' +
				'<tr>' +
					'<th scope="col" class="bg-dark text-white" colspan="2">To be Updated (Progressive)</th>' +
					'<th>'+data.region.ncr.tbu+'</th>' +
					'<th>'+data.region.nlz.tbu+'</th>' +
					'<th>'+data.region.slz.tbu+'</th>' +
					'<th>'+data.region.vis.tbu+'</th>' +
					'<th>'+data.region.min.tbu+'</th>' +
					'<th>'+data.total_tbu+'</th>' +
				'</tr>' +
			'</tfoot>' +
		'</table>';

		$('.gene-tbl-report').html(html);
		$('#gene-report').modal('show');
		tinymce.init({
			selector: 'textarea#insights',
			skin: 'bootstrap',
			plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
			toolbar: 'h1 h2 bold italic strikethrough blockquote bullist undo redo | styleselect | fontselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent numlist backcolor | link image media | removeformat help',
			font_formats: "Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",
			toolbar_mode: 'floating',
			tinycomments_mode: 'embedded',
			tinycomments_author: 'Author name',
			menubar: false
		});
	});
});

$(document).on('click', '.power-site', function (e) {
	e.preventDefault();
	console.log(profile.rectifier);
	let html = '';
	html += set_profile_cards(profile.basic_info, 'BASIC INFO');
	html += set_profile_cards(profile.acu, 'ACU');
	html += set_multi_rectifier(profile.rectifier);
	html += set_profile_cards(profile.pat, 'PAT ACCEPTANCE');
	html += set_profile_cards(profile.headroom, 'HEADROOM', 60);
	html += set_profile_cards(profile.util, 'UTILIZATION');
	
	$('.c-profiles').html(html);
	$('#power-item').modal('show');
});


function set_profile_cards(data, title = '', width = 50) {
	let html = '<div class="card rounded-0 shadow-sm border">' +
					'<div class="col-12 p-0 shadow-sm bg-success display-5 text-uppercase text-white text-center">' +
						'<strong>' + title + '</strong>' +
					'</div>' +
					'<div class="card-body p-2">' +
						'<table class="table table-bordered table-sm mb-0 small table-hovered">'+
							'<tbody>';
								_.forEach(data, function(val, key) {
									html += '<tr>' +
												'<td class="text-right pr-2" scope="row" style="width:'+width+'%">'+key+'</td>' +
												'<th class="text-left pl-2">'+val+'</th>' +
											'</tr>';					
								});
						html += '</tbody>' +
							'</table>' +
					'</div>' +
				'</div>';
	return html;
}

function multi_rectifier(data, title = '') {
	let html = '<div class="col-12 p-0 shadow-sm bg-success display-5 text-uppercase text-white text-center">' +
				'<strong>' + title + '</strong>' +
			'</div>' +
			'<div class="card-body p-2">' +
				'<table class="table table-bordered table-sm mb-0 small table-hovered">'+
					'<tbody>';
						_.forEach(data, function(val, key) {
							html += '<tr>' +
										'<td class="text-right pr-2" scope="row" style="width:50%">'+key+'</td>' +
										'<th class="text-left pl-2">'+val+'</th>' +
									'</tr>';					
						});
				html += '</tbody>' +
					'</table>' +
			'</div>';

	return html;		
}

function set_multi_rectifier(data) {
	let html = '<div class="card rounded-0 shadow-sm border">';
					html += multi_rectifier(data.rec_1, 'RECTIFIER 1');	
					html += multi_rectifier(data.rec_2, 'RECTIFIER 2');	
					html += multi_rectifier(data.rec_3, 'RECTIFIER 3');	
				html += '</div>';
	return html;
}

$(document).on('click', '.add_new_power', function (e) {
	e.preventDefault();
	get_acu_model();
	get_drop_wires();
	get_rec_bat();
	$('#ACU_TYPE').val('Window');
	$('#ACU_APPROX_LOAD').val(5.11);
	$('#add_new-power').modal('show');
});

$(document).on('click', '.add_new_power', function (e) {
	e.preventDefault();
	get_acu_model();
	get_drop_wires();
	get_rec_bat();
	$('#ACU_TYPE').val('Window');
	$('#ACU_APPROX_LOAD').val(5.11);
	$('#add_new-power').modal('show');
});

$(document).on('click', '.save_pro', function (e) {
	e.preventDefault();
	$('#save-pro').modal('show');
});

$(document).on('submit', '#save_proposal', function (e) {
	e.preventDefault();
	let param = {
		url: url + 'proposal_save_mdb',
		dataType: 'json',
		type: 'POST',
		data: $(this).serialize()
	};
	fortress(param).then((data) => {
		$('#save-pro').modal('show');
	});
});

function get_drop_wires() {
	let param = {
		url: url + 'get_drop_wires',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then((data) => {
		let html = '';
		_.forEach(data, function (val) {
			html += '<option>'+val.circuit_breaker+' ' +val.cable_mm+ 'mm</option>'
		});
		$('#DROP_WIRES').html(html).select2({ width: '100%' });
	});
}

function get_acu_model() {
	let param = {
		url: url + 'get_acu_model',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then((data) => {
		let html = '';
		_.forEach(data, function (val) {
			html += '<option data-type='+val.type+' data-amp='+val.amp+'>'+val.model+'-' +val.power+'</option>'
		});
		$('#ACU_CAPACITY_HP').html(html).select2({ width: '100%' });
	});
}

$(document).on('change', '#ACU_CAPACITY_HP', function (e) {
	e.preventDefault();
	let type = $(this).find(':selected').attr('data-type');
	let amp = $(this).find(':selected').attr('data-amp');
	$('#ACU_TYPE').val(type);
	$('#ACU_APPROX_LOAD').val(amp);
});

function get_rec_bat(num = 1) {
	let param = {
		url: url + 'get_rec_bat',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then((data) => {
		$('#OTHER_AC_LOAD_AAC').val(4);
		set_rec_select(data.rec, num);
		set_bat_select(data.bat, num);
	});
}

$(document).on('change', '#CABIN_TYPE', function (e) {
	e.preventDefault();
	let none = $(this).val() === 'Outdoor' ? 0 : '';
	let dis = $(this).val() === 'Outdoor';
	// $('#ACU_TYPE').attr("disabled", dis);
	// $('#OTHER_AC_LOAD_AAC').prop('disabled', dis);
	$('#ACU_QUANTITY, #ACU_CAPACITY_HP, #ACU_APPROX_LOAD').val(none);
});

$(document).on('click', '.bulk-upload_power', function (e) {
	e.preventDefault();
	$('#bulk-upload-power').modal('show');
});

$(document).on("click", ".export_power", function (e) {
	e.preventDefault();
	$(this).prop('disabled', true);

	let param = {
		url: url + 'export_power',
		dataType: 'json',
		beforeSend: $(this).html('<em>Exporting <i class="fas fa-spinner fa-spin text-muted"></i></em>')
	};

	fortress(param).then((data) => {
		let today = moment().format("DD/MM/YYYY");
		if (data.result.length > 0) {
			JSONToCSVConvertor(data.result, "POWER_" + data.geoarea + "(" + today + ")", true);
		} else {
			SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
		}
		$(this).html('<i class="fas fa-download"></i> EXPORT').prop('disabled', false);
	});

});

$(document).on('submit', '#add_powerdb', function (e) {
	e.preventDefault();
	let param = {
		url: url + 'add_powerdb',
		dataType: 'json',
		type: 'POST',
		data: $(this).serialize(),
	};
	fortress(param).then((data) => {
		SWAL(data.type, data.title, data.message);
		if (data.type === 'success') {
			sitesdatatable.ajax.reload();
			$('#add_new-power').modal('hide');
			$('#add_powerdb').find('input[type=text], input[type=number]').val('');
			$('.rec-form').removeClass('d-none');
			$('.btn-1').removeClass('d-none');
			$('.rs-2, .rs-3').remove();
			$('#SITE_TYPE').val('Greenfield');
			$('#SITE_CLASS').val('A1');
			$('#GRID_STATUS').val('G1');
			$('#CABIN_TYPE').val('Indoor');
			$('#POWER_CONNECTION').val('Genset Prime');
			$('#POWER_PHASE').val('Single Phase');
			$('#CABIN_QUANTITY').val('0');
			$('#ACU_APPROX_LOAD').val('5.11');
			$('#RECTIFIER_1, #RECTIFIER_2, #RECTIFIER_3').val('APM30H');
			$('#RS1_BATTERY_BRAND, #RS2_BATTERY_BRAND, #RS3_BATTERY_BRAND').val('ACME');
			$('#ACU_TYPE').val('Window').attr("disabled", false);
			$('#ACU_QUANTITY, #ACU_CAPACITY_HP, #OTHER_AC_LOAD_AAC').val('').prop('disabled', false);
			$('#OTHER_AC_LOAD_AAC').val(4);
			total_mc = 0;
			total_max = 0;
		}
	});
});

function set_response(data) {
	$('.insert').text(data.new_insert);
	$('.duplicate').text(data.duplicate_count);
	let html = '';
	let num = 1;

	if (data.duplicate_sites.length > 0) {
		_.forEach(data.duplicate_sites, function (val) {
			html += '<tr class="table-danger">\n' +
				'      <th scope="row">' + num++ + '.</th>\n' +
				'      <td>' + val.PLAID + '</td>\n' +
				'      <td>' + val.SITENAME + '</td>\n' +
				'    </tr>';
		});
	} else {
		html = '<tr class="col-12 mg-top-135 table-success">' +
			'<td colspan="3">' +
			'<h5 class="text-center text-muted">' +
			'<i class="fas fa-check display-2"></i></br></br>' +
			'<small class="text-center text-muted mg-top-10"><em>No duplicates detected.</em></small>' +
			'</h5>' +
			'</td>' +
			'</tr>';
	}
	$('.res_lists').html(html);
	$('#bulk-response-power').modal('show');
}

function get_power_data() {
	///////////////////////////////////////DATATABLE
	let param = {
		url: url + 'generatedatatable',
		dataType: 'json',
		type: 'POST',
		data: {displayto: table_name, request: "fields"},
	};
	
	$.each(inputs[0], function (index, value) {
		$.each(value, function (index, value2) {
			if(value2.name != "DROP_WIRES" && value2.name != "ACU_CAPACITY_HP" )
			{
				coldetails.push(value2);
			}
		});
	});

	get_all_db_dropdowns();

	fortress(param).then((data) => {
		var temp = "";
		$.each(data, function (index, value) {
			$.each(coldetails, function (indexin, valuecol) {
				if (valuecol.name == value) {
					tblcolumns.push({"title": valuecol.title, "name": value});
					temp = valuecol.name;
					return;
				}
			});
			if (temp != value) {
				tblcolumns.push({"title": value, "name": value});
			}

			// tblcolumns.push({"title":value});
		});

		let reg_hdr=1;
		$.each(tblcolumns, function (ind, val) {
			if(val.name === "REGION" || val.name === "GEOAREA" )
			{
				reg_hdr = ind;
			}
		});
		let hiddencols = $('#geo').val() !== 'nat' ? [0,reg_hdr] : [0];
		sitesdatatable = power_tbl.DataTable({
			"dom": 'lrt<"row"<"col"i><"#selectedtr_info.col"><"col"p>>',
			"select": true,
			"processing": true,
			"serverSide": true,
			"pageLength": 10,
			"lengthMenu": [[10, 30, 60, 100, -1], [10, 30, 60, 100, "All"]],
			"columns": tblcolumns,
			"orderCellsTop": true,
			scrollY: "68vh",
	        scrollX:        true,
	        scrollCollapse: true,
	        paging:         true,
	        fixedColumns: {
			    leftColumns: 4
			},
			"ajax": {
				url: url + "generatedatatable",
				data: {displayto: table_name, request: "data"},
				type: "POST",
			}
			,
			fnCreatedRow: function (nRow, aData) {
				$(nRow).attr('id', aData[0]);
				$(nRow).attr('plaid', aData[3]);
				$(nRow).attr('region', aData[1]);
			},
			"columnDefs": [
				{
					"visible": false,
					"targets": hiddencols,
				},
				{
					targets: '_all',
					render: function (data, type, row) {
						var color = 'black';
						let value = data === null || data === undefined? 'to be updated' : data.toLowerCase();
						if (value === "to be updated") {
							color = 'red';
							return '<span style="color:' + color + '">' + data + '</span>';
						} else if (value === "none") {
							color = 'gray';
							return '<span style="color:' + color + '">' + data + '</span>';
						}else{
							return data;
						}
					}
				},
			],
			initComplete: async function () {
				let power_length = $('#power_tbl_length');
				$(power_length).addClass('row mt-2');
				$('#power_tbl_length label').addClass('col-2 m-0 p-0 ml-3');
				$(power_length).append(`
	                <div class="col-3 form-group has-search m-0 p-0">
	                    <span class="fa fa-search form-control-feedback form-control-xs mt-2"></span>
	                    <input onsearch="OnSearch(this)" type="search" class="form-control form-control-sm rounded-pill" id="srchinput" placeholder="Search..." autofocus data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
	            	</div>`);
				$("#selectedtr_info").addClass('dataTables_info');
				if ($('#geo').val() !== 'nat' && $('#lvl').val() !== 'GUEST') {
					$(power_length).append(`
								<div class="col-6 ml-5">
									<div class="">
										<h5 class="font-weight-normal m-0"></h5>
									</div>
									<div class="row d-flex justify-content-end">
										<div class="operationtoggle btn-group btn-group-toggle" data-toggle="buttons">
											<label id="edit" class="btn btn-outline-warning my-1 px-1 p-0">
												<input type="radio" name="options"  autocomplete="off"><i class="fas fa-edit"></i> EDIT
											</label>
											<label id="delete" class="btn btn-outline-danger my-1 px-1 p-0">
												<input type="radio" name="options" autocomplete="off"><i class="fas fa-trash"></i> DELETE
											</label>
										</div>
										<button id="select" type="button" class="btn btn-sm btn-link"><i class="fas fa-check-square"></i> SELECT / UNSELECT All</button>
										<button id="clear" type="button" class="btn btn-sm btn-link"><i class="fas fa-eraser"></i> CLEAR</button>
										<button id="proceed" type="button" class="btn btn-sm btn-link"><i class="fas fa-check-circle"></i> PROCEED</button>
									</div>
								</div>`);
					$("#select").attr('disabled', true);
					$("#proceed").attr('disabled', true);
					$("#clear").attr('disabled', true);

				}
				$(".power_tbl th").addClass('global_color text-white text-uppercase text-center');
			},
		});
	});

}

$( document ).ajaxComplete(function() {
 reHighlight();
});
$(document).on('keyup', '.colsearch',function () {
	drawAllSearch(this,"colsearch");
} );
function OnColSearch(input) {
	drawAllSearch(input,"colsearch");
}
$(document).on("keyup", "#srchinput", function () {
	drawAllSearch(this,"global");
});
function OnSearch(input) {
	drawAllSearch(input,"global");
}
function drawAllSearch(input,searchArea)
{
	if(searchArea=="global")
	{
		sitesdatatable.search(document.getElementById("srchinput").value).draw();
	}
	else
	{
		let colindex = $(input).attr('id')-1;
		for(let i=1; i<=tblcolumns.length; i++){
		    if ( sitesdatatable.column(colindex).search() !== input.value ) {
		        sitesdatatable.column(colindex).search(input.value).draw();
		    }
		}
	}
	
}
$(document).on('click', '#proceed', function () {
	var updatedata = [];
	switch (activeoperation) {
		case 'edit':
			for (var i = 0; i < dataset.length; i++) {
				updatedata.push([dataset[i][1], dataset[i][2], dataset[i][4]]);
			}
			Swal.fire({
				title: 'Are you sure?',
				text: "Changes cannot be undone.",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes'
			}).then((result) => {
				if (result.value) {
					let param = {
						url: url + 'edit',
						dataType: 'json',
						type: 'POST',
						data: {"data": updatedata, "db": table_name},
					};
					fortress(param).then((data) => {
						SWAL('success', 'Success!', 'Data updated.');
						sitesdatatable.ajax.reload();
						get_completion();
						resetclear();
					});
				}
			});

			break;
		case 'delete':
			var selections = [];
			var delprint = "";
			if (selectallflag) {
				selections.push("all");
				delprint = "all row(s)?";
			} else {
				selections = selectedtr;
				delprint = selectedtr.length+ " row(s) selected?";
			}

			Swal.fire({
				title: 'Delete ' + delprint,
				text: "Changes cannot be undone.",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes'
			}).then((result) => {
				if (result.value) {
					let param = {
						url: url + 'delete',
						dataType: 'json',
						type: 'POST',
						data: {"selections": selections, "db": table_name, "fe": true},
					};
					fortress(param).then(() => {
						SWAL('success', 'Success!', 'Data removed.');
						sitesdatatable.ajax.reload();
						get_completion();
						resetclear();
					});
				}
			});
			break;
	}
});

function resetclear() {
	hidecolumns(0);
	$(".operationtoggle label").removeClass('active');
	activeoperation = "";
	selectedtr = [];
	selectallflag = 0;
	dataset = [];
	reHighlight();
	$("#select").attr('disabled', true);
	$("#proceed").attr('disabled', true);
	$("#clear").attr('disabled', true);
	$('.power-site, .plan-tagging').attr('disabled', true);
	sitesdatatable.ajax.reload();
}

function hidecolumns(bool) {
	// let startcol = 46;
	let startcol = 51;
	let endcol = 71;
	if (bool == 1) {
		for (var i = startcol; i <= endcol; i++) {
			sitesdatatable.column(i).visible(false, false);
		}
	} else {
		for (var i = startcol; i <= endcol; i++) {
			sitesdatatable.column(i).visible(true, false);
		}
	}
}

$(document).on('click', '.operationtoggle label', function () {
	if (nowblinking === undefined) {
		nowblinking = this;
	}
	if (clickedoperation !== undefined && clickedoperation !== ($(this)).attr("id")) {
		nowblinking = this;
		resetclear();
	}
	blink(this);
	clickedoperation = ($(this)).attr("id");
	activeoperation = clickedoperation;
	$("#clear").attr('disabled', false);
	switch (activeoperation) {
		case "delete":
			$("#select").attr('disabled', false);
			break;
		case "edit":
			hidecolumns(1);
			break;
	}
});

function blink(tag) {
	$(tag).fadeOut(500, function () {
		$(tag).fadeIn(500, function () {
			// console.log(nowblinking+' '+tag+' '+activeoperation);
			if (nowblinking != tag || activeoperation == '') {
				// console.log(2);
				return;
			} else blink(tag);
		});
	});
}

function reHighlight() {
	let pTable = document.getElementById('power_tbl');
	for (let i = 1, row; row = pTable.rows[i]; i++) {
		let rowid = $(pTable.rows[i].cells[0]).closest('tr').attr('plaid');
		selectedtr.includes(rowid) ? $(pTable.rows[i]).addClass('selected') : $(pTable.rows[i]).removeClass('selected');
	}
	if(selectedtr.length>0){
		$("#selectedtr_info").html(selectedtr.length+" row(s) selected.");
	}else{
		$("#selectedtr_info").html("");
	}
}

$(document).on('click', '#clear', function () {
	resetclear();
});

$(document).on('click', '#select', function () {
	if (selectallflag) {
		selectallflag = 0;
		selectedtr = [];
	} else {
		{
			selectallflag = 1;
			let pTable = document.getElementById('power_tbl');
			for (let i = 1, row; row = pTable.rows[i]; i++) {
				let rowid = $(pTable.rows[i].cells[0]).closest('tr').attr('plaid');
				selectedtr.push(rowid);
			}
		}
	}
	if (selectedtr.length !== 0) $("#proceed").attr('disabled', false);
	else $("#proceed").attr('disabled', true);
	reHighlight();
});

function getedited() {
	if (activeoperation === 'edit') {
		if (textinitial != null && textinitial !== textfinal) {
			var i = 0;
			for (i = 0; i < dataset.length; i++) {
				if (dataset[i][0] === tablecat && dataset[i][1] === rowid && dataset[i][2] === fieldid && dataset[i][4] !== textfinal) {
					dataset[i][4] = textfinal;
					return;
				}
			}
			dataset.push([tablecat, rowid, fieldid, textinitial, textfinal]);
		}
	}
}

$(document).on('blur', '#power_tbl td', function (e) {
	e.preventDefault();
	let istd = (this.innerHTML.indexOf("dtableselect"));
	if(istd==-1)
	{
		textfinal = $(this).text();
		getedited();
		textinitial = null;
		textfinal = null;
		$(this).prop('contenteditable', false);
	}
});


$(document).on("change",'.dtableselect', function(e) {
	e.preventDefault();
	if($(this).val()!=="disabled"){
		// rowid = $(this).closest("tr").attr("id");
		textfinal = $(this).find(":selected").val();
		console.log(textfinal);
		getedited();
		textinitial = null;
		textfinal = null;
		$("#proceed").attr('disabled', false);
	}

});

$(document).on("click",'#power_tbl td', function(e) {
	e.preventDefault();
	
		$('.power-site, .plan-tagging').removeAttr("disabled");
		
		$(this).toggleClass('selected');

		let istd = (this.innerHTML.indexOf("dtableselect"));
		if(activeoperation==="edit") {
			let $sel;
			rowid = $(this).closest("tr").attr("id");
			tdcolindex = sitesdatatable.cell(this).index().column;
			$.each(tblcolumns, function(index,value){
				if(index == tdcolindex){	
					fieldid = value.name;
				}
			});
			tablecat = $(this).closest("table").attr("id");
			var textkeydown = sitesdatatable.cell(this).render('display');
			if (textinitial == null && textinitial !== textkeydown) {
				textinitial = textkeydown;
			}
			let menutype='';
			$.each(coldetails, function(index,value){
				console.log(value.name + '---' + fieldid);
				if(value.name == fieldid){
					if(value.type=='select' && istd == -1){
						let temp = 0;
						menutype='sel';
						$sel = $('<select class="'+fieldid+' dtableselect cellinputsize custom-select form-control custom-select-sm" name ='+value.name+'></select>',{});
						$.each(value.drowpdown, function (k1, v1) {
	                         var $opt = $('<option></option>', {
	                             "text": v1,
	                             "value": v1
	                         });
	                         if(v1.replace(/\s/g, '').toLowerCase()==textkeydown.replace(/\s/g, '').toLowerCase()) {
	                             $opt.attr("selected", "selected");
	                             temp = 1;
	                         }
	                         $sel.append($opt);
	                     });
						if(!temp) {
						let $opt = $('<option disabled selected value="disabled">'+textkeydown+'</option>', {});
	                    	$sel.append($opt);
	                    }
					}
				}
			});

			if(menutype=='sel'){
				sitesdatatable.cell(this).data($sel.prop("outerHTML"));
				$('.dtableselect').select2({ width: '100%' });
			}
			else if(menutype=='sel2'){

			}
			else{
				$(this).prop('contenteditable', true);
			}
		}	
});

function get_all_db_dropdowns()
{
	let param = {
		url: url + 'get_all_db_dropdowns',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then((data) => {
		$.each(data, function (index, value) {
			let dropdown = [];

				switch(index)
				{
					case "acu":
						$.each(value, function (index2, val) {
							dropdown.push(val.model+'-'+val.power);
						});
						coldetails.push({"title": "ACU Model & Capacity(HP)", "name": "ACU_CAPACITY_HP", "type": "select", "drowpdown": dropdown});
						break;
					case "dropwire":
						$.each(value, function (index2, val) {
							dropdown.push(val.circuit_breaker+' ' +val.cable_mm+ 'mm');
						});
						coldetails.push({"title": "Dropwire", "name": "DROP_WIRES", "type": "select", "drowpdown": dropdown});
						break;
					
					case "rect":
						$.each(value, function (index2, val) {
							dropdown.push(val.name);
						});
						coldetails.push({"title": "Rectifier 1", "name": "RECTIFIER_1", "type": "select", "drowpdown": dropdown});
						coldetails.push({"title": "Rectifier 2", "name": "RECTIFIER_2", "type": "select", "drowpdown": dropdown});
						coldetails.push({"title": "Rectifier 3", "name": "RECTIFIER_3", "type": "select", "drowpdown": dropdown});
						break;
					case "batt":
						$.each(value, function (index2, val) {
							dropdown.push(val.name);
						});
						coldetails.push({"title": "RS 1 Battery Brand", "name": "RS1_BATTERY_BRAND", "type": "select", "drowpdown": dropdown});
						coldetails.push({"title": "RS 2 Battery Brand", "name": "RS2_BATTERY_BRAND", "type": "select", "drowpdown": dropdown});
						coldetails.push({"title": "RS 3 Battery Brand", "name": "RS3_BATTERY_BRAND", "type": "select", "drowpdown": dropdown});
						break;
					

				}
			
			// console.log({"title": index, "name": index, "type": "select", "drowpdown": drowpdown});
			
		});
		
	});
	// console.log(coldetails);
}
$(document).on("keyup", '#power_tbl td', function () {
	$("#proceed").attr('disabled', false);
});  

$(document).on("click", '#power_tbl tr', function () {
	let ret = [];
	const id = $(this).closest("tr").attr("id");
	const plaid = $(this).closest("tr").attr("plaid");
	const reg = $(this).closest("tr").attr("region");

	get_site_profile(id, plaid, reg);
	
	if(plaid!=="" || plaid!==undefined || plaid!==0){
		if (activeoperation === "delete") {
			selectedtr.push(plaid);
			for (var i = 0; i < selectedtr.length; i += 1) {
				if (ret.indexOf(selectedtr[i]) !== -1) {
					ret.splice(ret.indexOf(selectedtr[i]), 1);
				} else {
					ret.push(selectedtr[i]);
				}
			}
			selectedtr = ret;
			if (selectedtr.length !== 0) $("#proceed").attr('disabled', false);
			else $("#proceed").attr('disabled', true);
		}else{
			selectedtr = [];
			selectedtr.push(plaid);
		}
		reHighlight();
	}
	
});

function get_site_profile(id = 0, plaid = '', reg = '') {
	let param = {
		url: url + 'get_power_profile',
		dataType: 'json',
		data: {id: id, plaid: plaid, region: reg},
		type: 'POST'
	};
	fortress(param).then( (data) => {
		profile = data;	
		set_tbl_detail_rre(data.tech, data.tech.prof);
	});
}

$(document).on("click", '.add_new_equip', function () {
	clearform('.add_equip');
	$('#add_new_equip').modal('show');
});

$(document).on("click", '.add_new_rec', function () {
	clearform('.add_rec');
	$('#add_new_rec').modal('show');
});

$(document).on("click", '.add_new_bat', function () {
	clearform('.add_bat');
	$('#type').val('VRLA');
	$('#add_new_bat').modal('show');
});

$(document).on("submit", '.add_rec', function (e) {
	e.preventDefault();
	let param = {
		url: url + 'add_rec',
		dataType: 'json',
		type: 'POST',
		data: $(this).serialize(),
	};

	fortress(param).then((data) => {
		$('#add_new_rec').modal('hide');
		clearform('.add_rec');
		SWAL(data.type, data.title, data.msg);
		get_rec_data();
	});
});

$(document).on("submit", '.add_bat', function (e) {
	e.preventDefault();
	let param = {
		url: url + 'add_bat',
		dataType: 'json',
		type: 'POST',
		data: $(this).serialize(),
	};

	fortress(param).then((data) => {
		$('#add_new_bat').modal('hide');
		clearform('.add_bat');
		$('#type').val('VRLA');
		SWAL(data.type, data.title, data.msg);
		get_bat_data();
	});
});

$(document).on("submit", '.add_equip', function (e) {
	e.preventDefault();
	let param = {
		url: url + 'add_equip',
		dataType: 'json',
		type: 'POST',
		data: $(this).serialize(),
	};

	fortress(param).then((data) => {
		$('#add_new_equip').modal('hide');
		clearform('.add_equip');
		SWAL(data.type, data.title, data.msg);
		get_equip_data();
	});
});

$(document).on('click', '.btn-equip-del', function (e) {
	e.preventDefault();
	let id = $(this).data('id');
	Swal.fire({
		title: 'Are you sure?',
		text: "You want to delete this data",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	}).then((result) => {
		if (result.value) {
			let param = {
				url: url + 'del_equip',
				dataType: 'json',
				type: 'POST',
				data: {id: id},
			};

			fortress(param).then((data) => {
				SWAL(data.type, data.title, data.msg);
				get_equip_data();
			});
		}
	});
});

$(document).on('click', '.btn-rec-del', function (e) {
	e.preventDefault();
	let id = $(this).data('id');
	Swal.fire({
		title: 'Are you sure?',
		text: "You want to delete this data",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	}).then((result) => {
		if (result.value) {
			let param = {
				url: url + 'del_rec',
				dataType: 'json',
				type: 'POST',
				data: {id: id},
			};

			fortress(param).then((data) => {
				SWAL(data.type, data.title, data.msg);
				get_rec_data();
			});
		}
	});
});

$(document).on("click", '.btn-rec-edit', function (e) {
	e.preventDefault();
	let id = $(this).data('id');
	let name = $(this).closest("tr").find('td:eq(0)').text();
	let mc = $(this).closest("tr").find('td:eq(1)').text();
	let no_of_modules = $(this).closest("tr").find('td:eq(2)').text();
	let empty_modules = $(this).closest("tr").find('td:eq(3)').text();

	$('#rec_id').val(id);
	$('#name_rec').val(name);
	$('#module_capacity').val(mc);
	$('#no_of_modules').val(no_of_modules);
	$('#empty_modules').val(empty_modules);
	$('#add_new_rec').modal('show');
});

$(document).on('click', '.btn-bat-del', function (e) {
	e.preventDefault();
	let id = $(this).data('id');
	Swal.fire({
		title: 'Are you sure?',
		text: "You want to delete this data",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	}).then((result) => {
		if (result.value) {
			let param = {
				url: url + 'del_bat',
				dataType: 'json',
				type: 'POST',
				data: {id: id},
			};

			fortress(param).then((data) => {
				SWAL(data.type, data.title, data.msg);
				get_bat_data();
			});
		}
	});
});

$(document).on("click", '.btn-bat-edit', function (e) {
	e.preventDefault();
	let id = $(this).data('id');
	let name = $(this).closest("tr").find('td:eq(0)').text();
	// let capacity = $(this).closest("tr").find('td:eq(1)').text();

	$('#bat_id').val(id);
	$('#name_bat').val(name);
	// $('#capacity').val(capacity);
	$('#add_new_bat').modal('show');
});

$(document).on("click", '.btn-equip-edit', function (e) {
	e.preventDefault();
	let id = $(this).data('id');
	let name = $(this).closest("tr").find('td:eq(0)').text();
	let wattage = $(this).closest("tr").find('td:eq(1)').text();

	$('#equip_id').val(id);
	$('#name_equip').val(name);
	$('#wattage').val(wattage);
	$('#add_new_equip').modal('show');
});

function get_rec_data() {
	let param = {
		url: url + 'get_rec',
		dataType: 'json',
		type: 'POST',
		beforeSend: $('.rec_list').html('<tr class="col-12 ">' +
			'<td colspan="6">' +
			'<h5 class="text-center text-white mg-top-10">' +
			'<i class="fas fa-spinner fa-spin display-4 text-muted"></i></br></br>' +
			'<small class="text-center text-muted mg-top-10"><em>Loading data...</em></small>' +
			'</h5>' +
			'</td>'+
			'</tr>')
	};
	fortress(param).then((data) => {
		let html = '<table class="dt-responsive nowrap mg-top-10 tbl-rec table small table-sm table-hover table-striped table-bordered">\n' +
						'<thead class="global_color text-white text-uppercase">\n' +
							'<tr>\n' +
								// '<th scope="col" class="text-center" style="width: 5%">#</th>\n' +
								'<th scope="col">Name</th>\n' +
								'<th scope="col" class="text-center">Module Capacity</th>\n' +
								'<th scope="col" class="text-center">No. Maximum of Modules</th>\n' +
								'<th scope="col" class="text-center">Action</th>\n' +
							'</tr>\n' +
						'</thead>\n' +
						'<tbody class="rec_list">';
								let num = 1;
								if (data.length > 0) {
									_.forEach(data, function (val) {
										html += '<tr>\n' +
											// '      <th class="text-center" scope="row">'+num+++'.</th>\n' +
											'      <td>'+val.name+'</td>\n' +
											'      <td class="text-right">'+val.module_capacity+'</td>\n' +
											'      <td class="text-right">'+val.empty_modules+'</td>\n' +
											'      <td class="text-center">' +
											'			<button data-id='+val.id+' type="button" class="btn btn-sm btn-danger btn-rec-del"><i class="fas fa-trash"></i> DELETE</button>' +
											'			<button data-id='+val.id+' type="button" class="btn btn-sm btn-info btn-rec-edit"><i class="fas fa-edit"></i> EDIT</button>' +
											'	   </td>\n' +
											'    </tr>';
									});
								} else {
									html = '<tr class="col-12 ">' +
												'<td colspan="6">' +
												'<h5 class="text-center text-white mg-top-10">' +
												'<i class="fas fa-spinner fa-spin display-4 text-muted"></i></br></br>' +
												'<small class="text-center text-muted mg-top-10"><em>Loading data...</em></small>' +
												'</h5>' +
												'</td>'+
											'</tr>';
								}
						html += '</tbody>\n' +
					'</table>';

		$('.tbl_rec').html(html);
		if (data.length > 0) {$('.tbl-rec').dataTable();}
	});
}

function get_bat_data() {
	let param = {
		url: url + 'get_bat',
		dataType: 'json',
		type: 'POST',
		data: {search: ""},
		beforeSend: $('.bat_list').html('<tr class="col-12 ">' +
			'<td colspan="6">' +
			'<h5 class="text-center text-white mg-top-10">' +
			'<i class="fas fa-spinner fa-spin display-4 text-muted"></i></br></br>' +
			'<small class="text-center text-muted mg-top-10"><em>Loading data...</em></small>' +
			'</h5>' +
			'</td>'+
			'</tr>')
	};
	fortress(param).then((data) => {
		let html = '<table class="dt-responsive mg-top-10 tbl-rec small table mb-0 table-sm nowrap table-hover table-striped table-bordered">\n' +
			'<thead class="global_color text-white text-uppercase">\n' +
			'<tr>\n' +
			// '<th scope="col" class="text-center">#</th>\n' +
			'<th scope="col" class="text-center">Name</th>\n' +
			// '<th scope="col">Battery Capacity</th>\n' +
			'<th scope="col" class="text-center">Action</th>\n' +
			'</tr>\n' +
			'</thead>\n' +
			'<tbody class="bat_list">';
					let num = 1;
					if (data.length > 0) {
						_.forEach(data, function (val) {
							html += '<tr>\n' +
								// '      <th class="text-center" scope="row">'+num+++'.</th>\n' +
								'      <td class="text-center">'+val.name+'</td>\n' +
								// '      <td>'+val.capacity+'</td>\n' +
								'      <td class="text-center">' +
								'		<button data-id='+val.id+' type="button" class="btn btn-sm btn-danger btn-bat-del"><i class="fas fa-trash"></i> DELETE</button>' +
								'		<button data-id='+val.id+' type="button" class="btn btn-sm btn-info btn-bat-edit"><i class="fas fa-edit"></i> EDIT</button>' +
								'	   </td>\n' +
								'    </tr>';
						});
					} else {
						html = '<tr class="col-12 ">' +
							'<td colspan="6">' +
							'<h5 class="text-center text-white mg-top-10">' +
							'<i class="fas fa-spinner fa-spin display-4 text-muted"></i></br></br>' +
							'<small class="text-center text-muted mg-top-10"><em>Empty Data</em></small>' +
							'</h5>' +
							'</td>'+
							'</tr>';
					}
		html += '</tbody>\n' +
			'</table>';

		$('.tbl_bat').html(html);
		if (data.length > 0) {$('.tbl-bat').dataTable({
			"columnDefs": [
				{ "width": "10%", "targets": 0 }
			]
		})}

	});
}

function get_equip_data() {
	let param = {
		url: url + 'get_equip',
		dataType: 'json',
		type: 'POST',
		beforeSend: $('.rec_list').html('<tr class="col-12 ">' +
			'<td colspan="6">' +
			'<h5 class="text-center text-white mg-top-10">' +
			'<i class="fas fa-spinner fa-spin display-4 text-muted"></i></br></br>' +
			'<small class="text-center text-muted mg-top-10"><em>Loading data...</em></small>' +
			'</h5>' +
			'</td>'+
			'</tr>')
	};
	fortress(param).then((data) => {
		let html = '<table class="dt-responsive nowrap mg-top-10 tbl-equip table small table-sm table-hover table-striped table-bordered">\n' +
			'<thead class="global_color text-white text-uppercase">\n' +
			'<tr>\n' +
			// '<th scope="col" class="text-center" style="width: 5%">#</th>\n' +
			'<th scope="col" class="text-center">Item</th>\n' +
			'<th scope="col" class="text-center">Wattage</th>\n' +
			'<th scope="col" class="text-center">Action</th>\n' +
			'</tr>\n' +
			'</thead>\n' +
			'<tbody class="rec_list">';
		let num = 1;
		if (data.length > 0) {
			_.forEach(data, function (val) {
				html += '<tr>\n' +
					// '      <th class="text-center" scope="row">'+num+++'.</th>\n' +
					'      <td class="text-center">'+val.ITEM+'</td>\n' +
					'      <td class="text-right">'+val.WATTAGE+'</td>\n' +
					'      <td class="text-center">' +
					'			<button data-id='+val.id+' type="button" class="btn btn-sm btn-danger btn-equip-del"><i class="fas fa-trash"></i> DELETE</button>' +
					'			<button data-id='+val.id+' type="button" class="btn btn-sm btn-info btn-equip-edit"><i class="fas fa-edit"></i> EDIT</button>' +
					'	   </td>\n' +
					'    </tr>';
			});
		} else {
			html = '<tr class="col-12 ">' +
						'<td colspan="6">' +
							'<h5 class="text-center text-white mg-top-10">' +
							'<i class="fas fa-spinner fa-spin display-4 text-muted"></i></br></br>' +
							'<small class="text-center text-muted mg-top-10"><em>Loading data...</em></small>' +
							'</h5>' +
						'</td>'+
					'</tr>';
		}
		html += '</tbody>\n' +
			'</table>';

		$('.tbl_equip').html(html);
		if (data.length > 0) {$('.tbl-equip').dataTable();}
	});
}

function set_rec_select(data, num = 1) {
	let html = '';
	_.forEach(data, function (val) {
		html += '<option data-num="'+num+'" data-nom="'+val.no_of_modules+'" data-emp="'+val.empty_modules+'" data-mc="'+val.module_capacity+'">'+val.name+'</option>';
	});
	$('#RECTIFIER_'+num).html(html).select2({ width: '100%' });
}

$(document).on("change", '#RECTIFIER_1, #RECTIFIER_2, #RECTIFIER_3', function (e) {
	e.preventDefault();
	let num = $(this).find(':selected').attr('data-num');
	let nom = $(this).find(':selected').attr('data-nom');
	let emp = $(this).find(':selected').attr('data-emp');
	let mc = $(this).find(':selected').attr('data-mc');
	total_max = emp;
	$('#RS'+num+'_MODULE_CAPACITY').val(mc);
	$('#RS'+num+'_NO_OF_MODULES').val(nom);
	$('#RS'+num+'_NO_OF_EMPTY_MODULE').val(emp);
});

$(document).on("change", '#SITE_TYPE', function (e) {
	e.preventDefault();
	let val = $(this).val() === 'Greenfield' ? 4 : 2;
	$('#OTHER_AC_LOAD_AAC').val(val);
});

$(document).on("click", '.rs1-untag, .rs2-untag, .rs3-untag', function (e) {
	e.preventDefault();
	let num = $(this).data('num');
	let mc = $(this).data('mc');
	mc = mc === '' ? 0 : parseInt(mc);
	total_mc = total_mc - mc;
	$('#RS'+num+'_MODULE_CAPACITY').val(total_mc);
	$(this).parent('div').remove();
});

function set_bat_select(data, num = 1) {
	let html = '';
	_.forEach(data, function (val) {
		html += '<option data-num="'+num+'" data-cc="'+val.capacity+'">'+val.name+'</option>';
	});
	$('#RS'+num+'_BATTERY_BRAND').html(html).select2({ width: '100%' });
}

$(document).on("change", '#RS1_BATTERY_BRAND, #RS2_BATTERY_BRAND, #RS3_BATTERY_BRAND', function (e) {
	e.preventDefault();
	let num = $(this).find(':selected').attr('data-num');
	let cc = $(this).find(':selected').attr('data-cc');
});

$(document).on('click', '.rs-add-btn', function (e) {
	e.preventDefault();
	let num = $(this).data('rs');
	$('.btn-'+num).addClass('d-none');
	num = num + 1;
	$('.rec-form').addClass('d-none');
	let rs = num === 2 ? rs2 : rs3;
	let html = '<div class="col-4 rs-'+num+'">';
	html += '<div class="row">';
	_.forEach(rs, function (val) {
		html += '<div class="col-'+val.col+'">' +
			'<div class="form-group mb-0">\n';
		html += '<small for='+val.name+'>'+val.title+'</small>';
		if (val.type === 'select') {
			html += '<select class="custom-select form-control custom-select-sm" id='+val.name+' name ='+val.name+'>';
			_.forEach(val.drowpdown, function (val1,key) {
				html += '<option value="'+key+'">'+val1+'</option>';
			});
			html += '</select>';
		} else {
			html += val.ons !== '' ? ' <div class="input-group input-group-sm mb-2 mr-sm-2">\n' : '';
			html += ' <input onchange="setTwoNumberDecimal" min="0" max="100000" step = "0.01" type="'+val.type+'" name='+val.name+' class="form-control form-control-sm" id='+val.name+' placeholder="Enter '+val.title+'" '+val.required+' '+val.only+' '+val.readonly+'>\n';
			if (val.ons !== '') {
				html += '<div class="input-group-append">\n' +
					'      <div class="input-group-text">'+val.ons+'</div>\n' +
					'    </div>\n';
			}
			html += val.ons !== '' ? ' </div>' : '';
		}
		html += '</div>'+
			'</div>';
	});

	html += '</div>';
	if (num === 2) {
		html += '  <button data-rs="2" type="button" class="btn btn-2 btn-sm w-100 btn-block rs-add-btn btn-outline-primary"><i class="fas fa-plus"></i> Add Rectifier</button>\n' +
			'  <button data-rm="2" type="button" class="btn btn-2 btn-sm  w-100 btn-block rs-rem-btn btn-outline-danger"><i class="fas fa-minus"></i> Remove Rectifier</button>\n';
	} else {
		html += '<div class="mg-top-5 btn-group-vertical d-flex btn-group-sm" role="group">\n' +
			'  <button data-rm="3" type="button" class="btn w-100 rs-rem-btn btn-block btn-outline-danger"><i class="fas fa-minus"></i> Remove Rectifier</button>\n' +
			'</div>';
	}

	html += '</div>';

	$('.rs').append(html);
	get_rec_bat(num);
});

$(document).on('click', '.rs-rem-btn', function (e) {
	e.preventDefault();
	let num = $(this).data('rm');
	$('.rs-'+num).remove();
	num = num - 1;
	if (num === 1) {$('.rec-form').removeClass('d-none');}
	$('.btn-'+num).removeClass('d-none');
});

$(document).on('keyup', '#RS1_NO_OF_MODULES', function (e) {
	e.preventDefault();
	let rs = $('#RS1_NO_OF_EMPTY_MODULE');
	let val = $(this).val();
	let max = rs.val();
	let total = max - val;
	if (max < val) {
		SWAL('error', 'Invalid', 'Must be less than to Empty Modules');
		total = total_max;
		$('#RS1_NO_OF_MODULES').val(0);
	}
	rs.val(total);
});

$(document).on('keyup', '#RS2_NO_OF_MODULES', function (e) {
	e.preventDefault();
	let rs = $('#RS2_NO_OF_EMPTY_MODULE');
	let val = $(this).val();
	let max = rs.val();
	let total = max - val;
	if (max < val) {
		SWAL('error', 'Invalid', 'Must be less than to Empty Modules');
		total = total_max;
		$('#RS2_NO_OF_MODULES').val(0);
	}
	rs.val(total);
});

$(document).on('keyup', '#RS3_NO_OF_MODULES', function (e) {
	e.preventDefault();
	let rs = $('#RS3_NO_OF_EMPTY_MODULE');
	let val = $(this).val();
	let max = rs.val();
	let total = max - val;
	if (max < val) {
		SWAL('error', 'Invalid', 'Must be less than to Empty Modules');
		total = total_max;
		$('#RS3_NO_OF_MODULES').val(0);
	}
	rs.val(total);
});

function get_completion() {
	let param = {
		url: url + 'get_power_completion',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then((data) => {
		let percent = parseInt(data.total) === 0 ? 0 : getWholePercent(data.completed, data.total);

		$('.cmp').text(data.completed + ' / ' + data.total);
		$('.crate').text( percent + '%');
	});
}
