$(function () {
	get_prop_list();
	check_generate();
});

function get_prop_list() {
	let param = {
		url: url + 'get_prop_list',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then((data) => {
		let html = '';
		if (data.power.length > 0) {
			html += '<option value="" disabled selected>Select Proposal</option>';
			_.forEach(data.power, function (val) {
				html += '<option value='+val.id+'>'+val.name+'</option>'
			});
		} else {
			html += '<option disabled selected>No Available Proposal</option>';
		}
		html += '</ul>';
		$('#power-prop').html(html).select2({theme: "bootstrap"});

		let html1 = '';
		if (data.tower.length > 0) {
			html1 += '<option value="" disabled selected>Select Proposal</option>';
			_.forEach(data.tower, function (val) {
				html1 += '<option value='+val.name+'>'+val.title+'</option>'
			});
		} else {
			html1 += '<option disabled selected>No Available Proposal</option>';
		}
		html1 += '</ul>';
		$('#tower-prop').html(html1).select2({theme: "bootstrap"});
	});
}

function set_assessment(data) {
	let html = '<table class="shadow table tbl-assessment force-fit display nowrap table-striped table-hover table-bordered small">\n' +
		'\t\t\t<thead class="global_color text-white">\n' +
		'\t\t\t\t<tr class="text-center small">\n' +
		'\t\t\t\t\t<th scope="col">#</th>\n' +
		'\t\t\t\t\t<th scope="col">PLAID</th>\n' +
		'\t\t\t\t\t<th scope="col">SITENAME</th>\n' +
		'\t\t\t\t\t<th scope="col">REGION</th>\n' +
		'\t\t\t\t\t<th scope="col">TOWER ASSESSMENT RESULT</th>\n' +
		'\t\t\t\t\t<th scope="col">POWER AC ASSESSMENT RESULT</th>\n' +
		'\t\t\t\t\t<th scope="col">POWER DC ASSESSMENT RESULT</th>\n' +
		'\t\t\t\t\t<th scope="col">OVERALL POWER ASSESSMENT RESULT</th>\n' +
		'\t\t\t\t\t<th scope="col">SIMPLIFIED OVERALL FACILITIES ASSESSMENT</th>\n' +
		'\t\t\t\t\t<th scope="col">FACILITIES UPGRADE SCOPE</th>\n' +
		'\t\t\t\t\t<th scope="col">OVERALL FACILITIES ASSESSMENT</th>\n' +
		'\t\t\t\t</tr>\n' +
		'\t\t\t</thead>\n' +
		'\t\t\t<tbody>\n';
	    let num = 1;
		_.forEach(data, function (val) {
			let tower_ass = val.tower_ass;
			let tcolor = 'success';
			let ticon = 'fa-check-circle';
			if (tower_ass === 'Requires Upgrade') {
				tcolor = 'danger';
				ticon = 'fas fa-tools';
			} else if (tower_ass === 'Proceed_Tower @ Threshold') {
				tcolor = 'warning';
				ticon = 'fa-exclamation-triangle';
			}

			let ac_color = val.ac_ass === 'Sufficient' ? 'success' : 'danger';
			let ac_icon = val.ac_ass === 'Sufficient' ? 'fa-check-circle' : 'fas fa-tools';

			let dc_color = val.dc_ass === 'Sufficient' ? 'success' : 'danger';
			let dc_icon = val.dc_ass === 'Sufficient' ? 'fa-check-circle' : 'fas fa-tools';

			let op_color = val.overall_power_ass === 'Sufficient' ? 'success' : 'danger';
			let op_icon = val.overall_power_ass === 'Sufficient' ? 'fa-check-circle' : 'fas fa-tools';

			let simp_color = val.simp_overall_ass === 'Ready to Build' ? 'success' : (val.simp_overall_ass === 'Requires Upgrade' ? 'danger' : 'warning');
			let simp_icon = val.simp_overall_ass === 'Requires Upgrade' ? 'fas fa-tools' : 'fa-check-circle';

			let fus_color = val.fac_upgrade_scope === 'No Upgrade Requirement' ? 'success' : 'danger';
			let fus_icon = val.fac_upgrade_scope === 'No Upgrade Requirement' ? 'fa-check-circle' : 'fas fa-tools';

			let ov_color = val.overall_facilities_ass === 'RFI' ? 'success' : 'danger';
			let ov_icon = val.overall_facilities_ass === 'RFI' ? 'fa-check-circle' : 'fas fa-tools';

			html += '\t\t\t<tr>\n' +
				'\t\t\t\t<th class="text-center" scope="row">'+num+++'.</th>\n' +
				'\t\t\t\t<td class="text-center">'+val.plaid+'</td>\n' +
				'\t\t\t\t<td class="text-center">'+val.sitename+'</td>\n' +
				'\t\t\t\t<td class="text-center">'+val.region+'</td>\n' +
				'\t\t\t\t<td class="table-'+tcolor+'"><i class="fas '+ticon+'"></i> '+val.tower_ass+'</td>\n' +
				'\t\t\t\t<td class="table-'+ac_color+'"><i class="fas '+ac_icon+'"></i> '+val.ac_ass+'</td>\n' +
				'\t\t\t\t<td class="table-'+dc_color+'"><i class="fas '+dc_icon+'"></i> '+val.dc_ass+'</td>\n' +
				'\t\t\t\t<td class="table-'+op_color+'"><i class="fas '+op_icon+'"></i> '+val.overall_power_ass+'</td>\n' +
				'\t\t\t\t<td class="table-'+simp_color+'"><i class="fas '+simp_icon+'"></i> '+val.simp_overall_ass+'</td>\n' +
				'\t\t\t\t<td class="table-'+fus_color+'"><i class="fas '+fus_icon+'"></i> '+val.fac_upgrade_scope+'</td>\n' +
				'\t\t\t\t<td class="table-'+ov_color+'"><i class="fas '+ov_icon+'"></i> '+val.overall_facilities_ass+'</td>\n' +
				'\t\t\t</tr>\n';
		});
		html += '\t\t\t</tbody>\n' +
		'\t\t</table>';

	$('.assessment').html(html);
	$('.tbl-assessment').dataTable();
}

$(document).on("change", "#power-prop", function (e) {
	e.preventDefault();
	let tower = $('#tower-prop').val();
	let power = $(this).val();
	check_generate(power, tower);
});

$(document).on("change", "#tower-prop", function (e) {
	e.preventDefault();
	let tower = $(this).val();
	let power = $('#power-prop').val();
	check_generate(power, tower);
});

function check_generate(power = '', tower = '') {
	if ((power !== '' && power !== null) && (tower !== '' && tower !== null)) {
		$('.ass-generate').prop('disabled', false);
	} else {
		$('.ass-generate').prop('disabled', true);
	}
}

$(document).on('click', '.tbl-assessment tbody tr', function (e) {
	e.preventDefault();
	if ( $(this).hasClass('selected') ) {
		$(this).removeClass('selected');
	}
	else {
		$('.tbl-assessment tr.selected').removeClass('selected');
		$(this).addClass('selected');
	}
});

$(document).on("click", ".ass-generate", function (e) {
	e.preventDefault();
	let pid = $('#power-prop').val();
	let tid = $('#tower-prop').val();

	let ptext = $('#power-prop option:selected').text();
	let ttext = $('#tower-prop option:selected').text();

	$('.list-power').text(ptext);
	$('.list-tower').text(ttext);

	let param = {
		url: url + 'get_assessments',
		dataType: 'json',
		type: 'POST',
		data: {pid: pid, tid: tid},
		beforeSend: $(this).html('<em>Generating <i class="fas fa-spinner fa-spin text-muted"></i></em>').prop('disabled', true)
	};
	fortress(param).then((data) => {
		$('.select-proposal').addClass('d-none');
		$('.ass-tbl').removeClass('d-none');
		set_assessment(data);
	});
});

$(document).on("click", ".clear-ass", function (e) {
	e.preventDefault();
	$('.select-proposal').removeClass('d-none');
	$('.ass-tbl').addClass('d-none');
	$('#power-prop, #tower-prop').val('');
	$('.ass-generate').text('Generate').prop('disabled', true);
});
