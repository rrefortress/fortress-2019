let result = [];
function set_proposal() {
	$('.view-pro, .export_pro, .save_pro, .clear_pro').prop('disabled', true);
	$('#proposal-list').val('');
	$('#pro_div').html('<div class="col-12 mg-top-135" style="height: 45vh;">\n' +
		'\t\t\t<h5 class="text-center text-white">\n' +
		'\t\t\t\t<i class="fas fa-table display-2 text-muted"></i></br>\n' +
		'\t\t\t\t<small class="text-center text-muted mg-top-10"><em>Please upload file first to display proposal lists.</em></small>\n' +
		'\t\t\t</h5>\n' +
		'\t\t</div>');
}

function get_proposal_list() {
	let param = {
		url: url + 'get_prop_list',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then((data) => {
		let num = 1;
		let html = '<ul class="list-group">';
		if (data.power.length > 0) {
			_.forEach(data.power, function (val) {
				html += '<li class="list-group-item p3">' +
							'<div class="row">' +
								'<div class="col-7">'+num+++'. | '+val.name+'</div>' +
								'<div class="col-5">' +
									'<button data-id="'+val.id+'" type="button" class="btn btn-sm btn-outline-success view_prop-lists mr-1"><i class="fas fa-eye"></i> VIEW</button>' +
									'<button data-id="'+val.id+'" type="button" class="btn btn-sm btn-outline-danger del_prop mr-1"><i class="fas fa-trash"></i> DELETE</button>' +
									'<button data-name="'+val.name+'" data-id="'+val.id+'" type="button" class="btn btn-sm btn-outline-warning edit_prop"><i class="fas fa-edit"></i> RENAME</button>' +
								'</div>' +
							'</div>' +
						'</li>';
			});
		} else {
			html = '<li class="list-group-item list-group-item-danger">No Available Proposal.</li>';
		}
		html += '</ul>';
		$('.prop-lists').html(html);
	});
}

$(document).on("click", ".view_prop-lists", function (e) {
	e.preventDefault();
	let id = $(this).data('id');
	let param = {
		url: url + 'get_proposals',
		dataType: 'json',
		type: 'POST',
		data: {batch_key: id}
	};
	fortress(param).then((data) => {
		$('#view-pro-list').modal('hide');
		get_proposal(data);
	});
});

$(document).on('click', '.del_prop', function (e) {
	e.preventDefault();
	let id = $(this).data('id');
	Swal.fire({
		title: 'Are you sure?',
		text: "You want to delete this data",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	}).then((result) => {
		if (result.value) {
			let param = {
				url: url + 'del_prop',
				dataType: 'json',
				type: 'POST',
				data: {id: id},
			};

			fortress(param).then((data) => {
				SWAL(data.type, data.title, data.msg);
				get_proposal_list();
			});
		}
	});
});

$(document).on('click', '.edit_prop', function (e) {
	e.preventDefault();
	let id = $(this).data('id');
	let name = $(this).data('name');
	$('#id').val(id);
	$('#prop_name').val(name);
	$('#view-pro-list').modal('hide');
	$('#edit-pro-list').modal('show');
});

$(document).on('submit', '#rename_prop', function (e) {
	e.preventDefault();
	let param = {
		url: url + 'edit_prop',
		dataType: 'json',
		type: 'POST',
		data: $(this).serialize()
	};
	fortress(param).then((data) => {
		SWAL(data.type, data.title, data.msg);
		$('#edit-pro-list').modal('hide');
		$('#view-pro-list').modal('show');
		get_proposal_list();
	});
});

$(document).on('click', '.cancel_prop', function (e) {
	e.preventDefault();
	$('#edit-pro-list').modal('hide');
	$('#view-pro-list').modal('show');
});

$(document).on('click', '.proposal', function (e) {
	e.preventDefault();
	$('#bulk-proposal').modal('show');
});

$(document).on('click', '.view-proposal', function (e) {
	e.preventDefault();
	get_proposal_list();
	$('#view-pro-list').modal('show');
});

$(document).on("click", ".clear_pro", function (e) {
	e.preventDefault();
	set_proposal();
});

$(document).on("click", ".export_pro", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (result.length > 0) {
		JSONToCSVConvertor(result, "Proposal_(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on('click', '.del_all', function (e) {
	e.preventDefault();
	Swal.fire({
		title: 'Are you sure?',
		text: "You want to delete all data",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	}).then((result) => {
		if (result.value) {
			let param = {
				url: url + 'del_all_prop',
				dataType: 'json',
				type: 'GET',
			};

			fortress(param).then((data) => {
				SWAL(data.type, data.title, data.msg);
				get_proposal_list();
			});
		}
	});
});

function get_proposal(data) {
	$('.view-pro, .export_pro, .save_pro, .clear_pro').prop('disabled', false);
	result = data;
	let html = '\t\t\t\t\t\t<table id="proposal_table" class="force-fit display nowrap table-striped table-hover table-sm small table-bordered small nowrap mg-top-5">\n' +
		'\t\t\t\t\t\t<thead class="global_color text-white text-uppercase">\n' +
		'\t\t\t\t\t\t<tr>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">PLAID</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">Site Name</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">Site Class</th>\n' +
		'\t\t\t\t\t\t\t<th scope="co" class="text-center">Existing Capacity/Util</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">Existing Load AAC</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">Proposed Load AAC</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">Endstate Capacity/Util</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">R1 ITEM</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">R1 Addtional Module</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">R1 Remaining Headroom(W)</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">R1 Battery Remarks</th>\n' +

		'\t\t\t\t\t\t\t<th scope="col" class="text-center">R2 ITEM</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">R2 Addtional Module</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">R2 Remaining Headroom(W)</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">R2 Battery Remarks</th>\n' +

		'\t\t\t\t\t\t\t<th scope="col" class="text-center">R3 ITEM</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center"">R3 Addtional Module</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">R3 Remaining Headroom(W)</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">R3 Battery Remarks</th>\n' +

		'\t\t\t\t\t\t\t<th scope="col" class="text-center">Excluded</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">Excess Load W</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">Total Excess Load(W)</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">DC Recommendations</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">DC Remarks</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">AC Recommendations</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center">AC Remarks</th>\n' +
		'\t\t\t\t\t\t</tr>\n' +
		'\t\t\t\t\t\t</thead>\n' +
		'\t\t\t\t\t\t<tbody class="pro_lists">\n';
	_.forEach(data, function (val) {

		html += '<tr>' +
			'<td class="align-middle">'+val.plaid+'</td>' +
			'<td class="align-middle">'+val.sitename+'</td>' +
			'<td class="text-center align-middle">'+val.site_class+'</td>' +
			'<td class="align-middle"><span class="badge badge-primary">A</span> : '+val.ex_transformer_capacity+' ('+val.ex_transformer_util+'%) | ' +
				'<span class="badge badge-success">B</span> : '+val.ex_genset_capacity +' ('+ val.ex_genset_util +'%) | ' +
				'<span class="badge badge-warning">C</span> : '+val.ex_ecb_capacity+' ('+val.ex_ecb_util +'%) | ' +
				'<span class="badge badge-danger">D</span> : '+val.ex_acpdb_capacity+' ('+val.ex_acpdb_util +'%) ' +
			'</td>' +
			'<td class="text-right align-middle">'+val.existing_load+'</td>' +
			'<td class="text-right align-middle">'+val.proposed_load+'</td>' +
			'<td class="align-middle"><span class="badge badge-primary">A</span> : '+val.endstate_transformer+' ('+val.endstate_transformer_util+'%) | ' +
				'<span class="badge badge-success">B</span> : '+val.endstate_genset +' ('+ val.endstate_genset_util +'%) | ' +
				'<span class="badge badge-warning">C</span> : '+val.endstate_ecb+' ('+val.endstate_ecb_util +'%) | ' +
				'<span class="badge badge-danger">D</span> : '+val.endstate_acpdb+' ('+val.endstate_acpdb_util+'%)' +
			'</td>' +

			'<td class="align-middle">'+val.r1_item+'</td>' +
			'<td class="text-right align-middle">'+val.r1_proposed_module+'</td>' +
			'<td class="text-right align-middle">'+val.r1_remaining_headroom+'</td>' +
			'<td class="text-center align-middle">'+val.r1_battery_remarks+'</td>' +

			'<td class="align-middle">'+val.r2_item+'</td>' +
			'<td class="text-right align-middle">'+val.r2_proposed_module+'</td>' +
			'<td class="text-right align-middle">'+val.r2_remaining_headroom+'</td>' +
			'<td class="text-center align-middle">'+val.r2_battery_remarks+'</td>' +

			'<td class="align-middle">'+val.r3_item+'</td>' +
			'<td class="text-right align-middle">'+val.r3_proposed_module+'</td>' +
			'<td class="text-right align-middle">'+val.r3_remaining_headroom+'</td>' +
			'<td class="text-center align-middle">'+val.r3_battery_remarks+'</td>' +

			'<td class="align-middle">'+val.excluded+'</td>' +
			'<td class="text-right align-middle">'+val.excess_wattage+'</td>' +
			'<td class="text-right align-middle">'+val.total_excess+'</td>' +
			'<td class="align-middle">'+val.dc_recomendations+'</td>' +
			'<td class="align-middle">'+val.dc_remarks+'</td>' +
			'<td class="align-middle">'+val.ac_recomendations+'</td>' +
			'<td class="align-middle">'+val.ac_remarks+'</td>' +
			'</tr>';
	});

	html += '\t\t\t\t\t\t</tbody>\n';
	'\t\t\t\t\t</table>';
	$('#pro_div').html(html);
	$('#proposal_table').DataTable( {
		select: true
	} );
}

$(document).on('click', '#proposal_table tbody tr', function (e) {
	e.preventDefault();
	if ( $(this).hasClass('selected') ) {
		$(this).removeClass('selected');
	}
	else {
		$('#proposal_table tr.selected').removeClass('selected');
		$(this).addClass('selected');
	}
});
