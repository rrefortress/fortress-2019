let suff = {
	'suffi': 0,
	'unsuffi': 0
};

let sc = {
	A1 : 0,
	A2 : 0,
	B1 : 0,
	B2 : 0,
	C1 : 0,
	C2 : 0,
	C3 : 0,
	C4 : 0,
	C5 : 0,
};

let div_charts = [
	{
		name: 'te'
	},
	{
		name: 'sc'
	},
];

$(document).on('click', '.view-pro', function (e) {
	e.preventDefault();
	if (pro)
	$('.pro_charts').html(set_pro_charts());
	set_te_chart(suff);
	set_sc_chart(sc);
	$('#view-pro').modal('show');
});

function set_pro_charts() {
	let html = '';
	_.forEach(div_charts, function (val) {
		html += '<div class="col-6">\n' +
					'<div class="chart-container">\n' +
					'    <canvas id="' + val.name + '_chart"></canvas>\n' +
					'</div>' +
				'</div>';
	});
	return html;
}

function set_te_chart(data) {
	let canvasBar = document.getElementById("te_chart");
	let ctxBar = canvasBar.getContext('2d');
	let va1 = data.suffi;
	let va2 = data.unsuffi;
	let total = va1 + va2;

	let total_per = getWholePercent(va1, total) +
					getWholePercent(va2, total);

	let datas = {
		labels: [va1 +":Sufficient", va2 +": Un-sufficient"],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#f0a500', '#28a745'],
				data: [
					getWholePercent(va1, total),
					getWholePercent(va2, total),
				]
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText("Total Excess"),
		rotation : -0.7 * Math.PI,
		legend: {
			position: 'left',
		},
		layout: {
			padding: {
				left: 10,
				right: 10,
				top: 10,
				bottom: 10
			}
		}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', 'te_pro');
}

function set_sc_chart(data) {
	let canvasBar = document.getElementById("sc_chart");
	let ctxBar = canvasBar.getContext('2d');

	let A1 = data.A1;
	let A2 = data.A2;
	let B1 = data.B1;
	let B2 = data.B2;
	let C1 = data.C1;
	let C2 = data.C2;
	let C3 = data.C3;
	let C4 = data.C4;
	let C5 = data.C5;
	let total = A1 + A2 + B1 + B2 + C1 + C2 + C3 + C4 + C5;

	let total_per = getWholePercent(A1, total) +
		getWholePercent(A2, total) +
		getWholePercent(B1, total) +
		getWholePercent(B2, total) +
		getWholePercent(C1, total) +
		getWholePercent(C2, total) +
		getWholePercent(C3, total) +
		getWholePercent(C4, total) +
		getWholePercent(C5, total);

	let datas = {
		labels: [	A1 +": A1",
			A2 +": A2",
			B1 +": B1",
			B2 + ': B2',
			C1 + ': C1',
			C2 + ': C2',
			C3 + ': C3',
			C4 + ': C4',
			C5 + ': C5'
		],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: [	'#fa4252',
					'#448AFF',
					'#f0a500',
					'#438a5e',
					'#6a197d',
					'#835858',
					'#ff5722',
					'#303960',
					'#79d70f',
				],
				data: [
					getWholePercent(A1, total),
					getWholePercent(A2, total),
					getWholePercent(B1, total),
					getWholePercent(B2, total),
					getWholePercent(C1, total),
					getWholePercent(C2, total),
					getWholePercent(C3, total),
					getWholePercent(C4, total),
					getWholePercent(C5, total),
				]
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText("Site Class"),
		rotation : -0.7 * Math.PI,
		legend: {
			position: 'left',
		},
		layout: {
			padding: {
				left: 10,
				right: 10,
				top: 10,
				bottom: 10
			}
		}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', 'sc_pro');
}
