Dropzone.autoDiscover = false;

let fe_upload= new Dropzone(".dropzone_power",{
	url: url + "Power/power_upload",
	timeout: 180000,
	parallelUploads: 100,
	maxFiles: 1,
	method:"POST",
	dataType: 'json',
	acceptedFiles:".csv",
	paramName:"power_file",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:true,
	init: function() {
		this.on("success", function(file, response) {
			data = jQuery.parseJSON(response);
			SWAL(data.type, data.title, data.msg);
			if (data.type === 'success') {
				// set_response(data);
				get_completion();
				sitesdatatable.ajax.reload();
			}
		})
	}
});

fe_upload.on("queuecomplete", function () {
	this.removeAllFiles();
	$("#bulk-upload").modal('hide');
	$("#bulk-upload-power").modal('hide');
});

fe_upload.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});

fe_upload.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});

let pro_upload= new Dropzone(".dropzone_proposal",{
	url: url + "Power/proposal_upload",
	maxFiles: 1,
	method:"POST",
	dataType: 'json',
	acceptedFiles:".csv",
	paramName:"proposed_file",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:true,
	init: function() {
		this.on("success", function(file, response) {
			let data = jQuery.parseJSON(response);
			if (data.type === 'error') {
				SWAL(data.type, data.title, data.msg);
			} else {
				get_proposal_list();
				get_proposal(data);
			}
			$('#bulk-proposal').modal('hide');
		})
	}
});

pro_upload.on("queuecomplete", function () {
	this.removeAllFiles();
	$("#bulk-upload").modal('hide');
});

pro_upload.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});

pro_upload.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});
