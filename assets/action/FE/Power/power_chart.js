let stbu = 0;

function set_ac_chart(data, elem, param) {
	let canvasBar = document.getElementById(elem);
	let ctxBar = canvasBar.getContext('2d');

	let v1 = data.v1;
	let v2 = data.v2;
	let v3 = data.v3;
	let v4 = data.v4;
	let v5 = data.v5;
	let sc = data.sc;
	let tbu = data.tbu;
	let none = data.none;


	let total = v1 +
				v2 +
				v3 +
				v4 +
				v5 +
				none +
				tbu;

	let total_per = getWholePercent(v1, total) +
		getWholePercent(v2, total) +
		getWholePercent(v3, total) +
		getWholePercent(v4, total) +
		getWholePercent(v5, total) +
		getWholePercent(none, total) +
		getWholePercent(sc, total) +
		getWholePercent(tbu, total);

	let l1 = param.dkey == 0 ? ": Greater than 50KVA" : ": < 50KVA";
	let l2 = param.dkey == 0 ? ": Equal to 50KVA" : ": = 50KVA";
	let l3 = param.dkey == 0 ? ": Equal to 40KVA(37.5KVA)" : ": = 40KVA(37.5KVA)";
	let l4 = param.dkey == 0 ? ": Equal to 25KVA" : ": = 25KVA";
	let l5 = param.dkey == 0 ? ": Greater than or Equal to 15KVA" : ": <= 15KVA";
	let l6 = param.dkey == 0 ? ": To be updated" : ": TBU";
	let l7 = param.dkey == 0 ? ": None / Tap to Building" : ": None/TB";
	let l8 = param.dkey == 0 ? ": Small Cell" : ": SC";

	let datas = {
		labels: [
				v1   + l1,
				v2   + l2,
				v3   + l3,
				v4   + l4,
				v5   + l5,
				tbu  + l6,
				none + l7,
				sc   + l8
				],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: [
					'#54e346',
					'#0e49b5',
					'#fecd1a',
					'#f05454',
					'#8f384d',
					'#686d76',
					'#1b262c',
					'#f21170',
				],
				data: [
					getWholePercent(v1, total),
					getWholePercent(v2, total),
					getWholePercent(v3, total),
					getWholePercent(v4, total),
					getWholePercent(v5, total),
					getWholePercent(tbu, total),
					getWholePercent(none, total),
					getWholePercent(sc, total),
				],
			}
		]
	};

	let optionsPie = {
		elements : centerText(total),
		title : param.title,
		plugins  : setPlugins(),
		rotation : -0.7 * Math.PI,
		legend: {
			position: param.pos,
			labels: {
				boxWidth: param.box,
				padding: param.pad
			},
		},
		layout: {padding: 10}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', param.site);
}

function set_cc_chart(data, cname = '', name = '', subtitle = '') {
	let canvasBar = document.getElementById(cname+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let kva1 = data.v1;
	let kva2 = data.v2;
	let kva3 = data.v3;
	let kva4 = data.v4;
	let kva5 = data.v5;
	let tbu = data.tbu;
	let none = data.None;

	let total = kva1 + kva2 + kva3 +  kva4 +  kva5 + none + tbu;

	let total_per = getWholePercent(kva1, total) +
		getWholePercent(kva2, total) +
		getWholePercent(kva3, total) +
		getWholePercent(kva4, total) +
		getWholePercent(kva5, total) +
		getWholePercent(none, total) +
		getWholePercent(tbu, total);

	let datas = {
		labels: [kva1+": Greater than 50KVA",
			     kva2+": Equal to 50KVA",
				 kva3+": Equal to 40KVA (37.5KVA)",
			     kva4+": Equal to 25KVA",
				 kva5+": Less than 25KVA",
				 tbu + ': To be updated',
			     none+": None (No GENSET)",
				],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: [
					'#54e346',
					'#0e49b5',
					'#fecd1a',
					'#f05454',
					'#8f384d',
					'#686d76',
					'#1b262c',
				],
				data: [
					getWholePercent(kva1, total),
					getWholePercent(kva2, total),
					getWholePercent(kva3, total),
					getWholePercent(kva4, total),
					getWholePercent(kva5, total),
					getWholePercent(tbu, total),
					getWholePercent(none, total),
				],
			}
		]
	};

	let optionsPie = {
		elements : centerText(total),
		title: setTitle(name + " CAPACITY", subtitle),
		plugins  : setPlugins(),
		rotation : -0.7 * Math.PI,
		legend: {
			position: 'left',
		},
		layout: {padding: 10}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', cname);
}

function set_trf_chart(data, cname = '', name = '', subtitle = '') {
	let canvasBar = document.getElementById(cname+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let kva1 = data.g50;
	let kva2 = data.g37l50;
	let kva3 = data.g25l37;
	let kva4 = data.g15l25;
	let kva5 = data.l15;
	let none = data.None;

	let total = kva1 + kva2 + kva3 +  kva4 +  kva5;

	let total_per = getWholePercent(kva1, total) +
		getWholePercent(kva2, total) +
		getWholePercent(kva3, total) +
		getWholePercent(kva4, total) +
		getWholePercent(kva5, total);

	let datas = {
		labels: [
			kva1+": Greater than 50KVA",
			kva2+": 37.5KVA",
			kva3+": 25KVA",
			kva4+": 15KVA",
			kva5+": Less than 15KVA"
		],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: [
					'#54e346',
					'#0e49b5',
					'#fecd1a',
					'#f05454',
					'#8f384d',
				],
				data: [
					getWholePercent(kva1, total),
					getWholePercent(kva2, total),
					getWholePercent(kva3, total),
					getWholePercent(kva4, total),
					getWholePercent(kva5, total)
				],
			}
		]
	};

	let optionsPie = {
		elements : centerText(total),
		title: setTitle(name + " CAPACITY", subtitle),
		plugins  : setPlugins(),
		rotation : -0.7 * Math.PI,
		legend: {
			position: 'left',
		},
		layout: {padding: 10}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', cname);
}

function set_pc_chart(data) {
	let canvasBar = document.getElementById("pc_chart");
	let ctxBar = canvasBar.getContext('2d');

	let gen = data['Genset Prime'];
	let sol = data['Sole Transformer'];
	let tb = data['Tapped to Building'];
	let tn = data['Tapped to Neighbor'];
	let tg = data['Tapped to Genset'];
	let tc = data['Tempo to Coop'];
	let st = data['Shared Transformer'];
	let np = data['No Power'];
	let tbu = data['To be Updated'];

	let sc = data['SMALL CELL'];
	let total = gen + sol + tb + tn + tg + tc + st + np + sc + tbu;

	let total_per = getWholePercent(gen, total) +
		getWholePercent(sol, total) +
		getWholePercent(tb, total) +
		getWholePercent(tn, total) +
		getWholePercent(tg, total) +
		getWholePercent(tc, total) +
		getWholePercent(st, total) +
		getWholePercent(np, total) +
		getWholePercent(sc, total) +
		getWholePercent(tbu, total);

	let datas = {
		labels: [
			sol+": Sole Transformer (Permanent)",
			st+": Shared Transformer (Permanent)",
			tb+": Tapped to Building (Permanent)",
			tc+": Tapped to Coop (Temporary)",
			tn+": Tapped to Neighbor (Temporary)",
			tg+": Tapped to Genset (Temporary)",
			sc + ': Small Cell',
			gen+": Genset Prime (Endstate)",
			np+": No Power",
			tbu + ': To be Updated',
		],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: [
					'#0e49b5',
					'#28df99',
					'#ff4b5c',
					'#52057b',
					'#ff9642',
					'#fecd1a',
					'#f21170',
					'#af6b58',
					'#252525',
					'#686d76',
				],
				data: [
					getWholePercent(sol, total),
					getWholePercent(st, total),
					getWholePercent(tb, total),
					getWholePercent(tc, total),
					getWholePercent(tn, total),
					getWholePercent(tg, total),
					getWholePercent(sc, total),
					getWholePercent(gen, total),
					getWholePercent(np, total),
					getWholePercent(tbu, total),
				],
			}
		]
	};

	let optionsPie = {
		elements : centerText(total),
		title : setTitle("POWER CONNECTION"),
		plugins  : setPlugins(),
		rotation : -0.7 * Math.PI,
		legend: {
			position: 'left',
		},
		responsive: true,
		scales: {
			yAxes: [{
				display: false,
				ticks: {
					min:0,
					max:100
				}
			}]
		},
		// layout: {padding: 5}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', 'pcc');
}

function set_bat_chart(data, elem, param) {
	let canvasBar = document.getElementById(elem);
	let ctxBar = canvasBar.getContext('2d');
	let l4_hr = data.l4_hr;
	let g4l8_hr = data.g4l8_hr;
	let g8_hr = data.g8_hr;
	let tbu = data.tbu;
	let total = l4_hr + g4l8_hr + g8_hr + tbu;

	let total_per = getWholePercent(l4_hr, total) +
		getWholePercent(g4l8_hr, total) +
		getWholePercent(g8_hr, total) +
		getWholePercent(tbu, total);

	let l1 = param.dkey == 0 ? ': Greater than 8hrs' : ': < 8Hrs';
	let l2 = param.dkey == 0 ? ': Between 4hrs to 8hrs' : ': <= 4Hrs to >= 8Hrs';
	let l3 = param.dkey == 0 ? ': Less than 4hrs' : ': > 4Hrs';
	let l4 = param.dkey == 0 ? ': To be Updated' : ': TBU';
		
	let datas = {
		labels: [
				 g8_hr   + l1,
				 g4l8_hr + l2,
				 l4_hr   + l3,
				 tbu     + l4
				],

		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#0e49b5', '#fecd1a', '#fa4252', '#686d76'],
				data: [
					getWholePercent(g8_hr, total),
					getWholePercent(g4l8_hr, total),
					getWholePercent(l4_hr, total),
					getWholePercent(tbu, total),
				],
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText(total),
		title : param.title,
		rotation : -0.7 * Math.PI,
		legend: {
			position: param.pos,
			labels: {
				boxWidth: param.box,
				padding: param.pad
			},
		},
		layout: {padding: 10}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', param.site);
}

function set_dc_rec_chart(data, elem, param) {
	let canvasBar = document.getElementById(elem);
	let ctxBar = canvasBar.getContext('2d');

	let l16kw = data.l16kw;
	let g16kwl18kw = data.g16kwl18kw;
	let g18kwl24kw = data.g18kwl24kw;
	let g24kw = data.g24kw;
	let cc = data.cc;
	let total = l16kw + g16kwl18kw + g18kwl24kw + g24kw + cc;

	let total_per = getWholePercent(l16kw, total) +
		getWholePercent(g16kwl18kw, total) +
		getWholePercent(g18kwl24kw, total) +
		getWholePercent(g24kw, total) +
		getWholePercent(cc, total);

	let l1 = param.dkey == 0 ? ': Greater than 24KW' : ': < 24KW';	
	let l2 = param.dkey == 0 ? ': Between 18KW to 24KW' : ': <= 18KW to >= 24KW';	
	let l3 = param.dkey == 0 ? ': Between 16KW to 18KW' : ': <= 16KW to >= 18KW';	
	let l4 = param.dkey == 0 ? ': Less than 16KW' : ': < 16KW';	
	let l5 = param.dkey == 0 ? ': To be Updated' : ': TBU';

	let datas = {
		labels: [
			g24kw + l1,
			g18kwl24kw + l2,
			g16kwl18kw + l3,
			l16kw + l4,
			cc + l5
		],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#0e49b5', '#61b15a', '#fecd1a', '#fa4252', '#686d76'],
				data: [getWholePercent(g24kw, total),
					getWholePercent(g18kwl24kw, total),
					getWholePercent(g16kwl18kw, total),
					getWholePercent(l16kw, total),
					getWholePercent(cc, total)
				]
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText(total),
		title : param.title,
		rotation : -0.7 * Math.PI,
		legend: {
			position: param.pos,
			labels: {
				boxWidth: param.box,
				padding: param.pad
			},
		},
		layout: {padding: 10}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', param.site);
}

function set_sc_chart(data, elem, param) {
	let canvasBar = document.getElementById(elem);
	let ctxBar = canvasBar.getContext('2d');

	let A1 = data.A1 === 0 ? 0 : data.A1;
	let A2 = data.A2 === 0 ? 0 : data.A2;
	let B1 = data.B1 === 0 ? 0 : data.B1;
	let B2 = data.B2 === 0 ? 0 : data.B2;
	let B3 = data.B3 === 0 ? 0 : data.B3;
	let C1 = data.C1 === 0 ? 0 : data.C1;
	let C2 = data.C2 === 0 ? 0 : data.C2;
	let C3 = data.C3 === 0 ? 0 : data.C3;
	let C4 = data.C4 === 0 ? 0 : data.C4;
	let C5 = data.C5 === 0 ? 0 : data.C5;
	let tbu = data['To be Updated'] === 0 ? 0 : data['To be Updated'];
	stbu = tbu;
	let total = A1 + A2 + B1 + B2 + B3 + C1 + C2 + C3 + C4 + C5 + tbu;

	let a = A1 + A2;
	let b = B1 + B2 + B3;
	let c = C1 + C2 + C3 + C4 + C5;

	let total_per = getWholePercent(A1, total) +
		getWholePercent(A2, total) +
		getWholePercent(B1, total) +
		getWholePercent(B2, total) +
		getWholePercent(B3, total) +
		getWholePercent(C1, total) +
		getWholePercent(C2, total) +
		getWholePercent(C3, total) +
		getWholePercent(C4, total) +
		getWholePercent(C5, total) +
		getWholePercent(tbu, total);

	let datas = {
		labels: [	A1 +": A1",
			A2 +": A2",
			B1 +": B1",
			B2 + ': B2',
			B3 + ': B3',
			C1 + ': C1',
			C2 + ': C2',
			C3 + ': C3',
			C4 + ': C4',
			C5 + ': C5 (Small Cell)',
			tbu + ': To be Updated',
		],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: [
					'#1a237e',
					'#039be5',
					'#43a047',
					'#76ff03',
					'#fecd1a',
					'#fc8621',
					'#e91e63',
					'#8e24aa',
					'#c05555',
					'#e53935',
					'#686d76',
				],
				data: [
					getWholePercent(A1, total),
					getWholePercent(A2, total),
					getWholePercent(B1, total),
					getWholePercent(B2, total),
					getWholePercent(B3, total),
					getWholePercent(C1, total),
					getWholePercent(C2, total),
					getWholePercent(C3, total),
					getWholePercent(C4, total),
					getWholePercent(C5, total),
					getWholePercent(tbu, total)
				]
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText(total),
		title : elem === 'dash_SC_chart' ? '' : setTitle("SITE CLASS", "A - " + a + " | B - " + b + " | C - " + c),
		rotation : -0.7 * Math.PI,
		legend: {
			position: param.pos,
			labels: {
				boxWidth: param.box,
				padding: param.pad
			},
		},
		layout: {padding: 10}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', param.site);
}

function set_gs_chart(data) {
	let canvasBar = document.getElementById("GS_chart");
	let ctxBar = canvasBar.getContext('2d');

	let G1 = data.G1;
	let G2 = data.G2;
	let G3 = data.G3;
	let G4 = data.G4;
	let Prime = data.Prime;
	let tbu = data['to be updated'];
	let sc = data['SMALL CELL'];
	let total = G1 + G2 + G3 + G4 + Prime + sc + tbu;

	let total_per = getWholePercent(G1, total) +
		getWholePercent(G2, total) +
		getWholePercent(G3, total) +
		getWholePercent(G4, total) +
		getWholePercent(Prime, total) +
		getWholePercent(sc, total) +
		getWholePercent(tbu, total);

	let datas = {
		labels: [G1 +": Good Grid 0-2hrs CPF",
			     G2 +": Slight Bad Grid 2-4hrs CPF",
			     G3 +": Bad Grid 4-8hrs CPF",
			     G4 + ': Super Bad Grid > 8hrs CPF',
			     Prime + ': Off grid',
			     tbu + ': To be Updated',
			     sc + ': Small Cell (No BBUT)',],
		datasets: [
			{ 
				label: "",
				fill: true,
				backgroundColor: ['#0e49b5',
					              '#81b214',
					              '#fecd1a',
					              '#8f384d',
					              '#222831',
					              '#686d76',
								  '#f21170'],
				data: [
					getWholePercent(G1, total),
					getWholePercent(G2, total),
					getWholePercent(G3, total),
					getWholePercent(G4, total),
					getWholePercent(Prime, total),
					getWholePercent(tbu, total),
					getWholePercent(sc, total),
				]
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText(total),
		title : setTitle("POWER GRID CLASSIFICATION", "CPF - Commercial Power Failure"),
		rotation : -0.7 * Math.PI,
		legend: {
			position: 'left',
		},
		layout: {padding: 10}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', 'gs');
}

function set_bt_chart(data) {
	let canvasBar = document.getElementById("BT_chart");
	let ctxBar = canvasBar.getContext('2d');

	let li = data['Li-ion'];
	let vr = data['VRLA'];
	let bt = data['both'];
	let tbu = data['To be updated'];
	let total = li + vr + bt + tbu;

	let total_per = getWholePercent(li, total) +
		getWholePercent(vr, total) +
		getWholePercent(bt, total) +
		getWholePercent(tbu, total);

	let datas = {
		labels: [li +": Li-on", vr +": VRLA", bt +": Both", tbu +": To be updated"],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#79d70f', '#d8345f', '#f0a500', '#686d76'],
				data: [
					getWholePercent(li, total),
					getWholePercent(vr, total),
					getWholePercent(bt, total),
					getWholePercent(tbu, total),
				]
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText(total),
		title : setTitle("BATTERY TYPE"),
		rotation : -0.7 * Math.PI,
		legend: {
			position: 'left',
		},
		layout: {padding: 10}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', 'btt');
}

function set_rs_chart(data) {
	let canvasBar = document.getElementById("RSS_chart");
	let ctxBar = canvasBar.getContext('2d');

	let G1 = data.R1;
	let G2 = data.R2;
	let G3 = data.R3;
	let tbu = data.tbu;
	let none = data.none;
	let total = G1 + G2 + G3 + none + tbu;

	let total_per = getWholePercent(G1, total) +
		getWholePercent(G2, total) +
		getWholePercent(G3, total) +
		getWholePercent(none, total) +
		getWholePercent(tbu, total);

	let datas = {
		labels: [G1 +": 1 Rectifier", G2 +": 2 Rectifier", G3 +": 3 Rectifier", tbu + ": To be Updated", none + ": None"],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#0e49b5', '#fddb3a', '#54e346', '#686d76', '#252525'],
				data: [
					getWholePercent(G1, total),
					getWholePercent(G2, total),
					getWholePercent(G3, total),
					getWholePercent(tbu, total),
					getWholePercent(none, total),
				]
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText(total),
		title : setTitle("NO. OF RECTIFIERS PER SITE"),
		rotation : -0.7 * Math.PI,
		legend: {
			position: 'left',
		},
		layout: {padding: 10}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', 'rsss');
}

function set_rwdc_chart(data) {
	let canvasBar = document.getElementById("RWDC_chart");
	let ctxBar = canvasBar.getContext('2d');

	let l16kw = data.l16kw;
	let g16kwl18kw = data.g16kwl18kw;
	let g18kwl24kw = data.g18kwl24kw;
	let g24kw = data.g24kw;
	let cc = data.cc;
	let total = l16kw + g16kwl18kw + g18kwl24kw + g24kw + cc;

	let total_per = getWholePercent(l16kw, total) +
		getWholePercent(g16kwl18kw, total) +
		getWholePercent(g18kwl24kw, total) +
		getWholePercent(g24kw, total) +
		getWholePercent(cc, total);

	let datas = {
		labels: [l16kw +": < 16KW", g16kwl18kw +": > 16KW <= 18KW", g18kwl24kw +": > 18KW <= 24KW ", g24kw + ':> 24 KW', cc + ': Cannot Calculate'],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#fa4252','#448AFF', '#f0a500','#28df99', '#686d76'],
				data: [
					getWholePercent(l16kw, total),
					getWholePercent(g16kwl18kw, total),
					getWholePercent(g18kwl24kw, total),
					getWholePercent(g24kw, total),
					getWholePercent(cc, total)
				]
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText(total),
		title : setTitle("REMAINING WATTAGE HEADROOM DC"),
		rotation : -0.7 * Math.PI,
		legend: {
			position: 'left',
		},
		layout: {padding: 10}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', 'rwdc');
}

function set_dc_chart(data) {
	let canvasBar = document.getElementById("DCUTIL_chart");
	let ctxBar = canvasBar.getContext('2d');

	let l40 = data.l40;
	let g40l60 = data.g40l60;
	let g60l75 = data.g60l75;
	let g75 = data.g75;
	let tbu = data.tbu;

	let total = l40 + g40l60 + g60l75 + g75 + tbu;

	let total_per = getWholePercent(l40, total) +
		getWholePercent(g40l60, total) +
		getWholePercent(g60l75, total) +
		getWholePercent(g75, total) +
		getWholePercent(tbu, total);

	let datas = {
		labels: [
				 g75 + ': Greater than 75%',
				 g60l75 +": Between 60% to 75%",
			     g40l60 +": Between 40% to 60%",
				 l40 +": Less than 40%",
				 tbu +": To be Updated"
		],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#0e49b5', '#61b15a', '#fecd1a', '#fa4252', '#686d76', '#686d76'],
				data: [
					getWholePercent(g75, total),
					getWholePercent(g60l75, total),
					getWholePercent(g40l60, total),
					getWholePercent(l40, total),
					getWholePercent(tbu, total),
				]
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText(total),
		title : setTitle("DC RECTIFIER EQUIPPED UTILIZATION"),
		rotation : -0.7 * Math.PI,
		legend: {
			position: 'left',
		},
		layout: {
			padding: {
				left: 10,
				right: 10,
				top: 10,
				bottom: 10
			}
		}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', 'dcutil');
}

function set_dc_ab_chart(data, elem, param) {
	let canvasBar = document.getElementById(elem);
	let ctxBar = canvasBar.getContext('2d');

	let val1 = data.g40;
	let val2 = data.g30to40;
	let val3 = data.g0to30;
	let tbu = stbu + data.tbu;

	let total = val1 + val2 + val3 + tbu;

	let total_per = getWholePercent(val1, total) +
		getWholePercent(val2, total) +
		getWholePercent(val3, total) +
		getWholePercent(tbu, total);

	let datas = {
		labels: [
			val1 +": Greater than 40%",
			val2 +": Between 30% to 40%",
			val3 + ': Between 0% to 30%',
			tbu +": To be Updated"
		],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: [
					'#fa4252',
					'#fecd1a',
					'#54e346',
					'#686d76'],
				data: [
					getWholePercent(val1, total),
					getWholePercent(val2, total),
					getWholePercent(val3, total),
					getWholePercent(tbu, total),
				]
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText(total),
		title : param.title,
		rotation : -0.7 * Math.PI,
		legend: {
			position: param.pos,
			labels: {
				boxWidth: param.box,
				padding: param.pad
			},
		},
		layout: {padding: 10}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', param.site);
}

function set_dc_bc_chart(data, elem, param) {
	let canvasBar = document.getElementById(elem);
	let ctxBar = canvasBar.getContext('2d');

	let val1 = data.g75;
	let val2 = data.g60to75;
	let val3 = data.g0to60;
	let tbu = data.tbu;
	let sc = data.sc;

	let total = val1 + val2 + val3 + tbu + sc;

	let total_per = getWholePercent(val1, total) +
		getWholePercent(val2, total) +
		getWholePercent(val3, total) +
		getWholePercent(tbu, total) + 
		getWholePercent(sc, total)
		;

	let datas = {
		labels: [
			val1 +": Greater than 75%",
			val2 +": Between 60% to 75%",
			val3 + ': Between 0% to 60%',
			tbu +": To be Updated",
			sc +": Small Cell"
		],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: [
					'#fa4252',
					'#fecd1a',
					'#54e346',
					'#686d76',
					'#f21170'
				],
				data: [
					getWholePercent(val1, total),
					getWholePercent(val2, total),
					getWholePercent(val3, total),
					getWholePercent(tbu, total),
					getWholePercent(sc, total),
				]
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText(total),
		title : param.title,
		rotation : -0.7 * Math.PI,
		legend: {
			position: param.pos,
			labels: {
				boxWidth: param.box,
				padding: param.pad
			},
		},
		layout: {padding: 10}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', param.site);
}

function set_cabin_type(data) {
	let canvasBar = document.getElementById("CT_chart");
	let ctxBar = canvasBar.getContext('2d');

	let ind = data['INDOOR'];
	let out = data['OUTDOOR'];
	let indout = data['INDOOR-OUTDOOR'];
	let na = data['NA'];
	let tbu = data['To be Updated'];
	let total = ind + out + indout + na + tbu;

	let total_per = getWholePercent(ind, total) +
		getWholePercent(out, total) +
		getWholePercent(indout, total) +
		getWholePercent(na, total) +
		getWholePercent(tbu, total);

	let datas = {
		labels: [ind +": Indoor", out +": Outdoor", indout +": Indoor-Outdoor", na +": None", tbu + ": To be updated"],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#0e49b5', '#54e346', '#fddb3a', '#1b262c', '#686d76'],
				data: [
					getWholePercent(ind, total),
					getWholePercent(out, total),
					getWholePercent(indout, total),
					getWholePercent(na, total),
					getWholePercent(tbu, total),
				]
			}
		]
	};

	let optionsPie = {
		plugins  : setPlugins(),
		elements : centerText(total),
		title : setTitle("CABIN TYPE"),
		rotation : -0.7 * Math.PI,
		legend: {
			position: 'left',
		},
		layout: {
			padding: {
				left: 10,
				right: 10,
				top: 10,
				bottom: 10
			}
		}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', 'ctt');
}

$(document).on('change', '#geo_tower', function(e) {
	e.preventDefault();
	geo_tower = $(this).val();
	get_chart(geo_tower,geo_power, sc, status);
});

$(document).on('change', '#sc', function(e) {
	e.preventDefault();
	sc = $(this).val();
	get_chart(geo_tower,geo_power, sc, status);
});
