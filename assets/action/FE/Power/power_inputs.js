let inputs = [
	{
		basic : [
			{
				'title' : 'PLA ID',
				'name' : 'PLAID',
				'type'   : 'text',
				'required'  : 'required',
				'col' : 5,
				'ons' : ''
			},
			{
				'title' : 'Site Name',
				'name' : 'SITENAME',
				'type'   : 'text',
				'required'  : 'required',
				'col' : 7,
				'ons' : ''
			},
			{
				'title' : 'Site Type',
				'name' : 'SITE_TYPE',
				'type'   : 'select',
				'required'  : 'required',
				'drowpdown' : {
					'GREENFIELD' : 'Greenfield',
					'ROOFTOP' : 'Rooftop',
					'IBS': 'IBS',
					'COW LEGO TEMPO' : 'Cow lego Tempo',
					'SMALL CELL': 'Small Cell',
					'OD COLOC TO IBS' : 'OD Coloc to IBS',
					'SWAT' : 'Swat',
					'MACRO' : 'Macro',
					'to be updated' : 'To be updated'
				},
				'col' : 6
			},
			{
				'title' : 'Site Class',
				'name' : 'SITE_CLASS',
				'type'   : 'select',
				'required'  : 'required',
				'drowpdown' : {
					'A1' :'A1',
					'A2' :'A2',
					'B1' :'B1',
					'B2' :'B2',
					'C1' :'C1',
					'C2' :'C2',
					'C3' :'C3',
					'C4' :'C4',
					'C5' :'C5',
				},
				'col' : 6
			},
			{
				'title' : 'Grid Status',
				'name' : 'GRID_STATUS',
				'type'   : 'select',
				'required'  : 'required',
				'drowpdown' : {
					'G1' :'G1',
					'G2' :'G2',
					'G3' :'G3',
					'G4' :'G4',
					'Small Cell' : 'Small Cell',
					'Prime' : 'Prime'
				},
				'col' : 6
			},
			{
				'title' : 'Cabin Type',
				'name' : 'CABIN_TYPE',
				'type'   : 'select',
				'required'  : 'required',
				'drowpdown' : {
					'Indoor' :'Indoor',
					'Outdoor' :'Outdoor',
					'Indoor-Outdoor' :'Indoor-Outdoor',
					'NA' :'NA',
					'To be updated' :'To be updated'
				},
				'col' : 6
			},
			{
				'title' : 'Power Connection',
				'name' : 'POWER_CONNECTION',
				'type'   : 'select',
				'required'  : 'required',
				'drowpdown' : {
					'Genset Prime' :'Genset Prime',
					'Sole Transformer' :'Sole Transformer',
					'Tapped to Building' :'Tapped to Building',
					'Tapped to Neighbor' : 'Tapped to Neighbor',
					'Tapped to Genset' : 'Tapped to Genset',
					'Tempo to Coop' : 'Tempo to Coop',
					'Shared Transformer' : 'Shared Transformer',
					'No Power' : 'No Power',
					'Small Cell' : 'Small Cell',
					'To be updated' : 'To be updated'
				},
				'col' : 12
			},
			{
				'title' : 'Phase',
				'name' : 'POWER_PHASE',
				'type'   : 'select',
				'required'  : 'required',
				'drowpdown' : {
					'Single Phase' :'Single Phase',
					'Three Phase' :'Three Phase',
				},
				'col' : 6
			},
			{
				'title' : 'Cabin Count',
				'name' : 'CABIN_QUANTITY',
				'type'   : 'select',
				'required'  : 'required',
				'drowpdown' : {
					0 :'0',
					1 :'1',
					2 :'2',
					3 :'3'
				},
				'col' : 6
			},
			{
				'title' : 'Transformer Capacity',
				'name' : 'TRANSFORMER_CAPACITY_KVA',
				'type'   : 'number',
				'required'  : 'required',
				'col' : 6,
				'ons' : '.KVA'
			},
			{
				'title' : 'Genset Capacity',
				'name' : 'GENSET_CAPACITY_KVA',
				'type'   : 'number',
				'required'  : 'required',
				'col' : 6,
				'ons' : '.KVA'
			},
			{
				'title' : 'MDB / ECB ',
				'name' : 'MDP_ECB_RATING_AT',
				'type'   : 'number',
				'required'  : 'required',
				'col' : 6,
				'ons' : ''
			},
			{
				'title' : 'ACPDB Rating ',
				'name' : 'ACPDB_RATING_AT',
				'type'   : 'number',
				'required'  : 'required',
				'col' : 6,
				'ons' : ''
			},
			{
				'title' : 'Drop Wires ',
				'name' : 'DROP_WIRES',
				'type'   : 'select',
				'required'  : 'required',
				'col' : 12,
				'ons' : ''
			}
		],
		load: [
			{
				'title' : 'ACU Quantity',
				'name' : 'ACU_QUANTITY',
				'type'   : 'number',
				'required'  : 'required',
				'col' : 2,
				'ons' : ''
			},

			{
				'title' : 'ACU Capacity Model & (HP)',
				'name' : 'ACU_CAPACITY_HP',
				'type'   : 'select',
				'required'  : 'required',
				'col' : 2,
				// 'ons' : '.HP'
			},

			{
				'title' : 'ACU Type',
				'name' : 'ACU_TYPE',
				'type'   : 'text',
				'col' : 2,
				'ons' : '',
				// 'readonly' : 'readonly'
			},

			{
				'title' : 'ACU Approx Load',
				'name' : 'ACU_APPROX_LOAD',
				'type'   : 'number',
				'required'  : 'required',
				'col' : 2,
				'ons' : '',
				// 'readonly' : 'readonly'
			},

			{
				'title' : 'Other AC Load',
				'name' : 'OTHER_AC_LOAD_AAC',
				'type'   : 'number',
				'required'  : 'required',
				'col' : 2,
				'ons' : '',
				// 'readonly' : 'readonly'
			},

			{
				'title' : 'ECB Clamp Reading(AAC)',
				'name' : 'ECB_CLAMP_READING_AAC',
				'type'   : 'number',
				'required'  : 'required',
				'col' : 2,
				'ons' : ''
			},


		],
		// others: [
		// 	{
		// 		'title' : 'Rectifier 1',
		// 		'name' : 'RECTIFIER_1'
		// 	},

		// ]
	}
];
let tech = [
	{
		'id' : 0,
		'name' : 'G9',
		'key' : 'G900'
	},
	{
		'id' : 1,
		'name' : 'G18',
		'key' : 'G1800'
	},
	{
		'id' : 2,
		'name' : 'U21',
		'key' : 'U2100'
	},
	{
		'id' : 3,
		'name' : 'U21 DB',
		'key' : 'U21DB'
	},
	{
		'id' : 4,
		'name' : 'L7',
		'key' : 'L700'
	},
	{
		'id' : 5,
		'name' : 'L18',
		'key' : 'L1800'
	},
	{
		'id' : 6,
		'name' : 'L18 DB',
		'key' : 'L18DB'
	},
	{
		'id' : 7,
		'name' : 'L21',
		'key' : 'L2100'
	},
	{
		'id' : 8,
		'name' : 'L23',
		'key' : 'L2300'
	},
	{
		'id' : 9,
		'name' : 'L26',
		'key' : 'L2600'
	},
	{
		'id' : 10,
		'name' : 'L26 MM',
		'key' : 'L26MM'
	},
	{
		'id' : 11,
		'name' : 'NR26',
		'key' : 'nr26'
	},
	{
		'id' : 12,
		'name' : 'BBU 5G',
		'key' : 'BBU5G'
	},
];
let rs1 = [
		{
			'title' : 'Rectifier 1 <b class="rs1-tag">' + '</b>',
			'name' : 'RECTIFIER_1',
			'type'   : 'select',
			'drowpdown' : {
				'APM30H' :'APM30H',
				'ELTEK FP 2' :'ELTEK FP 2',
				'EMERSON 1500W' :'EMERSON 1500W',
				'EMERSON 2000' :'EMERSON 2000',
				'EMERSON 2900' :'EMERSON 2900',
				'EMERSON 3200' :'EMERSON 3200',
				'EUL RECTIFIER SYSTEM' :'EUL RECTIFIER SYSTEM',
				'HUAWEI MTS3000' :'HUAWEI MTS3000',
				'HUAWEI TP48200A' :'HUAWEI TP48200A',
				'HUAWEI TP48300B' :'HUAWEI TP48300B'
			},
			'col' : 12,
			'ons' : ''
		},

		{
			'title' : 'RS Module Capacity',
			'name' : 'RS1_MODULE_CAPACITY',
			'type'   : 'text',
			'required'  : 'required',
			'col' : 12,
			'ons' : ''
		},

		{
			'title' : 'No. of Modules',
			'name' : 'RS1_NO_OF_MODULES',
			'type'   : 'number',
			'required'  : 'required',
			'col' : 6,
			'ons' : ''
		},

		{
			'title' : 'No. of Emp Modules',
			'name' : 'RS1_NO_OF_EMPTY_MODULE',
			'type'   : 'number',
			'required'  : 'required',
			'col' : 6,
			'ons' : '',
			'readonly' : 'readonly'
		},

		{
			'title' : 'RS1 Actual Rectifier Load',
			'name' : 'RS1_ACTUAL_LOAD_ADC',
			'type'   : 'number',
			'required'  : 'required',
			'col' : 12,
			'ons' : ''
		},

		{
			'title' : 'Battery Brand',
			'name' : 'RS1_BATTERY_BRAND',
			'type'   : 'select',
			'drowpdown' : {
				'ACME' :'ACME',
				'AGISSON' :'AGISSON',
				'BSB' :'BSB',
				'COSLIGHT' :'COSLIGHT',
				'EMERSON' :'EMERSON',
				'FIAMM' :'FIAMM',
				'FISCHER' :'FISCHER',
				'LEOCH' :'LEOCH',
				'MONOLITE' :'MONOLITE',
				'NARAD/SHOTO' :'NARAD/SHOTO',
				'NARADA' :'NARADA',
				'NARADA/SACRED SUN' :'NARADA/SACRED SUN',
				'NARADA/SHOTO' :'NARADA/SHOTO',
				'OLONG(VRLA)' :'OLONG(VRLA)',
				'OLONG/NARADA' :'OLONG/NARADA',
				'OLONG/SHOTO' :'OLONG/SHOTO',
				'RITAR' :'RITAR',
				'RITAR ACME' :'RITAR ACME',
				'RITAR/BSB' :'RITAR/BSB',
				'SACRED SUN' :'SACRED SUN',
				'SHOTO' :'SHOTO',
				'SHOTO&FISHCER' :'SHOTO&FISHCER',
				'SHOTO/EMERSON' :'SHOTO/EMERSON',
				'SHOTO/NARADA' :'SHOTO/NARADA',
				'SHOTO/RITAR' :'SHOTO/RITAR',
				'TELION' :'TELION',
				'VISION' :'VISION',
				'VISION&NARADA' :'VISION&NARADA',
				'VISION/IAMM' :'VISION/FIAMM',
				'WOLONG' :'WOLONG',
			},
			'col' : 12,
			'ons' : ''
		},

		{
			'title' : 'RS1 Battery Type',
			'name' : 'RS1_BATTERY_TYPE',
			'type'   : 'select',
			'drowpdown' : {
				'VRLA' :'VRLA',
				'Lithium' :'Lithium',
			},
			'col' : 12,
			'ons' : ''
		},

		{
			'title' : 'RS1 Battery Capacity',
			'name' : 'RS1_BATTERY_CAPACITY',
			'type'   : 'text',
			'required'  : 'required',
			'col' : 12,
			'ons' : ''
		},
	];
let rs2 = [
	{
		'title' : 'Rectifier 2',
		'name' : 'RECTIFIER_2',
		'type'   : 'select',
		'drowpdown' : {
			'APM30H' :'APM30H',
			'ELTEK FP 2' :'ELTEK FP 2',
			'EMERSON 1500W' :'EMERSON 1500W',
			'EMERSON 2000' :'EMERSON 2000',
			'EMERSON 2900' :'EMERSON 2900',
			'EMERSON 3200' :'EMERSON 3200',
			'EUL RECTIFIER SYSTEM' :'EUL RECTIFIER SYSTEM',
			'HUAWEI MTS3000' :'HUAWEI MTS3000',
			'HUAWEI TP48200A' :'HUAWEI TP48200A',
			'HUAWEI TP48300B' :'HUAWEI TP48300B'
		},
		'col' : 12,
		'ons' : ''
	},

	{
		'title' : 'RS Module Capacity',
		'name' : 'RS2_MODULE_CAPACITY',
		'type'   : 'text',
		'required'  : 'required',
		'col' : 12,
		'ons' : ''
	},

	{
		'title' : 'RS2 No. of Modules',
		'name' : 'RS2_NO_OF_MODULES',
		'type'   : 'number',
		'required'  : 'required',
		'col' : 6,
		'ons' : ''
	},

	{
		'title' : 'No. of Emp Modules',
		'name' : 'RS2_NO_OF_EMPTY_MODULE',
		'type'   : 'number',
		'required'  : 'required',
		'col' : 6,
		'ons' : '',
		'readonly' : 'readonly'
	},

	{
		'title' : 'RS2 Actual Rectifier Load',
		'name' : 'RS2_ACTUAL_LOAD_ADC',
		'type'   : 'number',
		'required'  : 'required',
		'col' : 12,
		'ons' : ''
	},

	{
		'title' : 'RS2 Battery Brand',
		'name' : 'RS2_BATTERY_BRAND',
		'type'   : 'select',
		'drowpdown' : {
			'ACME' :'ACME',
			'AGISSON' :'AGISSON',
			'BSB' :'BSB',
			'COSLIGHT' :'COSLIGHT',
			'EMERSON' :'EMERSON',
			'FIAMM' :'FIAMM',
			'FISCHER' :'FISCHER',
			'LEOCH' :'LEOCH',
			'MONOLITE' :'MONOLITE',
			'NARAD/SHOTO' :'NARAD/SHOTO',
			'NARADA' :'NARADA',
			'NARADA/SACRED SUN' :'NARADA/SACRED SUN',
			'NARADA/SHOTO' :'NARADA/SHOTO',
			'OLONG(VRLA)' :'OLONG(VRLA)',
			'OLONG/NARADA' :'OLONG/NARADA',
			'OLONG/SHOTO' :'OLONG/SHOTO',
			'RITAR' :'RITAR',
			'RITAR ACME' :'RITAR ACME',
			'RITAR/BSB' :'RITAR/BSB',
			'SACRED SUN' :'SACRED SUN',
			'SHOTO' :'SHOTO',
			'SHOTO&FISHCER' :'SHOTO&FISHCER',
			'SHOTO/EMERSON' :'SHOTO/EMERSON',
			'SHOTO/NARADA' :'SHOTO/NARADA',
			'SHOTO/RITAR' :'SHOTO/RITAR',
			'TELION' :'TELION',
			'VISION' :'VISION',
			'VISION&NARADA' :'VISION&NARADA',
			'VISION/IAMM' :'VISION/FIAMM',
			'WOLONG' :'WOLONG',
		},
		'col' : 12,
		'ons' : ''
	},

	{
		'title' : 'RS2 Battery Type',
		'name' : 'RS2_BATTERY_TYPE',
		'type'   : 'select',
		'drowpdown' : {
			'VRLA' :'VRLA',
			'Lithium' :'Lithium',
		},
		'col' : 12,
		'ons' : ''
	},

	{
		'title' : 'RS2 Battery Capacity',
		'name' : 'RS2_BATTERY_CAPACITY',
		'type'   : 'text',
		'required'  : 'required',
		'col' : 12,
		'ons' : ''
	},
];
let rs3 = [
			{
				'title' : 'Rectifier 3',
				'name' : 'RECTIFIER_3',
				'type'   : 'select',
				'drowpdown' : {},
				'col' : 12,
				'ons' : ''
			},

			{
				'title' : 'RS Module Capacity',
				'name' : 'RS3_MODULE_CAPACITY',
				'type'   : 'text',
				'required'  : 'required',
				'col' : 12,
				'ons' : ''
			},

			{
				'title' : 'No. of Modules',
				'name' : 'RS3_NO_OF_MODULES',
				'type'   : 'number',
				'required'  : 'required',
				'col' : 6,
				'ons' : ''
			},

			{
				'title' : 'No. of Emp Modules',
				'name' : 'RS3_NO_OF_EMPTY_MODULE',
				'type'   : 'number',
				'required'  : 'required',
				'col' : 6,
				'ons' : '',
				'readonly' : 'readonly'
			},

			{
				'title' : 'RS 3 Actual Rectifier Load',
				'name' : 'RS3_ACTUAL_LOAD_ADC',
				'type'   : 'number',
				'required'  : 'required',
				'col' : 12,
				'ons' : ''
			},

			{
				'title' : 'RS3 Battery Brand',
				'name' : 'RS3_BATTERY_BRAND',
				'type'   : 'select',
				'drowpdown' : {},
				'col' : 12,
				'ons' : ''
			},

			{
				'title' : 'RS3 Battery Type',
				'name' : 'RS3_BATTERY_TYPE',
				'type'   : 'select',
				'drowpdown' : {
					'VRLA' :'VRLA',
					'Lithium' :'Lithium',
				},
				'col' : 12,
				'ons' : ''
			},

			{
				'title' : 'RS3 Battery Capacity',
				'name' : 'RS3_BATTERY_CAPACITY',
				'type'   : 'text',
				'required'  : 'required',
				'col' : 12,
				'ons' : ''
			},
		];

let sitesdatatable;
let tblcolumns=[];
let nowblinking;
let clickedoperation="";
let activeoperation="";
let selectedtr = [];
let selectallflag=0;
let tdcolindex=0;
let section;
let textfinal='';
let textinitial=null;
let rowid = null;
let fieldid = null;
let tablecat = null;
let dataset=[];
let coldetails = [];
let power_tbl = $('#power_tbl');

$(function () {
	set_inputs('basic', inputs[0].basic);
	set_inputs('util', inputs[0].util);
	set_inputs('rs1', rs1);
	set_inputs('load', inputs[0].load);

	set_exist_tech();
	get_power_data();
	get_rec_data();
	get_bat_data();
	get_equip_data();
	get_completion();
	set_proposal();
});

function set_inputs(class_name, data) {
	let html = '';
	_.forEach(data, function (val) {
		html += '<div class="col-'+val.col+'">' +
					'<div class="form-group mb-0">\n';
						html += '<small for='+val.name+'>'+val.title+'</small>';
						if (val.type === 'select') {
							html += '<select class="custom-select form-control custom-select-sm w-100" id='+val.name+' name ='+val.name+'>';
										_.forEach(val.drowpdown, function (val1,key) {
											html += '<option value="'+key+'">'+val1+'</option>';
										});
							html += '</select>';
						} else {
							html += val.ons !== '' ? ' <div class="input-group input-group-sm mb-2 mr-sm-2">\n' : '';
								html += ' <input onchange="setTwoNumberDecimal" min="0" max="100000" step = "any" type="'+val.type+'" name='+val.name+' class="form-control form-control-sm" id='+val.name+' placeholder="Enter '+val.title+'" '+val.required+' '+val.only+' '+val.readonly+'>\n';
								if (val.ons !== '') {
									html += '<div class="input-group-append">\n' +
										'      <div class="input-group-text">'+val.ons+'</div>\n' +
										'    </div>\n';
								}
							html += val.ons !== '' ? ' </div>' : '';
						}
					html += '</div>'+
				'</div>';
	});

	$('.'+class_name).html(html);
}

function setTwoNumberDecimal(event) {
	this.value = parseFloat(this.value).toFixed(2);
}

function set_exist_tech() {
	let html = '';
	_.forEach(tech, function (val, key) {
		html += '<tr>' +
			'<td>' +
			'<div class="custom-control custom-checkbox">' +
			'<input type="checkbox" class="custom-control-input check-tbl-selected" id="check-select-'+key+'">' +
			'<label class="custom-control-label" for="check-select-'+key+'"></label>' +
			'</div>' +
			'</td>' +
			'<td>'+val.name+'</td>' +
			'</tr>';
	});
	$('.exist_techs').html(html);
}


$(document).on('change', '#power_connection', function (e) {
	e.preventDefault();
	let condition = $(this).val() === 'Tempo Tapped';
	$('#transformer').val(condition ? 'No Transformer' : '').attr('readonly', condition);
});

$(document).on('click', '.edit', function (e) {
	e.preventDefault();
	$('#edit').modal('show');
});


