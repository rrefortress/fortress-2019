let tabs = [
	{
		name: 'Master Site',
		status : 'active',
		id_name: 'ms',
		icon : 'fab fa-mdb',
	},
	{
		name: 'Tower',
		status : '',
		id_name: 'tower',
		icon : 'fas fa-broadcast-tower',
	},
	{
		name: 'Power',
		status : '',
		id_name: 'power',
		icon : 'fas fa-power-off',
	}
];

let charts = {
		master_sites : [
			{
				id : 2,
				name: 'outdoor',
			},
		],
		tower : [
			{
				id: 4,
				title: 'Greenfield Site',
				name: "gf"
			},
			{
				id: 5,
				title: 'Rooftop Site',
				name: "rt"
			},
			{
				id: 6,
				title: 'Tower Capacity',
				name: "tc"
			},
			{
				id: 7,
				title: 'NSCP Compliance',
				name: "nscp"
			},
		],
		power : [
			{
				id: 5,
				name: 'SC',
				title: 'Site Class'
			},
			{
				id: 2,
				title: 'AC Capacity',
				name: "ac"
			},
			{
				id: 1,
				title: 'Power Connection',
				name: "pc"
			},
			{
				id: 2,
				name: 'GS',
				title: 'Grid Status',
			},
			{
				id: 3,
				name: 'RS',
				title: 'Battery Hour',
			},
			{
				id: 5,
				name: 'REC',
				title: 'Rectifier Module',
			},
			{
				id: 5,
				name: 'trc',
				title: 'Transformer Capacity',
			},
			{
				id: 5,
				name: 'gnc',
				title: 'Genset Capacity',
			},
			{
				id: 6,
				name: 'DCAB',
				title: 'DC UTIL',
			},
			{
				id: 6,
				name: 'DCBC',
				title: 'DC UTIL',
			},
			{
				id: 6,
				name: 'RSS',
				title: 'Rectifier',
			},
			{
				id: 3,
				name: 'CT',
				title: 'Cabin Type',
			},
		],
};
let func_loops = [
	{
		name : charts.master_sites,
		col : 'col-12',
		id_name: 'ms'
	},
	{
		name : charts.tower,
		col : 'col-6',
		id_name: 'tower'
	},
	{
		name : charts.power,
		col : 'col-6',
		id_name: 'power'
	}
];
let labels = [
	{
		'id'         : 1,
		'name'       : 'Tower',
		'value' 	 : 0,
		'icon'       : 'fas fa-broadcast-tower',
		'color'      : 'bg-danger',
	},
	{
		'id'         : 2,
		'name'       : 'Power',
		'value' 	 : 0,
		'icon'       : 'fas fa-power-off',
		'color'      : 'bg-success'
	},
];
let regions = [
	{
		id: 1,
		name: 'Mindanao',
		color: 'u-purple',
	},
	{
		id: 1,
		name: 'National Capital Region',
		color: 'bg-primary',
	},
	{
		id: 2,
		name: 'North Luzon',
		color: 'bg-success',
	},
	{
		id: 1,
		name: 'South Luzon',
		color: 'bg-danger',
	},
	{
		id: 1,
		name: 'Visayas',
		color: 'u-orange',
	},

];
let status = '';
let sc = '';
let geo_tower = '';
let geo_power = '';

let tcount = 0;
let pcount = 0;
$(function () {
	set_tabs();
	set_panel_project();
	_.forEach(func_loops, function (val) {set_main_charts(val.name, val.col, val.id_name);});
	get_chart(geo_tower,geo_power, sc, status);
});

function get_chart(geo_tower = '', geo_power = '', sc = '', status = '') {
	let chart = $('.chart-loader');
	chart.find('div.c-dis').addClass('d-none');
		let param = {
		url: url + 'get_chart',
		dataType: 'json',
		data: {geo_tower: geo_tower,geo_power: geo_power, sc : sc, status: status},
		type: 'POST',
		beforeSend: chart.append('<div class="col-12 c-load" style="margin-top: 5% !important;">' +
				'<h5 class="text-center text-white">' +
				'<img src='+loaders+' width="120" height="120" alt="loader" class="rounded-circle"><br>\n' +
				'<em class="text-muted small text-process">Loading Chart</em>' +
				'</h5>' +
				'</div>')
	};

	fortress(param).then( (data) => {
		chart.find('div.c-load').remove();
		chart.find('div.c-dis').removeClass('d-none');
		set_panel_project(data.master.project);

		_.forEach(['outdoor_chart', 'dash_st_chart'], function (val, key) {
			let param = {
				pos : key === 0 ? 'left' : 'bottom',
				site : key === 0 ? 'od' : 'sod',
				box : key === 0 ? 40 : 10,
				pad : key === 0 ? 10 : 10,
				// legend: key === 0,
				legend: true,
				title : key === 0 ? setTitle("SITE TYPE") : '',
			}
			set_st_chart(data.master.site_type, val, param);
		});

		_.forEach(['gf_chart', 'dash_gf_chart'], function (val, key) {
			let param = {
				pos : key === 0 ? 'left' : 'bottom',
				site : key === 0 ? 'gff' : 'sgff',
				box : key === 0 ? 40 : 10,
				pad : key === 0 ? 10 : 7,
				title : key === 0 ? setTitle("GREENFIELD SITE") : '',
			}
			set_gf_chart(data.tower.gf, val, param);
		});

		_.forEach(['rt_chart', 'dash_rt_chart'], function (val, key) {
			let param = {
				pos : key === 0 ? 'left' : 'bottom',
				site : key === 0 ? 'rts' : 'srts',
				box : key === 0 ? 40 : 10,
				pad : key === 0 ? 10 : 7,
				title : key === 0 ? setTitle("ROOFTOP SITE") : '',
			}
			set_rt_chart(data.tower.rt, val, param);
		});

		_.forEach(['tc_chart', 'dash_tc_chart'], function (val, key) {
			let param = {
				pos : key === 0 ? 'left' : 'bottom',
				site : key === 0 ? 'tcc' : 'stcc',
				box : key === 0 ? 40 : 10,
				pad : key === 0 ? 10 : 10,
				title : key === 0 ? setTitle("TOWER CAPACITY") : '',
			}
			set_tc_chart(data.tower.tc, val, param);
		});

		_.forEach(['nscp_chart', 'dash_nscp_chart'], function (val, key) {
			let param = {
				pos : key === 0 ? 'left' : 'bottom',
				site : key === 0 ? 'nscp' : 'snscp',
				box : key === 0 ? 40 : 10,
				pad : key === 0 ? 10 : 7,
				title : key === 0 ? setTitle("NSCP 7 COMPLIANCE") : '',
			}
			set_nscp_chart(data.tower.nscp, val, param);
		});

		set_pc_chart(data.power.pc);

		// _.forEach(['pc_chart', 'dash_pcn_chart'], function (val, key) {
		// 	let param = {
		// 		pos : key === 0 ? 'left' : 'bottom',
		// 		site : key === 0 ? 'pc' : 'pcn',
		// 		box : key === 0 ? 40 : 10,
		// 		pad : key === 0 ? 10 : 7,
		// 		title : key === 0 ? setTitle("Power Connection") : '',
		// 		dkey : key,
		// 	}
		// 	set_pc_chart(data.power.pc, val, param);
		// });

		_.forEach(['ac_chart', 'dash_ac_chart'], function (val, key) {
			let param = {
				pos : key === 0 ? 'left' : 'bottom',
				site : key === 0 ? 'ac' : 'sac',
				box : key === 0 ? 40 : 10,
				pad : key === 0 ? 10 : 7,
				title : key === 0 ? setTitle("AC POWER CAPACITY") : '',
				dkey : key,
			}
			set_ac_chart(data.power.ac, val, param);
		});

		_.forEach(['RS_chart', 'dash_RS_chart'], function (val, key) {
			let param = {
				pos : key === 0 ? 'left' : 'bottom',
				site : key === 0 ? 'bat' : 'sbat',
				box : key === 0 ? 40 : 10,
				pad : key === 0 ? 10 : 10,
				title : key === 0 ? setTitle("BATTERY BACK-UP TIME") : '',
				dkey : key,
			}
			set_bat_chart(data.power.bat, val, param);
		});


		_.forEach(['REC_chart', 'dash_REC_chart'], function (val, key) {
			let param = {
				pos : key === 0 ? 'left' : 'bottom',
				site : key === 0 ? 'rec' : 'srec',
				box : key === 0 ? 40 : 10,
				pad : key === 0 ? 10 : 10,
				title : key === 0 ? setTitle("DC RECTIFIER EQUIPPED CAPACITY") : '',
				dkey : key,
			}
			set_dc_rec_chart(data.power.dc, val, param);
		});

		_.forEach(['SC_chart', 'dash_SC_chart'], function (val, key) {
			let param = {
				pos : key === 0 ? 'left' : 'bottom',
				site : key === 0 ? 'scc' : 'sscc',
				box : key === 0 ? 40 : 10,
				pad : key === 0 ? 10 : 7
			}
			set_sc_chart(data.power.site_class, val, param);
		});


		set_gs_chart(data.power.grid_status);
		set_cabin_type(data.power.ct);
		set_rs_chart(data.power.rec_site);

		_.forEach(['DCAB_chart', 'dash_DCAB_chart'], function (val, key) {
			let param = {
				pos : key === 0 ? 'left' : 'bottom',
				site : key === 0 ? 'dcab' : 'sdcab',
				box : key === 0 ? 40 : 10,
				pad : key === 0 ? 10 : 7,
				title : key === 0 ? setTitle("CLASS A - B1 RECTIFIER UTILIZATION") : '',
			}
			set_dc_ab_chart(data.power.dc_ab, val, param);
		});

		_.forEach(['DCBC_chart', 'dash_DCBC_chart'], function (val, key) {
			let param = {
				pos : key === 0 ? 'left' : 'bottom',
				site : key === 0 ? 'dcbc' : 'sdcbc',
				box : key === 0 ? 40 : 10,
				pad : key === 0 ? 10 : 7,
				title : key === 0 ? setTitle("CLASS B2 - C RECTIFIER UTILIZATION") : '',
			}
			set_dc_bc_chart(data.power.dc_bc, val, param);
		});

		// set_dc_bc_chart(data.power.dc_bc);

		set_cc_chart(data.power.gnc, 'gnc', 'GENSET');
		set_trf_chart(data.power.trc, 'trc', 'TRANSFORMER', 'Total Counts for Sole & Shared Transformer');
	});
}

function set_tabs() {
	let tab = '';
	let content = '';
	_.forEach(tabs, function (val) {
		tab += '<li class="nav-item" role="presentation">\n' +
			'<a class="nav-link ' + val.status + '" id="' + val.id_name + '-tab" data-toggle="tab" href="#' + val.id_name + '" role="tab" aria-controls="' + val.id_name + '" aria-selected=""><i class="' + val.icon + '"></i> ' + val.name + '</a>' +
			'</li>';

		content += '<div class="tab-pane fade show ' + val.status + '" id="' + val.id_name + '" role="tabpanel" aria-labelledby="' + val.id_name + '-tab">' +
			'<div class="container-fluid mg-top-5" style="height: 67vh;overflow: auto">';
		content += '<form class="form-inline d-flex justify-content-start">\n';
		// if ($('#geo').val() === 'nat') {
		// 	content += '<div class="form-group">\n' +
		// 		'    <label for="geo" class="small">Filter Region:</label>\n' +
		// 		'     <select class="custom-select custom-select-sm mx-sm-2" id="geo_' + val.id_name + '">\n' +
		// 		'        <option value="" selected>All</option>\n' +
		// 		'        <option value="nlz">NLZ</option>\n' +
		// 		'        <option value="slz">SLZ</option>\n' +
		// 		'        <option value="ncr">NCR</option>\n' +
		// 		'        <option value="min">MIN</option>\n' +
		// 		'        <option value="vis">VIS</option>\n' +
		// 		'      </select>' +
		// 		'  </div>\n';
		// }
		if (val.name === 'Power') {
			content += '<div class="form-group">\n' +
				'    <label for="sc" class="small">Filter Site Class:</label>\n' +
				'    <select class="custom-select custom-select-sm mx-sm-2" id="sc">\n' +
				'        <option value="" selected>All</option>\n' +
				'        <option value="A">A</option>\n' +
				'        <option value="B">B</option>\n' +
				'        <option value="C">C</option>\n' +
				'        <option value="To be Updated">To be Updated</option>\n' +
				'      </select>' +
				'  </div>\n';
		} else if (val.name === 'Tower') {
			content += '<div class="form-group">\n' +
				'    <label for="sc" class="small">Filter Status:</label>\n' +
				'    <select class="custom-select custom-select-sm mx-sm-2" id="status">\n' +
				'        <option value="" selected>All</option>\n' +
				'        <option value="ON-AIRED" >On-Aired</option>\n' +
				'      </select>' +
				'  </div>\n';
		}
		content += '</form>';

								// if (val.name === 'Dashboard') {
								// 	content += get_dash_pages();
								// } else

								if (val.name === 'Master Site') {
									content += '<div class="row pad-lef-right-0">' +
										'<div class="col-3 pad-left-0 panel-project"></div>' +
										'<div class="col-9 pad-lef-right-0 row border-left" id="' + val.id_name + '_div"></div>' +
										'</div>';
								} else {
									content += '<div id="'+val.id_name+'_div" class="row mg-top-10"></div>';
								}
							content += '</div>' +
					'</div>';
	});

	$('#fe_tabs').html(tab);
	$('#fe_tab_content').html(content);
	$('.carousel').carousel({
		interval: false
	})
}

function set_main_charts(data, col, id_name) {
	let html = '';
	_.forEach(data, function (val) {
		html += '<div class="'+col+' chart-loader border pt-1 p1">\n' +
					'<div class="chart-container c-dis d-none">\n' +
					'    <canvas height="50vh" width="120vw" id="' + val.name + '_chart"></canvas>\n' +
					'</div>' +
				'</div>';
	});
	$('#'+id_name+'_div').html(html);
}

function set_panel_project(data) {
	let html = '';
	_.forEach(labels, function (val, key) {
		let value = 0;
		if (data !== undefined) {
			switch (key) {
				case 0 :
					value = data.tower;
					tcount = data.tower;
					break;
				case 1 :
					value = data.powerdb;
					pcount = data.powerdb;
					break;
			}
		}

		html += '<div class="card border mg-top-5 shadow">' +
						'<div class="card-body p-3 '+val.color+'">' +
							'<div class="row">' +
								'<div class="col-7">'+
								'<h5 class="card-title display-6 text-white counter">'+value+'</h5>' +
								'<em class="card-subtitle text-white">in total '+val.name+'</em>' + '</div>' +
								'<div class="col-5 text-right">' +
								'<i class="text-white display-4 '+val.icon+'"></i>' +
								'</div>' +
							'</div>' +
						'</div>' +
					'</div>';
	});

	$('.panel-project').html(html);
	$('.counter').countUp();
}

$(document).on('click', '.rfe-dash', function (e) {
	e.preventDefault();

	$('.Tower-count').text(tcount);
	$('.Power-count').text(pcount);
	get_tower_completion();
	get_power_completion();

	get_tchart_completion();
	get_pchart_completion();
	$('.view-dash-rfe').modal('show');
});


function get_tower_completion() {
	let param = {
		url: url + 'get_tower_completion',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then((data) => {
		let percent = parseInt(data.total) === 0 ? 0 : getWholePercent(data.completed, data.total);
		$('.tcmp').text(data.completed + ' / ' + data.total);
		$('.tcom').text( percent + '%');
	});
}

function get_power_completion() {
	let param = {
		url: url + 'get_power_completion',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then((data) => {
		let percent = parseInt(data.total) === 0 ? 0 : getWholePercent(data.completed, data.total);

		$('.pcmp').text(data.completed + ' / ' + data.total);
		$('.pcom').text( percent + '%');
	});
}

function get_tchart_completion() {
	let param = {
		url: url + 'get_tchart_completion',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then((data) => {
		let perncr = parseInt(data.ncr.total) === 0 ? 0 : getWholePercent(data.ncr.completed, data.ncr.total);
		$('.tncr-count').html(data.ncr.completed + ' (<b>' + perncr +'%</b>)');

		let pernlz = parseInt(data.nlz.total) === 0 ? 0 : getWholePercent(data.nlz.completed, data.nlz.total);
		$('.tnlz-count').html(data.nlz.completed + ' (<b>' + pernlz +'%</b>)');

		let perslz = parseInt(data.slz.total) === 0 ? 0 : getWholePercent(data.slz.completed, data.slz.total);
		$('.tslz-count').html(data.slz.completed + ' (<b>' + perslz +'%</b>)');

		let permin = parseInt(data.min.total) === 0 ? 0 : getWholePercent(data.min.completed, data.min.total);
		$('.tmin-count').html(data.min.completed + ' (<b>' + permin +'%</b>)');

		let pervis = parseInt(data.vis.total) === 0 ? 0 : getWholePercent(data.vis.completed, data.vis.total);
		$('.tvis-count').html(data.vis.completed + ' (<b>' + pervis +'%</b>)');

		let perover = parseInt(data.overall.total) === 0 ? 0 : getWholePercent(data.overall.completed, data.overall.total);
		$('.tover-count').html(data.overall.completed + ' (<b>' + perover +'%</b>)');
	});
}

function get_pchart_completion() {
	let param = {
		url: url + 'get_pchart_completion',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then((data) => {
		let perncr = parseInt(data.ncr.total) === 0 ? 0 : getWholePercent(data.ncr.completed, data.ncr.total);
		$('.pncr-count').html(data.ncr.completed + ' (<b>' + perncr +'%</b>)');

		let pernlz = parseInt(data.nlz.total) === 0 ? 0 : getWholePercent(data.nlz.completed, data.nlz.total);
		$('.pnlz-count').html(data.nlz.completed + ' (<b>' + pernlz +'%</b>)');

		let perslz = parseInt(data.slz.total) === 0 ? 0 : getWholePercent(data.slz.completed, data.slz.total);
		$('.pslz-count').html(data.slz.completed + ' (<b>' + perslz +'%</b>)');

		let permin = parseInt(data.min.total) === 0 ? 0 : getWholePercent(data.min.completed, data.min.total);
		$('.pmin-count').html(data.min.completed + ' (<b>' + permin +'%</b>)');

		let pervis = parseInt(data.vis.total) === 0 ? 0 : getWholePercent(data.vis.completed, data.vis.total);
		$('.pvis-count').html(data.vis.completed + ' (<b>' + pervis +'%</b>)');

		let perover = parseInt(data.overall.total) === 0 ? 0 : getWholePercent(data.overall.completed, data.overall.total);
		$('.pover-count').html(data.overall.completed + ' (<b>' + perover +'%</b>)');
	});
}

$(document).on('click', '#dash-carou li', function (e) {
	e.preventDefault();

	let val = $(this).data('slide-to');
	$('#dash-carou li').removeClass('active');
	// val.addClass('active');
	
	$('.slide-'+val).addClass('active');
});