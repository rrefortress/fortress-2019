$(document).on('click', '.P5G', function (e) {
	e.preventDefault();
	$('#p_5g').modal('show');
});
Dropzone.autoDiscover = false;
let p5g = new Dropzone(".drp_5g",{
	url: url + "Planning/p5g",
	timeout: 180000,
	parallelUploads: 1,
	maxFiles: 1,
	method:"POST",
	acceptedFiles:".csv",
	paramName:"plan",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:false,
	init: function() {
		this.on("success", function(file, response) {
			let data = jQuery.parseJSON(response);
			if (data.type === 'error') {} else {
				errorSwal()
			}
			SWAL(data.type, data.title, data.msg);
			d5g.ajax.reload();
		})
	}
});

p5g.on("queuecomplete", function () {
	this.removeAllFiles();
	$('#p_5g').modal('hide');
});
p5g.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});
p5g.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});

$(document).on('click', '.dl-P5G', function (e) {
	e.preventDefault();
	remove_all ('P5G', '5g');
});