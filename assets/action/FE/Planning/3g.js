$(document).on('click', '.P3G', function (e) {
	e.preventDefault();
	$('#p_3g').modal('show');
});
Dropzone.autoDiscover = false;
let p3g = new Dropzone(".drp_3g",{
	url: url + "Planning/p3g",
	timeout: 180000,
	parallelUploads: 1,
	maxFiles: 1,
	method:"POST",
	acceptedFiles:".csv",
	paramName:"plan",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:false,
	init: function() {
		this.on("success", function(file, response) {
			let data = jQuery.parseJSON(response);
			if (data.type === 'error') {} else {
				errorSwal()
			}
			SWAL(data.type, data.title, data.msg);
			d3g.ajax.reload();
		})
	}
});

p3g.on("queuecomplete", function () {
	this.removeAllFiles();
	$('#p_3g').modal('hide');
});
p3g.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});
p3g.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});

$(document).on('click', '.dl-P3G', function (e) {
	e.preventDefault();
	remove_all ('P3G', '3g');
});