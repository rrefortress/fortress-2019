let table;
let d2g = '';
let d3g = '';
let d4g = '';
let d5g = '';
$(function () {
    let param = {
		url: url + 'get_field_keys',
		dataType: 'json',
		type: 'GET',
        beforeSend: $('.d-dis').html('<div class="col-12 mg-top-100" style="min-height:49vh">' +
			'<h5 class="text-center text-white">' +
			'<img src='+loaders+' width="150" height="150" class="rounded-circle" alt="Loader"><br>' +
			'<small class="text-center text-muted pt-4"><em>Loading data...</em></small>' +
			'</h5>' +
			'</div>')
	};
	fortress(param).then((data) => {
        $('.d-dis').html('');
        _.forEach(['P2G', 'P3G', 'P4G', 'P5G'], function(val) {
            let html = '<div class="table-responsive">' +
                            '<table id="pl_'+val+'" class="display small compact nowrap text-center table-sm responsive table-hover table-striped table-bordered mt-2" cellspacing="0" width="100%">' +
                                '<thead class="global_color text-white text-uppercase">' +
                                    '<tr class="th_'+val+'"></tr>' +
                                '</thead>' +
                                '<tbody"></tbody>' +
                            '</table>' + 
                          '</div>';

            $('.d-'+val).html(html);       
        });
       
        set_header(data.g2, 'P2G');
        set_header(data.g3, 'P3G');
        set_header(data.g4, 'P4G');
        set_header(data.g5, 'P5G');
	});
});    

function set_header(data, id) {
    let html = '';
    _.forEach(data,function(val, key){
        html += '<th>'+val+'</th>'
    });

    $('.th_'+id).html(html);
    build_datatable(id);
}

function build_datatable(id) {
	table = $('#pl_'+id).DataTable({
        "order": [],
        "select": true,
        "processing": true,
        "serverSide": true,
        "pageLength": 10,
        "paging":  true,
        "fixedHeader":true,
        "orderCellsTop": true,
        "lengthMenu": [[10, 30, 60, 100, -1], [10, 30, 60, 100, "All"]],
        "scrollY":"75vh",
        "scrollX":true,
        "ordering" : true,
        "info"     : true,
        "searching": true,
        "scrollCollapse": true,
        "ajax": {
            "url": url + 'Planning/get_'+id,
            "type": "POST"
        },
        "columnDefs": [
            { 
                "width": 200,
                "targets": [ 0 ], //first column / numbering column
                "orderable": false, //set not orderable
            },
        ],
    });

    switch (id) {
        case 'P2G': d2g = table;break;
        case 'P3G': d3g = table;break;
        case 'P4G': d4g = table;break;
        case 'P5G': d5g = table;break;
    }
}

function remove_all (tech = '', par) {
    Swal.fire({
		title: 'Are you sure?',
		text: "You want to delete all item/s?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	}).then((result) => {
		if (result.value) {
			let param = {
				url: url + 'pl_remove',
				type: "post",
				dataType: 'json',
                data: {par : par},
			};

			fortress(param).then((data) => {
				SWAL(data.type, data.title, data.msg);
                switch (tech) {
                    case 'P2G': d2g.ajax.reload();break;
                    case 'P3G': d3g.ajax.reload();break;
                    case 'P4G': d4g.ajax.reload();break;
                    case 'P5G': d5g.ajax.reload();break;
                }
			});
		}
	});
}