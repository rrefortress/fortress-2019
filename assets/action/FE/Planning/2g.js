$(document).on('click', '.P2G', function (e) {
	e.preventDefault();
	$('#p_2g').modal('show');
});
Dropzone.autoDiscover = false;
let p2g = new Dropzone(".drp_2g",{
	url: url + "Planning/p2g",
	timeout: 180000,
	parallelUploads: 1,
	maxFiles: 1,
	method:"POST",
	acceptedFiles:".csv",
	paramName:"plan",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:false,
	init: function() {
		this.on("success", function(file, response) {
			let data = jQuery.parseJSON(response);
			if (data.type === 'error') {} else {
				errorSwal()
			}
			SWAL(data.type, data.title, data.msg);
			d2g.ajax.reload();
		})
	}
});

p2g.on("queuecomplete", function () {
	this.removeAllFiles();
	$('#p_2g').modal('hide');
});
p2g.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});
p2g.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});

$(document).on('click', '.dl-P2G', function (e) {
	e.preventDefault();
	remove_all('P2G', '2g');
});