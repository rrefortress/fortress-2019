$(document).on('click', '.P4G', function (e) {
	e.preventDefault();
	$('#p_4g').modal('show');
});
Dropzone.autoDiscover = false;
let p4g = new Dropzone(".drp_4g",{
	url: url + "Planning/p4g",
	maxFilesize: 209715200,
	timeout: 180000,
	parallelUploads: 100,
	maxFiles: 1,
	method:"POST",
	acceptedFiles:".csv",
	paramName:"plan",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:false,
	init: function() {
		this.on("success", function(file, response) {
			let data = jQuery.parseJSON(response);
			if (data.type === 'error') {} else {
				errorSwal()
			}
			SWAL(data.type, data.title, data.msg);
			d3g.ajax.reload();
		})
	}
});

p4g.on("queuecomplete", function () {
	this.removeAllFiles();
	$('#p_4g').modal('hide');
});
p4g.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});
p4g.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});

$(document).on('click', '.dl-P4G', function (e) {
	e.preventDefault();
	remove_all ('P4G', '4g');
});