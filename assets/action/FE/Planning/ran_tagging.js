let rre_tabs = [
	{
		name : '2G',
		id_name : 'twoG',
		status : 'active'
	},
	{
		name : '3G',
		id_name : 'threeG',
		status : ''
	},
	{
		name : '4G',
		id_name : 'fourG',
		status : ''
	},

	{
		name : '5G',
		id_name : 'fiveG',
		status : ''
	},

	{
		name : 'Antenna Summary',
		id_name : 'ant',
		status : ''
	}
];

$(document).on('click', '.plan-tagging', function (e) {
	e.preventDefault();
	$('#plan-tagging').modal('show');
});


function set_tbl_detail_rre(data, prof = Array()) {
	if (prof.SITENAME !== undefined) {
		let html = '<div class="">' +
						'<div class="row">' +
							'<div class="col-8">' +
									'\t\t\t\t\t\t\t\t<table class="table table-bordered table-sm bg-white">\n' +
									'\t\t\t\t\t\t\t\t\t<tbody class="small">\n' +
									'\t\t\t\t\t\t\t\t\t<tr>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="2" class="text-muted text-center bg-light">Sitename</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td colspan="5" class="text-center">'+prof.SITENAME+'</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="2" class="text-muted text-center bg-light">PlA ID</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="2">'+prof.PLAID+'</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="2" class="text-muted text-center bg-light">Site Solution</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="3">'+prof.Site_Solution+'</td>\n' +
									'\t\t\t\t\t\t\t\t\t</tr>\n' +

									'\t\t\t\t\t\t\t\t\t<tr>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td colspan="2" scope="row" class="text-muted text-center bg-light">Cell Coverage</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td colspan="3" class="text-center">'+prof.CELL_COVERAGE_TYPE+'</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td colspan="2" scope="row" class="text-muted text-center bg-light">Barangay</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td colspan="3" class="text-center">'+prof.BARANGAY+'</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="3" class="text-muted text-center bg-light">Municipal / Town</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td colspan="3" class="text-center">'+prof.TOWN+'</td>\n' +
									'\t\t\t\t\t\t\t\t\t</tr>\n' +

									'\t\t\t\t\t\t\t\t\t</tr>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td colspan="2" scope="row" class="text-muted text-center bg-light">Province</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td colspan="4" class="text-center">'+prof.PROVINCE+'</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td colspan="2" scope="row" class="text-muted text-center bg-light">Region</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="3">'+prof.REGIONAL_AREA+'</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="2" class="text-muted text-center bg-light">Cluster</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="3">0</td>\n' +
									'\t\t\t\t\t\t\t\t\t</tr>'+
									'\t\t\t\t\t\t\t\t\t<tr>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="2" class="text-muted text-center bg-light">Site Coverage</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="2">'+prof.CELL_COVERAGE_TYPE+'</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="2" class="text-muted text-center bg-light">Solution Type</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="2">'+prof.Site_Solution+'</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="2" class="text-muted text-center bg-light">Vendor</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="2">'+prof.VENDOR+'</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="2" class="text-muted text-center bg-light">STATUS</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="2">'+prof.STATUS+'</td>\n' +
									'\t\t\t\t\t\t\t\t\t</tr>\n' +

									'\t\t\t\t\t\t\t\t\t<tr>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td scope="row" class="text-muted text-center bg-light" colspan="2">Coordinates</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="3" class="text-muted text-center bg-light">Latitutde</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="4">'+prof.LATITUDE+'</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td scope="row" colspan="3" class="text-muted text-center bg-light">Longtitude</td>\n' +
									'\t\t\t\t\t\t\t\t\t\t<td class="text-center" colspan="4">'+prof.LONGITUDE+'</td>\n' +
									'\t\t\t\t\t\t\t\t\t</tr>\n' +
									'\t\t\t\t\t\t\t\t\t</tbody>\n' +
									'\t\t\t\t\t\t\t\t</table>\n'+
							'</div>' +
							'<div class="col-4">' +
								'<div id="rre_map" class="shadow-sm border border-light" style="height: 100%"></div>' +
							'</div>' +
						'</div>' +
					'</div>';

				html += '<ul class="nav nav-tabs" role="tablist">\n';
					_.forEach(rre_tabs, function (val) {
						html += ' <li class="nav-item" role="presentation">\n' +
							'    <a class="nav-link '+val.status+'" id="'+val.id_name+'-tab" data-toggle="tab" href="#'+val.id_name+'" role="tab" aria-controls='+val.id_name+' aria-selected="true">'+val.name+'</a>\n' +
							'  </li>\n';
					});

				html += '</ul>\n' +

				'<div class="tab-content" id="myTabContent">\n';
					_.forEach(rre_tabs, function (val, i) {
						html += '  <div class="tab-pane rre_tabs fade show '+val.status+'" id='+val.id_name+' role="tabpanel" aria-labelledby="'+val.id_name+'-tab">';
						switch (parseInt(i)) {
							case 0  : html += set_twog_tab(data.g2); break;
							case 1  : html += set_threeg_tab(data.g3); break;
							case 2  : html += set_fourg_tab(data.g4); break;
							case 3  : html += set_fiveg_tab(data.g5); break;
						}

						html += '</div>\n';
					});
				html += '	</div>\n';
				$('.p_tagging').html(html);
				let coordinates = {lat : prof.LATITUDE,lng : prof.LONGITUDE};	
				set_map(coordinates);			
				$('.rre_tbl').dataTable({ responsive: true,"autoWidth": false});
	} else {
		$('.p_tagging').html(
			'<div class="col-12 text-center" style="margin-top: 20vh">' +
				'<p class="text-center text-white">' +
					'<i class="fas fa-sad-tear display-1 text-muted"></i><br>' +
				'</p>' +
				'<p	class="card-title text-muted" style="font-size:41px">Sorry,</p>' +
				'<p class="text-center text-muted mg-top-10">We couldn\'t  find what you\'re searching.</p>' +
			'</div>'
		);
	}
}

function set_twog_tab(data) {
	let	html = '<div class="mg-top-5">' +
		'\t\t\t\t\t\t\t\t<table class="table table-bordered table-sm table-responsive dt-responsive nowrap rre_tbl">\n' +
		' <thead class="text-muted small text-center">\n' +
		'    <tr class="global_color text-white text-uppercase">\n' +
		'      <th scope="col">No.</th>\n' +
		'      <th scope="col">BSC</th>\n' +
		'      <th scope="col">CELLNAME</th>\n' +
		'      <th scope="col">Cell ID</th>\n' +
		'      <th scope="col">Freq. Band</th>\n' +
		'      <th scope="col">ACL</th>\n' +
		'      <th scope="col">Azimuth</th>\n' +
		'      <th scope="col">M-Tilt</th>\n' +
		'      <th scope="col">E-Tilt</th>\n' +
		'      <th scope="col">Cell Coverage</th>\n' +
		'      <th scope="col">Antenna Model</th>\n' +
		'      <th scope="col">Site Type</th>\n' +
		'      <th scope="col">Local Area Code</th>\n' +
		'      <th scope="col">NE STATE</th>\n' +
		'      <th scope="col">ADMINISTRATIVE_STATE</th>\n' +
		'    </tr>\n' +
		'  </thead>' +
		'<tbody>';
		let num = 1;
		_.forEach(data,function (val) {
			html += ' <tr class="text-center small">\n' +
				'      <td>'+num+++'</td>\n' +
				'      <td>'+val.BSC+'</td>\n' +
				'      <td>'+val.CELL_NAME+'</td>\n' +
				'      <td>'+val.CELL_IDENTIFIER+'</td>\n' +
				'      <td>'+val.FREQ_BAND+'</td>\n' +				
				'      <td>'+val.ACL+'</td>\n' +
				'      <td>'+val.Azimuth+'</td>\n' +
				'      <td>'+val.Mechanical_Tilt+'</td>\n' +
				'      <td>'+val.Electrical_Tilt+'</td>\n' +
				'      <td>'+val.CELL_COVERAGE_TYPE+'</td>\n' +
				'      <td>'+val.Antenna_Type+'</td>\n' +
				'      <td>'+val.Site_Solution+'</td>\n' +
				'      <td>'+val.LAC+'</td>\n' +
				'      <td>'+val.NE_STATE+'</td>\n' +
				'      <td>'+val.ADMINISTRATIVE_STATE+'</td>\n' +
				'</tr>';
		});
	 html += '</tbody>' +
	'\t\t\t\t\t\t\t\t</table>\n'+
	'</div>';

	return html;
}

function set_threeg_tab(data) {
	let	html = '<div class="mg-top-5">' +
		'\t\t\t\t\t\t\t\t<table class="table table-bordered table-sm table-responsive dt-responsive nowrap rre_tbl">\n' +
		' <thead class="text-muted small text-center">\n' +
		'    <tr class="global_color text-white text-uppercase">\n' +
		'      <th scope="col">No.</th>\n' +
		'      <th scope="col">RNC</th>\n' +
		'      <th scope="col">CELLNAME</th>\n' +
		'      <th scope="col">Cell ID</th>\n' +
		'      <th scope="col">BAND INDICATOR</th>\n' +
		'      <th scope="col">DL PRIMARY SCRAMBLING CODE</th>\n' +
		'      <th scope="col">DL_UARFCN</th>\n' +
		'      <th scope="col">ACL</th>\n' +
		'      <th scope="col">Azimuth</th>\n' +
		'      <th scope="col">M-Tilt</th>\n' +
		'      <th scope="col">E-Tilt</th>\n' +
		'      <th scope="col">Cell Coverage</th>\n' +
		'      <th scope="col">Antenna Model</th>\n' +
		'      <th scope="col">Site Type</th>\n' +
		'      <th scope="col">Local Area Code</th>\n' +
		'      <th scope="col">Dual Beam</th>\n' +
		'      <th scope="col">NE STATE</th>\n' +
		'      <th scope="col">VALIDATION INDICATION</th>\n' +
		'    </tr>\n' +
		'  </thead>' +
		'<tbody>';
		let num = 1;
		_.forEach(data,function (val) {
			html += ' <tr class="text-center small">\n' +
				'      <td>'+num+++'</td>\n' +
				'      <td>'+val.RNC+'</td>\n' +
				'      <td>'+val.CELL_NAME+'</td>\n' +
				'      <td>'+val.CELL_IDENTIFIER+'</td>\n' +
				'      <td>'+val.BAND_INDICATOR+'</td>\n' +		
				'      <td>'+val.DL_PRIMARY_SCRAMBLING_CODE+'</td>\n' +	
				'      <td>'+val.DL_UARFCN+'</td>\n' +			
				'      <td>'+val.ACL+'</td>\n' +
				'      <td>'+val.Azimuth+'</td>\n' +
				'      <td>'+val.Mechanical_Tilt+'</td>\n' +
				'      <td>'+val.Electrical_Tilt+'</td>\n' +
				'      <td>'+val.CELL_COVERAGE_TYPE+'</td>\n' +
				'      <td>'+val.Antenna_Type+'</td>\n' +
				'      <td>'+val.Site_Solution+'</td>\n' +
				'      <td>'+val.LAC+'</td>\n' +
				'      <td>'+val.DUAL_BEAM_TAGGING+'</td>\n' +
				'      <td>'+val.NE_STATE+'</td>\n' +
				'      <td>'+val.VALIDATION_INDICATION+'</td>\n' +
				'</tr>';
		});
	 html += '</tbody>' +
	'\t\t\t\t\t\t\t\t</table>\n'+
	'</div>';

	return html;
}

function set_fourg_tab(data) {
	let	html = '<div class="mg-top-5">' +
		'\t\t\t\t\t\t\t\t<table class="table table-bordered table-sm table-responsive dt-responsive nowrap rre_tbl">\n' +
		' <thead class="text-muted small text-center">\n' +
		'    <tr class="global_color text-white text-uppercase">\n' +
		'      <th scope="col">No.</th>\n' +
		'      <th scope="col">CELLNAME</th>\n' +
		'      <th scope="col">EnodeB ID</th>\n' +
		'      <th scope="col">EnodeB Name</th>\n' +
		'      <th scope="col">LTE BAND</th>\n' +
		'      <th scope="col">PCI</th>\n' +
		'      <th scope="col">ACL</th>\n' +
		'      <th scope="col">Azimuth</th>\n' +
		'      <th scope="col">M-Tilt</th>\n' +
		'      <th scope="col">E-Tilt</th>\n' +
		'      <th scope="col">Sector ID</th>\n' +
		'      <th scope="col">Cell Coverage</th>\n' +
		'      <th scope="col">Antenna Model</th>\n' +
		'      <th scope="col">Site Type</th>\n' +
		'      <th scope="col">Local Cell ID</th>\n' +
		'      <th scope="col">Dual Beam</th>\n' +
		'      <th scope="col">TAC</th>\n' +
		'      <th scope="col">CGI</th>\n' +
		'      <th scope="col">AT HOME MOBILE</th>\n' +
		'    </tr>\n' +
		'  </thead>' +
		'<tbody>';
		let num = 1;
		_.forEach(data,function (val) {
			html += ' <tr class="text-center small">\n' +
				'      <td>'+num+++'</td>\n' +
				'      <td>'+val.CELL_NAME+'</td>\n' +
				'      <td>'+val.eNodeBID+'</td>\n' +
				'      <td>'+val.eNodeBName+'</td>\n' +
				'      <td>'+val.LTE_BAND+'</td>\n' +		
				'      <td>'+val.PCI+'</td>\n' +		
				'      <td>'+val.ACL+'</td>\n' +
				'      <td>'+val.Azimuth+'</td>\n' +
				'      <td>'+val.Mechanical_Tilt+'</td>\n' +
				'      <td>'+val.Electrical_Tilt+'</td>\n' +
				'      <td>'+val.Sector_ID+'</td>\n' +
				'      <td>'+val.CELL_COVERAGE_TYPE+'</td>\n' +
				'      <td>'+val.Antenna_Type+'</td>\n' +
				'      <td>'+val.Site_Solution+'</td>\n' +
				'      <td>'+val.Local_Cell_ID+'</td>\n' +
				'      <td>'+val.DUAL_BEAM_TAGGING+'</td>\n' +
				'      <td>'+val.TAC+'</td>\n' +
				'      <td>'+val.CGI+'</td>\n' +
				'      <td>'+val.AT_HOME_MOBILE+'</td>\n' +
				'</tr>';
		});
	 html += '</tbody>' +
	'\t\t\t\t\t\t\t\t</table>\n'+
	'</div>';

	return html;
}

function set_fiveg_tab(data) {
	let	html = '<div class="mg-top-5">' +
		'<table class="table table-bordered table-sm table-responsive dt-responsive nowrap rre_tbl">\n' +
		' <thead class="text-muted small text-center">\n' +
		'    <tr class="global_color text-white text-uppercase">\n' +
		'      <th scope="col">No.</th>\n' +
		'      <th scope="col">CELLNAME</th>\n' +
		'      <th scope="col">GNodeB ID</th>\n' +
		'      <th scope="col">CELL ID</th>\n' +
		'      <th scope="col">Sector Name</th>\n' +
		'      <th scope="col">Frequency Band</th>\n' +
		'      <th scope="col">BandWidth</th>\n' +
		'      <th scope="col">ACL</th>\n' +
		'      <th scope="col">Azimuth</th>\n' +
		'      <th scope="col">M-Tilt</th>\n' +
		'      <th scope="col">E-Tilt</th>\n' +
		'      <th scope="col">Cell Coverage</th>\n' +
		'      <th scope="col">Antenna Model</th>\n' +
		'      <th scope="col">Site Type</th>\n' +
		'      <th scope="col">Duplex_Mode</th>\n' +
		'      <th scope="col">DLEARFCN</th>\n' +
		'      <th scope="col">ULEARFCN</th>\n' +
		'      <th scope="col">CGI</th>\n' +
		'      <th scope="col">AT HOME MOBILE</th>\n' +
		'    </tr>\n' +
		'  </thead>' +
		'<tbody>';
		let num = 1;
		_.forEach(data,function (val) {
			html += ' <tr class="text-center small">\n' +
				'      <td>'+num+++'</td>\n' +
				'      <td>'+val.CELL_NAME+'</td>\n' +
				'      <td>'+val.gNodeB_ID+'</td>\n' +
				'      <td>'+val.Cell_ID+'</td>\n' +
				'      <td>'+val.Sector_Name+'</td>\n' +
				'      <td>'+val.Frequency_Band+'</td>\n' +	
				'      <td>'+val.BandWidth+'</td>\n' +
				'      <td>'+val.ACL+'</td>\n' +
				'      <td>'+val.Azimuth+'</td>\n' +
				'      <td>'+val.Mechanical_Tilt+'</td>\n' +
				'      <td>'+val.Electrical_Tilt+'</td>\n' +
				'      <td>'+val.CELL_COVERAGE_TYPE+'</td>\n' +
				'      <td>'+val.Antenna_Type+'</td>\n' +
				'      <td>'+val.Site_Solution+'</td>\n' +
				'      <td>'+val.Duplex_Mode+'</td>\n' +
				'      <td>'+val.DLEARFCN+'</td>\n' +
				'      <td>'+val.ULEARFCN+'</td>\n' +
				'      <td>'+val.CGI+'</td>\n' +
				'      <td>'+val.AT_HOME_MOBILE+'</td>\n' +
				'</tr>';
		});
	 html += '</tbody>' +
	'\t\t\t\t\t\t\t\t</table>\n'+
	'</div>';

	return html;
}