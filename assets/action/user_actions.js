$(document).ready(function(){
    pagerefreshsite();
});

$(document).on('submit', '#change_password', function(e) {
    e.preventDefault();
    var oldpassword = $("#oldpassword").val();
    var newpassword = $("#newpassword").val();
    var confirmpassword = $("#confirmpassword").val();

    if (newpassword===confirmpassword && newpassword!=="" && confirmpassword!=="")
    {
		let param = {
			url: url+'changepassword',
			dataType: 'json',
			type: 'POST',
			data: {
				"newpassword"      : newpassword,
				"oldpassword"      : oldpassword
			},
		};

		fortress(param).then( (data) => {
			$("#oldpassword").val("");
			$("#newpassword").val("");
			$("#confirmpassword").val("");
			SWAL(data.type, data.title,  data.msg);
			emit_socket(data.logs);
		});
    }
    else
    {
        errorSwal();
    }
});

function pagerefreshsite(){
    $.ajax({
        type: 'POST',
        url: url+'getusers',
        dataType: 'json',
        async: false,
        success: function(data){
            var status;
            var editsite = '<img class="icon_size" src="'+url+'style/icons/edit-user-64.png">';
            var remove = '<img class="icon_size" src="'+url+'style/icons/remove-user-64.png">';
            $('#users_list').empty();
            $.each(data, function(index, value){

                switch(value.status){
                    case 'activated':
                        status = '<span><img class="icon_size" src="'+url+'style/icons/user-activated-64.png"></span>';
                    break;
                    case 'deactivated':
                        status = '<span><img class="icon_size" src="'+url+'style/icons/user-deactivated-64.png"></span>';
                    break;
                    case 'suspended':
                        status = '<span><img class="icon_size" src="'+url+'style/icons/user-suspended-64.png"></span>';
                    break;
                }

                $('#users_list').append('<tr><th>'+value.username+'</th><td>'+value.fname+' '+value.lname+'</td><td>'+value.department+'</td><td>'+value.access_level+'</td><td>'+status+'</td><td><span id="'+value.id+'"data-toggle="modal" data-target="#editSiteModal" class="edit_user">'+editsite+'</span><span id="'+value.id+'" class="remove_user">'+remove+'</span></td></tr>');
            });
        }
    });
}
