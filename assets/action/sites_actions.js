var sitesdatatable;
var tblcolumns=[];
var htablecolumns = [];
var nowblinking;
var clickedoperation="";
var activeoperation="";
var selectedtr = [];
var textinitial=null;
var mapmode = 0;
$('#mapdiv').hide(1);
$('#subdiv').hide(1);
$('#maindiv').height("550px");
var tblstart,tblrecordsTotal,tblend;
var section;
$.ajax({  
    url: url+"getSection", 
    type:"POST",
    success: function(data){
            // console.log(data);
            section = data;
        }
    });
function initMap() {

    var lng = 123.288;
    var lat = 11.867;

    map = new google.maps.Map(
      document.getElementById('map'),
      {center: new google.maps.LatLng(lat, lng), 
        zoom: 4,
        controlSize: 30,
    });
}
$(document).ready(function(){

    tabledata();

    $("#dtable").on('dblclick','tbody tr', function(evt){
        
        //$('#modalsitespage h5').empty();
        // const siteid = (sitesdatatable.row(this).data())[0];
        //     localStorage.setItem('siteid',siteid);
        //     window.location = url+"site_profile";
        //     console.log("tr click: "+siteid);
        
    });
     $('#srchinput').focus(function(event) {
        // $('#tablediv').slideDown(300);
        // $('#profilediv').slideUp(300);
     });

    $('.pagination').on( 'click', function () {
        if(!ajaxdone) reHighlight(); //selection
    });
    $('#dtable').on('click', 'td', function () {
        if(activeoperation=="edit")
        {
            $(this).prop('contenteditable', true);
        }
    });
    // $('#dtable').on('dblclick', 'td', function () {
    //     // rowid = $(this).closest('tr').attr('id');
    //     // console.log(rowid);
    //     // console.log(tblcolumns[(parseInt( $(this).index()))+1]["title"]);
    //     var idx = sitesdatatable.cell( this ).index().column;
    //     var title = $(sitesdatatable.column( idx ).header()).html();
     
    //     console.log(title);
    //     // $('#tablediv').slideUp(300);
    //     // $('#profilediv').slideDown(300);
    // });
    $('#dtable').on('keyup', 'td', function () {
        $("#proceed").attr('disabled', false);
    });
    $('#dtable').on( 'click', 'tr', function () {
        var ret = [];
            if(activeoperation=="batchEdit" ||  mapmode==1 || activeoperation=="delete")
            {
                const id= sitesdatatable.rows(this).data()[0][0];
                // console.log(id); 
                selectedtr.push(id);
                for (var i = 0; i < selectedtr.length; i += 1) {          
                    if (ret.indexOf(selectedtr[i]) !== -1) {
                        ret.splice(ret.indexOf(selectedtr[i]), 1);
                    } else {
                        ret.push(selectedtr[i]);
                    }
                }
                selectedtr = ret;
                // console.log(selectedtr);
                reHighlight();
                if(!mapmode){
                    $("#proceed").attr('disabled', false);
                }else $("#clear").attr('disabled', false);
            }
            
    });

    // var data = [
    // ['sample', 'sample', 'sample', 'sample', 'sample', 'sample', 'sample'],
    // ['sample', 'sample', 'sample', 'sample', 'sample', 'sample', 'sample'],
    // ];
     
    // jexcel(document.getElementById('jdiv'), {
    //     contextMenu: false,
    //     data:data,
    //     columns: [
    //         { type: 'text',wordWrap:true},
    //         { type: 'text',wordWrap:true},
    //         { type: 'text',wordWrap:true},
    //         { type: 'text',wordWrap:true},
    //         { type: 'text',wordWrap:true},
    //         { type: 'text',wordWrap:true},
    //         { type: 'text',wordWrap:true}
    //      ]
    // });




});
    var textfinal='';
    var textinitial=null;

    var rowid = null;
    var fieldid = null;
    var tablecat = null;
    var dataset=[];

    // var deletearray = [];
$(document).on('focus','td', function(e) {
    // console.log(sitesdatatable.rows().data());
    rowid = (sitesdatatable.cell(this).index().row)+1;
    var tdcolindex = sitesdatatable.cell(this).index().column;
    fieldid= $(sitesdatatable.column(tdcolindex).header()).html();
    tablecat = $(this).closest("table").attr("id");
    // console.log(tablecat+' - '+rowid+' - '+fieldid);
    var textkeydown =$(this).text();
    // $(this).prop('contenteditable', true)
        if(textinitial==null && textinitial != textkeydown)
        {
            textinitial = $(this).text();
        }
    });
$(document).on('blur','td', function(e) {
        // console.log('blur: '+$(this).text());
        textfinal = $(this).text();
        // console.log(' tbody: '+towerdbid+'  tr: '+celldbid+'  td: '+fieldid+' textinitial: '+textinitial+'  textfinal: '+textfinal );
        getedited();
        // console.log(dataset);
        textinitial = null;
        textfinal = null;
        $(this).prop('contenteditable', false);
    });
function getedited()
    {
        var datanow=[];
        // if(activeoperation=='edit'||activeoperation=='delete')
        if(activeoperation=='edit')
        {
            if(textinitial!=null && textinitial!= textfinal)
            {  
                var i=0;
                for(i=0; i<dataset.length; i++)
                {
                    // console.log(dataset[i][0]+' : '+dbidnow +'   '+ dataset[i][1]+' : '+fieldid +'   '+ dataset[i][3]+' : '+textfinal);
                    if(dataset[i][0]==tablecat && dataset[i][1]==rowid && dataset[i][2]==fieldid && dataset[i][4]!=textfinal)
                    {
                        dataset[i][4]=textfinal;
                        // console.log('set: '+dbidnow+' '+fieldid+' '+dataset[i][2]+' '+textfinal);
                        return;
                    }
                }
            dataset.push([tablecat,rowid,fieldid,textinitial,textfinal]);
            console.log('push: '+tablecat+' ['+rowid+']['+fieldid+'] '+textinitial+' '+textfinal);
            return;
            }

        }
    }
var tabledata = async () => {
    
        await $.ajax({  
                url: url+"generatedatatable", 
                type:"POST",
                data:{displayto: "sites",request: "fields"},
                success: function(data){
                    // console.log(data);
                    $.each(JSON.parse(data), function(index,value){
                        // $("#dtable thead tr").append(`<th>`+value+`</th>`);
                        var my_item = {};
                        my_item.title = value;
                        tblcolumns.push(my_item);
                        // console.log(value);
                        htablecolumns.push(value);
                    });
                    // console.log(htablecolumns);
                }
            });
        var tbdata;
       
        sitesdatatable = $('#dtable').DataTable({
            "dom": 'lrt<"row"<"col"i><"col"p>>',
            //// "dom": 'lr<<"#dtableBody.row"t><"#htableBody.row">><"row"<"col"i><"col"p>>',
            // "dom": 'lr<"#dtableBody"t><"row"<"col"i><"col"p>>',
            // "scrollResize": true,

            // "dom": 'lrtip',
            "processing":true,  
            "serverSide":true, 
            // "scrollX": true,
            // "scrollY": 410,

            // "autowidth":true,

            "pageLength": 20,
            "lengthMenu": [[20, 50, 100, -1], [20, 50, 100, "All"]],
            "columns": tblcolumns,
            "ajax":{  
                url: url+"generatedatatable",
                data:{displayto: "sites",request: "data"},
                type:"POST",
            }
            ,
            fnCreatedRow: function( nRow, aData, iDataIndex ) {
                $(nRow).attr('id', aData[0]);
            },
            "columnDefs":[
                {
                    "visible": false,
                    "targets": [ 0 ] 
                },
               // {  
                    // targets: "_all",
                    // render: $.fn.dataTable.render.ellipsis(15)
                    // createdCell: function (td, cellData, rowData, row, col) {
                    //   if (td) {
                    //     $(td).css('color', 'red')
                    //   }
                    // }
               // }
            ],
            initComplete: async function () {
                // const secondFunction = async () => {
                //   const result = await firstFunction()
                //   // do something else here after firstFunction completes
                // }
                // var trtfoot = '<tr role="row">';
                // $.each(htablecolumns, function(index,value){
                //     if(index!=0){ 
                //         trtfoot+='<th><input type="text" placeholder="Search '+value+'"/></th>'
                //         // trtfoot+='<th>'+value+'</th>'
                //     }});
                // trtfoot += '</tr>';
                // $('.dataTables_scrollHeadInner table').append(
                //     $('<tfoot/>').append(trtfoot
                //     // $('<tfoot/>').append( $(".dataTables_scrollHeadInner thead tr").clone())
                // ));
                // $('#dtable').append(
                //     $('<tfoot/>').append(trtfoot
                //     // $('<tfoot/>').append( $(".dataTables_scrollHeadInner thead tr").clone())
                // ));
                $('#dtable').append(
                    $('<tfoot/>').append( $("#dtable thead tr").clone()
                ));
                // sitesdatatable.cell(this).index().column
            //     this.api().columns().every( function () {
            //     var column = this;
            //     var select = $('<select><option value=""></option></select>')
            //         .appendTo( $(column.footer()).empty() )
            //         .on( 'change', function () {
            //             var val = $.fn.dataTable.util.escapeRegex(
            //                 $(this).val()
            //             );
 
            //             column
            //                 .search( val )
            //                 .draw();
            //         } );
 
            //     column.data().unique().sort().each( function ( d, j ) {
            //         select.append( '<option value="'+d+'">'+d+'</option>' );
            //     } );
            // } );
                // console.log($('#dtable').DataTable().footer());
                // console.log(sitesdatatable.footer());
                
                // console.log(trtfoot);
                // $('#dtable tfoot th').each( function (i) {
                //     console.log(i);
                //     // var title = $(this).text();
                //     // $(this).html( '<option type="text" placeholder="Search '+title+'" /></option>' );
                //     $( 'input', this ).on( 'keyup', function () {

                //         if ( sitesdatatable.column(i).search() !== this.value ) {
                //         sitesdatatable
                //         .column(i)
                //         .search( this.value )
                //         .draw();
                //         }
                //     } );

                // } );
            //     this.api().columns().every( function () {
            //     var that = this;
 
            //     $( 'input', this.footer() ).on( 'keyup change clear', function () {
            //         if ( that.search() !== this.value ) {
            //             that
            //                 .search( this.value )
            //                 .draw();
            //         }
            //     } );
            // } );
                this.api().columns().every( await function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' );
                } );
            } );
                // console.log($('#dtable').DataTable());
                // console.log(this);

        // $('#dtable tfoot tr th').each( function (i) {
        // var title = $(this).text();
        // $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
 
    //     $( 'input', this ).on( 'keyup', function () {
    //         if ( sitesdatatable.column(i).search() !== this.value ) {
    //             console.log(sitesdatatable.column(i));
    //             sitesdatatable
    //                 // .column(i)
    //                 // .search( this.value )
    //                 // .draw();
    //                 .search( this.value ? this.value : '', true, false )
    //                         .draw();
    //         }
    //     } );
    // } );

        // $('.dataTables_scrollHeadInner thead tr:eq(1) th').each( function () {
        //     var title = $(this).text();
        //     $(this).html( '<input type="text" placeholder="Search '+title+'" class="column_search" />' );
        // });
        $('#dtable_length').addClass('row mt-2');
        $('#dtable_length label').addClass('col-2 m-0 p-0 ml-3');
        $('#dtable_length').append(`

                <div class="col form-group has-search m-0 p-0">
                    <span class="fa fa-search form-control-feedback form-control-xs mt-2"></span>
                    <input type="text" class="form-control form-control-sm rounded-pill" id="srchinput" placeholder="Search..." autofocus data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">

            </div>`);
        $('#dtable_length').append(`
            <div class="col-6 d-flex justify-content-between mr-3">
                <div class="d-flex align-items-center">
                    <h5 class="font-weight-normal m-0"></h5>
                </div>
                <div class="row">
                    <div class="col custom-control custom-switch pt-1">
                        <input type="checkbox" class="custom-control-input" id="mapswitch">
                        <label class="custom-control-label" for="mapswitch">Map</label>
                    </div>
                    <div class="col operationtoggle btn-group btn-group-toggle" data-toggle="buttons">
                        <label id="add" class="btn btn-outline-primary my-1 px-1 p-0">
                            <input type="radio" name="options"  autocomplete="off"> Add
                        </label>
                        <label id="edit" class="btn btn-outline-primary my-1 px-1 p-0">
                            <input type="radio" name="options"  autocomplete="off">Edit
                        </label>
                        <label id="batchEdit" class="btn btn-outline-primary my-1 px-1 p-0">
                            <input type="radio" name="options"  autocomplete="off"> Batch Edit
                        </label>
                        <label id="delete" class="btn btn-outline-primary my-1 px-1 p-0">
                            <input type="radio" name="options" autocomplete="off"> Delete
                        </label>
                    </div>
                    <button id="clear" type="button" class="btn btn-xs btn-link">clear</button>
                    <button id="proceed" type="button" class="btn btn-xs btn-link">Proceed</button>
                </div>
            </div>`);
       $("#proceed").attr('disabled', true);
       $("#clear").attr('disabled', true);

            },
        }); 
        // sitesdatatable.on( 'draw', function () {
        //             buildSelect();
        // } );
        // sitesdatatable.on( 'draw', function () {
        //     // console.log(tblcolumns);
        //     // htabledraw();

        //     } );
        // console.log(sitesdatatable.page.info());
        // $('#dtable_wrapper').addClass("d-flex justify-content-center");
        // $('#dtableBody').hide();

        // console.log(sitesdatatable.rows().data());
        

        // const data = [
        //   ["a", "Kia", "Nissan", "Toyota", "Honda"],
        //   ["2008", 10, 11, 12, 13],
        //   ["2009", 20, 11, 14, 13],
        //   ["2010", 30, 15, 12, 13]
        // ];
        // // console.log(data);
        // var placeholder = document.getElementById('htableBody');
        // var myDataGrid = new Handsontable(placeholder, {
        //     data: data,
        //     colHeaders: true,
        //     rowHeaders: true,
        //     dropdownMenu: true,
        //     hiddenColumns: {
        //         columns: [0],
        //         indicators: true
        //     }
        // });

        

    // console.log(sitesdatatable.page.info());
        // console.log(sitesdatatable);
    

  // sitesdatatable.columns().every( function () {
  //   var column = sitesdatatable.column( this, {search: 'applied'} );
  //   console.log(column.footer());
  //   var select = $('<select><option value=""></option></select>')
  //   .appendTo( $(column.footer()).empty() )
  //   .on( 'change', function () {
  //     var val = $.fn.dataTable.util.escapeRegex(
  //       $(this).val()
  //     );

  //     column
  //     .search( val ? '^'+val+'$' : '', true, false )
  //     .draw();
  //   } );

  //   column.data().unique().sort().each( function ( d, j ) {
  //     select.append( '<option value="'+d+'">'+d+'</option>' );
  //   } );
    
  //   // The rebuild will clear the exisiting select, so it needs to be repopulated
  //   var currSearch = column.search();
  //   if ( currSearch ) {
  //     select.val( currSearch.substring(1, currSearch.length-1) );
  //   }
  // } );
}
function htabledraw()
{
                if(activeoperation == "batchEdit"){
                $('#htableBody').show();
                 $('#dtableBody').hide();
            }else {
                $('#htableBody').hide();
                 $('#dtableBody').show();
            }
            $('#htableBody').empty();
           
            var myDataGrid = new Handsontable(document.getElementById('htableBody'), {
                data: sitesdatatable.rows().data().toArray(),
                colHeaders: true,
                rowHeaders: true,
                dropdownMenu: true,
                colHeaders: htablecolumns,
                hiddenColumns: {
                    columns: [0],
                    indicators: true
                },
            });
}
// $.fn.dataTable.render.ellipsis = function ( cutoff ) {
//     return function ( data, type, row ) {
//         return type === 'display' && data.length > cutoff ?
//             data.substr( 0, cutoff ) +'…' :
//             data;
//     }
// }
$(document).on('click','#mapswitch', function() {
    if ($("input[type=checkbox]").is(":checked")) { 
        $('#maindiv').animate({ height: '350px' });
        $('#mapdiv').show("slow");
        mapmode = 1; //open
    } else { 
        $('#mapdiv').hide("slow");
        // $('#subdiv').hide(1);
        $('#maindiv').animate({ height: '550px' });
        mapmode = 0; //close
        resetclear();
    } 
});

$(document).on('click','#dtable', function() {
    
    });

$(document).on('click','#clear', function() {
    resetclear();
    });
$(document).on('click','#proceed', function() {
    var updatedata=[];
    // $('#tablediv').slideUp(300);
    // $('#profilediv').slideDown(300);
    // $.ajax({
    //         type: 'POST',
    //         url: url+'getquery/'+tech,
    //         dataType: 'json',
    //         data: {
    //             "siteid"      : passedsiteid
    //         },
    //         async: false,
    //         success: function(data){
    //             if(data===null){
    //                 console.log("null data returned" +data);
    //             }else{
    //                 // console.log(data);
    //                 datareturn = data;
    //             }
    //         }
    //     });
    // });
    switch(activeoperation)
    {
        case 'view':
            Swal.fire('No funtion yet.');
            break;
        case 'add':
            Swal.fire('No funtion yet.');
            break;
        case 'edit':
            for (var i = 0; i < dataset.length; i ++) {
                //dataset.push([tablecat,rowid,fieldid,textinitial,textfinal]);      
                updatedata.push([dataset[i][1],dataset[i][2],dataset[i][4]]);
            }
                 console.log(updatedata);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "Please confirm!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: url+'edit',
                            dataType: 'json',
                            async: false,
                            data:
                            {
                                "data"      : updatedata
                            },
                            success: function(data){
                                // data = JSON.parse(data);
                                if(data===null){
                                    console.log("null data returned" +data);
                                }else{
                                    console.log(data);
                                }
                            }
                        });

                        Swal.fire({
                            title: 'Submitted',
                            text: "",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'OK'
                        }).then((result) => {
                            if (result.value) {
                                // $('.bd-example-modal-lg').modal('hide');
                                // location.reload();
                                sitesdatatable.ajax.reload();
                                resetclear();
                            }
                        });
                    }
                });

            break;
        case 'batchEdit':
            // Swal.fire('No funtion yet.');
            // console.log(sitesdatatable.rows().data());

            break;
        case 'delete':
            Swal.fire('No funtion yet.');
            break;
        
    }
    

});
function resetclear()
    {
        $(".operationtoggle label").removeClass('active');
        activeoperation="";
        selectedtr = [];
        dataset=[];
        reHighlight();
        $("#proceed").attr('disabled', true);
        $("#clear").attr('disabled', true);
        sitesdatatable.ajax.reload();

    }
$(document).on('click','.operationtoggle label', function() {
    
    if(nowblinking==undefined)
    {
        nowblinking = this;
    }
    if(clickedoperation!=undefined && clickedoperation != ($(this)).attr("id"))
    {
        nowblinking = this;
        resetclear();
    }
    blink(this);
    clickedoperation = ($(this)).attr("id");
    activeoperation = clickedoperation;
    $("#clear").attr('disabled', false);
    switch(activeoperation)
    {
        case "add":
            $("#proceed").attr('disabled', false);
            break;
        case "batchEdit":
            // $('#dtableBody').hide("slow");
            htabledraw();
            // console.log(sitesdatatable.rows().data().toArray());
            
            break;
    }
});
    // function batchEdit(){

    // }
    function blink(tag) {
            $(tag).fadeOut(500, function(){
                $(tag).fadeIn(500, function(){
                    // console.log(nowblinking+' '+tag+' '+activeoperation);
                    if(nowblinking != tag || activeoperation=='')
                    {   
                        // console.log(2);
                        return;
                    }else blink(tag);
                });
            });
    }
$( document ).ajaxSend(function() {
    ajaxdone = 1;
});
$( document ).ajaxComplete(function() {
    ajaxdone = 0;
});
$(document).on("keyup","#srchinput",function() {
        sitesdatatable.search(this.value).draw();
    });
$('#dtable').on('draw.dt', function() {
    reHighlight();
});
function reHighlight()
{
    let oTable = document.getElementById('dtable');
    for (var i = 1, row; row = oTable.rows[i]; i++) {

        var rowid = $(oTable.rows[i].cells[0]).closest('tr').attr('id');
        selectedtr.includes(rowid) ? $(oTable.rows[i]).addClass('selected') : $(oTable.rows[i]).removeClass('selected');
    }
    $.ajax({  
        url: url+"generatedatatable", 
        type:"POST",
        data: {"selectedtr" :selectedtr,"request" : "search"},
        success: function(data){
                
                pins = JSON.parse(data);
                pin(pins);
            }
        });
}

 function pin(pins)
    {
        var lng = 123.288;
        var lat = 11.867;
        // var pins=[];
        
        // console.log(pins);
        // pins.push(["vis111",11.87,123.3]);
        // pins.push(["vis222",11.87,123.36]);
        var myOptions = {
            center: new google.maps.LatLng(lat, lng), 
            zoom: 15,
            controlSize: 20,
        };
        // map = new google.maps.Map($('#mapdiv')[0], myOptions);
        var thismap = document.getElementById('map');
        map = new google.maps.Map(thismap,myOptions);
        var bounds = new google.maps.LatLngBounds(); 

        for (var x = 0; x < pins.length; x++) {
            // console.log(999999999);
            var latlng = new google.maps.LatLng(pins[x]["LATITUDE"], pins[x]["LONGITUDE"]);

            new google.maps.Marker({
                label: {
                    color: 'gray',
                    fontWeight: 'bold',
                    text: pins[x]["PLAID"],
                },
                position: latlng,
                map: map
            });
            bounds.extend(latlng);

        }

        

        var listener = google.maps.event.addListener(map, "idle", function() {
            if (map.getZoom() < 10) map.setZoom(7); 
            google.maps.event.removeListener(listener); 
        });
        map.fitBounds(bounds);
}