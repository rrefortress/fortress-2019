$(document).ready(function () {
	$('[data-toggle="tooltip"]').tooltip();
	get_Nominations_Track();
});

$(document).on('keyup', '#'+track_key[0]+'Search', function(e) {
	e.preventDefault();
	get_Nominations_Track(0, row_page = 10, $(this).val());
});

$(document).on('click', '#'+track_key[0]+'_pagination a', function(e) {
	e.preventDefault();
	let row_num = $(this).attr('data-ci-pagination-page');
  get_Nominations_Track(row_num);
});

function get_Nominations_Track(row_num = 0, row_page = 10, search = '') {
  let html = '';
  let ctr = 1;
	$.ajax({
		url: url + 'FileTrack/get_Nominations_Track/'+row_num,
		dataType: 'json',
		data: {row_num: row_num, row_page : row_page, search: search},
		type: 'GET',
		beforeSend() {
      let html = '<div class="content">' +
        '<h5 class="text-center text-white">' +
        '<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>' +
        '<h4 class="text-dark text-center">Loading</h4>' +
        '</h5>' +
        '</div>';
      $('#'+track_key[0]+'_datas').html(html);
    },
		success: function (data) {
      let html = '';
      if (data.details.length > 0) {
           for (i in data.details) {
             html += '<tr class="border-bottom" row_id='+data.details[i].id+'>' +
                 '<th scope="row" class="text-center">'+data.details[i].id+'.</th>' +
                 '<td class="text-truncate" data-toggle="tooltip" data-placement="top" title='+data.details[i].SITE_NAME+'>'+data.details[i].SITE_NAME+'</td>' +
               '</tr>';
               ctr++;
           }
      } else {
        html = '<div class="content">' +
          '<h5 class="text-center text-white">' +
          '<i class="fas fa-tasks display-1 text-dark"></i><br>' +
          '<h4 class="text-dark text-center">Empty Data.</h4>' +
          '<label class="text-dark text-center">No data has been added yet.</label>' +
          '</h5>' +
          '</div>';
      }

      $('#nom_datas').html(html);
       $('#nom_pagination').html(data.pagination);
       $('[data-toggle="tooltip"]').tooltip();
		},
		error: function (xhr, status, error) {
			errorSwal();
		}
	});
}
