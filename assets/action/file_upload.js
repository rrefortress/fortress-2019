let xlsx_arr = [];

$(document).ready(function () {
	let dep = $('#dep').val();
	set_link(dep);
});

function set_link(dep) {
	$("#upload-link").attr("href", url +'Formats/'+ dep +'.xlsx');
}

$('#udbfile').on('change', function (evt) {
	let selectedFile = evt.target.files[0];
	let reader = new FileReader();
	reader.onload = function(event) {
		let num = 0;
		let data = event.target.result;
		let workbook = XLSX.read(data, {
			cellDates: true,
			type: 'binary',
			cellNF: false,
			cellText:false
		});
		workbook.SheetNames.forEach(function(sheetName) {
			xlsx_arr = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName], {raw: false});
			clear_data(sheetName);
			$('#submitButton, #clear_data_file').removeAttr('disabled');
			num++;
		});
	};

	reader.onerror = function(event) {
		console.error("File could not be read! Code " + event.target.error.code);
	};

	reader.readAsBinaryString(selectedFile);
});

function clear_data(name) {
	truncate_data(name).then( (data) => {
		console.log(data);
	});
}

$('#submitButton').on('click', function (e) {
	e.preventDefault();
	$('#submitButton, #clear_data_file').attr('disabled', 'disabled');
	$("#upload_loader").modal('show');
	let json = _.chunk(xlsx_arr, 100);
	let count = 0;

	for (i in json) {
		doAjax(json[i]).then( (data) => {
			if (data.type === 'success') {
				count += 1;
				if (count === json.length) {
					logs(json[i]).then( (datas) => {
						$("#upload_loader").modal('hide');
						SWAL(data.type, data.title, data.msg);
						emit_socket(datas);
						clearData();
					});
				}
			}
		});
	}
});

async function logs() {
	let result;

	try {
		result = await $.ajax({
			url: url + 'save_upload_logs',
			type: 'GET',
			dataType: 'json',
			contentType:"application/json",
		});

		return result;
	} catch (error) {
		errorSwal();
	}
}

async function doAjax(args) {
	let result;

	try {
		result = await $.ajax({
			url: url + 'udb_upload',
			type: 'POST',
			data: JSON.stringify(args),
			dataType: 'json',
			contentType:"application/json",
		});

		return result;
	} catch (error) {
		errorSwal();
	}
}

async function truncate_data(value) {
	let result;

	try {
		result = await $.ajax({
			url: url + 'truncate_data',
			type: 'POST',
			data: {name: value},
		});

		return result;
	} catch (error) {
		errorSwal();
		process.abort();
		$("#upload_loader").modal('hide');
		clearData();
	}
}



$('#clear_data_file').on('click', function () {
	clearData();
});

function clearData() {
	$('#udbfile, #udbfile-rre').val('');
	$('#database_table').val(0);
	xlsx_arr = [];
	$('#submitButton, #clear_data_file').attr("disabled", true);
}




