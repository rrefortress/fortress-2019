let config = [];
let ctr = 0;
let data = [];

$(function () {
	set_clp();
});

function set_clp() {
	$('.export_clp, .clear_clp').prop('disabled', true);
	$('.clp-div').html('<h5 class="text-center text-white mg-top-135">\n' +
								'<i class="far fa-list-alt display-2 text-muted"></i></br>\n' +
								'<small class="text-center text-muted mg-top-10"><em>Please upload file first to display CLP lists.</em></small>\n' +
								'</h5>');
}

$(document).on('submit', '#generate', function (e) {
	e.preventDefault();
	$('.config').val('');
	let planned_tech = $('.planned-tech').val();
	let sector = $('#sectors').val();
	let azimuth = $('#azimuth').val();
	let acl = $('#acl').val();
	let status = false;

	$('#prev_sectors').val(sector);
	$('#prev_azimuth').val(azimuth);
	$('#prev_acl').val(acl);

	arr = planned_tech.split(",");
	_.forEach(arr, function (val) {
		val = $.trim(val);
		if (val === 'G9') {
			$('#prev_g9').val(set_sector_value('4', sector));
			status = true;
		}

		if (val === 'G18') {
			$('#prev_g18').val(set_sector_value('8', sector));
		}

		if (val === 'U21') {
			$('#prev_u21').val(set_sector_value('2', sector));
		}

		if (val=== 'U9') {
			$('#prev_u9').val(set_sector_value('2', sector));
		}

		if (val=== 'L7') {
			$('#prev_l7').val(set_sector_value('1', sector));
		}

		if (val === 'L18') {
			$('#prev_l18').val(set_sector_value('2', sector));
		}

		if (val === 'L23') {
			$('#prev_l23').val(set_sector_value('2', sector));
		}

		if (val === 'L26') {
			$('#prev_l26').val(set_sector_value('2', sector));
		}
	});

	set_G9_CID(sector, status);
	set_NODE_ID(sector);
});

function set_G9_CID(sector = 0, status) {
	let html = '<b>Sector 1-'+sector+'</b> <br>G9:  <br>';
	if (sector > 3) {
		html += '<b>Sector 4 :</b> <br>G9: ';
	}
	$('#prev_cid').html(status === true ? html : '');
}

function set_NODE_ID(sector = 0) {
	let html = '<b>Sector 1- '+sector+' </b> <br>' +
		'U21 F1 / F2 | U9 F1:  <br>' +
		'U9 F2:  <br>\n\n';

	if (sector > 3) {
		html += '<b>Sector 4 :</b> <br>\n' +
			'U21 F1 / F2 | U9 F1:  <br>' +
			'U9 F2: ';
	}
	$('#prev_nid').html(html);
}

$(document).on('click', '.clear', function (e) {
	// e.preventDefault();
	$('.clp').val('');
	$('#sectors').val(3);
});

function set_sector_value(val, sector) {
	let ctr = 1;
	let value = '';
	let sec = sector;
	for (c = 0; sec > c; sec--) {
		value += val;
		if (sec > 1) {
			for (c1 = 0; c1 < 1; c1++) {
				value += ' + ';
			}
		}
		ctr++;
	}

	return value;
}

$(document).on('click', '.bulk-upload_clp', function (e) {
	e.preventDefault();
	$('#bulk-upload-clp').modal('show');
});

function get_clp(data) {
	let html = '\t\t\t\t\t\t<table id="clp_table" class="force-fit display nowrap table-sm small table-bordered table-striped table-hover small nowrap mg-top-5" style="max-width: 1180px !important;">\n' +
		'\t\t\t\t\t\t<thead class="thead-dark">\n' +
		'\t\t\t\t\t\t<tr>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center text-white bg-primary">SEARCHRING NAME</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center text-white bg-primary">PLAID</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center text-white bg-primary">NO. OF SECTORS</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center text-white bg-success" class="text-center">PLANNED TECH</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center bg-warning">GSM900</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center bg-warning">GSM1800</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center bg-warning">UMTS900</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center bg-warning">UMTS2100</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center bg-warning">LTE1800</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center bg-warning">LTE2100</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center bg-warning">LTE2300</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center bg-warning">LTE2600</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center bg-warning">LTE2600MM</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center bg-warning">LTE700</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center bg-warning">LTE900</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center bg-warning">NR2600</th>\n' +
		'\t\t\t\t\t\t\t<th scope="col" class="text-center bg-warning">NR3500 (5G)</th>\n' +

		'\t\t\t\t\t\t<tbody class="clp_lists">\n';
	_.forEach(data, function (val) {
		html += '<tr>' +
			'<td>'+val['SEARCHRING NAME']+'</td>' +
			'<td class="text-center">'+val['PLAID']+'</td>' +
			'<td class="text-center">'+val['No. of Sectors']+'</td>' +
			'<td class="text-center">'+val['Planned Tech']+'</td>' +
			'<td class="text-center">'+val['GSM900']+'</td>' +
			'<td class="text-center">'+val['GSM1800']+'</td>' +
			'<td class="text-center">'+val['UMTS900']+'</td>' +
			'<td class="text-center">'+val['UMTS2100']+'</td>' +
			'<td class="text-center">'+val['LTE1800']+'</td>' +
			'<td class="text-center">'+val['LTE2100']+'</td>' +
			'<td class="text-center">'+val['LTE2300']+'</td>' +
			'<td class="text-center">'+val['LTE2600']+'</td>' +
			'<td class="text-center">'+val['LTE2600MM']+'</td>' +
			'<td class="text-center">'+val['LTE700']+'</td>' +
			'<td class="text-center">'+val['LTE900']+'</td>' +
			'<td class="text-center">'+val['NR2600']+'</td>' +
			'<td class="text-center">'+val['NR3500 (5G)']+'</td>' +
			'</tr>';
	});

	html += '\t\t\t\t\t\t</tbody>\n';
	'\t\t\t\t\t</table>';
	$('.clp-div').html(html);
	$('#clp_table').dataTable();
}

$(document).on("click", ".export_clp", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (data.length > 0) {
		JSONToCSVConvertor(data, "CLP_(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
	$(this).html('<i class="fas fa-download"></i> EXPORT').prop('disabled', false);
});

$(document).on("click", ".clear_clp", function (e) {
	e.preventDefault();
	set_clp();
});

