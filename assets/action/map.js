var map;
var InforObj = [];
let urls = url + './style/pins/';

function setLocations(data, id_name) {
  map = new google.maps.Map(
      document.getElementById(id_name),
      {center: new google.maps.LatLng(11.164576, 123.805689), zoom: 6});

      let longlat = [];
      for (i in data) {
        if (data[i].latitude != '#N/A' && data[i].longtitude != '#N/A') {
          // let coordinates = data[i].latitude + ' ' + data[i].longtitude;
          // doGeocode(coordinates);

          longlat.push({
            position: new google.maps.LatLng(data[i].latitude, data[i].longtitude),
            type: 'info',
            title: data[i].SITE_NAME,
            icon : data[i].Category,
            Survey_Date: data[i].Survey_Date,
            Project: data[i].Project,
            rre: data[i].rre,
            RFE_Representative: data[i].RFE_Representative,
            SAQ_Representative: data[i].SAQ_Representative,
            Category          : data[i].Category,
            Vendor            : data[i].Vendor,
            Remarks           : data[i].Remarks,
            longtitude        : data[i].longtitude,
            latitude          : data[i].latitude
          });
        }
      }

    map.setMapTypeId(google.maps.MapTypeId.HYBRID);
    setMarkers(longlat);
}

// function initMap() {
//   map = new google.maps.Map(
//   document.getElementById('track_map'),
//   {center: new google.maps.LatLng(11.164576, 123.805689), zoom: 8});
// }

function setMarkers(features) {
  // Create markers.
  for (i in features) {
    let contentString = '<div id="content">' +
                          '<h6>' + features[i].title + '</h6>' +
                          '<ol style="list-style: none;">' +
                            '<li>Date of Survey: <b>'+features[i].Survey_Date+'</b></li>' +
                            '<li>Project Type: <b>'+features[i].Project+'</b></li>' +
                            '<li>RRE : <b>'+features[i].rre+'</b></li>' +
                            '<li>RFE : <b>'+features[i].RFE_Representative+'</b></li>' +
                            '<li>SAQ : <b>'+features[i].SAQ_Representative+'</b></li>' +
                            '<li>Category : <b>'+features[i].Category+'</b></li>' +
                            '<li>Vendor : <b>'+features[i].Vendor+'</b></li>' +
                            '<li>Remarks : <b>'+features[i].Remarks+'</b></li>' +
                            '<li>Coordinates : <b>'+features[i].latitude+ ', ' +features[i].longtitude+'</b> </li>' +
                          '</ol>' +
                        '</div>';

    const infowindow = new google.maps.InfoWindow({
         content: contentString,
         maxWidth: 500
     });

     let current_date = new Date();
     current_date.setHours(0,0,0,0);

     let png = urls + 'blue.png';
     if (new Date(features[i].Survey_Date) >= current_date) {
        png = urls + 'yellow.png';
     } else {
       switch (features[i].icon.toLowerCase()) {
         case 'new site': png = urls + 'blue.png'; break;
         case 'rre-survey': png = urls + 'orange.png'; break;
         case 'master plan': png = urls + 'red.png'; break;
         case 'owo': png = urls + 'purple.png'; break;
         case 'ewo': png = urls + 'purple.png'; break;
       }
     }

    const marker = new google.maps.Marker({
      position: features[i].position,
      icon: png,
      map: map,
      title: features[i].title,
      clickable: true
    });

    marker.addListener('click', function () {
        map.setZoom(16);
        closeOtherInfo();
        infowindow.open(marker.get('map'), marker);
        InforObj[0] = infowindow;
        map.setCenter(marker.getPosition());
    });


    let color = '#40bfc1';
    if (new Date(features[i].Survey_Date) >= current_date) {
       color = '#f6da63';
    } else {
    switch (features[i].icon.toLowerCase()) {
        case 'new site': color    = '#46b3e6'; break;
        case 're-survey': color   = '#ed8240'; break;
        case 'master plan': color = '#ea5e5e'; break;
        case 'owo': color         = '#d89cf6'; break;
        case 'ewo': color         = '#d89cf6'; break;
        default: color            = '#dedef0'; break;
      }
    }

    // Add circle overlay and bind to marker
    let circle = new google.maps.Circle({
      map: map,
      radius: 300,    // 10 miles in metres
      fillColor: color,
      fillOpacity: 0.35,
      strokeOpacity: 0.8,
      strokeWeight: 2,
    });
    circle.bindTo('center', marker, 'position');
  };
}

function closeOtherInfo() {
    if (InforObj.length > 0) {
        InforObj[0].set("marker", null);
        InforObj[0].close();
        InforObj.length = 0;
    }
}

function set_map(data = '') {
		// let mapProp = {
		// 	center:new google.maps.LatLng(data.lat,data.long),
		// 	zoom:5,
		// };
		// new google.maps.Map(document.getElementById("rre_map"),mapProp)
		let map = new google.maps.Map(document.getElementById("rre_map"), {
			zoom: 10,
			center: {lat: +data.lat, lng: +data.lng},
			mapTypeId: "terrain"
		});

		let marker = new google.maps.Marker({
			position: {lat: +data.lat, lng: +data.lng},
			map: map,
			title: "Hello World!"
		});

	var triangleCoords = [
		{
			lat: 25.774,
			lng: -80.19
		},
		{
			lat: 18.466,
			lng: -66.118
		},
		{
			lat: 32.321,
			lng: -64.757
		},
		{
			lat: 25.774,
			lng: -80.19
		}
	]; // Construct the polygon.

	var bermudaTriangle = new google.maps.Polygon({
		paths: triangleCoords,
		strokeColor: "#FF0000",
		strokeOpacity: 0.8,
		strokeWeight: 2,
		fillColor: "#FF0000",
		fillOpacity: 0.35
	});
	bermudaTriangle.setMap(map);
	map.setMapTypeId(google.maps.MapTypeId.HYBRID);
}
