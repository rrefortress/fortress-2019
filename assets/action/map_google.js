$(document).ready(function(){
	//alert('main here');
	var map;
	var markers = [];
	var markerslte = [];
	var lines = [];
	var flag2g = true;
	var flag3g = true;
	var flaglte = true;
	var lng = 123.5000;
	var lat = 11.0000;
	var s = document.createElement("script");
	s.type = "text/javascript";
	// s.src  = "https://maps.google.com/maps/api/js?libraries=geometry&v=3.22&key=AIzaSyCJiNdQZEwhcv8Hep7SF8qDo2SJDM4Ttdw&callback=gmap_draw";
	s.src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJiNdQZEwhcv8Hep7SF8qDo2SJDM4Ttdw&callback=gmap_draw";
	var passedsiteid;

	function sitenameload(){
	  passedsiteid = localStorage.getItem('siteid');
	  if(passedsiteid!=="" || passedsiteid!==null ){
	      $("#searchquery_btn").trigger( "click" );
	  }else{
	      passedsiteid = $("#searchquery").val()
	  }
  }

	$("head").append(s);

	window.gmap_draw = function(){

		var options = {
			zoom: 7,
			center: {lat: lat, lng: lng}
		}
		map = new google.maps.Map(document.getElementById('main_map'), options);
	};

	// Locate the site and center on the map
	$('#searchquery_btn').on('click',function(){
		var query  = passedsiteid;
		if(query=="" || query==null ){

		}else
		{
			removeMarkers();
			$.ajax({
				type: 'POST',
				url: url+'getquery/tower',
				async: true,
				dataType: 'json',
				data: {
					"siteid" : query,
				},
				success:function(data){
					if(data!==undefined){
						map.setZoom(14);
						map.setCenter(new google.maps.LatLng(data.latitude, data.longitude));
						map.setMapTypeId(google.maps.MapTypeId.ROADMAP);

						addSiteMarker(data.latitude,data.longitude,data.sitename,'red');
					}
				}
			});

			$.ajax({
				type: 'POST',
				url: url+'getneighbors/tower/3',
				async: true,
				dataType: 'json',
				data: {
					"siteid" : query,
				},
				success:function(data){
					for (var value in data){
						addSiteMarker(data[value]['latitude'],data[value]['longitude'],data[value]['sitename'],'blue');
					}
				}
			});
		}
	});

	//Add site marker in the google map
	function addSiteMarker(latitude, longitude, sitename, color){
		var sitecolor;

		switch(color) {
			case 'red':
			sitecolor = 'red_circle.png';
			break;
			case 'blue':
			sitecolor = 'blue_circle.png';
			break;
			default:
		    // code block
		}
		var marker = new google.maps.Marker({
			position: {lat: parseFloat(latitude), lng: parseFloat(longitude)},
			icon: {
				url: url+"/style/icons/"+sitecolor, // url
			    scaledSize: new google.maps.Size(15, 16), // scaled size
			    origin: new google.maps.Point(0,0), // origin
			    anchor: new google.maps.Point(7, 4) // anchor
			},
			draggable: false,
			map: map,
		});
		markers.push(marker);
		var infoWindow = new google.maps.InfoWindow({
			content: sitename
		});

		marker.addListener('click', function(){
			infoWindow.open(map,marker);
		});

	}

	function removeMarkers(){
		for(i=0; i < markers.length; i++){
			markers[i].setMap(null);
		}
	}
	sitenameload();
});
