
var passedsiteid;
var activecelltab;
var activeoperation;
var techglobal;
var displaytoglobal;
var editstate=false;
var activeoperationfield;
var selectalldeletefield = `disabled`;
var clickedoperation;
var nowblinking;
var lastdbid=0;
var latitudeglobal;
var longitudeglobal; 
$(document).ready(function(){
    // $("#selectalldeletefield").prop("disabled", true); 
    sitenameload();
////////////USE THIS TO GET ID OF THE CELL BEING EDITED
    // var $th = $td.parent('table').find('th').eq($td.index());

//////////////////site edit button click
$('#siteedit').on('click',function(){
    modalcontent('Site');
    $('#siteprofilemodal .modal-header h5').append(` View`);
     $('#siteprofilemodal .modal-header').append(`
        <div class="d-flex justify-content-between mr-3">
            <div>
                <div class="operationtoggle btn-group btn-group-toggle" data-toggle="buttons">
                    </label>
                    <label id="edit" class="btn btn-outline-primary my-1 px-1 p-0">
                        <input type="radio" name="options" id="option2" autocomplete="off"> Edit
                    </label>
                    <label id="delete" class="btn btn-outline-primary my-1 px-1 p-0">
                        <input type="radio" name="options" id="option3" autocomplete="off"> Delete
                    </label>
                </div>
                <button id="cellresetclear" type="button" class="btn btn-xs btn-link">reset/clear</button>
            </div>
        </div>`);

    loaddata('tower','edit');
    var cl = $('#siteprofilemodal  .modal-dialog');
        cl.removeClass('modal-full');
        cl.addClass('modal-lg');
    $('#siteprofilemodal').modal('show');
    });

//////////////////celltab edit button click
$('#celledit').on('click',function(){
    modalcontent(activecelltab);
    var cl = $('#siteprofilemodal  .modal-dialog');
        cl.removeClass('modal-lg');
        cl.addClass('modal-full');
        $('#siteprofilemodal  .modal-header').append(`
                        <div class="d-flex justify-content-between mr-3">
                            <div class="d-flex align-items-center">
                                <h5 class="font-weight-normal m-0"></h5>
                            </div>
                            <div>
                                <div class="operationtoggle btn-group btn-group-toggle" data-toggle="buttons">
                                    <label id="add" class="btn btn-outline-primary my-1 px-1 p-0">
                                        <input type="radio" name="options"  autocomplete="off"> Add
                                    </label>
                                    <label id="edit" class="btn btn-outline-primary my-1 px-1 p-0">
                                        <input type="radio" name="options"  autocomplete="off"> Edit
                                    </label>
                                    <label id="delete" class="btn btn-outline-primary my-1 px-1 p-0">
                                        <input type="radio" name="options" autocomplete="off"> Delete
                                    </label>
                                </div>
                                <button id="cellresetclear" type="button" class="btn btn-xs btn-link">reset/clear</button>
                            </div>
                        </div>`);
    loaddata(activecelltab,'edit');
    $('#siteprofilemodal').modal('show');
    });


    $('#checkbox input').change(function(){
       
        checkboxstatus = $(this).attr('id');
        if(($(this).attr('id'))==="sectorcount"){
            rowperpage = $(this).val();
        }
        checkboxchange();
    });
});
//////////////////////////////////////////////////////////////////////////////////
    // $(document).on('dblclick','tr', function() {
    // if(activeoperation == 'edit')
    // {
    //      //$(this).attr('contenteditable', true);
    // }
    //    //$(this).val().trigger( "click" );
    //    console.log($(this).attr("id"));
    //    var $th = $(this).closest('td');
    //    console.log($th);
    // });

    //////get active tab
    $(document).on('click','#celltabs ul li a', function() {
        activecelltab = ($('#celltabs a.active')).attr("id");
        loaddata(activecelltab,'tab');
        // /console.log(activecelltab);
    });

    $(document).on('click','#siteprofilemodal .modalclosebtn', function() {
        // activeoperation='';
        // editstate=false;
        // activeoperationfield = ``;
        resetclear();
        location.reload();
        loaddata(activecelltab,'tab');
    });
    $(document).on('click','.fa-plus-circle', function() {

        var lasttr = this;
        addrow(lasttr);
        //console.log(this);
        //lasttr = $('#siteprofilemodal tr.added'+techglobal);
    });
    $(document).on('click','.fa-minus-circle', function() {
        $(this).closest('tr').remove();
    });

    $(document).on('click','.operationtoggle label', function() {
        if(nowblinking==undefined)
        {
            nowblinking = this;
        }
        if(clickedoperation!=undefined && clickedoperation != ($(this)).attr("id"))
        {
            nowblinking = this;
        }
        blink(this);
        clickedoperation = ($(this)).attr("id");
        $("#siteprofilesubmit").prop('disabled', false);
        editoperation();
    });
    function blink(tag) {
            $(tag).fadeOut(500, function(){
                $(tag).fadeIn(500, function(){
                    // console.log(nowblinking+' '+tag+' '+activeoperation);
                    if(nowblinking != tag || activeoperation=='')
                    {
                        return;
                    }else blink(tag);
                });
            });
        // }
        
    }
/////////////////////////////////////////////////////////
    var textfinal='';
    var textinitial=null;
    var clickedtd = null;

    var towerdbid = null;
    var celldbid = null;
    var fieldid = null;
    var dataset=[];

    var deletearray = [];

    // $(document).click(function() {   
    //     if(this.checked) {
    //         // Iterate each checkbox
    //         $(':checkbox').each(function() {
    //             this.checked = true;                        
    //         });
    //     } else {
    //         $(':checkbox').each(function() {
    //             this.checked = false;                       
    //         });
    //     }
    // });
    $(document).on('change','#selectalldeletefield', function(e) {
        if(activeoperation=='delete')
        {
            if(this.checked)
            {
                $('#siteprofilemodal .deletecheckbox').each(function() {
                    this.checked = true;
                    celldbid=$(this).closest('#siteprofilemodal tr').attr('id');
                    deletearray.push(celldbid);
                });
            } else 
            {
                $('#siteprofilemodal .deletecheckbox').each(function() {
                    this.checked = false;
                    celldbid=$(this).closest('#siteprofilemodal tr').attr('id');
                    for(var i = deletearray.length - 1; i >= 0; i--) {
                        if(deletearray[i] === celldbid) {
                           deletearray.splice(i, 1);
                        }
                    }
                });
            }
        }
        deletearray = [...new Set(deletearray)];
        console.log(deletearray);
    });
    $(document).on('change','.deletecheckbox', function(e) {
        celldbid = $(this).closest('tr').attr('id');
        if($(this).is(':checked'))
        {
            // console.log(' tbody: '+towerdbid+'  tr: '+celldbid);
            if(activeoperation=='delete')
            {
                deletearray.push(celldbid);
            }
        }
        else
        {
            for(var i = deletearray.length - 1; i >= 0; i--) {
                if(deletearray[i] === celldbid) {
                   deletearray.splice(i, 1);
                }
            }
        }
        deletearray = [...new Set(deletearray)];
        console.log(deletearray);
    });
    $(document).on('focus','td', function(e) {
        towerdbid = $(this).closest('tbody').attr('id');
        celldbid = $(this).closest('tr').attr('id');
        fieldid = $(this).attr('id');
        var textkeydown =$(this).text();
        // console.log('focus: '+$(this).text());
        if(textinitial==null && textinitial != textkeydown)
        // if((textinitial==null || textinitial=='null') && textinitial != textkeydown)
        // if(textinitial==null)
        {
            textinitial = $(this).text();
        }
        // console.log('textkeydown: '+textkeydown+'   textinitial: '+textinitial);
    });
   $(document).on('blur','td', function(e) {
        // console.log('blur: '+$(this).text());
        textfinal = $(this).text();
        // console.log(' tbody: '+towerdbid+'  tr: '+celldbid+'  td: '+fieldid+' textinitial: '+textinitial+'  textfinal: '+textfinal );
        getedited();
        // console.log(dataset);
        textinitial = null;
        textfinal = null;
    });
    // $(document).on('click td', function(e) {
        
    // });
    function getedited()
    {
        var datanow=[];
        var dbidnow='';
        if(techglobal == 'tower'){ dbidnow=towerdbid;}
        else {dbidnow=celldbid;}
        // if(activeoperation=='edit'||activeoperation=='delete')
        if(activeoperation=='edit')
        {
            if(textinitial!=null && textinitial!= textfinal)
            {  
                var i=0;
                for(i=0; i<dataset.length; i++)
                {
                    // console.log(dataset[i][0]+' : '+dbidnow +'   '+ dataset[i][1]+' : '+fieldid +'   '+ dataset[i][3]+' : '+textfinal);
                    if(dataset[i][0]==dbidnow && dataset[i][1]==fieldid && dataset[i][3]!=textfinal)
                    {
                        dataset[i][3]=textfinal;
                        // console.log('set: '+dbidnow+' '+fieldid+' '+dataset[i][2]+' '+textfinal);
                        return;
                    }
                }
            dataset.push([dbidnow,fieldid,textinitial,textfinal]);
            // console.log('push: '+dbidnow+' '+fieldid+' '+textinitial+' '+textfinal);
            return;
            }

        }
    }
    function duplicatedatacheck(datanow)
    {
        var i;
        // console.log('datanow '+datanow[0]+' '+datanow[1]+' '+datanow[2]+' '+datanow[3]+'\n');
        for(i=0; i<dataset.length; i++)
        {
            if(dataset[i][0]==datanow[0] && dataset[i][1]==datanow[1] && dataset[i][3]!=datanow[3])
            {
                dataset[i][3]=datanow[3];
                console.log('set: ');
                console.log(dataset);
                return;
            }
            // console.log('dataset '+dataset[i][0]+' '+dataset[i][1]+' '+dataset[i][2]+' '+dataset[i][3]+'\n');
        }
        dataset.push(datanow);
        console.log('push: ');
        console.log(dataset);
        return;
        // console.log('dataset '+dataset[i][0]+' '+dataset[i][1]+' '+dataset[i][2]+' '+dataset[i][3]+'\n');
        // dataset.push([dbid2g,sitename+"-"+sc, cellcounter+btscid[btscidflag], azimuth[sc-1]]);
    }

/////////load  cells on antenna details tab////////////////////////////////////
    function loaddata(tech,displayto){
        techglobal = tech;
        displaytoglobal = displayto;
        var tablesize;

        console.log('#'+tech+'table');
        var datareturn;
        var divarea;
        $.ajax({
            type: 'POST',
            url: url+'getquery/'+tech,
            dataType: 'json',
            data: {
                "siteid"      : passedsiteid
            },
            async: false,
            success: function(data){
                if(data===null){
                    console.log("null data returned" +data);
                }else{
                    // console.log(data);
                    datareturn = data;
                }
            }
        });
        console.log("tech: "+tech+"    displayto: "+displayto+"    activecelltab: "+activecelltab);

        if(tech=='tower' && displayto=='tab') divarea='#siteprofilediv';
        else if(tech!='tower' && displayto=='tab') divarea='#celltabarea';
        else divarea='#siteprofilemodal .modal-body'; 
        $(divarea).empty();
        
        var cellnamecell = ``;

        if(tech=='tower')
        {
            $.each(datareturn, function(index, value){
                if(value==null || value=="null" || value==undefined || value=="undefined" || value=="")
                {
                    datareturn[index]='-';
                }
            });
        }else
        {
            $.each(datareturn, function(index, value){
                $.each(value, function(index2, value2){
                    if(value2==null || value2=="null" || value2==undefined || value2=="undefined" || value2=="")
                    {
                        datareturn[index][index2]='-';
                        // console.log(index2+value2);
                    }
                });
            });
        }
        switch(tech){
            case 'tower':
                latitudeglobal = datareturn.latitude;
                longitudeglobal = datareturn.longitude;
                sitenameglobal = datareturn.sitename;
                var siteeditfields = ``;
                var siteeditfields2 = ``;
                 $(divarea).append(`
                    <table id="siteprofile" class="table table-striped table-xs">
                    <tbody id="`+datareturn.siteid+`"></tbody>
                    </table>
                    `);
                if(displayto ==='edit')
                {
                    ////////////////MODAL
                    $(divarea+' tbody').append(`
                    <tr><th scope="row" style="width: 120px" >Sitename :</th><td contenteditable="`+editstate+`" id="sitename" class="col-10">`+datareturn.sitename+`</td></tr>
                    `);

                }
                if(activeoperation ==='edit'){

                    siteeditfields = `<tr><th scope="row">Latitude :</th><td contenteditable="`+editstate+`" id="latitude">`+datareturn.latitude+`</td></tr>
                                    <tr><th scope="row">Longitude :</th><td contenteditable="`+editstate+`" id="longitude">`+datareturn.longitude+`</td></tr>
                                    <tr><th scope="row">Street :</th><td contenteditable="`+editstate+`" id="street">`+datareturn.street+`</td></tr>
                                    <tr><th scope="row">Bldg Name:</th><td contenteditable="`+editstate+`" id="building">`+datareturn.bldg+`</td></tr>
                                    `;
                    siteeditfields2 = `<tr><th scope="row">Address :</th><td contenteditable="false" id="address">`+datareturn.address+`</td></tr>`;
                }else{
                    siteeditfields = `<tr><th scope="row">Coordinates :</th><td contenteditable="`+editstate+`" id="coordinates">`+datareturn.latitude+`,`+datareturn.longitude+`</td></tr>
                                    <tr><th scope="row">Address :</th><td contenteditable="`+editstate+`" id="address">`+datareturn.address+`</td></tr>
                                     `;
                        siteeditfields2 = ``;
                }
                    $('#sitename').text(datareturn.sitename);
                    $(divarea+' tbody').append(`
                    <tr><th scope="row" style="width: 120px">PLA ID :</th><td contenteditable="`+editstate+`" id="plaid" class="col-10">`+datareturn.plaid+`</td></tr>
                    `+siteeditfields+`
                    <tr><th scope="row">Barangay :</th><td contenteditable="`+editstate+`" id="barangay">`+datareturn.barangay+`</td></tr>
                    <tr><th scope="row">Town :</th><td contenteditable="`+editstate+`" id="municipality">`+datareturn.municipality+`</td></tr>
                    <tr><th scope="row">Province :</th><td contenteditable="`+editstate+`" id="province">`+datareturn.province+`</td></tr>
                    `+siteeditfields2+`
                    <tr><th scope="row">Region :</th> <td contenteditable="`+editstate+`" id="region">`+datareturn.region+`</td> </tr>
                    <tr><th scope="row">Area :</th> <td contenteditable="`+editstate+`" id="geoarea">`+datareturn.geoarea+`</td> </tr>
                    <tr><th scope="row">Site Type :</th> <td contenteditable="`+editstate+`" id="sitetype">`+datareturn.sitetype+`</td> </tr>
                    <tr><th scope="row">Tower Type :</th> <td contenteditable="`+editstate+`" id="towertype">`+datareturn.towertype+`</td></tr>
                    <tr><th scope="row">Tower Height :</th> <td contenteditable="`+editstate+`" id="towerheight">`+datareturn.towerheight+`</td> </tr>
                    <tr><th scope="row">Audited :</th> <td contenteditable="`+editstate+`" id="audited">`+datareturn.audited+`</td> </tr>
                    <tr><th scope="row">Permanent :</th> <td contenteditable="`+editstate+`" id="permanent">`+datareturn.permanent+`</td></tr>
                    <tr><th scope="row">Status :</th> <td contenteditable="`+editstate+`" id="status">`+datareturn.status+`</td></tr>
                    `);
                break;

            case '2g':
                $(divarea).append(`
                    <table id="2gtable" class="table table-striped table-responsive table-hover table-xxxs text-center">
                        <thead>
                            <tr>
                                <th scope="col">Cell Name</th>
                                <th scope="col">BSC Name</th>
                                <th scope="col">Cell ID</th>
                                <th scope="col">ACL</th>
                                <th scope="col">Azimuth</th>
                                <th scope="col">Longitude</th>
                                <th scope="col">Latitude</th>
                                <th scope="col">Coverage</th>
                                <th scope="col">M-tilt</th>
                                <th scope="col">E-tilt</th>
                                <th scope="col">Tech</th>
                                <th scope="col">LAC</th>
                                <th scope="col">RAC</th>
                                <th scope="col">NCC</th>
                                <th scope="col">BCC</th>
                                <th scope="col">BCCH</th>
                                <th scope="col">Vendor</th>
                                <th scope="col">Petal</th>
                                <th scope="col">Orientation</th>
                                <th scope="col">Solution</th>
                                <th scope="col">Audited</th>
                                <th scope="col">Status</th>
                                <th scope="col"><input type="checkbox" id="selectalldeletefield"`+selectalldeletefield+`></th>
                            </tr>
                        </thead>
                        <tbody id="`+passedsiteid+`">

                        </tbody>
                    </table>`);
                // console.log(datareturn);
                
                $.each(datareturn, function(index, value){
                
                if(activeoperation=='edit'||activeoperation=='add'||activeoperation=='delete')
                {
                    cellnamecell = `
                    <td contenteditable="`+editstate+`" id="cellname">`+value.cellname+`</td>`;
                }else 
                {
                    cellnamecell = `<th contenteditable="`+editstate+`" id="cellname">`+value.cellname+`</th>`;
                }
                $(divarea+' tbody').append(`

                    <tr id="`+value.dbcellid+`">
                    `+cellnamecell+`
                    <td contenteditable="`+editstate+`" id="homing">`+value.homing+`</td>
                    <td contenteditable="`+editstate+`" id="cid">`+(value.cid)+`</td>
                    <td contenteditable="`+editstate+`" id="acl">`+value.acl+`</td>
                    <td contenteditable="`+editstate+`" id="azimuth">`+value.azimuth+`</td>
                    <td contenteditable="`+editstate+`" id="longitude">`+value.longitude+`</td>
                    <td contenteditable="`+editstate+`" id="latitude">`+value.latitude+`</td>
                    <td contenteditable="`+editstate+`" id="sitetype">`+value.sitetype+`</td>
                    <td contenteditable="`+editstate+`" id="mtilt">`+value.mtilt+`</td>
                    <td contenteditable="`+editstate+`" id="etilt">`+value.etilt+`</td>
                    <td contenteditable="`+editstate+`" id="tech">`+value.tech+`</td>
                    <td contenteditable="`+editstate+`" id="lac">`+value.lac+`</td>
                    <td contenteditable="`+editstate+`" id="rac">`+value.rac+`</td>
                    <td contenteditable="`+editstate+`" id="ncc">`+value.ncc+`</td>
                    <td contenteditable="`+editstate+`" id="bcc">`+value.bcc+`</td>
                    <td contenteditable="`+editstate+`" id="bcch">`+value.bcch+`</td>
                    <td contenteditable="`+editstate+`" id="vendor">`+value.vendor+`</td>
                    <td contenteditable="`+editstate+`" id="petal">`+value.petal+`</td>
                    <td contenteditable="`+editstate+`" id="orientation">`+value.orientation+`</td>
                    <td contenteditable="`+editstate+`" id="solution">`+value.solution+`</td>
                    <td contenteditable="`+editstate+`" id="audited">`+value.audited+`</td>
                    <td contenteditable="`+editstate+`" id="status">`+value.status+`</td>
                    `+activeoperationfield+`
                    </tr>`);
                });

                break;
            case '3g':

                $(divarea).append(`
                    
                    <table id="3gtable" class="table table-striped table-responsive table-hover table-xxxs text-center">
                        <thead>
                            <tr>
                                <th scope="col">Cellname</th>
                                <th scope="col">RNC Name</th>
                                <th scope="col">Cell ID</th>
                                <th scope="col">nodeB Name</th>
                                <th scope="col">RNC ID</th>
                                <th scope="col">Azimuth</th>
                                <th scope="col">Longitude</th>
                                <th scope="col">Latitude</th>
                                <th scope="col">Coverage</th>
                                <th scope="col">ACL</th>
                                <th scope="col">M-tilt</th>
                                <th scope="col">E-tilt</th>
                                <th scope="col">Tech</th>
                                <th scope="col">Band</th>
                                <th scope="col">Carrier</th>
                                <th scope="col">Uplink</th>
                                <th scope="col">Downlink</th>
                                <th scope="col">PSC</th>
                                <th scope="col">LAC</th>
                                <th scope="col">RAC</th>
                                <th scope="col">Max Transmit Power</th>
                                <th scope="col">PCPICH</th>
                                <th scope="col">Vendor</th>
                                <th scope="col">Petal</th>
                                <th scope="col">Orientation</th>
                                <th scope="col">Solution</th>
                                <th scope="col">Audited</th>
                                <th scope="col">Status</th>
                                <th scope="col"><input type="checkbox" id="selectalldeletefield"`+selectalldeletefield+`></th>
                            </tr>
                        </thead>
                        <tbody id="`+passedsiteid+`">

                        </tbody>
                    </table>
                `);
                $.each(datareturn, function(index, value){
                if(activeoperation=='edit'||activeoperation=='add'||activeoperation=='delete')
                {
                    cellnamecell = `<td contenteditable="`+editstate+`" id="cellname">`+value.cellname+`</td>`;
                }else 
                {
                    cellnamecell = `<th contenteditable="`+editstate+`" id="cellname">`+value.cellname+`</th>`;
                }
                $(divarea+' tbody').append(`
                    <tr id="`+value.dbcellid+`">
                    `+cellnamecell+`
                    <td contenteditable="`+editstate+`" id="homing">`+value.homing+`</td>
                    <td contenteditable="`+editstate+`" id="cid">`+(value.cid).padStart(5, "0")+`</td>
                    <td contenteditable="`+editstate+`" id="nodebname">`+value.nodebname+`</td>
                    <td contenteditable="`+editstate+`" id="rncid">`+value.rncid+`</td>
                    <td contenteditable="`+editstate+`" id="azimuth">`+value.azimuth+`</td>
                    <td contenteditable="`+editstate+`" id="longitude">`+value.longitude+`</td>
                    <td contenteditable="`+editstate+`" id="latitude">`+value.latitude+`</td>
                    <td contenteditable="`+editstate+`" id="sitetype">`+value.sitetype+`</td>
                    <td contenteditable="`+editstate+`" id="acl">`+value.acl+`</td>
                    <td contenteditable="`+editstate+`" id="mtilt">`+value.mtilt+`</td>
                    <td contenteditable="`+editstate+`" id="etilt">`+value.etilt+`</td>
                    <td contenteditable="`+editstate+`" id="tech">`+value.tech+`</td>
                    <td contenteditable="`+editstate+`" id="band">`+value.band+`</td>
                    <td contenteditable="`+editstate+`" id="carrier">`+value.carrier+`</td>
                    <td contenteditable="`+editstate+`" id="uplinkuarfcn">`+value.uplinkuarfcn+`</td>
                    <td contenteditable="`+editstate+`" id="downlinkuarfcn">`+value.downlinkuarfcn+`</td>
                    <td contenteditable="`+editstate+`" id="psc">`+value.psc+`</td>
                    <td contenteditable="`+editstate+`" id="lac">`+value.lac+`</td>
                    <td contenteditable="`+editstate+`" id="rac">`+value.rac+`</td>
                    <td contenteditable="`+editstate+`" id="mtpowerofcell">`+value.mtpowerofcell+`</td>
                    <td contenteditable="`+editstate+`" id="pcpichtp">`+value.pcpichtp+`</td>
                    <td contenteditable="`+editstate+`" id="vendor">`+value.vendor+`</td>
                    <td contenteditable="`+editstate+`" id="petal">`+value.petal+`</td>
                    <td contenteditable="`+editstate+`" id="orientation">`+value.orientation+`</td>
                    <td contenteditable="`+editstate+`" id="solution">`+value.solution+`</td>
                    <td contenteditable="`+editstate+`" id="audited">`+value.audited+`</td>
                    <td contenteditable="`+editstate+`" id="status">`+value.status+`</td>
                    `+activeoperationfield+`
                    </tr>`);
                });
                break;
            case '4g':

                $(divarea).append(`
                    <table id="4gtable" class="table table-striped table-responsive table-hover table-xxxs text-center">
                        <thead>
                            <tr>
                                <th scope="col">Cellname</th>
                                <th scope="col">LTE Homing</th>
                                <th scope="col">Cell ID</th>
                                <th scope="col">ACL</th>
                                <th scope="col">Azimuth</th>
                                <th scope="col">Longitude</th>
                                <th scope="col">Latitude</th>
                                <th scope="col">Coverage</th>
                                <th scope="col">M-tilt</th>
                                <th scope="col">E-tilt</th>
                                <th scope="col">PCID</th>
                                <th scope="col">Tech</th>
                                <th scope="col">eNodeBID</th>
                                <th scope="col">Duplex</th>
                                <th scope="col">Band</th>
                                <th scope="col">Carrier</th>
                                <th scope="col">TAC</th>
                                <th scope="col">EARFCN</th>
                                <th scope="col">Bandwidth</th>
                                <th scope="col">Vendor</th>
                                <th scope="col">Petal</th>
                                <th scope="col">Orientation</th>
                                <th scope="col">Solution</th>
                                <th scope="col">Audited</th>
                                <th scope="col">Status</th>
                                <th scope="col"><input type="checkbox" id="selectalldeletefield"`+selectalldeletefield+`></th>
                            </tr>
                        </thead>
                        <tbody id="`+passedsiteid+`">

                        </tbody>
                    </table>
                `);
                $.each(datareturn, function(index, value){
                
                if(activeoperation=='edit'||activeoperation=='add'||activeoperation=='delete')
                {
                    cellnamecell = `<td contenteditable="`+editstate+`" id="cellname">`+value.cellname+`</td>`;
                }else 
                {
                    cellnamecell = `<th contenteditable="`+editstate+`" id="cellname">`+value.cellname+`</th>`;
                }
                $(divarea+' tbody').append(`
                    <tr id="`+value.dbcellid+`">
                    `+cellnamecell+`
                    <td contenteditable="`+editstate+`" id="homing">`+value.homing+`</td>
                    <td contenteditable="`+editstate+`" id="cid">`+(value.cid).padStart(5, "0")+`</td>
                    <td contenteditable="`+editstate+`" id="acl">`+value.acl+`</td>
                    <td contenteditable="`+editstate+`" id="azimuth">`+value.azimuth+`</td>
                    <td contenteditable="`+editstate+`" id="longitude">`+value.longitude+`</td>
                    <td contenteditable="`+editstate+`" id="latitude">`+value.latitude+`</td>
                    <td contenteditable="`+editstate+`" id="sitetype">`+value.sitetype+`</td>
                    <td contenteditable="`+editstate+`" id="mtilt">`+value.mtilt+`</td>
                    <td contenteditable="`+editstate+`" id="etilt">`+value.etilt+`</td>
                    <td contenteditable="`+editstate+`" id="pcid">`+value.pcid+`</td>
                    <td contenteditable="`+editstate+`" id="tech">`+value.tech+`</td>
                    <td contenteditable="`+editstate+`" id="enodebid">`+value.enodebid+`</td>
                    <td contenteditable="`+editstate+`" id="duplex">`+value.duplex+`</td>
                    <td contenteditable="`+editstate+`" id="band">`+value.band+`</td>
                    <td contenteditable="`+editstate+`" id="carrier">`+value.carrier+`</td>
                    <td contenteditable="`+editstate+`" id="tac">`+value.tac+`</td>
                    <td contenteditable="`+editstate+`" id="earfcn">`+value.earfcn+`</td>
                    <td contenteditable="`+editstate+`" id="bw">`+value.bw+`</td>
                    <td contenteditable="`+editstate+`" id="vendor">`+value.vendor+`</td>
                    <td contenteditable="`+editstate+`" id="petal">`+value.petal+`</td>
                    <td contenteditable="`+editstate+`" id="orientation">`+value.orientation+`</td>
                    <td contenteditable="`+editstate+`" id="solution">`+value.solution+`</td>
                    <td contenteditable="`+editstate+`" id="audited">`+value.audited+`</td>
                    <td contenteditable="`+editstate+`" id="status">`+value.status+`</td>
                    `+activeoperationfield+`
                    </tr>`);
                });
                break;
            default:
                console.log('tech: '+activecelltab+' loaddata error');
                break;
        }

    }
    function generatedbid(tech,lastid){
            var x;
            if(lastid==0)
            {
                $.ajax({
                type: 'POST',
                url: url+'getlastdbid',
                dataType: 'json',
                async: false,
                data:{'table':tech},
                success: function(id){
                    if(tech=='tower')
                    {
                        id=id.siteid;
                    }else{
                        id=id.dbcellid;
                    }
                    x=id;
                }
                });
            }else
            {
                x=lastid;
            }
            switch(tech) {
                case 'tower':
                    x=x.substring(2);   //remove "vs"
                    x=(parseInt(x))+1; //parse to int, increment
                    x= x.toString(); //toString
                    x=x.padStart(5, '0'); //pad with "0" to 5digits
                    x="vs"+(x);  //add "vs" then return
                    break;
                case '2g':
                    x=x.substring(3);   //remove "v2c"
                    x=(parseInt(x))+1; //parse to int, increment
                    x= x.toString(); //toString
                    x=x.padStart(5, '0'); //pad with "0" to 5digits
                    x="v2c"+(x);  //add "vs" then return
                    break;
                case '3g':
                    x=x.substring(3);   //remove "v2c"
                    x=(parseInt(x))+1; //parse to int, increment
                    x= x.toString(); //toString
                    x=x.padStart(5, '0'); //pad with "0" to 5digits
                    x="v3c"+(x);  //add "vs" then return
                    break;
                case '4g':
                    x=x.substring(3);   //remove "v4c"
                    x=(parseInt(x))+1; //parse to int, increment
                    x= x.toString(); //toString
                    x=x.padStart(5, '0'); //pad with "0" to 5digits
                    x="v4c"+(x);  //add "vs" then return
                    break;                default:
                break;
            }
            //console.log(x);
            return x;
        }
$(document).on('click','#cellresetclear', function() {
    resetclear();
    });
function resetclear()
    {
        $(".operationtoggle label").removeClass('active');
        // $('#siteprofilemodal').modal('hide');
        // console.log(techglobal);
        deletearray = {};
        activeoperation='';
        editstate=false;
        selectalldeletefield = `disabled`;
        activeoperationfield = ``;
        dataset=[];
        loaddata(techglobal,'edit');
        // $('#siteprofilemodal').modal('show');
        // $("#celledit").trigger("click");
        $("#siteprofilesubmit").prop('disabled', true);
    }
////////////////////////////////////////////////////////////////////////// ON SUBMIT
$(document).on('click','#siteprofilesubmit', function() {
    //alert('No Operation Selected');
    getedited();
    var data=[];

    var dbformatdata=[];
    var newdbid=0;
    lastdbid = 0;
    switch(activeoperation)
    {
        case 'add':
            var detailsafterarr=[];
            $('.added'+techglobal).map(function()
            {
                $(this).find('.added').map(function()
                {
                    data[$(this).attr('id')] = $(this).text();
                    return data;
                }).get();
                newdbid = generatedbid(techglobal,lastdbid);
                lastdbid = newdbid;
                data['dbcellid'] = newdbid;
                data['siteid'] = passedsiteid;
                data['sitename'] = sitenameglobal;
                detailsafterarr.push(data);
                console.log(data);
                data=[];
            }).get();
            

            dbformatdata = todbformat(undefined,detailsafterarr);
            console.log(dbformatdata);
            // console.log('activeoperation>'+activeoperation);
            ////////////////////////////////////////UPLOAD TO MDB Update and add to respective DBs
            Swal.fire({
            title: 'Are you sure?',
            text: "Please confirm!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: url+'insertsinglemdbupdate',
                    dataType: 'json',
                    async: false,
                    data:
                    {
                        "siteid":passedsiteid,
                        "tech":techglobal,
                        "operation":activeoperation,
                        "fieldnames":dbformatdata[0],
                        "detailsafter":dbformatdata[2]
                    },
                    success: function(data){
                        // console.log('activeoperation>>'+activeoperation);
                        data = JSON.parse(data);
                        console.log(typeof(data));
                        console.log(data);
                        // console.log('add '+techglobal+' '+data);
                        var id =data;
                        // console.log('ID: '+data+' created for approval.');
                        $.ajax({
                            type: 'POST',
                            url: url+'insertsingletodb',
                            data:{ 
                                'approvalid': id,
                                 "operation":activeoperation
                            },
                            async: false,
                            success: function(data){
                            // console.log('activeoperation>>>'+activeoperation);
                            console.log(data);
                            // SWAL('success', 'Success!', ' Cell/s successfully added to DB.');
                            // console.log('ID: '+id+' uploaded to db.');
                            }
                        });
                    }
                });

                Swal.fire({
                    title: 'Submitted',
                    text: "",
                    icon: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'OK'
                }).then((result) => {
                    if (result.value) {
                            $('.bd-example-modal-lg').modal('hide');
                            location.reload();
                    }
                })
            }
            });
            break;
        case 'edit':
            var detailsbefore = [];
            var detailsafter = [];
            var detailsbeforearr = [];
            var detailsafterarr = [];
            var alldata=[];
            var i = 0;
            if(techglobal=='tower')
            {
                console.log('siteprofile submit edit tower');
                var province = $("#province").text();
                var town = $("#municipality").text();
                var barangay = $("#barangay").text();
                var bldgname = $("#building").text();
                var street = $("#street").text();
                var oldaddress = $("#address").text();
                var address = "";
                
                for(i=0;i<dataset.length;i++)
                {
                    if(dataset[i][1]=='street' || dataset[i][1]=='building' || dataset[i][1]=='barangay' || dataset[i][1]=='municipality' || dataset[i][1]=='province')
                    {
                        if(bldgname!==""){ address += bldgname+", "; }
                        if(street!==""){ address += street+", "; }
                        address += "BRGY. "+barangay+", "+town+", "+province;
                        dataset.push([passedsiteid,'address',oldaddress,address]);
                        break;
                    }
                }
                detailsbefore['siteid'] = dataset[0][0];
                detailsafter['siteid'] = dataset[0][0];
                // console.log(dataset);
                for(i=0;i<dataset.length;i++)
                {
                    detailsbefore[dataset[i][1]] = dataset[i][2];
                    detailsafter[dataset[i][1]] = dataset[i][3];
                }
                // console.log(dataset);
                // detailsbeforearr.push(detailsbefore);
                detailsbeforearr.push(detailsbefore);
                detailsafterarr.push(detailsafter);
                console.log(detailsbeforearr);
                console.log(detailsafterarr);
                dbformatdata = todbformat(detailsbeforearr,detailsafterarr);
                console.log(dbformatdata);
            }
            else
            {
                detailsbeforearr = [];
                detailsafterarr = [];
                console.log('siteprofile submit EDIT 2g 3g 4g');
                // console.log(dataset);
                var j;
                var pushflag=0;
                var temp

                for(i=0;i<dataset.length;i++)
                {
                    if(i==0)
                    {

                        detailsbeforearr[i]=[];
                        detailsbeforearr[i]["siteid"] = passedsiteid;
                        detailsbeforearr[i]["dbcellid"] = dataset[i][0];
                        detailsbeforearr[i][dataset[i][1]] = dataset[i][2];
                        detailsafterarr[i]=[];
                        detailsafterarr[i]["siteid"] = passedsiteid;
                        detailsafterarr[i]["dbcellid"] = dataset[i][0];
                        detailsafterarr[i][dataset[i][1]] = dataset[i][3];
                    }
                    else
                    {
                        for(j=0;j<detailsbeforearr.length;j++)
                        {
                            // console.log(detailsbeforearr[j]["dbcellid"]+' '+dataset[i][0]);
                            if(detailsbeforearr[j]["dbcellid"] == dataset[i][0])
                            {
                                detailsbeforearr[j][dataset[i][1]] = dataset[i][2];
                                detailsafterarr[j][dataset[i][1]] = dataset[i][3];
                                pushflag = 0;
                                break;
                            }
                            else
                            {
                                pushflag = 1;
                            }
                        }
                        
                        if(pushflag)
                        {
                            detailsbeforearr[j]=[];
                            detailsbeforearr[j]["siteid"] = passedsiteid;
                            detailsbeforearr[j]["dbcellid"] = dataset[i][0];
                            detailsbeforearr[j][dataset[i][1]] = dataset[i][2];
                            detailsafterarr[j]=[];
                            detailsafterarr[j]["siteid"] = passedsiteid;
                            detailsafterarr[j]["dbcellid"] = dataset[i][0];
                            detailsafterarr[j][dataset[i][1]] = dataset[i][3];
                            pushflag=0;
                        }
                    }
                }
                // console.log(detailsbeforearr);
                // console.log(detailsafterarr);
////////////////////////////////DB CONVERSION FORMAT
                dbformatdata = todbformat(detailsbeforearr,detailsafterarr);
                
            }
            // console.log(typeof(detailsafterarr));

            Swal.fire({
            title: 'Are you sure?',
            text: "Please confirm!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: url+'insertsinglemdbupdate',
                    dataType: 'json',
                    async: false,
                    data:
                    {
                        "siteid":passedsiteid,
                        "tech":techglobal,
                        "operation":activeoperation,
                        "fieldnames":dbformatdata[0],
                        "detailsbefore":dbformatdata[1],
                        "detailsafter":dbformatdata[2]
                    },
                    success: function(data){
                        data = JSON.parse(data);
                        console.log('edit '+techglobal+' '+data);
                        var id =data;
                        console.log('ID: '+data+' created for approval.');
                        $.ajax({
                            type: 'POST',
                            url: url+'insertsingletodb',
                            data:{ 
                                'approvalid': id,
                                "operation":activeoperation
                            },
                            async: false,
                            success: function(data){
                            console.log(data);
                            // SWAL('success', 'Success!', ' Cell/s successfully added to DB.');
                            // console.log('ID: '+id+' uploaded to db.');
                            }
                        });
                    }
                });

                Swal.fire({
                    title: 'Submitted',
                    text: "",
                    icon: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'OK'
                }).then((result) => {
                    if (result.value) {
                            $('.bd-example-modal-lg').modal('hide');
                            location.reload();
                    }
                })
            }
            });
            break;
        case 'delete':
        ////////////////////////////////////////////////////////////////////////////
            var detailsbefore = {};
            var detailsbeforearr = [];
            var datareturn;
            var techtodelete;
            var mdbupdatecontroller;
            var deletedbcontroller;
            if(techglobal=='tower')
            {
                techtodelete = 'all';
                mdbupdatecontroller = 'insertsinglemdbupdate';
                deletedbcontroller = 'deletesinglefromdb';

                var dbtodelete = ['tower','2g','3g','4g'];
                


                    Swal.fire({
                        title: 'Are you sure you want to delete Site?',
                        text: "Please confirm!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then((result) => {
                        if (result.value) {
                            $.each( dbtodelete , function( key, value1 )
                            {
                                console.log(value1+"----------------------------------------------------------------");
                                $.ajax({
                                    type: 'POST',
                                    url: url+'getquery/'+value1,
                                    dataType: 'json',
                                    data: {
                                        "siteid"      : passedsiteid
                                    },
                                    async: false,
                                    success: function(data){
                                        if(data===null){
                                            console.log("null data returned" +data);
                                        }else{
                                            if(value1==='tower')
                                            {
                                                datareturn = [];
                                                datareturn.push(data);
                                            }else
                                            {
                                                datareturn = data;
                                            }
                                        }
                                    }
                                });
                                console.log(datareturn.length);
                                if(datareturn.length>0)
                                {

                                dbformatdata = todbformat(datareturn,undefined);
                                console.log(dbformatdata);
                                $.ajax({
                                    type: 'POST',
                                    url: url+'insertsinglemdbupdate',
                                    dataType: 'json',
                                    async: false,
                                    data:
                                    {
                                        "siteid":passedsiteid,
                                        "tech":value1,
                                        "fieldnames":dbformatdata[0],
                                        'detailsbefore': dbformatdata[1],
                                        "operation":activeoperation
                                    },
                                    success: function(data){
                                        // if(techtodelete !== 'all')
                                        // {
                                            data = JSON.parse(data);
                                        // }
                                        console.log('delete '+value1+' '+data);
                                        var id =data;
                                        console.log('ID: '+data+' created for approval.');
                                        $.ajax({
                                            type: 'POST',
                                            url: url+deletedbcontroller,
                                            data:{ 
                                                'approvalid': id,
                                                "operation":activeoperation
                                            },
                                            async: false,
                                            success: function(data){
                                                // console.log(data);
                                                // SWAL('success', 'Success!', ' Cell/s successfully added to DB.');
                                                // console.log('ID: '+id+' uploaded to db.');
                                            }
                                        });
                                    }
                                });
                            }
                            });
                            Swal.fire({
                                title: 'Submitted',
                                text: "",
                                icon: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'OK'
                            }).then((result) => {
                                if (result.value) {
                                    $('.bd-example-modal-lg').modal('hide');
                                    if(techtodelete !== 'all')
                                    {
                                        location.reload();
                                    }
                                    else
                                    {
                                        window.location.href = url+"sitespage";
                                    }
                                }
                            })
                        }
                    });
            }
            else
            {
                techtodelete = techglobal;
                mdbupdatecontroller = 'insertsinglemdbupdate';
                deletedbcontroller = 'deletesinglefromdb';
                $.ajax({
                    type: 'POST',
                    url: url+'getquery/'+techglobal,
                    dataType: 'json',
                    data: {
                        "siteid"      : passedsiteid
                    },
                    async: false,
                    success: function(data){
                        if(data===null){
                            console.log("null data returned" +data);
                        }else{
                            datareturn = data;
                        }
                    }
                });
                for(index1 of deletearray)
                {
                    for(index2 of datareturn)
                    {
                        if(index1 == index2.dbcellid)
                        {
                            detailsbeforearr.push(index2);
                        // console.log(index2);
                        }
                    }
                }
                console.log(detailsbeforearr);
                dbformatdata = todbformat(detailsbeforearr,detailsafterarr);
                console.log(dbformatdata);
////////////////////////////////////////////////////////////////////////////////////////
                Swal.fire({
                title: 'Are you sure you want to Delete?',
                text: "Please confirm!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
                }).then((result) => {
                if (result.value) {

                    $.ajax({
                        type: 'POST',
                        url: url+mdbupdatecontroller,
                        dataType: 'json',
                        async: false,
                        data:
                        {
                            "siteid":passedsiteid,
                            "tech":techtodelete,
                            "fieldnames":dbformatdata[0],
                            'detailsbefore': dbformatdata[1],
                            "operation":activeoperation
                        },
                        success: function(data){
                            if(techtodelete !== 'all')
                            {
                                data = JSON.parse(data);
                            }
                            console.log('delete '+techglobal+' '+data);
                            var id =data;
                            console.log('ID: '+data+' created for approval.');
                            $.ajax({
                                type: 'POST',
                                url: url+deletedbcontroller,
                                data:{ 
                                    'approvalid': id,
                                    "operation":activeoperation
                                },
                                async: false,
                                success: function(data){
                                console.log(data);
                                // SWAL('success', 'Success!', ' Cell/s successfully added to DB.');
                                // console.log('ID: '+id+' uploaded to db.');
                                }
                            });
                        }
                    });

                    Swal.fire({
                        title: 'Submitted',
                        text: "",
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        if (result.value) {
                            $('.bd-example-modal-lg').modal('hide');
                            if(techtodelete !== 'all')
                            {
                                location.reload();
                            }
                            else
                            {
                                window.location.href = url+"sitespage";
                            }
                        }
                    })
                }
                });
            }
            break;
    }

    });

    function todbformat(detailsbeforearr,detailsafterarr)
    {
        var dbfieldnames=[];
        var dbdetailsbefore=[];
        var dbdetailsafter=[];
        var fieldnamesource=[];
        if(detailsbeforearr==undefined)
        {
            fieldnamesource =detailsafterarr;
        }else
        {
            fieldnamesource =detailsbeforearr;
        }
        // console.log(detailsbeforearr);
        // console.log(detailsafterarr);
        // console.log(fieldnamesource);
        $.each(fieldnamesource , function( key, value)
        {
            var fieldnamesline=[];
            var detailsbeforeline=[];
            var detailsafterline=[];
            for(var  key2 in value)
            {
                fieldnamesline.push(key2);
                switch(activeoperation)
                {
                    case 'edit':
                        detailsbeforeline.push(value[key2]);
                        detailsafterline.push(detailsafterarr[key][key2]);
                        break;
                    case 'add':
                        detailsafterline.push(detailsafterarr[key][key2]);
                        break;
                    case 'delete':
                        detailsbeforeline.push(value[key2]);
                        break;
                }
            }
            dbfieldnames.push(fieldnamesline);
            dbdetailsbefore.push(detailsbeforeline);
            dbdetailsafter.push(detailsafterline);
        });
        
        if(activeoperation!=='edit')
        {
            var dbfieldnamestemp = dbfieldnames[0];
            dbfieldnames = [];
            dbfieldnames[0]=dbfieldnamestemp;
        }
        if(activeoperation=='add')
        {
            dbdetailsbefore=undefined;
        }
        if(activeoperation=='delete')
        {
            dbdetailsafter=undefined;
        }
        // console.log(dbfieldnames);
        // console.log(dbdetailsbefore);
        // console.log(dbdetailsafter);
        return [dbfieldnames,dbdetailsbefore,dbdetailsafter];
    }

    function sitenameload(){
        passedsiteid = localStorage.getItem('siteid');
       console.log('sitename load: '+passedsiteid);
        if(passedsiteid=='' || passedsiteid==null ){
            passedsiteid = $("#searchquery").val('');
            //localStorage.setItem('sitename','');
        }else{
            activecelltab='2g'; ////////////////////////////////////ACTIVE CELL SET TO 4G
            loaddata('tower','tab');
            loaddata(activecelltab,'tab');

            // $("#searchquery_btn").trigger( "click" );
            //localStorage.setItem('sitename','');
        }
    }

    function addrow(lasttr){
        var newtr=``;
        newdbid = generatedbid(techglobal,lastdbid);
        lastdbid = newdbid;
        switch(techglobal)
        {
            case '2g':
            // console.log(dbcellid2g);
                
                newtr=`
                <tr id="`+newdbid+`" class="added2g">
                <td class="added" contenteditable="true" id="cellname" placeholder="Enter Cell Name"> </td>
                <td class="added" contenteditable="true" id="homing"></td>
                <td class="added" contenteditable="true" id="cid"></td>
                <td class="added" contenteditable="true" id="acl"></td>
                <td class="added" contenteditable="true" id="azimuth"></td>
                <td class="added" contenteditable="true" id="longitude"></td>
                <td class="added" contenteditable="true" id="latitude"></td>
                <td class="added" contenteditable="true" id="sitetype"></td>
                <td class="added" contenteditable="true" id="mtilt"></td>
                <td class="added" contenteditable="true" id="etilt"></td>
                <td class="added" contenteditable="true" id="tech"></td>
                <td class="added" contenteditable="true" id="lac"></td>
                <td class="added" contenteditable="true" id="rac"></td>
                <td class="added" contenteditable="true" id="ncc"></td>
                <td class="added" contenteditable="true" id="bcc"></td>
                <td class="added" contenteditable="true" id="bcch"></td>
                <td class="added" contenteditable="true" id="vendor"></td>
                <td class="added" contenteditable="true" id="petal"></td>
                <td class="added" contenteditable="true" id="orientation"></td>
                <td class="added" contenteditable="true" id="solution"></td>
                <td class="added" contenteditable="true" id="audited"></td>
                <td class="added" contenteditable="true" id="status"></td>
                <td><i class="fas fa-plus-circle btn-link"></i>  <i class="fas fa-minus-circle btn-link"></i></td>
                </tr>
                `;
                break;
            case '3g':
                // console.log(dbcellid3g);
                newtr=`
                <tr id="`+newdbid+`" class="added3g">
                <td class="added" contenteditable="true" id="cellname"></td>
                <td class="added" contenteditable="true" id="homing"></td>
                <td class="added" contenteditable="true" id="cid"></td>
                <td class="added" contenteditable="true" id="nodebname"></td>
                <td class="added" contenteditable="true" id="rncid"></td>
                <td class="added" contenteditable="true" id="azimuth"></td>
                <td class="added" contenteditable="true" id="longitude"></td>
                <td class="added" contenteditable="true" id="latitude"></td>
                <td class="added" contenteditable="true" id="sitetype"></td>
                <td class="added" contenteditable="true" id="acl"></td>
                <td class="added" contenteditable="true" id="mtilt"></td>
                <td class="added" contenteditable="true" id="etilt"></td>
                <td class="added" contenteditable="true" id="tech"></td>
                <td class="added" contenteditable="true" id="band"></td>
                <td class="added" contenteditable="true" id="carrier"></td>
                <td class="added" contenteditable="true" id="uplinkuarfcn"></td>
                <td class="added" contenteditable="true" id="downlinkuarfcn"></td>
                <td class="added" contenteditable="true" id="psc"></td>
                <td class="added" contenteditable="true" id="lac"></td>
                <td class="added" contenteditable="true" id="rac"></td>
                <td class="added" contenteditable="true" id="mtpowerofcell"></td>
                <td class="added" contenteditable="true" id="pcpichtp"></td>
                <td class="added" contenteditable="true" id="vendor"></td>
                <td class="added" contenteditable="true" id="petal"></td>
                <td class="added" contenteditable="true" id="orientation"></td>
                <td class="added" contenteditable="true" id="solution"></td>
                <td class="added" contenteditable="true" id="audited"></td>
                <td class="added" contenteditable="true" id="status"></td>
                <td><i class="fas fa-plus-circle"></i><i class="fas fa-minus-circle"></i></td>
                </tr>`;
                break;
            case '4g':
 
                    newtr=`
                    <tr id="`+newdbid+`" class="added4g">
                    <td class="added" contenteditable="true" id="cellname"></td>
                    <td class="added" contenteditable="true" id="homing"></td>
                    <td class="added" contenteditable="true" id="cid"></td>
                    <td class="added" contenteditable="true" id="acl"></td>
                    <td class="added" contenteditable="true" id="azimuth"></td>
                    <td class="added" contenteditable="true" id="longitude"></td>
                    <td class="added" contenteditable="true" id="latitude"></td>
                    <td class="added" contenteditable="true" id="sitetype"></td>
                    <td class="added" contenteditable="true" id="mtilt"></td>
                    <td class="added" contenteditable="true" id="etilt"></td>
                    <td class="added" contenteditable="true" id="pcid"></td>
                    <td class="added" contenteditable="true" id="tech"></td>
                    <td class="added" contenteditable="true" id="enodebid"></td>
                    <td class="added" contenteditable="true" id="duplex"></td>
                    <td class="added" contenteditable="true" id="band"></td>
                    <td class="added" contenteditable="true" id="carrier"></td>
                    <td class="added" contenteditable="true" id="tac"></td>
                    <td class="added" contenteditable="true" id="earfcn"></td>
                    <td class="added" contenteditable="true" id="bw"></td>
                    <td class="added" contenteditable="true" id="vendor"></td>
                    <td class="added" contenteditable="true" id="petal"></td>
                    <td class="added" contenteditable="true" id="orientation"></td>
                    <td class="added" contenteditable="true" id="solution"></td>
                    <td class="added" contenteditable="true" id="audited"></td>
                    <td class="added" contenteditable="true" id="status"></td>
                    <td><i class="fas fa-plus-circle"></i><i class="fas fa-minus-circle"></i></td>
                    </tr>`;

                break;
            default:
                break;
        }
        $(newtr).insertAfter((lasttr).closest("tr"));
    }
    function editoperation(){
        // selectalldeletefield = `disabled`;
        activeoperationfield = ``;
        editstate=false;
        
        console.log('activeoperation: '+clickedoperation+' techglobal: '+techglobal+' displaytoglobal: '+displaytoglobal);
        // var actions = $(".modal table tbody tr:last-child").index();
        // console.log(actions+' qqqqqq');
        if(clickedoperation==activeoperation){
           // resetclear();
        }else{
            dataset=[];
            activeoperation = clickedoperation;

            switch(activeoperation)
            {
                case 'add':
                    loaddata(techglobal,'edit');
                    var lasttr = $('#siteprofilemodal tr:last');
                    addrow(lasttr);
                    break;
                case 'edit':
                    editstate=true;
                    loaddata(techglobal,'edit');
                    break;
                case 'delete':
                    //resetclear();
                    selectalldeletefield = `enabled`;
                    activeoperationfield = `<td contenteditable="false"><input type="checkbox" class="deletecheckbox"></td>`;
                    loaddata(techglobal,'edit');
                    //<input type="checkbox" class="form-check-input" id="exampleCheck1">
                    break;
                default:
                    console.log('operationtoggle click');
                    break;
            }
        }
    }
        ////modal header footer adder
    function modalcontent(headertitle){
        $('#siteprofilemodal .modal-header').empty();
        $('#siteprofilemodal .modal-header').append(`
            <h5 class="modal-title ml-3 m-0">`+headertitle+`</h5>
            `);
        $('#siteprofilemodal .modal-footer').empty();
        $('#siteprofilemodal .modal-footer').append(`
            <button id="siteprofilesubmit" type="button" class="btn btn-primary btn-sm" disabled>Apply</button>
            <button type="button" class="btn btn-secondary btn-sm modalclosebtn" data-dismiss="modal">Close</button>`);
    }

