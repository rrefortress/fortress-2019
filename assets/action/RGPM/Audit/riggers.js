let sects = 0;

$(document).ready(function () {
    $(".next-step").click(function () {
        var active = $('.wizard .nav-tabs li.active');
        active.next().removeClass('disabled');
        nextTab(active);
    });
    $(".prev-step").click(function () {
        var active = $('.wizard .nav-tabs li.active');
        prevTab(active);
    });

    $('.tech').select2({ width: '100%', theme: "classic" });    
    
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
      });
});

$(document.body).on('change',"#region",function (e) {
    e.preventDefault();
    let region = $(this).val();
    let plaid = $('#plaid').val();
    let sname = $('#sname').val();
   
    check_stg1(plaid, sname, region);
 });

$(document.body).on('keyup',"#plaid",function (e) {
    e.preventDefault();
    let plaid = $(this).val();
    let sname = $('#sname').val();
    let region = $('#region').val();
    check_stg1(plaid, sname, region);
 });

 $(document.body).on('keyup',"#sname",function (e) {
    e.preventDefault();
    let sname = $(this).val();
    let plaid = $('#plaid').val();
    let region = $('#region').val();
    check_stg1(plaid, sname, region);
 });

 function check_stg1(plaid, sname, region) {
     console.log(region);
    if (plaid !== '' & sname !== '' & region !== null) {
        $('.stg1').removeAttr('disabled');
    } else {
        $('.stg1').attr('disabled', true);
    }
}


$(document.body).on('change',".tech",function (e) {
    e.preventDefault();
    let tech = $(this).val();
    let height = $('#height').val();
    check_stg2(tech, height);
 });

 $(document.body).on('keyup',".height",function (e) {
    e.preventDefault();
    let height = $(this).val();
    let tech = $('#tech').val();
    check_stg2(tech, height);
 });

 function check_stg2(v1, v2) {
    if (v1 !== '' & v2 !== '') {
        $('.stg2').removeAttr('disabled');
    } else {
        $('.stg2').attr('disabled', true);
    }
 }


function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}


$('.nav-tabs').on('click', 'li', function() {
    $('.nav-tabs li.active').removeClass('active');
    $(this).addClass('active');
});

$(document.body).on('change',"#sectors",function (e) {
    e.preventDefault();
    sects = $("#sectors option:selected").val();

    let num = 1;
    let html = '';
    let count = 0;

    _.range(sects).forEach(() => {
        let bool = num == 1 ? true : false;
        let bools = num == 1 ? 'show' : '';
        
      
        html += '<div class="card">' +
                     '<div class="card-header bg-secondary text-white sectorings" data-toggle="collapse" data-target="#collapse-'+num+'" aria-expanded="'+bool+'">' +    
                        '<span class="title text-center">Sector #'+num+' </span>' +
                        '<span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>' +
                    '</div>' +

                    '<div id="collapse-'+num+'" class="collapse '+bools+'" data-parent="#sectoring">' +
                        '<div class="container">' +
                            '<div class="row">' +
                               '<div class="col-sm-12">' +
                                    '<div class="form-group">' +
                                        '<label class="small text-muted text-uppercase">Azimuth <b class="text-danger">*</b></label>' +
                                        '<input class="form-control rounded-pill border border-secondary sec-'+num+'" type="number" name="sec-'+num+'" placeholder="Enter Azimuth" autocomplete="off" type="number" min="0" maxlength="3" oninput="this.value=this.value.slice(0,this.maxLength||0/0);this.value=(this.value   < -1) ? (0/0) : this.value;">' +
                                    '</div>' +
                                '</div>' +
                                ' <div class="col-sm-12">' +
                                    '<div class="form-group">' +
                                        '<label class="small text-muted text-uppercase">M-Tilt <b class="text-danger">*</b></label>' +
                                        '<input class="form-control rounded-pill border border-secondary mtt-'+num+'" type="number" name="mt-'+num+'" placeholder="Enter M-Tilt" type="number" min="0" maxlength="1" oninput="this.value=this.value.slice(0,this.maxLength||0/0);this.value=(this.value   < -1) ? (0/0) : this.value;" autocomplete="off">' +
                                    '</div>' +
                                '</div>' +
                                ' <div class="col-sm-1  2">' +
                                    '<div class="form-group">' +
                                        '<label class="small text-muted text-uppercase">E-Tilt <b class="text-danger">*</b></label>' +
                                        '<input class="form-control rounded-pill border border-secondary ett-'+num+'" type="number" name="et-'+num+'" placeholder="Enter E-Tilt" type="number" min="0" maxlength="1" oninput="this.value=this.value.slice(0,this.maxLength||0/0);this.value=(this.value   < -11) ? (0/0) : this.value;" autocomplete="off">' +
                                    '</div>' +
                                '</div>' +
                                ' <div class="col-sm-12">' +
                                    '<div class="form-group">' +
                                        '<label class="text-muted text-uppercase">Antenna Model <b class="text-danger">*</b></label>' +
                                        '<select class="custom-select text-center am-'+num+' rounded-pill border border-secondary" name="am-'+num+'" id="am-'+num+'"">' +
                                            '<option value="" selected="selected" disabled>Select antenna</option>' +
                                            '<option>4 Ports Rosenberger</option>' +
                                            '<option>6 Ports Rosenberger</option>' +
                                            '<option>10 Ports Rosenberger</option>' +
                                            '<option selected>12 Ports Rosenberger</option>' +
                                            '<option>16 Ports Rosenberger</option>' +
                                        '</select>' +
                                     '</div>' +
                                '</div>' +
                            '</div>' +
                            

                        '</div>' +
                    '</div>' +
                '</div>';   
        num++;
        count++;

    });

    $('#sectoring').html(html);
    if (count == 1) {
        $('.s2, .s3, .s4').addClass('d-none');
    } else if (count == 2) {
        $('.s3, .s4').addClass('d-none');
        $('.s2').removeClass('d-none');
    } else if (count == 3) {
        $('.s4').addClass('d-none');
        $('.s2, .s3').removeClass('d-none');
    } else {
        $('.s2, .s3, .s4').removeClass('d-none');
    } 
 });

document.addEventListener('DOMContentLoaded', (ev) => {
    let input1 = document.getElementById('capture-1');
    input1.addEventListener('change', (ev) => {
        let img1 = document.getElementById('img-1');
        $("#img-1").removeClass('d-none');
        img1.src = window.URL.createObjectURL(input1.files[0]);
    });

    let input2 = document.getElementById('capture-2');
    input2.addEventListener('change', (ev) => {
        let img2 = document.getElementById('img-2');
        $("#img-2").removeClass('d-none');
        img2.src = window.URL.createObjectURL(input2.files[0]);
    });

    let input3 = document.getElementById('capture-3');
    input3.addEventListener('change', (ev) => {
        let img3 = document.getElementById('img-3');
        $("#img-3").removeClass('d-none');
        img3.src = window.URL.createObjectURL(input3.files[0]);
    });

    let input4 = document.getElementById('capture-4');
    input4.addEventListener('change', (ev) => {
        let img4 = document.getElementById('img-4');
        $("#img-4").removeClass('d-none');
        img4.src = window.URL.createObjectURL(input4.files[0]);
    });
});

$(document).on("submit","#audit_form", function(e) {    
    e.preventDefault();

    let th = new FormData(this);
    let sectors = $('#sectors').val();
    if (sectors == '' || sectors == null) {
        SWAL('error', 'Oops!', 'Please select sectors first.');
    } else {
        if (sectors == 1) {
            if ($('.sec-1').val() == '' || $('.mtt-1').val() == '' || $('.am-1').val() == null) {
                SWAL('error', 'Oops!', 'Missing input for Sector 1.');
            // } 

            // else if (!$('#img-1, this').attr('src')) {
                // SWAL('error', 'Oops!', 'Please capture image for sector first.');
            } else {
                onsave_audit(th);
            }
        } 
        
        else if (sectors == 2) {
            if ($('.sec-1').val() == '' || $('.mtt-1').val() == '' || $('.am-1').val() == null) {
                SWAL('error', 'Oops!', 'Missing input for Sector 1.');
            } 
            else if ($('.sec-2').val() == '' || $('.mtt-2').val() == '' || $('.am-2').val() == null) {
                SWAL('error', 'Oops!', 'Missing input for Sector 2.');
            // } 
            // else if (!$('#img-1, this').attr('src') || !$('#img-2, this').attr('src')) {
                // SWAL('error', 'Oops!', 'Please capture image for sector first.');
            } else {
                onsave_audit(th);
            }  
        }

        else if (sectors == 3) {
            if ($('.sec-1').val() == '' || $('.mtt-1').val() == '' || $('.am-1').val() == null) {
                SWAL('error', 'Oops!', 'Missing input for Sector 1.');
            } 
            else if ($('.sec-2').val() == '' || $('.mtt-2').val() == '' || $('.am-2').val() == null) {
                SWAL('error', 'Oops!', 'Missing input for Sector 2.');
            } 
            else if ($('.sec-3').val() == '' || $('.mtt-3').val() == '' || $('.am-3').val() == null) {
                SWAL('error', 'Oops!', 'Missing input for Sector 3.');
            // } 
            // else if (!$('#img-1, this').attr('src') || !$('#img-2, this').attr('src') || !$('#img-3, this').attr('src')) {
                // SWAL('error', 'Oops!', 'Please capture image for sector first.');
            } else {
                onsave_audit(th);
            }  
        }

        else if (sectors == 4) {
            if ($('.sec-1').val() == '' || $('.mtt-1').val() == '' || $('.am-1').val() == '') {
                SWAL('error', 'Oops!', 'Missing input for Sector 1.');
            } 
            else if ($('.sec-2').val() == '' || $('.mtt-2').val() == '' || $('.am-2').val() == '') {
                SWAL('error', 'Oops!', 'Missing input for Sector 2.');
            } 
            else if ($('.sec-3').val() == '' || $('.mtt-3').val() == '' || $('.am-3').val() == '') {
                SWAL('error', 'Oops!', 'Missing input for Sector 3.');
            } 
            else if ($('.sec-4').val() == '' || $('.mtt-4').val() == '' || $('.am-4').val() == '') {
                SWAL('error', 'Oops!', 'Missing input for Sector 4.');
            // } 
            // else if (!$('#img-1, this').attr('src') || !$('#img-2, this').attr('src') || !$('#img-3, this').attr('src') || !$('#img-4, this').attr('src')) {
                // SWAL('error', 'Oops!', 'Please capture image for sector first.');
            } else {
                onsave_audit(th);
            }
        }
    }
 });


function onsave_audit(th) {
    $('.btn-back, .btn-audit').attr('disabled', 'disabled');
    $('.btn-audit').html('<i class="fas fa-spinner fa-spin text-success"></i> Saving');
    $.ajax({
        url: url + 'Audit/save_audit',
        dataType: 'json',
        type: 'POST',
        data: th,
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        success:function(data) {
            $('.btn-back, .btn-audit').removeAttr('disabled');
            $('.btn-audit').html('Save');
            Swal.fire({
                title: data.title,
                text: data.msg,
                type: data.type,
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'OK',
                allowOutsideClick: false,
                allowEscapeKey: false
            }).then((result) => {
                if (result.value) {
                    location.reload(true);
                }
            });
        },
        error: function () {
           errorSwal();
        }
    })
}
