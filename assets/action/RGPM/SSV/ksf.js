// let tree = {
//   1: {
//       2: '',
//       3: {
//           6: '',
//           7: '',
//       },
//       4: '',
//       5: ''
//   }
// };

let tree = {
  1: {
      2: {
        4: '',
        5: '',
        6: '',
        7: '',
      },
      3: {
          8: '',
          9: '',
          10: '',
          11: '',
      },
  }
};

let params = {
  1: {trad: 'OVERALL TRFS'},
  2: {trad: 'VISAYAS'},
  3: {trad: 'MINDANAO'},
  4: {trad: 'NOKIA'},
  5: {trad: 'HUAWEI'},
  6: {trad: 'BAU'},
  7: {trad: '5G'},
  8: {trad: 'NOKIA'},
  9: {trad: 'HUAWEI'},
  10: {trad: 'BAU'},
  11: {trad: '5G'}
};

treeMaker(tree, {
  id: 'my_tree',
  card_click: function (element) {
      console.log(element);
  },
  treeParams: params,
  'link_width': '2px',
  'link_color': '#2199e8',
});