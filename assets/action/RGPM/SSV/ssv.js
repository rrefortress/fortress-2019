$(document).on('click', '.upload', function (e) {
	e.preventDefault();
	$('#bulk-upload-ssv').modal('show');
});

$(document).on('change', '#region', function (e) {
	e.preventDefault();
	console.log(this.value);
    get_ssv(this.value);
});

$(document).ready(function() {
    get_ssv('all');    
    get_callouts();
});

$(document).on('click', '#btnSubmit', function (e) {
	e.preventDefault();
    let callouts = $('#callouts').val();

    let param = {
		url: url + 'ssv_callouts',
		dataType: 'json',
        data: {callouts: callouts},
		type: 'POST'
	};

    fortress(param).then((data) => { 
        SWAL(data.type, data.title, data.msg);
        if (data.type === 'success') {get_callouts();}
    });
});

$(document).on("click", ".export_ssv", function (e) {
	e.preventDefault();
	$(this).prop('disabled', true);

	let param = {
		url: url + 'export_ssv',
		dataType: 'json',
		beforeSend: $(this).html('<em>Exporting <i class="fas fa-spinner fa-spin text-muted"></i></em>')
	};

	fortress(param).then((data) => {
		let today = moment().format("DD/MM/YYYY");
		if (data.result.length > 0) {
			JSONToCSVConvertor(data.result, "SSV_TRACKER_" + data.geoarea + "(" + today + ")", true);
		} else {
			SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
		}
		$(this).html('<i class="fas fa-download"></i> EXPORT').prop('disabled', false);
	});

});

function get_callouts() {
    let param = {
		url: url + 'get_callouts',
		dataType: 'json',
		type: 'GET'
	};
    fortress(param).then((data) => { 
        $('#callouts').text(data.callouts);
    });
}

function get_ssv(filter = 'all') {
    let param = {
		url: url + 'get_ssv',
		dataType: 'json',
        data: {region: filter},
		type: 'GET'
	};
	fortress(param).then((data) => {
        set_expansion_chart(data.chart.expansion);
        set_milestone_chart(data.chart.new_site);

        set_tbl_values(data.vendor_view.expansion.amdocs, 'amc', 'exp');
        set_tbl_values(data.vendor_view.expansion.ht, 'ht', 'exp');
        set_tbl_values(data.vendor_view.expansion.nokia, 'nsb', 'exp');
        set_tbl_values(data.vendor_view.expansion.gt, 'gt', 'exp');

        set_tbl_values(data.vendor_view.new_site.amdocs, 'amc', 'new');
        set_tbl_values(data.vendor_view.new_site.ht, 'ht', 'new');
        set_tbl_values(data.vendor_view.new_site.nokia, 'nsb', 'new');
        set_tbl_values(data.vendor_view.new_site.gt, 'gt', 'new');

        set_vendor_total(data.vendor_view.expansion.total, 'exp');
        set_vendor_total(data.vendor_view.new_site.total, 'new');

        set_score_trfs(data.chart.expansion, 'exp-ab','trfs');
        set_score_sub(data.chart.expansion, 'exp-ab', 'sub');

        set_score_trfs(data.chart.new_site, 'nsb-ab','trfs1');
        set_score_sub(data.chart.new_site, 'nsb-ab', 'sub1');
	});
}

function set_score_trfs(data, type, cl) {
    let exp = getWholePercent(data.SSV_SUBMIT_DATE, data.PD_TRFS);
    $('.'+cl+'-'+type).text(exp + '%');
    
    let html = exp >= 75 ? 'success' : (exp < 75 && exp >= 50 ? 'warning' : 'danger');
 
    $('.'+cl+'-remarks').html('<i class="shadow far fa-circle text-'+html+' bg-'+html+' rounded-circle"></i>')
}

function set_score_sub(data, type, cl) {
    let exp = getWholePercent(data.SSV_APPROVED_DATE, data.SSV_SUBMIT_DATE);
    $('.'+cl+'-'+type).text(exp + '%');

    let html = exp >= 75 ? 'success' : (exp < 75 && exp >= 50 ? 'warning' : 'danger');
 
    $('.'+cl+'-remarks').html('<i class="shadow far fa-circle text-'+html+' bg-'+html+' rounded-circle"></i>')
}

function set_tbl_values(data, val, cl) {
    $('.'+cl+'-'+val+'-'+'pd').text(data.PD_TRFS);
    $('.'+cl+'-'+val+'-'+'ssv').text(data.SSV_APPROVED_DATE);
    $('.'+cl+'-'+val+'-'+'sub').text(data.SSV_READY);
    $('.'+cl+'-'+val+'-'+'appr').text(data.SSV_SUBMIT_DATE);
}

function set_vendor_total(data, type) {
    $('.'+type+'-'+'total-pd').text(data.PD_TRFS);
    $('.'+type+'-'+'total-ssv').text(data.SSV_APPROVED_DATE);
    $('.'+type+'-'+'total-sub').text(data.SSV_READY);
    $('.'+type+'-'+'total-appr').text(data.SSV_SUBMIT_DATE);
}

function set_labels(val1, val2, cl1, cl2, total) {
    let value = val1 - val2;
    let perc = getWholePercent(value, total);

    $('.'+cl1).text(value);
    $('.'+cl2).text(perc + '%');
}

function set_expansion_chart(data = '') {
    let canvasBar = document.getElementById("expansion_chart");
    let ctxBar = canvasBar.getContext('2d');
    let val1 = parseInt(data.PD_TRFS);
    let val2 = parseInt(data.SSV_APPROVED_DATE);
    let val3 = parseInt(data.SSV_READY);
    let val4 = parseInt(data.SSV_SUBMIT_DATE);

    set_labels(val1, val2, 'exp-pd', 'exp-pd-perc', val1);
    set_labels(val2, val3, 'exp-ssv', 'exp-ssv-perc', val2);
    set_labels(val3, val4, 'exp-sub', 'exp-sub-perc', val3);

    let datas = {
        labels: ["PD TRFS APPROVE", "READY FOR SSV", "SSV SUBMISSION", "SSV APPROVAL"],
        datasets: [
            {
                axis: 'y',
                label: "",
                fill: true,
                backgroundColor: ['#f0a500', '#28a745', '#0779e4', '#d8345f'],
                data: [
                    val1,
                    val2,
                    val3,
                    val4
                ]
            }
        ]
    };

    let optionsBar = {
        plugins  : setPlugins_ssv(),
        // title : setTitle("Regional files upload chart : " + total),
        rotation : -0.7 * Math.PI,
        legend: {
            display: false
        },
        layout: {
            padding: {
                left: 10,
                right: 10,
                top: 10,
                bottom: 10
            }
        }
    };

    build_chart(datas, optionsBar, ctxBar, '', 'bar', 'expansions');
}

function set_milestone_chart(data = '') {
    let canvasBar = document.getElementById("milestone_chart");
    let ctxBar = canvasBar.getContext('2d');
    let val1 = parseInt(data.PD_TRFS);
    let val2 = parseInt(data.SSV_APPROVED_DATE);
    let val3 = parseInt(data.SSV_READY);
    let val4 = parseInt(data.SSV_SUBMIT_DATE);

    set_labels(val1, val2, 'new-pd', 'new-pd-perc', val1);
    set_labels(val2, val3, 'new-ssv', 'new-ssv-perc', val2);
    set_labels(val3, val4, 'new-sub', 'new-sub-perc', val3);

    let datas = {
        labels: [val1 +":PD TRFS APPROVE", val2 +": READY FOR SSV", val3 + ": SSV SUBMISSION", val4 + ": SSV APPROVAL"],
        datasets: [
            {
                label: "",
                fill: true,
                backgroundColor: ['#f0a500', '#28a745', '#0779e4', '#d8345f'],
                data: [
                    val1,
                    val2,
                    val3,
                    val4
                ]
            }
        ]
    };

    let optionsBar = {
        plugins  : setPlugins_ssv(),
        rotation : -0.7 * Math.PI,
        legend: {
            display: false
        },
        layout: {
            padding: {
                left: 10,
                right: 10,
                top: 10,
                bottom: 10
            }
        }
    };

    build_chart(datas, optionsBar, ctxBar, '', 'bar', 'milestone');
}

function EnableDisable(txtPassportNumber) {
    //Reference the Button.
    var btnSubmit = document.getElementById("btnSubmit");

    //Verify the TextBox value.
    if (txtPassportNumber.value.trim() != "") {
        //Enable the TextBox when TextBox has value.
        btnSubmit.disabled = false;
    } else {
        //Disable the TextBox when TextBox is empty.
        btnSubmit.disabled = true;
    }
};