Dropzone.autoDiscover = false;

let ssv_upload= new Dropzone(".dropzone_ssv",{
	url: url + "SSV/ssv_upload",
	timeout: 180000,
	parallelUploads: 100,
	maxFiles: 1,
	method:"POST",
	dataType: 'json',
	acceptedFiles:".csv",
	paramName:"ssv_file",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:true,
	init: function() {
		this.on("success", function(file, response) {
			data = jQuery.parseJSON(response);
			SWAL(data.type, data.title, data.msg);
			if (data.type === 'success') {
				get_ssv('all');
			}
		})
	}
});

ssv_upload.on("queuecomplete", function () {
	this.removeAllFiles();
	$("#bulk-upload").modal('hide');
	$("#bulk-upload-ssv").modal('hide');
});

ssv_upload.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});

ssv_upload.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});