let lte_tab = 'TDD';
let volte_tab = 'VOLTE-TDD';
let calendar = $('.kpi-calendar');

$(function () {
	get_2g_kpi();
	get_3g_kpi();
	get_4g_kpi();
	get_volte_kpi();
	get_5g_kpi();

	set_2g_values();
	set_3g_values();
	set_4g_values();
	set_volte_values();
	set_5g_values();
	$('.lte_tab').val(lte_tab);
	$('.volte_tab').val(volte_tab);
});

function set_2g_values() {
	$('#target_csfr_2g').val(1.1);
	$('#target_dcr_2g').val(0.7);
	$('#target_iafr_2g').val(1.7);
}

function set_3g_values() {
	$('#voice_csfr_3g').val(0.45);
	$('#voice_dcr_3g').val(0.5);
	$('#voice_sms_3g').val(2.0);

	$('#hsdpa_csfr_3g').val(1.25);
	$('#hsdpa_dcr_3g').val(0.7);
}

function set_4g_values() {
	$('#target_tdd_rrc').val(99.4);
	$('#target_tdd_erab').val(99.5);
	$('#target_tdd_intra').val(99);
	$('#target_tdd_dcr').val(0.3);

	$('#target_fdd_rrc').val(99.4);
	$('#target_fdd_erab').val(99.5);
	$('#target_fdd_intra').val(99);
	$('#target_fdd_dcr').val(0.3);
}

function set_volte_values() {
	$('#volte-target_tdd_estab').val(99);
	$('#volte-target_tdd_erab').val(99);
	$('#volte-target_tdd_sdvcc').val(99);
	$('#volte-target_tdd_dcr').val(0.5);

	$('#volte-target_fdd_estab').val(99);
	$('#volte-target_fdd_erab').val(99);
	$('#volte-target_fdd_sdvcc').val(99);
	$('#volte-target_fdd_dcr').val(0.5);
}

function set_5g_values() {
	$('#target_sgnb_sr_5g').val(98.5);
	$('#target_sgnb_rd_5g').val(1.2);
	$('#target_sgnb_ro_5g').val(1.2);
}

function callCalendar(link = '', type = '') {
	$.ajax({
		url: url + link,
		dataType: 'json',
		type: "POST",
		data : {type: type},
		beforeSend() {
			$('.loaders').removeClass('d-none');
			calendar.addClass('d-none');
		},
		success: function(data) {
			let datas = [];
			if (data.length > 0) {
				_.forEach(data, function (val) {
					let json = {
						title: 'DONE UPLOAD',
						start:val.date_upload,
						backgroundColor : '#46b3e6',
						textColor: '#000000',
						borderColor: '#46b3e6',
					};

					datas.push(json);
				});

			}
			$('.loaders').addClass('d-none');
			calendar.removeClass('d-none');
			calendar.fullCalendar('updateEvent', calendar);
			setCalendar(datas);
			calendar.fullCalendar('refresh', calendar);
		},
		error: function(xhr, status, error) {
			errorSwal();
		}
	});
}

function setCalendar(data) {
	$('.fc-day ').css({"background-color": "#ddd", "text-align": "center"});
	calendar.fullCalendar({
		height: $(window).height()*0.83,
		viewDisplay: resizeCalendar,
		timeZone: 'UTC',
		themeSystem: 'jquery-ui',
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay,listMonth'
		},
		editable: false,
		events: data,
		eventRender: function (event, element, view) {
			let dateString = moment(event.start, 'DD.MM.YYYY').format('YYYY-MM-DD');
			$(view.el[0]).find('.fc-day[data-date="' + dateString + '"]').css(
				{"background-color": "#46b3e6", "text-align": "center"});
		},
		resources: data,
		displayEventTime : false,
		eventLimit: true,
		views: {
			timeGrid: {
				eventLimit: 6 // adjust to 6 only for timeGridWeek/timeGridDay
			}
		}
	});
}

function resizeCalendar(calendarView) {
	if(calendarView.name === 'agendaWeek' || calendarView.name === 'agendaDay') {
		// if height is too big for these views, then scrollbars will be hidden
		calendarView.setHeight(9999);
	}
}

function empty_kpi() {
	return '<div class="col-12 mg-top-135 text-center">' +
		'<h5 class="text-center text-white">' +
		'   <i class="fas fa-check-circle rounded-circle shadow-sm display-2 text-success"></i></br>' +
		'	<h4 class="text-muted lead text-center">Area KPI is within the network Target.</h4>' +
		'</h5>' +
		'</div>';
}
