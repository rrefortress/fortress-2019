$(document).on('click', '.clear2g', function () {
	Swal.fire({
		title: 'Are you sure?',
		text: "You want to delete all item/s and it's histories?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	}).then((result) => {
		if (result.value) {
			let param = {
				url: url + 'clear_2g',
				type: "post",
				dataType: 'json',
				data: {ktype : ktype},
			};

			fortress(param).then((data) => {
				SWAL(data.type, data.title, data.msg);
				get_2g_kpi();
			});
		}
	});
});
$(document).on('click', '.kpi2g_upload', function (e) {
	e.preventDefault();
	$('#kpi2g_upload').modal('show');
});
Dropzone.autoDiscover = false;
let kpi2g_upload = new Dropzone(".dropzone2g_KPI",{
	url: url + "KPI/kpi2g_upload",
	timeout: 180000,
	parallelUploads: 1,
	maxFiles: 1,
	method:"POST",
	acceptedFiles:".csv",
	paramName:"kpi",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:false,
	init: function() {
		this.on("sending", function(file, xhr, formData){
			formData.append("ktype", ktype);
		});
		this.on("success", function(file, response) {
			let data = jQuery.parseJSON(response);
			if (data.type === 'error') {} else {
				// get_2g_kpi(data.date);
				$('#date2g_csfr_select, #date2g_dcr_select, #date2g_iafr_select').val(data.date);
				set_2g_values();
			}
			SWAL(data.type, data.title, data.msg);
		})
	}
});

kpi2g_upload.on("queuecomplete", function () {
	this.removeAllFiles();
	$('#kpi2g_upload').modal('hide');
});
kpi2g_upload.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});
kpi2g_upload.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});
//--------------------------------- SEARCH 2G ------------------------------------------------------------------------------------//
$(document).on('keyup', '#target_csfr_2g', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#date2g_csfr_select').val();
	get_2g_man_kpi('csfr', date, 'get_2g_csfr', target);
}, 3000));

$(document).on('keyup', '#target_dcr_2g', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#date2g_dcr_select').val();
	get_2g_man_kpi('dcr', date, 'get_2g_dcr', target);
}, 3000));

$(document).on('keyup', '#target_iafr_2g', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#date2g_iafr_select').val();
	get_2g_man_kpi('iafr', date, 'get_2g_iafr', target);
}, 3000));
//----------------------------------------------------------FILTER BY DATE (CSFR, DCR & IAFR)-------------------------------------//
$(function() {
	$('#date2g_csfr_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#target_csfr_2g').val();
		let date = $(this).val();
		get_2g_man_kpi('csfr', date, 'get_2g_csfr', target);
	});
	$('#date2g_dcr_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#target_dcr_2g').val();
		let date = $(this).val();
		get_2g_man_kpi('dcr', date, 'get_2g_dcr', target);
	});
	$('#date2g_iafr_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#target_iafr_2g').val();
		let date = $(this).val();
		get_2g_man_kpi('iafr', date, 'get_2g_iafr', target);
	});
});
//---------------------------------------------------------------------------------------------------------------------------------//
function get_2g_man_kpi(val = '', date = '', link = '', target = 0) {
	let param = {
		url: url + link,
		dataType: 'json',
		type: 'POST',
		data: {target: target, date: date, ktype : ktype},
		beforeSend: $('.'+val+'_2g_tbl').html('<div class="col-12 mg-top-100">' +
			'<h5 class="text-center text-white">' +
			'<img src='+loaders+' width="150" height="150" class="rounded-circle" alt="Loader"><br>' +
			'<small class="text-center text-muted pt-4"><em>Loading data...</em></small>' +
			'</h5>' +
			'</div>')
	};

	fortress(param).then((data) => {
		set_2g_table(data, val);
	});
}

function get_2g_kpi(date = '') {
	if (date === '') {
		_.forEach(['csfr', 'dcr', 'iafr'], function (val, key) {
			$('.'+val+'_2g_tbl').html(empty_kpi());
			$('.'+val+'_2g_total').text(0);
		});
	} else {
		let param = {
			url: url + 'get_2g_kpi',
			dataType: 'json',
			type: 'POST',
			data: {date: date, ktype : ktype},
			beforeSend: $('.csfr_2g_tbl, .dcr_2g_tbl, .iafr_2g_tbl').html('<div class="col-12 mg-top-100">' +
				'<h5 class="text-center text-white">' +
				'<img src='+loaders+' width="150" height="150" class="rounded-circle" alt="Loader"><br>' +
				'<small class="text-center text-muted pt-4"><em>Loading data...</em></small>' +
				'</h5>' +
				'</div>')
		};

		fortress(param).then((data) => {
			set_2g_table(data.csfr,  'csfr');
			set_2g_table(data.dcr,  'dcr');
			set_2g_table(data.iafr,  'iafr');
		});
	}
}

let csfr_2g = [];
let dcr_2g = [];
let iafr_2g = [];

function set_2g_table(data, type) {
	switch (type.toUpperCase()) {
		case 'CSFR': csfr_2g = data; break;
		case 'DCR': dcr_2g = data; break;
		case 'IAFR': iafr_2g = data; break;
	}
	let html = '';
	if (data.length > 0) {
		html += '<table class="table-bordered force-fit small display dt-responsive table-sm '+type+'-tbl table mb-0 table-sm nowrap table-hover table-striped" style="font-size: 11px !important;">\n' +
			'<thead class="global_color text-white">\n' +
			'<tr>\n' +
			'<th scope="col" class="text-center">RANK</th>\n' +
			'<th scope="col" class="text-center">CELLNAME ('+type.toUpperCase()+')</th>\n' +
			'<th scope="col" class="text-center">ALARMS / Availability</th>\n' +
			'<th scope="col" class="text-center">CAPACITY</th>\n' +
			'<th scope="col" class="text-center">COVERAGE</th>\n' +
			'<th scope="col" class="text-center" colspan="3">QUALITY (Interference)</th>\n' +
			'<th scope="col" class="text-center">CONTRIBUTORS</th>\n' +
			'<th scope="col" class="text-center">AREA KPI</th>\n' +
			'</tr>\n' +
			'</thead>\n' +
			'<tbody class='+type+'_list>';

		_.forEach(data, function (val) {
			html += '<tr>\n' +
				'      <th class="text-center align-middle" scope="row">'+val.rank+'</th>\n' +
				'      <td class="text-center align-middle">'+val.CELLS+'</td>\n' +
				'      <td class="text-right align-middle">' +
				'		2G Availability = <b>'+val.NP_2G_CELL_AVAIL+'</b> <br>' +
				'       TRX Availability = <b>'+val.NP_2G_TRX_AVAIL+'</b> <br>' +
				'       SDCCH TRaffic = <b>'+val.NP_2G_SD_TRAFF+'</b> <br>' +
				'       TCH Traffic = <b>' +val.NP_2G_TCH_TRAFF +'</b>' +
				'		</td>\n' +
				'      <td class="text-right align-middle">' +
				'		SDCCH Blocking =  <b>'+val.NP_2G_SBLK+'% </b><br>' +
				'		SDCCH Drop =  <b>'+val.NP_2G_SDROP+'% </b><br>' +
				'		TCH Blocking = <b>'+val.NP_2G_TBLK+'% </b><br>' +
				'		TCHFR Loading = <b>'+val.NP_TCHFR_Loading+'%</b>'+
				'       </td>\n' +
				'      <td class="text-right align-middle">' +
				'	   0 - 1 KM = <b>' +val.KM01+'</b> <br>' +
				'	   1 - 3 KM = <b>' +val.KM13+'</b> <br>' +
				'	   3 - 5 KM = <b>' +val.KM35+'</b> <br>' +
				'	   5 KM = <b>' +val.KM5+'<b>' +
				'      </td>\n' +
				'    <td class="text-right align-middle">' +
				'	   SD 1 = <b>' +val.sd1+'%</b> <br>' +
				'	   SD 2 = <b>' +val.sd2+'%</b> <br>' +
				'	   SD 3 = <b>' +val.sd3+'%</b> <br>' +
				'	   SD 4 = <b>' +val.sd4+'%</b> <br>' +
				'	   SD 5 = <b>' +val.sd5+'%</b> <br>' +
				'	</td>\n' +
				'    <td class="text-right align-middle">' +
				'	   TCHFR 1 = <b>' +val.fr1+'%</b> <br>' +
				'	   TCHFR 2 = <b>' +val.fr2+'%</b> <br>' +
				'	   TCHFR 3 = <b>' +val.fr3+'%</b> <br>' +
				'	   TCHFR 4 = <b>' +val.fr4+'%</b> <br>' +
				'	   TCHFR 5 = <b>' +val.fr5+'%</b> <br>' +
				'	</td>\n' +
				'      <td class="text-right align-middle">' +
				'	   TCHHR 1 = <b>' +val.hr1+'%</b> <br>' +
				'	   TCHHR 2 = <b>' +val.hr2+'%</b> <br>' +
				'	   TCHHR 3 = <b>' +val.hr3+'%</b> <br>' +
				'	   TCHHR 4 = <b>' +val.hr4+'%</b> <br>' +
				'	   TCHHR 5 = <b>' +val.hr5+'%</b> <br>' +
				'	</td>\n' +
				'      <td class="text-center align-middle">'+val.contributor+'%</td>\n' +
				'      <td class="text-center align-middle">'+val.kpi+'%</td>\n' +
				'    </tr>';
		});
		html += '</tbody>\n' +
			'</table>';
	} else {
		html = empty_kpi();
	}

	$('.'+type+'_2g_tbl').html(html);
	$('.'+type+'_2g_total').text(data.length);
	if (data.length > 0) {
		$('.'+type+'-tbl').dataTable({
			"lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
		});
	}
}

$(document).on('click', '.view_2g_calendar', function (e) {
	e.preventDefault();
	$('.kpi-2g-sched').modal('show');
	callCalendar('get_2g_calendar', ktype);
});

$(document).on("click", ".export_csfr_2g", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (csfr_2g.length > 0) {
		JSONToCSVConvertor(csfr_2g, "2G-CSFR-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".export_dcr_2g", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (dcr_2g.length > 0) {
		JSONToCSVConvertor(dcr_2g, "2G-DCR-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".export_iafr_2g", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (iafr_2g.length > 0) {
		JSONToCSVConvertor(iafr_2g, "2G-IAFR-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".csfr-tbl tbody tr, .iafr-tbl tbody tr, .dcr-tbl tbody tr", function (e) {
	e.preventDefault();
	let cell_2g = [];
	let cell_name = $(this).find('td:eq(0)').text();
	cell_2g.push(cell_name);
	$('.ana-cell-name').text(cell_name);
	wcl2g = true;
	get_filter_menus('2g');
	get_2g_graph({cell: cell_2g, type: 'cell'});
	$('.view-2g-analyzer').modal('show');
});
