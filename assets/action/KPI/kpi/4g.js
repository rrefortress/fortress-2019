$('.lte_tab a[data-toggle="tab"]').on('shown.bs.tab', function (event) {
	lte_tab = event.target.href.split('#')[1].toUpperCase();
});

$(document).on('click', '.kpi4g_upload', function (e) {
	e.preventDefault();
	$('#kpi4g_upload').modal('show');
});

Dropzone.autoDiscover = false;
let kpi4g_upload = new Dropzone(".dropzone4g_KPI",{
	url: url + "KPI/kpi4g_upload",
	timeout: 180000,
	parallelUploads: 50,
	maxFiles: 1,
	method:"POST",
	acceptedFiles:".csv",
	paramName:"kpi",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:true,
	init: function() {
		this.on("sending", function(file, xhr, formData){
			formData.append("tab", lte_tab);
			formData.append("ktype", ktype);
		});
		this.on("success", function(file, response) {
			let data = jQuery.parseJSON(response);
			if (data.type === 'error') {} else {
				// get_4g_kpi(lte_tab, data.date);
				$('#date_tdd_rrc_select, #date_tdd_erab_select, #date_tdd_intra_select, #date_tdd_dcr_select, #date_fdd_rrc_select, #date_fdd_erab_select, #date_fdd_intra_select, #date_fdd_dcr_select').val(data.date);
				set_4g_values();
			}
			SWAL(data.type, data.title, data.msg);
		});
	}
});

kpi4g_upload.on("queuecomplete", function () {
	this.removeAllFiles();
	$('#kpi4g_upload').modal('hide');
});
kpi4g_upload.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});
kpi4g_upload.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});
//---------------------------------------------------------------------------------------------------------------------------------//
$(document).on('click', '.clear4g', function () {
	Swal.fire({
		title: 'Are you sure?',
		text: "You want to delete all item/s and it's histories?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	}).then((result) => {
		if (result.value) {
			let param = {
				url: url + 'clear_4g',
				type: "post",
				dataType: 'json',
				data: {ktype : ktype},
			};

			fortress(param).then((data) => {
				SWAL(data.type, data.title, data.msg);
				get_4g_kpi();
			});
		}
	});
});
//--------------------------------- SEARCH 2G ------------------------------------------------------------------------------------//
$(document).on('keyup', '#target_tdd_rrc', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#date_tdd_rrc_select').val();
	get_4g_man_kpi('rrc', date, 'get_rrc', 'tdd', target);
}, 3000));

$(document).on('keyup', '#target_tdd_erab', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#date_tdd_erab_select').val();
	get_4g_man_kpi('erab', date, 'get_erab', 'tdd', target);
}, 3000));

$(document).on('keyup', '#target_tdd_intra', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#date_tdd_intra_select').val();
	get_4g_man_kpi('intra', date, 'get_intra', 'tdd', target);
}, 3000));

$(document).on('keyup', '#target_tdd_dcr', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#date_tdd_dcr_select').val();
	get_4g_man_kpi('dcr', date, 'get_4g_dcr', 'tdd', target);
}, 3000));

$(document).on('keyup', '#target_fdd_rrc', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#date_fdd_rrc_select').val();
	get_4g_man_kpi('rrc', date, 'get_rrc', 'fdd', target);
}, 3000));

$(document).on('keyup', '#target_fdd_erab', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#date_fdd_erab_select').val();
	get_4g_man_kpi('erab', date, 'get_erab', 'fdd', target);
}, 3000));

$(document).on('keyup', '#target_fdd_intra', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#date_fdd_intra_select').val();
	get_4g_man_kpi('intra', date, 'get_intra', 'fdd', target);
}, 3000));

$(document).on('keyup', '#target_fdd_dcr', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#date_fdd_dcr_select').val();
	get_4g_man_kpi('dcr', date, 'get_4g_dcr', 'fdd', target);
}, 3000));
//---------------------------------------------------------------------------------------------------------------------------------//

//----------------------------------------------------------FILTER BY DATE (CSFR, DCR & IAFR)-------------------------------------//
$(function() {
	$('#date_tdd_rrc_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#target_tdd_rrc').val();
		let date = $(this).val();
		get_4g_man_kpi('rrc', date, 'get_rrc', 'tdd', target);
	});
	$('#date_tdd_erab_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#target_tdd_erab').val();
		let date = $(this).val();
		get_4g_man_kpi('erab', date, 'get_erab', 'tdd', target);
	});
	$('#date_tdd_intra_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#target_tdd_intra').val();
		let date = $(this).val();
		get_4g_man_kpi('intra', date, 'get_intra', 'tdd', target);
	});
	$('#date_tdd_dcr_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#target_tdd_dcr').val();
		let date = $(this).val();
		get_4g_man_kpi('dcr', date, 'get_4g_dcr', 'tdd', target);
	});

	$('#date_fdd_rrc_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#target_fdd_rrc').val();
		let date = $(this).val();
		get_4g_man_kpi('rrc', date, 'get_rrc', 'fdd', target);
	});
	$('#date_fdd_erab_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#target_fdd_erab').val();
		let date = $(this).val();
		get_4g_man_kpi('erab', date, 'get_erab', 'fdd', target);
	});
	$('#date_fdd_intra_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#target_fdd_intra').val();
		let date = $(this).val();
		get_4g_man_kpi('intra', date, 'get_intra', 'fdd', target);
	});
	$('#date_fdd_dcr_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#target_fdd_dcr').val();
		let date = $(this).val();
		get_4g_man_kpi('dcr', date, 'get_4g_dcr', 'fdd', target);
	});
});
//---------------------------------------------------------------------------------------------------------------------------------//
function get_4g_man_kpi(val = '', date = '', link = '', tab = '', target = 0) {
	let param = {
		url: url + link,
		dataType: 'json',
		type: 'POST',
		data: {target: target, date: date, tab: tab, ktype : ktype},
		beforeSend: $('.'+tab+'_'+val+'_tbl').html('<div class="col-12 mg-top-100">' +
			'<h5 class="text-center text-white">' +
			'<img src='+loaders+' width="150" height="150" class="rounded-circle" alt="Loader"><br>' +
			'<small class="text-center text-muted pt-4"><em>Loading data...</em></small>' +
			'</h5>' +
			'</div>')
	};

	fortress(param).then((data) => {
		set_4g_table(data, tab, val);
	});
}

function get_4g_kpi(tab = '', date = '') {
	if (date === '') {
		$('.tdd_rrc_tbl, .tdd_erab_tbl, .tdd_intra_tbl, .tdd_dcr_tbl').html(empty_kpi());
		$('.fdd_rrc_tbl, .fdd_erab_tbl, .fdd_intra_tbl, .fdd_dcr_tbl').html(empty_kpi());
	} else {
		let loader = tab === '' ? 'loader_4g' : 'loader_'+tab.toLowerCase();
		let param = {
			url: url + 'get_4g_kpi',
			dataType: 'json',
			type: 'POST',
			data: {target: '', date: '', tab: tab, ktype : ktype},
			beforeSend: $('.'+loader).html('<div class="col-12 mg-top-100">' +
				'<h5 class="text-center text-white">' +
				'<img src='+loaders+' width="150" height="150" class="rounded-circle" alt="Loader"><br>' +
				'<small class="text-center text-muted pt-4"><em>Loading data...</em></small>' +
				'</h5>' +
				'</div>')
		};

		fortress(param).then((data) => {
			if (tab === '') {
				set_4g_table(data.tdd.rrc, 'tdd', 'rrc');
				set_4g_table(data.tdd.erab, 'tdd', 'erab');
				set_4g_table(data.tdd.intra, 'tdd', 'intra');
				set_4g_table(data.tdd.dcr, 'tdd', 'dcr');

				set_4g_table(data.fdd.rrc, 'fdd', 'rrc');
				set_4g_table(data.fdd.erab, 'fdd', 'erab');
				set_4g_table(data.fdd.intra, 'fdd', 'intra');
				set_4g_table(data.fdd.dcr, 'fdd', 'dcr');
			} else if (tab === 'TDD') {
				set_4g_table(data.tdd.rrc, 'tdd', 'rrc');
				set_4g_table(data.tdd.erab, 'tdd', 'erab');
				set_4g_table(data.tdd.intra, 'tdd', 'intra');
				set_4g_table(data.tdd.dcr, 'tdd', 'dcr');
			} else if (tab === 'FDD') {
				set_4g_table(data.fdd.rrc, 'fdd', 'rrc');
				set_4g_table(data.fdd.erab, 'fdd', 'erab');
				set_4g_table(data.fdd.intra, 'fdd', 'intra');
				set_4g_table(data.fdd.dcr, 'fdd', 'dcr');
			}
		});
	}
}

let tdd_rrc = [];
let tdd_erab = [];
let tdd_intra = [];
let tdd_dcr = [];

let fdd_rrc = [];
let fdd_erab = [];
let fdd_intra = [];
let fdd_dcr = [];

function set_4g_table(data, tab, type) {
	if (tab === 'tdd') {
		switch (type.toUpperCase()) {
			case 'RRC': tdd_rrc = data; break;
			case 'ERAB': tdd_erab = data; break;
			case 'INTRA': tdd_intra = data; break;
			case 'DCR': tdd_dcr = data; break;
		}
	} else {
		switch (type.toUpperCase()) {
			case 'RRC': fdd_rrc = data; break;
			case 'ERAB': fdd_erab = data; break;
			case 'INTRA': fdd_intra = data; break;
			case 'DCR': fdd_dcr = data; break;
		}
	}

	let html = '';
	if (data.length > 0) {
		html += '<table class="table-bordered force-fit small display dt-responsive table-sm '+type+'-'+tab+'-tbl table mb-0 table-sm nowrap table-hover table-striped">\n' +
			'<thead class="global_color text-white">\n' +
			'<tr>\n' +
			'<th scope="col" class="text-center">RANK</th>\n' +
			'<th scope="col" class="text-center">CELLNAME ('+type.toUpperCase()+')</th>\n' +
			'<th scope="col" class="text-center">ALARMS / Availability</th>\n' +
			'<th scope="col" class="text-center">CAPACITY</th>\n' +
			'<th scope="col" class="text-center">COVERAGE</th>\n' +
			'<th scope="col" class="text-center">QUALITY (Interference)</th>\n' +
			// '<th scope="col" class="text-center">Contributor</th>\n' +
			'<th scope="col" class="text-center">AREA KPI</th>\n' +
			'</tr>\n' +
			'</thead>\n' +
			'<tbody class='+type+'_'+tab+'_list>';

		_.forEach(data, function (val) {
			html += '<tr>\n' +
				'      <th class="text-center align-middle" scope="row">'+val.Rank+'</th>\n' +
				'      <td class="text-left align-middle">'+val.Cellname+'</td>\n' +
				'      <td class="text-center align-middle">'+val.availability+'%</td>\n' +
				'      <td class="text-right align-middle">' +
				'       LTE Active User = '+val.lte_active_user+' <br>' +
				'       DL PRB Utilization = '+val.dl_prb_util+' <br>' +
				'       </td>\n' +
				'      <td class="text-right align-middle">' +
				'		Timing Advance (TA) <br>' +
				'		TA0 - '+val.ta0+' <br>' +
				'		TA1 - '+val.ta1+' <br>' +
				'		TA2 - '+val.ta2+' <br>' +
				'		TA3 - '+val.ta3+' <br>' +
				'		TA4 - '+val.ta4+' <br>' +
				'		TA5 - '+val.ta5+' <br>' +
				'		TA6 - '+val.ta6+' <br>' +
				'		TA7 - '+val.ta7+' <br>' +
				'		TA8 - '+val.ta8+' <br>' +
				'		TA9 - '+val.ta9+' <br>' +
				'		TA10 - '+val.ta10+' <br>' +
				'		TA11 - '+val.ta11+' <br>' +
				'      </td>\n' +
				'      <td class="text-right align-middle">' +
				'       Interference <br>' +
				'		</td>\n' +
				// '      <td class="text-right align-middle">' +val.contributor + '</td>\n' +
				'      <td class="text-center align-middle">'+val.kpi+'%</td>\n' +
				'    </tr>';
		});
		html += '</tbody>\n' +
			'</table>';
	} else {
		html = empty_kpi();
	}

	$('.'+tab+'_'+type+'_tbl').html(html);
	$('.'+tab+'_'+type+'_total').html(data.length);
	if (data.length > 0) {
		$('.'+type+'-'+tab+'-tbl').dataTable({
			"lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
		});}
}

$(document).on('click', '.view_4g_calendar', function (e) {
	e.preventDefault();
	$('.kpi-4g-sched').modal('show');
	callCalendar('get_4g_calendar', ktype);
});

$(document).on("click", ".export_tdd_rrc", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (tdd_rrc.length > 0) {
		JSONToCSVConvertor(tdd_rrc, "4G-TDD-RRC-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".export_tdd_erab", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (tdd_erab.length > 0) {
		JSONToCSVConvertor(tdd_erab, "4G-TDD-ERAB-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".export_tdd_intra", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (tdd_intra.length > 0) {
		JSONToCSVConvertor(tdd_intra, "4G-TDD-INTRA-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".export_tdd_dcr", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (tdd_dcr.length > 0) {
		JSONToCSVConvertor(tdd_dcr, "4G-TDD-INTRA-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".export_fdd_rrc", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (fdd_rrc.length > 0) {
		JSONToCSVConvertor(fdd_rrc, "4G-FDD-RRC-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".export_fdd_erab", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (fdd_erab.length > 0) {
		JSONToCSVConvertor(fdd_erab, "4G-FDD-ERAB-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".export_fdd_intra", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (fdd_intra.length > 0) {
		JSONToCSVConvertor(fdd_intra, "4G-FDD-INTRA-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".export_fdd_dcr", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (fdd_dcr.length > 0) {
		JSONToCSVConvertor(fdd_dcr, "4G-FDD-DCR-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".tdd_rrc_tbl tbody tr, .tdd_erab_tbl tbody tr, .tdd_intra_tbl tbody tr, .tdd_dcr_tbl tbody tr, .fdd_rrc_tbl tbody tr, .fdd_erab_tbl tbody tr, .fdd_intra_tbl tbody tr, .fdd_dcr_tbl tbody tr", function (e) {
	e.preventDefault();
	let cell_4g = [];
	let cell_name = $(this).find('td:eq(0)').text();
	cell_4g.push(cell_name);

	$('.ana-cell-name').text(cell_name);
	wcl4g = true;
	get_filter_menus('4g');
	get_4g_graph({cell: cell_4g, type: 'cell'});
	$('.view-4g-analyzer').modal('show');
});
