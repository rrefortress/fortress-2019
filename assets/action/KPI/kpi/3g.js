$(document).on('click', '.kpi3g_upload', function (e) {
	e.preventDefault();
	$('#kpi3g_upload').modal('show');
});

Dropzone.autoDiscover = false;
let kpi3g_upload = new Dropzone(".dropzone3g_KPI",{
	url: url + "KPI/kpi3g_upload",
	timeout: 180000,
	parallelUploads: 50,
	maxFiles: 1,
	method:"POST",
	acceptedFiles:".csv",
	paramName:"kpi",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:true,
	init: function() {
		this.on("sending", function(file, xhr, formData){
			formData.append("ktype", ktype);
		});
		this.on("success", function(file, response) {
			let data = jQuery.parseJSON(response);
			if (data.type === 'error') {} else {
				// get_3g_kpi(data.date);
				$('#datev_csfr_select, #datev_dcr_select, #datev_sms_select, #dateh_csfr_select, #dateh_dcr_select').val(data.date);
				set_3g_values();
			}
			SWAL(data.type, data.title, data.msg);
		})
	}
});

kpi3g_upload.on("queuecomplete", function () {
	this.removeAllFiles();
	$('#kpi3g_upload').modal('hide');
});
kpi3g_upload.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});
kpi3g_upload.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});
//---------------------------------------------------------------------------------------------------------------------------------//
$(document).on('click', '.clear3g', function () {
	Swal.fire({
		title: 'Are you sure?',
		text: "You want to delete all item/s and it's histories?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	}).then((result) => {
		if (result.value) {
			let param = {
				url: url + 'clear_3g',
				type: "post",
				dataType: 'json',
				data: {ktype : ktype},
			};

			fortress(param).then((data) => {
				SWAL(data.type, data.title, data.msg);
				get_3g_kpi();
			});
		}
	});
});
//--------------------------------- SEARCH 3G ------------------------------------------------------------------------------------//
$(document).on('keyup', '#voice_csfr_3g', _.debounce(function (e) {
	e.preventDefault();
	let target = $('#voice_csfr_3g').val();
	let date = $('#datev_csfr_select').val();
	get_3g_man_kpi('csfr', date, 'get_voice_csfr', 'voice', target);
}, 3000));

$(document).on('keyup', '#voice_dcr_3g', _.debounce(function (e) {
	e.preventDefault();
	let target = $('#voice_dcr_3g').val();
	let date = $('#datev_dcr_select').val();
	get_3g_man_kpi('dcr', date, 'get_voice_dcr', 'voice' , target);
}, 3000));

$(document).on('keyup', '#voice_sms_3g', _.debounce(function (e) {
	e.preventDefault();
	let target = $('#voice_sms_3g').val();
	let date = $('#datev_sms_select').val();
	get_3g_man_kpi('sms', date, 'get_voice_sms', 'voice', target);
}, 3000));

$(document).on('keyup', '#hsdpa_csfr_3g', _.debounce(function (e) {
	e.preventDefault();
	let target = $('#hsdpa_csfr_3g').val();
	let date = $('#dateh_csfr_select').val();
	get_3g_man_kpi('csfr', date, 'get_hsdpa_csfr', 'hsdpa', target);
}, 3000));

$(document).on('keyup', '#hsdpa_dcr_3g', _.debounce(function (e) {
	e.preventDefault();
	let target = $('#hsdpa_dcr_3g').val();
	let date = $('#dateh_dcr_select').val();
	get_3g_man_kpi('dcr', date, 'get_hsdpa_dcr', 'hsdpa', target);
}, 3000));
//---------------------------------------------------------------------------------------------------------------------------------//

//----------------------------------------------------------FILTER BY DATE (CSFR, DCR & IAFR)-------------------------------------//
$(function() {
	$('#datev_csfr_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#voice_csfr_3g').val();
		let date = $(this).val();
		get_3g_man_kpi('csfr', date, 'get_voice_csfr', 'voice', target);
	});
	$('#datev_dcr_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#voice_dcr_3g').val();
		let date = $(this).val();
		get_3g_man_kpi('dcr', date, 'get_voice_dcr', 'voice', target);
	});
	$('#datev_sms_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#voice_sms_3g').val();
		let date = $(this).val();
		get_3g_man_kpi('sms', date, 'get_voice_sms', 'voice', target);
	});

	$('#dateh_csfr_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#hsdpa_csfr_3g').val();
		let date = $(this).val();
		get_3g_man_kpi('csfr', date, 'get_hsdpa_csfr', 'hsdpa', target);
	});

	$('#dateh_dcr_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#hsdpa_dcr_3g').val();
		let date = $(this).val();
		get_3g_man_kpi('dcr', date, 'get_hsdpa_dcr', 'hsdpa', target);
	});
});
//---------------------------------------------------------------------------------------------------------------------------------//
function get_3g_man_kpi(val = '', date = '', link = '', tab = '', target = 0) {
	let param = {
		url: url + link,
		dataType: 'json',
		type: 'POST',
		data: {target: target, date: date, ktype : ktype},
		beforeSend: $('.'+val+'_'+tab+'_tbl').html('<div class="col-12 mg-top-100">' +
			'<h5 class="text-center text-white">' +
			'<img src='+loaders+' width="150" height="150" class="rounded-circle" alt="Loader"><br>' +
			'<small class="text-center text-muted pt-4"><em>Loading data...</em></small>' +
			'</h5>' +
			'</div>')
	};

	fortress(param).then((data) => {
		set_3g_table(data, tab, val);
	});
}

function get_3g_kpi(date = '') {
	if (date === '') {
		_.forEach(['csfr_voice', 'dcr_voice', 'sms_voice', 'csfr_hsdpa', 'dcr_hsdpa'], function (val, key) {
			$('.'+val+'_tbl').html(empty_kpi());
			$('.'+val+'_total').text(0);
		});
	} else {
		let param = {
			url: url + 'get_3g_kpi',
			dataType: 'json',
			type: 'POST',
			data: {date: date, ktype : ktype},
			beforeSend: $('.loader_3g').html('<div class="col-12 mg-top-100">' +
				'<h5 class="text-center text-white">' +
				'<img src='+loaders+' width="150" height="150" class="rounded-circle" alt="Loader"><br>' +
				'<small class="text-center text-muted pt-4"><em>Loading data...</em></small>' +
				'</h5>' +
				'</div>')
		};
		fortress(param).then((data) => {
			set_3g_table(data.voice.csfr, 'voice', 'csfr');
			set_3g_table(data.voice.dcr, 'voice', 'dcr');
			set_3g_table(data.voice.sms, 'voice', 'sms');

			set_3g_table(data.hsdpa.csfr, 'hsdpa', 'csfr');
			set_3g_table(data.hsdpa.dcr, 'hsdpa', 'dcr');
		});
	}
}

let voice_csfr_3g = [];
let voice_dcr_3g = [];
let voice_sms_3g = [];

let hspda_csfr_3g = [];
let hspda_dcr_3g = [];

function set_3g_table(data, tab, type) {
	let ttype = tab === 'voice' ? 'CS' : 'PS';
	if (tab === 'voice') {
		switch (type.toUpperCase()) {
			case 'CSFR': voice_csfr_3g = data; break;
			case 'DCR': voice_dcr_3g = data; break;
			case 'SMS': voice_sms_3g = data; break;
		}
	} else {
		switch (type.toUpperCase()) {
			case 'CSFR': hspda_csfr_3g = data; break;
			case 'DCR': hspda_dcr_3g = data; break;
		}
	}


	let html = '';
	if (data.length > 0) {
		html += '<table class="table-bordered force-fit small display dt-responsive table-sm '+type+'-'+tab+'-tbl table mb-0 table-sm nowrap table-hover table-striped">\n' +
			'<thead class="global_color text-white">\n' +
			'<tr>\n' +
			'<th scope="col" class="text-center">RANK</th>\n' +
			'<th scope="col" class="text-center">CELLNAME</th>\n' +
			'<th scope="col" class="text-center">ALARMS / Availability</th>\n' +
			'<th scope="col" class="text-center">CAPACITY</th>\n' +
			'<th scope="col" class="text-center">COVERAGE</th>\n' +
			'<th scope="col" class="text-center">QUALITY (Interference)</th>\n' +
			// '<th scope="col" class="text-center">CONTRIBUTORS</th>\n' +
			'<th scope="col" class="text-center">AREA KPI</th>\n' +
			'</tr>\n' +
			'</thead>\n' +
			'<tbody class='+type+'_'+tab+'_list>';

		_.forEach(data, function (val) {
			html += '<tr>\n' +
				'      <th class="text-center align-middle" scope="row">'+val.rank+'</th>\n' +
				'      <td class="text-left align-middle">'+val.CELLS+'</td>\n' +
				'      <td class="text-center align-middle">100%' +
				'		</td>\n' +
				'      <td class="text-right small align-middle">' +
				'		AMR Traffi =  '+val.amr_traffic+'% <br>' +
				'		CELL DCH USERS =  '+val.dch_user+'% <br>' +
				'		HSUPA USER = '+val.hsdpa_user+'% <br>' +
				'		HSDPA USER = ' +val.hsupa_user +'% <br>'+
				'		VS.RRC.FailConnEstab.Cong = ' +val.rrc_failconstabcong + '% <br>' +
				'		RAB Fail Estab '+ttype+'.Cong = ' +val.rab_failstab + '% <br>' +
				'       </td>\n' +
				'      <td class="text-right small align-middle">' +
				'		Propagation Delay <br>' +
				'		TA 0 = '+val.ta+' <br>' +
				'		VS.RRC.FailConnEstab.NoReply = '+val.constabnorepyl+' <br>' +
				'		RAB Fail Estab '+ttype+'.Un No Reply = '+val.csunnoreply+' <br>' +
				'      </td>\n' +
				'      <td class="text-right align-middle">' +
				'		RTWP =' + val.mean_rtwp + '%' +
				'		</td>\n' +
				// '      <td class="text-center align-middle">'+val.contributor+'%</td>\n' +
				'      <td class="text-center align-middle">'+val.kpi+'%</td>\n' +
				'    </tr>';
		});
		html += '</tbody>\n' +
			'</table>';
	} else {
		html = empty_kpi();
	}

	$('.'+type+'_'+tab+'_tbl').html(html);
	$('.'+tab+'_'+type+'_total').text(data.length);
	if (data.length > 0) {
		$('.'+type+'-'+tab+'-tbl').dataTable({
			"lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
		});
	}
}

$(document).on('click', '.view_3g_calendar', function (e) {
	e.preventDefault();
	$('.kpi-3g-sched').modal('show');
	callCalendar('get_3g_calendar', ktype);
});

$(document).on("click", ".export_voice_csfr_3g", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (voice_csfr_3g.length > 0) {
		JSONToCSVConvertor(voice_csfr_3g, "3G-Voice-CSFR-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".export_voice_dcr_3g", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (voice_dcr_3g.length > 0) {
		JSONToCSVConvertor(voice_dcr_3g, "3G-Voice-DCR-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".export_voice_sms_3g", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (voice_sms_3g.length > 0) {
		JSONToCSVConvertor(voice_sms_3g, "3G-Voice-SMS-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".export_voice_csfr_3g", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (voice_csfr_3g.length > 0) {
		JSONToCSVConvertor(voice_csfr_3g, "3G-Voice-CSFR-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});


$(document).on("click", ".export_hsdpa_csfr_3g", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (hspda_csfr_3g.length > 0) {
		JSONToCSVConvertor(hspda_csfr_3g, "3G-HSPDA-CSFR-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".export_hsdpa_dcr_3g", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (hspda_dcr_3g.length > 0) {
		JSONToCSVConvertor(hspda_dcr_3g, "3G-HSDPA-DCR-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".csfr_voice_tbl tbody tr, .dcr_voice_tbl tbody tr, .sms_voice_tbl tbody tr, .csfr_hsdpa_tbl tbody tr, .dcr_hsdpa_tbl tbody tr", function (e) {
	e.preventDefault();
	let cell_3g = [];
	let cell_name = $(this).find('td:eq(0)').text();
	cell_3g.push(cell_name);

	$('.ana-cell-name').text(cell_name);
	wcl3g = true;
	get_filter_menus('3g');
	get_3g_graph({cell: cell_3g, type: 'cell'});
	$('.view-3g-analyzer').modal('show');
});
