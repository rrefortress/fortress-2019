$(document).on('click', '.kpi5g_upload', function (e) {
	e.preventDefault();
	$('#kpi5g_upload').modal('show');
});

Dropzone.autoDiscover = false;
let kpi5g_upload = new Dropzone(".dropzone5g_KPI",{
	url: url + "KPI/kpi5g_upload",
	timeout: 180000,
	parallelUploads: 1,
	maxFiles: 1,
	method:"POST",
	acceptedFiles:".csv",
	paramName:"kpi",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:false,
	init: function() {
		this.on("sending", function(file, xhr, formData){
			formData.append("ktype", ktype);
		});
		this.on("success", function(file, response) {
			let data = jQuery.parseJSON(response);
			if (data.type === 'error') {} else {
				// get_5g_kpi(data.date);
				$('#date5g_sgnb_sr_select, #date5g_sgnb_rd_select, #date5g_sgnb_ro_select').val(data.date);
				set_5g_values();
			}
			SWAL(data.type, data.title, data.msg);
		})
	}
});

kpi5g_upload.on("queuecomplete", function () {
	this.removeAllFiles();
	$('#kpi5g_upload').modal('hide');
});
kpi5g_upload.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});
kpi5g_upload.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});

$(document).on('click', '.clear5g', function () {
	Swal.fire({
		title: 'Are you sure?',
		text: "You want to delete all item/s and it's histories?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	}).then((result) => {
		if (result.value) {
			let param = {
				url: url + 'clear_5g',
				type: "post",
				dataType: 'json',
				data: {ktype : ktype},
			};

			fortress(param).then((data) => {
				SWAL(data.type, data.title, data.msg);
				get_5g_kpi();
			});
		}
	});
});
//--------------------------------- SEARCH 2G ------------------------------------------------------------------------------------//
$(document).on('keyup', '#target_sgnb_sr_5g', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#date5g_sgnb_sr_select').val();
	get_5g_man_kpi('sgnb_sr', date, 'get_sgnb_sr', target);
}, 3000));

$(document).on('keyup', '#target_sgnb_rd_5g', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#date5g_sgnb_rd_select').val();
	get_5g_man_kpi('sgnb_rd', date, 'get_sgnb_rd', target);
}, 3000));

$(document).on('keyup', '#target_sgnb_ro_5g', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#date5g_sgnb_ro_select').val();
	get_5g_man_kpi('sgnb_ro', date, 'get_sgnb_ro', target);
}, 3000));

$(function() {
	$('#date5g_sgnb_sr_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#target_sgnb_sr_5g').val();
		let date = $(this).val();
		get_5g_man_kpi('sgnb_sr', date, 'get_sgnb_sr', target);
	});
	$('#date5g_sgnb_rd_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#target_sgnb_rd_5g').val();
		let date = $(this).val();
		get_5g_man_kpi('sgnb_rd', date, 'get_sgnb_rd', target);
	});
	$('#date5g_sgnb_ro_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#target_sgnb_ro_5g').val();
		let date = $(this).val();
		get_5g_man_kpi('sgnb_ro', date, 'get_sgnb_ro', target);
	});
});

function get_5g_man_kpi(val = '', date = '', link = '', target = 0) {
	let param = {
		url: url + link,
		dataType: 'json',
		type: 'POST',
		data: {target: target, date: date, ktype : ktype},
		beforeSend: $('.'+val+'_5g_tbl').html('<div class="col-12 mg-top-100">' +
			'<h5 class="text-center text-white">' +
			'<img src='+loaders+' width="150" height="150" class="rounded-circle" alt="Loader"><br>' +
			'<small class="text-center text-muted pt-4"><em>Loading data...</em></small>' +
			'</h5>' +
			'</div>')
	};

	fortress(param).then((data) => {
		set_5g_table(data, val);
	});
}

function get_5g_kpi(date = '') {
	if (date === '') {
		$('.sgnb_sr_5g_tbl, .sgnb_rd_5g_tbl, .sgnb_ro_5g_tbl').html(empty_kpi());
	} else {
		let param = {
			url: url + 'get_5g_kpi',
			dataType: 'json',
			type: 'POST',
			data: {date: date, ktype : ktype},
			beforeSend: $('.loader_5g').html('<div class="col-12 mg-top-100">' +
				'<h5 class="text-center text-white">' +
				'<img src='+loaders+' width="150" height="150" class="rounded-circle" alt="Loader"><br>' +
				'<small class="text-center text-muted pt-4"><em>Loading data...</em></small>' +
				'</h5>' +
				'</div>')
		};

		fortress(param).then((data) => {
			set_5g_table(data.sgnb_sr, 'sgnb_sr');
			set_5g_table(data.sgnb_rd, 'sgnb_rd');
			set_5g_table(data.sgnb_ro, 'sgnb_ro');
		});
	}
}

let sgnb_sr = [];
let sgnb_rd = [];
let sgnb_ro = [];

function set_5g_table(data, type) {
	switch (type.toUpperCase()) {
		case 'SGNB_SR': sgnb_sr = data; break;
		case 'SGNB_RD': sgnb_rd = data; break;
		case 'SGNB_RO': sgnb_ro = data; break;
	}
	let html = '';
	if (data.length > 0) {
		html += '<table class="table-bordered force-fit small display dt-responsive table-sm '+type+'-tbl table mb-0 table-sm nowrap table-hover table-striped">\n' +
			'<thead class="global_color text-white">\n' +
			'<tr>\n' +
			'<th scope="col" class="text-center">RANK</th>\n' +
			'<th scope="col" class="text-center">CELLNAME ('+type.toUpperCase()+')</th>\n' +
			'<th scope="col" class="text-center">ALARMS / Availability</th>\n' +
			'<th scope="col" class="text-center">CAPACITY</th>\n' +
			'<th scope="col" class="text-center">COVERAGE</th>\n' +
			'<th scope="col" class="text-center">QUALITY (Interference)</th>\n' +
			'<th scope="col" class="text-center">AREA KPI</th>\n' +
			'</tr>\n' +
			'</thead>\n' +
			'<tbody class='+type+'_list>';

		_.forEach(data, function (val) {
			html += '<tr>\n' +
				'      <th class="text-center align-middle" scope="row">'+val.rank+'</th>\n' +
				'      <td class="text-left align-middle">'+val.NRCELL+'</td>\n' +
				'      <td class="text-right align-middle">'+val.alarms+'%</td>\n' +
				'      <td class="text-right align-middle">' +val.capacity +'</td>\n' +
				'      <td class="text-right align-middle">' +val.coverage +'</td>\n' +
				'      <td class="text-right align-middle">' +val.quality +'</td>\n' +
				'      <td class="text-center align-middle">'+val.kpi+'%</td>\n' +
				'    </tr>';
		});
		html += '</tbody>\n' +
			'</table>';
	} else {
		html = empty_kpi();
	}

	$('.'+type+'_5g_tbl').html(html);
	$('.'+type+'_5g_total').html(data.length);
	if (data.length > 0) {
		$('.'+type+'-tbl').dataTable({
			"lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
		});
	}
}

$(document).on("click", ".export_sgnb_sr_5g", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (sgnb_sr.length > 0) {
		JSONToCSVConvertor(sgnb_sr, "5G-SGNB-SR-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".export_sgnb_rd_5g", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (sgnb_rd.length > 0) {
		JSONToCSVConvertor(sgnb_rd, "5G-SGNB-RD-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".export_sgnb_ro_5g", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (sgnb_ro.length > 0) {
		JSONToCSVConvertor(sgnb_ro, "5G-SGNB-OVERALL-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".sgnb_sr_5g_tbl tbody tr, .sgnb_rd_5g_tbl tbody tr, .sgnb_ro_5g_tbl tbody tr",function (e) {
	e.preventDefault();
	let cell_5g = [];
	let cell_name = $(this).find('td:eq(0)').text();
	cell_5g.push(cell_name);
	$('.ana-cell-name').text(cell_name);
	wcl5g = true;
	get_filter_menus('5g');
	get_5g_graph({cell: cell_5g, type: 'cell'});
	$('.view-5g-analyzer').modal('show');
});
