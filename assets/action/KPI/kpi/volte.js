$('.volte_tab a[data-toggle="tab"]').on('shown.bs.tab', function (event) {
	volte_tab = event.target.href.split('#')[1].toUpperCase();
});

$(document).on('click', '.kpivolte_upload', function (e) {
	e.preventDefault();
	$('#kpivolte_upload').modal('show');
});

Dropzone.autoDiscover = false;
let kpivolte_upload = new Dropzone(".dropzonevolte_KPI",{
	url: url + "KPI/kpivolte_upload",
	timeout: 180000,
	parallelUploads: 50,
	maxFiles: 1,
	method:"POST",
	acceptedFiles:".csv",
	paramName:"kpi",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:true,
	init: function() {
		this.on("sending", function(file, xhr, formData){
			formData.append("tab", volte_tab);
			formData.append("ktype", ktype);
		});
		this.on("success", function(file, response) {
			let data = jQuery.parseJSON(response);
			if (data.type === 'error') {} else {
				$('#volte-date_tdd_estab_select, #volte-date_tdd_erab_select, #volte-date_tdd_sdvcc_select, #volte-date_tdd_dcr_select, #volte-date_fdd_estab_select, #volte-date_fdd_erab_select, #volte-date_tdd_sdvcc_select, #volte-date_fdd_dcr_select').val(data.date);
				// get_volte_kpi(volte_tab, data.date);
				set_volte_values();
			}
			SWAL(data.type, data.title, data.msg);
		});
	}
});

kpivolte_upload.on("queuecomplete", function () {
	this.removeAllFiles();
	$('#kpivolte_upload').modal('hide');
});
kpivolte_upload.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});
kpivolte_upload.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});
//---------------------------------------------------------------------------------------------------------------------------------//
$(document).on('click', '.clearvolte', function () {
	Swal.fire({
		title: 'Are you sure?',
		text: "You want to delete all item/s and it's histories?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	}).then((result) => {
		if (result.value) {
			let param = {
				url: url + 'clear_volte',
				type: "post",
				dataType: 'json',
				data: {ktype : ktype},
			};

			fortress(param).then((data) => {
				SWAL(data.type, data.title, data.msg);
				get_volte_kpi();
			});
		}
	});
});
//--------------------------------- SEARCH 2G ------------------------------------------------------------------------------------//
$(document).on('keyup', '#volte-target_tdd_estab', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#volte-date_tdd_estab_select').val();
	get_volte_man_kpi('estab', date, 'get_volte_estab', 'tdd', target);
}, 3000));

$(document).on('keyup', '#volte-target_tdd_erab', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#volte-date_tdd_erab_select').val();
	get_volte_man_kpi('erab', date, 'get_volte_erab', 'tdd', target);
}, 3000));

$(document).on('keyup', '#volte-target_tdd_sdvcc', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#volte-date_tdd_sdvcc_select').val();
	get_volte_man_kpi('sdvcc', date, 'get_volte_sdvcc', 'tdd', target);
}, 3000));

$(document).on('keyup', '#volte-target_tdd_dcr', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#volte-date_tdd_dcr_select').val();
	get_volte_man_kpi('dcr', date, 'get_volte_dcr', 'tdd', target);
}, 3000));

$(document).on('keyup', '#volte-target_fdd_estab', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#volte-date_fdd_estab_select').val();
	get_volte_man_kpi('estab', date, 'get_volte_estab', 'fdd', target);
}, 3000));

$(document).on('keyup', '#volte-target_fdd_erab', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#volte-date_fdd_erab_select').val();
	get_volte_man_kpi('erab', date, 'get_volte_erab', 'fdd', target);
}, 3000));

$(document).on('keyup', '#volte-target_fdd_sdvcc', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#volte-date_fdd_sdvcc_select').val();
	get_volte_man_kpi('sdvcc', date, 'get_volte_sdvcc', 'fdd', target);
}, 3000));

$(document).on('keyup', '#volte-target_fdd_dcr', _.debounce(function (e) {
	e.preventDefault();
	let target = $(this).val();
	let date = $('#volte-date_fdd_dcr_select').val();
	get_volte_man_kpi('dcr', date, 'get_volte_dcr', 'fdd', target);
}, 3000));
//---------------------------------------------------------------------------------------------------------------------------------//

//----------------------------------------------------------FILTER BY DATE (CSFR, DCR & IAFR)-------------------------------------//
$(function() {
	$('#volte-date_tdd_estab_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#volte-target_tdd_estab').val();
		let date = $(this).val();
		get_volte_man_kpi('estab', date, 'get_volte_estab', 'tdd', target);
	});
	$('#volte-date_tdd_erab_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#volte-target_tdd_erab').val();
		let date = $(this).val();
		get_volte_man_kpi('erab', date, 'get_volte_erab', 'tdd', target);
	});
	$('#volte-date_tdd_sdvcc_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#volte-target_tdd_sdvcc').val();
		let date = $(this).val();
		get_volte_man_kpi('sdvcc', date, 'get_volte_sdvcc', 'tdd', target);
	});
	$('#volte-date_tdd_dcr_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#volte-target_tdd_dcr').val();
		let date = $(this).val();
		get_volte_man_kpi('dcr', date, 'get_volte_dcr', 'tdd', target);
	});

	$('#volte-date_fdd_estab_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#volte-target_fdd_estab').val();
		let date = $(this).val();
		get_volte_man_kpi('estab', date, 'get_volte_estab', 'fdd', target);
	});
	$('#volte-date_fdd_erab_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#volte-target_fdd_erab').val();
		let date = $(this).val();
		get_volte_man_kpi('erab', date, 'get_volte_erab', 'fdd', target);
	});
	$('#volte-date_fdd_sdvcc_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#volte-target_fdd_sdvcc').val();
		let date = $(this).val();
		get_volte_man_kpi('sdvcc', date, 'get_volte_sdvcc', 'fdd', target);
	});
	$('#volte-date_fdd_dcr_select').datepicker({format: 'yyyy-mm-dd',}).on('changeDate', function(e) {
		e.preventDefault();
		let target = $('#volte-target_fdd_dcr').val();
		let date = $(this).val();
		get_volte_man_kpi('dcr', date, 'get_volte_dcr', 'fdd', target);
	});
});
//---------------------------------------------------------------------------------------------------------------------------------//
function get_volte_man_kpi(val = '', date = '', link = '', tab = '', target = 0) {
	let param = {
		url: url + link,
		dataType: 'json',
		type: 'POST',
		data: {target: target, date: date, tab: 'VOLTE-'+tab.toUpperCase(), ktype : ktype},
		beforeSend: $('.volte-'+tab+'_'+val+'_tbl').html('<div class="col-12 mg-top-100">' +
			'<h5 class="text-center text-white">' +
			'<img src='+loaders+' width="150" height="150" class="rounded-circle" alt="Loader"><br>' +
			'<small class="text-center text-muted pt-4"><em>Loading data...</em></small>' +
			'</h5>' +
			'</div>')
	};

	fortress(param).then((data) => {
		set_volte_table(data, tab, val);
	});
}

function get_volte_kpi(tab = '', date = '') {
	if (date === '') {
		$('.volte-tdd_estab_tbl, .volte-tdd_erab_tbl, .volte-tdd_sdvcc_tbl, .volte-tdd_dcr_tbl').html(empty_kpi());
		$('.volte-fdd_estab_tbl, .volte-fdd_erab_tbl, .volte-fdd_sdvcc_tbl, .volte-fdd_dcr_tbl').html(empty_kpi());
	} else {
		let tfdd = tab === '' ? '' : _.split(tab, '-', 2);
		let loader = tab === '' ? 'loader_volte' : 'volte-loader_'+tfdd[1].toLowerCase();
		let param = {
			url: url + 'get_volte_kpi',
			dataType: 'json',
			type: 'POST',
			data: {target: '', date: date, tab: tab, ktype : ktype},
			beforeSend: $('.'+loader).html('<div class="col-12 mg-top-100">' +
				'<h5 class="text-center text-white">' +
				'<img src='+loaders+' width="150" height="150" class="rounded-circle" alt="Loader"><br>' +
				'<small class="text-center text-muted pt-4"><em>Loading data...</em></small>' +
				'</h5>' +
				'</div>')
		};
		fortress(param).then((data) => {
			tab = tab === '' ? '' : tfdd[1].toUpperCase();
			if (tab === '') {
				set_volte_table(data.tdd.estab, 'tdd', 'estab');
				set_volte_table(data.tdd.erab, 'tdd', 'erab');
				set_volte_table(data.tdd.sdvcc, 'tdd', 'sdvcc');
				set_volte_table(data.tdd.dcr, 'tdd', 'dcr');

				set_volte_table(data.fdd.estab, 'fdd', 'estab');
				set_volte_table(data.fdd.erab, 'fdd', 'erab');
				set_volte_table(data.fdd.sdvcc, 'fdd', 'sdvcc');
				set_volte_table(data.fdd.dcr, 'fdd', 'dcr');
			} else if (tab === 'TDD') {
				set_volte_table(data.tdd.estab, 'tdd', 'estab');
				set_volte_table(data.tdd.erab, 'tdd', 'erab');
				set_volte_table(data.tdd.sdvcc, 'tdd', 'sdvcc');
				set_volte_table(data.tdd.dcr, 'tdd', 'dcr');
			} else if (tab === 'FDD') {
				set_volte_table(data.fdd.estab, 'fdd', 'estab');
				set_volte_table(data.fdd.erab, 'fdd', 'erab');
				set_volte_table(data.fdd.sdvcc, 'fdd', 'sdvcc');
				set_volte_table(data.fdd.dcr, 'fdd', 'dcr');
			}
		});
	}
}

let vtdd_estab = [];
let vtdd_erab = [];
let vtdd_sdvcc = [];
let vtdd_dcr = [];

let vfdd_estab = [];
let vfdd_erab = [];
let vfdd_sdvcc = [];
let vfdd_dcr = [];

function set_volte_table(data, tab, type) {
	if (tab === 'tdd') {
		switch (type.toUpperCase()) {
			case 'ESTAB': vtdd_estab = data; break;
			case 'ERAB': vtdd_erab = data; break;
			case 'SDVCC': vtdd_sdvcc = data; break;
			case 'DCR': vtdd_dcr = data; break;
		}
	} else {
		switch (type.toUpperCase()) {
			case 'ESTAB': vfdd_rrc = data; break;
			case 'ERAB': vfdd_erab = data; break;
			case 'SDVCC': vfdd_intra = data; break;
			case 'DCR': vfdd_dcr = data; break;
		}
	}

	let html = '';
	if (data.length > 0) {
		html += '<table class="table-bordered force-fit small display dt-responsive table-sm volte-'+type+'-'+tab+'-tbl table mb-0 table-sm nowrap table-hover table-striped">\n' +
			'<thead class="global_color text-white">\n' +
			'<tr>\n' +
			'<th scope="col" class="text-center">RANK</th>\n' +
			'<th scope="col" class="text-center">CELLNAME ('+type.toUpperCase()+')</th>\n' +
			'<th scope="col" class="text-center">ALARMS / Availability</th>\n' +
			'<th scope="col" class="text-center">CAPACITY</th>\n' +
			'<th scope="col" class="text-center">COVERAGE</th>\n' +
			'<th scope="col" class="text-center">QUALITY (Interference)</th>\n' +
			'<th scope="col" class="text-center">AREA KPI</th>\n' +
			'</tr>\n' +
			'</thead>\n' +
			'<tbody class='+type+'_'+tab+'_list>';

		_.forEach(data, function (val) {
			html += '<tr>\n' +
				'      <th class="text-center align-middle" scope="row">'+val.rank+'</th>\n' +
				'      <td class="text-left align-middle">'+val.Cellname+'</td>\n' +
				'      <td class="text-center align-middle">None</td>\n' +
				'      <td class="text-right align-middle">None</td>\n' +
				'      <td class="text-right align-middle">None</td>\n' +
				'      <td class="text-right align-middle">' +
				'       Interference <br>' +
				'		</td>\n' +
				'      <td class="text-center align-middle">'+val.kpi+'%</td>\n' +
				'    </tr>';
		});
		html += '</tbody>\n' +
			'</table>';
	} else {
		html = '<div class="col-12 mg-top-135 text-center">' +
			'<h5 class="text-center text-white">' +
			'   <i class="fas fa-check-circle rounded-circle shadow-sm display-2 text-success"></i></br>' +
			'	<h4 class="text-muted text-center">Area KPI is within the network Target.</h4>' +
			'</h5>' +
			'</div>';
	}

	$('.volte-'+tab+'_'+type+'_tbl').html(html);
	$('.volte-'+tab+'_'+type+'_total').html(data.length);
	if (data.length > 0) {
		$('.volte-'+type+'-'+tab+'-tbl').dataTable({
			"lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
		});}
}

$(document).on('click', '.view_4g_calendar', function (e) {
	e.preventDefault();
	$('.kpi-volte-sched').modal('show');
	callCalendar('get_volte_calendar', ktype);
});

$(document).on("click", ".volte-export_tdd_estab", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (vtdd_estab.length > 0) {
		JSONToCSVConvertor(vtdd_estab, "VOLTE-TDD-ESTAB-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".volte-export_tdd_erab", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (vtdd_erab.length > 0) {
		JSONToCSVConvertor(vtdd_erab, "VOLTE-TDD-ERAB-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".volte-export_tdd_sdvcc", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (vtdd_sdvcc.length > 0) {
		JSONToCSVConvertor(vtdd_sdvcc, "VOLTE-TDD-SDVCC-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".volte-export_tdd_dcr", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (vtdd_dcr.length > 0) {
		JSONToCSVConvertor(vtdd_dcr, "VOLTE-TDD-DCR-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".volte-export_fdd_estab", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (vfdd_estab.length > 0) {
		JSONToCSVConvertor(vfdd_estab, "VOLTE-FDD-ESTAB-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".volte-export_fdd_erab", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (vfdd_erab.length > 0) {
		JSONToCSVConvertor(vfdd_erab, "VOLTE-FDD-ERAB-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".volte-export_fdd_sdvcc", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (vfdd_sdvcc.length > 0) {
		JSONToCSVConvertor(vfdd_sdvcc, "VOLTE-FDD-SDVCC-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});

$(document).on("click", ".volte-export_fdd_dcr", function (e) {
	e.preventDefault();
	let today = moment().format("DD/MM/YYYY");
	if (vfdd_dcr.length > 0) {
		JSONToCSVConvertor(vfdd_dcr, "VOLTE-FDD-DCR-(" + today + ")", true);
	} else {
		SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
	}
});
