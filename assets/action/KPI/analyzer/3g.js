let wcl3g = false;

function get_3g_graph(data = []) {
	let chart = $('.chart-loader');
	chart.find('div.c3g-dis').addClass('d-none');
	let param = {
		url: url + 'get_3g_graph',
		dataType: 'json',
		type: 'POST',
		data: data,
		beforeSend: chart.append('<div class="col-12 c3g-load" style="margin-top: 5% !important;">' +
			'<h5 class="text-center text-white">' +
			'<img src='+loaders+' width="120" height="120" alt="loader" class="rounded-circle"><br>\n' +
			'<em class="text-muted small text-process">Loading Chart</em>' +
			'</h5>' +
			'</div>')
	};

	fortress(param).then((data) => {
		chart.find('div.c3g-load').remove();
		chart.find('div.c3g-dis').removeClass('d-none');
		set_3g_basic_chart(data.vcsfr, data.date, 'voice_csfr');
		set_3g_basic_chart(data.hcsfr, data.date, 'hs_csfr');
		set_3g_basic_chart(data.vdcr, data.date, 'voice_dcr');
		set_3g_basic_chart(data.hdcr, data.date, 'hs_dcr');

		set_3g_single_line_chart(data.avail, data.date, 'avail', false, 'line', '#0e49b5');
		set_3g_single_line_chart(data.amr, data.date, 'amr_trf', true, 'line', '#0e49b5');
		set_3g_single_line_chart(data.dch, data.date, 'dch', true, 'bar', '#fb743e');

		set_3g_basic_chart(data.sms, data.date, 'sms');
		set_3g_basic_chart(data.sh, data.date, 'sh');

		set_3g_2_bar_chart(data.rtwp, data.date, 'rtwp', 'line', 'VS MaxRTWP(dBm)', 'VS MinRTWP(dBm)', false);
		set_3g_2_bar_chart(data.dtv, data.date, 'dtv', 'bar', 'DL Traffic Volume', 'UL Traffic Volume', false);
		set_3g_2_bar_chart(data.rre, data.date, 'rre', 'bar', 'VS RRC FailConnEstab Cong', 'VS RRC FailConnEstab NoReply', true);

		set_3g_hrdpa_stack_bar_chart(data.hrpa, data.date, 'hsrpa');
		set_3g_hrdpa_stack_bar_chart(data.rpe, data.date, 'rpe');

		set_3g_rrc_stack_bar_chart(data.rrc, data.date, 'rrc');
		set_3g_pd_stack_bar_chart(data.pd, data.date,'pd');

		set_3g_rrj_stack_bar_chart(data.rrj, data.date, 'rrj');
		set_3g_rce_stack_bar_chart(data.rce, data.date, 'rce');
		set_3g_rca_stack_bar_chart(data.rca, data.date, 'rca');
		set_3g_rpa_stack_bar_chart(data.rpa, data.date, 'rpa');
		set_3g_isho_stack_bar_chart(data.isho, data.date, 'isho');
	});

	function set_3g_basic_chart(data, vdate, type) {
		let avg = [];
		let sum = [];
		let date = [];

		_.forEach(data.v1, (value) => {
			avg.push(value);
		});

		_.forEach(data.v2, (value) => {
			sum.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		get_3g_basic_chart(avg, sum, date, type, 'bar', 'line', true, _.max(avg));
	}

	function set_3g_single_line_chart(data, vdate, type, fill, ch1, color) {
		let avg = [];
		let date = [];

		_.forEach(data, (value) => {
			avg.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		get_3g_single_line_chart(avg, date, type, true, fill, ch1, color);
	}

	function set_3g_pd_stack_bar_chart(data, vdate, type) {
		let int0 = [];
		let int1 = [];
		let int2 = [];
		let int3 = [];
		let int4 = [];
		let int5 = [];
		let int6 = [];
		let int7 = [];
		let int8 = [];
		let int9 = [];
		let int10 = [];
		let int11 = [];
		let date = [];

		_.forEach(data.tp0, (value) => {
			int0.push(value);
		});

		_.forEach(data.tp1, (value) => {
			int1.push(value);
		});

		_.forEach(data.tp2, (value) => {
			int2.push(value);
		});

		_.forEach(data.tp3, (value) => {
			int3.push(value);
		});

		_.forEach(data.tp4, (value) => {
			int4.push(value);
		});

		_.forEach(data.tp5, (value) => {
			int5.push(value);
		});

		_.forEach(data.tp6to9, (value) => {
			int6.push(value);
		});

		_.forEach(data.tp10to15, (value) => {
			int7.push(value);
		});

		_.forEach(data.tp16to25, (value) => {
			int8.push(value);
		});

		_.forEach(data.tp26to35, (value) => {
			int9.push(value);
		});

		_.forEach(data.tp36to55, (value) => {
			int10.push(value);
		});

		_.forEach(data.tpmore55, (value) => {
			int11.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'tc0' : int0,
			'tc1' : int1,
			'tc2' : int2,
			'tc3' : int3,
			'tc4' : int4,
			'tc5' : int5,
			'tc6' : int6,
			'tc7' : int7,
			'tc8' : int8,
			'tc9' : int9,
			'tc10' : int10,
			'tc11' : int11,
		};

		get_3g_pd_stack_bar_chart(datas, date, type, 'bar', true);
	}

	function set_3g_hrdpa_stack_bar_chart(data, vdate, type) {
		let int1 = [];
		let int2 = [];
		let int3 = [];
		let int4 = [];
		let int5 = [];
		let date = [];

		_.forEach(data.tp1, (value) => {
			int1.push(value);
		});

		_.forEach(data.tp2, (value) => {
			int2.push(value);
		});

		_.forEach(data.tp3, (value) => {
			int3.push(value);
		});

		_.forEach(data.tp4, (value) => {
			int4.push(value);
		});

		_.forEach(data.tp5, (value) => {
			int5.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'tp1' : int1,
			'tp2' : int2,
			'tp3' : int3,
			'tp4' : int4,
			'tp5' : int5,
		};

		get_3g_stack_bar_chart(datas, date, type, 'bar', true);
	}

	function set_3g_rrc_stack_bar_chart(data, vdate, type) {
		let int1 = [];
		let int2 = [];
		let int3 = [];
		let int4 = [];
		let int5 = [];
		let int6 = [];
		let int7 = [];
		let int8 = [];
		let int9 = [];
		let int10 = [];
		let date = [];

		_.forEach(data.tp1, (value) => {
			int1.push(value);
		});

		_.forEach(data.tp2, (value) => {
			int2.push(value);
		});

		_.forEach(data.tp3, (value) => {
			int3.push(value);
		});

		_.forEach(data.tp4, (value) => {
			int4.push(value);
		});

		_.forEach(data.tp5, (value) => {
			int5.push(value);
		});

		_.forEach(data.tp6, (value) => {
			int6.push(value);
		});

		_.forEach(data.tp7, (value) => {
			int7.push(value);
		});

		_.forEach(data.tp8, (value) => {
			int8.push(value);
		});

		_.forEach(data.tp9, (value) => {
			int9.push(value);
		});

		_.forEach(data.tp10, (value) => {
			int10.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'tp1' : int1,
			'tp2' : int2,
			'tp3' : int3,
			'tp4' : int4,
			'tp5' : int5,
			'tp6' : int6,
			'tp7' : int7,
			'tp8' : int8,
			'tp9' : int9,
			'tp10' : int10
		};

		get_3g_rrc_stack_bar_chart(datas, date, type, 'bar', true);
	}

	function set_3g_rrj_stack_bar_chart(data, vdate, type) {
		let	l1 = [];
		let b1 = [];
		let b2 = [];
		let b3 = [];
		let b4 = [];
		let date = [];

		_.forEach(data.l1, (value) => {
			l1.push(value);
		});

		_.forEach(data.b1, (value) => {
			b1.push(value);
		});

		_.forEach(data.b2, (value) => {
			b2.push(value);
		});

		_.forEach(data.b3, (value) => {
			b3.push(value);
		});

		_.forEach(data.b4, (value) => {
			b4.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'l1' : l1,
			'b1' : b1,
			'b2' : b2,
			'b3' : b3,
			'b4' : b4
		};

		get_3g_rrj_chart(l1, datas, date, type, 'bar', 'line', _.max(l1));
	}

	function set_3g_rce_stack_bar_chart(data, vdate, type) {
		let	l1 = [];
		let b1 = [];
		let b2 = [];
		let b3 = [];
		let b4 = [];
		let b5 = [];
		let b6 = [];
		let date = [];

		_.forEach(data.l1, (value) => {
			l1.push(value);
		});

		_.forEach(data.b1, (value) => {
			b1.push(value);
		});

		_.forEach(data.b2, (value) => {
			b2.push(value);
		});

		_.forEach(data.b3, (value) => {
			b3.push(value);
		});

		_.forEach(data.b4, (value) => {
			b4.push(value);
		});

		_.forEach(data.b5, (value) => {
			b6.push(value);
		});

		_.forEach(data.b6, (value) => {
			b6.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'l1' : l1,
			'b1' : b1,
			'b2' : b2,
			'b3' : b3,
			'b4' : b4,
			'b5' : b5,
			'b6' : b6
		};

		get_3g_rce_chart(l1, datas, date, type, 'bar', 'line', _.max(l1));
	}

	function set_3g_rca_stack_bar_chart(data, vdate, type) {
		let	l1 = [];
		let b1 = [];
		let b2 = [];
		let b3 = [];
		let b4 = [];
		let b5 = [];
		let b6 = [];
		let b7 = [];
		let b8 = [];
		let date = [];

		_.forEach(data.l1, (value) => {
			l1.push(value);
		});

		_.forEach(data.b1, (value) => {
			b1.push(value);
		});

		_.forEach(data.b2, (value) => {
			b2.push(value);
		});

		_.forEach(data.b3, (value) => {
			b3.push(value);
		});

		_.forEach(data.b4, (value) => {
			b4.push(value);
		});

		_.forEach(data.b5, (value) => {
			b5.push(value);
		});

		_.forEach(data.b6, (value) => {
			b6.push(value);
		});

		_.forEach(data.b7, (value) => {
			b7.push(value);
		});

		_.forEach(data.b8, (value) => {
			b8.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'l1' : l1,
			'b1' : b1,
			'b2' : b2,
			'b3' : b3,
			'b4' : b4,
			'b5' : b5,
			'b6' : b6,
			'b7' : b7,
			'b8' : b8
		};

		get_3g_rca_chart(l1, datas, date, type, 'bar', 'line', _.max(l1));
	}

	function set_3g_rpa_stack_bar_chart(data, vdate, type) {
		let	l1 = [];
		let	l2 = [];
		let b1 = [];
		let b2 = [];
		let b3 = [];
		let b4 = [];
		let b5 = [];
		let b6 = [];
		let b7 = [];
		let b8 = [];
		let date = [];

		_.forEach(data.l1, (value) => {
			l1.push(value);
		});

		_.forEach(data.l2, (value) => {
			l2.push(value);
		});

		_.forEach(data.b1, (value) => {
			b1.push(value);
		});

		_.forEach(data.b2, (value) => {
			b2.push(value);
		});

		_.forEach(data.b3, (value) => {
			b3.push(value);
		});

		_.forEach(data.b4, (value) => {
			b4.push(value);
		});

		_.forEach(data.b5, (value) => {
			b5.push(value);
		});

		_.forEach(data.b6, (value) => {
			b6.push(value);
		});

		_.forEach(data.b7, (value) => {
			b7.push(value);
		});

		_.forEach(data.b8, (value) => {
			b8.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'b1' : b1,
			'b2' : b2,
			'b3' : b3,
			'b4' : b4,
			'b5' : b5,
			'b6' : b6,
			'b7' : b7,
			'b8' : b8
		};

		let line = {
			'l1' : l1,
			'l2' : l2
		}

		get_3g_rpa_chart(line, datas, date, type, 'bar', 'line', _.max(l1) + _.max(l2));
	}

	function set_3g_isho_stack_bar_chart(data, vdate, type) {
		let	l1 = [];
		let l2 = [];
		let b1 = [];
		let b2 = [];
		let date = [];

		_.forEach(data.l1, (value) => {
			l1.push(value);
		});

		_.forEach(data.l2, (value) => {
			l2.push(value);
		});

		_.forEach(data.b1, (value) => {
			b1.push(value);
		});

		_.forEach(data.b2, (value) => {
			b2.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'b1' : b1,
			'b2' : b2,
		};

		let line = {
			'l1' : l1,
			'l2' : l2,
		}

		get_3g_isho_chart(line, datas, date, type, 'bar', 'line', _.max(l1) + _.max(l2));
	}

	function set_3g_2_bar_chart(data, vdate, type, chtype, lb1, lb2, stacked) {
		let trf1 = [];
		let trf2 = [];
		let date = [];

		_.forEach(data.tr1, (value) => {
			trf1.push(value);
		});

		_.forEach(data.tr2, (value) => {
			trf2.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'trf1' : trf1,
			'trf2' : trf2,
		};

		get_3g_2_bar_chart(datas, date, type, chtype, lb1, lb2, stacked);
	}
}

function get_3g_basic_chart(avg = '', sum = '', labels = '', name, ch1 = '', ch2 = '', legend, max = 0) {
	name = wcl3g === true ? name + '-wcl3g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let  options = {
		stacked: false,
		layout: pad,
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			display: legend
		},
		scales: {
			yAxes: [{
				id: 'A',
				type: 'linear',
				position: 'left',
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}, {
				id: 'B',
				type: 'linear',
				position: 'right',
				ticks: {
					max: max,
					min: 0
				}
			}]
		}
	}

	let datas = {
		datasets: [
			{
				type: 'line',
				fill: false,
				yAxisID: 'B',
				label: name.toUpperCase() +' DEN',
				data: avg,
				backgroundColor: 'rgba(255, 255, 255, 0.9)',
				borderWidth: 3,
				borderColor: '#0e49b5',
			},
			{
				fill: false,
				label: name.toUpperCase(),
				data: sum,
				yAxisID: 'A',
				backgroundColor: '#fb743e',
				borderWidth: 3,
				borderColor: '#fb743e',
			},

		],
		labels: labels,
	};

	build_chart(datas, options, ctxBar, 0, ch1, name);
}

function get_3g_single_line_chart(avg = '', labels = '', name = '', legend, fill = Boolean, ch1, color) {
	name = wcl3g === true ? name + '-wcl3g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
			filler: {
				propagate: true
			}
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			fontfontSize: 5,
			boxWidth: 10,
			display: false
		},
		layout: pad,
		scales: {
			xAxes: [{
				stacked: true
			}],
			yAxes: [{
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	};

	let datas = {
		datasets: [
			{
				fill: fill,
				data: avg,
				backgroundColor: ch1 === 'bar' || name === 'amr_trf' ? color : 'rgba(255, 255, 255, 0.9)',
				borderWidth: 3,
				borderColor: color,
			},
		],
		labels: labels,
	};

	build_chart(datas, options, ctxBar, 0, ch1, name);
}

function get_3g_2_bar_chart(data = '', labels = '', name, ch1 = '', lb1 = '', lb2 = '', stacked = false) {
	name = wcl3g === true ? name + '-wcl3g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			fontfontSize: 5,
			boxWidth: 10,
			display: true
		},
		layout: pad,
		scales: {
			xAxes: [{
				stacked: stacked
			}],
			yAxes: [{
				stacked: stacked,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	};

	let datas = {
		datasets: [
			{
				fill: false,
				label: lb1,
				backgroundColor: ch1 === 'bar' ? '#0e49b5' : 'rgba(255, 255, 255, 0.9)',
				borderWidth: 3,
				borderColor: '#0e49b5',
				data: data.trf1
			},
			{
				fill: false,
				label: lb2,
				backgroundColor: '#fb743e',
				borderWidth: 3,
				borderColor: '#fb743e',
				data: data.trf2
			}
		],
		labels: labels
	};

	build_chart(datas, options, ctxBar, 0, ch1, name);
}

function get_3g_pd_stack_bar_chart(data = '', labels = '', name) {
	name = wcl3g === true ? name + '-wcl3g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'left',
			fontfontSize: 5,
			boxWidth: 10,
			display: false
		},
		layout: pad,
		scales: {
			xAxes: [{
				stacked: true
			}],
			yAxes: [{
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	};

	let datas = {
		datasets: [
			{
				label: "TCH Call Drops Other",
				backgroundColor: '#0e49b5',
				data: data.tc0
			},
			{
				label: "TCH Call Drops Other",
				backgroundColor: '#fb743e',
				data: data.tc1
			},
			{
				label: "TCH Call ULDL Fer",
				backgroundColor: "#eb596e",
				data: data.tc2
			},
			{
				label: "TCH Call Drops Rcv Level",
				backgroundColor: "#ffd31d",
				data: data.tc3
			},
			{
				label: "TCH Call Drops TA",
				backgroundColor: "#0779e4",
				data: data.tc4
			},
			{
				label: "TCH Call Drops UL QUAL",
				backgroundColor: "#54e346",
				data: data.tc5
			},
			{
				label: "TCH Call Drops DL RCV Level",
				backgroundColor: "#de7119",
				data: data.tc6
			},
			{
				label: "TCH Call Drops UL FER",
				backgroundColor: "#687466",
				data: data.tc7
			},
			{
				label: "TCH Call Drops UL QUAL",
				backgroundColor: "#7d5e2a",
				data: data.tc8
			},
			{
				label: "TCH Call Drops ULDL Rcv Level",
				backgroundColor: "#fb3640",
				data: data.tc9
			},
			{
				label: "TCH Call Drops DL FER",
				backgroundColor: "#295939",
				data: data.tc10
			},
			{
				label: "TCH Call Drops ULDL QUAL",
				backgroundColor: "#1b262c",
				data: data.tc11
			},
		],
		labels: labels
	};

	build_chart(datas, options, ctxBar, 0, 'bar', name);
}

function get_3g_stack_bar_chart(data = '', labels = '', name){
	name = wcl3g === true ? name + '-wcl3g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			fontfontSize: 5,
			boxWidth: 10,
			display: false
		},
		layout: pad,
		scales: {
			xAxes: [{
				stacked: true
			}],
			yAxes: [{
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	};

	let datas = {
		datasets: [
			{
				label: 'VS HSDPA RAB AbnormRel',
				backgroundColor: '#0e49b5',
				data: data.tp1
			},
			{
				label: 'VS HSDPA RAB AbnormRel RF',
				backgroundColor: '#fb743e',
				data: data.tp2
			},
			{
				label : 'VS HSDPA RAB AbnormRel H2P',
				backgroundColor: "#ffd31d",
				data: data.tp3
			},
			{
				label : 'VS HSDPA RAB FailEstab DLPower Cong',
				backgroundColor: "#0779e4",
				data: data.tp4
			},
			{
				label : 'VS HSDPA RAB FailEstab DLIUBBand Cong',
				backgroundColor: "#54e346",
				data: data.tp5
			},
		],
		labels: labels
	};

	build_chart(datas, options, ctxBar, 0, 'bar', name);
}

function get_3g_rrc_stack_bar_chart(data = '', labels = '', name){
	name = wcl3g === true ? name + '-wcl3g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			fontfontSize: 5,
			boxWidth: 10,
			display: false
		},
		layout: pad,
		scales: {
			xAxes: [{
				stacked: true
			}],
			yAxes: [{
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	};

	let datas = {
		datasets: [
			{
				label: 'VS RRC Rej Redir Dist',
				backgroundColor: '#0e49b5',
				data: data.tp1
			},
			{
				label: 'VS RRC Rej DLCE Cong',
				backgroundColor: '#fb743e',
				data: data.tp2
			},
			{
				label: 'VS RRC Rej ULCE Cong',
				backgroundColor: "#ffd31d",
				data: data.tp3
			},
			{
				label: 'VS RRC Rej DLIUBBand Cong',
				backgroundColor: "#0779e4",
				data: data.tp4
			},
			{
				label: 'VS RRC Rej ULIUBBand Cong',
				backgroundColor: "#54e346",
				data: data.tp5
			},
			{
				label: 'VS RRC Rej DLPower Cong',
				data: data.tp6,
				backgroundColor: '#931a25',
			},
			{
				label: 'VS RRC Rej ULPower Cong',
				fill: true,
				backgroundColor: "#f55c47",
				data: data.tp7
			},
			{
				label: 'VS RRC Rej Redir Service',
				fill: true,
				backgroundColor: "#f21170",
				data: data.tp8
			},
			{
				label: 'VS RRC Rej Redir InterRAT',
				fill: true,
				backgroundColor: "#892cdc",
				data: data.tp9
			},
			{
				label: 'VS RRC Rej Redir InterRAT',
				fill: true,
				backgroundColor: "#1f441e",
				data: data.tp10
			},
		],
		labels: labels
	};

	build_chart(datas, options, ctxBar, 0, 'bar', name);
}

function get_3g_rrj_chart(line = '', data = '', labels = '', name, ch1 = '', ch2 = '', max = 0) {
	name = wcl3g === true ? name + '-wcl3g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let  options = {
		stacked: false,
		layout: pad,
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			display: false
		},
		scales: {
			xAxes: [{
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}],
			yAxes: [{
				id: 'A',
				type: 'linear',
				position: 'left',
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}, {
				id: 'B',
				type: 'linear',
				position: 'right',
				stacked: true,
				ticks: {
					max: max,
					min: 0,
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				},
			}],
		},
	}

	let datas = {
		datasets: [
			{
				type: 'line',
				fill: false,
				yAxisID: 'B',
				label: 'VS RRC Rej Sum',
				data: line,
				backgroundColor: 'rgba(255, 255, 255, 0.9)',
				borderColor: '#0e49b5',
				borderWidth: 3,
			},
			{
				fill: false,
				label: 'VS RRC Rej TNL Fail',
				data: data.b1,
				yAxisID: 'A',
				backgroundColor: '#fb743e',
				borderWidth: 3,
				borderColor: '#fb743e',
			},
			{
				label: 'VS RRC Rej RL Fail',
				data: data.b2,
				yAxisID: 'A',
				backgroundColor: '#eb596e',
				borderWidth: 3,
				borderColor: '#eb596e',
			},
			{
				label : 'VS RRC Rej Power Cong',
				data: data.b3,
				yAxisID: 'A',
				backgroundColor: '#ffd31d',
				borderWidth: 3,
				borderColor: '#ffd31d'
			},
			{
				label : 'VS RRC Rej Code Cong',
				data: data.b4,
				yAxisID: 'A',
				backgroundColor: '#54e346',
				borderWidth: 3,
				borderColor: '#54e346'
			}
		],
		labels: labels,
	};

	build_chart(datas, options, ctxBar, 0, ch1, name);
}

function get_3g_rce_chart(line = '', data = '', labels = '', name, ch1 = '', ch2 = '', max = 0) {
	name = wcl3g === true ? name + '-wcl3g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let  options = {
		stacked: false,
		layout: pad,
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			display: false
		},
		scales: {
			xAxes: [{
				stacked: true
			}],
			yAxes: [{
				id: 'A',
				type: 'linear',
				position: 'left',
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}, {
				id: 'B',
				type: 'linear',
				position: 'right',
				stacked: true,
				ticks: {
					max: max,
					min: 0
				},
			}],
		},
	}

	let datas = {
		datasets: [
			{
				type: 'line',
				fill: false,
				yAxisID: 'B',
				label: 'VS RAB FailEstab CS Cong',
				data: line,
				backgroundColor: 'rgba(255, 255, 255, 0.9)',
				borderColor: '#0e49b5',
				borderWidth: 3,
			},
			{
				fill: false,
				label: 'VS RAB FailEstab CS Code Cong',
				data: data.b1,
				yAxisID: 'A',
				backgroundColor: '#fb743e',
				borderWidth: 3,
				borderColor: '#fb743e',
			},
			{
				label: 'VS RAB FailEst Cs Power Cong',
				data: data.b2,
				yAxisID: 'A',
				backgroundColor: '#eb596e',
				borderWidth: 3,
				borderColor: '#eb596e',
			},
			{
				label : 'VSnRAB FailEstab CS UuNoReply',
				data: data.b3,
				yAxisID: 'A',
				backgroundColor: '#ffd31d',
				borderWidth: 3,
				borderColor: '#ffd31d'
			},
			{
				label : 'VS RAB FailEstab CS RNL',
				data: data.b4,
				yAxisID: 'A',
				backgroundColor: '#54e346',
				borderWidth: 3,
				borderColor: '#54e346'
			},
			{
				label : 'VS RAB FailEstab CS UuFail',
				data: data.b5,
				yAxisID: 'A',
				backgroundColor: '#892cdc',
				borderWidth: 3,
				borderColor: '#892cdc'
			},
			{
				label : 'VS RAB FailEstab CS TNL',
				data: data.b6,
				yAxisID: 'A',
				backgroundColor: '#966c3b',
				borderWidth: 3,
				borderColor: '#966c3b'
			}
		],
		labels: labels,
	};

	build_chart(datas, options, ctxBar, 0, ch1, name);
}

function get_3g_rca_chart(line = '', data = '', labels = '', name, ch1 = '', ch2 = '', max = 0) {
	name = wcl3g === true ? name + '-wcl3g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let  options = {
		stacked: false,
		layout: pad,
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			display: false
		},
		scales: {
			xAxes: [{
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}],
			yAxes: [{
				id: 'A',
				type: 'linear',
				position: 'left',
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}, {
				id: 'B',
				type: 'linear',
				position: 'right',
				stacked: true,
				ticks: {
					max: max,
					min: 0
				},
			}],
		},
	}

	let datas = {
		datasets: [
			{
				type: 'line',
				fill: false,
				yAxisID: 'B',
				label: 'VS RAB AbnormRel CS',
				data: line,
				backgroundColor: 'rgba(255, 255, 255, 0.9)',
				borderColor: '#0e49b5',
				borderWidth: 3,
			},
			{
				fill: false,
				label: 'VS RAB AbnormRel CS OLC',
				data: data.b1,
				yAxisID: 'A',
				backgroundColor: '#fb743e',
				borderWidth: 3,
				borderColor: '#fb743e',
			},
			{
				label: 'VS RAB AbnormRel CS RF SF SRBReset',
				data: data.b2,
				yAxisID: 'A',
				backgroundColor: '#eb596e',
				borderWidth: 3,
				borderColor: '#eb596e',
			},
			{
				label : 'VS RAB AbnormRel CS IuAAL2',
				data: data.b3,
				yAxisID: 'A',
				backgroundColor: '#ffd31d',
				borderWidth: 3,
				borderColor: '#ffd31d'
			},
			{
				label : 'VS RAB AbnormRel CS Preempt',
				data: data.b4,
				yAxisID: 'A',
				backgroundColor: '#54e346',
				borderWidth: 3,
				borderColor: '#54e346'
			},
			{
				label : 'VS RAB AbnormRel CS RF',
				data: data.b5,
				yAxisID: 'A',
				backgroundColor: '#892cdc',
				borderWidth: 3,
				borderColor: '#892cdc'
			},
			{
				label : 'VS RAB AbnormRel CS RF UuNoReply',
				data: data.b6,
				yAxisID: 'A',
				backgroundColor: '#966c3b',
				borderWidth: 3,
				borderColor: '#966c3b'
			},
			{
				label : 'VS RAB AbnormRel CS RF ULSync',
				data: data.b7,
				yAxisID: 'A',
				backgroundColor: '#364547',
				borderWidth: 3,
				borderColor: '#364547'
			},
			{
				label : 'VS RAB AbnormRel CS OM',
				data: data.b8,
				yAxisID: 'A',
				backgroundColor: '#16c79a',
				borderWidth: 3,
				borderColor: '#16c79a'
			}

		],
		labels: labels,
	};

	build_chart(datas, options, ctxBar, 0, ch1, name);
}

function get_3g_rpa_chart(line = '', data = '', labels = '', name, ch1 = '', ch2 = '', max = 0) {
	name = wcl3g === true ? name + '-wcl3g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let  options = {
		stacked: false,
		layout: pad,
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			display: false
		},
		scales: {
			xAxes: [{
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}],
			yAxes: [{
				id: 'A',
				type: 'linear',
				position: 'left',
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}, {
				id: 'B',
				type: 'linear',
				position: 'right',
				stacked: false,
				ticks: {
					max: max,
					min: 0,
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				},
			}],
		},
	}

	let datas = {
		datasets: [
			{
				type: 'line',
				fill: false,
				label: 'VS RAB AbnormRel PS RF',
				data: line.l2,
				yAxisID: 'B',
				backgroundColor: 'rgba(255, 255, 255, 0.9)',
				borderWidth: 3,
				borderColor: '#fb743e',
			},
			{
				type: 'line',
				fill: false,
				yAxisID: 'B',
				label: 'VS RAB AbnormRel PS',
				data: line.l1,
				backgroundColor: 'rgba(255, 255, 255, 0.9)',
				borderColor: '#0e49b5',
				borderWidth: 3,
			},
			{
				label: 'VS RAB AbnormRel PS GTPULoss',
				data: data.b1,
				yAxisID: 'A',
				backgroundColor: '#eb596e',
				borderWidth: 3,
				borderColor: '#eb596e',
			},
			{
				label : 'VS RAB AbnormRel PS OLC',
				data: data.b2,
				yAxisID: 'A',
				backgroundColor: '#ffd31d',
				borderWidth: 3,
				borderColor: '#ffd31d'
			},
			{
				label : 'VS RAB AbnormRel PS RF SRBReset',
				data: data.b3,
				yAxisID: 'A',
				backgroundColor: '#54e346',
				borderWidth: 3,
				borderColor: '#54e346'
			},
			{
				label : 'VS RAB AbnormRel PS Preempt',
				data: data.b4,
				yAxisID: 'A',
				backgroundColor: '#892cdc',
				borderWidth: 3,
				borderColor: '#892cdc'
			},
			{
				label : 'VS RAB AbnormRel PS RF TRBReset',
				data: data.b5,
				yAxisID: 'A',
				backgroundColor: '#966c3b',
				borderWidth: 3,
				borderColor: '#966c3b'
			},
			{
				label : 'VS RAB AbnormRel PS RF UuNoReply',
				data: data.b6,
				yAxisID: 'A',
				backgroundColor: '#364547',
				borderWidth: 3,
				borderColor: '#364547'
			},
			{
				label : 'VS RAB AbnormRel PS OM',
				data: data.b7,
				yAxisID: 'A',
				backgroundColor: '#16c79a',
				borderWidth: 3,
				borderColor: '#16c79a'
			},
			{
				label : 'VS RAB AbnormRel PS RF ULSync',
				data: data.b8,
				yAxisID: 'A',
				backgroundColor: '#ff4646',
				borderWidth: 3,
				borderColor: '#ff4646'
			}

		],
		labels: labels,
	};

	build_chart(datas, options, ctxBar, 0, ch1, name);
}

function get_3g_isho_chart(line = '', data = '', labels = '', name, ch1 = '', ch2 = '', max = 0) {
	name = wcl3g === true ? name + '-wcl3g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let  options = {
		stacked: false,
		layout: pad,
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			display: false
		},
		scales: {
			xAxes: [{
				stacked: true
			}],
			yAxes: [{
				id: 'A',
				type: 'linear',
				position: 'left',
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}, {
				id: 'B',
				type: 'linear',
				position: 'right',
				stacked: false,
				ticks: {
					max: max,
					min: 0
				},
			}],
		},
	}

	let datas = {
		datasets: [
			{
				type: 'line',
				fill: false,
				label: 'ISHO (3G to 2G) Successful Rate RT(%)',
				data: line.l2,
				yAxisID: 'B',
				backgroundColor: 'rgba(255, 255, 255, 0.9)',
				borderWidth: 3,
				borderColor: '#fb743e',
			},
			{
				type: 'line',
				fill: false,
				yAxisID: 'B',
				label: 'Sum of ISHO (3G to 2G) Successful Rate NRT(%)',
				data: line.l1,
				backgroundColor: 'rgba(255, 255, 255, 0.9)',
				borderColor: '#0e49b5',
				borderWidth: 3,
			},
			{
				label: 'NPM ISHO NRT_Den',
				data: data.b1,
				yAxisID: 'A',
				backgroundColor: '#eb596e',
				borderWidth: 3,
				borderColor: '#eb596e',
			},
			{
				label : 'NPM ISHO RT_Den',
				data: data.b2,
				yAxisID: 'A',
				backgroundColor: '#ffd31d',
				borderWidth: 3,
				borderColor: '#ffd31d'
			},
		],
		labels: labels,
	};

	build_chart(datas, options, ctxBar, 0, ch1, name);
}

