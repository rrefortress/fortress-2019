let wcl5g = false;

function get_5g_graph(data = []) {
	let chart = $('.chart-loader');
	chart.find('div.c5g-dis').addClass('d-none');
	let param = {
		url: url + 'get_5g_graph',
		dataType: 'json',
		type: 'POST',
		data: data,
		beforeSend: chart.append('<div class="col-12 c5g-load" style="margin-top: 5% !important;">' +
			'<h5 class="text-center text-white">' +
			'<img src='+loaders+' width="120" height="120" alt="loader" class="rounded-circle"><br>\n' +
			'<em class="text-muted small text-process">Loading Chart</em>' +
			'</h5>' +
			'</div>')
	};

	fortress(param).then((data) => {
		chart.find('div.c5g-load').remove();
		chart.find('div.c5g-dis').removeClass('d-none');

		set_5g_basic_chart(data.acc, data.date, 'acc', 'NR SgNB Addition Success Rate(%)', 'N NsaDc SgNB Add Att');
		set_5g_basic_chart(data.ret, data.date, 'ret', 'NR SgNB Abnormal Release Rate %', 'N NsaDc SgNB Rel');

		set_5g_2_bar_chart(data.tua, data.date, 'tua', 'bar', 'N.Cell.Unavail.Dur.System(s)', 'N.Cell.Unavail.Dur.Manual(s)', false);
		set_5g_2_bar_chart(data.au, data.date, 'au', 'bar', 'Average User Number', 'N User NsaDc PSCell Avg', false);
		set_5g_2_bar_chart(data.prb, data.date, 'prb', 'line', 'Average User Number', 'N User NsaDc PSCell Avg', false);
		set_5g_2_bar_chart(data.tv, data.date, 'tv', 'bar', 'DL Traffic Volume', 'UL Traffic Volume', false);
		set_5g_2_bar_chart(data.ct, data.date, 'ct', 'bar', 'Cell DL Average Throughput', 'NR Cell UL Average Throughput', false);
		set_5g_2_bar_chart(data.ut, data.date, 'ut', 'bar', 'User DL Average Throughput', 'NR User UL Average Throughput', false);
		set_5g_2_bar_chart(data.sfr, data.date, 'sfr', 'bar', 'N.NsaDc.SgNB.Add.Fail.Radio', 'N.NsaDc.SgNB.Add.Fail.TNL', true);
		set_5g_2_bar_chart(data.mob, data.date, 'mob', 'line', 'Intra-SgNB Pscell Change SR', 'Inter-SgNB Pscell Change SR', false);

		set_5g_3_bar_chart(data.srfr, data.date, 'srfr', 'bar', 'N.NsaDc.SgNB.Add.Fail.Radio.License', 'N.NsaDc.SgNB.Add.Fail.Radio.NoRes', 'N.NsaDc.SgNB.Add.Fail.Radio.UeCapability', true);
		set_5g_3_bar_chart(data.arr, data.date, 'arr', 'bar', 'N.NsaDc.SgNB.AbnormRel.NoReply', 'N.NsaDc.SgNB.AbnormRel.Radio', 'N.NsaDc.SgNB.AbnormRel.Trans', true);
		set_5g_3_bar_chart(data.arrr, data.date, 'arrr', 'bar', 'N.NsaDc.SgNB.AbnormRel.Radio.SUL', 'N.NsaDc.SgNB.AbnormRel.Radio.UeLost', 'N.NsaDc.SgNB.AbnormRel.Radio.ULSyncFail', true);

		set_5g_single_line_chart(data.ulf, data.date, 'ulf');

		set_5g_stack_line_chart(data.ta, data.date, 'tia');
	});

	function set_5g_basic_chart(data, vdate, type, $l1, $l2) {
		let line = [];
		let bar = [];
		let date = [];

		_.forEach(data.v1, (value) => {
			line.push(value);
		});

		_.forEach(data.v2, (value) => {
			bar.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		get_5g_basic_chart(line, bar, date, type, 'bar', 'line', true, _.max(line), $l1, $l2);
	}

	function set_5g_2_bar_chart(data, vdate, type, chtype, lb1, lb2, stacked) {
		let t1 = [];
		let t2 = [];
		let date = [];

		_.forEach(data.v1, (value) => {
			t1.push(value);
		});

		_.forEach(data.v2, (value) => {
			t2.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			't1' : t1,
			't2' : t2,
		};

		get_5g_2_bar_chart(datas, date, type, chtype, lb1, lb2, stacked);
	}

	function set_5g_3_bar_chart(data, vdate, type, chtype, lb1, lb2, lb3, stacked) {
		let t1 = [];
		let t2 = [];
		let t3 = [];
		let date = [];

		_.forEach(data.v1, (value) => {
			t1.push(value);
		});

		_.forEach(data.v2, (value) => {
			t2.push(value);
		});

		_.forEach(data.v3, (value) => {
			t3.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			't1' : t1,
			't2' : t2,
			't3' : t3,
		};

		get_5g_3_bar_chart(datas, date, type, chtype, lb1, lb2, lb3, stacked);
	}

	function set_5g_single_line_chart(data, vdate, name) {
		let avg = [];
		let date = [];

		_.forEach(data, (value) => {
			avg.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		get_5g_single_line_chart(avg, date, name);
	}

	function set_5g_stack_line_chart(data, vdate, type) {
		let ta_0 = [];
		let ta_1 = [];
		let ta_2 = [];
		let ta_3 = [];
		let ta_4 = [];
		let ta_5 = [];
		let ta_6 = [];
		let ta_7 = [];
		let ta_8 = [];
		let ta_9 = [];
		let ta_10 = [];
		let ta_11 = [];
		let ta_12 = [];
		let date = [];

		_.forEach(data.ta0, (value) => {
			ta_0.push(value);
		});

		_.forEach(data.ta1, (value) => {
			ta_1.push(value);
		});

		_.forEach(data.ta2, (value) => {
			ta_2.push(value);
		});

		_.forEach(data.ta3, (value) => {
			ta_3.push(value);
		});

		_.forEach(data.ta4, (value) => {
			ta_4.push(value);
		});

		_.forEach(data.ta5, (value) => {
			ta_5.push(value);
		});

		_.forEach(data.ta6, (value) => {
			ta_6.push(value);
		});

		_.forEach(data.ta7, (value) => {
			ta_7.push(value);
		});

		_.forEach(data.ta8, (value) => {
			ta_8.push(value);
		});

		_.forEach(data.ta9, (value) => {
			ta_9.push(value);
		});

		_.forEach(data.ta10, (value) => {
			ta_10.push(value);
		});

		_.forEach(data.ta11, (value) => {
			ta_11.push(value);
		});

		_.forEach(data.ta12, (value) => {
			ta_12.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			't0' : ta_0,
			't1' : ta_1,
			't2' : ta_2,
			't3' : ta_3,
			't4' : ta_4,
			't5' : ta_5,
			't6' : ta_6,
			't7' : ta_7,
			't8' : ta_8,
			't9' : ta_9,
			't10' : ta_10,
			't11' : ta_11,
			't12' : ta_12
		}

		get_5g_ta_stack_line_chart(datas, date, type, 'line');
	}
}

function get_5g_basic_chart(avg = '', sum = '', labels = '', name, ch1 = '', ch2 = '', legend, max = 0, $l1, $l2) {
	name = wcl5g === true ? name + '-wcl5g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let  options = {
		stacked: false,
		layout: pad,
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			display: legend
		},
		scales: {
			yAxes: [{
				id: 'A',
				type: 'linear',
				position: 'left',
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}, {
				id: 'B',
				type: 'linear',
				position: 'right',
				ticks: {
					max: max,
					min: 0,
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	}

	let datas = {
		datasets: [
			{
				type: 'line',
				fill: false,
				yAxisID: 'B',
				label: $l1,
				data: avg,
				backgroundColor: 'rgba(255, 255, 255, 0.9)',
				borderWidth: 3,
				borderColor: '#ffc107',
			},
			{
				fill: false,
				label: $l2,
				data: sum,
				yAxisID: 'A',
				backgroundColor: '#198754',
				borderWidth: 3,
				borderColor: '#198754',
			},

		],
		labels: labels,
	};

	build_chart(datas, options, ctxBar, 0, ch1, name);
}

function get_5g_2_bar_chart(data = '', labels = '', name, ch1 = '', lb1 = '', lb2 = '', stacked = false) {
	name = wcl5g === true ? name + '-wcl5g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			fontfontSize: 5,
			boxWidth: 10,
			display: true
		},
		layout: pad,
		scales: {
			xAxes: [{
				stacked: stacked
			}],
			yAxes: [{
				stacked: stacked,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	};

	let datas = {
		datasets: [
			{
				fill: false,
				label: lb1,
				backgroundColor: ch1 === 'line' ? 'rgba(255, 255, 255, 0.9)' : '#ffc107',
				borderWidth: 3,
				borderColor: '#ffc107',
				data: data.t1
			},
			{
				fill: false,
				label: lb2,
				backgroundColor: ch1 === 'line' ? 'rgba(255, 255, 255, 0.9)' : '#198754',
				borderWidth: 3,
				borderColor: '#198754',
				data: data.t2
			}
		],
		labels: labels
	};

	build_chart(datas, options, ctxBar, 0, ch1, name);
}

function get_5g_3_bar_chart(data = '', labels = '', name, ch1 = '', lb1 = '', lb2 = '', lb3 = '', stacked = false) {
	name = wcl5g === true ? name + '-wcl5g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			fontfontSize: 5,
			boxWidth: 10,
			display: false
		},
		layout: pad,
		scales: {
			xAxes: [{
				stacked: stacked
			}],
			yAxes: [{
				stacked: stacked,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	};

	let datas = {
		datasets: [
			{
				fill: false,
				label: lb1,
				backgroundColor: '#ffd31d',
				borderWidth: 3,
				borderColor: '#ffd31d',
				data: data.t1
			},
			{
				fill: false,
				label: lb2,
				backgroundColor: '#28a745',
				borderWidth: 3,
				borderColor: '#28a745',
				data: data.t2
			},
			{
				fill: false,
				label: lb3,
				backgroundColor: '#fb3640',
				borderWidth: 3,
				borderColor: '#fb3640',
				data: data.t3
			}
		],
		labels: labels
	};

	build_chart(datas, options, ctxBar, 0, ch1, name);
}

function get_5g_single_line_chart(data = '', labels = '', name) {
	name = wcl5g === true ? name + '-wcl5g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			fontfontSize: 5,
			boxWidth: 10,
			display: true
		},
		layout: pad,
		scales: {
			xAxes: [{
				stacked: false,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}],
			yAxes: [{
				stacked: false,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	};

	let datas = {
		datasets: [
			{
				fill: false,
				label: 'N.UL.NI.Avg(dBm)',
				backgroundColor: 'rgba(255, 255, 255, 0.9)',
				borderWidth: 3,
				borderColor: '#ffc107',
				data: data
			}
		],
		labels: labels
	};

	build_chart(datas, options, ctxBar, 0, 'line', name);
}

function get_5g_ta_stack_line_chart(data = '', labels = '', name) {
	name = wcl5g === true ? name + '-wcl5g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
			filler: {
				propagate: true
			}
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			fontfontSize: 5,
			boxWidth: 10,
			display: false
		},
		layout: pad,
		scales: {
			xAxes: [{
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}],
			yAxes: [{
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	};

	let datas = {
		datasets: [
			{
				fill: true,
				backgroundColor: "#f55c47",
				borderWidth: 3,
				borderColor: "#f55c47",
				data: data.t0,
				label: 'N.RA.TA.UE.Index0',
			},
			{
				fill: true,
				backgroundColor: "#28a745",
				borderWidth: 3,
				borderColor: "#28a745",
				data: data.t1,
				label: 'N.RA.TA.UE.Index1',
			},
			{
				fill: true,
				backgroundColor: "#ffd31d",
				borderWidth: 3,
				borderColor: "#ffd31d",
				data: data.t2,
				label: 'N.RA.TA.UE.Index2',
			},
			{
				fill: true,
				backgroundColor: "#0779e4",
				borderWidth: 3,
				borderColor: "#0779e4",
				data: data.t3,
				label: 'N.RA.TA.UE.Index3',
			},
			{
				fill: true,
				backgroundColor: "#54e346",
				borderWidth: 3,
				borderColor: "#54e346",
				data: data.t4,
				label: 'N.RA.TA.UE.Index4',
			},
			{
				fill: true,
				backgroundColor: "#511281",
				borderWidth: 3,
				borderColor: "#511281",
				data: data.t5,
				label: 'N.RA.TA.UE.Index5',
			},
			{
				fill: true,
				backgroundColor: "#393e46",
				borderWidth: 3,
				borderColor: "#393e46",
				data: data.t6,
				label: 'N.RA.TA.UE.Index6',
			},
			{
				fill: true,
				backgroundColor: "#f55c47",
				borderWidth: 3,
				borderColor: "#f55c47",
				data: data.t7,
				label: 'N.RA.TA.UE.Index7',
			},
			{
				fill: true,
				backgroundColor: "#f21170",
				borderWidth: 3,
				borderColor: "#f21170",
				data: data.t8,
				label: 'N.RA.TA.UE.Index8',
			},
			{
				fill: true,
				backgroundColor: "#9ede73",
				borderWidth: 3,
				borderColor: "#9ede73",
				data: data.t9,
				label: 'N.RA.TA.UE.Index9',
			},
			{
				fill: true,
				backgroundColor: "#966c3b",
				borderWidth: 3,
				borderColor: "#966c3b",
				data: data.t10,
				label: 'N.RA.TA.UE.Index10',
			},
			{
				fill: true,
				backgroundColor: "#383e56",
				borderWidth: 3,
				borderColor: "#383e56",
				data: data.t11,
				label: 'N.RA.TA.UE.Index11',
			},
			{
				fill: true,
				backgroundColor: "#161d6f",
				borderWidth: 3,
				borderColor: "#161d6f",
				data: data.t12,
				label: 'N.RA.TA.UE.Index12',
			}
		],
		labels: labels
	};

	build_chart(datas, options, ctxBar, 0, 'line', name);
}
