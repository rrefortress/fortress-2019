Dropzone.autoDiscover = false;
let pad =  {
	left: 5,
	right: 5,
	top: 10,
	bottom: 5
};

let ktype = $('#ktype').val();

let f_date_2g_length = 0;
let f_time_2g_length = 0;
let f_cell_2g_length = 0;

let f_date_3g_length = 0;
let f_time_3g_length = 0;
let f_cell_3g_length = 0;

let f_date_4g_length = 0;
let f_time_4g_length = 0;
let f_cell_4g_length = 0;

let f_date_5g_length = 0;
let f_time_5g_length = 0;
let f_cell_5g_length = 0;

let f_date_volte_length = 0;
let f_time_volte_length = 0;
let f_cell_volte_length = 0;

$(function () {
	if (ktype === 'ANALYZER') {
		get_filter_menus('2g');
		get_filter_menus('3g');
		get_filter_menus('4g');
		get_filter_menus('5g');

		get_2g_graph();
		get_3g_graph();
		get_4g_graph();
		get_5g_graph();
	}
});

function get_filter_menus(tech) {
	let param = {
		url: url + 'get_filter_menus',
		dataType: 'json',
		type: 'POST',
		data: {tech: tech},
		beforeSend: $('.list-'+tech).html('<a href="#" class="small border-0 list-group-item list-group-item-action active" aria-current="true">Loading</a>')
	};

	fortress(param).then((data) => {
		set_filters(data.cell, 'cell', tech);
		set_filters(data.time, 'time', tech);
		set_filters(data.date, 'date', tech);

		switch (tech) {
			case '2g':
				f_date_2g_length = data.date.length;
				f_time_2g_length = data.time.length;
				f_cell_2g_length = data.cell.length;
				break;
			case '3g':
				f_date_3g_length = data.date.length;
				f_time_3g_length = data.time.length;
				f_cell_3g_length = data.cell.length;
				break;
			case '4g':
				f_date_4g_length = data.date.length;
				f_time_4g_length = data.time.length;
				f_cell_4g_length = data.cell.length;
				break;
			case '5g':
				f_date_5g_length = data.date.length;
				f_time_5g_length = data.time.length;
				f_cell_5g_length = data.cell.length;
				break;
		}
	});
}

function set_filters(data, name, tech) {
	let id = '#'+name+'-'+tech;
	let html = '';
	if (data.length > 0) {
		_.forEach(data, function (val) {
			html += '<option>'+val.name+'</option>';
		});
	}
	$(id).html(html).multiselect({
		nonSelectedText: 'Select',
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		buttonWidth:'280px',
		includeSelectAllOption: true,
		allSelectedText: 'All '+ _.startCase(name) +' Selected',
		maxHeight: 420,
		dropRight: true,
		// onChange: function() {
		// 	let brands = $(id + ' option:selected');
		// 	$(brands).each(function(){
		// 		selected.push($(this).val());
		// 	})
		// }
	});

	$(id).multiselect('selectAll', false).multiselect('updateButtonText');
}

let cell_2g = [];
let date_2g = [];
let time_2g = [];

let cell_3g = [];
let date_3g = [];
let time_3g = [];

let cell_4g = [];
let date_4g = [];
let time_4g = [];

let cell_5g = [];
let date_5g = [];
let time_5g = [];

$(document).on('change', '#cell-2g', function (e) {
	e.preventDefault();
	cell_2g = [];
	let brands = $(this).find('option:selected');
	let type = $(this).data('type');
	let tech = $(this).data('tech').toUpperCase();
	let leng = f_cell_2g_length;

	if ($(this).val() === '' || $(this).val().length === 0 || ($(this).val().length === leng)) {
		cell_2g = [];
	} else {
		$(brands).each(function(){
			cell_2g.push($(this).val());
		})
	}

	get_2g_graph({date: date_2g, time: time_2g, cell: cell_2g, type: type});
});

$(document).on('change', '#date-2g', function (e) {
	e.preventDefault();
	date_2g = [];
	let brands = $(this).find('option:selected');
	let type = $(this).data('type');
	let tech = $(this).data('tech').toUpperCase();
	let leng = f_date_2g_length;

	if ($(this).val() === '' || $(this).val().length === 0 || ($(this).val().length === leng)) {
		date_2g = [];
	} else {
		$(brands).each(function(){
			date_2g.push($(this).val());
		})
	}

	get_2g_graph({date: date_2g, time: time_2g, cell: cell_2g, type: type});
});

$(document).on('change', '#time-2g', function (e) {
	e.preventDefault();
	time_2g = [];
	let brands = $(this).find('option:selected');
	let type = $(this).data('type');
	let tech = $(this).data('tech').toUpperCase();
	let leng = f_time_2g_length;

	if ($(this).val() === '' || $(this).val().length === 0 || ($(this).val().length === leng)) {
		time_2g = [];
	} else {
		$(brands).each(function(){
			time_2g.push($(this).val());
		})
	}
	get_2g_graph({date: date_2g, time: time_2g, cell: cell_2g, type: type});
});


$(document).on('change', '#cell-3g', function (e) {
	e.preventDefault();
	cell_3g = [];
	let brands = $(this).find('option:selected');
	let type = $(this).data('type');
	let tech = $(this).data('tech').toUpperCase();

	let leng = f_cell_3g_length;

	if ($(this).val() === '' || $(this).val().length === 0 || ($(this).val().length === leng)) {
		cell_3g = [];
	} else {
		$(brands).each(function(){
			cell_3g.push($(this).val());
		})
	}

	get_3g_graph({date: date_3g, time: time_3g, cell: cell_3g, type: type});
});

$(document).on('change', '#date-3g', function (e) {
	e.preventDefault();
	date_3g = [];
	let brands = $(this).find('option:selected');
	let type = $(this).data('type');
	let tech = $(this).data('tech').toUpperCase();

	let leng = f_date_3g_length;

	if ($(this).val() === '' || $(this).val().length === 0 || ($(this).val().length === leng)) {
		date_3g = [];
	} else {
		$(brands).each(function(){
			date_3g.push($(this).val());
		})
	}

	get_3g_graph({date: date_3g, time: time_3g, cell: cell_3g, type: type});
});

$(document).on('change', '#time-3g', function (e) {
	e.preventDefault();
	time_3g = [];
	let brands = $(this).find('option:selected');
	let type = $(this).data('type');
	let tech = $(this).data('tech').toUpperCase();
	let leng = f_time_3g_length;

	if ($(this).val() === '' || $(this).val().length === 0 || ($(this).val().length === leng)) {
		time_3g = [];
	} else {
		$(brands).each(function(){
			time_3g.push($(this).val());
		})
	}
	get_3g_graph({date: date_3g, time: time_3g, cell: cell_3g, type: type});
});


$(document).on('change', '#cell-4g', function (e) {
	e.preventDefault();
	cell_4g = [];
	let brands = $(this).find('option:selected');
	let type = $(this).data('type');
	let tech = $(this).data('tech').toUpperCase();
	let leng = f_cell_4g_length;

	if ($(this).val() === '' || $(this).val().length === 0 || ($(this).val().length === leng)) {
		cell_4g = [];
	} else {
		$(brands).each(function(){
			cell_4g.push($(this).val());
		})
	}

	get_4g_graph({date: date_4g, time: time_4g, cell: cell_4g, type: type});
});

$(document).on('change', '#date-4g', function (e) {
	e.preventDefault();
	date_4g = [];
	let brands = $(this).find('option:selected');
	let type = $(this).data('type');
	let tech = $(this).data('tech').toUpperCase();
	let leng = f_date_4g_length;

	if ($(this).val() === '' || $(this).val().length === 0 || ($(this).val().length === leng)) {
		date_4g = [];
	} else {
		$(brands).each(function(){
			date_4g.push($(this).val());
		})
	}

	get_4g_graph({date: date_4g, time: time_4g, cell: cell_4g, type: type});
});

$(document).on('change', '#time-4g', function (e) {
	e.preventDefault();
	time_4g = [];
	let brands = $(this).find('option:selected');
	let type = $(this).data('type');
	let tech = $(this).data('tech').toUpperCase();
	let leng = f_time_4g_length;

	if ($(this).val() === '' || $(this).val().length === 0 || ($(this).val().length === leng)) {
		time_4g = [];
	} else {
		$(brands).each(function(){
			time_4g.push($(this).val());
		})
	}
	get_4g_graph({date: date_4g, time: time_4g, cell: cell_4g, type: type});
});


$(document).on('change', '#cell-5g', function (e) {
	e.preventDefault();
	cell_5g = [];
	let brands = $(this).find('option:selected');
	let type = $(this).data('type');
	let tech = $(this).data('tech').toUpperCase();
	let leng = f_cell_5g_length;

	if ($(this).val() === '' || $(this).val().length === 0 || ($(this).val().length === leng)) {
		cell_5g = [];
	} else {
		$(brands).each(function(){
			cell_5g.push($(this).val());
		})
	}

	get_5g_graph({date: date_5g, time: time_5g, cell: cell_5g, type: type});
});

$(document).on('change', '#date-5g', function (e) {
	e.preventDefault();
	date_5g = [];
	let brands = $(this).find('option:selected');
	let type = $(this).data('type');
	let tech = $(this).data('tech').toUpperCase();

	let leng = f_date_5g_length;

	if ($(this).val() === '' || $(this).val().length === 0 || ($(this).val().length === leng)) {
		date_5g = [];
	} else {
		$(brands).each(function(){
			date_5g.push($(this).val());
		})
	}

	get_5g_graph({date: date_5g, time: time_5g, cell: cell_5g, type: type});
});

$(document).on('change', '#time-5g', function (e) {
	e.preventDefault();
	time_5g = [];
	let brands = $(this).find('option:selected');
	let type = $(this).data('type');
	let tech = $(this).data('tech').toUpperCase();
	let leng = f_time_5g_length;

	if ($(this).val() === '' || $(this).val().length === 0 || ($(this).val().length === leng)) {
		time_5g = [];
	} else {
		$(brands).each(function(){
			time_5g.push($(this).val());
		})
	}
	get_5g_graph({date: date_5g, time: time_5g, cell: cell_5g, type: type});
});

function set_tech_filters(tech, type) {
	if (tech === '2G') {
		get_2g_graph(cell, date, time);
	} else if (tech === '3G') {
		get_3g_graph(cell, date, time);
	} else if (tech === '4G') {
		get_4g_graph(cell, date, time);
	} else if (tech === 'VOLTE') {

	} else if (tech === '5G') {
		get_5g_graph(cell, date, time);
	}
}
