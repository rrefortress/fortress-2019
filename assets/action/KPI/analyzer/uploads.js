$(document).on('click', '.kpi2g_upload', function (e) {
	e.preventDefault();
	$('#kpi2g_upload').modal('show');
});

let kpi2g_upload = new Dropzone(".dropzone2g_KPI",{
	url: url + "KPI/kpi_chart_2g_upload",
	timeout: 180000,
	parallelUploads: 1,
	maxFiles: 1,
	method:"POST",
	acceptedFiles:".csv",
	paramName:"kpi",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:false,
	maxFilesize: 500,
	init: function() {
		this.on("success", function(file, response) {
			let data = jQuery.parseJSON(response);
			if (data.type === 'error') {} else {
				get_filter_menus('2g');
				get_2g_graph();
			}
			SWAL(data.type, data.title, data.msg);
		})
	}
});

kpi2g_upload.on("queuecomplete", function () {
	this.removeAllFiles();
	$('#kpi2g_upload').modal('hide');
});
kpi2g_upload.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});
kpi2g_upload.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});
$(document).on('click', '.ana_2g_upload', function (e) {
	e.preventDefault();
	$('#ana_2g_upload').modal('show');
});

//---------------------------------------------------------------------------------------------------------------------------------//
$(document).on('click', '.kpi3g_upload', function (e) {
	e.preventDefault();
	$('#kpi3g_upload').modal('show');
});

let kpi3g_upload = new Dropzone(".dropzone3g_KPI",{
	url: url + "KPI/kpi_chart_3g_upload",
	timeout: 180000,
	parallelUploads: 1,
	maxFiles: 1,
	method:"POST",
	acceptedFiles:".csv",
	paramName:"kpi",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:false,
	maxFilesize: 500,
	init: function() {
		this.on("success", function(file, response) {
			let data = jQuery.parseJSON(response);
			if (data.type === 'error') {} else {
				get_filter_menus('3g');
				get_3g_graph();
			}
			SWAL(data.type, data.title, data.msg);
		})
	}
});

kpi3g_upload.on("queuecomplete", function () {
	this.removeAllFiles();
	$('#kpi3g_upload').modal('hide');
});
kpi3g_upload.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});
kpi3g_upload.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});
//---------------------------------------------------------------------------------------------------------------------------------//
$(document).on('click', '.kpi4g_upload', function (e) {
	e.preventDefault();
	$('#kpi4g_upload').modal('show');
});

let kpi4g_upload = new Dropzone(".dropzone4g_KPI",{
	url: url + "KPI/kpi_chart_4g_upload",
	timeout: 180000,
	parallelUploads: 1,
	maxFiles: 1,
	method:"POST",
	acceptedFiles:".csv",
	paramName:"kpi",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:false,
	maxFilesize: 500,
	init: function() {
		this.on("success", function(file, response) {
			let data = jQuery.parseJSON(response);
			if (data.type === 'error') {} else {
				get_filter_menus('4g');
				get_4g_graph();
			}
			SWAL(data.type, data.title, data.msg);
		})
	}
});

kpi4g_upload.on("queuecomplete", function () {
	this.removeAllFiles();
	$('#kpi4g_upload').modal('hide');
});
kpi4g_upload.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});
kpi4g_upload.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});
//---------------------------------------------------------------------------------------------------------------------------------//
$(document).on('click', '.kpi5g_upload', function (e) {
	e.preventDefault();
	$('#kpi5g_upload').modal('show');
});

let kpi5g_upload = new Dropzone(".dropzone5g_KPI",{
	url: url + "KPI/kpi_chart_5g_upload",
	timeout: 180000,
	parallelUploads: 1,
	maxFiles: 1,
	method:"POST",
	acceptedFiles:".csv",
	paramName:"kpi",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:false,
	maxFilesize: 500,
	init: function() {
		this.on("success", function(file, response) {
			let data = jQuery.parseJSON(response);
			if (data.type === 'error') {} else {
				get_filter_menus('5g');
				get_5g_graph();
			}
			SWAL(data.type, data.title, data.msg);
		})
	}
});

kpi5g_upload.on("queuecomplete", function () {
	this.removeAllFiles();
	$('#kpi5g_upload').modal('hide');
});
kpi5g_upload.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});
kpi5g_upload.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});
