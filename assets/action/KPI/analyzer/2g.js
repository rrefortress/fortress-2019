let wcl2g = false;

function get_2g_graph(data = []) {
	let chart = $('.chart-loader');
	chart.find('div.c2g-dis').addClass('d-none');
	let param = {
		url: url + 'get_2g_graph',
		dataType: 'json',
		type: 'POST',
		data: data,
		beforeSend: chart.append('<div class="col-12 c2g-load" style="margin-top: 5% !important;">' +
			'<h5 class="text-center text-white">' +
			'<img src='+loaders+' width="120" height="120" alt="loader" class="rounded-circle"><br>\n' +
			'<em class="text-muted small text-process">Loading Chart</em>' +
			'</h5>' +
			'</div>')
	};

	fortress(param).then((data) => {
		chart.find('div.c2g-load').remove();
		chart.find('div.c2g-dis').removeClass('d-none');
		set_2g_basic_chart(data.csfr, data.date, 'csfr');
		set_2g_basic_chart(data.dcr, data.date, 'dcr');
		set_2g_basic_chart(data.iafr, data.date, 'iafr');

		set_2g_single_line_chart(data.tch, data.date, 'tch');
		set_2g_multi_line_chart(data.cb, data.date, 'cb');

		set_2g_trf_bar_chart(data.trf, data.date, 'trf');
		set_2g_basic_chart(data.iho, data.date, 'iho');
		set_2g_basic_chart(data.oho, data.date, 'oho');

		set_2g_basic_chart(data.dlb, data.date, 'dlb');
		set_2g_basic_chart(data.ulb, data.date, 'ulb');

		set_2g_int_stack_bar_chart(data.sd, data.date, 'sd');
		set_2g_int_stack_bar_chart(data.tchhr, data.date, 'tchhr');
		set_2g_int_stack_bar_chart(data.tchfr, data.date, 'tchfr');

		set_2g_stack_line_chart(data.ta, data.date, 'ta');
		set_2g_sc_stack_bar_chart(data.sc, data.date, 'sc');

		set_2g_tcdc_stack_bar_chart(data.tcdc, data.date, 'tcdc');
	});

	function set_2g_basic_chart(data, vdate, type) {
		let avg = [];
		let sum = [];
		let date = [];

		_.forEach(data.avg, (value) => {
			avg.push(value);
		});

		_.forEach(data.sum, (value) => {
			sum.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		get_2g_basic_chart(avg, sum, date, type, 'bar', 'line', true, _.max(avg));
	}

	function set_2g_single_line_chart(data, vdate, type) {
		let avg = [];
		let date = [];

		_.forEach(data, (value) => {
			avg.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		get_2g_single_line_chart(avg, date, type, 'line', true);
	}

	function set_2g_multi_line_chart(data, vdate, type) {
		let tbr = [];
		let sblk = [];
		let sd = [];
		let date = [];

		_.forEach(data.tbr, (value) => {
			tbr.push(value);
		});

		_.forEach(data.sblk, (value) => {
			sblk.push(value);
		});

		_.forEach(data.sd, (value) => {
			sd.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'tbr' : tbr,
			'sblk' : sblk,
			'sd' : sd
		};

		get_2g_multi_line_chart(datas, date, type, 'line', true);
	}

	function set_2g_int_stack_bar_chart(data, vdate, type) {
		let int1 = [];
		let int2 = [];
		let int3 = [];
		let int4 = [];
		let int5 = [];
		let date = [];

		_.forEach(data.int1, (value) => {
			int1.push(value);
		});

		_.forEach(data.int2, (value) => {
			int2.push(value);
		});

		_.forEach(data.int3, (value) => {
			int3.push(value);
		});

		_.forEach(data.int4, (value) => {
			int4.push(value);
		});

		_.forEach(data.int5, (value) => {
			int5.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'int1' : int1,
			'int2' : int2,
			'int3' : int3,
			'int4' : int4,
			'int5' : int5
		};

		get_2g_int_stack_bar_chart(datas, date, type, 'bar', true);
	}

	function set_2g_trf_bar_chart(data, vdate, type) {
		let trf1 = [];
		let trf2 = [];
		let date = [];

		_.forEach(data.tr1, (value) => {
			trf1.push(value);
		});

		_.forEach(data.tr2, (value) => {
			trf2.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'trf1' : trf1,
			'trf2' : trf2,
		};

		get_2g_trf_bar_chart(datas, date, type, 'bar', true);
	}

	function set_2g_stack_line_chart(data, vdate, type) {
		let ta_0 = [];
		let ta_1 = [];
		let ta_2 = [];
		let ta_3 = [];
		let ta_4 = [];
		let ta_5 = [];
		let ta_6 = [];
		let ta_7 = [];
		let ta_8 = [];
		let ta_9 = [];
		let ta_10 = [];
		let ta_11 = [];
		let ta_12 = [];
		let ta_13 = [];
		let ta_14 = [];
		let ta_15 = [];
		let ta_16 = [];
		let ta_17 = [];
		let ta_18 = [];
		let ta_19 = [];
		let ta_20 = [];
		let ta_21 = [];
		let ta_22 = [];
		let ta_23 = [];
		let ta_24 = [];
		let ta_25 = [];
		let ta_26 = [];
		let ta_27 = [];
		let ta_28 = [];
		let ta_29 = [];
		let ta_30_to_31 = [];
		let ta_32_to_33 = [];
		let ta_34_to_35 = [];
		let ta_36_to_37 = [];
		let ta_38_to_39 = [];
		let ta_40_to_44 = [];
		let ta_45_to_49 = [];
		let ta_50_to_54 = [];
		let ta_55_to_63 = [];
		let ta_GT_63 = [];
		let date = [];

		_.forEach(data.ta0, (value) => {
			ta_0.push(value);
		});

		_.forEach(data.ta1, (value) => {
			ta_1.push(value);
		});

		_.forEach(data.ta2, (value) => {
			ta_2.push(value);
		});

		_.forEach(data.ta3, (value) => {
			ta_3.push(value);
		});

		_.forEach(data.ta4, (value) => {
			ta_4.push(value);
		});

		_.forEach(data.ta5, (value) => {
			ta_5.push(value);
		});

		_.forEach(data.ta6, (value) => {
			ta_6.push(value);
		});

		_.forEach(data.ta7, (value) => {
			ta_7.push(value);
		});

		_.forEach(data.ta8, (value) => {
			ta_8.push(value);
		});

		_.forEach(data.ta9, (value) => {
			ta_9.push(value);
		});

		_.forEach(data.ta10, (value) => {
			ta_10.push(value);
		});

		_.forEach(data.ta11, (value) => {
			ta_11.push(value);
		});

		_.forEach(data.ta12, (value) => {
			ta_12.push(value);
		});

		_.forEach(data.ta13, (value) => {
			ta_13.push(value);
		});

		_.forEach(data.ta14, (value) => {
			ta_14.push(value);
		});

		_.forEach(data.ta15, (value) => {
			ta_15.push(value);
		});

		_.forEach(data.ta16, (value) => {
			ta_16.push(value);
		});

		_.forEach(data.ta17, (value) => {
			ta_17.push(value);
		});

		_.forEach(data.ta18, (value) => {
			ta_18.push(value);
		});

		_.forEach(data.ta19, (value) => {
			ta_19.push(value);
		});

		_.forEach(data.ta20, (value) => {
			ta_20.push(value);
		});

		_.forEach(data.ta21, (value) => {
			ta_21.push(value);
		});

		_.forEach(data.ta22, (value) => {
			ta_22.push(value);
		});

		_.forEach(data.ta23, (value) => {
			ta_23.push(value);
		});

		_.forEach(data.ta24, (value) => {
			ta_24.push(value);
		});

		_.forEach(data.ta25, (value) => {
			ta_25.push(value);
		});

		_.forEach(data.ta26, (value) => {
			ta_26.push(value);
		});

		_.forEach(data.ta27, (value) => {
			ta_27.push(value);
		});

		_.forEach(data.ta28, (value) => {
			ta_28.push(value);
		});

		_.forEach(data.ta_29, (value) => {
			ta_29.push(value);
		});

		_.forEach(data.t30TO31, (value) => {
			ta_30_to_31.push(value);
		});

		_.forEach(data.t32TO33, (value) => {
			ta_32_to_33.push(value);
		});

		_.forEach(data.t34TO35, (value) => {
			ta_34_to_35.push(value);
		});

		_.forEach(data.t36TO37, (value) => {
			ta_36_to_37.push(value);
		});

		_.forEach(data.t38TO39, (value) => {
			ta_38_to_39.push(value);
		});

		_.forEach(data.t40TO44, (value) => {
			ta_40_to_44.push(value);
		});

		_.forEach(data.t45TO49, (value) => {
			ta_45_to_49.push(value);
		});

		_.forEach(data.t50TO54, (value) => {
			ta_50_to_54.push(value);
		});

		_.forEach(data.t55TO63, (value) => {
			ta_55_to_63.push(value);
		});

		_.forEach(data.tGT63, (value) => {
			ta_GT_63.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'ta0' : ta_0,
			'ta1' : ta_1,
			'ta2' : ta_2,
			'ta3' : ta_3,
			'ta4' : ta_4,
			'ta5' : ta_5,
			'ta6' : ta_6,
			'ta7' : ta_7,
			'ta8' : ta_8,
			'ta9' : ta_9,
			'ta10' : ta_10,
			'ta11' : ta_11,
			'ta12' : ta_12,
			'ta13' : ta_13,
			'ta14' : ta_14,
			'ta15' : ta_15,
			'ta16' : ta_16,
			'ta17' : ta_17,
			'ta18' : ta_18,
			'ta19' : ta_19,
			'ta20' : ta_20,
			'ta21' : ta_21,
			'ta22' : ta_22,
			'ta23' : ta_23,
			'ta24' : ta_24,
			'ta25' : ta_25,
			'ta26' : ta_26,
			'ta27' : ta_27,
			'ta28' : ta_28,
			'ta29' : ta_29,
			't30TO31' : ta_30_to_31,
			't32TO33' : ta_32_to_33,
			't34TO35' : ta_34_to_35,
			't36TO37' : ta_36_to_37,
			't38TO39' : ta_38_to_39,
			't40TO44' : ta_40_to_44,
			't45TO49' : ta_45_to_49,
			't50TO54' : ta_50_to_54,
			't55TO63' : ta_55_to_63,
			'tGT63'   : ta_GT_63,
		}

		get_2g_ta_stack_line_chart(datas, date, type, 'line');
	}

	function set_2g_sc_stack_bar_chart(data, vdate, type) {
		let int1 = [];
		let int2 = [];
		let int3 = [];
		let int4 = [];
		let date = [];

		_.forEach(data.sc1, (value) => {
			int1.push(value);
		});

		_.forEach(data.sc2, (value) => {
			int2.push(value);
		});

		_.forEach(data.sc3, (value) => {
			int3.push(value);
		});

		_.forEach(data.sc4, (value) => {
			int4.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'int1' : int1,
			'int2' : int2,
			'int3' : int3,
			'int4' : int4,
		};

		get_2g_sc_stack_bar_chart(datas, date, type, 'bar', true);
	}

	function set_2g_tcdc_stack_bar_chart(data, vdate, type) {
		let int1 = [];
		let int2 = [];
		let int3 = [];
		let int4 = [];
		let int5 = [];
		let int6 = [];
		let int7 = [];
		let int8 = [];
		let int9 = [];
		let int10 = [];
		let int11 = [];
		let date = [];

		_.forEach(data.tc1, (value) => {
			int1.push(value);
		});

		_.forEach(data.tc2, (value) => {
			int2.push(value);
		});

		_.forEach(data.tc3, (value) => {
			int3.push(value);
		});

		_.forEach(data.tc4, (value) => {
			int4.push(value);
		});

		_.forEach(data.tc5, (value) => {
			int5.push(value);
		});

		_.forEach(data.tc6, (value) => {
			int6.push(value);
		});

		_.forEach(data.tc7, (value) => {
			int7.push(value);
		});

		_.forEach(data.tc8, (value) => {
			int8.push(value);
		});

		_.forEach(data.tc9, (value) => {
			int9.push(value);
		});

		_.forEach(data.tc10, (value) => {
			int10.push(value);
		});

		_.forEach(data.tc11, (value) => {
			int11.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'tc1' : int1,
			'tc2' : int2,
			'tc3' : int3,
			'tc4' : int4,
			'tc5' : int5,
			'tc6' : int6,
			'tc7' : int7,
			'tc8' : int8,
			'tc9' : int9,
			'tc10' : int10,
			'tc11' : int11,
		};

		get_2g_tcdc_stack_bar_chart(datas, date, type, 'bar', true);
	}
}

Chart.defaults.global.defaultFontColor = 'black';
function get_2g_basic_chart(avg = '', sum = '', labels = '', name, ch1 = '', ch2 = '', legend, max = 0) {
	name = wcl2g === true ? name + '-wcl2g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let  options = {
		stacked: false,
		layout: pad,
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			display: legend
		},
		scales: {
			yAxes: [{
				id: 'A',
				type: 'linear',
				position: 'left',
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}, {
				id: 'B',
				type: 'linear',
				position: 'right',
				ticks: {
					max: max,
					min: 0
				}
			}]
		}
	}

	let datas = {
		datasets: [
			{
				type: 'line',
				fill: false,
				yAxisID: 'B',
				label: name.toUpperCase() +' DEN',
				data: avg,
				backgroundColor: 'rgba(255, 255, 255, 0.9)',
				borderColor: '#892cdc',
				borderWidth: 3,
			},
			{
				fill: false,
				label: name.toUpperCase(),
				data: sum,
				yAxisID: 'A',
				backgroundColor: '#eb596e',
				borderWidth: 3,
				borderColor: '#eb596e',
			},

		],
		labels: labels,
	};
	build_chart(datas, options, ctxBar, 0, ch1, name);
}

function get_2g_single_line_chart(avg = '', labels = '', name, ch1 = '', legend) {
	name = wcl2g === true ? name + '-wcl2g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			display: legend
		},
		scales: {
			yAxes: [{
				ticks: {
					max: 100,
					min: 0,
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	}

	let datas = {
		datasets: [
			{
				fill: false,
				label: 'TCH Availability',
				data: avg,
				backgroundColor: 'rgba(255, 255, 255, 0.9)',
				borderWidth: 3,
				borderColor: '#892cdc',
			},
		],
		labels: labels,
	};
	build_chart(datas, options, ctxBar, 0, ch1, name);
}

function get_2g_multi_line_chart(data = '', labels = '', name, ch1 = '', legend) {
	name = wcl2g === true ? name + '-wcl2g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false,
			callback: function(value, index, values) {
				return String(value).commarize();
			}
		},
		legend: {
			position: 'top',
			display: legend
		},
		layout: pad
	};

	let datas = {
		datasets: [{
			fill: false,
			label: 'TCH Blocking Rate(%)',
			data: data.tbr,
			backgroundColor: 'rgba(255, 255, 255, 0.9)',
			borderWidth: 3,
			borderColor: '#892cdc',
		}, {
			type: 'line',
			fill: false,
			label: 'SBLK',
			data: data.sblk,
			backgroundColor: 'rgba(255, 255, 255, 0.9)',
			borderWidth: 3,
			borderColor: '#eb596e',
		}, {
			type: 'line',
			fill: false,
			label: 'SDCCH Drop(%)',
			data: data.sd,
			backgroundColor: 'rgba(255, 255, 255, 0.9)',
			borderWidth: 3,
			borderColor: '#fa7d09',
		},
		],
		labels: labels
	};
	build_chart(datas, options, ctxBar, 0, 'line', name);
}

function get_2g_int_stack_bar_chart(data = '', labels = '', name, ch1 = '', legend) {
	name = wcl2g === true ? name + '-wcl2g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false,
		},
		legend: {
			position: 'top',
			fontfontSize: 5,
			boxWidth: 10,
			display: legend
		},
		layout: pad,
		scales: {
			xAxes: [{
				stacked: true,
			}],
			yAxes: [{
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	};

	let datas = {
		datasets: [
			{
				label: name.toUpperCase() + " 1",
				backgroundColor: "#892cdc",
				data: data.int1
			},
			{
				label: name.toUpperCase() + " 2",
				backgroundColor: "#eb596e",
				data: data.int2
			},
			{
				label: name.toUpperCase() + " 3",
				backgroundColor: "#ffd31d",
				data: data.int3
			},
			{
				label: name.toUpperCase() + " 4",
				backgroundColor: "#0779e4",
				data: data.int4
			},
			{
				label: name.toUpperCase() + " 5",
				backgroundColor: "#54e346",
				data: data.int5
			},
		],
		labels: labels
	};
	build_chart(datas, options, ctxBar, 0, 'bar', name);
}

function get_2g_sc_stack_bar_chart(data = '', labels = '', name, ch1 = '', legend) {
	name = wcl2g === true ? name + '-wcl2g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			fontfontSize: 5,
			boxWidth: 10,
			display: legend
		},
		layout: pad,
		scales: {
			xAxes: [{
				stacked: true,
			}],
			yAxes: [{
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	};

	let datas = {
		datasets: [
			{
				label: "Call Drops Other",
				backgroundColor: "#892cdc",
				borderWidth: 3,
				borderColor: "#892cdc",
				data: data.int1
			},
			{
				label: "Call Drops Qual",
				backgroundColor: "#eb596e",
				borderWidth: 3,
				borderColor: "#eb596e",
				data: data.int2
			},
			{
				label: "Call Drops Rcv Level",
				backgroundColor: "#ffd31d",
				borderWidth: 3,
				borderColor: "#ffd31d",
				data: data.int3
			},
			{
				label: "Call Drops TA",
				backgroundColor: "#0779e4",
				borderWidth: 3,
				borderColor: "#0779e4",
				data: data.int4
			},
		],
		labels: labels
	};
	build_chart(datas, options, ctxBar, 0, 'bar', name);
}

function get_2g_trf_bar_chart(data = '', labels = '', name, ch1 = '', legend) {
	name = wcl2g === true ? name + '-wcl2g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false,
			callback: function(value, index, values) {
					return String(value).commarize();
			}
		},
		legend: {
			position: 'top',
			fontfontSize: 5,
			boxWidth: 10,
			display: legend
		},
		layout: pad,
	};

	let datas = {
		datasets: [
			{
				label: "TCH Traffic",
				backgroundColor: "#892cdc",
				data: data.trf1
			},
			{
				label: "SDCCH Traffic",
				backgroundColor: "#eb596e",
				data: data.trf2
			}
		],
		labels: labels
	};
	build_chart(datas, options, ctxBar, 0, 'bar', name);
}

function get_2g_tcdc_stack_bar_chart(data = '', labels = '', name, ch1 = '', legend) {
	name = wcl2g === true ? name + '-wcl2g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'left',
			fontfontSize: 5,
			boxWidth: 10,
			display: false
		},
		layout: pad,
		scales: {
			xAxes: [{
				stacked: true
			}],
			yAxes: [{
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	};

	let datas = {
		datasets: [
			{
				label: "TCH Call Drops Other",
				backgroundColor: "#892cdc",
				data: data.tc1
			},
			{
				label: "TCH Call ULDL Fer",
				backgroundColor: "#eb596e",
				data: data.tc2
			},
			{
				label: "TCH Call Drops Rcv Level",
				backgroundColor: "#ffd31d",
				data: data.tc3
			},
			{
				label: "TCH Call Drops TA",
				backgroundColor: "#0779e4",
				data: data.tc4
			},
			{
				label: "TCH Call Drops UL QUAL",
				backgroundColor: "#54e346",
				data: data.tc5
			},
			{
				label: "TCH Call Drops DL RCV Level",
				backgroundColor: "#de7119",
				data: data.tc6
			},
			{
				label: "TCH Call Drops UL FER",
				backgroundColor: "#687466",
				data: data.tc7
			},
			{
				label: "TCH Call Drops UL QUAL",
				backgroundColor: "#7d5e2a",
				data: data.tc8
			},
			{
				label: "TCH Call Drops ULDL Rcv Level",
				backgroundColor: "#fb3640",
				data: data.tc9
			},
			{
				label: "TCH Call Drops DL FER",
				backgroundColor: "#295939",
				data: data.tc10
			},
			{
				label: "TCH Call Drops ULDL QUAL",
				backgroundColor: "#1b262c",
				data: data.tc11
			},
		],
		labels: labels
	};
	build_chart(datas, options, ctxBar, 0, 'bar', name);
}

function get_2g_ta_stack_line_chart(data = '', labels = '', name, ch1 = '') {
	name = wcl2g === true ? name + '-wcl2g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {

		plugins: {
			datalabels: {
				display: false,
			},
			filler: {
				propagate: true
			}
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			fontfontSize: 5,
			boxWidth: 10,
			display: false,
		},

		layout: pad,
		scales: {
			xAxes: [{
				stacked: true
			}],
			yAxes: [{
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	};

	let datas = {
		datasets: [
			{
				fill: true,
				backgroundColor: "#892cdc",
				borderWidth: 3,
				borderColor: "#892cdc",
				data: data.t0
			},
			{
				fill: true,
				backgroundColor: "#eb596e",
				borderWidth: 3,
				borderColor: "#eb596e",
				data: data.t1
			},
			{
				fill: true,
				backgroundColor: "#ffd31d",
				borderWidth: 3,
				borderColor: "#ffd31d",
				data: data.t2
			},
			{
				fill: true,
				backgroundColor: "#0779e4",
				borderWidth: 3,
				borderColor: "#0779e4",
				data: data.t3
			},
			{
				fill: true,
				backgroundColor: "#54e346",
				borderWidth: 3,
				borderColor: "#54e346",
				data: data.t4
			},
			{
				fill: true,
				backgroundColor: "#511281",
				borderWidth: 3,
				borderColor: "#511281",
				data: data.t5
			},
			{
				fill: true,
				backgroundColor: "#393e46",
				borderWidth: 3,
				borderColor: "#393e46",
				data: data.t6
			},
			{
				fill: true,
				backgroundColor: "#f55c47",
				borderWidth: 3,
				borderColor: "#f55c47",
				data: data.t7
			},
			{
				fill: true,
				backgroundColor: "#f21170",
				borderWidth: 3,
				borderColor: "#f21170",
				data: data.t8
			},
			{
				fill: true,
				backgroundColor: "#9ede73",
				borderWidth: 3,
				borderColor: "#9ede73",
				data: data.t9
			},
			{
				fill: true,
				backgroundColor: "#966c3b",
				borderWidth: 3,
				borderColor: "#966c3b",
				data: data.t10
			},
			{
				fill: true,
				backgroundColor: "#383e56",
				borderWidth: 3,
				borderColor: "#383e56",
				data: data.t11
			},
			{
				fill: true,
				backgroundColor: "#161d6f",
				borderWidth: 3,
				borderColor: "#161d6f",
				data: data.t12
			},
			{
				fill: true,
				backgroundColor: "#845ec2",
				borderWidth: 3,
				borderColor: "#845ec2",
				data: data.t13
			},
			{
				fill: true,
				backgroundColor: "#eb5e0b",
				borderWidth: 3,
				borderColor: "#eb5e0b",
				data: data.t14
			},
			{
				fill: true,
				backgroundColor: "#ffe227",
				borderWidth: 3,
				borderColor: "#ffe227",
				data: data.t15
			},
			{
				fill: true,
				backgroundColor: "#9c3d54",
				borderWidth: 3,
				borderColor: "#9c3d54",
				data: data.t16
			},
			{
				fill: true,
				backgroundColor: "#29bb89",
				borderWidth: 3,
				borderColor: "#29bb89",
				data: data.t17
			},
			{
				fill: true,
				backgroundColor: "#966c3b",
				borderWidth: 3,
				borderColor: "#966c3b",
				data: data.t18
			},
			{
				fill: true,
				backgroundColor: "#1c1427",
				borderWidth: 3,
				borderColor: "#1c1427",
				data: data.t19
			},
			{
				fill: true,
				backgroundColor: "#1f441e",
				borderWidth: 3,
				borderColor: "#1f441e",
				data: data.t20
			},
			{
				fill: true,
				backgroundColor: "#ac0d0d",
				borderWidth: 3,
				borderColor: "#ac0d0d",
				data: data.t21
			},
			{
				fill: true,
				backgroundColor: "#ff005c",
				borderWidth: 3,
				borderColor: "#ff005c",
				data: data.t22
			},
			{
				fill: true,
				backgroundColor: "#f48b29",
				borderWidth: 3,
				borderColor: "#f48b29",
				data: data.t23
			},
			{
				fill: true,
				backgroundColor: "#440a67",
				borderWidth: 3,
				borderColor: "#440a67",
				data: data.t24
			},
			{
				fill: true,
				backgroundColor: "#c67ace",
				borderWidth: 3,
				borderColor: "#c67ace",
				data: data.t25
			},
			{
				fill: true,
				backgroundColor: "#2978b5",
				borderWidth: 3,
				borderColor: "#2978b5",
				data: data.t26
			},
			{
				fill: true,
				backgroundColor: "#845460",
				borderWidth: 3,
				borderColor: "#845460",
				data: data.t27
			},
			{
				fill: true,
				backgroundColor: "#132c33",
				borderWidth: 3,
				borderColor: "#132c33",
				data: data.t28
			},
			{
				fill: true,
				backgroundColor: "#72147e",
				borderWidth: 3,
				borderColor: "#72147e",
				data: data.t29
			},
			{
				fill: true,
				backgroundColor: "#c64756",
				borderWidth: 3,
				borderColor: "#c64756",
				data: data.t30TO31
			},
			{
				fill: true,
				backgroundColor: "#864000",
				borderWidth: 3,
				borderColor: "#864000",
				data: data.t32TO33
			},
			{
				fill: true,
				backgroundColor: "#6930c3",
				borderWidth: 3,
				borderColor: "#6930c3",
				data: data.t34TO35
			},
			{
				fill: true,
				backgroundColor: "#1a508b",
				borderWidth: 3,
				borderColor: "#1a508b",
				data: data.t36TO37
			},
			{
				fill: true,
				backgroundColor: "#c0e218",
				borderWidth: 3,
				borderColor: "#c0e218",
				data: data.t38TO39
			},
			{
				fill: true,
				backgroundColor: "#70af85",
				borderWidth: 3,
				borderColor: "#70af85",
				data: data.t40TO44
			},
			{
				fill: true,
				backgroundColor: "#aa8976",
				borderWidth: 3,
				borderColor: "#aa8976",
				data: data.t45TO49
			},
			{
				fill: true,
				backgroundColor: "#0e49b5",
				borderWidth: 3,
				borderColor: "#0e49b5",
				data: data.t50TO54
			},
			{
				fill: true,
				backgroundColor: "#db6400",
				borderWidth: 3,
				borderColor: "#db6400",
				data: data.tGT63
			},

		],
		labels: labels
	};
	build_chart(datas, options, ctxBar, 0, 'line', name);
}
