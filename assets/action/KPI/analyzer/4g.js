let wcl4g = false;
function get_4g_graph(data = []) {
	let chart = $('.chart-loader');
	chart.find('div.c4g-dis').addClass('d-none');
	let param = {
		url: url + 'get_4g_graph',
		dataType: 'json',
		type: 'POST',
		data: data,
		beforeSend: chart.append('<div class="col-12 c4g-load" style="margin-top: 5% !important;">' +
			'<h5 class="text-center text-white">' +
			'<img src='+loaders+' width="120" height="120" alt="loader" class="rounded-circle"><br>\n' +
			'<em class="text-muted small text-process">Loading Chart</em>' +
			'</h5>' +
			'</div>')
	};

	fortress(param).then((data) => {
		chart.find('div.c4g-load').remove();
		chart.find('div.c4g-dis').removeClass('d-none');

		set_4g_basic_chart(data.psdrr, data.date, 'psdrr', 'Call Drop Rate%', 'Call Drop Den');
		set_4g_basic_chart(data.rrssr, data.date, 'rrssr', 'RRC Conn Success Rate%', 'RRC Success  Rate Den');
		set_4g_basic_chart(data.erssr, data.date, 'erssr', 'ERAB setup SR(%)', 'ERAB SETUP SUCC RATE DEN RCM');

		set_4g_2_bar_chart(data.s1, data.date, 's1', 'bar', 'LTE S1 Connectivity NUM', 'LTE S1 Connectivity DENUM', false);
		set_4g_2_bar_chart(data.put, data.date, 'put', 'line', 'LTE S1 Connectivity NUM', 'LTE S1 Connectivity DENUM', false);
		set_4g_2_bar_chart(data.ulit, data.date, 'ulit', 'line', 'NPI_4G UL PRB Utilization Rate%', 'NPI_4G DL PRB Utilization Rate%', false);
		set_4g_2_bar_chart(data.cvu, data.date, 'cvu', 'bar', 'L.Traffic.User.Avg', 'L.Traffic.ActiveUser.Avg', false);
		set_4g_2_bar_chart(data.trv, data.date, 'trv', 'bar', 'DL Traffic Volume (GB)', 'UL Traffic Volume (GB)', false);
		set_4g_2_bar_chart(data.uth, data.date, 'uth', 'line', 'User DL Throughput (Mbps)', 'User UL Throughput (Mbps)', false);
		set_4g_2_bar_chart(data.cth, data.date, 'cth', 'line', 'Cell DL Average Throughput (Mbps)', 'Cell UL Average Throughput (Mbps)', false);

		set_4g_single_line_chart(data.sa, data.date, 'sa');
		set_4g_stack_line_chart(data.tad, data.date, 'tad');

		set_4g_lb_chart(data.csf, data.date, 'csf', true, 'CSFB Respond Success Rate%', 'CSFB Respond Num', 'CSFB Performed Success Rate%', 'CSFB Performed Den');
		set_4g_lb_chart(data.hosr, data.date, 'hosr', false, 'Intra Freq HO Success Rate%', 'Intra Freq HO Den', 'Inter Freq HO Success Rate%', 'Inter Freq HO Den');

		set_4g_rcfr_stack_bar_chart(data.rcfr, data.date, 'rcfr');
		set_4g_erer_stack_bar_chart(data.erer, data.date, 'erer');

		set_4g_erar_stack_bar_chart(data.erar, data.date, 'erar');
	});

	function set_4g_basic_chart(data, vdate, type, $l1, $l2) {
		let line = [];
		let bar = [];
		let date = [];

		_.forEach(data.v1, (value) => {
			line.push(value);
		});

		_.forEach(data.v2, (value) => {
			bar.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		get_4g_basic_chart(line, bar, date, type, 'bar', 'line', true, _.max(line), $l1, $l2);
	}

	function set_4g_2_bar_chart(data, vdate, type, chtype, lb1, lb2, stacked) {
		let t1 = [];
		let t2 = [];
		let date = [];

		_.forEach(data.v1, (value) => {
			t1.push(value);
		});

		_.forEach(data.v2, (value) => {
			t2.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			't1': t1,
			't2': t2,
		};

		get_4g_2_bar_chart(datas, date, type, chtype, lb1, lb2, stacked);
	}

	function set_4g_single_line_chart(data, vdate, name) {
		let avg = [];
		let date = [];

		_.forEach(data, (value) => {
			avg.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		get_4g_single_line_chart(avg, date, name);
	}

	function set_4g_stack_line_chart(data, vdate, type) {
		let ta_0 = [];
		let ta_1 = [];
		let ta_2 = [];
		let ta_3 = [];
		let ta_4 = [];
		let ta_5 = [];
		let ta_6 = [];
		let ta_7 = [];
		let ta_8 = [];
		let ta_9 = [];
		let ta_10 = [];
		let ta_11 = [];
		let date = [];

		_.forEach(data.ta0, (value) => {
			ta_0.push(value);
		});

		_.forEach(data.ta1, (value) => {
			ta_1.push(value);
		});

		_.forEach(data.ta2, (value) => {
			ta_2.push(value);
		});

		_.forEach(data.ta3, (value) => {
			ta_3.push(value);
		});

		_.forEach(data.ta4, (value) => {
			ta_4.push(value);
		});

		_.forEach(data.ta5, (value) => {
			ta_5.push(value);
		});

		_.forEach(data.ta6, (value) => {
			ta_6.push(value);
		});

		_.forEach(data.ta7, (value) => {
			ta_7.push(value);
		});

		_.forEach(data.ta8, (value) => {
			ta_8.push(value);
		});

		_.forEach(data.ta9, (value) => {
			ta_9.push(value);
		});

		_.forEach(data.ta10, (value) => {
			ta_10.push(value);
		});

		_.forEach(data.ta11, (value) => {
			ta_11.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			't0': ta_0,
			't1': ta_1,
			't2': ta_2,
			't3': ta_3,
			't4': ta_4,
			't5': ta_5,
			't6': ta_6,
			't7': ta_7,
			't8': ta_8,
			't9': ta_9,
			't10': ta_10,
			't11': ta_11
		}

		get_4g_ta_stack_line_chart(datas, date, type, 'line');
	}

	function set_4g_lb_chart(data, vdate, type, stacked, lb1, lb2, lb3, lb4) {
		let l1 = [];
		let l2 = [];
		let b1 = [];
		let b2 = [];
		let date = [];

		_.forEach(data.l1, (value) => {
			l1.push(value);
		});

		_.forEach(data.l2, (value) => {
			l2.push(value);
		});

		_.forEach(data.b1, (value) => {
			b1.push(value);
		});

		_.forEach(data.b2, (value) => {
			b2.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'b1': b1,
			'b2': b2,
		};

		let line = {
			'l1': l1,
			'l2': l2
		}

		let lbl = {
			'lb1': lb1,
			'lb2': lb2,
			'lb3': lb3,
			'lb4': lb4
		}

		get_4g_lb_chart(line, datas, date, type, 'bar', 'line', _.max(l1) + _.max(l2), stacked, lbl);
	}

	function set_4g_rcfr_stack_bar_chart(data, vdate, type) {
		let int1 = [];
		let int2 = [];
		let int3 = [];
		let int4 = [];
		let int5 = [];
		let date = [];

		_.forEach(data.v1, (value) => {
			int1.push(value);
		});

		_.forEach(data.v2, (value) => {
			int2.push(value);
		});

		_.forEach(data.v3, (value) => {
			int3.push(value);
		});

		_.forEach(data.v4, (value) => {
			int4.push(value);
		});

		_.forEach(data.v5, (value) => {
			int5.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'tp1' : int1,
			'tp2' : int2,
			'tp3' : int3,
			'tp4' : int4,
			'tp5' : int5,
		};

		get_4g_rcfr_chart(datas, date, type, 'bar', true);
	}

	function set_4g_erer_stack_bar_chart(data, vdate, type) {
		let int1 = [];
		let int2 = [];
		let int3 = [];
		let int4 = [];
		let int5 = [];
		let int6 = [];
		let date = [];

		_.forEach(data.v1, (value) => {
			int1.push(value);
		});

		_.forEach(data.v2, (value) => {
			int2.push(value);
		});

		_.forEach(data.v3, (value) => {
			int3.push(value);
		});

		_.forEach(data.v4, (value) => {
			int4.push(value);
		});

		_.forEach(data.v5, (value) => {
			int5.push(value);
		});

		_.forEach(data.v6, (value) => {
			int6.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'tp1' : int1,
			'tp2' : int2,
			'tp3' : int3,
			'tp4' : int4,
			'tp5' : int5,
			'tp6' : int6
		};

		get_4g_erer_chart(datas, date, type, 'bar', true);
	}

	function set_4g_erar_stack_bar_chart(data, vdate, type) {
		let	l1 = [];
		let b1 = [];
		let b2 = [];
		let b3 = [];
		let b4 = [];
		let b5 = [];
		let b6 = [];
		let b7 = [];
		let date = [];

		_.forEach(data.l1, (value) => {
			l1.push(value);
		});

		_.forEach(data.b1, (value) => {
			b1.push(value);
		});

		_.forEach(data.b2, (value) => {
			b2.push(value);
		});

		_.forEach(data.b3, (value) => {
			b3.push(value);
		});

		_.forEach(data.b4, (value) => {
			b4.push(value);
		});

		_.forEach(data.b5, (value) => {
			b5.push(value);
		});

		_.forEach(data.b6, (value) => {
			b6.push(value);
		});

		_.forEach(data.b7, (value) => {
			b7.push(value);
		});

		_.forEach(vdate, (val) => {
			date.push(val.date);
		});

		let datas = {
			'l1' : l1,
			'b1' : b1,
			'b2' : b2,
			'b3' : b3,
			'b4' : b4,
			'b5' : b5,
			'b6' : b6,
			'b7' : b7
		};

		get_4g_erar_chart(l1, datas, date, type, 'bar', 'line', _.max(l1));
	}
}

function get_4g_basic_chart(avg = '', sum = '', labels = '', name, ch1 = '', ch2 = '', legend, max = 0, $l1, $l2) {
	name = wcl4g === true ? name + '-wcl4g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let  options = {
		stacked: false,
		layout: pad,
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			display: legend
		},
		scales: {
			yAxes: [{
				id: 'A',
				type: 'linear',
				position: 'left',
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}, {
				id: 'B',
				type: 'linear',
				position: 'right',
				ticks: {
					max: max,
					min: 0
				}
			}]
		}
	}

	let datas = {
		datasets: [
			{
				type: 'line',
				fill: false,
				yAxisID: 'B',
				label: $l1,
				data: avg,
				backgroundColor: 'rgba(255, 255, 255, 0.9)',
				borderWidth: 3,
				borderColor: '#fb3640',
			},
			{
				fill: false,
				label: $l2,
				data: sum,
				yAxisID: 'A',
				backgroundColor: '#0e49b5',
				borderWidth: 3,
				borderColor: '#0e49b5',
			},

		],
		labels: labels,
	};

	build_chart(datas, options, ctxBar, 0, ch1, name);
}

function get_4g_2_bar_chart(data = '', labels = '', name, ch1 = '', lb1 = '', lb2 = '', stacked = false) {
	name = wcl4g === true ? name + '-wcl4g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			fontfontSize: 5,
			boxWidth: 10,
			display: true
		},
		layout: pad,
		scales: {
			xAxes: [{
				stacked: stacked
			}],
			yAxes: [{
				stacked: stacked,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	};

	let datas = {
		datasets: [
			{
				fill: false,
				label: lb1,
				backgroundColor: ch1 === 'line' ? 'rgba(255, 255, 255, 0.9)' : '#fb3640',
				borderWidth: 3,
				borderColor: '#fb3640',
				data: data.t1
			},
			{
				fill: false,
				label: lb2,
				backgroundColor: ch1 === 'line' ? 'rgba(255, 255, 255, 0.9)' : '#0e49b5',
				borderWidth: 3,
				borderColor: '#0e49b5',
				data: data.t2
			}
		],
		labels: labels
	};

	build_chart(datas, options, ctxBar, 0, ch1, name);
}

function get_4g_single_line_chart(data = '', labels = '', name) {
	name = wcl4g === true ? name + '-wcl4g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			fontfontSize: 5,
			boxWidth: 10,
			display: true
		},
		layout: pad,
		scales: {
			xAxes: [{
				stacked: false
			}],
			yAxes: [{
				stacked: false,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	};

	let datas = {
		datasets: [
			{
				fill: false,
				label: 'Site Availability%',
				backgroundColor: 'rgba(255, 255, 255, 0.9)',
				borderWidth: 3,
				borderColor: '#fb3640',
				data: data
			}
		],
		labels: labels
	};

	build_chart(datas, options, ctxBar, 0, 'line', name);
}

function get_4g_ta_stack_line_chart(data = '', labels = '', name) {
	name = wcl4g === true ? name + '-wcl4g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
			filler: {
				propagate: true
			}
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			fontfontSize: 5,
			boxWidth: 10,
			display: false
		},
		layout: pad,
		scales: {
			xAxes: [{
				stacked: true
			}],
			yAxes: [{
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	};

	let datas = {
		datasets: [
			{
				fill: true,
				backgroundColor: "#f55c47",
				borderWidth: 3,
				borderColor: "#f55c47",
				data: data.t0
			},
			{
				fill: true,
				backgroundColor: "#4aa96c",
				borderWidth: 3,
				borderColor: "#4aa96c",
				data: data.t1
			},
			{
				fill: true,
				backgroundColor: "#ffd31d",
				borderWidth: 3,
				borderColor: "#ffd31d",
				data: data.t2
			},
			{
				fill: true,
				backgroundColor: "#0779e4",
				borderWidth: 3,
				borderColor: "#0779e4",
				data: data.t3
			},
			{
				fill: true,
				backgroundColor: "#54e346",
				borderWidth: 3,
				borderColor: "#54e346",
				data: data.t4
			},
			{
				fill: true,
				backgroundColor: "#511281",
				borderWidth: 3,
				borderColor: "#511281",
				data: data.t5
			},
			{
				fill: true,
				backgroundColor: "#393e46",
				borderWidth: 3,
				borderColor: "#393e46",
				data: data.t6
			},
			{
				fill: true,
				backgroundColor: "#f55c47",
				borderWidth: 3,
				borderColor: "#f55c47",
				data: data.t7
			},
			{
				fill: true,
				backgroundColor: "#f21170",
				borderWidth: 3,
				borderColor: "#f21170",
				data: data.t8
			},
			{
				fill: true,
				backgroundColor: "#9ede73",
				borderWidth: 3,
				borderColor: "#9ede73",
				data: data.t9
			},
			{
				fill: true,
				backgroundColor: "#966c3b",
				borderWidth: 3,
				borderColor: "#966c3b",
				data: data.t10
			},
			{
				fill: true,
				backgroundColor: "#383e56",
				borderWidth: 3,
				borderColor: "#383e56",
				data: data.t11
			}
		],
		labels: labels
	};

	build_chart(datas, options, ctxBar, 0, 'line', name);
}

function get_4g_lb_chart(line = '', data = '', labels = '', name, ch1 = '', ch2 = '', max = 0, stacked = false, lbl) {
	name = wcl4g === true ? name + '-wcl4g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let  options = {
		stacked: false,
		layout: pad,
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			display: false
		},
		scales: {
			xAxes: [{
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}],
			yAxes: [{
				id: 'A',
				type: 'linear',
				position: 'left',
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}, {
				id: 'B',
				type: 'linear',
				position: 'right',
				stacked: false,
				ticks: {
					max: max,
					min: 0
				},
			}],
		},
	}

	let datas = {
		datasets: [
			{
				type: 'line',
				fill: false,
				label: lbl.lb3,
				data: line.l2,
				yAxisID: 'B',
				backgroundColor: 'rgba(255, 255, 255, 0.9)',
				borderWidth: 3,
				borderColor: '#0e49b5',
			},
			{
				type: 'line',
				fill: false,
				yAxisID: 'B',
				label: lbl.lb1,
				data: line.l1,
				backgroundColor: 'rgba(255, 255, 255, 0.9)',
				borderColor: '#fb3640',
				borderWidth: 3,
			},
			{
				label: lbl.lb2,
				data: data.b1,
				yAxisID: 'A',
				backgroundColor: '#28a745',
				borderWidth: 3,
				borderColor: '#28a745',
			},
			{
				label : lbl.lb4,
				data: data.b2,
				yAxisID: 'A',
				backgroundColor: '#ffd31d',
				borderWidth: 3,
				borderColor: '#ffd31d'
			}
		],
		labels: labels,
	};

	build_chart(datas, options, ctxBar, 0, ch1, name);
}

function get_4g_rcfr_chart(data = '', labels = '', name){
	name = wcl4g === true ? name + '-wcl4g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			fontfontSize: 5,
			boxWidth: 10,
			display: false
		},
		layout: pad,
		scales: {
			xAxes: [{
				stacked: true
			}],
			yAxes: [{
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	};

	let datas = {
		datasets: [
			{
				label: 'L.RRC.SetupFail.NoReply',
				backgroundColor: '#fb3640',
				data: data.tp1
			},
			{
				label: 'L.RRC.SetupFail.Rej',
				backgroundColor: '#fb743e',
				data: data.tp2
			},
			{
				label : 'L.RRC.SetupFail.ResFail',
				backgroundColor: "#ffd31d",
				data: data.tp3
			},
			{
				label : 'L.RRC.SetupFail.ResFail.PUCCH',
				backgroundColor: "#0779e4",
				data: data.tp4
			},
			{
				label : 'L.RRC.SetupFail.ResFail.SRS',
				backgroundColor: "#54e346",
				data: data.tp5
			},
		],
		labels: labels
	};

	build_chart(datas, options, ctxBar, 0, 'bar', name);
}

function get_4g_erer_chart(data = '', labels = '', name){
	name = wcl4g === true ? name + '-wcl4g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let options = {
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			fontfontSize: 5,
			boxWidth: 10,
			display: false
		},
		layout: pad,
		scales: {
			xAxes: [{
				stacked: true
			}],
			yAxes: [{
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}]
		}
	};

	let datas = {
		datasets: [
			{
				label: 'L.E-RAB.FailEst.MME',
				backgroundColor: '#fb3640',
				data: data.tp1
			},
			{
				label: 'L.E-RAB.FailEst.NoRadioRes',
				backgroundColor: '#fb743e',
				data: data.tp2
			},
			{
				label: 'L.E-RAB.FailEst.NoReply',
				backgroundColor: '#ffd31d',
				data: data.tp3
			},
			{
				label : 'L.E-RAB.FailEst.RNL',
				backgroundColor: "#0779e4",
				data: data.tp4
			},
			{
				label : 'L.E-RAB.FailEst.SecurModeFail',
				backgroundColor: "#54e346",
				data: data.tp5
			},
			{
				label : 'L.E-RAB.FailEst.TNL',
				backgroundColor: "#892cdc",
				data: data.tp6
			},
		],
		labels: labels
	};

	build_chart(datas, options, ctxBar, 0, 'bar', name);
}

function get_4g_erar_chart(line = '', data = '', labels = '', name, ch1 = '', ch2 = '', max = 0) {
	name = wcl4g === true ? name + '-wcl4g' : name;
	let canvasBar = document.getElementById(name+"_chart");
	let ctxBar = canvasBar.getContext('2d');

	let  options = {
		stacked: false,
		layout: pad,
		plugins: {
			datalabels: {
				display: false,
			},
		},
		ticks: {
			beginAtZero: false,
			display: false
		},
		legend: {
			position: 'top',
			display: false
		},
		scales: {
			xAxes: [{
				stacked: true
			}],
			yAxes: [{
				id: 'A',
				type: 'linear',
				position: 'left',
				stacked: true,
				ticks: {
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				}
			}, {
				id: 'B',
				type: 'linear',
				position: 'right',
				stacked: true,
				ticks: {
					max: max,
					min: 0,
					callback: function(value, index, values) {
						return String(value).commarize();
					}
				},
			}],
		},
	}

	let datas = {
		datasets: [
			{
				type: 'line',
				fill: false,
				yAxisID: 'B',
				label: 'L.E-RAB.AbnormRel',
				data: line,
				backgroundColor: 'rgba(255, 255, 255, 0.9)',
				borderColor: '#fb3640',
				borderWidth: 3,
			},
			{
				fill: false,
				label: 'L.E-RAB.AbnormRel.Cong',
				data: data.b1,
				yAxisID: 'A',
				backgroundColor: '#0e49b5',
				borderWidth: 3,
				borderColor: '#0e49b5',
			},
			{
				label: 'L.E-RAB.AbnormRel.HOFailure',
				data: data.b2,
				yAxisID: 'A',
				backgroundColor: '#eb596e',
				borderWidth: 3,
				borderColor: '#eb596e',
			},
			{
				label : 'L.E-RAB.AbnormRel.MME',
				data: data.b3,
				yAxisID: 'A',
				backgroundColor: '#ffd31d',
				borderWidth: 3,
				borderColor: '#ffd31d'
			},
			{
				label : 'L.E-RAB.AbnormRel.Radio',
				data: data.b4,
				yAxisID: 'A',
				backgroundColor: '#28a745',
				borderWidth: 3,
				borderColor: '#28a745'
			},
			{
				label : 'L.E-RAB.AbnormRel.TNL',
				data: data.b5,
				yAxisID: 'A',
				backgroundColor: '#892cdc',
				borderWidth: 3,
				borderColor: '#892cdc'
			},
			{
				label : 'L.E-RAB.AbnormRel.Radio.UuNoReply',
				data: data.b6,
				yAxisID: 'A',
				backgroundColor: '#966c3b',
				borderWidth: 3,
				borderColor: '#966c3b'
			},
			{
				label : 'L.E-RAB.AbnormRel.CAUser',
				data: data.b7,
				yAxisID: 'A',
				backgroundColor: '#16c79a',
				borderWidth: 3,
				borderColor: '#16c79a'
			}

		],
		labels: labels,
	};

	build_chart(datas, options, ctxBar, 0, ch1, name);
}
