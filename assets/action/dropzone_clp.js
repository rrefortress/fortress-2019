Dropzone.autoDiscover = false;

let clp = new Dropzone(".dropzone",{
	url: url + "CLP/clp_upload",
	maxFiles: 1,
	method:"POST",
	dataType: 'json',
	acceptedFiles:".csv",
	paramName:"clp_file",
	dictInvalidFileType:"Type file not allowed",
	addRemoveLinks:true,
	init: function() {
		this.on("success", function(file, response) {
			data = jQuery.parseJSON(response);
			if (data.type === 'error') {
				SWAL(data.type, data.title, data.msg);
			} else {
				$('.export_clp, .clear_clp').prop('disabled', false);
				get_crfs(data);
			}
		})
	}
});

clp.on("queuecomplete", function () {
	this.removeAllFiles();
	$("#bulk-upload-clp").modal('hide');
});

clp.on("success", function(){
	$(".dz-success-mark svg").css("background", "#79d70f").css('border-radius', '30px');
	$(".dz-error-mark").css("display", "none");
});

clp.on("error", function() {
	$(".dz-error-mark svg").css("background", "#ec524b").css('border-radius', '30px');
	$(".dz-success-mark").css("display", "none");
});
