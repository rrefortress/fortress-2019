// let socket = io.connect(hostname+port);
// socket.on('logs', function(data) {
// 	let html = '<li class="list-group-item list-padding">\n' +
// 		'<div class="row">' +
// 		'<div class="col-2 text-center">' +
// 		'<img src='+data.profile+' height="73" width="73" class=" rounded-circle rounded-sm shadow" alt='+data.name+'>'+
// 		'</div>'+
// 		'<div class="col-10">' +
// 		'<div class="d-flex w-100 justify-content-between">\n' +
// 		'<small class="mb-0">'+data.name+'</small>\n' +
// 		'<small class="text-muted">'+data.ago+'</small>\n' +
// 		'</div>\n' +
// 		'<h6 class="mb-1">'+data.action+'</h6>\n' +
// 		'<em class="small text-muted">'+data.department+' - '+data.geoarea+'</em>\n' +
// 		'</div>'+
// 		'</div>' +
// 		'</li>';
// 	$('.logs-lists').prepend(html);
// });
//
// socket.on('active_users', function(data) {
// 	$('.total-active').text(data.active);
// });
//
// socket.on('activities', function(datas) {
// 	let html = '';
// 	let ctr = 1, count = 0;
// 	let total = 0;
// 	let data = datas.active.data;
// 	if (data.length > 0) {
// 		for (i in data) {
// 			for (o in data[i].names) {
// 				let counts = data[i].names[o].subject.length;
// 				let more = parseInt(counts - 1) === 0 ? '' : ' +' + parseInt(counts - 1) + ' more <i class="fas fa-calendar-plus"></i>';
//
// 				let day = data[i].names[o].subject[0].day;
// 				let month = data[i].names[o].subject[0].month;
//
// 				let subs = "";
// 				let num = 1;
// 				for (a = 0; a < data[i].names[o].subject.length; a++) {
// 					subs += num + '.' + data[i].names[o].subject[a].subject_name + '\n';
// 					num++;
// 					total++;
// 				}
//
// 				if (ctr <= 4) {
// 					html +=
// 						'<div class="card mb-2">' +
// 						'<div class="row no-gutters">' +
// 						'<div class="col-md-4 bg-warning text-center">' +
// 						'<div class="card-body">' +
// 						'<p class="card-text">' +
// 						'<b>'+day+'</b><br>' +
// 						'<small class="text-bold">'+month+'</small>' +
// 						'</p>' +
// 						'</div>' +
// 						'</div>' +
// 						'<div class="col-md-8">' +
// 						'<p class="card-text container text-truncate mg-top-10">' +
// 						'<b data-toggle="tooltip" data-placement="top" title="' + data[i].names[o].subject[0].subject_name + '">'+data[i].names[o].subject[0].subject_name+'</b><br>' +
// 						'<small class="text-muted">'+data[i].names[o].subject[0].name+'</small><br>';
// 					if (parseInt(counts - 1) > 0) {
// 						html += '<small class="text-muted" data-toggle="tooltip" data-placement="top" title="' + subs + '">'+ more +'</small>';
// 					}
// 					html += '</p>' +
// 						'</div>' +
// 						'</div>' +
// 						'</div>';
// 					count++;
// 				}
// 				ctr++;
// 			}
// 		}
// 		let ctrs = data.length - count;
// 		if (ctrs > 0) {
// 			html += '<div class="card">' +
// 				'<b class="card-text text-center"> +' +  ctrs +  ' more' + '</b>' +
// 				'</div>';
// 		}
// 	} else {
// 		html = '<div class="empty mg-top-135">' +
// 			'<h5 class="text-center text-white">' +
// 			'<span class="far fa-calendar-alt display-3 text-dark"></span><br>' +
// 			'<h5 class="text-dark text-center">No events.</h5>' +
// 			'<label class="text-muted small text-center ">No list has been displayed yet.</label>' +
// 			'</h5>' +
// 			'</div>';
// 	}
// 	$('.surveys').text(data.length);
// 	$('.total_rings').text(total);
// 	$('.act-list').html('').html(html);
// 	$('[data-toggle="tooltip"]').tooltip();
// });
//
