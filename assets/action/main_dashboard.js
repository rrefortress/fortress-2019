let main_cards = [
	{
		'id'         : 1,
		'name'       : 'RAN',
		'derived'    : 'RRE',
		'value' 	 : '0',
		'icon'       : 'fas fa-brain',
		'url'        : url + 'sections',
		'color'      : 'bg-warning',
		'sub_content' : [
			{
				name : 'Events',
				value: 0,
			},
			{
				name : 'Permanent',
				value: 0,
			},
			{
				name : 'Temporary',
				value: 0,
			},
		],
	},
	{
		'id'         : 2,
		'name'       : 'FACILITIES',
		'derived'    : 'RFE',
		'value' 	 : '0',
		'icon'       : 'fas fa-building',
		'url'        : url + 'sections',
		'color'      : 'bg-danger'
	},
	{
		'id'         : 3,
		'name'       : 'WIRELINE',
		'derived'    : 'RWE',
		'value' 	 : '0',
		'icon'       : 'fas fa-network-wired',
		'url'        : url + 'sections',
		'color'      : 'bg-success'
	},
	{
		'id'         : 4,
		'name'       : 'RCNE',
		'derived'    : 'RCNE',
		'value' 	 : '0',
		'icon'       : 'fas fa-boxes',
		'url'        : url + 'sections',
		'color'      : 'u-teal'
	},
	{
		'id'         : 5,
		'name'       : 'RGPM',
		'derived'    : 'RGPM',
		'value' 	 : '0',
		'icon'       : 'fas fa-ethernet',
		'url'        : url + 'sections',
		'color'      : 'bg-primary'

	}
];

let rre_tech_data = [
	{
		'name'       : 'Solution',
		'text'       : '',
		'value' 	 : '0',
		'color'      : 'bg-warning'
	},
	{
		'name'       : '2G',
		'text'       : '',
		'value' 	 : '0',
		'color'      : 'bg-warning'
	},
	{
		'name'       : '3G',
		'text'       : '',
		'value' 	 : '0',
		'color'      : 'bg-warning'
	},
	{
		'name'       : '4G',
		'text'       : 'Huawei',
		'value' 	 : '0',
		'color'      : 'bg-warning'
	}
];

let card_charts = [
	{
		id : 2,
		name: 'RFE',
	},
	{
		id : 3,
		name: 'RWE',
	},
	{
		id : 4,
		name: 'RCNE',
	},
	{
		id : 5,
		name: 'RGPM',
	},
];

$(function () {
	set_main_cards();
	set_card_charts();
	get_main_counts();
	set_rre_techs();
	set_chart_rfe_projects();
	set_chart_rwe_projects();
	set_chart_rcne_projects();
	set_chart_rgpm_projects();
});

function get_main_counts() {
	let param = {
		url: url + 'main_dash_counts',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then( (data) => {
		set_main_cards(data);
		set_rre_techs(data.rre);
		set_chart_rfe_projects(data.rfe);
		set_chart_rwe_projects(data.rwe);
		set_chart_rcne_projects(data.rcne);
		set_chart_rgpm_projects(data.rgpm);
		$('.counter').countUp();
	});
}

function set_main_cards(data) {
	let html = '';
	let sector = $('#sector').val();
	for (i in main_cards) {
		let url = main_cards[i].derived === sector || sector === 'Sub-Admin' ? main_cards[i].url : '#';
		let value = 0;
		if (data !== undefined) {
			switch (main_cards[i].name) {
				case "RAN": value = data.counts.rre; break;
				case "FACILITIES" : value = data.counts.rfe; break;
				case "RGPM": value = data.counts.rgpm; break;
				case "WIRELINE": value = data.counts.rwe; break;
				case "RCNE": value = data.counts.rcne; break;
			}
		}

		html +=
			'<div class="card border shadow">' +
				'<div class="card-body p-3 '+main_cards[i].color+'">' +
					'<div class="row">' +
						'<div class="col-8">'+
							'<h5 class="card-title text-white counter">'+value+'</h5>' +
							'<em class="card-subtitle text-white">in total Sites</em>' +

						'</div>' +
						'<div class="col-4">' +
							'<i class="text-white f-2_5 '+main_cards[i].icon+'"></i>' +
						'</div>' +
					'</div>' +
				'</div>' +

				'<div class="card-footer text-center bg-transparent border-top border-secondary">'+
					'<a href='+ url+' class="text-muted">'+main_cards[i].name+'</a>'+
			   '</div>' +
			'</div>';
	}

	$('#main_cards').html(html);
}

function set_rre_techs(data) {
	let html = '';
	let value = 0;

	let g9 = 0;
	let g18 = 0;
	let l7 = 0;
	let l18 = 0;
	let l23 = 0;
	let l26 = 0;

	let event = 0;
	let per = 0;
	let temp = 0;

	for (i in rre_tech_data) {
		if (data !== undefined) {
			switch (rre_tech_data[i].name) {
				case "Solution" :
					event = data.solution.events;
					per = data.solution.permanent;
					temp = data.solution.temporary;
					break;
				case "2G" :
					value = data.two_g.physical;
					g9 = data.two_g.g9;
					g18 = data.two_g.g18;
					break;
				case "3G" :
					value = data.three_g.physical;
					g9 = data.three_g.u9;
					g18 = data.three_g.u21;
					break;
				case "4G" :
					value = data.four_g.physical;
					l7 = data.four_g.l7;
					l18 = data.four_g.l18;
					l23 = data.four_g.l23;
					l26 = data.four_g.l26;
					break;
			}
		}

		let lbl9 = rre_tech_data[i].name === '2G' ? 'G9' : 'U9';
		let lbl18 = rre_tech_data[i].name === '2G' ? 'G18' : 'U21';

		html += '<div class="text-center">\n' +
			'<div class="card border">\n' +
					'<div class="card-body p-2">\n' +
						'<p class="u-yellow mb-1 shadow-sm text-white teal">'+rre_tech_data[i].name+'</p>' +
						'<div class="col-12">' +
							'<div class="row pad-00">';
								if (rre_tech_data[i].name === 'Solution') {
									html += '<div class="col-4 pad-00 small teal">Events <br/> <b class="counter">'+ event +'</b></div>' +
										'<div class="col-4 pad-00 small">Permanent <br/> <b class="counter">'+ per +'</b></div>' +
										'<div class="col-4 pad-00 small">Temporary  <br/> <b class="counter">'+ temp +'</b></div>';
								} else if (rre_tech_data[i].name === '4G') {
									html += '<div class="col-6 pad-00 border-right small">L7:  <b class="counter">'+ l7 +'</b></div>' +
										'<div class="col-6 pad-00 small">L18: <b class="counter">'+ l18 +'</b></div>' +
										'<div class="col-6 pad-00 border-right small">L23:  <b class="counter">'+ l23 +'</b></div>' +
										'<div class="col-6 pad-00 small">L26: <b class="counter">'+ l26 +'</b></div>';
								} else {
									html += '<div class="col-6 pad-00 border-right small">'+lbl9+':  <b class="counter">'+ g9 +'</b></div>' +
									'<div class="col-6 pad-00 small">'+lbl18+': <b class="counter">'+ g18 +'</b></div>';
								}
							html += '</div>' +
						'</div>' +
					'</div>\n' +

				'</div>\n' +
			'</div>';
	}

	$('.rre_techs').html(html);
}

function set_card_charts() {
	let html = '';
	for (i in card_charts) {
		html += '<div class="col-6 border">\n' +
				'  <div class="card-body p-1">\n' +
				'    <canvas id="'+card_charts[i].name+'_chart"></canvas>' +
				'  </div>\n' +
				'</div>';
	}
	$('.card-charts').html(html);
}

function set_chart_rfe_projects(data) {
	let canvasBar = document.getElementById("RFE_chart");
	let ctxBar = canvasBar.getContext('2d');

	let gf = 0;
	let ibs = 0;
	let rooftop = 0;
	let sm = 0;
	let swat = 0;
	let rfe = 100;

	if (data !== undefined) {
		gf = data.gf;
		ibs = data.ibs;
		rooftop = data.rooftop;
		sm = data.sm;
		swat = data.swat;
		rfe = data.rfe;
	}

	let total_per = getWholePercent(gf, rfe) +
		getWholePercent(ibs, rfe) +
		getWholePercent(rooftop, rfe) +
		getWholePercent(sm, rfe) +
		getWholePercent(swat, rfe);

	let datas = {
		labels: [gf +": Greenfield", ibs +": IBS", rooftop+ ": Rooftop", sm +": Small Cell", swat+": SWAT"],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#28a745', '#dc3545', '#448AFF', '#f6d743', '#6a197d'],
				data: [
					getWholePercent(gf, rfe),
					getWholePercent(ibs, rfe),
					getWholePercent(rooftop, rfe),
					getWholePercent(sm, rfe),
					getWholePercent(swat, rfe),
				],
			}
		]
	};

	let optionsPie = {
		title    : setTitle("RFE"),
		plugins  : setPlugins(),
		elements : centerText("Site Type"),
		rotation : -0.7 * Math.PI,legend: {
			position: 'left',
		},
		layout: {
			padding: {
				left: 5,
				right: 5,
				top: 5,
				bottom: 5
			}
		}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', 'pieRFE');
}

function set_chart_rwe_projects(data) {
	let canvasBar = document.getElementById("RWE_chart");
	let ctxBar = canvasBar.getContext('2d');

	let ASDL = 0;
	let VSDL = 0;
	let GPON = 0;
	let rwe = 100;

	if (data !== undefined) {
		ASDL = data.ASDL;
		VSDL = data.VSDL;
		GPON = data.GPON;
		rwe = data.rwe;
	}

	let total_per = getWholePercent(ASDL, rwe) +
		getWholePercent(VSDL, rwe) +
		getWholePercent(GPON, rwe);

	let datas = {
		labels: [ASDL+ ": ADSL", VSDL+": VDSL", GPON+": GPON"],
		datasets: [
			{
				label: "Projects",
				fill: true,
				backgroundColor: ['#28a745', '#dc3545', '#448AFF'],
				data: [
					getWholePercent(ASDL, rwe),
					getWholePercent(VSDL, rwe),
					getWholePercent(GPON, rwe),
				],
			}
		]
	};

	let optionsPie = {
		title    : setTitle("WIRELINE"),
		plugins  : setPlugins(),
		elements : centerText("Techonolgy"),
		rotation : -0.7 * Math.PI,
		legend: {
			position: 'left',
		},
		layout: {
			padding: {
				left: 5,
				right: 5,
				top: 5,
				bottom: 5
			}
		}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', 'pieRWE');
}

function set_chart_rcne_projects(data) {
	let canvasBar = document.getElementById("RCNE_chart");
	let ctxBar = canvasBar.getContext('2d');

	let milo = 0;
	let nap = 0;
	let rcne = 100;

	if (data !== undefined) {
		milo = data.milo;
		nap = data.nap;
		rcne = data.rcne;
	}

	let total_per = getWholePercent(milo, rcne) +
		getWholePercent(nap, rcne);

	let datas = {
		labels: [milo+ ": MILO", nap+ ": NAP Augmentation"],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#00897b', '#ffc107'],
				data: [
					getWholePercent(milo, rcne),
					getWholePercent(nap, rcne)
				],
			}
		]
	};

	let optionsPie = {
		title    : setTitle("RCNE"),
		plugins  : setPlugins(),
		elements : centerText("Cable"),
		rotation : -0.7 * Math.PI,
		legend: {
			position: 'left',
		},
		layout: {
			padding: {
				left: 5,
				right: 5,
				top: 5,
				bottom: 5
			}
		}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', 'pieRCNE');
}

function set_chart_rgpm_projects(data) {
	let canvasBar = document.getElementById("RGPM_chart");
	let ctxBar = canvasBar.getContext('2d');

	let crfs = 0;
	let trfs = 0;
	let rgpm = 100;

	if (data !== undefined) {
		crfs = data.crfs;
		trfs = data.trfs;
		rgpm = data.rgpm;
	}

	let total_per = getWholePercent(crfs, rgpm) +
		getWholePercent(trfs, rgpm);

	let datas = {
		labels: [trfs+": TRFS", crfs+": CRFS"],
		datasets: [
			{
				label: "",
				fill: true,
				backgroundColor: ['#448AFF', '#dc3545'],
				data: [
					getWholePercent(trfs, rgpm),
					getWholePercent(crfs, rgpm)
				],
			}
		]
	};

	let optionsPie = {
		title    : setTitle("RGPM"),
		plugins  : setPlugins(),
		elements : centerText("TRFS VS CRFS"),
		rotation : -0.7 * Math.PI,
		legend: {
			position: 'left',
		},
		layout: {
			padding: {
				left: 5,
				right: 5,
				top: 5,
				bottom: 5
			}
		}
	};

	build_chart(datas, optionsPie, ctxBar, total_per, 'doughnut', 'pieRGPM');
}

