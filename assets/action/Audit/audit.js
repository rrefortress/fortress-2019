let id = '';
let plaid = '';
let sname = '';
let sectors = '';
let height = '';
let tech = '';
let azi = '';
let mtilt = '';
let etilt = '';
let antmodel = '';
let db = '';
let region = '';
let tmdb = '';

$(document).on('click', '.audit-site', function (e) {
	e.preventDefault();
	$('#plaid').text(plaid);
	$('#sitename').text(sname);
	$('#sectors').text(sectors);
	$('#azimuth').text(azi);
	$('#height').text(height);
	$('#tech').text(tech);
	$('#antmodel').text(antmodel);
	$('#mtilt').text(mtilt);
	$('#etilt').text(etilt);
	$('#dualbeam').text(db);

	set_img(sectors);

	$('#audit-profile').modal('show');
	$('.lazy').lazy();
});

function set_img(sectors) {
	let html = '';
	let num = 1;
	_.range(sectors).forEach(() => {
		html += '<div class="col-3">'+
						'<img class="img-fluid mw-100 border lazy shadow img-'+num+'" alt="Sector 1">'+
						'<label class="mt-2 shadow small lead border border-secondary p-2 w-100 rounded-pill text-dark">Sector '+num+'</label>' + 
				'</div>';
		num++;
    });
	$('.aud-img').html(html);

	if (sectors == 1) {
		$('.img-2, .img-3, .img-4').addClass('d-none');
		$('.img-1').attr('src',url + 'assets/audit/' + region + '/' + sname + '/' + tmdb + '/Sector.jpg');
	} else if (sectors == 2) {
		$('.img-2').removeClass('d-none');
		$('.img-3, .img-4').addClass('d-none');
		$('.img-1').attr('src',url + 'assets/audit/' + region + '/' + sname + '/' + tmdb + '/Sector.jpg');
		$('.img-2').attr('src',url + 'assets/audit/' + region + '/' + sname + '/' + tmdb + '/Sector1.jpg');
	} else if (sectors == 3) {
		$('.img-2, .img-3').removeClass('d-none');
		$('.img-4').addClass('d-none');
		$('.img-1').attr('src',url + 'assets/audit/' + region + '/' + sname + '/' + tmdb + '/Sector.jpg');
		$('.img-2').attr('src',url + 'assets/audit/' + region + '/' + sname + '/' + tmdb + '/Sector1.jpg');
		$('.img-3').attr('src',url + 'assets/audit/' + region + '/' + sname + '/' + tmdb + '/Sector2.jpg');
	} else if (sectors == 4) {
		$('.img-2, .img-3, .img-4').removeClass('d-none');
		$('.img-1').attr('src',url + 'assets/audit/' + region + '/' + sname + '/' + tmdb + '/Sector.jpg');
		$('.img-2').attr('src',url + 'assets/audit/' + region + '/' + sname + '/' + tmdb + '/Sector1.jpg');
		$('.img-3').attr('src',url + 'assets/audit/' + region + '/' + sname + '/' + tmdb + '/Sector2.jpg');
		$('.img-4').attr('src',url + 'assets/audit/' + region + '/' + sname + '/' + tmdb + '/Sector3.jpg');
	}
}


$(document).ready(function () {
    get_audit();
});

function get_audit() {
    let param = {
		url: url + 'Audit/get_audit',
		dataType: 'json',
		type: 'GET',
	};
	fortress(param).then( (data) => {
		let html = '<table class="shadow-sm table table-bordered display nowrap table-sm small tbl-audit table-hover">' +
            '<thead class="global_color small text-white text-center">'+
                '<tr>'+
                    '<th scope="col" style="width:3%">#</th>'+
                    '<th scope="col">PLAID</th>'+
                    '<th scope="col">SITENAME</th>'+
					'<th scope="col">HEIGHT(M)</th>'+
                    '<th scope="col">TECHNOLOGY</th>'+
					'<th scope="col">SECTOR</th>'+
                    '<th scope="col">AZIMUTH(°)</th>'+
                    '<th scope="col">M-TILT</th>'+
					'<th scope="col">E-TILT</th>'+
                    '<th scope="col">ANTENNA MODEL</th>'+
                    '<th scope="col">DUALBEAM</th>'+
                    '<th scope="col">ADDED/REVISED</th>'+
                '</tr>' +
            '</thead>';
			let num = 1;
			html += '<tbody class="text-center small">';
			_.forEach(data, function(val, key) { 
				region = val.region;
				html += '<tr>' +
							'<td style="text-align: center" row_id='+val.id+' data-tmdb="'+val.tech_dir+'">'+num+++'.</td>' +
							'<td style="text-align: center">'+val.plaid+'</td>' +
							'<td style="text-align: center">'+val.sitename+'</td>' +
							'<td style="text-align: center">'+val.height+'</td>' +
							'<td style="text-align: center">'+val.tech+'</td>' +
							'<td style="text-align: center">'+val.sectors+'</td>' +
							'<td style="text-align: center">'+val.azimuth+'</td>' +
							'<td style="text-align: center">'+val.mtilt+'</td>' +
							'<td style="text-align: center">'+val.etilt+'</td>' +
							'<td style="text-align: center">'+val.antenna+'</td>' +
							'<td style="text-align: center">'+val.dualbeam+'</td>' +
							'<td style="text-align: center">'+val.date_updated+'</td>';
						'<tr>';
			});

			html += '</tbody>' +
			'</table>';
			
			$('#maindiv').html(html);
			$('.tbl-audit').DataTable({select: true});
			$(".tbl-audit th").addClass('global_color text-white text-uppercase text-center');
			
	});
}

$(document).on('click', '.tbl-audit tbody tr', function (e) {
	e.preventDefault();
	if ($(this).hasClass('selected')) {
		$(this).removeClass('selected');
		$('.audit-site, .del-site').attr('disabled', 'disabled');
	} else {
		$('.selected').removeClass('selected');
		$(this).toggleClass('selected');
		$('.audit-site, .del-site').removeAttr("disabled");

		// id = $(this).closest("tr").find('td:eq(0)').data('id');
		id = $(this).closest("tr").find('td:eq(0)').attr('row_id');
		tmdb = $(this).closest("tr").find('td:eq(0)').data('tmdb');
		console.log(tmdb);
		plaid = $(this).closest("tr").find('td:eq(1)').text();
		sname = $(this).closest("tr").find('td:eq(2)').text();
		height = $(this).closest("tr").find('td:eq(3)').text();
		tech = $(this).closest("tr").find('td:eq(4)').text();
		sectors = $(this).closest("tr").find('td:eq(5)').text();
		azi = $(this).closest("tr").find('td:eq(6)').text();
		mtilt = $(this).closest("tr").find('td:eq(7)').text();
		etilt = $(this).closest("tr").find('td:eq(8)').text();
		antmodel = $(this).closest("tr").find('td:eq(9)').text();
		db = $(this).closest("tr").find('td:eq(10)').text();
	}
});

$(document).on("click", ".export_audit", function (e) {
	e.preventDefault();
	$(this).prop('disabled', true);

	let param = {
		url: url + 'export_audit',
		dataType: 'json',
		beforeSend: $(this).html('<em>Exporting <i class="fas fa-spinner fa-spin text-muted"></i></em>')
	};

	fortress(param).then((data) => {
		let today = moment().format("DD/MM/YYYY");
		if (data.result.length > 0) {
			JSONToCSVConvertor(data.result, "Sites_" + data.geoarea + "(" + today + ")", true);
		} else {
			SWAL('error', "Invalid Data!", 'Empty data, no need to export.');
		}
		$(this).html('<i class="fas fa-download"></i> EXPORT').prop('disabled', false);
	});
});

$(document).on("click", ".del_audit", function (e) {
	e.preventDefault();

	Swal.fire({
		title: 'Are you sure?',
		text: "You want to delete this Site",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	}).then((result) => {
		if (result.value) {
			console.log(1);
			let param = {
				url: url + 'del_audit',
				dataType: 'json',
				data:{id:ids, tech:tech},
				type: 'get'
			};
		
			fortress(param).then((data) => {
				SWAL(data.type, data.title, data.msg);
			});
			// $.ajax({
			// 	url: url + 'del_items',
			// 	type: "post",
			// 	data:{id:ids},
			// 	dataType: 'json',
			// 	success: function (response) {
			// 		get_items();
			// 		SWAL('success', "SUCCESS!", "account successfully deleted.");
			// 		clearItems();
			// 	},
			// 	error: function(jqXHR, textStatus, errorThrown) {
			// 		errorSwal();
			// 		clearItems()
			// 	}
			// });
		}
	});

	
});