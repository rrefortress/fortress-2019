
$(document).ready(function(){

	var region;
	var btscid=[];
	var btscidcount=0;
	var nodebcidcount=0;
	var nodebcid=[];
	var psc=[];
	var nodebcidplacement=[];
	var abc=["A","B","C","D","E","F"];
	var ghi=["G","H","I","J","K","L"];
	var g18cn = ["4","5","0"];
	var multiplier;
	var keycount;
	var siteid;
	var sectorcount;
	var carriercount;
	var sectorcountflag;
	var btscidflag=0;
	var nodebcidflag=0;
	var carriername = "";
	var alldata=[];
	var carrierarray = [];
	var azimuth;
	var g900_config;
	var g1800_config;
	var u900_config;
	var u2100_config;
	var u2100_configcheck;
	var cidcountminus;
	var randkey2g;
	var randkey3g;
	var randkeypsc;
	var sitetypeval='';
	var sitetypeannex='';
	var solutiontypeval='';
	var currentsolutiontypeval = '';
	var longlat=[];
	var coords=[];
	var usebrgypsgcflag=0;
	var brgypsgc;
	$('#loadspinner').hide();

	templatechange();
	siteannex();
	generatecid('2g',1);
	generatecid('3g',1);
	generatecid('psc',1);


	var templati;
	var templongi;
	var tempbrgy;
    var temppsgc;
    var temptown;
    var temppro;
    var tempreg;

  //   var data = [
  //   ['sample', 'sample', 'sample', 'sample', 'sample', 'sample', 'sample'],
  //   ['sample', 'sample', 'sample', 'sample', 'sample', 'sample', 'sample'],
  //   ];
     
  // jexcel(document.getElementById('subdiv'), {
  //       contextMenu: false,
  //       data:data,
  //       columns: [
  //           { type: 'text'},
  //           { type: 'text'},
  //           { type: 'text'},
  //           { type: 'text'},
  //           { type: 'text'},
  //           { type: 'text'},
  //           { type: 'text'}
  //        ]
  //   });

	$('#coordstextarea').on("focusin", function(){
		$('#brgydetails').hide('fast');
		$('#coordstextarea').parent().animate({ height: '100%' });
	});

	$('#coordstextarea').on("blur", function(){
        $('#coordstextarea').parent().animate({ height: '50px' });
        $('#brgydetails').show('slow');
		// $('#brgydetails').show('slow');
	});

	$('#latlongloadbtn').on("click", function(){
		$('#coordstextarea').parent().animate({ height: '50px' });
        $('#brgydetails').show('slow');
		getcoords();
	});
    $('#loadcoordsbtn').on("click", function(){
    // var nextElement = $('#coordinatesOption > option:selected').next('option');
      // if (nextElement.length > 0) {
        locateCoords();
        // $("#downcoord").trigger( "click" );
        // }
        // else{
        // locateCoords();
        // }
        
    });
    // $(document).on('change','#coordinatesOption',function(){
    //     locateCoords();
    // });
    $('#clearbtn').on("click", function(){
        $('#coordstextarea').val('');
    });
    $("#downcoord").click(function() {
      var nextElement = $('#coordinatesOption > option:selected').next('option');
      if (nextElement.length > 0) {
        $('#coordinatesOption > option:selected').removeAttr('selected').next('option').attr('selected', 'selected');
        // $("#loadcoordsbtn").trigger( "click" );
        }
        // locateCoords();
    });
    $("#upcoord").click(function() {
      var nextElement = $('#coordinatesOption > option:selected').prev('option');
      if (nextElement.length > 0) {
        $('#coordinatesOption > option:selected').removeAttr('selected').prev('option').attr('selected', 'selected');
        }
        // locateCoords();
    });
    $(document).ajaxSend(function() {
        // console.log("ajaxLoad");
        $('#loadspinner').show();
    });
    $(document).ajaxStop(function() {
        setTimeout(function() {$('#loadspinner').hide();},1000);
        // console.log("ajaxDone");
    });

	function getcoords()
	{
		coords=[];
		var coordstemp=[];
		const regex = /-?[\d\.]+/g;
		const str = $('#coordstextarea').val();
		let m;
        $('#coordinatesOption').empty();
		if(str!="")
		{
			while ((m = regex.exec(str)) !== null)
			{
				// This is necessary to avoid infinite loops with zero-width matches
				if (m.index === regex.lastIndex)
				{
					regex.lastIndex++;
				}
				coordstemp.push(m[0]);
			}
			$.each(coordstemp, function(index, value)
            {
				if(index%2==0)
				{
                    if(coordstemp[index]<coordstemp[index+1])
                    {
                        coords.push({"lat": coordstemp[index], "lng": coordstemp[index+1]});
                    }else
                    {
                        coords.push({"lat": coordstemp[index+1], "lng": coordstemp[index]});
                    }
                    // console.log(coords.length);
				}
			});
            $.each(coords, function(index, value)
            {
                $('#coordinatesOption').append('<option value="'+coords[index]['lat']+', '+coords[index]['lng']+' ">['+(index+1)+'] '+coords[index]['lat']+', '+coords[index]['lng']+' </option>');
            });
			// console.log(coords);
            $("#totalCoordinates").text(coords.length);
		}
        // console.log(coords);
	}

    function locateCoords()
    {   
        var activeCoord = $('#coordinatesOption').val();
        longlat=[];
        const regex = /[0-9.]+([0-9]+)?/gm;
        let m;
        var lati;
        var longi;
        var temp;
        var geojson;
        
        if(coords.length>0)
        {
            if(activeCoord!="")
            {
                while ((m = regex.exec(activeCoord)) !== null) {
                    // This is necessary to avoid infinite loops with zero-width matches
                    if (m.index === regex.lastIndex) {
                        regex.lastIndex++;
                    }
                    // console.log(m[0]);
                    longlat.push(m[0]);
                }
                // console.log(longlat);
                longlat.sort(function(a, b){return a-b});
                // console.log(longlat);
                lati = longlat[0];
                longi = longlat[1];
            }
             $('#loadspinner').show();
            $.ajax({
                type: 'POST',
                url: url+'locateCoord',
                dataType: 'json',
                async: false,
                data:{
                    'longitude':longi,
                    'latitude': lati},
                success: function(data){
                    if(data.Bgy_name!=null)
                    {
                        // geojson=JSON.parse(data.geojson);
                        console.log(" "+data.Bgy_psgc+" ");
                        temppsgc = data.Bgy_psgc;
                        tempbrgy=data.Bgy_name;
						temptown=data.Town_name;
						temppro=data.Pro_name;
						tempreg=data.Reg_name;
						$('#temptable').append(`<tr><td>`+lati+`</td><td>`+longi+`</td><td>`+temppsgc+`</td><td>`+tempbrgy+`</td><td>`+temptown+`</td><td>`+temppro+`</td><td>`+tempreg+`</td></tr>`);
                        // console.log(data);
                        // console.log(JSON.stringify(geojson));
                        // console.log(22);


                        // $("#coordaddress").text(data.Bgy_name+', '+data.Town_name+', '+data.Pro_name);
                        // $("#coordpsgc").text(data.Bgy_psgc);
                        // $('#addprovince_sitename').val(data.Pro_name);
                        // $('#addtown_sitename').val(data.Town_name);
                        // $('#addbarangay_sitename').val(data.Bgy_name);


                        // sitenamesuggest();


                        // $('#towncode').text(data.Town_annex);
                        // $('#provincecode').text(data.Pro_annex);
                    }
                    else
                    {
                        $("#coordpsgc").text(data.Bgy_psgc+' not found on Annex');
                        geojson=JSON.parse(data.geojson);
                    }
                },
               error: function(XMLHttpRequest, textStatus, errorThrown) {
                    // console.log();
               }
            });
            // console.log(44);
            // mapcurrent(lati,longi,geojson);
            // sitenamesuggest();
            // $('#coordinatesOption > option:selected').removeAttr('selected').next('option').attr('selected', 'selected');
        }
    }
	// $('#checkazimuth').on("click", function(){
	// 	//console.log(generateallcells());
	// 	generateallcells();
	// });
	// $('#sectoradjust').on("click",function(){
	// 	templatechange();
	// });
	// $('.carrier').on("click",function(){
	// 	carriercount = $(this).val();
	// 	carriername = $(this).attr('id');
	// 	carriername= carriername.substring(0, carriername.length - 1);
	// 	//console.log(carriername);
	// 	templatechange();
	// });
	// $('#towertype').on("change", function(){
	// 	if(currentsolutiontypeval===permanent)
	// 	{
	// 		sitetypeannex = $("#towertype").val();
	// 	}
	// 	sitenamesuggest();
	// });
	// $(document).on('change','.sitetype,.solutiontype',function(){
	// 	// $('.sitetype').on("change", function(){
	// 	siteannex();
	// });
	// $('.solutiontype').on("change", function(){
	//     siteannex();
	// });
	function siteannex()
	{
		sitetypeannex = '';
		var radios = document.getElementsByName('solutiontyperadio');
		radios.forEach(function (item, index)
		{
			if(item.checked)
			{
				solutiontypeval = $(item).attr('id');
			}
		});
		radios = document.getElementsByName('sitetyperadio');
		radios.forEach(function (item, index)
		{
			if(item.checked)
			{
				sitetypeval = $(item).attr('id');
			}
		});
		switch(sitetypeval)
		{
			case 'indoor':
				sitetypeval = 'Indoor';
				break;
			case 'outdoor':
				sitetypeval = 'Outdoor';
				break;
			case 'indooroutdoor':
				sitetypeval = 'Indoor-Outdoor';
				break;
			case 'smallcell':
				sitetypeval = 'Small Cell';
				break;
		}
		if(currentsolutiontypeval!==solutiontypeval || solutiontypeval==='permanent')
		{   
            // console.log('im in');
			switch(solutiontypeval)
			{
				case 'temporary':
					$('#addplaid_sitename').prop('disabled', true);
					$('#towertype').empty();
					$('#towertype').append(`
                        <option selected value disabled>--Temporary/Test Sites--</option>
                        <option value="SPCOW">Special Event COW</option>
                        <option value="SPLEGO">Special Event LEGO</option>
                        <option value="LEGO">LEGO</option>
                        <option value="TST">Test Site</option>
                        <option disabled>--Small Cells--</option>
                        <option value="FW">Flexi Zone Mini Macro (NOKIA)</option>
                        <option value="AS">Air Scale Micro RRH (NOKIA)</option>
                        <option value="AC">Atom Cell (HUAWEI)</option>
                        <option value="LS">Lamp Site (HUAWEI)</option>
                        <option value="EM">Easy Macro (HUAWEI)</option>
                        <option value="PC">Pico Cell (HUAWEI)</option>
                        <option value="PNP">Plug N' Play (HUAWEI)</option>
                        <option value="BBR">Book RRU (HUAWEI)</option>`);
					break;
				case 'permanent':
					$('#addplaid_sitename').prop('disabled', false);
					switch(sitetypeval)
					{
						case 'Indoor':
							// sitetypeval = 'Indoor';
							sitetypeannex = 'ID';
							$('#towertype').empty();
							$('#towertype').append(`
                                <option value="IBS">Indoor Bldg. Solution</option>`);
							break;
						case 'Outdoor':
							// sitetypeval = 'Outdoor';
							sitetypeannex = '';
							$('#towertype').empty();
							$('#towertype').append(`
                                <option selected value disabled>--Select Tower Type--</option>
                                <option value="4-legged SST">4-legged SST</option>
                                <option value="3-legged SST">3-legged SST</option>
                                <option value="Guyed Monopole">Guyed Monopole</option>
                                <option value="SWAT">SWAT Site</option>
                                <option value="Swesite">Swesite</option>
                                <option disabled>--Rooftop Sites--</option>
                                <option value="Distributed Monopole">Distributed Monopole</option>
                                <option value="Mini SST">Mini SST</option>
                                <option value="SWAT">SWAT Site</option>
                                <option value="Tripod">Tripod</option>
                                <option value="Bipod">Bipod</option>
                                <option disabled>--Small Cells--</option>
                                <option value="FW">Flexi Zone Mini Macro (NOKIA)</option>
                                <option value="AS">Air Scale Micro RRH (NOKIA)</option>
                                <option value="AC">Atom Cell (HUAWEI)</option>
                                <option value="LS">Lamp Site (HUAWEI)</option>
                                <option value="EM">Easy Macro (HUAWEI)</option>
                                <option value="PC">Pico Cell (HUAWEI)</option>
                                <option value="PNP">Plug N' Play (HUAWEI)</option>
                                <option value="BBR">Book RRU (HUAWEI)</option>`);
							break;
						case 'Indoor-Outdoor':
							// sitetypeval = 'Indoor-Outdoor';
							sitetypeannex = 'IO';
							$('#towertype').empty();
							$('#towertype').append(`
                                <option selected value disabled>--Select Tower Type--</option>
                                <option value="4-legged SST">4-legged SST</option>
                                <option value="3-legged SST">3-legged SST</option>
                                <option value="Guyed Monopole">Guyed Monopole</option>
                                <option value="SWAT">SWAT Site</option>
                                <option value="Swesite">Swesite</option>
                                <option disabled>--Rooftop Sites--</option>
                                <option value="Distributed Monopole">Distributed Monopole</option>
                                <option value="Mini SST">Mini SST</option>
                                <option value="SWAT">SWAT Site</option>
                                <option value="Tripod">Tripod</option>
                                <option value="Bipod">Bipod</option>`);
							break;
						case 'Small Cell':
							// sitetypeval = 'Small Cell';
							sitetypeannex = $("#towertype").val();
							$('#towertype').empty();
							$('#towertype').append(`
                                <option disabled>--Small Cells--</option>
                                <option value="FW">Flexi Zone Mini Macro (NOKIA)</option>
                                <option value="AS">Air Scale Micro RRH (NOKIA)</option>
                                <option value="AC">Atom Cell (HUAWEI)</option>
                                <option value="LS">Lamp Site (HUAWEI)</option>
                                <option value="EM">Easy Macro (HUAWEI)</option>
                                <option value="PC">Pico Cell (HUAWEI)</option>
                                <option value="PNP">Plug N' Play (HUAWEI)</option>
                                <option value="BBR">Book RRU (HUAWEI)</option>`);
							break;
					}
					break;
			}
		}
		sitenamesuggest();
		currentsolutiontypeval = solutiontypeval;
		console.log(currentsolutiontypeval);
	}
	$('#addlatlong_sitename').on("blur", function(){
		longlat=[];
		const regex = /[0-9.]+([0-9]+)?/gm;
		const str = $(this).val();
		let m;

		if(str!="")
		{
			while ((m = regex.exec(str)) !== null) {
				// This is necessary to avoid infinite loops with zero-width matches
				if (m.index === regex.lastIndex) {
					regex.lastIndex++;
				}
				// console.log(m[0]);
				longlat.push(m[0]);
			}
			// console.log(longlat);
			longlat.sort(function(a, b){return a-b});
			// console.log(longlat);
			const lati = longlat[0];
			const longi = longlat[1];
			if(longi!=undefined && lati!=undefined)
			{
				$(this).val(lati+" , "+longi);
			}else
			{
				longlat[0]="";
				longlat[1]="";
				$(this).val("");
			}
		}
	});

	function templatechange(){

		var azimuth = getazimuth();
		azimuth=strtoarr(azimuth);
		// var azimuth = $("#azimuth").val();
		var c=0,a=0, b=0, d=0;
		alldata=[];
		nodebcidplacement=[];
		multiplier =0;
		var carrieradjust = carriercount;
		var x=0;
		var config=carriercount;
		var sectoradjust = $("#sectoradjust").val();

		carrierarray["g9"] = $("#g900_configx").val();
		carrierarray["g18"] = $("#g1800_configx").val();
		carrierarray["u9"] = $("#u900_configx").val();
		carrierarray["u21"] = $("#u2100_configx").val();
		//console.log('sectoradjust:'+sectoradjust+'   sectorcount:'+sectorcount);
		if( sectorcount>=7) multiplier=3;
		else if(sectorcount>=4) multiplier=2;
		else multiplier=1;

		if(carrierarray["u21"]>=3) a=2;
		else if(carrierarray["u21"]>=1) a=1;
		if(carrierarray["u9"]>=2) b=2;
		else if(carrierarray["u9"]>=1) b=1;

		switch(a)
		{
			case 2:
				switch(b)
				{
					case 2:
						c=3;
						nodebcidplacement=[1,1,1];
						break;
					case 1:
					case 0:
						c=2;
						nodebcidplacement=[1,1,0];
						break;
				}
				break;
			case 1:
				switch(b)
				{
					case 2:
						c=2;
						nodebcidplacement=[1,0,1];
						break;
					case 1:
					case 0:
						c=1;
						nodebcidplacement=[1,0,0];
						break;
				}
				break;
			case 0:
				switch(b)
				{
					case 2:
						c=2;
						nodebcidplacement=[1,0,1];
						break;
					case 1:
						c=1;
						nodebcidplacement=[1,0,0];
						break;
					case 0:
						c=0;
						nodebcidplacement=[0,0,0];
						break;
				}
				break;
		}

		//number of cell cell id for 3g keycount
		keycount=c*multiplier;

		if((parseInt(carrierarray["g9"]))<=0 && (parseInt(carrierarray["g18"])<=0)) d=0;
		else d=1;
		//console.log('nodebcidplacement: '+nodebcidplacement);
		// console.log(typeof parseInt(carrierarray["g9"]));
		//console.log('a:'+a+' b:'+b+' c:'+  c+' d:'+  d);

		//number of cell id for 2g
		btscidcount=d*multiplier;
		//console.log('multiplier:'+ multiplier+' keycount: '+ keycount+' btscidcount: '+ btscidcount);

		if(parseInt(sectoradjust)===parseInt(sectorcount)){
			sectorcountflag=0;
		}else{
			sectorcountflag=1;
		}

		if(carriername=="")
		{
			//console.log('carriername blank');
			for(var key in carrierarray)
			{
				//carrierarray[key] = carrierarray[key].replace(/[^0-9]/g,'');
				//carrierarray[key]=carrierarray[key].charAt(0);

				config=carrierarray[key];
				if(config=='0'){
					$("#"+key+"00_config").val('');
				}else
				{
					alldata.push([key,carrierarray[key],sectorcount]);
					for(x=0;x<sectoradjust-1;x++)
					{
						config+="+"+carrierarray[key];
					}
					$("#"+key+'00_config').val(config);
					x=0;

					config=azimuth[x];
					// console.log(azimuth);
					for(x=0;x<sectoradjust-1;x++)
					{
						if(azimuth[x+1]==undefined||azimuth[x+1]=='')
						{
							if(azimuth[x]>=360)
							{
								azimuth[x]=0;
							}
							if((azimuth[x]+60) >359)
							{
								//azimuth[x] = (360-azimuth[x]);
								azimuth[x+1] = 60-(360-azimuth[x]);
								console.log('>360: '+azimuth[x]+' >  '+azimuth[x+1]);
							}
							else
							{
								azimuth[x+1] = azimuth[x]+60;
								// console.log(azimuth[x]+' >  '+azimuth[x+1]);
							}
						}
						config+="/"+azimuth[x+1];
					}
					$("#azimuth").val(config);
					$('#azimuthcount').empty();
					for(var key in azimuth){
						// $('#azimuthcount').append(''+(parseInt(key)+1)+'-'+azimuth[key]+'° ');
						$('#azimuthcount').append(+azimuth[key]+'° ');
					}
					// console.log('xxx: '+config);
				}
				//console.log(config);
			}
		}
		else
		{
			if(config=='0'){
				$("#"+carriername).val('');
			}else
			{
				for(x=0;x<sectoradjust-1;x++)
				{
					config+="+"+carrieradjust;
				}
				$("#"+carriername).val(config);
			}
		}
		//console.log('carriername: '+carriername);
		carriername = "";
		carrierarray = [];
		// console.log(alldata);

	}
	$('#generatebtsid').on('click',function(){
		generatecid('2g',1);
	});
	$('#generatenodebid').on('click',function(){
		generatecid('3g',1);
	});
	$('#generatepsc').on('click',function(){
		generatecid('psc',1);
	});
	$('#azimuth').change(function(){
		templatechange();
	});

	function generatedbid(tech,lastid){

		var x;
		if(lastid==0)
		{
			$.ajax({
				type: 'POST',
				url: url+'getlastdbid',
				dataType: 'json',
				async: false,
				data:{'table':tech},
				success: function(id){
					if(tech=='tower')
					{
						id=id.siteid;
					}else{
						id=id.dbcellid;
					}
					x=id;
				}
			});
		}else
		{
			x=lastid;
		}
		switch(tech) {
			case 'tower':
				x=x.substring(2);   //remove "vs"
				x=(parseInt(x))+1; //parse to int, increment
				x= x.toString(); //toString
				x=x.padStart(5, '0'); //pad with "0" to 5digits
				x="vs"+(x);  //add "vs" then return
				break;
			case '2g':
				x=x.substring(3);   //remove "v2c"
				x=(parseInt(x))+1; //parse to int, increment
				x= x.toString(); //toString
				x=x.padStart(5, '0'); //pad with "0" to 5digits
				x="v2c"+(x);  //add "vs" then return
				break;
			case '3g':
				x=x.substring(3);   //remove "v2c"
				x=(parseInt(x))+1; //parse to int, increment
				x= x.toString(); //toString
				x=x.padStart(5, '0'); //pad with "0" to 5digits
				x="v3c"+(x);  //add "vs" then return
				break;
			// case '4g':
			//     break;
			default:
				break;
		}
		// console.log(x);
		return x;
	}

	function checksitenameduplicate(){
		var x;
		var sitename = $("#addsuggested_sitename").val();
		$.ajax({
			type: 'POST',
			url: url+'checkduplicate',
			dataType: 'json',
			async: false,
			data:{"sitename":sitename},
			success: function(data){
				x= data;
			}
		});
		return x;
	}

	// function refreshid(tech){
	//     switch(tech) {
	//         case '2g':
	//             var cid = generatecid(tech);
	//             $('#btsidlabel').empty();
	//             $('#btsidlabel').append(cid+",");
	//             btscid=cid;
	//             break;
	//         case '3g':
	//             var cid = generatecid(tech);
	//             $('#nodebidlabel').empty();
	//             $('#nodebidlabel').append(cid+", ");
	//             nodebcid=cid;
	//             break;
	//         case 'psc':
	//             var pscode = generatecid(tech);
	//             $('#psclabel').empty();
	//             $('#psclabel').append(pscode+", ");
	//             psc=pscode;
	//             break;
	//         default:
	//             break;
	//     }
	// }

	function strtoarr(arr){
		var regex=/\d+\.\d+|\.\d+|\d+/g;
		results = [];
		while(n = regex.exec(arr)) {
			results.push(parseFloat(n[0]));
		}
		//console.log(results);
		return results;
	}

	function generatecid(tech,getnewflag){
		// getnewflag=parseInt(getnewflag);
		// // console.log('getnewflag: '+getnewflag);
		// var cid2g;
		// var cid3g;
		// var i = 0;
		// var j = 0;
		// var randkey;
		// var cid=[];
		// var pscode=[];``
		// var longitude = longlat[0];
		// var latitude = longlat[1];
		// var radius = $("#radius").val();
		// switch(tech) {
		// 	case '2g':
		// 	case '3g':
		// 	{
		// 		$.ajax({
		// 			type: 'POST',
		// 			url: url+'generatecellid/'+tech,
		// 			dataType: 'json',
		// 			async: false,
		// 			success: function(data){
		// 				var keys = Object.keys(data);
		// 				if(tech=='3g'){
		// 					if(getnewflag==1){
		// 						randkey=keys[Math.floor(Math.random() * keys.length)];
		// 						randkey3g=randkey;
		// 					}else randkey=randkey3g;
		// 					//get ids using randkey generated
		// 					while(i<keys.length-keycount)
		// 					{
		// 						if(data[i]!==data[randkey]){
		// 							i++;
		// 						}else {break;}

		// 					}
		// 					while(j<keycount){
		// 						cid[j] = data[keys[i]];
		// 						i++;
		// 						j++;
		// 					}
		// 					i=0;
		// 					j=0;
		// 					$('#nodebidlabel').empty();
		// 					$('#nodebidlabel').append(cid.join('/'));
		// 					nodebcid=cid;
		// 				}else if(tech=='2g')
		// 				{
		// 					if(getnewflag==1){
		// 						randkey=keys[Math.floor(Math.random() * keys.length)];
		// 						randkey2g=randkey;
		// 					}else randkey=randkey2g;
		// 					//get ids using randkey generated, 10 IDs
		// 					while(i<keys.length-btscidcount)
		// 					{
		// 						if(data[i]!==data[randkey]){
		// 							i++;
		// 						}else {break;}

		// 					}

		// 					while(j<btscidcount){
		// 						cid[j] = data[keys[i]];
		// 						i++;
		// 						j++;
		// 					}
		// 					i=0;
		// 					j=0;
		// 					$('#btsidlabel').empty();
		// 					$('#btsidlabel').append(cid.join('/'));
		// 					btscid=cid;
		// 				}
		// 			}
		// 		});
		// 	}
		// 		//               console.log(cid);
		// 		return cid;
		// 		break;
		// 	case 'psc':
		// 		$.ajax({
		// 			type: 'POST',
		// 			url: url+'generatepsc',
		// 			data:
		// 				{
		// 					"sitetype"  :   sitetypeval,
		// 					"longitude"  :  longitude,
		// 					"latitude"  :   latitude,
		// 					"radius"  :   radius
		// 				},
		// 			dataType: 'json',
		// 			async: false,
		// 			success: function(data){
		// 				var keys = Object.keys(data);
		// 				if(getnewflag==1){
		// 					randkey=keys[Math.floor(Math.random() * keys.length)];
		// 					randkeypsc=randkey;
		// 				}else randkey=randkeypsc;
		// 				while(i<keys.length-sectorcount)
		// 				{
		// 					if(data[i]!==data[randkey]){
		// 						i++;
		// 					}else {break;}

		// 				}
		// 				while(j<sectorcount){
		// 					pscode[j] = data[keys[i]];
		// 					i++;
		// 					j++;
		// 				}
		// 				i=0;
		// 				j=0;
		// 			}
		// 		});
		// 		$('#psclabel').empty()
		// 		$('#psclabel').append(pscode.join('/'));
		// 		psc=pscode;
		// 		return pscode;
		// 		break;s
		// 	default:
		// 		break;
		// }
		// getnewflag=0;
	}

	// $.ajax({
	// 	type: 'POST',
	// 	url: url+'loadannex/province',
	// 	dataType: 'json',
	// 	async: false,
	// 	success: function(data){
	// 		var datas=[];
	// 		$.each(data, function(index, value) {
	// 			datas.push(value);
	// 		});
	// 		datas.sort();
	// 		$.each(datas, function(index, value) {
	// 			$('#addprovince_datalists').append('<option value="'+value+'">'+value+'</option>');
	// 		});
	// 	}
	// });

	var lastselectedprovince="";
	$("#addprovince_sitename").change(function(){
		var province = $("#addprovince_sitename").val();
		$.ajax({
			type: 'POST',
			url: url+'loadannex/town',
			data:
				{
					"province"  :   province,
				},
			dataType: 'json',
			async: false,
			success: function(data){
				var datas=[];
				$.each(data, function(index, value) {
					datas.push(value);
				});
				datas.sort();
				$('#addtown_sitename').empty();
				$.each(datas, function(index, value) {
					$('#addtown_datalists').append('<option value="'+value+'">'+value+'</option>');
				});
				if(lastselectedprovince !== province){
					$('#addtown_sitename').val('');
					$('#addbarangay_sitename').val('');
					$("#brgypsgc").val('');
					lastselectedprovince=province;
				}
				// $('#addtown_sitename').val(datas[0]);
			}
		});
		sitenamesuggest();
	});

	var lastselectedtown="";
	$(document).on('change','#addtown_sitename,#addprovince_sitename',function(){
		// alert();
		var province = $("#addprovince_sitename").val();
		var town = $("#addtown_sitename").val();
		town =town.toUpperCase();
		$.ajax({
			type: 'POST',
			url: url+'loadannex/barangay',
			data:
				{
					"province"  :   province,
					"town"  :   town
				},
			dataType: 'json',
			async: false,
			success: function(data){
				var datas=[];
				$.each(data, function(index, value) {
					datas.push(value);
				});
				datas.sort();
				$('#addbarangay_sitename').empty();
				$.each(datas, function(index, value) {
					$('#addbarangay_datalists').append('<option value="'+value+'">'+value+'</option>');
				});
				if(lastselectedtown !== town){
					$('#addbarangay_sitename').val('');
					$("#brgypsgc").val('');
					lastselectedprovince=province;
				}
			}
		});
		sitenamesuggest();
	});

	$("#addtown_sitename,#addbarangay_sitename,#sitetype").change(function(){
		inputaddressmethod()
		sitenamesuggest();
	});
	$("#addstreet_sitename,#addbldg_sitename").keyup(function(){
		sitenamesuggest();
	});

	$('#usebrgypsgc').on('click',function(){
		inputaddressmethod();
		sitenamesuggest();
	});
	$('#brgypsgc').on('blur',function(){

		var brgypsgc = $("#brgypsgc").val();
		$.ajax({
			type: 'POST',
			url: url+'loadannex/brgypsgc',
			data:
				{
					"brgypsgc"  :   brgypsgc,
				},
			dataType: 'json',
			async: false,
			success: function(data){
				if(data!=null || data!=undefined)
				{
					$('.annexaddress').empty();
					$('#addprovince_sitename').val(data.province);
					$('#addtown_sitename').val(data.town);
					$('#addbarangay_sitename').val(data.barangay);
					// $('#addprovince_sitename').append('<option value="'+data.province+'">'+data.province+'</option>');
					// $('#addtown_sitename').append('<option value="'+data.town+'">'+data.town+'</option>');
					// $('#addbarangay_sitename').append('<option value="'+data.barangay+'">'+data.barangay+'</option>');
				}
			}
		});
		// sitenamesuggest();
	});
	function inputaddressmethod()
	{
	// 	if($('#usebrgypsgc')[0].checked)
	// 	{
	// 		usebrgypsgcflag=1;
	// 		$('#brgypsgc').prop('disabled', false);
	// 		$('input.annexaddress').prop('disabled', true);

	// 		console.log(brgypsgc);
	// 		$('#brgypsgc').empty();
	// 		$('#brgypsgc').val(brgypsgc);
	// 	}
	// 	else
		{
			usebrgypsgcflag=0;
			$('#brgypsgc').prop('disabled', true);
			$('input.annexaddress').prop('disabled', false);
			$.ajax({
				type: 'POST',
				url: url+'loadannex/province',
				dataType: 'json',
				async: false,
				success: function(data){

					var datas=[];
					$.each(data, function(index, value) {
						datas.push(value);
					});
					datas.sort();
					$.each(datas, function(index, value) {
						$('#addprovince_sitename').append('<option value="'+value+'">'+value+'</option>');
					});
				}
			});
		}
	}
	function sitenamesuggest(){

		var brgycharcount = 6;
		var provincetowncode;
		var provincetowncodelength;
		var province = $("#addprovince_sitename").val();
		var town = $("#addtown_sitename").val();
		var barangay = $("#addbarangay_sitename").val();
		var addbldg_sitename = $("#addbldg_sitename").val();
		var addstreet_sitename = $("#addstreet_sitename").val();
        var bcfprefix;
		// var brgypsgc = $("#brgypsgc").val();

		// console.log(addbldg_sitename);
		// sitetypeannex = $("#towertype").val();
		if(sitetypeannex===null)
		{
			sitetypeannex='';
		}
		// if(province!=="" && town!=="" && barangay!=="" && province!==null && town!==null && barangay!==null)
		// {
		// 	// $.ajax({
		// 	// 	type: 'POST',
		// 	// 	url: url+'generatesitename',
		// 	// 	data:
		// 	// 		{
		// 	// 			"province"          :   province,
		// 	// 			"town"              :   town,
		// 	// 			"barangay"          :   barangay
		// 	// 		},
		// 	// 	dataType: 'json',
		// 	// 	async: false,
		// 	// 	success: function(data){
		// 	// 		// console.log(data);
		// 	// 		provincetowncode = data.town_code+data.province_code;
		// 	// 		region = data.region;
		// 	// 		brgypsgc = data.psgc;
		// 	// 		// console.log(brgypsgc);
		// 	// 	}
		// 	// });
  //  //          provincetowncode = data.town_code+data.province_code;
  //  //          region = data.region;
  //           // brgypsgc = data.psgc;

		// 	// $("#brgypsgc").val(brgypsgc);
		// 	barangay = barangay.toUpperCase();
		// 	barangay = barangay.split(' ');
		// 	var i;
		// 	for(i=0;i<barangay.length;i++)
		// 	{
		// 		if(barangay[i]=='BARANGAY') barangay[i] = 'BR';
		// 		if(barangay[i]=='POBLACION') barangay[i] = 'POB';
		// 		if(barangay[i]=='DISTRICT') barangay[i] = 'DST';
		// 		if(barangay[i]=='ZONE') barangay[i] = 'ZN';
		// 		if(barangay[i]=='SITIO') barangay[i] = 'SIT';
		// 		if(barangay[i]=='BARIO') barangay[i] = 'BRO';
		// 	}
		// 	for(i=0;i<barangay.length;i++)
		// 	{
		// 		if(barangay[i] == 'POB' && barangay[i+1] == 'DIST')
		// 		{
		// 			barangay[i] = 'PD';
		// 			barangay[i+1] = '';
		// 		}
		// 	}
		// 	// if(barangay[0]=='BARANGAY') barangay[0] = 'BR';
		// 	// if(barangay[0]=='POBLACION') barangay[0] = 'POB';
		// 	barangay = barangay.join();
		// 	barangay = barangay.replace(/[^a-z0-9]/gi,'');
		// 	barangay = (barangay.replace(/\s/g, '')).substring(0,brgycharcount);
		// 	var stbldgcharcount = ((17-(provincetowncode.length))-barangay.length)+3;
		// 	if(addbldg_sitename!=="" || addbldg_sitename!==undefined)
		// 	{
		// 		addbldg_sitename.toUpperCase();
		// 		addbldg_sitename = addbldg_sitename.replace(/[^a-z0-9]/gi,'');
		// 		addbldg_sitename = (addbldg_sitename.replace(/\s/g, '')).substring(0,stbldgcharcount);
		// 		addstreet_sitename = '';
		// 	}else if(addstreet_sitename!=="")
		// 	{
		// 		addstreet_sitename.toUpperCase();
		// 		addstreet_sitename = addstreet_sitename.replace(/[^a-z0-9]/gi,'');
		// 		addstreet_sitename = (addstreet_sitename.replace(/\s/g, '')).substring(0,stbldgcharcount);
		// 		addbldg_sitename = '';
		// 	}else
		// 	{
		// 		brgycharcount = 9;
		// 	}
  //           bcfprefix = addbldg_sitename+addstreet_sitename+barangay;
  //           console.log(bcfprefix);
		// 	$("#bcfprefix").val(bcfprefix);
		// 	$("#bcfname").text(addbldg_sitename+addstreet_sitename+barangay+provincetowncode+sitetypeannex);
		// }
	}


	function getazimuth(){
		var x=0;
		var first;
		var sortedazimuth=[];
		azimuth = $("#azimuth").val();

		azimuth=strtoarr(azimuth);
		azimuth=[...new Set(azimuth)];
		azimuth.sort(function(a, b){return a - b});

		for(x=0; x<=azimuth.length; x++)
		{

			if(azimuth[x]>360)
			{   //above 360
				alert('azimuth must be 0° to 359°');
				$('#azimuthcount').append('azimuth must be 0° to 359°');
			}
			else if((azimuth[x+1]-azimuth[x])<60)
			{
				alert('azimuth must have at least 60° interval');
				azimuth=[azimuth[x]];
				$('#azimuthcount').append('azimuth must have at least 60° interval');
			}
			else if(azimuth[x]<=30)
			{
				// console.log('aaa');
				//push
				first = azimuth[x];
			}
			else if(azimuth[x]>=330)
			{
				console.log('eee');
				//push
				first = azimuth[x];
			}
			// else
			// {
			//     console.log('elsesssssss');
			// }

			// console.log('first: '+first+ '  azi[x]: '+ azimuth[x]);
		}

		sortedazimuth.push(first);
		azimuth.forEach(azimuthpush);
		function azimuthpush(item){
			if(item!=first){
				sortedazimuth.push(item);
			}
		}
		// console.log('xxx: '+sortedazimuth);
		// azimuth = azimuthsort(azimuth);
		sectorcount = azimuth.length;
		// console.log('sectorcount:'+sectorcount);
		//$('#azimuthcount').append(" \""+sectorcount+'\" Sectors');
		$('#azimuthcount').empty();
		// for(var key in sortedazimuth){
		//     // $('#azimuthcount').append(''+(parseInt(key)+1)+'-'+azimuth[key]+'° ');
		//     $('#azimuthcount').append(+sortedazimuth[key]+'° ');
		// }

		return sortedazimuth;
	}

	function generateallcells(){
		templatechange();
		generatecid('2g',0);
		generatecid('3g',0);
		generatecid('psc',0);
		var sitename = $("#addsuggested_sitename").val().toUpperCase();
		sitename = sitename.toUpperCase();

		dbid2g=generatedbid('2g',0);
		dbid3g=generatedbid('3g',0);
		var data2g=[];
		var data3g=[];
		btscidflag=0;
		nodebcidflag=0;
		var div = Math.floor(sectorcount/3);
		var divremainder = sectorcount%3;
		var sc=1;
		var scp=3;
		var dl;
		var cellcounter=0;
		// console.log('div: '+typeof(div)+' divremainder: '+typeof(divremainder));
		if(div==0 && divremainder>0)
		{
			scp=divremainder;
			divremainder = 0;
		}
		//console.log('sectorcountflag: '+sectorcountflag);

		var nodebcidtemp=[]
		var mult=multiplier;
		var tempcount1=0,tempcount2=0;
		while(mult>0)
		{
			for( key in nodebcidplacement)
			{
				if(nodebcidplacement[key]==1)
				{
					nodebcidtemp[tempcount1] = nodebcid[tempcount2];
					tempcount2++;
				}
				tempcount1++;
			}
			mult--;
		}
		console.log('nodebcidtemp: '+nodebcidtemp);
		console.log('sectorcountflag: '+sectorcountflag);
		console.log('alldata: '+alldata);
		if(parseInt(sectorcountflag)===1){
			$('#azimuthcount').empty();
			$('#azimuthcount').append('CLP and Azimuth sectorcount not match.');
		}else{
			console.log('nodebcid: '+nodebcid);
			while(div>=0){
				while(sc<=scp){
					dl = alldata.length-1;
					while(dl>=0){
						if(sc>3) cellcounter=sc-3;
						else cellcounter=sc;
						// console.log("dl:"+dl+" dr:"+divremainder+" scp:"+scp+" sc:"+sc+' cellcounter:'+cellcounter);
						switch(alldata[dl][0]){
							case "g9":
								data2g.push(['',sitename+"-"+sc, cellcounter+btscid[btscidflag], azimuth[sc-1]]);
								break;
							case "g18":
								data2g.push(['',sitename+"X-"+sc,g18cn[cellcounter-1]+btscid[btscidflag], azimuth[sc-1]]);
								break;
							case "u9":
								///// F1
								if(alldata[dl][1]>=1){
									data3g.push(['',sitename+"Z",sitename+"J-"+sc,nodebcidtemp[nodebcidflag]+(cellcounter+6), azimuth[sc-1],psc[sc-1]]);
								}
								///// F2
								if(alldata[dl][1]>=2){
									data3g.push(['',sitename+"Z",sitename+"J-"+abc[sc-1],nodebcidtemp[nodebcidflag+2]+(cellcounter+6), azimuth[sc-1],psc[sc-1]]);
								}
								break;
							case "u21":
								///// F1
								if(alldata[dl][1]>=1){
									data3g.push(['',sitename+"Z",sitename+"Z-"+sc,nodebcidtemp[nodebcidflag]+cellcounter, azimuth[sc-1],psc[sc-1]]);
								}
								///// F2
								if(alldata[dl][1]>=2){
									data3g.push(['',sitename+"Z",sitename+"Z-"+abc[sc-1],nodebcidtemp[nodebcidflag]+(cellcounter+3), azimuth[sc-1],psc[sc-1]]);
								}
								///// F3
								if(alldata[dl][1]>=3){
									data3g.push(['',sitename+"Z",sitename+"Z-"+ghi[sc-1],nodebcidtemp[nodebcidflag+1]+cellcounter, azimuth[sc-1],psc[sc-1]]);
								}
								break;
							default:
								break;
						}
						dl--;
					}
					// console.log(btscid);
					// console.log('btscidflag[btscidflag+1]: '+btscid[btscidflag]+' '+btscidflag);
					// console.log(nodebcid);
					// console.log('nodebcid[nodebcidflag]  : '+nodebcidtemp[nodebcidflag]+' '+nodebcidflag);
					// console.log('nodebcid[nodebcidflag+1]: '+nodebcidtemp[nodebcidflag+1]+' '+(nodebcidflag+1));
					// console.log('nodebcid[nodebcidflag+2]: '+nodebcidtemp[nodebcidflag+2]+' '+(nodebcidflag+2));
					sc++;
				}
				div--;
				if(div>0){
					scp+=3;
					btscidflag++;
					nodebcidflag+=3;
					console.log("div>0");
				}
				else if(divremainder>0){
					console.log("divremainder>0");
					div++;
					scp+=divremainder;
					divremainder=0;
					btscidflag++;
					nodebcidflag+=3;
				}
				// if(div==0&&)
			}
		}
		data2g.sort(function(a,b){ return a[1] > b[1] ? 1 : -1; });
		data3g.sort(function(a,b){ return a[2] > b[2] ? 1 : -1; });
		var i=0;
		for(i=0;i<data2g.length;i++)
		{
			data2g[i][0] = dbid2g;
			dbid2g=generatedbid('2g',dbid2g);
		}
		for(i=0;i<data3g.length;i++)
		{
			data3g[i][0] = dbid3g;
			dbid3g=generatedbid('3g',dbid3g);
		}
		// console.log(data2g);
		// console.log(data3g);
		return [data2g,data3g];
	}

	//create site
	$('#create_site').on("click", function(event)
	{
		// var req = ($('input,textarea,select').find('[required]:visible'));
		// req = req.attr('id');
		// console.log($('input[value=""]').attr('id'));
		//var req = $("[required]").css( "border-color", "red" );

		// $(req).css( "border-color", "red" );

		var data = generateallcells();
		var data2g=data[0];
		var data3g=data[1];
		siteid = generatedbid('tower',0);
		console.log(data);
		var modalth1 = "<th scope=\"col\">Sitename</th><th scope=\"col\">PLA ID</th><th scope=\"col\">Province</th><th scope=\"col\">Town</th><th scope=\"col\">Barangay</th><th scope=\"col\">Address</th><th scope=\"col\">Region</th></tr>";
		var modalth2 = "<tr><th scope=\"col\">Site ID</th><th scope=\"col\">Latitude</th><th scope=\"col\">Longitude</th><th scope=\"col\">Tower Type</th><th scope=\"col\">Tower Height</th><th scope=\"col\">Site Type</th><th scope=\"col\">Permanent</th></tr>";
		const modalth3 = `<tr><th colspan="4"scope=\"col\" style="text-align:left;">RF Parameter Details</th></tr><tr><th scope=\"col\">DB Cell ID</th><th scope=\"col\">NodeB Name</th><th scope=\"col\">Cellname</th><th scope=\"col\">Cell ID</th><th scope=\"col\">Azimuth</th><th scope=\"col\">PSC</th></tr>`;

		$('#addsitemodal1 thead').empty();
		$('#addsitemodal2 thead').empty();
		$('#addsitemodal3 thead').empty();
		$('#addsitemodal1 thead').append(modalth1);
		$('#addsitemodal2 thead').append(modalth2);
		$('#addsitemodal3 thead').append(modalth3);
		var province = $("#addprovince_sitename").val();
		var town = $("#addtown_sitename").val();
		var barangay = $("#addbarangay_sitename").val();
		var bldgname = $("#addbldg_sitename").val();
		var street = $("#addstreet_sitename").val();
		var latitude = longlat[0];
		var longitude = longlat[1];
		var plaid = $("#addplaid_sitename").val();
		var sitename = $("#addsuggested_sitename").val();
		sitename = sitename.toUpperCase();
		var address = "";
		var towertype = $("#towertype").val();
		var towerheight = $("#towerheight").val();

		var g900_config = $("#g900_config").val();
		var g1800_config = $("#g1800_config").val();
		var u900_config = $("#u900_config").val();
		var u2100_config = $("#u2100_config").val();
		// var u2100_configcheck=0;
		// if($("#u2100_configcheck").is(':checked')){u2100_configcheck=1;}
		var l700_config = $("#l700_config").val();
		var l1800_config = $("#l1800_config").val();
		// var l1800_configcheck=0;
		// if($("#l1800_configcheck").is(':checked')){ l1800_configcheck=1;}
		var l2300_config = $("#l2300_config").val();
		var l2600_config = $("#l2600_config").val();
		var l2600mm_config = $("#l2600mm_config").val();
		var azimuth = $("#azimuth").val();

		var allinputs = [];
		var blankinputflag;
		allinputs.push(barangay,town,province,sitename,longitude,latitude,plaid,towertype,towerheight,sitetypeval,solutiontypeval);

		solutiontypeval = (solutiontypeval =='permanent') ? "yes" : "no";

		for (var i = 0; i < allinputs.length; i++)
		{
			if(allinputs[i]=="" || undefined || null){
				blankinputflag = 1;
				break;
			}else blankinputflag = 0;
		}

		for (var i = 0; i < allinputs.length; i++)
		{
			if(allinputs[i]==undefined || allinputs[i]==null)
			{
				allinputs[i] = "-";
			}
		}

		if(bldgname!=="")
		{
			address += bldgname+", ";
		}
		if(street!=="")
		{
			address += street+", ";
		}
		address += barangay+", "+town+", "+province;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// if(blankinputflag==1){
		//     SWAL('error', 'Opps!', 'Please fill required fields.');
		// }else
		// if(checksitenameduplicate()==1){
		//     SWAL('error', 'Opps!', 'Sitename already in database. PLease edit.');
		// }else
		// if(parseInt(sectorcountflag)==1){
		//     SWAL('error', 'Opps!', 'CLP and Azimuth sectorcount not match.');
		// }else
		{
			$('#addsitemodal1 tbody').empty();
			$('#addsitemodal2 tbody').empty();
			$('#addsitemodal3 tbody').empty();
			var tr1 = "<tr>";
			//tr1 += "<td>"+ new Date().toLocaleString(); +"</td>";
			tr1 += "<td>"+ sitename +"</td>";
			tr1 += "<td>"+ plaid +"</td>";
			tr1 += "<td>"+ province +"</td>";
			tr1 += "<td>"+ town +"</td>";
			tr1 += "<td>"+ barangay +"</td>";
			tr1 += "<td>"+ address +"</td>";
			tr1 += "<td>"+ region +"</td>";
			tr1 += "</tr>";

			var tr2 = "<tr>";
			tr2 += "<td>"+ siteid +"</td>";
			tr2 += "<td>"+ latitude +"</td>";
			tr2 += "<td>"+ longitude +"</td>";
			tr2 += "<td>"+ towertype +"</td>";
			tr2 += "<td>"+ towerheight +"</td>";
			tr2 += "<td>"+ sitetypeval +"</td>";
			tr2 += "<td>"+ solutiontypeval +"</td>";
			tr2 += "</tr>";

			var tr3;
			for(index in data2g){
				tr3 += "<tr>";
				tr3 += "<td>"+ data2g[index][0] +"</td>";
				tr3 += "<td></td>";
				tr3 += "<td>"+ data2g[index][1] +"</td>";
				tr3 += "<td>"+ data2g[index][2] +"</td>";
				tr3 += "<td>"+ data2g[index][3] +"</td>";
				tr3 += "<td></td>";
				tr3 += "</tr>";
			}
			for(index in data3g){
				tr3 += "<tr>";
				tr3 += "<td>"+ data3g[index][0] +"</td>";
				tr3 += "<td>"+ data3g[index][1] +"</td>";
				tr3 += "<td>"+ data3g[index][2] +"</td>";
				tr3 += "<td>"+ data3g[index][3] +"</td>";
				tr3 += "<td>"+ data3g[index][4] +"</td>";
				tr3 += "<td>"+ data3g[index][5] +"</td>";
				tr3 += "</tr>";
			}

			$('#addsitemodal1 tbody').append(tr1);
			$('#addsitemodal2 tbody').append(tr2);
			$('#addsitemodal3 tbody').append(tr3);

			$('.main_modal').modal('show');

		}
	});

	// var slider = document.getElementById("radius");
	// var output = document.getElementById("radiuslabel");
	// output.innerHTML = slider.value; // Display the default slider value

	// Update the current slider value (each time you drag the slider handle)
	// slider.oninput = function() {
	// 	output.innerHTML = this.value;
	// 	templatechange();
	// }
//////////////submit
	$('#createsitesubmit').on("click", function(e){

		var data = generateallcells();
		var data2g=data[0];
		var data3g=data[1];
		var data2garr=[];
		var data3garr=[];
		var province = $("#addprovince_sitename").val();
		var town = $("#addtown_sitename").val();
		var barangay = $("#addbarangay_sitename").val();
		var bldgname = $("#addbldg_sitename").val();
		var street = $("#addstreet_sitename").val();
		var latitude = $("#addlatitude_sitename").val();
		var longitude = $("#addlongitude_sitename").val();
		var plaid = $("#addplaid_sitename").val();
		var sitename = $("#addsuggested_sitename").val();
		sitename = sitename.toUpperCase();
		var towertype = $("#towertype").val();
		var towerheight = $("#towerheight").val();

		var address = "";
		if(bldgname!==""){ address += bldgname+", "; }
		if(street!==""){ address += street+", "; }
		address += barangay+", "+town+", "+province;

		solutiontypeval = (solutiontypeval =='permanent') ? "yes" : "no";
		var datasite =[{
			//"date"      :new Date().toLocaleString(),
			"siteid"  :siteid,
			"province"  :province,
			"municipality":town,
			"barangay"  :barangay,
			"latitude"  :latitude,
			"longitude" :longitude,
			"plaid"     :plaid,
			"towerheight": towerheight,
			"towertype" :towertype,
			"sitetype"   :sitetypeval,
			"sitename"  :sitename,
			"region"    :region,
			"address"   :address,
			"permanent"   :solutiontypeval,
			"geoarea"  :"VIS",
			"status"  :"newsite"
			////TO BE ADDED TO DB FIELD OF TOWER VIS
		}];

		var i=0;
		for(i=0;i<data2g.length;i++)
		{
			data2garr[i] = {};
			data2garr[i]['dbcellid']=data2g[i][0];
			data2garr[i]['siteid']=siteid;
			data2garr[i]['sitename']=sitename;
			data2garr[i]['cellname']=data2g[i][1];
			data2garr[i]['cid']=data2g[i][2];
			data2garr[i]['azimuth']=data2g[i][3];
			data2garr[i]['longitude']=longitude;
			data2garr[i]['latitude']=latitude;
			data2garr[i]['sitetype']=sitetypeval;
			// data2garr[i]['petal']='No';
		}
		console.log(data2garr);

		for(i=0;i<data3g.length;i++)
		{
			data3garr[i] = {};
			data3garr[i]['dbcellid']=data3g[i][0];
			data3garr[i]['siteid']=siteid;
			data3garr[i]['sitename']=sitename;
			data3garr[i]['cellname']=data3g[i][2];
			data3garr[i]['nodebname']=data3g[i][1];
			data3garr[i]['cid']=data3g[i][3];
			data3garr[i]['azimuth']=data3g[i][4];
			data3garr[i]['psc']=data3g[i][5];
			data3garr[i]['longitude']=longitude;
			data3garr[i]['latitude']=latitude;
			data3garr[i]['sitetype']=sitetypeval;
			// data2garr[i]['petal']='No';
		}
		console.log(data3garr);
		var dbformatdatatower = todbformat(datasite);
		var dbformatdata2g = todbformat(data2garr);
		var dbformatdata3g = todbformat(data3garr);

		var dbtoadd = ['tower','2g','3g'];
		$.each( dbtoadd , function( key, value1 )
		{
			var dbformatdata=eval('dbformatdata' +value1);
			// console.log(value1+': '+dbformatdata);
			$.ajax({
				type: 'POST',
				url: url+'insertsinglemdbupdate',
				dataType: 'json',
				async: false,
				data:
					{
						"operation": "add",
						"siteid":siteid,
						"fieldnames":dbformatdata[0],
						'detailsafter': dbformatdata[1],
						"tech":value1
					},
				success: function(data){
					data = JSON.parse(data);
					console.log(data);
					var id=data;
					console.log('ID: '+id+' created for approval.');
					$.ajax({
						type: 'POST',
						url: url+'insertsingletodb',
						data:{
							'approvalid': id,
							'operation':'add'
						},
						async: false,
						success: function(data2){
							//console.log(data+',');
							// SWAL('success', 'Success!', 'Tower and Cells successfull added to DB.');
							console.log('ID: '+data2+' uploaded to db.');
							//        $('.main_modal').modal('hide');
							//         location.reload();

							Swal.fire({
								title: 'Success',
								text: "Success!",
								icon: 'success',
								showCancelButton: false,
								confirmButtonColor: '#3085d6',
								cancelButtonColor: '#d33',
								confirmButtonText: 'OK'
							}).then((result) => {
								if (result.value) {
									$('.main_modal').modal('hide');
									// location.reload();
									// var siteid = (sitesdatatable.row(this).data())[0];
									localStorage.setItem('siteid',siteid);
									window.location = url+"siteprofilepage";
									console.log("tr click: "+siteid);
								}
							})
						}
					});
				}
			});
		});
	});

	function todbformat(detailsafterarr)
	{
		var dbfieldnames=[];
		var dbdetailsafter=[];

		$.each(detailsafterarr , function( key, value)
		{
			var fieldnamesline=[];
			var detailsafterline=[];
			for(var  key2 in value)
			{
				fieldnamesline.push(key2);
				detailsafterline.push(detailsafterarr[key][key2]);

			}

			dbfieldnames.push(fieldnamesline);
			dbdetailsafter.push(detailsafterline);
		});
		var dbfieldnamestemp = dbfieldnames[0];
		dbfieldnames = [];
		dbfieldnames[0]=dbfieldnamestemp;
		return [dbfieldnames,dbdetailsafter];
	}
    // $('#mapa').dblclick(function(){
    //     var row =  $(this).html();
    //     var newRow = $(this).clone();
    //     $('.main_modal .modal-body').html(newRow);
    //     $('.main_modal').modal();
    // });
    function mapcurrent(lati,longi,geojson)
    {
        map = new google.maps.Map(
          document.getElementById('addsiteMap'),
          {center: new google.maps.LatLng(lati, longi), 
            zoom: 13,
            controlSize: 20,
        });

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lati,longi),
            map: map,
            controlSize: 18
        });
        var marker2 = new google.maps.Marker({
            position:  new google.maps.LatLng(8.506760, 124.449403),
            map: map,
            // controlSize: 18
        });

        map.data.addGeoJson(
        {
            "type": "Feature","properties": {}, "geometry": geojson
        });

        var bounds = new google.maps.LatLngBounds(); 
        map.data.forEach(function(feature){
          feature.getGeometry().forEachLatLng(function(latlng){
             bounds.extend(latlng);
          });
        });
        // bounds.extend(new google.maps.LatLng(8.506760, 124.449403));
        map.fitBounds(bounds);

        var listener = google.maps.event.addListener(map, "idle", function() {
            if (map.getZoom() > 13) map.setZoom(13); 
            google.maps.event.removeListener(listener); 
        });
    }
   
});

function initMap() {

    var lng = 123.288;
    var lat = 11.867;

    map = new google.maps.Map(
      document.getElementById('addsiteMap'),
      {center: new google.maps.LatLng(lat, lng), 
        zoom: 4,
        controlSize: 20,
        styles: 
        [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.attraction",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.business",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]
    });
}


