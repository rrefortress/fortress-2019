let ms_stone = [
  // {
  //   'id'         : 1,
	// 	'name'       : 'Nominations',
  //   'percentage' : '100%',
  //   'on_progress'   : true
	// },
	// {
  //   'id'         : 2,
  //   'name'       : 'AWE',
  //   'percentage' : '0%' ,
  //   'on_progress'   : false
	// },
	{
    'id'         : 1,
    'name'       : 'PMO',
    'percentage' : '0%',
    'on_progress'   : false
	},
	{
    'id'         : 2,
    'name'       : 'SAQ',
    'percentage' : '0%',
    'on_progress'   : false
	},
	{
    'id'         : 3,
    'name'       : 'SR',
    'percentage' : '0%',
    'on_progress'   : false
	},
	{
    'id'         : 4,
    'name'       : 'JTS',
    'percentage' : '0%',
    'on_progress'   : false
 	},
	{
    'id'         : 5,
    'name'       : 'RTB',
    'percentage' : '0%',
    'on_progress'   : false
 	},
	{
    'id'         : 6,
    'name'       : 'CLP',
    'percentage' : '0%',
    'on_progress'   : false
	},
  {
    'id'         : 7,
    'name'       : 'DDD',
    'percentage' : '0%',
    'on_progress'   : false
	},
  {
    'id'         : 8,
    'name'       : 'Builds',
    'percentage' : '0%',
    'on_progress'   : false
	},
  {
    'id'         : 9,
    'name'       : 'TRFS',
    'percentage' : '0%',
    'on_progress'   : false
	},

];

let ms_checkLists = [
  {
    'id'   : 1,
    'name' : 'Tower',
    'status' : true,
  },
  {
    'id'   : 2,
    'name' : 'ATM',
    'status' : false,
  },
  {
    'id'   : 3,
    'name' : 'TBM',
    'status' : true,
  },
  {
    'id'   : 4,
    'name' : 'Cost & Expenses',
    'status' : false,
  },
  {
    'id'   : 5,
    'name' : 'Travel',
    'status' : true,
  },
  {
    'id'   : 6,
    'name' : 'Utilities',
    'status' : false,
  },
  {
    'id'   : 7,
    'name' : 'Labor',
    'status' : true,
  },
  {
    'id'   : 8,
    'name' : 'Travel',
    'status' : true,
  },
  {
    'id'   : 9,
    'name' : 'Utilities',
    'status' : false,
  },
  {
    'id'   : 10,
    'name' : 'Labor',
    'status' : true,
  },
];

$(function () {
	get_mile_stone_lists();
  get_mile_stone_line();
  get_check_lists();
});

$(document).on('click', '#import_ms_name', function(e) {
  e.preventDefault();
  $('#import_view_modal').modal('show');
});

$(document).on('click', '.ms_lists tbody tr', function() {
  reset_array_values();
  let id = $(this).closest('tr').attr('row_id');
  let name = $(this).closest("tr").find('td:eq(0)').text();
  $('.site_title').text(name);
  $('.ms_lists tbody tr').removeClass('bg-primary text-white');
  $(this).addClass('bg-primary text-white');

  $.ajax({
    url: url + 'search_site_track',
    type: "post",
    async:false,
    data: {name: name},
    dataType: 'json',
    success: function (data) {
    let progress_count = 1;
    $.each(data, function(key,value) {
       if (value == true) {
         progress_count += 1;
       }
     });

     for (let i = 0; i < progress_count; i++) {
       ms_stone[i].percentage = '100%';
       ms_stone[i].on_progress = true;
     }
     let total_per = getWholePercent(progress_count, 11) + '%';
     $('.ms-pstep').css('width', total_per);
     $('.ms-pstep').text(total_per);
     $('.p-percentages').text(progress_count + '/ 11');

     get_mile_stone_line();
     get_check_lists(progress_count);
    },
    error: function(jqXHR, textStatus, errorThrown) {
      SWAL('error', "Oops!", "something went wrong.");
    }
  });
});

$(document).on('keyup', '#search_items', function(e) {
	e.preventDefault();
	get_mile_stone_lists(0, row_page = 10, $(this).val());
});

$(document).on('click', '#ms_pagination a', function(e) {
	e.preventDefault();
	let row_num = $(this).attr('data-ci-pagination-page');
  get_mile_stone_lists(row_num);
});

function get_mile_stone_lists(row_num = 0, row_page = 10, search = '') {
  let html = '';
  let ctr = 1;

	$.ajax({
		url: url + 'MileStone/get_site_names/'+row_num,
		type: "GET",
    dataType: 'json',
		data: {row_num: row_num, row_page : row_page, search: search},
		beforeSend() {
			let html = '<div class="content">' +
				'<h5 class="text-center text-white">' +
				'<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>' +
				'<h4 class="text-dark text-center">Loading</h4>' +
				'</h5>' +
				'</div>';
			$('.ms_table').html(html);
		},
		success: function (data) {
			let html = '';
        if (data.data.length > 0) {
          for (i in data.data) {
            html += '<tr class="border-bottom" row_id='+data.data[i].id+'>' +
                '<th scope="row" class="text-center">'+data.data[i].id+'.</th>' +
                '<td class="text-truncate" data-toggle="tooltip" data-placement="top" title='+data.data[i].SITE_NAME+'>'+data.data[i].SITE_NAME+'</td>' +
              '</tr>';
              ctr++;
          }
        }
			else
			{
				html = '<div class="content mg-top-80">' +
					'<h5 class="text-center text-white">' +
					'<i class="fas fa-tasks display-1 text-dark"></i><br>' +
					'<h4 class="text-dark text-center">Empty Data.</h4>' +
					'<label class="text-dark text-center">No data has been added yet.</label>' +
					'</h5>' +
					'</div>';
			}

			$('.ms_table').html(html);
      $('#ms_pagination').html(data.pagination);
      $('[data-toggle="tooltip"]').tooltip();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			SWAL('error', "Oops!", "something went wrong.");
		}
	});
}

function get_mile_stone_line() {
  let ctr = 1;
  let html = '';

  if (ms_stone.length > 0) {
    for (i in ms_stone) {
      let progresz = ms_stone[i].on_progress ? 'is-complete' : '';
      html += '<li class="progress-step '+progresz+' anim-ripple">' +
        '<div class="progress-marker" data-text='+ctr+'></div>' +
        '<div class="progress-text">' +
          '<h6 class="progress-title">'+ms_stone[i].name+'</h6>' +
          ms_stone[i].percentage  +
        '</div>' +
      '</li>';
      ctr++;
    }
    $('#progress_tracks').html(html);
  }
}

function get_check_lists(progress = 0) {
  let ctr = 1;
  let html = '';
  let details = '';

  if (progress > 0) {
    for (let i = 0; progress > i; progress--) {
        let name = ms_stone[progress - 1].on_progress ? 'Completed' : 'Ongoing';
        let icon = ms_stone[progress - 1].on_progress ? 'fas fa-check' : 'fas fa-spinner fa-spin';
        html += ' <div class="card">' +
               '<button id="heading-'+ctr+'" class="btn collapse-'+ctr+' border-bottom bg-primary text-white text-left rounded-0" type="button" data-toggle="collapse" data-target="#collapse-'+ctr+'" aria-expanded="false" aria-controls="collapse-'+ctr+'">' +
                 '<i class="'+icon+'"></i> '+ms_stone[progress - 1].name+' <i>'+name+'</i>' +
               '</button>' +
           '<div id="collapse-'+ctr+'" class="collapse-'+ctr+'" aria-labelledby="heading-'+ctr+'" data-parent="#check_lists_accordion">' +
              '<ul class="list-group list-group-flush ms_checkLists"></ul>' +
           '</div>' +
         '</div>';
       ctr++;
    }

    if (ms_checkLists.length > 0) {
      for(i in ms_checkLists) {
        let icon = ms_checkLists[i].status ? 'fa-check-circle' : 'fa-times-circle';
        let text = ms_checkLists[i].status ? 'success' : 'danger';
        details += ' <li class="list-group-item"><i class="fas '+icon+' text-'+text+'"></i> '+ms_checkLists[i].name+'</li>';
      }
    }
  } else {
    html = '<div class="content">' +
              '<h5 class="text-center text-white">' +
                '<i class="fas fa-arrow-left display-1 text-dark"></i><br>' +
                '<h4 class="text-dark text-center">Please select lists on the left.</h4>' +
                '<label class="text-dark text-center">To view the details of the specific Site names.</label>' +
              '</h5>' +
          '</div>';
  }

  $('.check_lists_accordion').html(html);
  $('.ms_checkLists').html(details);

  $('#check_lists_accordion').collapse({
    toggle: false
  });
}

function reset_array_values() {
  for (i in ms_stone) {
    if (i > 1) {
      ms_stone[i].percentage = '0%';
      ms_stone[i].on_progress = false;
    }
  }
}
