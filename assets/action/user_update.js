$(function() {
	$(document).on("change",".uploadFile", function() {
		$('.imagePreview').attr("src", "");
		var uploadFile = $(this);
		var files = !!this.files ? this.files : [];
		if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

		if (/^image/.test( files[0].type)){ // only image file
			var reader = new FileReader(); // instance of the FileReader
			reader.readAsDataURL(files[0]); // read the local file

			reader.onloadend = function(){ // set image data as background of div
				$('.imagePreview').attr("src",this.result);
				$('#image_profile').val(this.result);
			}
		}
	});
});

$(document).on("submit","#update_user", function(e) {
	e.preventDefault();
	let param = {
		url: url + 'updateUser',
		dataType: 'json',
		type: 'POST',
		data:new FormData(this),
	};

	fortress(param).then( (data) => {
		// SWAL(data.type, data.title, data.message);
		// emit_socket(data.logs);
	});
});


