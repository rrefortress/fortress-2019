let table1_lists = ['LAHUG', '1016RESCEB', 'BALAMB', 'BTCIN', 'CYBROB', 'ZARALO', 'TOBOSO', 'MINGLA', 'ORMOC', 'PAVIA2'];
let select_lists = [];
let config = [];
let ctr = 0;

let models = [
	{
		'id'    : 1,
		'model' : 'G900',
		'radius': 550,
		'color' : '#43a047',
	},
	{
		'id'    : 2,
		'model' : 'G1800',
		'radius': 520,
		'color' : '#8bc34a'
	},
	{
		'id'    : 3,
		'model' : 'U2100',
		'radius': 450,
		'color' : '#2196f3'
	},
	{
		'id'    : 4,
		'model' : 'U900',
		'radius': 480,
		'color' : '#01579b'
	},
	{
		'id'    : 5,
		'model' : 'L1800',
		'radius': 390,
		'color' : '#ff9800'
	},
	{
		'id'    : 6,
		'model' : 'L2600',
		'radius': 330,
		'color' : '#9c27b0'
 	},
	{
		'id'    : 7,
		'model' : 'L700',
		'radius': 420,
		'color' : '#f44336'
 	},
	{
		'id'    : 8,
		'model' : 'L2300',
		'radius': 360,
		'color' : '#e91e63'
	},
];

$(function () {
	setTable_lists_data();
	setTable_lists_excel();
	setSelected_datas();
	setViewer();
});

$('#btnImportExcel').on('click', function () {
	$('#showImportExcel').modal('show');
});

$('#chk_data').on('change', function () {
	$(".check-tbl-data").prop('checked', !!$(this).is(":checked"));
	$('#move_data').prop('disabled', !$(this).is(":checked"));
});

$(document).on('click', '.check-tbl-data', function () {
	let chk = $('#chk_data');
	let total_checked = $('#table-lists-data').find('.check-tbl-data:checked').length;
	let total_length = $('#table-lists-data tr').length;
	chk.prop('checked', total_checked === total_length);

	if (total_checked === total_length) {
		$('#move_data').prop('disabled', !chk.is(":checked"));
	} else {
		$('#move_data').prop('disabled', total_checked === 0);
	}
});

function setTable_lists_data() {
	let html = '';

	if (select_lists.length > 0) {
		for (i in table1_lists) {
			html += '<tr>' +
				'<td>' +
				'<div class="custom-control custom-checkbox chk-'+ctr+'">' +
				'<input type="checkbox" class="custom-control-input check-tbl-data" id="check-data-'+ctr+'">' +
				'<label class="custom-control-label" for="check-data-'+ctr+'"></label>' +
				'</div>' +
				'</td>' +
				'<td>'+$.trim(select_lists[i])+'</td>' +
				'</tr>';
			ctr++;
		}
	} else {
		html = '<div class="content">' +
			'<h5 class="text-center text-white">' +
			'<i class="fas fa-list display-1 text-dark"></i><br>' +
			'<h4 class="text-dark text-center">Empty Data.</h4>' +
			'<label class="text-dark text-center">No data has been uploaded yet.</label>' +
			'</h5>' +
			'</div>';
	}


	$('#table-lists-data').html(html);
}

$('#chk_excel').on('change', function () {
	$(".check-tbl-excel").prop('checked', !!$(this).is(":checked"));
	$('#move_excel').prop('disabled', !$(this).is(":checked"));
});

$(document).on('click', '.check-tbl-excel', function () {
	let chk = $('#chk_excel');
	let total_checked = $('#table-lists-excel').find('.check-tbl-excel:checked').length;
	let total_length = $('#table-lists-excel tr').length;
	chk.prop('checked', total_checked === total_length);

	if (total_checked === total_length) {
		$('#move_excel').prop('disabled', !chk.is(":checked"));
	} else {
		$('#move_excel').prop('disabled', total_checked === 0);
	}
});

function setTable_lists_excel() {
	let html = '';

	if (select_lists.length > 0) {
		for (i in table1_lists) {
			html += '<tr>' +
				'<td>' +
				'<div class="custom-control custom-checkbox chk-'+ctr+'">' +
				'<input type="checkbox" class="custom-control-input check-tbl-excel" id="check-excel-'+ctr+'">' +
				'<label class="custom-control-label" for="check-excel-'+ctr+'"></label>' +
				'</div>' +
				'</td>' +
				'<td>'+$.trim(table1_lists[i])+'</td>' +
				'</tr>';
			ctr++;
		}
	} else {
		html = '<div class="content">' +
			'<h5 class="text-center text-white">' +
			'<i class="fas fa-file-excel display-1 text-dark"></i><br>' +
			'<h4 class="text-dark text-center">Empty Data.</h4>' +
			'<label class="text-dark text-center">No data has been uploaded yet.</label>' +
			'</h5>' +
			'</div>';
	}


	$('#table-lists-excel').html(html);
}

$('#chk_sel').on('change', function () {
	$(".check-tbl-selected").prop('checked', !!$(this).is(":checked"));
	$('#move_back').prop('disabled', !$(this).is(":checked"));
});

$(document).on('click', '.check-tbl-selected', function () {
	let chk = $('#chk_sel');
	let total_checked = $('#table_selected_datas').find('.check-tbl-selected:checked').length;
	let total_length = $('#table_selected_datas tr').length;
	chk.prop('checked', total_checked === total_length);

	if (total_checked === total_length) {
		$('#move_back').prop('disabled', !chk.is(":checked"));
	} else {
		$('#move_back').prop('disabled', total_checked === 0);
	}
});

function setSelected_datas() {
	let html = '';
	if (table1_lists.length > 0) {
		for (i in table1_lists) {
			html += '<tr>' +
				'<td>' +
				'<div class="custom-control custom-checkbox my-'+ctr+' mr-sm-2">' +
				'<input type="checkbox" class="custom-control-input check-tbl-selected" id="check-select-'+ctr+'">' +
				'<label class="custom-control-label" for="check-select-'+ctr+'"></label>' +
				'</div>' +
				'</td>' +
				'<td>'+$.trim(table1_lists[i])+'</td>' +
				'</tr>';
			ctr++;
		}
	} else {
		html = '<div class="content">' +
			'<h5 class="text-center text-white">' +
				'<i class="fas fa-list display-1 text-dark"></i><br>' +
				'<h4 class="text-dark text-center">Empty Data.</h4>' +
				'<label class="text-dark text-center">No data has been selected yet.</label>' +
				'</h5>' +
			'</div>';
	}


	$('#table_selected_datas').html(html);
}

$('#chk_view').on('change', function () {
	config = [];
	$(".check-tbl-view").prop('checked', !!$(this).is(":checked"));
	$('#download_kml').prop('disabled', !$(this).is(":checked"));

	let total_length = $('#table-viewer-lists tr').length;

	if ($(this).is(":checked")) {
		for (i = 1; i <= total_length; i++) {
			let row = $('.row-'+i);
			let model = row.closest("tr").find('td:eq(1)').text();
			let color = row.closest("tr").find('.view_color').val();
			let radius = row.closest("tr").find('.radius').text();

			config.push({model: model, color: color, radius: radius});
		}
		console.log(config);
	}
});

$(document).on('click', '.check-tbl-view', function () {
	let chk = $('#chk_view');
	let total_checked = $('#table-viewer-lists').find('.check-tbl-view:checked').length;
	let total_length = $('#table-viewer-lists tr').length;
	chk.prop('checked', total_checked === total_length);

	if (total_checked === total_length) {
		$('#download_kml').prop('disabled', !chk.is(":checked"));
	} else {
		$('#download_kml').prop('disabled', total_checked === 0);
	}

	let model = $(this).closest("tr").find('td:eq(1)').text();
	let color = $(this).closest("tr").find('.view_color').val();
	let radius = $(this).closest("tr").find('.radius').text();

	if ($(this).is(":checked")) {
		config.push({model: model, color: color, radius: radius});
	} else {
		config.pop($(this).closest('tr').attr('row_id'));
	}
	console.log(config);
});

$(document).on('change', '.view_color', function() {
	// let total_length = $('#table-viewer-lists tr').length;
	// console.log($(this).val() + ' ' + length);
	// $(this).closest("tr").attr('row_id');
	// console.log($(this).closest("tr").attr('row_id'));
	console.log($(this).val());

	// let row = $(this).closest("tr").attr('row_id');
	// $('#col-'+parseInt(row)).val($(this).val());

	// $(this).closest('view_color').val($(this).val());
});

function setViewer() {
	let html = '';
	for (i in models) {
		html += '<tr class="row-'+models[i].id+'"  row_id='+models[i].id+'>' +
				'<td scope="row">' +
					'<div class="custom-control custom-checkbox my-1 mr-sm-2">' +
						'<input type="checkbox" class="custom-control-input check-tbl-view" id="check-view-'+ctr+'">' +
						'<label class="custom-control-label" for="check-view-'+ctr+'"></label>' +
					'</div>' +
				'</td>' +
				'<td>'+models[i].model+'</td>' +
				'<td><input type="color" name="favcolor" id="col-'+models[i].id+'" class="view_color" value='+models[i].color+'></td>' +
				'<td><div class="row_data radius" edit_type="click" col_name="radius">'+models[i].radius+'</div> </td>' +
			'</tr>';
		ctr++;
	}
	$('#table-viewer-lists').html(html);
}

$(document).on('click', '.row_data', function (e) {
	e.preventDefault();
	$(this).closest('div').attr('contenteditable', true);

	$(this).addClass('bg-white').css('padding', '5px');

	$(this).focus();
});

$(document).on('focusout', '.row_data', function (e) {
	e.preventDefault();

	let row_id = $(this).closest('tr').attr('row_id');
	let row_div = $(this).removeAttr('contenteditable').removeClass('bg-white').css('padding', '');

	let col_name = row_div.attr('col_name');
	let col_val = row_div.html();

	let arr = {};
	arr[col_name] = col_val;

	$.extend(arr, {row_id:row_id});

	console.log(JSON.stringify(arr, null, 2));
});

$(document).on('click', '#download_kml', function () {
	let radio = $("input[name=radio]:checked").val();
	let info = $("#info").is(':checked');

	let data = {
		config: config,
		option: radio,
		lists: table1_lists,
		info: info
	};



	$.ajax({
		type: 'POST',
		url: url+'mapkml',
		data: data,
		async: false,
		error: function(data){
			SWAL('error', 'Ooops!', 'Someting went wrong please try again.');
		},
		success: function(){
		$.fileDownload(url+'mapkml',{
			httpMethod: 'POST',
		   	data:data,
		    successCallback:function(data){
		    }
		});
		}
	});

	console.log(data);
});
