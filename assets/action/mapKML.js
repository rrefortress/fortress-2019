let table1_lists = [];
let ctr = 0;
let table2_lists =[];
let default_outdoor = [
	{
		'model' : 'G900',
		'radius': 550,
		'color' : '#43a047',
	},
	{
		'model' : 'G1800',
		'radius': 520,
		'color' : '#8bc34a'
	},
	{
		'model' : 'U2100',
		'radius': 450,
		'color' : '#2196f3'
	},
	{
		'model' : 'U900',
		'radius': 480,
		'color' : '#01579b'
	},
	{
		'model' : 'L1800',
		'radius': 390,
		'color' : '#ff9800'
	},
	{
		'model' : 'L2600',
		'radius': 330,
		'color' : '#9c27b0'
 	},
	{
		'model' : 'L700',
		'radius': 420,
		'color' : '#f44336'
 	},
	{
		'model' : 'L2300',
		'radius': 360,
		'color' : '#e91e63'
	},
];

let default_indoor = [
	{
		'model' : 'G900',
		'radius': 300,
		'color' : '#43a047',
	},
	{
		'model' : 'G1800',
		'radius': 270,
		'color' : '#8bc34a'
	},
	{
		'model' : 'U2100',
		'radius': 210,
		'color' : '#2196f3'
	},
	{
		'model' : 'U900',
		'radius': 240,
		'color' : '#01579b'
	},
	{
		'model' : 'L1800',
		'radius': 160,
		'color' : '#ff9800'
	},
	{
		'model' : 'L2600',
		'radius': 100,
		'color' : '#9c27b0'
 	},
	{
		'model' : 'L700',
		'radius': 190,
		'color' : '#f44336'
 	},
	{
		'model' : 'L2300',
		'radius': 130,
		'color' : '#e91e63'
	},
];

let default_site = [
	{
		'model' : 'G900',
		'radius': 0,
		'color' : '#ffff00',
	},
	{
		'model' : 'G1800',
		'radius': 0,
		'color' : '#ffff00'
	},
	{
		'model' : 'U2100',
		'radius': 0,
		'color' : '#ffff00'
	},
	{
		'model' : 'U900',
		'radius': 0,
		'color' : '#ffff00'
	},
	{
		'model' : 'L1800',
		'radius': 0,
		'color' : '#ffff00'
	},
	{
		'model' : 'L2600',
		'radius': 0,
		'color' : '#ffff00'
 	},
	{
		'model' : 'L700',
		'radius': 0,
		'color' : '#ffff00'
 	},
	{
		'model' : 'L2300',
		'radius': 0,
		'color' : '#ffff00'
	},
];

$(function () {
	dataload();
	setTable_lists_data();
	setSelected_datas();
	setViewer(default_site);
});

function dataload(){
	$.ajax({
		type: 'POST',
		url: url+'getsitelist',
		dataType: 'json',
		async: false,
		error: function(data){
			SWAL('error', 'Ooops!', 'Someting went wrong please try again.');
		},
		success: function(data){

			if (data.length > 0)
			{
				for (x in data)
				{
					table2_lists.push(data[x].sitename);
				}
			}
			table2_lists.sort();
		}
	});
}

function setTable_lists_data() {
	let html = '';
	
	if (table2_lists.length > 0)
	{
		for (i in table2_lists)
		{
			html += '<tr>' +
				'<td>' +
				'<div class="custom-control custom-checkbox chk-'+i+'">' +
				'<input type="checkbox" name="table2_lists" class="custom-control-input check-tbl-data" id="check-data-'+i+'">' +
				'<label class="custom-control-label" for="check-data-'+i+'"></label>' +
				'</div>' +
				'</td>' +
				'<td>'+$.trim(table2_lists[i])+'</td>' +
				'</tr>';
		}
	}
	else
	{
		html = '<div class="content">' +
			'<h5 class="text-center text-white">' +
			'<i class="fas fa-list display-1 text-dark"></i><br>' +
			'<h4 class="text-dark text-center">Empty Data.</h4>' +
			'<label class="text-dark text-center">No data has been uploaded yet.</label>' +
			'</h5>' +
			'</div>';
	}
	$('#table-lists-data').html(html);
	//$('#table-lists-data').empty();
}

$('#move_data').click(function(){
	$("input:checkbox[name=table2_lists]:checked").each(function(){
    	table1_lists.push($(this).closest("tr").text());
	});
	
	table2_lists = table2_lists.filter(item => !table1_lists.includes(item));
	table2_lists.sort();
	table1_lists.sort();
	//console.log(table2_lists);
	setTable_lists_data();
	setSelected_datas();
	//console.log(table2_lists);
});


$('#move_back').click(function(){
	$("input:checkbox[name=table1_lists]:checked").each(function(){
    	table2_lists.push($(this).closest("tr").text());
	});
	
	table1_lists = table1_lists.filter(item => !table2_lists.includes(item));
	table1_lists.sort();
	table2_lists.sort();
	//console.log(table1_lists);
	setTable_lists_data();
	setSelected_datas();
	//console.log(table2_lists);
});


$(document).on('click', '.check-tbl-selected', function () {
	let chk = $('#chk_sel');
	let total_checked = $('#table_selected_datas').find('.check-tbl-selected:checked').length;
	let total_length = $('#table_selected_datas tr').length;
	chk.prop('checked', total_checked === total_length);

	if (total_checked === total_length) {
		$('#move_back').prop('disabled', !chk.is(":checked"));
	} else {
		$('#move_back').prop('disabled', total_checked === 0);
	}
});

function setSelected_datas() {
	let html = '';
	if (table1_lists.length > 0) {
		for (i in table1_lists) {
			html += '<tr>' +
				'<td>' +
				'<div class="custom-control custom-checkbox my-'+ctr+' mr-sm-2">' +
				'<input type="checkbox" name="table1_lists" class="custom-control-input check-tbl-selected" id="check-select-'+ctr+'">' +
				'<label class="custom-control-label" for="check-select-'+ctr+'"></label>' +
				'</div>' +
				'</td>' +
				'<td>'+$.trim(table1_lists[i])+'</td>' +
				'</tr>';
			ctr++;
		}
	} else {
		html = '<div class="content">' +
			'<h5 class="text-center text-white">' +
				'<i class="fas fa-list display-1 text-dark"></i><br>' +
				'<h4 class="text-dark text-center">Empty Data.</h4>' +
				'<label class="text-dark text-center">No data has been selected yet.</label>' +
				'</h5>' +
			'</div>';
	}


	$('#table_selected_datas').html(html);
}


function setViewer(models) {
	let html = '';
	for (i in models) {
		html += '<tr class="row-'+models[i].model+'"  row_id='+models[i].model+'>' +
				'<td scope="row">' +
					'<div class="custom-control custom-checkbox my-1 mr-sm-2">' +
						'<input type="checkbox" name="checkboxes" class="custom-control-input check-tbl-view" id="check-view-'+ctr+'">' +
						'<label class="custom-control-label" for="check-view-'+ctr+'"></label>' +
					'</div>' +
				'</td>' +
				'<td>'+models[i].model+'</td>' + 
				'<td><input type="color" name="favcolor" id="col-'+models[i].model+'" class="view_color" value='+models[i].color+'></td>' +
				'<td><div class="row_data radius" edit_type="click" col_name="radius">'+models[i].radius+'</div> </td>' +
			'</tr>';
		ctr++;
	}
	$('#table-viewer-lists').html(html);
}



$("input[type=radio][name=requesttype]").change(function(){
    switch(this.value){
    	case 'site':
    	setViewer(default_site);
    	break;
    	case 'indoor':
    	setViewer(default_indoor);
    	break;
    	case 'outdoor':
    	setViewer(default_outdoor);
    	break;
    }
});

$('#chk_view').on('change', function () {
	$(".check-tbl-view").prop('checked', !!$(this).is(":checked"));
	$('#download_kml').prop('disabled', !$(this).is(":checked"));
});

$(document).on('click', '.check-tbl-view', function () {
	let chk = $('#chk_view');
	let total_checked = $('#table-viewer-lists').find('.check-tbl-view:checked').length;
	let total_length = $('#table-viewer-lists tr').length;
	chk.prop('checked', total_checked === total_length);

	if (total_checked === total_length) {
		$('#download_kml').prop('disabled', !chk.is(":checked"));
	} else {
		$('#download_kml').prop('disabled', total_checked === 0);
	}
});

$(document).on('click', '#download_kml', function () {
	let radio = $("input[name=radio]:checked").val();
	let info = $("#info").is(':checked');
	let config = [];
	let option = $("input[type=radio][name=requesttype]:checked").val()

	
	$("input:checkbox[name=checkboxes]:checked").each(function(){
    	config.push({model: $(this).closest("tr").find('td:eq(1)').text(), color: $(this).closest("tr").find('.view_color').val(), radius: $(this).closest("tr").find('.radius').text()});
	});

	let data = {
		config: config,
		option: option,
		lists: table1_lists,
		info: info
	};


	$.ajax({
		type: 'POST',
		url: url+'sitetokml',
		data: data,
		async: false,
		error: function(data){
			SWAL('error', 'Oops!', 'Someting went wrong please try again.');
		},
		success: function(){
		$.fileDownload(url+'sitetokml',{
			httpMethod: 'POST',
		   	data:data,
		    successCallback:function(data){
		    }
		});
		}
	});

	console.log(data);
});


