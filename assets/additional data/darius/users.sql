-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2020 at 12:48 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gt_fortress`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `globe_id` int(10) NOT NULL COMMENT 'Company ID of User',
  `profile` varchar(255) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(32) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `lname` varchar(15) NOT NULL,
  `department` varchar(10) NOT NULL,
  `geoarea` varchar(3) NOT NULL,
  `access_level` varchar(10) NOT NULL,
  `status` varchar(11) NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT current_timestamp(),
  `active` int(2) NOT NULL COMMENT 'Active - 1, Not - 0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `globe_id`, `profile`, `username`, `password`, `fname`, `lname`, `department`, `geoarea`, `access_level`, `status`, `date_modified`, `active`) VALUES
(10, 0, '', 'admin', 'CTJQZglkU2xXbA==', 'admin', 'admin', 'RRE', 'vis', 'Admin', 'activated', '2019-09-26 08:57:19', 0),
(21, 11473, '', 'Bryan', 'CDBTcwpzWG8DOA==', 'Bryan', 'Espina', 'RRE', 'vis', 'Standard', 'activated', '2019-10-23 10:56:49', 0),
(22, 11942, '', 'Pao', 'CiBfbA5h', 'Paolo Miguel', 'Andalao', 'RRE', 'vis', 'Standard', 'activated', '2019-10-23 11:00:59', 0),
(23, 4380, '', 'aldrin', 'W2BXaVk9VHBXawg0', 'Aldrin', 'Salao', 'RRE', 'vis', 'Standard', 'activated', '2019-10-23 14:13:10', 0),
(28, 11971, '', 'daisy', 'XmBTYF43VXBWeg==', 'Daisy Rose', 'Tampus', 'RRE', 'vis', 'Standard', 'activated', '2019-10-23 15:35:23', 0),
(29, 9229, '', 'rj', 'CCBTaw==', 'Ronnel Jacob', 'Aliganga', 'RRE', 'vis', 'Standard', 'activated', '2019-10-23 15:38:44', 0),
(30, 20009305, '', 'dieno', 'XWNVbl04VGxQag==', 'Dieno', 'Canlas', 'RRE', 'vis', 'Standard', 'activated', '2019-10-23 15:39:16', 0),
(31, 10012461, '', 'joel', 'XGwHMAFuU2k=', 'Joel Arvy Nestor', 'Aragon', 'RRE', 'vis', 'Standard', 'activated', '2019-10-23 15:40:00', 0),
(32, 11491, '', 'kring', 'Xm9QcFozADhXZQ==', 'Christsha Gemelle', 'Nogra', 'RRE', 'vis', 'Standard', 'activated', '2019-10-23 15:40:50', 0),
(33, 9100, '', 'jerwin', 'DDxSZQx+ByYAPAk1', 'Jerwin', 'Garcia', 'RRE', 'vis', 'Standard', 'activated', '2019-10-23 15:41:27', 0),
(34, 10886, '', 'kirby', 'Xm9eZQh6BzNSfg==', 'Kirby Jefferson', 'Lusoc', 'RRE', 'vis', 'Standard', 'activated', '2019-10-23 15:42:05', 0),
(35, 10005438, 'http://localhost:80/fortress/pictures/96659507_2590699234515209_5001646642059804672_n.jpg', 'marco', 'Cj1QYwt5VGFXbQ==', 'Marco', 'Esguerra', 'RRE', 'vis', 'Standard', 'activated', '2019-10-23 15:42:36', 0),
(36, 10011370, '', 'john', 'WmoDPg5mAzs=', 'John Crawford', 'Habasa', 'RRE', 'vis', 'Standard', 'activated', '2019-10-23 15:43:13', 0),
(37, 11925, '', 'junell', 'Dz9XcF0zV2QHPgY4', 'Junell', 'Bertis', 'RRE', 'vis', 'Standard', 'activated', '2019-10-23 15:43:33', 0),
(38, 11933, '', 'dp', 'W2VQcg==', 'Darius Paul', 'Bacate', 'RRE', 'vis', 'Standard', 'activated', '2019-10-28 09:43:57', 0),
(39, 12215, '', 'ian', 'XW4HNF4w', 'Ian Keith', 'Talisaysay', 'RRE', 'vis', 'Standard', 'activated', '2019-10-28 09:44:34', 0),
(40, 11386, '', 'wang', 'WndWZQxiV2Y=', 'Joshua', 'Villaver', 'RRE', 'vis', 'Standard', 'activated', '2020-01-07 17:03:26', 0),
(65, 11911, '', 'mac', 'XG1XbAl7UGRecg==', 'Macmac', 'Whosder', 'RWE', 'vis', 'Standard', 'activated', '2020-05-24 18:40:01', 0),
(66, 11900, 'http://localhost:80/fortress/pictures/viber_image_2020-05-14_13-38-18.jpg', 'Gerson', 'ATwCNQFzV3JXbQM/', 'Gerson', 'Gerson', 'RFE', 'vis', 'Standard', 'activated', '2020-05-27 22:32:09', 0),
(68, 1111, '', 'Vernon', 'DiJRZgByVm4EPlVp', 'Vernon', 'Vernon', 'RGPM', 'vis', 'Standard', 'activated', '2020-06-08 10:33:09', 0),
(69, 100000, '', 'fred', 'ADxefl47UmA=', 'Fredderson', 'Go', 'RCNE', 'vis', 'Standard', 'activated', '2020-06-21 12:30:36', 0),
(72, 1234567, '', 'juno', 'XGxUc1wyVm8=', 'Juno', 'Cedeno', '', 'vis', 'Guest', 'activated', '2020-06-24 01:35:23', 0),
(79, 11944, '', 'abner', 'XWZUZA5gWGsDJA==', 'Abner', 'Cañas', 'RRE', 'vis', 'Standard', 'activated', '2020-07-05 14:26:02', 0),
(80, 1700, '', 'Julyenda', 'W0sDJAxgWXYAMAI+AjNQYw==', 'Julyenda', 'Sandil', 'RRE', 'vis', 'Standard', 'activated', '2020-07-12 17:50:54', 0),
(81, 1009383, '', 'Henielle', 'XkxXYA1jUW5QYFFvVG1QZw==', 'Henielle Joelle Kay', 'San Juan', 'RFE', 'vis', 'Standard', 'activated', '2020-07-12 17:51:30', 0),
(82, 1870, '', 'Samuel', 'DwZVZlo3AiFWZgI8', 'Samuel', 'Galapon', 'RFE', 'vis', 'Standard', 'activated', '2020-07-12 17:52:34', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
