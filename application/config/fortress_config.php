<?php
$config['site_name'] = 'Fortess';
$config['site_version'] = 'v1.0.0';
$config['site_author'] = 'Team Fortress';
$config['site_department'] = 'AEPM | RRE Visayas';
$config['site_reserved'] = 'All Rights Reserved 2020 ©';

$config['sliders'] = array('gf2', 'rt', 'sc3', 'pole', 'sc4', 'pole1', 'sc', 'pole2', 'sc1');

$config['css'] = array("global",
	                    "sidebar",
						"bootstrap.min",
						"datepicker",
						"bootstrap4-toggle.min",
						"responsive.bootstrap4.min",
						"fullcalendar.min",
						"chart",
						"jquery-ui",
						"progress-tracker",
						"offline-theme-chrome",
						"offline-language-english.min",
						"fontawesome-free-5.11.2-web/css/all",
						"dropzone.min",
						"select2.min",
						"dataTables.min",
						"jquery.dataTables.yadcf",
						"select2-bootstrap",
						"select2-bootstrap.min",
						"buttons.bootstrap4.min",
						"bootstrap-multiselect",
						"tree_maker"
					  );

$config['scripts'] = array(
							// "jquery.min",	
							"jquery-3.5.1",
	  					    "popper",
							// "site",
							"bootstrap-datepicker",
							// "popper.min",
							"bootstrap.bundle.min",
							"bootstrap4-toggle.min",
							"moment",
//							"socket.io",
							// "progressbar",
							"fullcalendar.min",
							// "jquery.min",
							// "jquery.md5.min",
							"Chart.bundle.min",
							"chartjs-plugin-datalabels",
							// "jquery.form",
							"sweetalert",
							"lodash",
							"cookies",
							"offline.min",
							"jsdownload",
							"sidebar",
							// "modernizr",
							"JSONToCSVConvertor",
							"waypoints",
							"jquery.countup",
							// "xlsx.full.min",
							"dropzone.min",
							"dataTables.min",
							"select2.min",
							"jquery.dataTables.yadcf",
							// "FileSaver.min",
							// "tableexport.min",
							// "pdfmake.min",
							// "vfs_fonts",
							// "moment.min",
							// "jszip.min",
							"jquery.lazy.min",
							"buttons.bootstrap4.min",
							"dataTables.buttons.min",
							"buttons.html5.min",
							"bootstrap-multiselect",
							"tree_maker"
                          );

$config['actions'] = array("global",
//	"socket"
);

$config['2g-1-report'] = array(
	array(
		'title' => '2G CSFR',
		'name' => 'csfr',
	),
	array(
		'title' => '2G DCR',
		'name' => 'dcr',
	),
	array(
		'title' => '2G IAFR',
		'name' => 'iafr',
	),
);

$config['2g-2-report'] = array(
	array(
		'title' => 'TCH Availability',
		'name' => 'tch',
	),
	array(
		'title' => 'Channel Blocking',
		'name' => 'cb',
	),
	array(
		'title' => 'Traffic',
		'name' => 'trf',
	),
	array(
		'title' => 'Incoming Hand Over',
		'name' => 'iho',
	),
	array(
		'title' => 'Outgoing Hand Over',
		'name' => 'oho',
	),
	array(
		'title' => 'DL PDCH Blocking',
		'name' => 'dlb',
	),
	array(
		'title' => 'UL PDCH Blocking',
		'name' => 'ulb',
	),
	array(
		'title' => 'SD Interference',
		'name' => 'sd',
	),
	array(
		'title' => 'TCHHR Interference',
		'name' => 'tchhr',
	),
	array(
		'title' => 'TCHFR Interference',
		'name' => 'tchfr',
	),
	array(
		'title' => 'Timing Advance',
		'name' => 'ta',
	),
	array(
		'title' => 'SD Counters',
		'name' => 'sc',
	),
	array(
		'title' => 'TCH Call Drop Counters',
		'name' => 'tcdc',
	),
);

$config['3g-1-report'] = array(
	array(
		'title' => '3G Voice CSFR',
		'name' => 'voice_csfr',
	),
	array(
		'title' => 'HS CSFR',
		'name' => 'hs_csfr',
	),
	array(
		'title' => '3G Voice DCR',
		'name' => 'voice_dcr',
	),
	array(
		'title' => 'HS DCR',
		'name' => 'hs_dcr',
	),
);

$config['3g-2-report'] = array(
	array(
		'title' => 'Availability',
		'name' => 'avail',
	),
	array(
		'title' => 'AMR Traffic',
		'name' => 'amr_trf',
	),
	array(
		'title' => 'Cell DCH Users',
		'name' => 'dch',
	),
	array(
		'title' => 'Propagation Delay',
		'name' => 'pd',
	),
	array(
		'title' => 'RTWP',
		'name' => 'rtwp',
	),
	array(
		'title' => 'SMS FR',
		'name' => 'sms',
	),
	array(
		'title' => 'RRC Rejection',
		'name' => 'rrj',
	),
	array(
		'title' => 'RRC Establishment Failure',
		'name' => 'rre',
	),
	array(
		'title' => 'RRC Rejection Counters',
		'name' => 'rrc',
	),
	array(
		'title' => 'RAB CS Establishment Failure',
		'name' => 'rce',
	),
	array(
		'title' => 'RAB PS Establishment Failure',
		'name' => 'rpe',
	),
	array(
		'title' => 'RAB CS Abnormal Release',
		'name' => 'rca',
	),
	array(
		'title' => 'RAB PS Abnormal Release',
		'name' => 'rpa',
	),
	array(
		'title' => 'HSPDA RAB PS Abnormal Release',
		'name' => 'hsrpa',
	),
	array(
		'title' => 'Data Traffic Volume',
		'name' => 'dtv',
	),
	array(
		'title' => 'Soft Handover',
		'name' => 'sh',
	),
	array(
		'title' => 'ISHO',
		'name' => 'isho',
	),
);

$config['5g-tabs'] = array(
	array(
		'title' => 'Addition Success Rate',
		'name' => 'sgnb_sr',
	),
	array(
		'title' => 'Abnormal Release Rate Ratio',
		'name' => 'sgnb_rd',
	),
	array(
		'title' => 'Abnormal Release Overall',
		'name' => 'sgnb_ro',
	),
);

$config['4g-report'] = array(
	array(
		'title' => 'Availability',
		'name' => 'sa',
	),
	array(
		'title' => 'PS Service Drop Rate',
		'name' => 'psdrr',
	),
	array(
		'title' => 'RRC Set-up Success Rate',
		'name' => 'rrssr',
	),
	array(
		'title' => 'E-Rab Set-up Success Rate',
		'name' => 'erssr',
	),
	array(
		'title' => 'S1 Connectivity',
		'name' => 's1',
	),
	array(
		'title' => 'Prb Utilization',
		'name' => 'put',
	),
	array(
		'title' => 'UL Interference',
		'name' => 'ulit',
	),
	array(
		'title' => 'Timing Advance',
		'name' => 'tad',
	),
	array(
		'title' => 'CS Fallback',
		'name' => 'csf',
	),
	array(
		'title' => 'Hand Over Success Rate',
		'name' => 'hosr',
	),
	array(
		'title' => 'Connected and Active Users',
		'name' => 'cvu',
	),
	array(
		'title' => 'Traffic Volume',
		'name' => 'trv',
	),
	array(
		'title' => 'User Throughput',
		'name' => 'uth',
	),
	array(
		'title' => 'Cell Throughput',
		'name' => 'cth',
	),
	array(
		'title' => 'RRC Set-up Failure Rootcause',
		'name' => 'rcfr',
	),
	array(
		'title' => 'E-Rab Establishment Rootcause',
		'name' => 'erer',
	),
	array(
		'title' => 'E-Rab Abnormal Release Rootcause',
		'name' => 'erar',
	),
);

$config['5g-1-report'] = array(
	array(
		'title' => 'Time Unavailable(s)',
		'name' => 'tua',
	),
	array(
		'title' => 'Average Users',
		'name' => 'au',
	),
	array(
		'title' => 'Accessibility',
		'name' => 'acc',
	),
	array(
		'title' => 'Retainability',
		'name' => 'ret',
	),
	array(
		'title' => 'UL Interference',
		'name' => 'ulf',
	),
	array(
		'title' => 'Timing Advance',
		'name' => 'tia',
	),
	array(
		'title' => 'Traffic Volume (Gbps)',
		'name' => 'tv',
	),
	array(
		'title' => 'PRB Utilization',
		'name' => 'prb',
	),
	array(
		'title' => 'Cell Throughput (Mbps)',
		'name' => 'ct',
	),
	array(
		'title' => 'User Throughput (Mbps)',
		'name' => 'ut',
	),
	array(
		'title' => 'Sgnb Failure Rootcause',
		'name' => 'sfr',
	),
	array(
		'title' => 'Sgnb Radio Failure Rootcause',
		'name' => 'srfr',
	),
	array(
		'title' => 'Abnormal Release Rootcause',
		'name' => 'arr',
	),
	array(
		'title' => 'Abnormal Release Radio Rootcause',
		'name' => 'arrr',
	),
	array(
		'title' => 'Mobility',
		'name' => 'mob',
	),
);

$config['rfe-type'] = array(
	array(
		'id'         => 1,
		'name'       => 'Tower',
		'value' 	 => 0,
		'icon'       => 'fas fa-broadcast-tower',
		'color'      => 'bg-danger',
	),
	array(
		'id'         => 2,
		'name'       => 'Power',
		'value' 	 => 0,
		'icon'       => 'fas fa-power-off',
		'color'      => 'bg-success',
	),
);

$config['tower-dash'] = array(
	array(
		'id'         => 1,
		'name'       => 'rt',
		'title'		 => 'Rooftop',
		'type'       => 1,
	),
	array(
		'id'         => 1,
		'name'       => 'gf',
		'title'		 => 'Greenfield',
		'type'       => 1,
	),
	array(
		'id'         => 1,
		'name'       => 'nscp',
		'title'		 => 'NSCP7 Compliance',
		'type'       => 1,
	),
);

$config['power-dash'] = array(
	array(
		'id'         => 1,
		'name'       => 'SC',
		'title'		 => 'Site Class',
		'type'       => 2
	),
	array(
		'id'         => 1,
		'name'       => 'ac',
		'title'		 => 'AC Capacity',
		'type'       => 2
	),
	array(
		'id'         => 1,
		'name'       => 'REC',
		'title'      => 'DC Capacity',
		'type'       => 2
 	),
	array(
		'id'         => 1,
		'name'       => 'RS',
		'title'      => 'BBUT',
		'type'       => 2
	),
	// array(
	// 	'id'         => 1,
	// 	'name'       => 'pcn',
	// 	'title'      => 'Power Connection',
	// 	'type'       => 2
	// ),
);



?>
