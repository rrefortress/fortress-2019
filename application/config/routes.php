<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'main/loginpage';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//Set Session / Destroy Session
$route['sign_in'] = 'auth/sign_in';
$route['social_login'] = 'auth/social_login';
$route['sign_out'] = 'auth/sign_out';
$route['login'] = 'main/loginpage';

//Page navigation
$route['home'] = 'main/home';
$route['sections'] = 'main/sections';
$route['main_dash_counts'] = 'main/main_dash_counts';
$route['dash_counts'] = 'main/dash_counts';

$route['dashboard'] = 'main/dashboard';
$route['sites'] = 'main/sitespage';
$route['add_site'] = 'main/addsitepage';
$route['site_profile'] = 'main/siteprofilepage';

$route['users'] = 'main/usermanagepage';
$route['file_upload'] = 'main/fileuploadpage';
$route['restrictedPage'] = 'main/restrictedPage';


$route['mdb'] = 'main/mdbupdatespage';
$route['projectmanagepage'] = 'main/projectmanagepage';
$route['accounts'] = 'main/useraccountpage';
#ABOUT
$route['about'] = 'main/aboutpage';
#RECORDS
$route['records'] = 'main/recordspage';


//Admin actions
$route['adduser'] = 'admin/adduser';
$route['getusers'] = 'admin/getusers';
$route['deleteuser'] = 'admin/deleteuser';
$route['fetchuser'] = 'admin/fetchuser';
$route['edituser'] = 'admin/edituser';
$route['getadminsitelist'] = 'admin/getadminsitelist';
$route['get_all_menus'] = 'Settings/get_all_menus';

//Site action
$route['fileupload'] = 'site/fileupload';
$route['udb_upload'] = 'FileUpload/udb_upload';
$route['save_upload_logs'] = 'FileUpload/save_upload_logs';
$route['truncate_data'] = 'FileUpload/truncate_data';
$route['getsearch'] = 'site/getsearch';
$route['getalltowers'] = 'site/getalltowers';
$route['insertsite'] = 'site/insertsite';
$route['inserttodb'] = 'site/inserttodb';
$route['getsite'] = 'site/getsite';
$route['insertsinglemdbupdate'] = 'site/insertsinglemdbupdate';
$route['insertmdbupdate'] = 'site/insertmdbupdate';
$route['getmdbupdate'] = 'site/getmdbupdate';
$route['getquery/(:any)'] = 'site/getquery/$1';
$route['loadannex/(:any)'] = 'site/loadannex/$1';
$route['generatesitename'] = 'site/generatesitename';
$route['getnumber'] = 'site/getnumber';
$route['generatecellid/(:any)'] = 'site/generatecellid/$1';
$route['getneighbors/(:any)/(:any)'] = 'site/getNeighbors/$1/$2';
$route['generatepsc'] = 'site/generatepsc';
$route['generatedatatable'] = 'site/generatedatatable';
$route['checkduplicate'] = 'site/checkduplicate';
$route['getlastdbid'] = 'site/getlastdbid';
$route['insertsingleapproval'] = 'site/insertsingleapproval';
$route['insertsingletodb'] = 'site/insertsingletodb';
$route['deletesinglefromdb'] = 'site/deletesinglefromdb';
$route['deletesitefromdb'] = 'site/deletesitefromdb';
$route['dbadjust'] = 'site/dbadjust';
$route['edit'] = 'site/edit';
$route['delete'] = 'site/delete';
$route['getSection'] = 'site/getSection';
$route['locateCoord'] = 'site/locateCoord';


//User action
$route['changepassword'] = 'user/changepassword';
$route['fileTrackUpload'] = 'FileTrack/fileTrackUpload';
$route['updateUser'] = 'user/updateUser';

#File Tracking
$route['file_tracker'] = 'FileTrack/file_tracking';
$route['set_calendar'] = 'FileTrack/set_calendar';
$route['getTrackUsers'] = 'FileTrack/getTrackUsers';
$route['get_tracks'] = 'FileTrack/get_tracks';
$route['get_IBS_Track'] = 'FileTrack/get_IBS_Track';
$route['get_Surveys_Track'] = 'FileTrack/get_Surveys_Track';
$route['get_Surveys_Track_name'] = 'FileTrack/get_Surveys_Track_name';

$route['get_Nominations_Track'] = 'FileTrack/get_Nominations_Track';
$route['save_nomination_track'] = 'FileTrack/save_nomination_track';
$route['nom_upload'] = 'FileTrack/nom_upload';
$route['nomSearch'] = 'FileTrack/nomSearch';
$route['del_nom_track'] = 'FileTrack/del_nom_track';

$route['get_AWE_Track'] = 'FileTrack/get_AWE_Track';
$route['save_awe_track'] = 'FileTrack/save_awe_track';
$route['awe_upload'] = 'FileTrack/awe_upload';
$route['aweSearch'] = 'FileTrack/aweSearch';
$route['del_awe_track'] = 'FileTrack/del_awe_track';

$route['get_PMO_TRACK'] = 'FileTrack/get_PMO_TRACK';
$route['save_pmo_track'] = 'FileTrack/save_pmo_track';
$route['pmo_upload'] = 'FileTrack/pmo_upload';
$route['pmoSearch'] = 'FileTrack/pmoSearch';
$route['del_pmo_track'] = 'FileTrack/del_pmo_track';

$route['survey_upload'] = 'FileTrack/survey_upload';
$route['surveySearch'] = 'FileTrack/surveySearch';
$route['del_survey_track'] = 'FileTrack/del_survey_track';
$route['save_survey_track'] = 'FileTrack/save_survey_track';

$route['get_TSSR_Track'] = 'FileTrack/get_TSSR_Track';
$route['tssr_upload'] = 'FileTrack/tssr_upload';
$route['save_tssr_track'] = 'FileTrack/save_tssr_track';
$route['tssrSearch'] = 'FileTrack/tssrSearch';
$route['del_tssr_track'] = 'FileTrack/del_tssr_track';

$route['ibs_upload'] = 'FileTrack/ibs_upload';
$route['save_ibs_track'] = 'FileTrack/save_ibs_track';
$route['ibsSearch'] = 'FileTrack/ibsSearch';
$route['del_ibs_track'] = 'FileTrack/del_ibs_track';

//Map
$route['sitetokml'] = 'MapKML/sitetokml';
$route['getsitelist'] = 'MapKML/getsitelist';
$route['mapkml'] = 'Main/mapKmlTool';

//ADMIN
$route['getActivites'] = 'Admin/getActivites';
$route['getUsers'] = 'User/getUsers';
$route['delete_user'] = 'User/delete_user';
$route['reset_password'] = 'User/reset_password';
$route['get_coordinates'] = 'Main/get_coordinates';

//Inventory
$route['inventories'] = 'Inventory/inventory_page';
$route['save_item'] = 'Inventory/save_item';
$route['get_items'] = 'Inventory/get_items';
$route['del_items'] = 'Inventory/del_items';
$route['save_to_cart'] = 'Inventory/save_to_cart';
$route['get_item_cart'] = 'Inventory/get_item_cart';
$route['save_to_requests'] = 'Inventory/save_to_requests';
$route['get_requested_lists'] = 'Inventory/get_requested_lists';
$route['get_requested_item'] = 'Inventory/get_requested_item';
$route['accept_request'] = 'Inventory/accept_request';
$route['return_item'] = 'Inventory/return_item';
$route['get_users'] = 'Inventory/get_users';
$route['remove_cart_lists'] = 'Inventory/remove_cart_lists';
$route['reject_request'] = 'Inventory/reject_request';

//Dashboard
$route['get_dashboard'] = 'Admin/get_dashboard';
$route['get_logs'] = 'Admin/get_logs';
$route['map_kml'] = 'Admin/map_kml';

//Mile Stone
$route['mile_stone_page'] = 'MileStone/mile_stone_page';
$route['get_site_names'] = 'MileStone/get_site_names';
$route['search_site_track'] = 'MileStone/search_site_track';
//Nom Page
$route['nom_page'] = 'MileStone/nom_page';
//CLP
$route['clp'] = 'CLP/clp_page';
$route['get_CID'] = 'CLP/get_CID';
$route['clp_upload'] = 'CLP/clp_upload';

//SETTTINGS
$route['settings'] = 'Settings/settings_page';
$route['new_menu'] = 'Settings/new_menu';
$route['del_menu'] = 'Settings/del_menu';
$route['sub_menu'] = 'Settings/sub_menu';

#ULTIMATE DB
$route['ultimate_db'] = 'main/ultimate_db';
$route['search_keys'] = 'Ultimate_DB/search_keys';
$route['search_data'] = 'Ultimate_DB/search_data';


$route['back_up']     = 'main/back_up';
$route['get_datas']   = 'Admin/get_datas';
$route['download_db'] = 'Admin/download_db';
$route['backup_db']   = 'Admin/backup_db';
$route['delete_db']   = 'Admin/delete_db';

$route['SendController'] = 'SendController';
$route['message/send']   = 'SendController/send';

#RFE Automation
$route['power']   = 'main/power';
$route['tower']   = 'main/tower';
$route['assessment']   = 'main/assessment';
$route['assessment']   = 'main/assessment';
$route['insert_site'] = 'Power/insert_site';
$route['get_ac_load'] = 'Power/get_ac_load';
$route['get_db_acload'] = 'Power/get_db_acload';
$route['proposed'] = 'Power/proposed';
$route['get_power'] = 'Power/get_power';
$route['get_tower'] = 'Power/get_tower';
$route['get_chart'] = 'Power/get_chart';
$route['power_upload'] = 'Power/power_upload';
$route['add_powerdb'] = 'Power/add_powerdb';
$route['export_power'] = 'Power/export_power';
$route['export_tower'] = 'Tower/export_tower';
$route['get_rec'] = 'Power/get_rec';
$route['get_equip'] = 'Power/get_equip';
$route['del_equip'] = 'Power/del_equip';
$route['del_rec'] = 'Power/del_rec';
$route['get_bat'] = 'Power/get_bat';
$route['add_rec'] = 'Power/add_rec';
$route['del_rec'] = 'Power/del_rec';
$route['add_bat'] = 'Power/add_bat';
$route['add_equip'] = 'Power/add_equip';
$route['del_bat'] = 'Power/del_bat';
$route['get_drop_wires'] = 'Power/get_drop_wires';
$route['get_rec_bat'] = 'Power/get_rec_bat';
$route['proposal_save_mdb'] = 'Power/proposal_save_mdb';
$route['get_prop_list'] = 'Power/get_prop_list';
$route['get_proposals'] = 'Power/get_proposals';
$route['get_acu_model'] = 'Power/get_acu_model';
$route['del_prop'] = 'Power/del_prop';
$route['del_all_prop'] = 'Power/del_all_prop';
$route['edit_prop'] = 'Power/edit_prop';
$route['get_all_db_dropdowns'] = 'Power/get_all_db_dropdowns';

$route['get_power_completion'] = 'Power/get_power_completion';
$route['get_tower_completion'] = 'Tower/get_tower_completion';

$route['get_pchart_completion'] = 'Power/get_pchart_completion';
$route['get_tchart_completion'] = 'Tower/get_tchart_completion';

$route['tower_upload'] = 'Tower/tower_upload';
$route['get_tower_list'] = 'Tower/get_tower_list';
$route['get_antenna_epa_table'] = 'Tower/get_antenna_epa_table';
$route['get_tower_epa_table'] = 'Tower/get_tower_epa_table';
$route['save_tower_proposal'] = 'Tower/save_proposal';
$route['get_tower_proposal'] = 'Tower/get_proposal';
$route['get_tower_proposals_list'] = 'Tower/get_proposals_list';
$route['get_assessments'] = 'Power/get_assessments';
$route['get_power_reports'] = 'Power/get_power_reports';
$route['get_tower_reports'] = 'Tower/get_tower_reports';
$route['get_power_profile'] = 'Power/get_power_profile';
$route['get_tower_profile'] = 'Tower/get_tower_profile';
#ACCEPTANCE
$route['bluebook'] = 'main/bluebook';
$route['get_main'] = 'Bluebook/get_main';
// $route['get_files'] = 'Bluebook/get_files';
$route['get_categories'] = 'Bluebook/get_categories';
// $route['remove_file'] = 'Bluebook/remove_file';
// $route['edit_filename'] = 'Bluebook/edit_filename';
// $route['get_region'] = 'Bluebook/get_region';
#REPO
$route['repo'] = 'main/repo';
$route['get_region_count'] = 'Repo/get_region_count';
$route['get_sites'] = 'Repo/get_sites';
$route['get_content'] = 'Repo/get_content';
$route['delete_content'] = 'Repo/delete_content';
$route['get_file_count'] = 'Repo/get_file_count';
$route['get_repo_chart'] = 'Repo/get_repo_chart';

#PLANNING
$route['planning'] = 'main/planning';
$route['p2g'] = 'Planning/p2g';
$route['p3g'] = 'Planning/p3g';
$route['p4g'] = 'Planning/p4g';
$route['p5g'] = 'Planning/p5g';
$route['get_field_keys'] = 'Planning/get_field_keys';
$route['pl_remove'] = 'Planning/pl_remove';

#TRANSPORT
$route['transport'] = 'main/transport';
$route['trans_upload'] = 'Transport/trans_upload';
$route['get_trans_fkeys'] = 'Transport/get_trans_fkeys';
$route['trans_remove'] = 'Transport/trans_remove';


#RGPM
$route['dashboard'] = 'Main/dashboard';
$route['kpi'] = 'Main/kpi';
$route['macro'] = 'Main/macro';
$route['ibs'] = 'Main/ibs';
$route['analyzer'] = 'Main/analyzer';
$route['get_kpi'] = 'KPI/get_kpi';
#2G KPI
$route['get_2g_kpi'] = 'KPI/get_2g_kpi';
$route['get_2g_csfr'] = 'KPI/get_2g_csfr';
$route['get_2g_dcr'] = 'KPI/get_2g_dcr';
$route['get_2g_iafr'] = 'KPI/get_2g_iafr';
$route['clear_2g'] = 'KPI/clear_2g';
#3G KPI
$route['get_3g_kpi'] = 'KPI/get_3g_kpi';
$route['get_voice_csfr'] = 'KPI/get_voice_csfr';
$route['get_voice_dcr'] = 'KPI/get_voice_dcr';
$route['get_voice_sms'] = 'KPI/get_voice_sms';
$route['get_hsdpa_csfr'] = 'KPI/get_hsdpa_csfr';
$route['get_hsdpa_dcr'] = 'KPI/get_hsdpa_dcr';
$route['clear_3g'] = 'KPI/clear_3g';
#4G KPI
$route['get_4g_kpi'] = 'KPI/get_4g_kpi';
$route['get_erab'] = 'KPI/get_erab';
$route['get_rrc'] = 'KPI/get_rrc';
$route['get_intra'] = 'KPI/get_intra';
$route['get_4g_dcr'] = 'KPI/get_4g_dcr';
$route['get_2g_calendar'] = 'KPI/get_2g_calendar';
$route['get_3g_calendar'] = 'KPI/get_3g_calendar';
$route['get_4g_calendar'] = 'KPI/get_4g_calendar';
$route['clear_4g'] = 'KPI/clear_4g';
#VOLTE
$route['get_volte_kpi'] = 'KPI/get_volte_kpi';
$route['get_volte_estab'] = 'KPI/get_volte_estab';
$route['get_volte_erab'] = 'KPI/get_volte_erab';
$route['get_volte_sdvcc'] = 'KPI/get_volte_sdvcc';
$route['get_volte_dcr'] = 'KPI/get_volte_dcr';
$route['clear_volte'] = 'KPI/clear_volte';
#5G KPI
$route['get_5g_kpi'] = 'KPI/get_5g_kpi';
$route['clear_5g'] = 'KPI/clear_5g';
$route['get_sgnb_sr'] = 'KPI/get_sgnb_sr';
$route['get_sgnb_rd'] = 'KPI/get_sgnb_rd';
$route['get_sgnb_ro'] = 'KPI/get_sgnb_ro';

#KPI REPORT CHART
$route['get_filter_menus'] = 'KPI/get_filter_menus';
//$route['get_3g_filter_menus'] = 'KPI/get_3g_analyzer_menus';
$route['kpi_chart_2g_upload'] = 'KPI/kpi_chart_2g_upload';
$route['kpi_chart_3g_upload'] = 'KPI/kpi_chart_3g_upload';
$route['kpi_chart_4g_upload'] = 'KPI/kpi_chart_4g_upload';
$route['kpi_chart_volte_upload'] = 'KPI/kpi_chart_volte_upload';
$route['kpi_chart_5g_upload'] = 'KPI/kpi_chart_5g_upload';
#GRAPH
$route['get_2g_graph'] = 'KPI/get_2g_graph';
$route['get_3g_graph'] = 'KPI/get_3g_graph';
$route['get_4g_graph'] = 'KPI/get_4g_graph';
$route['get_5g_graph'] = 'KPI/get_5g_graph';

#RIGGER FORM
$route['rigger'] = 'Main/rigger';
$route['audit'] = 'Main/audit';

$route['save_audit'] = 'Audit/save_audit';
$route['get_audit'] = 'Audit/get_audit';
$route['export_audit'] = 'Audit/export_audit';
$route['del_audit'] = 'Audit/del_audit';
#SSV
$route['get_ssv'] = 'SSV/get_ssv';
$route['ssv_upload'] = 'SSV/ssv_upload';
$route['ssv_callouts'] = 'SSV/ssv_callouts';
$route['get_callouts'] = 'SSV/get_callouts';
$route['export_ssv'] = 'SSV/export_ssv';
#WIRELINE
$route['wireline'] = 'main/wireline';
$route['get_wire_fkeys'] = 'Wireline/get_wire_fkeys';
$route['get_wireline'] = 'Wireline/get_wireline';
$route['wireline_upload'] = 'Wireline/wireline_upload';
$route['wire_remove'] = 'Wireline/wire_remove';
