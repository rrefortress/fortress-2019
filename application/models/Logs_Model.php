<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs_Model extends CI_Model
{

	public function set_config($total = 0, $func_name = '', $row_page = 0)
	{
		$config['base_url'] = base_url($func_name);
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $total;
		$config['per_page'] = $row_page;

		$config['full_tag_open'] = '<center><div class="padding"><nav><ul class="pagination text-center paginate_track" style="justify-content: center;">';
		$config['full_tag_close'] = '</ul></nav></div>';
		$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['next_tag_close'] = '<span aria-hidden="true"></span></span></li>';
		$config['prev_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['prev_tag_close'] = '</span></li>';
		$config['first_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['first_tag_close'] = '</span></li>';
		$config['last_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['last_tag_close'] = '</span></li></center>';

		// Initialize
		$this->pagination->initialize($config);
		return $config;
	}

	public function save_logs($datas, $action)
	{
		$data = array(
			'user_id' => $datas['id'],
			'action' => $action,
			'date' => date('Y-m-d H:i:s'),
		);

		if ($datas['access_level'] !== 'Admin') {
			$this->db->insert('logs', $data);
			$id = $this->db->insert_id();
			return $this->get_prepend_logs($id);
		}
	}

	public function get_prepend_logs($id = 0) {
		$this->db->select("logs.*, users.*");
		$this->db->from('logs');
		$this->db->join('users', 'logs.user_id = users.id');
		$this->db->where('logs.id', $id);
		$query = $this->db->get()->row();

		$arr = array(
			'id' => $query->id,
			'profile' => empty($query->profile) ? base_url('pictures/default.jpg') : $query->profile,
			'action' => $query->action,
			'ago' => $this->timestamp($query->date),
			'name' => $query->fname . ' ' . $query->lname,
			'department' => empty($query->department) ? $query->access_level : $query->department,
			'geoarea' => strtoupper($query->geoarea)
		);
		return $arr;
	}

	public function read_logs($data = array(), $row_num = 0)
	{
		$row_page = $this->input->get('row_page');

		$arr = array();
		$res = $this->db->get('logs');

		if ($row_num != 0) {
			$row_num = ($row_num - 1) * $row_page;
		}

		$this->set_config($res->num_rows(), $func_name = 'get_logs', $row_page);

		$query = $this->get_details($data, $row_num, $row_page);
		foreach ($query as $row) {
			$arr[] = array(
				'id' => $row->id,
				'profile' => empty($row->profile) ? base_url('assets/pictures/default.jpg') : $row->profile,
				'action' => $row->action,
				'ago' => $this->timestamp($row->date),
				'name' => $row->fname . ' ' . $row->lname,
				'department' => empty($row->department) ? $row->access_level : $row->department,
				'geoarea' => strtoupper($row->geoarea)
			);
		}

		return $arrays = array(
			'data' => $arr,
			'pagination' => $this->pagination->create_links(),
			'row_num' => $row_num
		);
	}

	public function get_details($data = array(), $row_num = 0, $row_page = 0)
	{
		$this->db->select("logs.*, users.*");
		$this->db->from('logs');
		$this->db->join('users', 'logs.user_id = users.id');
		if (!empty($data['date'])) {
			$this->db->like("logs.date", !empty($data['date']) ? $data['date'] : '');
		} else {
			$this->db->like("logs.action", !empty($data['search']) ? $data['search'] : '');
			$this->db->or_like("users.fname", !empty($data['search']) ? $data['search'] : '');
			$this->db->or_like("users.lname", !empty($data['search']) ? $data['search'] : '');
			$this->db->or_like("users.geoarea", !empty($data['search']) ? $data['search'] : '');
			$this->db->or_like("users.department", !empty($data['search']) ? $data['search'] : '');
			$this->db->order_by("date", "desc");
		}

		$this->db->limit($row_page, $row_num);
		$query = $this->db->get();
		return $query->result();
	}

	public function timestamp($date)
	{
		$this->load->helper('date');

		// Declare timestamps
		$last = new DateTime($date);
		$now = new DateTime(date('d-m-Y H:i:s', time()));

		// Find difference
		$interval = $last->diff($now);
		$years = (int)$interval->format('%Y');
		$months = (int)$interval->format('%m');
		$days = (int)$interval->format('%d');
		$hours = (int)$interval->format('%H');
		$minutes = (int)$interval->format('%i');

		if ($years > 0) {
			return $years . ' Year' .($years > 1 ? 's ' : ' ') . $months . ' Month' .($months > 1 ? 's ' : ' '). $days . ' Day'.($days>1 ? 's ' : ' ') . $hours . ' Hour'.($hours > 1 ? 's ' : ' ') . $minutes . ' minute'.($minutes > 1 ? 's' : '').' ago';
		} else if ($months > 0) {
			return $months . ' Month' .($months > 1 ? 's ' : ' ') . $days . ' Day'.($days>1 ? 's ' : ' ') . $hours . ' Hour'.($hours > 1 ? 's ' : ' ') . $minutes . ' minute'.($minutes > 1 ? 's' : '').' ago';
		} else if ($days > 0) {
			return $days . ' Day'.($days>1 ? 's ' : ' ') . $hours . ' Hour'.($hours > 1 ? 's ' : ' ') . $minutes . ' minute'.($minutes > 1 ? 's' : '').' ago';
		} else if ($hours > 0) {
			return $hours . ' Hour'.($hours > 1 ? 's ' : ' ') . $minutes . ' minute'.($minutes > 1 ? 's' : '').' ago';
		} else {
			return $minutes == 0 ? ' Now' : $minutes . ' minute'.($minutes > 1 ? 's' : '').' ago';
		}
	}
}
