<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SSV_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->geoarea = $this->session->userdata('geoarea');
		$this->tbl = "ssv_vismin_mdb";
		$this->filter = $this->input->get('region');
	}

	#GET EXPANSION CHART
	private function region_expansion_query() {
		$this->db->where('(PROGRAM = "Macro" 
							OR PROGRAM = "Mobile Expansion" 
							OR PROGRAM = "OWO" 
							OR PROGRAM = "Small Cell"
							OR PROGRAM = "SRW"
							OR PROGRAM = "SRW (TowerCo - GAS)"
							OR PROGRAM = "TowerCo (Macro) - GAS"
							OR PROGRAM = "TowerCo (Macro)"
							OR PROGRAM = "TowerCo (Macro) - BTS")'
						);
		$this->db->select(
						  'COUNT(PD_TRFS) as PD_TRFS,
						   COUNT(SSV_SUBMIT_DATE) as SSV_SUBMIT_DATE, 
						   COUNT(SSV_READY) as SSV_READY, 
						   COUNT(SSV_APPROVED_DATE) as SSV_APPROVED_DATE');

		if ($this->filter <> 'all') {$this->db->where('REGION', strtoupper($this->filter));}
		$this->db->from($this->tbl);

		return $this->db->get()->row();
	}

	#GET NEW SITE CHART
	private function region_new_site_query() {
		$this->db->where('(PROGRAM = "3G to 4G Migration" 
							OR PROGRAM = "5G" 
							OR PROGRAM = "5RFM"
							OR PROGRAM = "Enabler"
							OR PROGRAM = "WTTx")'
						);
		$this->db->select('COUNT(PD_TRFS) as PD_TRFS,
						   COUNT(SSV_SUBMIT_DATE) as SSV_SUBMIT_DATE, 
						   COUNT(SSV_READY) as SSV_READY, 
						   COUNT(SSV_APPROVED_DATE) as SSV_APPROVED_DATE');
		// $this->db->from($this->tbl);
		if ($this->filter <> 'all') {$this->db->where('REGION', strtoupper($this->filter));}
		$this->db->from($this->tbl);
		return $this->db->get()->row();
	}

	public function get_ssv()
	{
		$arr = array();

		#BY REGION QUERY
		$expansion = $this->region_expansion_query();

		#GET NEW SITE CHART
		$new_site = $this->region_new_site_query();

		//------------------------------------------------------------//
		#VENDOR VIEW -> EXPANSION -> AMDOCS
		$expan_amdocs = $this->vendor_expansion_query('AMDOCS');
		#VENDOR VIEW -> EXPANSION -> HT
		$expan_ht = $this->vendor_expansion_query('HT');
		#VENDOR VIEW -> EXPANSION -> HT
		$expan_nsb = $this->vendor_expansion_query('NOKIA');
		#VENDOR VIEW -> EXPANSION -> HT
		$expan_gt = $this->vendor_expansion_query('GT');
		//------------------------------------------------------------//
		#VENDOR VIEW -> NEW SITE -> AMDOCS
		$new_amdocs = $this->vendor_newsite_query('AMDOCS');	
		#VENDOR VIEW -> NEW SITE -> HT
		$new_ht = $this->vendor_newsite_query('HT');
		#VENDOR VIEW -> NEW SITE -> HT
		$new_nsb = $this->vendor_newsite_query('NOKIA');	
		#VENDOR VIEW -> NEW SITE -> GT
		$new_gt = $this->vendor_expansion_query('GT');

		$arr = array(
			'chart' => array(
				'expansion' => $expansion,
				'new_site' => $new_site,
			),
			'callouts' => '',
			'scoreboard' => '',
			'vendor_view' => array(
				'expansion' => array(
					'amdocs' => $expan_amdocs,
					'ht' => $expan_ht,
					'nokia' => $expan_nsb,
					'gt' => $expan_gt,
					'total' => array(
						'PD_TRFS' => $expan_amdocs->PD_TRFS + $expan_ht->PD_TRFS + $expan_nsb->PD_TRFS + $expan_gt->PD_TRFS,	
						'SSV_APPROVED_DATE' => $expan_amdocs->SSV_APPROVED_DATE + $expan_ht->SSV_APPROVED_DATE + $expan_nsb->SSV_APPROVED_DATE + $expan_gt->SSV_APPROVED_DATE,
						'SSV_READY' => $expan_amdocs->SSV_READY + $expan_ht->SSV_READY + $expan_nsb->SSV_READY + $expan_gt->SSV_READY,
						'SSV_SUBMIT_DATE' => $expan_amdocs->SSV_SUBMIT_DATE + $expan_ht->SSV_SUBMIT_DATE + $expan_nsb->SSV_SUBMIT_DATE + $expan_gt->SSV_SUBMIT_DATE,
					)
				),
				'new_site' => array(
					'amdocs' => $new_amdocs,
					'ht' => $new_ht,
					'nokia' => $new_nsb,
					'gt' => $new_gt,
					'total' => array(
						'PD_TRFS' => $new_amdocs->PD_TRFS + $new_ht->PD_TRFS + $new_nsb->PD_TRFS + $new_gt->PD_TRFS,	
						'SSV_APPROVED_DATE' => $new_amdocs->SSV_APPROVED_DATE + $new_ht->SSV_APPROVED_DATE + $new_nsb->SSV_APPROVED_DATE + $new_gt->SSV_APPROVED_DATE,
						'SSV_READY' => $new_amdocs->SSV_READY + $new_ht->SSV_READY + $new_nsb->SSV_READY + $new_gt->SSV_READY,
						'SSV_SUBMIT_DATE' => $new_amdocs->SSV_SUBMIT_DATE + $new_ht->SSV_SUBMIT_DATE + $new_nsb->SSV_SUBMIT_DATE + $new_gt->SSV_SUBMIT_DATE,
					)
				),
			)
		);

		return $arr;
	}

	private function vendor_expansion_query($where) {
		$this->db->where('VENDOR', $where);
		$this->db->where('(PROGRAM = "Macro" 
							OR PROGRAM = "Mobile Expansion" 
							OR PROGRAM = "OWO" 
							OR PROGRAM = "Small Cell"
							OR PROGRAM = "SRW"
							OR PROGRAM = "SRW (TowerCo - GAS)"
							OR PROGRAM = "TowerCo (Macro) - GAS"
							OR PROGRAM = "TowerCo (Macro)"
							OR PROGRAM = "TowerCo (Macro) - BTS")'
						);

		$this->db->select('COUNT(PD_TRFS) as PD_TRFS,
						   COUNT(SSV_SUBMIT_DATE) as SSV_SUBMIT_DATE, 
						   COUNT(SSV_READY) as SSV_READY, 
						   COUNT(SSV_APPROVED_DATE) as SSV_APPROVED_DATE');
		// $this->db->from($this->tbl);
		if ($this->filter <> 'all') {$this->db->where('REGION', strtoupper($this->filter));}
		$this->db->from($this->tbl);
		return $this->db->get()->row();
	} 

	private function vendor_newsite_query($where) {
		$this->db->where('VENDOR', $where);
		$this->db->where('(PROGRAM = "3G to 4G Migration" 
							OR PROGRAM = "5G" 
							OR PROGRAM = "5RFM"
							OR PROGRAM = "Enabler"
							OR PROGRAM = "WTTx")'
						);
		
		$this->db->select('COUNT(PD_TRFS) as PD_TRFS,
						   COUNT(SSV_SUBMIT_DATE) as SSV_SUBMIT_DATE, 
						   COUNT(SSV_READY) as SSV_READY, 
						   COUNT(SSV_APPROVED_DATE) as SSV_APPROVED_DATE');
		// $this->db->from($this->tbl);
		if ($this->filter <> 'all') {$this->db->where('REGION', strtoupper($this->filter));}
		$this->db->from($this->tbl);
		return $this->db->get()->row();
	} 

	public function ssv_upload() {
		#FILE NAME
		$name = trim($_FILES['ssv_file']['name']);
		#GET 0 - SSV | 1 - TRACKER | 2 - REGION (VIS OR MIN)
		$pieces = explode("_", $name);

		#GET EXTENSION OF FILE
		$array = explode('.', $pieces[2]);
		$region = $array[0];

		if ($region == strtoupper($this->geoarea)) {
			if (isset($_FILES["ssv_file"]["name"])) {
				$path = $_FILES["ssv_file"]["tmp_name"];
				$object = PHPExcel_IOFactory::load(strval($path));
	
				#GET FORMAT HEADER OF EXCEL
				$cell_collection = $object->getActiveSheet()->getCellCollection();
				foreach ($cell_collection as $cell) {
					$column = $object->getActiveSheet()->getCell($cell)->getColumn();
					$row = $object->getActiveSheet()->getCell($cell)->getRow();
					$data_value = $object->getActiveSheet()->getCell($cell)->getValue();
					//The header will/should be in row 1 only. of course, this can be modified to suit your need.
					if ($row == 1) {
						$header[$row][$column] = trim($data_value);
					}
				}
	
	
				#CHECK IF EXCEL IMPORT MATCHES TO DATABASE FORMAT OF EXCEL
				if (28 == count($header[1])) {
					foreach ($object->getWorksheetIterator() as $worksheet) {
						$highestRow = $worksheet->getHighestRow();
						for ($row = 2; $row <= $highestRow; $row++) {
							
							$data[] = array(
								'VENDOR' => $worksheet->getCellByColumnAndRow(0, $row)->getValue(),
								'NOMINATION_ID' => $worksheet->getCellByColumnAndRow(1, $row)->getValue(),
								'SOW_TRFS_YEAR' => $worksheet->getCellByColumnAndRow(2, $row)->getValue(),
								'REGION' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
								'PROGRAM' => $worksheet->getCellByColumnAndRow(4, $row)->getValue(),
								'SUB_PROJECT' => $worksheet->getCellByColumnAndRow(5, $row)->getValue(),
								'PLAID' => $worksheet->getCellByColumnAndRow(6, $row)->getValue(),
								'BCFNAME' => $worksheet->getCellByColumnAndRow(7, $row)->getValue(),
								'TECHMDBSCOPE' => $worksheet->getCellByColumnAndRow(8, $row)->getValue(),
								"VENDORNAME_PLAID_TECHMDBSCOPE" => $worksheet->getCellByColumnAndRow(9, $row)->getValue(),
								'VENDORNAME_BCFNAME_TECHMDBSCOPE' => $worksheet->getCellByColumnAndRow(10, $row)->getValue(),
								
								'RF_SERVICES_A_VENDOR' => $worksheet->getCellByColumnAndRow(11, $row)->getValue(),
								'RFA_Services_Scope' => $worksheet->getCellByColumnAndRow(12, $row)->getValue(),
								'RF_SERVICES_B_VENDOR' => $worksheet->getCellByColumnAndRow(13, $row)->getValue(),
								'RF_SERVICES_PO_REFERENCE' => $worksheet->getCellByColumnAndRow(14, $row)->getValue(),
								'RFAB_Services_Scope' => $worksheet->getCellByColumnAndRow(15, $row)->getValue(),
								'BFT' => $worksheet->getCellByColumnAndRow(16, $row)->getValue(),
								'PD_TRFS' => $worksheet->getCellByColumnAndRow(17, $row)->getValue(),
								'SSV_READY' => $worksheet->getCellByColumnAndRow(18, $row)->getValue(),
								'SSV_SUBMIT_DATE' => $worksheet->getCellByColumnAndRow(19, $row)->getValue(),
								'SSV_REJECTION_DATE' => $worksheet->getCellByColumnAndRow(20, $row)->getValue(),

								'SSV_RESUBMITTED' => $worksheet->getCellByColumnAndRow(21, $row)->getValue(),
								'SSV_APPROVED_DATE' => $worksheet->getCellByColumnAndRow(22, $row)->getValue(),
								'REVIEW_REMARKS' => $worksheet->getCellByColumnAndRow(23, $row)->getValue(),
								'TIMELINE_OF_COMPLETION' => $worksheet->getCellByColumnAndRow(24, $row)->getValue(),
								'RF_DEPENDENCY' => $worksheet->getCellByColumnAndRow(25, $row)->getValue(),
								'RF_STATUS' => $worksheet->getCellByColumnAndRow(26, $row)->getValue(),
								'RESPONSIBLE_GROUP' => $worksheet->getCellByColumnAndRow(27, $row)->getValue()
							);
						}
					}
	
					#TRUNCATE TABLE BEFORE INSERT
					// $this->db->truncate($this->tbl);
					$this->db->where('REGION', $this->geoarea);
					$this->db->delete($this->tbl);
					
					$this->db->insert_batch($this->tbl, array_filter($data));
	
					if ($this->db->affected_rows() == 0) {
						$json = array(
							'title' => 'Oops!',
							'msg'   => 'Upload failed.',
							'type'  => 'error',
						);
					} else {
						#GET RANK WORST CELLS PER CELL NAMES OR SITE
						$json = array(
							'title' => 'Success',
							'msg'   => 'Successfully uploaded.',
							'type'  => 'success',
						);
					}
					
				} else {
					$json = array(
						'title' => 'Oops!',
						'msg'   => 'Import file does not match to desired format.',
						'type'  => 'error',
					);
				}
			} else {
				$json = array(
					'title' => 'Ooops!',
					'msg'   => 'Something went wrong.',
					'type'  => 'error',
					'date' => ''
				);
			}
		} else {
			$json = array(
				'title' => 'Ooops!',
				'msg'   => 'Filename does not match, must be SSV_TRACKER_VIS/MIN by respective Region',
				'type'  => 'error',
				'date' => ''
			);
		}

		return $json;
	}

	public function ssv_callouts() {
		$this->db->truncate('ssv_callouts');
		$callouts = trim($this->input->post('callouts'));
		$this->db->insert('ssv_callouts', array("callouts" => $callouts));

		return array(
			'title' => 'Success',
			'msg'   => 'Successfully uploaded.',
			'type'  => 'success',
		);
	}

	public function get_callouts() {
		return $this->db->get('ssv_callouts')->row();
	}

	public function export_ssv()
	{
		$this->db->where('REGION', $this->geoarea);
		$this->db->order_by('BCFNAME', 'ASC');
		$this->db->select('*');
		$result = $this->db->get($this->tbl);

		$data = array(
			'result' => $result->result(),
			'geoarea' => strtoupper($this->geoarea)
		);

		return $data;
	}
}
