<?php


class UDB_Model extends CI_Model
{
	public function search_keys() {
		$search = $this->input->get('search');
		$geoarea = $this->session->userdata('geoarea');
		$rfe_tbl = 'rfe_'.$geoarea;
		$rwe_tbl = 'rwe_'.$geoarea;
		$rre_tbl = 'rre_'.$geoarea;
		$rgpm_tbl = 'rgpm_'.$geoarea;
		$rcne_tbl = 'rcne_milo_'.$geoarea;

		#RRE
		$this->db->select('id, PLAID, BTS_NAME as SITE_NAME');
		$this->db->from($rre_tbl);
		$this->db->like("PLAID", $search);
		$this->db->or_like("BTS_NAME", $search);
		$rre = $this->db->get_compiled_select();
		#RFE
		$this->db->select('id, PLAID, SITE_NAME');
		$this->db->from($rfe_tbl);
		$this->db->like("PLAID", $search);
		$this->db->or_like("SITE_NAME", $search);
		$rfe = $this->db->get_compiled_select();
		#RWE
		$this->db->select('id, PLAID, DENIRO as SITE_NAME');
		$this->db->from($rwe_tbl);
		$this->db->like("PLAID", $search);
		$this->db->or_like("DENIRO", $search);
		$this->db->or_like("LOOP_NAME", $search);
		$this->db->or_like("OMIP", $search);
		$this->db->or_like("CABINET_NAME", $search);
		$rwe = $this->db->get_compiled_select();

		#RCNE
		$this->db->select('id, PLAID, SITE_NAME');
		$this->db->from($rcne_tbl);
		$this->db->like("PLAID", $search);
		$this->db->or_like("SITE_NAME", $search);
		$this->db->or_like("NAP_NUMBER", $search);
		$rcne = $this->db->get_compiled_select();

		#RGPM
		$this->db->select('id, PLAID, SITENAME as SITE_NAME');
		$this->db->from($rgpm_tbl);
		$this->db->like("PLAID", $search);
		$this->db->or_like("SITENAME", $search);
		$rgpm = $this->db->get_compiled_select();

		#NOTE N/A OF PLAID WILL NOT APPEAR ON THE RESULTS
		$result = $this->db->query("SELECT id, PLAID, SITE_NAME FROM (" . $rre." UNION ALL ".$rfe." UNION ALL " .$rwe. " UNION ALL " .$rcne. " UNION ALL " .$rfe. " UNION ALL " .$rgpm. ") AS X WHERE PLAID != ? AND PLAID != ? GROUP BY PLAID ORDER BY id DESC", array("#N/A", ''));

		return $result->result();
	}

	public function search_data() {
		$geoarea = $this->session->userdata('geoarea');
		$rfe_tbl = 'rfe_'.$geoarea;
		$rwe_tbl = 'rwe_'.$geoarea;
		$rcne_tbl = 'rcne_milo_'.$geoarea;
		$rre_tbl = 'rre_'.$geoarea;
		$rgpm_tbl = 'rgpm_'.$geoarea;

		$pla_id = $this->input->post('plaid');

		#GET RRE DATA
		$this->db->where('PLAID', $pla_id);
		$rre = $this->db->get($rre_tbl);

		$two_g = array();
		$three_g = array();
		$ht_four_g = array();
		$nokia_four_g = array();

		#CHECK IF EXISTS
		if ($rre->num_rows() > 0) {
			$row = $rre->row();
			#2G
			$this->db->where('BTS_NAME', $row->BTS_Name);
			$two_g = $this->db->get('2g_'.$rre_tbl)->result();
			#3G
			$this->db->where('BTS_NAME', $row->BTS_Name);
			$three_g = $this->db->get('3g_'.$rre_tbl)->result();
			#NOKIA
			$this->db->where('BTS_NAME', $row->BTS_Name);
			$nokia_four_g = $this->db->get('4gns_'.$rre_tbl)->result();
			#HUAWEI
			$this->db->where('BTS_NAME', $row->BTS_Name);
			$ht_four_g = $this->db->get('4ght_'.$rre_tbl)->result();
		}


		$rre_data = array(
			'rre_details' => $rre->row(),
			'two_g'  => $two_g,
			'three_g'  => $three_g,
			'nokia_four_g'  => $nokia_four_g,
			'ht_four_g'  => $ht_four_g
		);

		#GET RFE DATA
		$this->db->where('PLAID', $pla_id);
		$rfe = $this->db->get($rfe_tbl);

		#GET RWE DATA
		$this->db->where('PLAID', $pla_id);
		$rwe = $this->db->get($rwe_tbl);

		#GET RCNE DATA
		$this->db->where('PLAID', $pla_id);
		$rcne = $this->db->get($rcne_tbl);

		#GET RGPM DATA
		$this->db->where('PLAID', $pla_id);
		$rgpm = $this->db->get($rgpm_tbl);

		$data = array(
			'rre' => $rre_data,
			'rfe' => $rfe->result(),
			'rwe' => $rwe->result(),
			'rgpm' => $rgpm->result(),
			'rcne' => $rcne->result(),
		);

		return $data;
	}

	#BATCH INSERT OF ARRAYS
	public function batch_insert()
	{
		$this->load->helper('security');
		$table_name = $this->session->userdata('table_name');

		$json = array();
//		$geoarea = $this->session->userdata('geoarea');
//		$dep = strtolower($this->session->userdata('department'));
//		$table_name = $dep.'_'.$geoarea;

		$filename = $this->security->entity_decode($this->input->raw_input_stream);
		$jsonArray = json_decode($filename, true);
		$count = 0;

		for ($i= 0; $i<count($jsonArray);$i++) {
			$this->insertDataByBatch($jsonArray[$i], $table_name);
			$count++;
		}
		if ($count == count($jsonArray)) {
			$json = array(
				'title' => 'Success',
				'msg'   => 'The file successfully imported.',
				'type'  => 'success'
			);
		}
		return $json;
	}

	public function insertDataByBatch($data, $table_name)
	{
		$this->db->escape($data);
		$this->db->insert($table_name, $data);
		return $this->db->affected_rows();
	}

	public function truncate_data() {
		$geoarea = $this->session->userdata('geoarea');
		$table_name = $this->input->post('name').'_'.$geoarea;
		$this->session->set_userdata(array('table_name' => $table_name));
		return $this->db->truncate($table_name);
	}
}
