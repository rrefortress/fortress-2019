<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bluebook_Model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		ini_set( 'memory_limit', '1G' );
		ini_set('upload_max_filesize', '1G');
		ini_set('post_max_size', '1G');
		ini_set('max_input_time', 600000);
		ini_set('max_execution_time', 600000);
		$this->tbl = "bluebook";
	}

	public function set_config($total = 0, $func_name = '', $row_page = 0)
	{
		$config['base_url'] = base_url($func_name);
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $total;
		$config['per_page'] = $row_page;

		$config['full_tag_open'] = '<ul style="height: 0;" class="mg-top-5 pagination small text-center paginate_track">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['next_tag_close'] = '<span aria-hidden="true"></span></span></li>';
		$config['prev_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['prev_tag_close'] = '</span></li>';
		$config['first_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['first_tag_close'] = '</span></li>';
		$config['last_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['last_tag_close'] = '</span></li>';

		// Initialize
		$this->pagination->initialize($config);
		return $config;
	}

	public function get_files($row_num = 0)
	{
		$vendor = strtoupper($this->input->post('vendor'));
		$yearly = $this->input->post('yearly');
		$region = $this->input->post('region');

		$row_page = $this->input->post('row_page');

		if ($yearly === '2020') {
			$this->db->where('region', $region);
		}

		$this->db->where('vendor', $vendor);
		$this->db->where('year', $yearly);
		$res = $this->db->get('acceptance');

		if ($row_num != 0) {
			$row_num = ($row_num - 1) * $row_page;
		}

		$this->set_config($res->num_rows(), $func_name = 'get_files', $row_page);

		$query = $this->get_details($row_num, $row_page);

		$result = array();
		$num = 1;
		foreach($query as $row) {
			$result[] = array(
				'id' => $row->id,
				'num' => $num++,
				'name' => $row->file_name,
				'size' => $this->filesize_formatted($row->file_size),
				'date' => $row->date_uploaded
			);
		}

		return $arrays = array(
			'result' => $result,
			'pagination' => $this->pagination->create_links(),
			'row_num' => $row_num,
			'total' => $res->num_rows()
		);
	}

	public function get_details($row_num = 0, $row_page = 0)
	{
		$search = trim($this->input->post('search'));
		$sort = $this->input->post('sort');

		$vendor = strtoupper($this->input->post('vendor'));
		$yearly = $this->input->post('yearly');
		$region = $this->input->post('region');

		if ($yearly === '2020') {$this->db->where('region', $region);}
		
		$this->db->like('file_name', $search);
		$this->db->where('vendor', $vendor);
		$this->db->where('year', $yearly);
		$this->db->order_by('file_name', $sort);
		$this->db->order_by("id", "DESC");
		$this->db->limit($row_page, $row_num);
		$res = $this->db->get('acceptance');
		return $res->result();
	}

	public function acceptance_upload() {
		$geoarea = strtoupper($this->session->userdata('geoarea'));

		$region = strtoupper($this->input->post('region'));

		#CHECK IF SESSION EXPIRE
		if(empty($geoarea)) {redirect(base_url('login'));}

		$geoarea = $geoarea == 'NAT' ? '' : $geoarea;
		$vendor = strtoupper($this->session->userdata('vendor'));
		$yearly = $this->input->post('yearly');

		#FILE NAME
		$name = trim($_FILES['acceptance_file']['name']);
		// #GET EXTENSION OF FILE
		$array = explode('.', $name);
		$extension = end($array);

			// #MAKE DIRECTORY FOR SITES
			// $vendor = $vendor == 'NOKIA' ? 'NSB' : ($vendor == 'BAU' ? 'BAU' : 'HT');
			
			#CHECK FOR YEARLY: 2019 & Below | 2020
			if ($yearly === '2020') {
				$config['upload_path'] = FCPATH.'/vendor/'.$vendor.'/'.$yearly .'/'.$region;	
			} else {
				$config['upload_path'] = FCPATH.'/vendor/'.$vendor.'/'.$yearly;	
			}

			$config['allowed_types'] = 'pdf';
			$this->load->library('upload',$config);
			if(!file_exists($config['upload_path'])) {
				mkdir($config['upload_path'],0777, TRUE);
			}

			#INSERT IN DIRECTORY PER SITE
			if($this->upload->do_upload('acceptance_file')) {
				
				#INSERT INTO DIRECTORY FILE
				$filename = $this->upload->data('file_name');
				$size = filesize($config['upload_path'].'/'.$filename);
				#INSERT DATA INTO DB
					$data = array(
						'vendor' => $vendor,
						'region' => $region,
						'year' => $yearly,
						'file_name' => $filename,
						'file_size' => $size,
						'file_type' => $extension,
					);
					$this->db->set('date_uploaded', 'NOW()', FALSE);
					$this->db->insert('acceptance', $data);

					$json = array(
						'type' 	=> 'success',
						'title' => 'Success!',
						'msg' 	=> 'File successfully uploaded.'
					);
			}

		return $json;
	}

	public function get_main() {
		#Design and Standards
		$this->db->select('SUM(file_size) as size, count(file_name) as files');
		$this->db->where('label', 'Design and Standards');
		$ds = $this->db->get($this->tbl)->row_array();
		#Guidelines
		$this->db->select('SUM(file_size) as size, count(file_name) as files');
		$this->db->where('label', 'Guidelines');
		$gl = $this->db->get($this->tbl)->row_array();
		#Type Approvals
		$this->db->select('SUM(file_size) as size, count(file_name) as files');
		$this->db->where('label', 'Type Approvals');
		$ta = $this->db->get($this->tbl)->row_array();
		#Size
		$this->db->select('SUM(file_size) as size, count(file_name) as files');
		$total = $this->db->get($this->tbl)->row_array();

		return $data = array(
			'size' => array(
				0 => array(
					'name' => 'Design and Standards',
					'label'  => 'ds',
					'icon' => "fas fa-pencil-ruler",
					'size' => $ds['size'] == NULL ? 0 : $this->filesize_formatted($ds['size']),
					'count' => $ds['files']
				),
				1 => array (
					'name' => 'Guidelines',
					'label'  => 'gl',
					'icon' => "fas fa-book",
					'size' => $gl['size'] == NULL ? 0 : $this->filesize_formatted($gl['size']),
					'count' => $gl['files']
				),
				2 => array(
					'name' => 'Type Approvals',
					'label'  => 'ta',
					'icon' => "fas fa-vote-yea",
					'size' => $ta['size'] == NULL ? 0 : $this->filesize_formatted($ta['size']),
					'count' => $ta['files']
				),
				4 => array(
					'name' => 'Standards & Checklist',
					'label'  => 'ta',
					'icon' => "fas fa-tasks",
					'size' => $ta['size'] == NULL ? 0 : $this->filesize_formatted($ta['size']),
					'count' => $ta['files']
				),
		
			),
			'total' => $this->filesize_formatted($total['size']),
			'count' => $total['files']
		);
	}

	public function get_categories() {
		$label = $this->input->post('label');

		// $res = array();
		// foreach(array('ds', 'gl', 'ta') as $row) {
		// 	$this->db->select('SUM(file_size) as size, count(file_name) as files');
		// 	$this->db->where('label', $label);
		// 	$this->db->where('year', $row);
		// 	$rows = $this->db->get($this->tbl)->row_array();

		// 	$res[] = array(
		// 		'name' => $row,
		// 		'files' => $rows['files'],
		// 		'size' => $rows['size'] == NULL ? 0 : $this->filesize_formatted($rows['size']),
		// 		'link' => $row == 'ds' ? 'gl' : 'ta'
		// 	);
		// }

		// return $res;

		return array();
	}

	public function get_region() {
		$vendor = $this->input->post('vendor');
		$yearly = $this->input->post('yearly');
		#NLZ
		$this->db->select('SUM(file_size) as size, count(file_name) as files');
		$this->db->where('vendor', $vendor);
		$this->db->where('year', $yearly);
		$this->db->where('REGION', 'NLZ');
		$nlz = $this->db->get('acceptance')->row_array();
		#SLZ
		$this->db->select('SUM(file_size) as size, count(file_name) as files');
		$this->db->where('vendor', $vendor);
		$this->db->where('year', $yearly);
		$this->db->where('REGION', 'SLZ');
		$slz = $this->db->get('acceptance')->row_array();
		#NCR
		$this->db->select('SUM(file_size) as size, count(file_name) as files');
		$this->db->where('vendor', $vendor);
		$this->db->where('year', $yearly);
		$this->db->where('REGION', 'NCR');
		$ncr = $this->db->get('acceptance')->row_array();
		#VIS
		$this->db->select('SUM(file_size) as size, count(file_name) as files');
		$this->db->where('vendor', $vendor);
		$this->db->where('year', $yearly);
		$this->db->where('REGION', 'VIS');
		$vis = $this->db->get('acceptance')->row_array();
		#MIN
		$this->db->select('SUM(file_size) as size, count(file_name) as files');
		$this->db->where('vendor', $vendor);
		$this->db->where('year', $yearly);
		$this->db->where('REGION', 'MIN');
		$min = $this->db->get('acceptance')->row_array();

		return $data = array(
			0 => array(
				'label' => 'SOUTH LUZON',
				'name' => 'SLZ',
				'size' => $slz['size'] == NULL ? 0 : $this->filesize_formatted($slz['size']),
				'count' => $slz['files']
			),
			1 => array (
				'label' => 'NORTH LUZON',
				'name' => 'NLZ',
				'size' => $nlz['size'] == NULL ? 0 : $this->filesize_formatted($nlz['size']),
				'count' => $nlz['files']
			),
			2 => array(
				'label' => 'NATIONAL CAPITAL REGION',
				'name' => 'NCR',
				'size' => $ncr['size'] == NULL ? 0 : $this->filesize_formatted($ncr['size']),
				'count' => $ncr['files']
			),
			3 => array(
				'label' => 'VISAYAS',
				'name' => 'VIS',
				'size' => $vis['size'] == NULL ? 0 : $this->filesize_formatted($vis['size']),
				'count' => $vis['files']
			),
			4 => array(
				'label' => 'MINDANAO',
				'name' => 'MIN',
				'size' => $min['size'] == NULL ? 0 : $this->filesize_formatted($min['size']),
				'count' => $min['files']
			),
		);
	}

	public function remove_file() {
		$res = false;

		$id = $this->input->post('id');
		$vendor = $this->input->post('vendor');
		$yearly = $this->input->post('yearly');
		$region = $this->input->post('region');

		$geoarea = strtoupper($this->session->userdata('geoarea'));
		if(empty($geoarea)) {redirect(base_url('login'));}

			foreach($id as $row) {
				#CHECK IF FILE EXISTS
				$this->db->where('id', $row);
				$check = $this->db->get('acceptance');

				if ($check->num_rows() > 0) {
					$res = $check->row_array();
					#CHECK FOR YEARLY: 2019 & Below | 2020
					if ($yearly === '2020') {
						$path = FCPATH.'/vendor/'.$vendor.'/'.$yearly .'/'.$region .'/'.$res['file_name'];	
					} else {
						$path = FCPATH.'/vendor/'.$vendor.'/'.$yearly .'/'.$res['file_name'];	
					}
					#REMOVE FILE FROM DIRECTORY
					if(file_exists($path)){
						unlink($path);
					}

					#REMOVE FROM DB
					$this->db->where('id', $row);
					$res = $this->db->delete('acceptance');	
				}				
			}

		return $res;
	}

	public function edit_filename() {
		$res = false;

		$eid = $this->input->post('eid');
		$vendor = $this->input->post('vendor');
		$yearly = $this->input->post('yearly');
		$region = $this->input->post('region');
		$fname = trim($this->input->post('fname'));

		#CHECK IF VALUE EXISTS
		$this->db->where('id', $eid);
		$res = $this->db->get('acceptance');

		if ($res->num_rows() > 0){
			$res = $res->row_array();

			#UPDATE / RENAME FILENAME TO DIRECTORY
			#CHECK FOR YEARLY: 2019 & Below | 2020
			if ($yearly === '2020') {
				$old = FCPATH.'/vendor/'.$vendor.'/'.$yearly .'/'.$region .'/'.$res['file_name'];	
				$new = FCPATH.'/vendor/'.$vendor.'/'.$yearly .'/'.$region .'/'.$fname;	
			} else {
				$old = FCPATH.'/vendor/'.$vendor.'/'.$yearly .'/'.$res['file_name'];	
				$new = FCPATH.'/vendor/'.$vendor.'/'.$yearly .'/'.$fname;	
			}

			rename($old, $new);

			#UPDATE / RENAME FILENAME TO DB
			$data = array('file_name' => $fname);
			$this->db->where('id', $eid);
			$res = $this->db->update('acceptance', $data);
		} 

		return $res;
	}

	public function filesize_formatted($size) {
		$units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
		$power = $size > 0 ? floor(log($size, 1024)) : 0;
		return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
	}

	public function get_file_count() {
		$site_name = $this->input->post('site_name');

		#ENGINEERING PLANS
		$ep = array('SITENAME'=>$site_name, 'folder_name' => 'ENGINEERING PLANS');
		$engr_plans = $this->db->get_where('repo_files', $ep);

		#IMPLEMENTATION
		$im = array('SITENAME'=>$site_name, 'folder_name' => 'IMPLEMENTATION');
		$imple = $this->db->get_where('repo_files', $im);

		#TOTAL SIZE
		$this->db->select('COUNT(id) as count, SUM(file_size) as file_size');
		$this->db->from('repo_files');
		$total = $this->db->get()->row();

		$data = array(
			'result' => array(
				0 => array(
					'name' => 'ENGINEERING PLANS',
					'count' => $engr_plans->num_rows(),
				),
				1 => array(
					'name' => 'IMPLEMENTATION',
					'count' => $imple->num_rows()
				),
			),
			'total' => $total->file_size == NULL ? 0 : $this->filesize_formatted($total->file_size),
			'count' => $total->count
		);
		return $data;
	}

	
}


