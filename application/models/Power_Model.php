<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Power_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('string');
		$this->geoarea = $this->session->userdata('geoarea');
		$this->tablename = "powerdb_" . $this->geoarea;
		$this->tbl_prop = "power_proposal_tbl_" . $this->geoarea;
		$this->tbl_tprop = "tower_proposal_" . $this->geoarea;
		$this->power_propsal_mdb = "power_propsal_mdb_".$this->geoarea;
		$this->geo = $this->input->post('geo_power');
		$this->sc = $this->input->post('sc');
		$this->regions = array('min', 'slz', 'nlz', 'ncr', 'vis');
		$this->tbu = 'To be Updated';

		$this->tbl2g = "plan_2g";
		$this->tbl3g = "plan_3g";
		$this->tbl4g = "plan_4g";
		$this->tbl5g = "plan_5g";
	}

	public function get_acload_fr_database($db_table, $id_key)
	{
		$this->db->where($id_key);
		return $this->db->get($db_table)->row()->TOTAL_ACLOAD;
	}

	public function proposed_model($input_array)
	{
		$tech_value = array();
		$filename = $this->security->entity_decode($this->input->raw_input_stream);
		$jsonArray = json_decode($filename, true);
		for ($i = 0; $i < count($jsonArray); $i++) {
			$tech = $jsonArray[$i]['tech'];
			$val = $jsonArray[$i]['value'];
			$tech_value[] = array(
				'tech' => $tech,
				'total' => $this->rre_equip_powertable($tech, $val),
				'val' => $val
			);
		}
		print_r($tech_value);
	}

	public function convert_wattage($input_amp, $input_volts, $amp_type, $phase)
	{
		switch ($amp_type) {
			case 'AC':
				switch (strtoupper($phase)) {
					case 'SINGLE PHASE':
						return $input_amp * $input_volts;
						break;

					case 'THREE PHASE':
						return $input_amp * $input_volts * 3;
						break;

					default:
						return "unknown phase";
						break;
				}
				break;
			case 'DC':
				return $input_amp * $input_volts;
				break;
			default:
				return "unkwown amp type";
				break;
		}
	}

	public function highest_value($data_array)
	{
		$top_value = 0;
		foreach ($data_array as $value) {
			if ($value == "N/A") {
				$value = 0;
			}
			if ($top_value < $value) {
				$top_value = $value;
			}
		}
		return $top_value;
	}

	public function export_power()
	{
		$geoarea = $this->session->userdata('geoarea');
		if ($this->geoarea === 'nat') {
			$result = array();

			$this->db->select('*');
			$this->db->from('powerdb_nlz');
			$nlz = $this->db->get_compiled_select();

			$this->db->select('*');
			$this->db->from('powerdb_slz');
			$slz = $this->db->get_compiled_select();

			$this->db->select('*');
			$this->db->from('powerdb_ncr');
			$ncr = $this->db->get_compiled_select();

			$this->db->select('*');
			$this->db->from('powerdb_vis');
			$vis = $this->db->get_compiled_select();

			$this->db->select('*');
			$this->db->from('powerdb_min');
			$min = $this->db->get_compiled_select();

			$result = $this->db->query("SELECT * FROM (" . $nlz." UNION ALL ".$slz." UNION ALL " .$ncr. " UNION ALL " .$vis. " UNION ALL " .$min.") AS X ORDER BY SITENAME ASC");
		} else {
			$tablename = "powerdb_" . $geoarea;
			$this->db->order_by('SITENAME', 'ASC');
			$this->db->select('*');
			$result = $this->db->get($tablename);
		}

		$data = array(
			'result' => $result->result(),
			'geoarea' => strtoupper($geoarea)
		);

		return $data;
	}

	public function power_upload()
	{
		if (isset($_FILES["power_file"]["name"])) {
			$path = $_FILES["power_file"]["tmp_name"];
			$object= PHPExcel_IOFactory::load($path);

			#GET FORMAT HEADER OF EXCEL
			$cell_collection = $object->getActiveSheet()->getCellCollection();
			foreach ($cell_collection as $cell) {
				$column = $object->getActiveSheet()->getCell($cell)->getColumn();
				$row = $object->getActiveSheet()->getCell($cell)->getRow();
				$data_value = $object->getActiveSheet()->getCell($cell)->getValue();
				//The header will/should be in row 1 only. of course, this can be modified to suit your need.
				if ($row == 1) {
					$header[$row][$column] = $data_value;
				}
			}

			#CHECK IF EXCEL IMPORT MATCHES TO DATABASE FORMAT OF EXCEL
			if (54 == count($header[1])) {
				foreach ($object->getWorksheetIterator() as $worksheet) {
					$highestRow = $worksheet->getHighestRow();
					for ($row = 2; $row <= $highestRow; $row++) {
						$data[] = array(
							'GEOAREA' => $worksheet->getCellByColumnAndRow(0, $row)->getValue(),
							'SITENAME' => $worksheet->getCellByColumnAndRow(1, $row)->getValue(),
							'PLAID' => $worksheet->getCellByColumnAndRow(2, $row)->getValue(),
							'SITE_CLASS' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
							'SITE_TYPE' => $worksheet->getCellByColumnAndRow(4, $row)->getValue(),
							'CABIN_QUANTITY' => $worksheet->getCellByColumnAndRow(5, $row)->getValue(),
							'CABIN_TYPE' => $worksheet->getCellByColumnAndRow(6, $row)->getValue(),
							'POWER_CONNECTION' => $worksheet->getCellByColumnAndRow(7, $row)->getValue(),
							'POWER_PHASE' => $worksheet->getCellByColumnAndRow(8, $row)->getValue(),
							'GRID_STATUS' => $worksheet->getCellByColumnAndRow(9, $row)->getValue(),
							'TRANSFORMER_CAPACITY_KVA' => $worksheet->getCellByColumnAndRow(10, $row)->getValue(),
							'GENSET_CAPACITY_KVA' => $worksheet->getCellByColumnAndRow(11, $row)->getValue(),
							'MDP_ECB_RATING_AT' => $worksheet->getCellByColumnAndRow(12, $row)->getValue(),
							'ACPDB_RATING_AT' => $worksheet->getCellByColumnAndRow(13, $row)->getValue(),
							'ECB_CLAMP_READING_AAC' => $worksheet->getCellByColumnAndRow(14, $row)->getValue(),
							'RECTIFIER_1' => $worksheet->getCellByColumnAndRow(15, $row)->getValue(),
							'RS1_MODULE_CAPACITY' => $worksheet->getCellByColumnAndRow(16, $row)->getValue(),
							'RS1_NO_OF_MODULES' => $worksheet->getCellByColumnAndRow(17, $row)->getValue(),
							'RS1_NO_OF_EMPTY_MODULE' => $worksheet->getCellByColumnAndRow(18, $row)->getValue(),
							'RS1_ACTUAL_LOAD_ADC' => $worksheet->getCellByColumnAndRow(19, $row)->getValue(),
							'RS1_BATTERY_BRAND' => $worksheet->getCellByColumnAndRow(20, $row)->getValue(),
							'RS1_BATTERY_TYPE' => $worksheet->getCellByColumnAndRow(21, $row)->getValue(),
							'RS1_BATTERY_CAPACITY' => $worksheet->getCellByColumnAndRow(22, $row)->getValue(),

							'RECTIFIER_2' => $worksheet->getCellByColumnAndRow(23, $row)->getValue(),
							'RS2_MODULE_CAPACITY' => $worksheet->getCellByColumnAndRow(24, $row)->getValue(),
							'RS2_NO_OF_MODULES' => $worksheet->getCellByColumnAndRow(25, $row)->getValue(),
							'RS2_NO_OF_EMPTY_MODULE' => $worksheet->getCellByColumnAndRow(26, $row)->getValue(),
							'RS2_ACTUAL_LOAD_ADC' => $worksheet->getCellByColumnAndRow(27, $row)->getValue(),
							'RS2_BATTERY_BRAND' => $worksheet->getCellByColumnAndRow(28, $row)->getValue(),
							'RS2_BATTERY_TYPE' => $worksheet->getCellByColumnAndRow(29, $row)->getValue(),
							'RS2_BATTERY_CAPACITY' => $worksheet->getCellByColumnAndRow(30, $row)->getValue(),

							'RECTIFIER_3' => $worksheet->getCellByColumnAndRow(31, $row)->getValue(),
							'RS3_MODULE_CAPACITY' => $worksheet->getCellByColumnAndRow(32, $row)->getValue(),
							'RS3_NO_OF_MODULES' => $worksheet->getCellByColumnAndRow(33, $row)->getValue(),
							'RS3_NO_OF_EMPTY_MODULE' => $worksheet->getCellByColumnAndRow(34, $row)->getValue(),
							'RS3_ACTUAL_LOAD_ADC' => $worksheet->getCellByColumnAndRow(35, $row)->getValue(),
							'RS3_BATTERY_BRAND' => $worksheet->getCellByColumnAndRow(36, $row)->getValue(),
							'RS3_BATTERY_TYPE' => $worksheet->getCellByColumnAndRow(37, $row)->getValue(),
							'RS3_BATTERY_CAPACITY' => $worksheet->getCellByColumnAndRow(38, $row)->getValue(),

							'ACU_QUANTITY' => $worksheet->getCellByColumnAndRow(39, $row)->getValue(),
							'ACU_CAPACITY_HP' => $worksheet->getCellByColumnAndRow(40, $row)->getValue(),
							'ACU_TYPE' => $worksheet->getCellByColumnAndRow(41, $row)->getValue(),
							'ACU_APPROX_LOAD' => $worksheet->getCellByColumnAndRow(42, $row)->getValue(),
							'OTHER_AC_LOAD_AAC' => $worksheet->getCellByColumnAndRow(43, $row)->getValue(),
							'DROP_WIRES' => $worksheet->getCellByColumnAndRow(44, $row)->getValue(),
							'W_RMS' => $worksheet->getCellByColumnAndRow(45, $row)->getValue(),
							'GENSET_EUL' => $worksheet->getCellByColumnAndRow(46, $row)->getValue(),
							'GENSET_INSTALLATION_DATE' => $worksheet->getCellByColumnAndRow(47, $row)->getValue(),

							'BUILD_SCOPE_A' => $worksheet->getCellByColumnAndRow(48, $row)->getValue(),
							'BUILD_SCOPE_B' => $worksheet->getCellByColumnAndRow(49, $row)->getValue(),
							'BUILD_SCOPE_C' => $worksheet->getCellByColumnAndRow(50, $row)->getValue(),
							'BUILD_SCOPE_D' => $worksheet->getCellByColumnAndRow(51, $row)->getValue(),
							'BUILD_REMARKS' => $worksheet->getCellByColumnAndRow(52, $row)->getValue(),
							'BUILD_COMPLETION' => $worksheet->getCellByColumnAndRow(53, $row)->getValue(),
						);
					}
				}

				#CLEAR DATA BEFORE INSERT
				$this->db->truncate($this->tablename);
				$this->db->insert_batch($this->tablename, array_filter($data));

				#GET COMPLETION
				$this->db->select('id');
				$tobe_update = $this->db->get($this->tablename)->result();
				$power_id = array();
				foreach ($tobe_update as $key)
				{
					array_push($power_id, $key->id);
				}

				$this->update_data_powerdb($power_id);

				if ($this->db->affected_rows() == 0) {
					$json = array(
						'title' => 'Oops!',
						'msg'   => 'Upload failed.',
						'type'  => 'error',
					);
				} else {
					$json = array(
						'title' => 'Success',
						'msg'   => 'Successfully uploaded.',
						'type'  => 'success',
					);
				}

			} else {
				$json = array(
					'title' => 'Oops!',
					'msg'   => 'Import file does not match to desired format.',
					'type'  => 'error'
				);
			}
		} else {
			$json = array(
				'title' => 'Ooops!',
				'msg'   => 'Something went wrong.',
				'type'  => 'error'
			);
		}
		return $json;
	}

	public function getTableFields($database_table)
	{
		return $this->db->list_fields($database_table);
	}

	public function get_power()
	{
		$geoarea = $this->session->userdata('geoarea');
		$tablename = "powerdb_" . $geoarea;

		$this->db->order_by('SITENAME', 'ASC');
		$result = $this->db->get($tablename);
		return $result->result();
	}

	public function is_nonduplicate($existed_data_array, $new_data, $key_array)
	{

		foreach ($existed_data_array as $existed_key) {
			$score = 0;
			foreach ($key_array as $key) {
				if ($existed_key->$key == $new_data[$key]) {
					$score++;
				}
			}
			if ($score == count($key_array)) {
				return 0;
			}
		}
		return 1;
	}

	public function update_data_powerdb($id)
	{
		set_time_limit(0);
		foreach ($id as $value)
		{
			$add_array = $this->Site_database->getSingle($this->tablename, array("id" => $value), "*");

			$tobeupdatedcellcount = 0;
			$tempcount = 1;
			foreach ($add_array as $valuetobe)
			{
				if (strtolower($valuetobe) == "to be updated" || $valuetobe == null || $valuetobe == " ")
				{
					$tobeupdatedcellcount++;
				}
				if ($tempcount >= 46)
				{
					break;
				}
				$tempcount++;
			}
			$add_array->completion = number_format((46 - $tobeupdatedcellcount) * (100 / 46), 2);

			if($add_array->completion==100)
			{
				$add_array->BATTERY_CHARGING_COEFFICIENT = $this->charging_rate_calculation($add_array->GRID_STATUS, $add_array->TRANSFORMER_CAPACITY_KVA, $add_array->GENSET_CAPACITY_KVA);

				$add_array->RS1_DC_HEADROOM_W = $this->rectifier_calculation($add_array->RECTIFIER_1, $add_array->SITE_CLASS, $add_array->RS1_MODULE_CAPACITY, $add_array->RS1_NO_OF_MODULES, $add_array->RS1_ACTUAL_LOAD_ADC, $add_array->RS1_NO_OF_EMPTY_MODULE);

				$add_array->RS1_BATTERY_HOURS = $this->battery_hr_calculation($add_array->RS1_BATTERY_CAPACITY, $add_array->RS1_ACTUAL_LOAD_ADC,$add_array->RS1_BATTERY_TYPE);

				$add_array->RS2_DC_HEADROOM_W = $this->rectifier_calculation($add_array->RECTIFIER_2, $add_array->SITE_CLASS, $add_array->RS2_MODULE_CAPACITY, $add_array->RS2_NO_OF_MODULES, $add_array->RS2_ACTUAL_LOAD_ADC, $add_array->RS2_NO_OF_EMPTY_MODULE);

				$add_array->RS2_BATTERY_HOURS = $this->battery_hr_calculation($add_array->RS2_BATTERY_CAPACITY, $add_array->RS2_ACTUAL_LOAD_ADC,$add_array->RS2_BATTERY_TYPE);

				$add_array->RS3_DC_HEADROOM_W = $this->rectifier_calculation($add_array->RECTIFIER_3, $add_array->SITE_CLASS, $add_array->RS3_MODULE_CAPACITY, $add_array->RS3_NO_OF_MODULES, $add_array->RS3_ACTUAL_LOAD_ADC, $add_array->RS3_NO_OF_EMPTY_MODULE);

				$add_array->RS3_BATTERY_HOURS = $this->battery_hr_calculation($add_array->RS3_BATTERY_CAPACITY, $add_array->RS3_ACTUAL_LOAD_ADC,$add_array->RS3_BATTERY_TYPE);

				$add_array->TOTAL_AC_LOAD_A = $this->sum_calculation(array($this->adc_to_aac($add_array->RS1_ACTUAL_LOAD_ADC), $this->adc_to_aac($add_array->RS2_ACTUAL_LOAD_ADC), $this->adc_to_aac($add_array->RS3_ACTUAL_LOAD_ADC), $this->battery_charging_load_ac($add_array->RS1_BATTERY_CAPACITY, $add_array->BATTERY_CHARGING_COEFFICIENT), $this->battery_charging_load_ac($add_array->RS2_BATTERY_CAPACITY, $add_array->BATTERY_CHARGING_COEFFICIENT), $this->battery_charging_load_ac($add_array->RS3_BATTERY_CAPACITY, $add_array->BATTERY_CHARGING_COEFFICIENT), $this->aircon_approx_calculation($add_array->ACU_APPROX_LOAD, $add_array->ACU_QUANTITY), $add_array->OTHER_AC_LOAD_AAC));


				$add_array->TRANSFORMER_HEADROOM_W = $add_array->TRANSFORMER_CAPACITY_KVA == "NO DATA" ? " NO DATA" : $this->kva_to_watts($add_array->TRANSFORMER_CAPACITY_KVA, $add_array->TOTAL_AC_LOAD_A);

				$add_array->ECB_HEADROOM_A = $add_array->MDP_ECB_RATING_AT == "NO DATA" ? " NO DATA" : $this->amphere_trip($add_array->MDP_ECB_RATING_AT, $add_array->TOTAL_AC_LOAD_A);

				$add_array->ACPDB_HEADROOM_A = $add_array->ACPDB_RATING_AT == "NO DATA" ? " NO DATA" : $this->amphere_trip($add_array->ACPDB_RATING_AT, $add_array->TOTAL_AC_LOAD_A);

				$add_array->GENSET_HEADROOM_W = $add_array->GENSET_CAPACITY_KVA == "NO DATA" ? " NO DATA" : $this->kva_to_watts($add_array->GENSET_CAPACITY_KVA, $add_array->TOTAL_AC_LOAD_A);

				$add_array->REMAINING_WATTAGE_HEADROOM_DC = $this->sum_calculation(array($add_array->RS1_DC_HEADROOM_W, $add_array->RS2_DC_HEADROOM_W, $add_array->RS3_DC_HEADROOM_W));

				$add_array->REMAINING_WATTAGE_HEADROOM_AC = $this->remaining_watt_headroom_min($add_array->TRANSFORMER_HEADROOM_W, $add_array->GENSET_HEADROOM_W, $add_array->ECB_HEADROOM_A, $add_array->ACPDB_HEADROOM_A);

				$add_array->TRANSFORMER_UTIL = $this->util_calculation($this->use_ecb_clamp($add_array->TOTAL_AC_LOAD_A, $add_array->ECB_CLAMP_READING_AAC), $add_array->TRANSFORMER_CAPACITY_KVA, $add_array->POWER_PHASE, "KVA", "NO");
				$add_array->GENSET_UTIL = $this->util_calculation($this->use_ecb_clamp($add_array->TOTAL_AC_LOAD_A, $add_array->ECB_CLAMP_READING_AAC), $add_array->GENSET_CAPACITY_KVA, $add_array->POWER_PHASE, "KVA", "YES");
				$add_array->ECB_UTIL = $this->util_calculation($this->use_ecb_clamp($add_array->TOTAL_AC_LOAD_A, $add_array->ECB_CLAMP_READING_AAC), $add_array->MDP_ECB_RATING_AT, $add_array->POWER_PHASE, "AT", "NO");
				$add_array->ACPDB_UTIL = $this->util_calculation($this->use_ecb_clamp($add_array->TOTAL_AC_LOAD_A, $add_array->ECB_CLAMP_READING_AAC), $add_array->ACPDB_RATING_AT, $add_array->POWER_PHASE, "AT", "NO");
				$add_array->SITE_UTIL = $this->site_util_max($add_array->TRANSFORMER_UTIL, $add_array->GENSET_UTIL, $add_array->ECB_UTIL, $add_array->ACPDB_UTIL);

				$add_array->RS1_ACTUAL_LOAD_ADC = strtolower($add_array->RS1_ACTUAL_LOAD_ADC) == 'to be updated' ? 0 : $add_array->RS1_ACTUAL_LOAD_ADC;
				$add_array->RS2_ACTUAL_LOAD_ADC = strtolower($add_array->RS2_ACTUAL_LOAD_ADC) == 'to be updated' ? 0 : $add_array->RS2_ACTUAL_LOAD_ADC;
				$add_array->RS3_ACTUAL_LOAD_ADC = strtolower($add_array->RS3_ACTUAL_LOAD_ADC) == 'to be updated' ? 0 : $add_array->RS3_ACTUAL_LOAD_ADC;

				$add_array->RS1_MODULE_CAPACITY = strtolower($add_array->RS1_MODULE_CAPACITY) == 'to be updated' ? 0 : $add_array->RS1_MODULE_CAPACITY;
				$add_array->RS2_MODULE_CAPACITY = strtolower($add_array->RS2_MODULE_CAPACITY) == 'to be updated' ? 0 : $add_array->RS2_MODULE_CAPACITY;
				$add_array->RS3_MODULE_CAPACITY = strtolower($add_array->RS3_MODULE_CAPACITY) == 'to be updated' ? 0 : $add_array->RS3_MODULE_CAPACITY;

				$add_array->RS1_NO_OF_MODULES   = strtolower($add_array->RS1_NO_OF_MODULES) == 'to be updated' ? 0 : $add_array->RS1_NO_OF_MODULES;
				$add_array->RS2_NO_OF_MODULES   = strtolower($add_array->RS2_NO_OF_MODULES) == 'to be updated' ? 0 : $add_array->RS2_NO_OF_MODULES;	
				$add_array->RS3_NO_OF_MODULES   = strtolower($add_array->RS3_NO_OF_MODULES) == 'to be updated' ? 0 : $add_array->RS3_NO_OF_MODULES;

				$add_array->RS1_MODULE_UTIL = $this->rectifier_util($add_array->RS1_ACTUAL_LOAD_ADC, $add_array->RS1_MODULE_CAPACITY, $add_array->RS1_NO_OF_MODULES);

				$add_array->RS2_MODULE_UTIL = $this->rectifier_util($add_array->RS2_ACTUAL_LOAD_ADC, $add_array->RS2_MODULE_CAPACITY, $add_array->RS2_NO_OF_MODULES);

				$add_array->RS3_MODULE_UTIL = $this->rectifier_util($add_array->RS3_ACTUAL_LOAD_ADC, $add_array->RS3_MODULE_CAPACITY, $add_array->RS3_NO_OF_MODULES);

				$add_array->RDC_UTIL = $this->rdc_util($add_array->RS1_ACTUAL_LOAD_ADC, $add_array->RS2_ACTUAL_LOAD_ADC, $add_array->RS3_ACTUAL_LOAD_ADC, $add_array->RS1_MODULE_CAPACITY, $add_array->RS2_MODULE_CAPACITY, $add_array->RS3_MODULE_CAPACITY, $add_array->RS1_NO_OF_MODULES, $add_array->RS2_NO_OF_MODULES, $add_array->RS3_NO_OF_MODULES);

				$add_array->RDC_HR = $this->rdc_hr(array($add_array->RS1_BATTERY_HOURS,$add_array->RS2_BATTERY_HOURS,$add_array->RS3_BATTERY_HOURS));
			}

			$add_array->date = date('Y-m-d');

			$this->db->update($this->tablename, $add_array, array("id" => $value));
		}
		return $this->db->affected_rows();
	}

	public function aircon_approx_calculation($aircon_load_aac = 0, $acu_quantity = 0)
	{
		$aircon_load_aac = is_numeric($aircon_load_aac) ? $aircon_load_aac : 0;
		$acu_quantity = is_numeric($acu_quantity) ? $acu_quantity : 0;
		return ($acu_quantity*$aircon_load_aac) + (0.25*$aircon_load_aac);
	}

	public function charging_rate_calculation($grid_status, $transformer_capacity, $genset_capacity)
	{
		$val = 0.05;
		if ($grid_status !== "G1") {
			$val = 0.1;
		}
		if ($transformer_capacity == 0 || $genset_capacity == 0) {
			$val = 0.1;
		}

		return $val;
//		else {
//			return 0.05;
//		}
	}

	public function rectifier_calculation($rectifier_number, $site_class, $rectifier_capacity, $rectifier_module, $rectifier_actual, $rectifier_empty)
	{
		if ($this->datanum_validator(array($rectifier_capacity, $rectifier_module, $rectifier_empty, $rectifier_actual)) == "False") {
			return "none";
		}
		if ($rectifier_number == "0" || $rectifier_number == "" || $rectifier_number == "To be Updated") {
			return 0;
		} elseif ($site_class == "A" || $site_class == "A1" || $site_class == "A2" || $site_class == "A3" || $site_class == "B1") {
			return $rectifier_capacity * 0.4 * ($rectifier_module + $rectifier_empty) - ($rectifier_actual * 53.4);
		} else {
			return $rectifier_capacity * 0.75 * ($rectifier_module + $rectifier_empty) - ($rectifier_actual * 53.4);
		}
	}

	public function datanum_validator($data_array)
	{
		foreach ($data_array as $value) {
			if (is_numeric($value) == "") {
				return "False";
			}
		}
		return "True";
	}

	public function battery_hr_calculation($battery_capacity, $battery_amp,$battery_type)
	{
		$multiplier =1;
		if ($this->datanum_validator(array($battery_capacity, $battery_amp)) == "False") {
			return 0;
		}

		if ($battery_capacity == 0 || $battery_amp == 0) {
			return 0;
		}

		if($battery_type=="Li-ion")
		{
			$multiplier = 0.95;
		}
		return number_format(($battery_capacity*$multiplier) / $battery_amp, 2);
	}

	public function sum_calculation($data_array)
	{
		if ($this->datanum_validator($data_array) == "False") {
			return "none";
		}
		$sum = 0;
		foreach ($data_array as $value) {
			$sum += $value;
			//echo $value."<br>";
		}
		return number_format($sum, 2);
	}

	public function adc_to_aac($load_adc)
	{
		if ($this->datanum_validator(array($load_adc)) == "False") {
			return 0;
		}
		return $load_adc * 53.4 / 230;
	}

	public function battery_charging_load_ac($battery_capacity, $charging_rate)
	{
		if ($this->datanum_validator(array($battery_capacity, $charging_rate)) == "False") {
			return 0;
		}
		return $battery_capacity * $charging_rate * 53.4 / 230;
	}

	public function kva_to_watts($kva, $amp_load)
	{
		if ($this->datanum_validator(array($kva)) == "False") {
			return 0;
		}
		if ($this->datanum_validator(array($amp_load)) == "False") {
			return 0;
		}
		return ($kva * 1000 * 0.9) - ($amp_load * 230);
	}

	public function amphere_trip($amp, $load)
	{
		if ($this->datanum_validator(array($amp)) == "False") {
			return 0;
		}
		if ($this->datanum_validator(array($load)) == "False") {
			return 0;
		}
		return ($amp * 0.8) - $load;

	}

	public function remaining_watt_headroom_min($t_head, $g_head, $e_head, $a_head)
	{
		if ($this->datanum_validator(array($t_head, $g_head, $e_head, $a_head)) == "False") {
			return "none";
		}
		$min_array = array();
		if ($t_head != 0) {
			array_push($min_array, $t_head);
		}
		if ($g_head != 0) {
			array_push($min_array, $g_head);
		}
		if ($e_head != 0) {
			array_push($min_array, $e_head * 230);
		}
		if ($a_head != 0) {
			array_push($min_array, $a_head * 230);
		}
		if (empty($min_array)) {
			return "none";
		}

		return min($min_array);
	}

	public function util_calculation($amp, $capacity, $phase, $unit, $genset)
	{
		$phase_multiplier = 0;
		$phase_root = 0;

		if ($this->datanum_validator(array($amp, $capacity)) == "False") {
			return "none";
		}
		if($capacity==0){
			return 0;
		}
		$phase=strtoupper($phase);
		switch ($phase) {
			case 'THREE PHASE':
				$phase_multiplier = 3;
				$phase_root = 1.732;
				
				break;

			default:
				$phase_multiplier = 1;
				$phase_root = 1;
				
				break;
		}
		if ($genset == "YES") {
			$phase_multiplier = 1;
			$phase_root = 1;

		}
		if($genset=="YES" && $phase =="THREE PHASE")
		{
			$mul = 1;
		}
		else
		{
			$mul =1;
		}
		switch ($unit) {
			case 'AT':
				return number_format($amp / ($capacity * $phase_root) * 100, 2);
				break;
			case 'KVA':
				return number_format($amp / ($capacity * $phase_multiplier) / 1000 * 230 * 100, 2);

			default:
				# code...
				break;
		}
	}

	public function use_ecb_clamp($status, $ecb_clamp)
	{
		if ($status == "none") {
			return $ecb_clamp;
		}
		return $status;
	}

	public function site_util_max($t_util, $g_util, $e_util, $a_util)
	{
		/*		if($this->datanum_validator(array($t_util,$g_util,$e_util,$a_util))=="False")
				{
					return "To be Updated";
				}*/
		$max_array = array();
		if ($t_util != 0 || $this->datanum_validator(array($t_util)) != "False") {
			array_push($max_array, $t_util);
		}
		if ($g_util != 0 || $this->datanum_validator(array($g_util)) != "False") {
			array_push($max_array, $g_util);
		}
		if ($e_util != 0 || $this->datanum_validator(array($e_util)) != "False") {
			array_push($max_array, $e_util);
		}
		if ($a_util != 0 || $this->datanum_validator(array($a_util)) != "False") {
			array_push($max_array, $a_util);
		}
		if (empty($max_array)) {
			return "none";
		}
		return max($max_array);
	}

	public function rectifier_util($r_adc, $r_cap, $r_num = 0)
	{
		if ($this->datanum_validator(array($r_adc, $r_cap, $r_num)) == "False") {
			return "none";
		}
		if ($r_adc == 0 || $r_cap == 0 || $r_num == 0) {
			return "none";
		}

		return number_format((($r_adc * 53.4) / ($r_cap * $r_num)) * 100, 2);

	}

	public function rdc_util($r1_adc = 0, $r2_adc = 0, $r3_adc = 0, $r1_cap = 0, $r2_cap = 0, $r3_cap = 0, $r1_nummod = 0, $r2_nummod = 0, $r3_nummod = 0)
	{
		//$adc_total = 0;
		//$cap_total = 0;

		$adc_total = $this->is_number($r1_adc) + $this->is_number($r2_adc) + $this->is_number($r3_adc);
		$cap_total = ($this->is_number($r1_cap) * $this->is_number($r1_nummod)) + ($this->is_number($r2_cap) * $this->is_number($r2_nummod)) + ($this->is_number($r3_cap) * $this->is_number($r3_nummod));

		if ($adc_total == 0 || $cap_total == 0) {
			return "none";
		}
		return number_format(($adc_total * 53.4 / $cap_total) * 100, 2);
	}

	public function is_number($num)
	{
		if (is_numeric($num) == "") {
			return 0;
		}
		return $num;
	}

	public function rdc_hr($batt_hr_array)
	{
		/*$adc_total = $this->is_number($r1_adc) + $this->is_number($r2_adc) + $this->is_number($r3_adc);
		$bat_total = $this->is_number($r1_bat) + $this->is_number($r2_bat) + $this->is_number($r3_bat);

		if ($adc_total == 0 || $bat_total == 0) {
			return "none";
		}
		return number_format(($bat_total / $adc_total), 2);*/
		$sum_hr=0;
		foreach ($batt_hr_array as $batt_hr)
		{
			if(is_numeric($batt_hr)!="")
			{
				$sum_hr += $batt_hr;
			}
		}
		return number_format($sum_hr,2);
	}

	public function check_site_name()
	{
		$sname = $this->input->post('SITENAME');
		$plaid = $this->input->post('PLAID');
		$this->db->where('SITENAME', trim($sname));
		$this->db->or_where('PLAID', trim($plaid));
		$result = $this->db->get($this->tablename);
		return $result->num_rows() > 0 ? true : false;
 	}

	public function add_data_powerdb($add_array)
	{
		$geoarea = $this->session->userdata('geoarea');
		set_time_limit(0);
		$tablename = "powerdb_" . $geoarea;

		$add_array["BATTERY_CHARGING_COEFFICIENT"] = $this->charging_rate_calculation($add_array['GRID_STATUS'], $add_array["TRANSFORMER_CAPACITY_KVA"], $add_array["GENSET_CAPACITY_KVA"]);

		$add_array["RS1_DC_HEADROOM_W"] = $this->rectifier_calculation($add_array['RECTIFIER_1'], $add_array['SITE_CLASS'], $add_array['RS1_MODULE_CAPACITY'], $add_array['RS1_NO_OF_MODULES'], $add_array['RS1_ACTUAL_LOAD_ADC'], $add_array['RS1_NO_OF_EMPTY_MODULE']);

		$add_array["RS1_BATTERY_HOURS"] = $this->battery_hr_calculation($add_array['RS1_BATTERY_CAPACITY'], $add_array['RS1_ACTUAL_LOAD_ADC'],$add_array["RS1_BATTERY_TYPE"]);

		$add_array["RS2_DC_HEADROOM_W"] = $this->rectifier_calculation(isset($add_array['RECTIFIER_2']) ? $add_array['RECTIFIER_2'] : 'None', $add_array['SITE_CLASS'], isset($add_array['RS2_MODULE_CAPACITY']) ? $add_array['RS2_MODULE_CAPACITY'] : 'None', isset($add_array['RS2_NO_OF_MODULES']) ? $add_array['RS2_NO_OF_MODULES'] : 'None', isset($add_array['RS2_ACTUAL_LOAD_ADC']) ? $add_array['RS2_ACTUAL_LOAD_ADC'] : 'None', isset($add_array['RS2_NO_OF_EMPTY_MODULE']) ? $add_array['RS2_NO_OF_EMPTY_MODULE'] : 'None');

		$add_array["RS2_BATTERY_HOURS"] = $this->battery_hr_calculation(isset($add_array['RS2_BATTERY_CAPACITY']) ? $add_array['RS2_BATTERY_CAPACITY'] : 'None', isset($add_array['RS2_ACTUAL_LOAD_ADC']) ? $add_array['RS2_ACTUAL_LOAD_ADC'] : 'None', isset($add_array['RS2_BATTERY_TYPE']) ? $add_array['RS2_BATTERY_TYPE'] : 'None');

		$add_array["RS3_DC_HEADROOM_W"] = $this->rectifier_calculation(isset($add_array['RECTIFIER_3']) ? $add_array['RECTIFIER_3'] : 'None', $add_array['SITE_CLASS'], isset($add_array['RS3_MODULE_CAPACITY']) ? $add_array['RS3_MODULE_CAPACITY'] : 'None', isset($add_array['RS3_NO_OF_MODULES']) ? $add_array['RS3_NO_OF_MODULES'] : 'None', isset($add_array['RS3_ACTUAL_LOAD_ADC']) ? $add_array['RS3_ACTUAL_LOAD_ADC'] : 'None', isset($add_array['RS3_NO_OF_EMPTY_MODULE']) ? $add_array['RS3_NO_OF_EMPTY_MODULE'] : 'None');

		$add_array["RS3_BATTERY_HOURS"] = $this->battery_hr_calculation(isset($add_array['RS3_BATTERY_CAPACITY']) ? $add_array['RS3_BATTERY_CAPACITY'] : 'None', isset($add_array['RS3_ACTUAL_LOAD_ADC']) ? $add_array['RS3_ACTUAL_LOAD_ADC'] : 'None', isset($add_array['RS3_BATTERY_TYPE']) ? $add_array['RS3_BATTERY_TYPE'] : 'None');

		$add_array["TOTAL_AC_LOAD_A"] = $this->sum_calculation(array($this->adc_to_aac($add_array['RS1_ACTUAL_LOAD_ADC']), $this->adc_to_aac(isset($add_array['RS2_ACTUAL_LOAD_ADC']) ? $add_array['RS2_ACTUAL_LOAD_ADC'] : 'None'), $this->adc_to_aac(isset($add_array['RS3_ACTUAL_LOAD_ADC']) ? $add_array['RS3_ACTUAL_LOAD_ADC'] : 'None'), $this->battery_charging_load_ac($add_array['RS1_BATTERY_CAPACITY'], $add_array["BATTERY_CHARGING_COEFFICIENT"]), $this->battery_charging_load_ac(isset($add_array['RS2_BATTERY_CAPACITY']) ? $add_array['RS2_BATTERY_CAPACITY'] : 'None', $add_array["BATTERY_CHARGING_COEFFICIENT"]), $this->battery_charging_load_ac(isset($add_array['RS3_BATTERY_CAPACITY']) ? $add_array['RS3_BATTERY_CAPACITY'] : 'None', $add_array["BATTERY_CHARGING_COEFFICIENT"]), $this->aircon_approx_calculation(isset($add_array['ACU_APPROX_LOAD']) || strtolower($add_array['ACU_APPROX_LOAD']) <> 'none' ? $add_array['ACU_APPROX_LOAD'] : 0, isset($add_array['ACU_QUANTITY']) ? $add_array['ACU_QUANTITY'] : 0), isset($add_array['OTHER_AC_LOAD_AAC']) ? $add_array['OTHER_AC_LOAD_AAC'] : 0));

		$add_array["TRANSFORMER_HEADROOM_W"] = $add_array["TRANSFORMER_CAPACITY_KVA"] == "NO DATA" ? " NO DATA" : $this->kva_to_watts($add_array["TRANSFORMER_CAPACITY_KVA"], $add_array["TOTAL_AC_LOAD_A"]);

		$add_array["ECB_HEADROOM_A"] = $add_array["MDP_ECB_RATING_AT"] == "NO DATA" ? " NO DATA" : $this->amphere_trip($add_array["MDP_ECB_RATING_AT"], $add_array["TOTAL_AC_LOAD_A"]);

		$add_array["ACPDB_HEADROOM_A"] = $add_array["ACPDB_RATING_AT"] == "NO DATA" ? " NO DATA" : $this->amphere_trip($add_array["ACPDB_RATING_AT"], $add_array["TOTAL_AC_LOAD_A"]);

		$add_array["GENSET_HEADROOM_W"] = $add_array["GENSET_CAPACITY_KVA"] == "NO DATA" ? " NO DATA" : $this->kva_to_watts($add_array["GENSET_CAPACITY_KVA"], $add_array["TOTAL_AC_LOAD_A"]);

		$add_array["REMAINING_WATTAGE_HEADROOM_DC"] = $this->sum_calculation(array($add_array["RS1_DC_HEADROOM_W"], $add_array["RS2_DC_HEADROOM_W"], $add_array["RS3_DC_HEADROOM_W"]));

		$add_array["REMAINING_WATTAGE_HEADROOM_AC"] = $this->remaining_watt_headroom_min($add_array["TRANSFORMER_HEADROOM_W"], $add_array["GENSET_HEADROOM_W"], $add_array["ECB_HEADROOM_A"], $add_array["ACPDB_HEADROOM_A"]);

		$add_array["TRANSFORMER_UTIL"] = $this->util_calculation($this->use_ecb_clamp($add_array["TOTAL_AC_LOAD_A"], isset($add_array['ACU_APPROX_LOAD']) ? $add_array['ACU_APPROX_LOAD'] : 0), $add_array["TRANSFORMER_CAPACITY_KVA"], $add_array["POWER_PHASE"], "KVA", "NO");
		$add_array["GENSET_UTIL"] = $this->util_calculation($this->use_ecb_clamp($add_array["TOTAL_AC_LOAD_A"], isset($add_array['ACU_APPROX_LOAD']) ? $add_array['ACU_APPROX_LOAD'] : 0), $add_array["GENSET_CAPACITY_KVA"], $add_array["POWER_PHASE"], "KVA", "YES");
		$add_array["ECB_UTIL"] = $this->util_calculation($this->use_ecb_clamp($add_array["TOTAL_AC_LOAD_A"], isset($add_array['ACU_APPROX_LOAD']) ? $add_array['ACU_APPROX_LOAD'] : 0), $add_array["MDP_ECB_RATING_AT"], $add_array["POWER_PHASE"], "AT", "NO");
		$add_array["ACPDB_UTIL"] = $this->util_calculation($this->use_ecb_clamp($add_array["TOTAL_AC_LOAD_A"], isset($add_array['ACU_APPROX_LOAD']) ? $add_array['ACU_APPROX_LOAD'] : 0), $add_array["ACPDB_RATING_AT"], $add_array["POWER_PHASE"], "AT", "NO");
		$add_array["SITE_UTIL"] = $this->site_util_max($add_array["TRANSFORMER_UTIL"], $add_array["GENSET_UTIL"], $add_array["ECB_UTIL"], $add_array["ACPDB_UTIL"]);
		
		$add_array["RS1_MODULE_UTIL"] = $this->rectifier_util($add_array["RS1_ACTUAL_LOAD_ADC"], $add_array["RS1_MODULE_CAPACITY"], $add_array["RS1_NO_OF_MODULES"]);

		$add_array["RS2_MODULE_UTIL"] = $this->rectifier_util(isset($add_array['RS2_ACTUAL_LOAD_ADC']) ? $add_array['RS2_ACTUAL_LOAD_ADC'] : 'None', isset($add_array['RS2_MODULE_CAPACITY']) ? $add_array['RS2_MODULE_CAPACITY'] : 'None', isset($add_array['RS2_NO_OF_MODULES']) ? $add_array['RS2_NO_OF_MODULES'] : 'None');

		$add_array["RS3_MODULE_UTIL"] = $this->rectifier_util(isset($add_array['RS3_ACTUAL_LOAD_ADC']) ? $add_array['RS3_ACTUAL_LOAD_ADC'] : 'None', isset($add_array['RS3_MODULE_CAPACITY']) ? $add_array['RS3_MODULE_CAPACITY'] : 'None', $add_array["RS1_NO_OF_MODULES"]);

		#IF NONE FOR RS2
		$add_array["RECTIFIER_2"] = isset($add_array['RECTIFIER_2']) ? $add_array['RECTIFIER_2'] : 'None';
		$add_array["RS2_MODULE_CAPACITY"] = isset($add_array['RS2_MODULE_CAPACITY']) ? $add_array['RS2_MODULE_CAPACITY'] : 'None';
		$add_array["RS2_NO_OF_MODULES"] = isset($add_array['RS2_NO_OF_MODULES']) ? $add_array['RS2_NO_OF_MODULES'] : 'None';
		$add_array["RS2_NO_OF_EMPTY_MODULE"] = isset($add_array['RS2_NO_OF_EMPTY_MODULE']) ? $add_array['RS2_NO_OF_EMPTY_MODULE'] : 'None';
		$add_array["RS2_ACTUAL_LOAD_ADC"] = isset($add_array['RS2_ACTUAL_LOAD_ADC']) ? $add_array['RS2_ACTUAL_LOAD_ADC'] : 'None';
		$add_array["RS2_BATTERY_BRAND"] = isset($add_array['RS2_BATTERY_BRAND']) ? $add_array['RS2_BATTERY_BRAND'] : 'None';
		$add_array["RS2_BATTERY_TYPE"] = isset($add_array['RS2_BATTERY_TYPE']) ? $add_array['RS2_BATTERY_TYPE'] : 'None';
		$add_array["RS2_BATTERY_CAPACITY"] = isset($add_array['RS2_BATTERY_CAPACITY']) ? $add_array['RS2_BATTERY_CAPACITY'] : 'None';
		#FOR RS3
		$add_array["RECTIFIER_3"] = isset($add_array['RECTIFIER_3']) ? $add_array['RECTIFIER_3'] : 'None';
		$add_array["RS3_MODULE_CAPACITY"] = isset($add_array['RS3_MODULE_CAPACITY']) ? $add_array['RS3_MODULE_CAPACITY'] : 'None';
		$add_array["RS3_NO_OF_MODULES"] = isset($add_array['RS3_NO_OF_MODULES']) ? $add_array['RS3_NO_OF_MODULES'] : 'None';
		$add_array["RS3_NO_OF_EMPTY_MODULE"] = isset($add_array['RS3_NO_OF_EMPTY_MODULE']) ? $add_array['RS3_NO_OF_EMPTY_MODULE'] : 'None';
		$add_array["RS3_ACTUAL_LOAD_ADC"] = isset($add_array['RS3_ACTUAL_LOAD_ADC']) ? $add_array['RS3_ACTUAL_LOAD_ADC'] : 'None';
		$add_array["RS3_BATTERY_BRAND"] = isset($add_array['RS3_BATTERY_BRAND']) ? $add_array['RS3_BATTERY_BRAND'] : 'None';
		$add_array["RS3_BATTERY_TYPE"] = isset($add_array['RS3_BATTERY_TYPE']) ? $add_array['RS3_BATTERY_TYPE'] : 'None';
		$add_array["RS3_BATTERY_CAPACITY"] = isset($add_array['RS3_BATTERY_CAPACITY']) ? $add_array['RS3_BATTERY_CAPACITY'] : 'None';
		#ACU IF OUTDOOR
		$add_array["ACU_QUANTITY"] = isset($add_array['ACU_QUANTITY']) ? $add_array['ACU_QUANTITY'] : 0;
		$add_array["ACU_CAPACITY_HP"] = isset($add_array['ACU_CAPACITY_HP']) ? $add_array['ACU_CAPACITY_HP'] : 'None';
		$add_array["ACU_TYPE"] = isset($add_array['ACU_TYPE']) ? $add_array['ACU_TYPE'] : 'None';
		$add_array["ACU_APPROX_LOAD"] = isset($add_array['ACU_APPROX_LOAD']) ? $add_array['ACU_APPROX_LOAD'] : 0;
		$add_array["OTHER_AC_LOAD_AAC"] = isset($add_array['OTHER_AC_LOAD_AAC']) ? $add_array['OTHER_AC_LOAD_AAC'] : 0;


		$add_array["RDC_UTIL"] = $this->rdc_util(array($add_array["RS1_ACTUAL_LOAD_ADC"], isset($add_array['RS2_ACTUAL_LOAD_ADC']) ? $add_array['RS2_ACTUAL_LOAD_ADC'] : 'None', isset($add_array['RS3_ACTUAL_LOAD_ADC']) ? $add_array['RS3_ACTUAL_LOAD_ADC'] : 'None'), array($add_array["RS1_MODULE_CAPACITY"], isset($add_array['RS2_MODULE_CAPACITY']) ? $add_array['RS2_MODULE_CAPACITY'] : 'None', isset($add_array['RS3_MODULE_CAPACITY']) ? $add_array['RS3_MODULE_CAPACITY'] : 'None'), array($add_array["RS1_NO_OF_MODULES"], isset($add_array['RS2_NO_OF_MODULES']) ? $add_array['RS2_NO_OF_MODULES'] : 'None', isset($add_array['RS3_NO_OF_MODULES']) ? $add_array['RS3_NO_OF_MODULES'] : 'None'));

		$add_array["RDC_HR"] = $this->rdc_hr(array($add_array["RS1_BATTERY_HOURS"],$add_array["RS2_BATTERY_HOURS"],$add_array["RS3_BATTERY_HOURS"]));

		$tobeupdatedcellcount = 0;
		$tempcount = 1;
		foreach ($add_array as $valuetobe) {
			if (strtolower($valuetobe) == "to be updated") {
				$tobeupdatedcellcount++;
			}
			if ($tempcount > 46) {
				break;
			}
			$tempcount++;
		}
		$add_array["completion"] = number_format((46 - $tobeupdatedcellcount) * (100 / 46));


			$add_array["date"] = date('Y-m-d');

		$this->db->insert($tablename, $add_array);
		return $this->db->affected_rows();
	}

	public function delete_powerdb($del_array)
	{
		$geoarea = $this->session->userdata('geoarea');
		set_time_limit(0);
		$tablename = "powerdb_" . $geoarea;
		$this->db->insert($tablename, $del_array);
	}

	public function batch_proposal($filename)
	{
		$geoarea = $this->session->userdata('geoarea');
		//$handle = fopen(site_url('upload/') . $filename, "r");
		$handle = fopen('upload/' . $filename, "r");
		set_time_limit(0);
		$proposed_array = array();
		$filesop = fgetcsv($handle, 1000, ',');
		$tablename = "powerdb_" . $geoarea;
		$i = 0;
		$count_update = 0;
		$ex_pro_data = array();
		$key = 0;
		$assesment = array();
		$y=0;
		$flag=0;
		$list = array('GEOAREA','SITENAME','PLAID','EQUIPMENT','DISMANTLE');
		foreach ($list as $value)
		{
			if ($y > 4)
			{
				break;
			}
				
			if($value != utf8_encode($filesop[$y]))
			{
				$flag++;
			}
			$y++;
		}

		if($flag>0)
		{
			fclose($handle);
			unlink('upload/' . $filename);
			return array(
				'title' => 'Oops!',
				'msg'   => 'Import file does not match to desired format.',
				'type'  => 'error'
			);
		}
		else
		{
			#INSERT PROPOSAL DATA
			$this->db->insert($this->tbl_prop, array('name' => strtoupper($this->geoarea).'_'.'Batch Proposal'.'_'.random_string('alnum',5)));
			$insert_id = $this->db->insert_id();

			while (($filesop = fgetcsv($handle, 1000, ',')) !== false)
			{
				$p_loads_array = array();
				$p_sitename = utf8_encode($filesop[1]);
				$p_plaid = utf8_encode($filesop[2]);
				$p_equipments = utf8_encode($filesop[3]);
				$p_dismantle = utf8_encode($filesop[4]);
				$equipment_details = array();
				$equipments = explode(",", trim($p_equipments));
				$dismantles = explode(",", trim($p_dismantle));
				
				$e_item=array();
				$d_item=array();
				$e_wattage=array();
				$d_wattage=array();

				$emp_dis = false;
				foreach ($dismantles as $d) {
					$this->db->select('WATTAGE');
					$item = $this->db->get_where("proposed_equipment",array("ITEM"=>$d));
					if($item->num_rows() > 0)
					{
						$item = $item->row();
						$d_item[] = $d;
						$d_wattage[]= $item->WATTAGE;
						$emp_dis = false;
					} else {
						$emp_dis = true;
					}
				}

				$emp_equip = false;
				foreach ($equipments as $x)
				{
					$this->db->select('WATTAGE');
					$item = $this->db->get_where("proposed_equipment",array("ITEM"=>$x));
					if($item->num_rows() > 0)
					{
						$item = $item->row();
						$e_item[] = $x;
						$e_wattage[]= $item->WATTAGE;
						$emp_equip = false;
					} else {
						$emp_equip = true;
					}
				}

				if (!empty($p_equipments)) {
					if($emp_equip) {
						$json = array(
							'title' => 'INVALID!',
							'msg'   => 'Equipment inputs does not match Brands->Equipments data.',
							'type'  => 'error',
						);
						fclose($handle);
						unlink('upload/' . $filename);

						return $json;
						break;
					}
				}
				
				if (!empty($p_dismantle)) {
					if ($emp_dis) {
						$json = array(
							'title' => 'INVALID!',
							'msg'   => 'Dismantled inputs does not match Brands->Equipments data.',
							'type'  => 'error',
							'date' => ''
						);

						fclose($handle);
						unlink('upload/' . $filename);

						return $json;
						break;
					} 
				}

				$equipment_details = array(
					"sitename"=> $p_sitename,
					"ITEM"=> $e_item,
					"WATTAGE"=> $e_wattage,
					"DISMANTLE_ITEM" => $d_item,
					"DISMANTLE_WATT" => $d_wattage,
				);

				$this->db->where("SITENAME", $p_sitename);
				$this->db->where('PLAID', $p_plaid);
				$this->db->where('completion', "100.00");
				$existing_data = $this->db->get($this->tablename);

				if($existing_data->num_rows() > 0)
				{
					$data = $existing_data->row();
					$rectifiers = array(
						"r1"=> array(
							"model"			=> $data->RECTIFIER_1,
							"mod_cap"		=> $data->RS1_MODULE_CAPACITY,
							"mod_active"	=> $data->RS1_NO_OF_MODULES,
							"mod_inactive"	=> $data->RS1_NO_OF_EMPTY_MODULE,
							"adc"			=> $data->RS1_ACTUAL_LOAD_ADC,
							"batt_brand"	=> $data->RS1_BATTERY_BRAND,
							"batt_type"		=> $data->RS1_BATTERY_TYPE,
							"batt_cap"		=> $data->RS1_BATTERY_CAPACITY,
							"headroom"		=> $data->RS1_DC_HEADROOM_W,
							"mod_util"		=> $data->RS1_MODULE_UTIL,
							"batt_hr"		=> $data->RS1_BATTERY_HOURS
						),
						"r2"=> array(
							"model"			=> $data->RECTIFIER_2,
							"mod_cap"		=> $data->RS2_MODULE_CAPACITY,
							"mod_active"	=> $data->RS2_NO_OF_MODULES,
							"mod_inactive"	=> $data->RS2_NO_OF_EMPTY_MODULE,
							"adc"			=> $data->RS2_ACTUAL_LOAD_ADC,
							"batt_brand"	=> $data->RS2_BATTERY_BRAND,
							"batt_type"		=> $data->RS2_BATTERY_TYPE,
							"batt_cap"		=> $data->RS2_BATTERY_CAPACITY,
							"headroom"		=> $data->RS2_DC_HEADROOM_W,
							"mod_util"		=> $data->RS2_MODULE_UTIL,
							"batt_hr"		=> $data->RS2_BATTERY_HOURS
						),
						"r3"=> array(
							"model"			=> $data->RECTIFIER_3,
							"mod_cap"		=> $data->RS3_MODULE_CAPACITY,
							"mod_active"	=> $data->RS3_NO_OF_MODULES,
							"mod_inactive"	=> $data->RS3_NO_OF_EMPTY_MODULE,
							"adc"			=> $data->RS3_ACTUAL_LOAD_ADC,
							"batt_brand"	=> $data->RS3_BATTERY_BRAND,
							"batt_type"		=> $data->RS3_BATTERY_TYPE,
							"batt_cap"		=> $data->RS3_BATTERY_CAPACITY,
							"headroom"		=> $data->RS3_DC_HEADROOM_W,
							"mod_util"		=> $data->RS3_MODULE_UTIL,
							"batt_hr"		=> $data->RS3_BATTERY_HOURS
						)
					);
					$assesment[] = $this->rectifier_recomendations($p_sitename,$p_plaid,$rectifiers,$equipment_details,$data, $filename, $insert_id);
				}
			}

			fclose($handle);
			unlink('upload/' . $filename);

			if (sizeof($assesment) == 0) {				
				#DELETE PROPOSAL DATA IF EMPTY
				$this->db->where('id', $insert_id);
				$this->db->delete($this->tbl_prop);
			} else {
				#INSERT DATA
				$this->insert_power_proposal($assesment);
			}
			return $assesment;
		}
		
	}

	public function rectifier_recomendations($sitename,$plaid,$rectifier,$equipments,$existing, $filename, $insert_id)
	{
		$rectifiers_remarks = array();

		//choose the higher headroom using max capacity
		//if headroom < 0, check for module upgrade else add RS
		
		//CLASS ALGO IDENTIFIER
		if($existing->SITE_CLASS=="A"||$existing->SITE_CLASS=="A1"||$existing->SITE_CLASS=="A2"||$existing->SITE_CLASS=="A3"||$existing->SITE_CLASS=="B1")
		{
			$class_multiplier = 0.4;
		}
		else
		{
			$class_multiplier = 0.75;
		}

		$batt_threshold = 0;
		if(strpos($existing->SITE_CLASS, "A") !== false)
		{
		    $batt_threshold =8;
		    $ah = '600AH';
		} 
		else if(strpos($existing->SITE_CLASS, "B") !== false)
		{
		    $batt_threshold =4;
			$ah = '600AH';
		} else {
			$ah = '450AH';
		}

		$r1_active = !is_numeric($rectifier["r1"]["mod_active"]) ? 0 : $rectifier["r1"]["mod_active"];
		$r1_inactive = !is_numeric($rectifier["r1"]["mod_inactive"]) ? 0 : $rectifier["r1"]["mod_inactive"];
		$r1_adc = !is_numeric($rectifier["r1"]["adc"]) ? 0 : $rectifier["r1"]["adc"];

		$r2_active = !is_numeric($rectifier["r2"]["mod_active"]) ? 0 : $rectifier["r2"]["mod_active"];
		$r2_inactive = !is_numeric($rectifier["r2"]["mod_inactive"]) ? 0 : $rectifier["r2"]["mod_inactive"];
		$r2_adc = !is_numeric($rectifier["r2"]["adc"]) ? 0 : $rectifier["r2"]["adc"];

		$r3_active = !is_numeric($rectifier["r3"]["mod_active"]) ? 0 : $rectifier["r3"]["mod_active"];
		$r3_inactive = !is_numeric($rectifier["r3"]["mod_inactive"]) ? 0 : $rectifier["r3"]["mod_inactive"];
		$r3_adc = !is_numeric($rectifier["r3"]["adc"]) ? 0 : $rectifier["r3"]["adc"];

		$r1_max_module = $r1_active + $r1_inactive;
		$r1_mod = $r1_active;
		$r1_cap = !is_numeric($rectifier["r1"]["mod_cap"]) ? 0 : $rectifier["r1"]["mod_cap"];
		$r1_cur_head = ($r1_cap * $r1_max_module *$class_multiplier) - ($r1_adc * 53.4);
		$r1_cur_wattage = ($r1_adc * 53.4);
		$r1_cur_adc = $rectifier["r1"]["adc"];
		$r1_cur_batt_type = $rectifier["r1"]["batt_type"];
		$r1_cur_batt_cap = $rectifier["r1"]["batt_cap"];

		$r2_max_module = $r2_active + $r2_inactive;
		$r2_mod = $r2_active;
		$r2_cap = !is_numeric($rectifier["r2"]["mod_cap"]) ? 0 : $rectifier["r2"]["mod_cap"];
		$r2_cur_head = ($r2_cap * $r2_max_module *$class_multiplier) - ($r2_adc * 53.4);
		$r2_cur_wattage = ($r2_adc * 53.4);
		$r2_cur_adc = $r2_adc;
		$r2_cur_batt_type = $rectifier["r2"]["batt_type"];
		$r2_cur_batt_cap = $rectifier["r2"]["batt_cap"];

		$r3_max_module = $r3_active + $r3_inactive;
		$r3_mod = $r3_active;
		$r3_cap = !is_numeric($rectifier["r3"]["mod_cap"]) ? 0 : $rectifier["r3"]["mod_cap"];
		$r3_cur_head = ($r3_cap * $r3_max_module *$class_multiplier) - ($r3_adc * 53.4);
		$r3_cur_wattage = ($r3_adc * 53.4);
		$r3_cur_adc = $r3_adc;
		$r3_cur_batt_type = $rectifier["r3"]["batt_type"];
		$r3_cur_batt_cap = $rectifier["r3"]["batt_cap"];
		$excluded_tech = '';
		$excess_wattage = '';
		$r1_proposed_module='';
		$r2_proposed_module='';
		$r3_proposed_module='';
		$r1_item='';
		$r2_item='';
		$r3_item='';
		$r1_d_item = '';
		$r2_d_item = '';
		$r3_d_item = '';
		$r1_remaining_headroom='none';
		$r2_remaining_headroom='none';
		$r3_remaining_headroom='none';
		$r1_battery_remarks='';
		$r2_battery_remarks='';
		$r3_battery_remarks='';
		$total_excess =0;

		$rec1 = array();

		$rec2 = array();

		$rec3 = array();
		$r1_proposed_wattage=0;
		$r2_proposed_wattage=0;
		$r3_proposed_wattage=0;

		$total_dismantle = 0;
		$total_equip = 0;
		for($x=0;$x<count($equipments["DISMANTLE_WATT"]);$x++)
		{

			$total_dismantle += $equipments["DISMANTLE_WATT"][$x];

			$r1_percent= number_format($equipments["DISMANTLE_WATT"][$x]/($r1_cur_head - $equipments["DISMANTLE_WATT"][$x]),3);
			$r2_percent= number_format($equipments["DISMANTLE_WATT"][$x]/($r2_cur_head - $equipments["DISMANTLE_WATT"][$x]),3);
			$r3_percent= number_format($equipments["DISMANTLE_WATT"][$x]/($r3_cur_head - $equipments["DISMANTLE_WATT"][$x]),3);


			if($r1_percent<0)
			{
				$r1_percent = 100;
			}
			if($r2_percent<0)
			{
				$r2_percent = 100;
			}
			if($r3_percent<0)
			{
				$r3_percent = 100;
			}


			if( $r1_cur_head>=$equipments["DISMANTLE_WATT"][$x] && $r1_percent < $r2_percent && $r1_percent < $r3_percent)
			{
				$r1_cur_head -= $equipments["DISMANTLE_WATT"][$x];
				$r1_d_item .= $equipments["DISMANTLE_ITEM"][$x]." ";
				$r1_remaining_headroom = number_format($r1_cur_head,2);
				$r1_proposed_wattage -= $equipments["DISMANTLE_WATT"][$x];

			}

			else if($r2_cur_head>=$equipments["DISMANTLE_WATT"][$x] && $r2_percent < $r1_percent && $r2_percent < $r3_percent)
			{
				$r2_cur_head -= $equipments["DISMANTLE_WATT"][$x];
				$r2_d_item .= $equipments["DISMANTLE_ITEM"][$x]." ";
				$r2_remaining_headroom = number_format($r2_cur_head,2);
				$r2_proposed_wattage -= $equipments["DISMANTLE_WATT"][$x];
			}

			else if($r3_cur_head>=$equipments["DISMANTLE_WATT"][$x] && $r3_percent < $r1_percent && $r3_percent < $r2_percent)
			{
				$r3_cur_head -= $equipments["DISMANTLE_WATT"][$x];
				$r3_d_item .= $equipments["DISMANTLE_ITEM"][$x]." ";
				$r3_remaining_headroom = number_format($r3_cur_head,2);
				$r3_proposed_wattage -= $equipments["DISMANTLE_WATT"][$x];
			}
			else if($r1_cur_head < $equipments["DISMANTLE_WATT"][$x] && $r2_cur_head < $equipments["DISMANTLE_WATT"][$x] && $r3_cur_head < $equipments["WATTAGE"][$x])
			{
//				$excluded_tech .= $equipments["DISMANTLE_ITEM"][$x]." ";
//				$excess_wattage .= $equipments["DISMANTLE_WATT"][$x]." ";
//				$total_excess += $equipments["DISMANTLE_WATT"][$x];
			}

		}

		//CALCULATION FOR EQUIPMENT TECH WATTAGE AND RECTIFIER OPTIMIZED DISTRIBUTION
		for($x=0;$x<count($equipments["WATTAGE"]);$x++)
		{
			$total_equip += $equipments["WATTAGE"][$x];
			$r1_percent= number_format($equipments["WATTAGE"][$x]/($r1_cur_head - $equipments["WATTAGE"][$x]),3);
			$r2_percent= number_format($equipments["WATTAGE"][$x]/($r2_cur_head - $equipments["WATTAGE"][$x]),3);
			$r3_percent= number_format($equipments["WATTAGE"][$x]/($r3_cur_head - $equipments["WATTAGE"][$x]),3);


			if($r1_percent<0)
			{
				$r1_percent = 100;
			}
			if($r2_percent<0)
			{
				$r2_percent = 100;
			}
			if($r3_percent<0)
			{
				$r3_percent = 100;
			}


			if( $r1_cur_head>=$equipments["WATTAGE"][$x] && $r1_percent < $r2_percent && $r1_percent < $r3_percent)
			{
				$r1_cur_head -= $equipments["WATTAGE"][$x];
				$r1_item .= $equipments["ITEM"][$x]." ";
				$r1_remaining_headroom = number_format($r1_cur_head,2);
				$r1_proposed_wattage += $equipments["WATTAGE"][$x];
				
			}

			else if($r2_cur_head>=$equipments["WATTAGE"][$x] && $r2_percent < $r1_percent && $r2_percent < $r3_percent)
			{
				$r2_cur_head -= $equipments["WATTAGE"][$x];
				$r2_item .= $equipments["ITEM"][$x]." ";
				$r2_remaining_headroom = number_format($r2_cur_head,2);
				$r2_proposed_wattage += $equipments["WATTAGE"][$x];
			}

			else if($r3_cur_head>=$equipments["WATTAGE"][$x] && $r3_percent < $r1_percent && $r3_percent < $r2_percent)
			{
				$r3_cur_head -= $equipments["WATTAGE"][$x];
				$r3_item .= $equipments["ITEM"][$x]." ";
				$r3_remaining_headroom = number_format($r3_cur_head,2);
				$r3_proposed_wattage += $equipments["WATTAGE"][$x];
			}
			else if($r1_cur_head < $equipments["WATTAGE"][$x] && $r2_cur_head < $equipments["WATTAGE"][$x] && $r3_cur_head < $equipments["WATTAGE"][$x])
			{
				$excluded_tech .= $equipments["ITEM"][$x]." ";
				$excess_wattage .= $equipments["WATTAGE"][$x]." ";
				$total_excess += $equipments["WATTAGE"][$x];
			}

		}

		$total_aac_load = round(($total_equip - $total_dismantle) / 230,2);

		//ADDITIONAL RECTIFIER MODULE
		if($r1_cap>0 && $r1_proposed_wattage>0)
		{
			if(ceil(($r1_proposed_wattage+$r1_cur_wattage)/($r1_cap*$class_multiplier))-$r1_mod<0)
			{
				$r1_proposed_module = 0;
			}
			else
			{
				$r1_proposed_module =ceil(($r1_proposed_wattage+$r1_cur_wattage)/($r1_cap*$class_multiplier))-$r1_mod;
			}
		}
		else
		{
			$r1_proposed_module = 0;
		}
		if ($r2_cap>0 && $r2_proposed_wattage>0)
		{
			if(ceil(($r2_proposed_wattage+$r2_cur_wattage)/($r2_cap*$class_multiplier))-$r2_mod<0)
			{
				$r2_proposed_module = 0;
			}
			else
			{
				$r2_proposed_module = ceil(($r2_proposed_wattage+$r2_cur_wattage)/($r2_cap*$class_multiplier))-$r2_mod;
			}
			
		}
		else
		{
			$r2_proposed_module = 0;
		}
		if($r3_cap>0 && $r3_proposed_wattage>0)
		{
			if(ceil(($r3_proposed_wattage+$r3_cur_wattage)/($r3_cap*$class_multiplier))-$r3_mod<0)
			{
				$r3_proposed_module = 0;
			}
			else
			{
				$r3_proposed_module = ceil(($r3_proposed_wattage+$r3_cur_wattage)/($r3_cap*$class_multiplier))-$r3_mod;
			}
		}
		else
		{
			$r3_proposed_module = 0;
		}


		//BATTERY CALCULATION
		$r1_exprop_adc = ($r1_proposed_wattage/53.4)+$r1_cur_adc;
		$r2_exprop_adc = ($r2_proposed_wattage/53.4)+$r2_cur_adc;
		$r3_exprop_adc = ($r3_proposed_wattage/53.4)+$r3_cur_adc;

		$r1_batt_charging = 0;
		$r2_batt_charging = 0;
		$r3_batt_charging = 0;

		if($r1_exprop_adc!=0 && $r1_proposed_wattage>0)
		{
			if($r1_cur_batt_type=="Li-ion" && ($r1_cur_batt_cap/$r1_exprop_adc)<$batt_threshold)
			{
				$r1_batt_cells = ceil((($batt_threshold*$r1_exprop_adc)-$r1_cur_batt_cap)/(150*0.95));
				$r1_batt_cap = $r1_batt_cells*150;
				$r1_battery_remarks = "Additional ".$r1_batt_cells."x 150AH Li-ion Cells";
				$r1_batt_charging = $r1_batt_cap*$existing->BATTERY_CHARGING_COEFFICIENT*53.4/230;
			}
			else if($r1_cur_batt_type=="VRLA" && ($r1_cur_batt_cap/$r1_exprop_adc)<$batt_threshold)
			{
				$r1_batt_cells = ceil(($batt_threshold*$r1_exprop_adc)/(150*0.95));
				$r1_batt_cap = $r1_batt_cells*150;
				$r1_battery_remarks = "Battery changeout VRLA to ".$r1_batt_cells."x 150AH Li-ion Cells";
				$r1_batt_charging = $r1_batt_cap*$existing->BATTERY_CHARGING_COEFFICIENT*53.4/230;
			}
			else if($r1_cur_batt_type=="none")
			{
				$r1_battery_remarks = "No Battery Found";
			}
			else
			{
				$r1_battery_remarks = "Sufficient";
			}	
			
		}
		else
		{
			$r1_battery_remarks = "none";
		}
		
		if($r2_exprop_adc!=0 && $r2_proposed_wattage>0)
		{
			if($r2_cur_batt_type=="Li-ion" && ($r2_cur_batt_cap/$r2_exprop_adc)<$batt_threshold)
			{
				$r2_batt_cells = ceil((($batt_threshold*$r2_exprop_adc)-$r2_cur_batt_cap)/150);
				$r2_batt_cap = $r2_batt_cells*150;
				$r2_battery_remarks = "Additional ".$r2_batt_cells."x 150AH Li-ion Cells";
				$r2_batt_charging = $r2_batt_cap*$existing->BATTERY_CHARGING_COEFFICIENT*53.4/230;
			}
			else if($r2_cur_batt_type=="VRLA" && ($r2_cur_batt_cap/$r2_exprop_adc)<$batt_threshold)
			{
				$r2_batt_cells = ceil(($batt_threshold*$r2_exprop_adc)/150);
				$r2_batt_cap = $r2_batt_cells*150;
				$r2_battery_remarks = "Battery changeout VRLA to ".$r2_batt_cells."x 150AH Li-ion Cells";
				$r2_batt_charging = $r2_batt_cap*$existing->BATTERY_CHARGING_COEFFICIENT*53.4/230;
			}	
			else if($r2_cur_batt_type=="none")
			{
				$r2_battery_remarks = "No Battery Found";
			}
			else
			{
				$r2_battery_remarks = "Sufficient";
			}		
		}
		else
		{
			$r2_battery_remarks = "none";
		}

		if($r3_exprop_adc!=0 && $r3_proposed_wattage>0)
		{
			if($r3_cur_batt_type=="Li-ion" && ($r3_cur_batt_cap/$r3_exprop_adc)<$batt_threshold)
			{
				$r3_batt_cells = ceil((($batt_threshold*$r3_exprop_adc)-$r3_cur_batt_cap)/150);
				$r3_batt_cap = $r3_batt_cells*150;
				$r3_battery_remarks = "Additional ".$r3_batt_cells."x 150AH Li-ion Cells";
				$r3_batt_charging = $r3_batt_cap*$existing->BATTERY_CHARGING_COEFFICIENT*53.4/230;
			}
			else if($r3_cur_batt_type=="VRLA" && ($r3_cur_batt_cap/$r3_exprop_adc)<$batt_threshold)
			{
				$r3_batt_cells = ceil(($batt_threshold*$r3_exprop_adc)/150);
				$r3_batt_cap = $r3_batt_cells*150;
				$r3_battery_remarks = "Battery changeout VRLA to ".$r3_batt_cells."x 150AH Li-ion Cells";
				$r3_batt_charging = $r3_batt_cap*$existing->BATTERY_CHARGING_COEFFICIENT*53.4/230;
			}
			else if($r3_cur_batt_type=="none")
			{
				$r3_battery_remarks = "No Battery Found";
			}
			else
			{
				$r3_battery_remarks = "Sufficient";
			}
		}
		else
		{
			$r3_battery_remarks = "none";
		}
		//ADDITIONAL RECTIFIER REMARKS
		$additional = ceil($total_excess / (4000*5*$class_multiplier));

		//CAPACITY (EXISTING + PROPOSED)
		if($existing->POWER_PHASE=="3-Phase")//for genset only
		{
			$phase_multiplier = 0.9;//
		}
		else
		{
			$phase_multiplier = 0.8;//
		}
		$total_prop_aac = number_format(($total_aac_load)+$r1_batt_charging+$r2_batt_charging+$r3_batt_charging,2);

		$total_aac_exprop = $total_prop_aac + $existing->TOTAL_AC_LOAD_A;
		$total_aac_exprop = is_numeric($total_aac_exprop) != ' ' ? 0 : $total_aac_exprop;
/*		$genset_aac_exprop = $total_prop_aac+$existing->TOTAL_AC_LOAD_A;
		$ecb_aac_exprop = $total_prop_aac+$existing->TOTAL_AC_LOAD_A;
		$acpdb_aac_exprop = $total_prop_aac+$existing->TOTAL_AC_LOAD_A;*/
		$trans = is_numeric($existing->TRANSFORMER_CAPACITY_KVA) != ' ' ? 0 : $existing->TRANSFORMER_CAPACITY_KVA;
		$gen = is_numeric($existing->GENSET_CAPACITY_KVA) != ' ' ? 0 : $existing->GENSET_CAPACITY_KVA;
		$mdp = is_numeric($existing->MDP_ECB_RATING_AT) != ' ' ? 0 : $existing->MDP_ECB_RATING_AT;
		$acp = is_numeric($existing->ACPDB_RATING_AT) != ' ' ? 0 : $existing->ACPDB_RATING_AT;

		$transformer_headroom_exprop = ($trans*1000) - ($total_aac_exprop * 230);//
		$genset_headroom_exprop =($gen*1000) - ($total_aac_exprop * 230);//
		$ecb_headroom_exprop = ($mdp) - $total_aac_exprop ;//
		$acpdb_headroom_exprop = ($acp) - $total_aac_exprop;//

		$prop_bundle["KVA"]=array(15,25,37.5,50,75,100,125,150,175,200,225,250);	
		$prop_bundle["AT"]=array(60,70,125,175,225,250,275,300,325,350,375,400,425);

		$score_transformer=0;
		$score_genset=0;
		$score_ecb=0;
		$score_acpdb=0;
		//Calculation for new utilization
		//$new_ac_load = $total_prop_aac+$existing->TOTAL_AC_LOAD_A;
		if(is_numeric($transformer_headroom_exprop)!="")
		{
			$i=0;
			$prop_transformer = $trans;
			while($prop_transformer*1000*0.9 < $total_aac_exprop*230)
			{
				if($i>count($prop_bundle["KVA"]))
				{
					break;
				}
				$i++;
				$prop_transformer = $prop_bundle["KVA"][$i];
				//$transformer_headroom_exprop+=($prop_transformer*1000*0.9) - ($total_prop_aac*230);
				
				
			}
			$score_transformer=$i;
		}
		if(is_numeric($genset_headroom_exprop)!="")
		{
			$i=0;
			$prop_genset=$gen;
			while($prop_genset*1000*$phase_multiplier < $total_aac_exprop*230)
			{
				if($i>count($prop_bundle["KVA"]))
				{
					break;
				}
				$i++;
				$prop_genset = $prop_bundle["KVA"][$i];
				//$genset_headroom_exprop+=($prop_genset*1000*$phase_multiplier) - ($total_prop_aac*230);
				

			}
			$score_genset=$i;
		}
		if(is_numeric($ecb_headroom_exprop)!="")
		{
			$i=0;
			$prop_ecb=$mdp;
			while($prop_ecb*0.8 < $total_aac_exprop)
			{
				if($i> count($prop_bundle["AT"]))
				{
					break;
				}
				$i++;
				$prop_ecb = $prop_bundle["AT"][$i];
				//$ecb_headroom_exprop+=($prop_ecb*0.8) - $total_prop_aac;
			
				
			}
			$score_ecb=$i;
		}
		if(is_numeric($acpdb_headroom_exprop)!="")
		{
			$i=0;
			$prop_acpdb=$acp;
			while($prop_acpdb*0.8 < $total_aac_exprop)
			{
				if($i>count($prop_bundle["AT"]))
				{
					break;
				}
				$i++;
				$prop_acpdb = $prop_bundle["AT"][$i];
				//$acpdb_headroom_exprop+=($prop_acpdb*0.8) - $total_prop_aac;
				
				
			}
			$score_acpdb=$i;
		}
		
		//KVA
		$propmax_bundle_KVA = max($score_transformer,$score_genset);

		if($prop_bundle["KVA"][$propmax_bundle_KVA]>$existing->TRANSFORMER_CAPACITY_KVA && $prop_bundle["KVA"][$propmax_bundle_KVA]>$existing->GENSET_CAPACITY_KVA && $total_prop_aac!=0)//&& $total_prop_aac!=0
		{
			$endstate_transformer =  floatval($prop_bundle["KVA"][$propmax_bundle_KVA]);
			$endstate_genset = floatval($prop_bundle["KVA"][$propmax_bundle_KVA]);
		}
		else if($prop_bundle["KVA"][$propmax_bundle_KVA]>$existing->TRANSFORMER_CAPACITY_KVA && $prop_bundle["KVA"][$propmax_bundle_KVA]<=$existing->GENSET_CAPACITY_KVA && $total_prop_aac!=0)//&& $total_prop_aac!=0
		{
			$endstate_transformer = floatval($prop_bundle["KVA"][$propmax_bundle_KVA]);
			$endstate_genset = floatval($existing->GENSET_CAPACITY_KVA);
		}
		else if($prop_bundle["KVA"][$propmax_bundle_KVA]<=$existing->TRANSFORMER_CAPACITY_KVA && $prop_bundle["KVA"][$propmax_bundle_KVA]>$existing->GENSET_CAPACITY_KVA && $total_prop_aac!=0)//&& $total_prop_aac!=0
		{
			$endstate_transformer = floatval($existing->TRANSFORMER_CAPACITY_KVA);
			$endstate_genset = floatval($prop_bundle["KVA"][$propmax_bundle_KVA]);
		}
		else
		{
			$endstate_transformer = floatval($existing->TRANSFORMER_CAPACITY_KVA);
			$endstate_genset = floatval($existing->GENSET_CAPACITY_KVA);
		}
		
		//AT
		$propmax_bundle_AT = max($score_ecb,$score_acpdb);

		if($prop_bundle["AT"][$propmax_bundle_AT]>$existing->MDP_ECB_RATING_AT && $prop_bundle["AT"][$propmax_bundle_AT]>$existing->ACPDB_RATING_AT&& $total_prop_aac!=0)//&& $total_prop_aac!=0
		{
			$endstate_ecb = $prop_bundle["AT"][$propmax_bundle_AT];
			$endstate_acpdb = $prop_bundle["AT"][$propmax_bundle_AT];
		}
		else if($prop_bundle["AT"][$propmax_bundle_AT]>$existing->MDP_ECB_RATING_AT && $prop_bundle["AT"][$propmax_bundle_AT]<=$existing->ACPDB_RATING_AT && $total_prop_aac!=0)//&& $total_prop_aac!=0
		{
			$endstate_ecb = $prop_bundle["AT"][$propmax_bundle_AT];
			$endstate_acpdb = $existing->ACPDB_RATING_AT;
		}
		else if($prop_bundle["AT"][$propmax_bundle_AT]<=$existing->MDP_ECB_RATING_AT && $prop_bundle["AT"][$propmax_bundle_AT]>$existing->ACPDB_RATING_AT && $total_prop_aac!=0)//&& $total_prop_aac!=0
		{
			$endstate_ecb = $existing->MDP_ECB_RATING_AT;
			$endstate_acpdb = $prop_bundle["AT"][$propmax_bundle_AT];
		}
		else
		{
			$endstate_ecb = $existing->MDP_ECB_RATING_AT;
			$endstate_acpdb = $existing->ACPDB_RATING_AT;
		}

		$reco_transformer = $endstate_transformer;
		$reco_genset = $endstate_genset;
		if (in_array($existing->POWER_CONNECTION, array('Sole Transformer', 'Tempo to Coop', 'Shared Transformer'))) {
			if ($endstate_transformer == $existing->TRANSFORMER_CAPACITY_KVA) {
				$reco_transformer = 'none';
//				$reco_genset = 'none';
			}
		}  else {
			$reco_transformer = 0;
//			$reco_genset = 0;
		}

		if ($endstate_genset == $existing->GENSET_CAPACITY_KVA) {
			$reco_genset = 'none';
		}


//		$reco_genset = $endstate_genset == $existing->GENSET_CAPACITY_KVA ? 'none' : $endstate_genset;
		$reco_ecb = $endstate_ecb == $existing->MDP_ECB_RATING_AT ? 'none' : $endstate_ecb;
		$reco_acpdb = $endstate_acpdb == $existing->ACPDB_RATING_AT ? 'none' : $endstate_acpdb;
		$ac_remarks_array = array();

		if ($reco_transformer!="none")
		{
			if($existing->TRANSFORMER_CAPACITY_KVA=="none" || $existing->TRANSFORMER_CAPACITY_KVA==0)
			{
				$reco_transformer = "";
			}
			else
			{
				$ac_remarks_array[] = '<span class="badge badge-primary">A</span> : '.$reco_transformer;
			}
			
		}
		if ($reco_genset!="none")
		{
			if($existing->GENSET_CAPACITY_KVA=="none" || $existing->GENSET_CAPACITY_KVA==0)
			{
				$reco_genset = "";
			}
			else
			{
//				$ac_remarks_array[]="B:".$reco_genset;
				$ac_remarks_array[] = '<span class="badge badge-success">B</span> : '.$reco_genset;
			}
			
		}
		if ($reco_ecb!="none")
		{
			if($existing->MDP_ECB_RATING_AT=="none")
			{
				$reco_ecb = "";
			}
			else
			{
//				$ac_remarks_array[]="C:".$reco_ecb;
				$ac_remarks_array[] = '<span class="badge badge-warning">C</span> : '.$reco_ecb;
			}
			
		}
		if ($reco_acpdb!="none")
		{
			if($existing->ACPDB_RATING_AT=="none")
			{
				$reco_acpdb = "";
			}
			else
			{
//				$ac_remarks_array[]="D:".$reco_acpdb;
				$ac_remarks_array[] = '<span class="badge badge-danger">D</span> : '.$reco_acpdb;
			}
			
		}

		if(empty($ac_remarks_array)&&empty($r1_item)&&empty($r2_item)&&empty($r3_item))
		{
			$ac_remarks = "none"; //none
			$ac_reco = "none"; //none
		}
		else if(empty($ac_remarks_array))
		{
			$ac_remarks = "Ready to build"; //sufficient
			$ac_reco = "N/A"; //none
		}
		else
		{
			$ac_remarks = "For Upgrade ";
			$ac_reco = '';
			foreach ($ac_remarks_array as $ac_value)
			{
				$ac_reco  .= $ac_value." ";
			}
		}

		$dc_rectifier_remarks = "For Upgrade";
		$dc_reco = '';
		if($r1_proposed_module > 0)
		{
			$dc_reco = "R1 Module: +".$r1_proposed_module;
		}
		if($r2_proposed_module > 0)
		{
			if($dc_reco!= "")
			{
				$dc_reco .= "<br>";
			}
			$dc_reco .= "R2 Module: +".$r2_proposed_module;
		}
		if($r3_proposed_module > 0)
		{
			if($dc_reco!= "")
			{
				$dc_reco .= "<br>";
			}
			$dc_reco .= "R3 Module: +".$r3_proposed_module;
		}

		//Battery Remarks
		if(strtoupper($r1_battery_remarks) !="NONE" && $r1_battery_remarks!="Sufficient")
		{
			if($dc_reco!= "")
			{
				$dc_reco .= "<br>";
			}
			$dc_reco .= "R1 Battery: ".$r1_battery_remarks;
		}
		if(strtoupper($r2_battery_remarks) !="NONE" && $r2_battery_remarks!="Sufficient")
		{
			if($dc_reco!= "")
			{
				$dc_reco .= "<br>";
			}
			$dc_reco .= "R2 Battery: ".$r2_battery_remarks;
		}
		if(strtoupper($r3_battery_remarks) !="NONE" && $r3_battery_remarks!="Sufficient")
		{
			if($dc_reco != "")
			{
				$dc_reco .= "<br>";
			}
			$dc_reco .= "R3 Battery: ".$r3_battery_remarks;
		}
		$add_bat_load = 0;
		if($total_excess > 0)
		{
			if($dc_reco != "")
			{
				$dc_reco .= "<br>";
			}
			$dc_reco .= $additional."x Rectifier 4000W 5-MOD". ', Li-ion ' .$ah.' Battery';
			$add_bat_load = round((((600*$existing->BATTERY_CHARGING_COEFFICIENT)*53.4)/230) * $additional, 2);
		}

		if (empty($dc_reco)) {
			$dc_rectifier_remarks = 'Sufficient';
			$dc_reco = 'N/A';
		}

		$endstate_transformer_util = round(((($total_aac_exprop + $add_bat_load) *230)/($endstate_transformer*1000))*100,2);
		$endstate_genset_util = round(((($total_aac_exprop + $add_bat_load) *230)/($endstate_genset*1000))*100,2);
		$endstate_ecb_util = round((($total_aac_exprop + $add_bat_load) /($endstate_ecb))*100,2);
		$endstate_acpdb_util = round((($total_aac_exprop + $add_bat_load) /($endstate_acpdb))*100,2);

		//$dc_rectifier_remarks = $total_excess > 0 ? $additional."x Rectifier 4000W 5-MOD". ', Li-ion ' .$ah.' Battery' : "Sufficient";

		$tutil = $existing->TRANSFORMER_UTIL == 'none' ? 0 : $existing->TRANSFORMER_UTIL;
		$gutil = $existing->GENSET_UTIL == 'none' ? 0 : $existing->GENSET_UTIL;
		$eutil = $existing->ECB_UTIL == 'none' ? 0 : $existing->ECB_UTIL;
		$autil = $existing->ACPDB_UTIL == 'none' ? 0 : $existing->ACPDB_UTIL;

		$r1_d_item = empty($r1_d_item) ? '' : '<b class="text-danger">('. $r1_d_item .')</b>';
		$r2_d_item = empty($r2_d_item) ? '' : '<b class="text-danger">('. $r2_d_item .')</b>';
		$r3_d_item = empty($r3_d_item) ? '' : '<b class="text-danger">('. $r3_d_item .')</b>';

		if (!in_array($existing->POWER_CONNECTION, array('Sole Transformer', 'Tempo to Coop', 'Shared Transformer'))) {
			$endstate_transformer = 0;
			$endstate_transformer_util = 0;
		}

		if ($existing->GENSET_CAPACITY_KVA == 0) {
			$endstate_genset = 0;
			$endstate_genset_util = 0;
		}

		return  array(
				"sitename" => strtoupper($sitename),
				"plaid" => strtoupper($plaid),
				"site_class"=> $existing->SITE_CLASS,
				"existing_load"=> $existing->TOTAL_AC_LOAD_A,
				"proposed_load"=> $total_prop_aac,
				"ex_transformer_capacity" => $existing->TRANSFORMER_CAPACITY_KVA,
				"ex_genset_capacity" => $existing->GENSET_CAPACITY_KVA,
				"ex_ecb_capacity" => $existing->MDP_ECB_RATING_AT,
				"ex_acpdb_capacity" => $existing->ACPDB_RATING_AT,
				"ex_transformer_util" => $tutil,
				"ex_genset_util" => $gutil,
				"ex_ecb_util" => $eutil,
				"ex_acpdb_util" => $autil,
				"endstate_transformer" => $endstate_transformer,
				"endstate_genset" => $endstate_genset,
				"endstate_ecb" => $endstate_ecb,
				"endstate_acpdb" => $endstate_acpdb,
				"endstate_transformer_util"=> $endstate_transformer_util,
				"endstate_genset_util" => $endstate_genset_util,
				"endstate_ecb_util" => $endstate_ecb_util,
				"endstate_acpdb_util" => $endstate_acpdb_util,
				"r1_proposed_module" => $r1_proposed_module,
				"r2_proposed_module" => $r2_proposed_module,
				"r3_proposed_module" => $r3_proposed_module,
				"r1_item" => $r1_item .' '. $r1_d_item,
				"r2_item" => $r2_item .' '. $r2_d_item,
				"r3_item" => $r3_item .' '. $r3_d_item,
				"r1_remaining_headroom" => $r1_remaining_headroom,
				"r2_remaining_headroom" => $r2_remaining_headroom,
				"r3_remaining_headroom" => $r3_remaining_headroom,
				"r1_battery_remarks" => $r1_battery_remarks,
				"r2_battery_remarks" => $r2_battery_remarks,
				"r3_battery_remarks" => $r3_battery_remarks,
				"excluded" => $excluded_tech,
				"excess_wattage" => $excess_wattage,
				"total_excess" => $total_excess,
				"dc_recomendations" => $dc_rectifier_remarks,
				"dc_remarks" => $dc_reco,
				"ac_recomendations" =>$ac_remarks,
				"ac_remarks" => $ac_reco,
				"date" => date('Y-m-d'),
				"batch_key" => $insert_id
			);
	}

	public function insert_power_proposal($prop_res)
	{
		$feedback = 0;
		if (!empty($prop_res))
		{
			$this->db->insert_batch('power_propsal_mdb_'.$this->geoarea,$prop_res);
			$feedback = $this->db->affected_rows();
		}
		$this->session->unset_userdata(array('proposed_result, id'));

		return $feedback;
	}

	public function check_headroom()
	{

	}

	public function check_module()
	{

	}

	public function util_remarks($util_array, $threshold)
	{
		foreach ($util_array as $value) {
			if ($value > $threshold) {
				return "NOT PASSED";
			}
		}
		return "PASSED";
	}

	// ------------------------- CHART ------------------------------//

	public function power_connection()
	{
		$type = array('Genset Prime',
			'Sole Transformer',
			'Tapped to Building',
			'Tapped to Neighbor',
			'Tapped to Genset',
			'Tempo to Coop',
			'Shared Transformer',
			'No Power',
			'SMALL CELL',
			'To be Updated',
		);

		$data = array();
		if (empty($this->geo) && $this->geoarea === 'nat') {
			foreach ($type as $key => $value) {
				$counts = 0;
				$sc = 0;
				foreach ($this->regions as $keys => $values) {
					$this->db->where('POWER_CONNECTION', $value);
					if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
					$counts += $this->db->get('powerdb_' . $values)->num_rows();
				}
				$data[$value] = $counts;
			}
		} else {
			$tbl_name = $this->geoarea === 'nat' ? 'powerdb_' . $this->geo : $this->tablename;
			foreach ($type as $key => $row) {
				$this->db->where("POWER_CONNECTION", $row);
				if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
				$data[$row] = $this->db->get($tbl_name)->num_rows();
			}

			$this->db->where("SITE_TYPE", 'SMALL CELL');
			$data['sc'] = $this->db->get($tbl_name)->num_rows();
		}

		return $data;
	}

	public function get_rectifier_bat_hour()
	{
		$l4_hr = 0;
		$g4l8_hr = 0;
		$g8_hr = 0;
		$tbu = 0;
		if (empty($this->geo) && $this->geoarea === 'nat') {
			foreach ($this->regions as $key => $value) {
				$this->db->where("SITE_TYPE <>", 'SMALL CELL');
				if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
				$result = $this->db->get('powerdb_' . $value);

				if ($result->num_rows() > 0) {
					foreach ($result->result() as $row) {
						$value = $row->RDC_HR === null ? null : intval($row->RDC_HR);
						if ($value === null) {
							$tbu += 1;
						} else if ($value > 8) {
							$g8_hr += 1;
						} else if ($value >= 4 && $value <= 8) {
							$g4l8_hr += 1;
						} else if ($value < 4) {
							$l4_hr += 1;
						}
					}
				}
			}
		} else {
			$tbl_name = $this->geoarea === 'nat' ? 'powerdb_' . $this->geo : $this->tablename;
			$this->db->where("SITE_TYPE <>", 'SMALL CELL');
			if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
			$result = $this->db->get($tbl_name);

			if ($result->num_rows() > 0) {
				foreach ($result->result() as $row) {
					$value = $row->RDC_HR === null ? null : intval($row->RDC_HR);
					if ($value === null) {
						$tbu += 1;
					} else if ($value > 8) {
						$g8_hr += 1;
					} else if ($value >= 4 && $value <= 8) {
						$g4l8_hr += 1;
					} else if ($value < 4) {
						$l4_hr += 1;
					}
				}
			}
		}
		return array(
			'l4_hr' => $l4_hr,
			'g8_hr' => $g8_hr,
			'g4l8_hr' => $g4l8_hr,
			'tbu' => $tbu
		);
	}

	public function get_ac_capacity()
	{
		$v1 = 0;
		$v2 = 0;
		$v3 = 0;
		$v4 = 0;
		$v5 = 0;
		$scc = 0;
		$tbu = 0;
		$none = 0;
		if (empty($this->geo) && $this->geoarea === 'nat') {
			#FOR NATIONAL
				foreach ($this->regions as $keys => $rows) {
					$this->db->select('TRANSFORMER_CAPACITY_KVA as ac');
					if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
					$result = $this->db->get('powerdb_' . $rows);
					if ($result->num_rows() > 0) {
						foreach ($result->result() as $row) {
							#SMALL CELL (8 KVA)
							#15 KVA (less 15, physical 15, tap to building where AT < 125 AT Single Phase or 100 AT Three-Phase on any of ECB/MDP/ACPDB)with options to consider Service Entrance (SE) drop wire < 38 mm2
							#eg. Transformer/Genset 15 KVA and/or ECB/ACPDB is 100 AT and less
							#Tap to Buiding 15 KVA Genset and/or ECP/ACPDB is 100 AT Single Phase and less
							#25 KVA (Physical 25 KVA, tap to building where AT is 125 AT and 150 AT Single Phase or 100 AT Three- Phase on any of ECB/MDP/ACPDB) with options to consider SE drop wire 38 mm2 50 mm2
							#37.5 KVA (Physical 37.5 KVA Transformer, 40 KVA Genset, tap to building where AT is 175 AT Single Phase
							#50 KVA (Physical 50 KVA, tap to building where AT is 225 AT Single Phase on any of ECB/MDP/ACPDB)
							
							if (strtoupper($row->ac) === 'TO BE UPDATED' || $row->ac  == 'null'){
								$tbu += 1;
							} else {
								$value = $row->ac;		
								if (strtoupper($value)  == 'NONE') {
									$none += 1;
								} else if ($value > 50) {
									$v1 += 1;
								} else if ($value == 50) {	
									$v2 += 1;
								} else if ($value >= 37.5 && $value <= 40) {
									$v3 += 1;
								} else if ($value == 25) {
									$v4 += 1;
								} else if ($value <= 15) {
									$v5 += 1;
								} 
							}					
						}
		
						#SMALL CELL
						$this->db->where("SITE_TYPE", 'SMALL CELL');
						$scc += $this->db->get('powerdb_' . $rows)->num_rows();
					}
				}
		} else {
			$tbl_name = $this->geoarea === 'nat' ? 'powerdb_'.$this->input->post('geo_power') : $this->tablename;
			#FOR REGION
			$this->db->select('TRANSFORMER_CAPACITY_KVA as ac');
			if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
			$result = $this->db->get($tbl_name);
			if ($result->num_rows() > 0) {
				foreach ($result->result() as $row) {
					#SMALL CELL (8 KVA)
					#15 KVA (less 15, physical 15, tap to building where AT < 125 AT Single Phase or 100 AT Three-Phase on any of ECB/MDP/ACPDB)with options to consider Service Entrance (SE) drop wire < 38 mm2
    				#eg. Transformer/Genset 15 KVA and/or ECB/ACPDB is 100 AT and less
					#Tap to Buiding 15 KVA Genset and/or ECP/ACPDB is 100 AT Single Phase and less
					#25 KVA (Physical 25 KVA, tap to building where AT is 125 AT and 150 AT Single Phase or 100 AT Three- Phase on any of ECB/MDP/ACPDB) with options to consider SE drop wire 38 mm2 50 mm2
					#37.5 KVA (Physical 37.5 KVA Transformer, 40 KVA Genset, tap to building where AT is 175 AT Single Phase
					#50 KVA (Physical 50 KVA, tap to building where AT is 225 AT Single Phase on any of ECB/MDP/ACPDB)
					
					if (strtoupper($row->ac) === 'TO BE UPDATED' || $row->ac  == 'null'){
						$tbu += 1;
					} else {
						$value = $row->ac;		
						if (strtoupper($value)  == 'NONE') {
							$none += 1;
						} else if ($value > 50) {
							$v1 += 1;
						} else if ($value == 50) {	
							$v2 += 1;
						} else if ($value >= 37.5 && $value <= 40) {
							$v3 += 1;
						} else if ($value == 25) {
							$v4 += 1;
						} else if ($value <= 15) {
							$v5 += 1;
						} 
					}					
				}

				#SMALL CELL
				$this->db->where("SITE_TYPE", 'SMALL CELL');
				$scc = $this->db->get($tbl_name)->num_rows();
			}
		}

		return array(
			'v1' => $v1,
			'v2' => $v2,
			'v3' => $v3,
			'v4' => $v4,
			'v5' => $v5,
			'sc' => $scc,
			'tbu' => $tbu,
			'none' => $none
		);
	}

	public function get_transformer_capacity()
	{
		$g50 = 0;
		$g37l50 = 0;
		$g25l37 = 0;
		$g15l25 = 0;
		$l15 = 0;
		$None = 0;
		$tbu = 0;

		if (empty($this->geo) && $this->geoarea === 'nat') {
			#FOR NATIONAL
			foreach ($this->regions as $keys => $rows) {
				#Greater than 50
//				$this->db->where("SITE_TYPE <>", 'SMALL CELL');
				$this->db->where('(POWER_CONNECTION = "Sole Transformer" OR POWER_CONNECTION = "Shared Transformer")');
				$this->db->where('TRANSFORMER_CAPACITY_KVA >=', 50);
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'None');
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'To be Updated');
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', null);
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', '');
				if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
				$g50 += $this->db->get('powerdb_' . $rows)->num_rows();

				#EQUAL TO 37.5
//				$this->db->where("SITE_TYPE <>", 'SMALL CELL');
				$this->db->where('(POWER_CONNECTION = "Sole Transformer" OR POWER_CONNECTION = "Shared Transformer")');
				$this->db->where('TRANSFORMER_CAPACITY_KVA', 37.5);
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'None');
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'To be Updated');
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', null);
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', '');
				if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
				$g37l50 += $this->db->get('powerdb_' . $rows)->num_rows();

				#EQUAL TO 25
//				$this->db->where("SITE_TYPE <>", 'SMALL CELL');
				$this->db->where('TRANSFORMER_CAPACITY_KVA', 25);
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'None');
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'To be Updated');
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', null);
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', '');
				$this->db->where('(POWER_CONNECTION = "Sole Transformer" OR POWER_CONNECTION = "Shared Transformer")');
				if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
				$g25l37 += $this->db->get('powerdb_' . $rows)->num_rows();

				#EQUAL TO 15
//				$this->db->where("SITE_TYPE <>", 'SMALL CELL');
				$this->db->where('TRANSFORMER_CAPACITY_KVA', 15);
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'None');
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'To be Updated');
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', null);
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', '');
				$this->db->where('(POWER_CONNECTION = "Sole Transformer" OR POWER_CONNECTION = "Shared Transformer")');
				if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
				$g15l25 += $this->db->get('powerdb_' . $rows)->num_rows();

				#Less than 15
//				$this->db->where("SITE_TYPE <>", 'SMALL CELL');
				$this->db->where('TRANSFORMER_CAPACITY_KVA <', 15);
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'None');
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'To be Updated');
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', null);
				$this->db->where('TRANSFORMER_CAPACITY_KVA <>', '');
				$this->db->where('(POWER_CONNECTION = "Sole Transformer" OR POWER_CONNECTION = "Shared Transformer")');
				if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
				$l15  += $this->db->get('powerdb_' . $rows)->num_rows();
				#None
//				$this->db->where("SITE_TYPE <>", 'SMALL CELL');
				$this->db->where('TRANSFORMER_CAPACITY_KVA', 'None');
				$this->db->where('(POWER_CONNECTION = "Sole Transformer" OR POWER_CONNECTION = "Shared Transformer")');
				if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
				$None += $this->db->get('powerdb_' . $rows)->num_rows();
				#To be updated
//				$this->db->where("SITE_TYPE <>", 'SMALL CELL');
				$this->db->where('TRANSFORMER_CAPACITY_KVA', $this->tbu);
				$this->db->where('(POWER_CONNECTION = "Sole Transformer" OR POWER_CONNECTION = "Shared Transformer")');
				if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
				$tbu += $this->db->get('powerdb_' . $rows)->num_rows();
			}
		} else {
			$tbl_name = $this->geoarea === 'nat' ? 'powerdb_'.$this->input->post('geo_power') : $this->tablename;
			#FOR REGION
			#Greater than 50
//			$this->db->where("SITE_TYPE <>", 'SMALL CELL');
			$this->db->where('(POWER_CONNECTION = "Sole Transformer" OR POWER_CONNECTION = "Shared Transformer")');
			$this->db->where('TRANSFORMER_CAPACITY_KVA >=', 50);
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'None');
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'To be Updated');
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', null);
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', '');
			if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
			$g50 = $this->db->get($tbl_name)->num_rows();

			#EQUAL TO 37.5
//			$this->db->where("SITE_TYPE <>", 'SMALL CELL');
			$this->db->where('(POWER_CONNECTION = "Sole Transformer" OR POWER_CONNECTION = "Shared Transformer")');
			$this->db->where('TRANSFORMER_CAPACITY_KVA', 37.5);
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'None');
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'To be Updated');
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', null);
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', '');
			if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
			$g37l50 = $this->db->get($tbl_name)->num_rows();

			#EQUAL TO 25
//			$this->db->where("SITE_TYPE <>", 'SMALL CELL');
			$this->db->where('TRANSFORMER_CAPACITY_KVA', 25);
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'None');
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'To be Updated');
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', null);
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', '');
			$this->db->where('(POWER_CONNECTION = "Sole Transformer" OR POWER_CONNECTION = "Shared Transformer")');
			if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
			$g25l37 = $this->db->get($tbl_name)->num_rows();

			#EQUAL TO 15
//			$this->db->where("SITE_TYPE <>", 'SMALL CELL');
			$this->db->where('TRANSFORMER_CAPACITY_KVA', 15);
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'None');
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'To be Updated');
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', null);
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', '');
			$this->db->where('(POWER_CONNECTION = "Sole Transformer" OR POWER_CONNECTION = "Shared Transformer")');
			if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
			$g15l25 = $this->db->get($tbl_name)->num_rows();

			#Less than 15
//			$this->db->where("SITE_TYPE <>", 'SMALL CELL');
			$this->db->where('TRANSFORMER_CAPACITY_KVA <', 15);
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'None');
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', 'To be Updated');
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', null);
			$this->db->where('TRANSFORMER_CAPACITY_KVA <>', '');
			$this->db->where('(POWER_CONNECTION = "Sole Transformer" OR POWER_CONNECTION = "Shared Transformer")');
			if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
			$l15 = $this->db->get($tbl_name)->num_rows();
			#None
//			$this->db->where("SITE_TYPE <>", 'SMALL CELL');
			$this->db->where('TRANSFORMER_CAPACITY_KVA', 'None');
			$this->db->where('(POWER_CONNECTION = "Sole Transformer" OR POWER_CONNECTION = "Shared Transformer")');
			if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
			$None = $this->db->get($tbl_name)->num_rows();
			#To be updated
//			$this->db->where("SITE_TYPE <>", 'SMALL CELL');
			$this->db->where('TRANSFORMER_CAPACITY_KVA', $this->tbu);
			$this->db->where('(POWER_CONNECTION = "Sole Transformer" OR POWER_CONNECTION = "Shared Transformer")');
			if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
			$tbu = $this->db->get($tbl_name)->num_rows();
		}

		$count = array(
			'g50' => $g50,
			'g37l50' => $g37l50,
			'g25l37' => $g25l37,
			'g15l25' => $g15l25,
			'l15' => $l15,
			'None' => $None,
			'tbu' => $tbu
		);

		return $count;
	}

	public function get_dc_rec()
	{
		$l16kw = 0;
		$g16kwl18kw = 0;
		$g18kwl24kw = 0;
		$g24kw = 0;
		$cc = 0;

		$condition = '((RS1_MODULE_CAPACITY * (RS1_NO_OF_MODULES + RS1_NO_OF_EMPTY_MODULE)) + (RS2_MODULE_CAPACITY * (RS2_NO_OF_MODULES + RS2_NO_OF_EMPTY_MODULE)) + (RS3_MODULE_CAPACITY * (RS3_NO_OF_MODULES + RS3_NO_OF_EMPTY_MODULE))) as rs_total_cap';

		if (empty($this->geo) && $this->geoarea === 'nat') {
			foreach ($this->regions as $key => $value) {
				$this->db->select($condition);
				$this->db->where("SITE_TYPE <>", 'SMALL CELL');
				if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
				$this->db->from('powerdb_'.$value);
				$result = $this->db->get();

				if ($result->num_rows() > 0) {
					foreach ($result->result() as $row) {
						$value = $row->rs_total_cap === null ? null : intval($row->rs_total_cap);
						if ($value === null) {
							$cc += 1;
						} else if ($value <= 16000) {
							$l16kw += 1;
						} else if ($value > 16000 && $value <= 18000) {
							$g16kwl18kw += 1;
						} else if ($value > 18000 && $value <= 24000) {
							$g18kwl24kw += 1;
						} else if ($value > 24000) {
							$g24kw += 1;
						}
					}
				}
			}
		} else {
			$tbl_name = $this->geoarea === 'nat' ? 'powerdb_' . $this->geo : $this->tablename;
			$this->db->select($condition);
			$this->db->where("SITE_TYPE <>", 'SMALL CELL');
			if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
			 $this->db->from($tbl_name);
			$result = $this->db->get();

			if ($result->num_rows() > 0) {
				foreach ($result->result() as $row) {
					$value = $row->rs_total_cap === null ? null : intval($row->rs_total_cap);
					if ($value === null) {
						$cc += 1;
				    } else if ($value <= 16000) {
						$l16kw += 1;
					} else if ($value > 16000 && $value <= 18000) {
						$g16kwl18kw += 1;
					} else if ($value > 18000 && $value <= 24000) {
						$g18kwl24kw += 1;
					} else if ($value > 24000) {
						$g24kw += 1;
					}
				}
			}
		}

		return array(
			'l16kw' => $l16kw,
			'g16kwl18kw' => $g16kwl18kw,
			'g18kwl24kw' => $g18kwl24kw,
			'g24kw' => $g24kw,
			'cc' => $cc
		);
	}

	public function remain_wattage_dc() {
		$l16kw = 0;
		$g16kwl18kw = 0;
		$g18kwl24kw = 0;
		$g24kw = 0;
		$cc = 0;

		if (empty($this->geo) && $this->geoarea === 'nat') {
			foreach ($this->regions as $key => $value) {
				if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
				$result = $this->db->get('powerdb_'.$value);

				if ($result->num_rows() > 0) {
					foreach ($result->result() as $row) {
						$value = $this->floatvalue($row->REMAINING_WATTAGE_HEADROOM_DC);
						if ($value <= 16000) {
							$l16kw += 1;
						} else if ($value > 16000 && $value <= 18000) {
							$g16kwl18kw += 1;
						} else if ($value > 18000 && $value <= 24000) {
							$g18kwl24kw += 1;
						} else if ($value > 24000) {
							$g24kw += 1;
						} else {
							$cc += 1;
						}
					}
				}
			}
		} else {
			$tbl_name = $this->geoarea === 'nat' ? 'powerdb_' . $this->geo : $this->tablename;
			if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
			$result = $this->db->get($tbl_name);

			if ($result->num_rows() > 0) {
				foreach ($result->result() as $row) {
					$value = $this->floatvalue($row->REMAINING_WATTAGE_HEADROOM_DC);
					if ($value <= 16000) {
						$l16kw += 1;
					} else if ($value > 16000 && $value <= 18000) {
						$g16kwl18kw += 1;
					} else if ($value > 18000 && $value <= 24000) {
						$g18kwl24kw += 1;
					} else if ($value > 24000) {
						$g24kw += 1;
					} else {
						$cc += 1;
					}
				}
			}
		}

		return array(
			'l16kw' => $l16kw,
			'g16kwl18kw' => $g16kwl18kw,
			'g18kwl24kw' => $g18kwl24kw,
			'g24kw' => $g24kw,
			'cc' => $cc
		);
	}

	public function get_site_class()
	{
		$data = array();
		$type = array('A1', 'A2', 'B1', 'B2', 'B3', 'C1', 'C2', 'C3', 'C4', 'C5', 'To be Updated');
		if (empty($this->geo) && $this->geoarea === 'nat') {
			foreach ($type as $key => $row) {
				$counts = 0;
				foreach ($this->regions as $keys => $rows) {
					$sc = empty($this->sc) ? $row : $this->sc;
					$this->db->where('SITE_CLASS', $sc);
					$counts += $this->db->get('powerdb_' . $rows)->num_rows();
				}
				$data[$row] = $counts;
			}
		} else {
				$tbl_name = $this->geoarea === 'nat' ? 'powerdb_' . $this->geo : $this->tablename;
				foreach ($type as $key => $row) {
					$sc = empty($this->sc) ? $row : $this->sc;
					$this->db->where('SITE_CLASS', $sc);
					$data[$row] = $this->db->get($tbl_name)->num_rows();
				}
		}
		return $data;
	}

	public function grid_status()
	{
		$data = array();
		$type = array('G1', 'G2', 'G3', 'G4', 'Prime', 'SMALL CELL', 'to be updated');
		if (empty($this->geo) && $this->geoarea === 'nat') {
			foreach ($type as $key => $row) {
				$counts = 0;
				$sc = 0;
				foreach ($this->regions as $keys => $rows) {
					$this->db->where('GRID_STATUS', $row);
					if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
					$counts += $this->db->get('powerdb_' . $rows)->num_rows();

					$this->db->where("SITE_TYPE", 'SMALL CELL');
					$sc += $this->db->get('powerdb_' . $rows)->num_rows();
				}
				$data[$row] = $counts;
			}
		} else {
			$tbl_name = $this->geoarea === 'nat' ? 'powerdb_' . $this->geo : $this->tablename;
			foreach ($type as $key => $row) {
				$this->db->where("GRID_STATUS", $row);
				if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
				$data[$row] = $this->db->get($tbl_name)->num_rows();
			}

			$this->db->where("SITE_TYPE", 'SMALL CELL');
			$data['sc'] = $this->db->get($tbl_name)->num_rows();
		}
		return $data;
	}

	public function site_type()
	{
		$data = array();
		$type = array('Greenfield', 'Rooftop', 'In-Building', $this->tbu);
		if (empty($this->geo) && $this->geoarea === 'nat') {
			foreach ($type as $key => $value) {
				$counts = 0;
				foreach ($this->regions as $keys => $values) {
					$this->db->where("SITE_TYPE", $value);
					if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
					$counts += $this->db->get('powerdb_'.$values)->num_rows();
				}
				$data[$value] = $counts;
			}
		} else {
			$tbl_name = $this->geoarea === 'nat' ? 'powerdb_' . $this->geo : $this->tablename;
			foreach ($type as $key => $value) {
				$this->db->where("SITE_TYPE", $value);
				if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
				$data[$value] = $this->db->get($tbl_name)->num_rows();
			}
		}
		return $data;
	}

	public function cabin_type() {
		$data = array();
		$type = array('INDOOR', 'OUTDOOR', 'INDOOR-OUTDOOR', 'None', 'To be Updated');
		if (empty($this->geo) && $this->geoarea === 'nat') {
			foreach ($type as $key => $value) {
				$counts = 0;
				foreach ($this->regions as $keys => $values) {
					$this->db->where("CABIN_TYPE", $value);
					if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
					$counts += $this->db->get('powerdb_'.$values)->num_rows();
				}
				$data[$value] = $counts;
			}
		} else {
			$tbl_name = $this->geoarea === 'nat' ? 'powerdb_' . $this->geo : $this->tablename;
			foreach ($type as $key => $value) {
				$this->db->where("CABIN_TYPE", $value);
				if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
				$data[$value] = $this->db->get($tbl_name)->num_rows();
			}
		}
		return $data;
	}

	public function battery_type() {
		$data = array();
		$type = array('Li-ion', 'VRLA', $this->tbu);
		if (empty($this->geo) && $this->geoarea === 'nat') {
			foreach ($type as $key => $value) {
				$counts = 0;
				foreach ($this->regions as $keys => $values) {
					$this->db->where("CABIN_TYPE", $value);
					if (!empty($this->sc)) {
						$this->db->where('SITE_CLASS', $this->sc);
					}
					$counts += $this->db->get('powerdb_'.$values)->num_rows();
				}
				$data[$value] = $counts;
			}
		} else {
			$tbl_name = $this->geoarea === 'nat' ? 'powerdb_' . $this->geo : $this->tablename;
			$liion = 0;
			$vrla = 0;
			$both = 0;
			$tbu = 0;
			$arr1 = array();
			if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
			$res = $this->db->get($tbl_name);
			foreach ($res->result() as $row) {
				if ($row->RS1_BATTERY_TYPE == $this->tbu || $row->RS2_BATTERY_TYPE == $this->tbu || $row->RS3_BATTERY_TYPE == $this->tbu) {
					$tbu++;
				} else {
					$arr = array($row->RS1_BATTERY_TYPE, $row->RS2_BATTERY_TYPE, $row->RS3_BATTERY_TYPE);
					 $arr = array_unique($arr);
					asort($arr);
					$arr = array_diff($arr, ['None']);
					$val = '';
					if (count($arr) > 1) {
						foreach ($arr as $rows) {
							$val.= $rows;
						}
					} else {
//						$val = $arr[0];
					}
					$arr1[] = $val;
				}

			}

			foreach ($arr1 as $row2) {
				if ($row2 == 'VRLA') {
					$vrla += 1;
				}  else if ($row2 == 'Li-ion') {
					$liion += 1;
				} else if ($row2 == 'Li-ionVRLA') {
					$both += 1;
				}
			}

			$data = array(
				'Li-ion' => $liion,
				'VRLA' => $vrla,
				'both' => $both,
				'To be updated' => $tbu
			);
		}
		return $data;
	}

	public function rectifier_site() {
		$data = array();
		$r1 = 0;
		$r2 = 0;
		$r3 = 0;
		$tbu = 0;
		$none = 0;
		if (empty($this->geo) && $this->geoarea === 'nat') {
			foreach ($this->regions as $keys => $values) {
				$this->db->select('RECTIFIER_1 r1, RECTIFIER_2 r2, RECTIFIER_3 r3');
				if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
				$this->db->from('powerdb_'.$values);
				$result = $this->db->get();

				if ($result->num_rows() > 0) {
					foreach ($result->result() as $row) {
						$rc1 = strtoupper($row->r1);
						$rc2 = strtoupper($row->r2);
						$rc3 = strtoupper($row->r3);
						if (($rc1 <> "TO BE UPDATED" && $rc1 <> "0" && $rc1 <> "NONE" && $rc1 <> "") &&
							($rc2 === "TO BE UPDATED" || $rc2 === "0" || $rc2 === "NONE" || $rc2 === "") &&
							($rc3 === "TO BE UPDATED" || $rc3 === "0" || $rc3 === "NONE" || $rc3 === "")) {
							$r1 += 1;
						} else if (($rc1 <> "TO BE UPDATED" && $rc1 <> "0" && $rc1 <> "NONE" && $rc1 <> "") &&
							($rc2 <> "TO BE UPDATED" && $rc2 <> "0" && $rc2 <> "NONE" && $rc2 <> "") &&
							($rc3 === "TO BE UPDATED" || $rc3 === "0" || $rc3 === "NONE" || $rc3 === "")) {
							$r2 += 1;
						} else if (($rc1 <> "TO BE UPDATED" && $rc1 <> "0" && $rc1 <> "NONE" && $rc1 <> "") &&
							($rc2 <> "TO BE UPDATED" && $rc2 <> "0" && $rc2 <> "NONE" && $rc2 <> "") &&
							($rc3 <> "TO BE UPDATED" && $rc3 <> "0" && $rc3 <> "NONE" && $rc3 <> "")) {
							$r3 += 1;
						} else if ($rc1 <> "TO BE UPDATED" || $rc2 <> "TO BE UPDATED" || $rc3 <> "TO BE UPDATED") {
							$tbu += 1;
						} else {
							$none += 1;
						}
					}
				}
			}

		} else {
			$tbl_name = $this->geoarea === 'nat' ? 'tower_'.$this->geo : $this->tablename;
			$this->db->select('RECTIFIER_1 r1, RECTIFIER_2 r2, RECTIFIER_3 r3');
			if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
			$this->db->from($tbl_name);
			$result = $this->db->get();

			if ($result->num_rows() > 0) {
				foreach ($result->result() as $row) {
					$rc1 = strtoupper($row->r1);
					$rc2 = strtoupper($row->r2);
					$rc3 = strtoupper($row->r3);
					if (($rc1 <> "TO BE UPDATED" && $rc1 <> "0" && $rc1 <> "NONE" && $rc1 <> "") &&
						($rc2 === "TO BE UPDATED" || $rc2 === "0" || $rc2 === "NONE" || $rc2 === "") &&
						($rc3 === "TO BE UPDATED" || $rc3 === "0" || $rc3 === "NONE" || $rc3 === "")) {
						$r1 += 1;
					} else if (($rc1 <> "TO BE UPDATED" && $rc1 <> "0" && $rc1 <> "NONE" && $rc1 <> "") &&
						($rc2 <> "TO BE UPDATED" && $rc2 <> "0" && $rc2 <> "NONE" && $rc2 <> "") &&
						($rc3 === "TO BE UPDATED" || $rc3 === "0" || $rc3 === "NONE" || $rc3 === "")) {
						$r2 += 1;
					} else if (($rc1 <> "TO BE UPDATED" && $rc1 <> "0" && $rc1 <> "NONE" && $rc1 <> "") &&
						($rc2 <> "TO BE UPDATED" && $rc2 <> "0" && $rc2 <> "NONE" && $rc2 <> "") &&
						($rc3 <> "TO BE UPDATED" && $rc3 <> "0" && $rc3 <> "NONE" && $rc3 <> "")) {
						$r3 += 1;
					} else if ($rc1 <> "TO BE UPDATED" || $rc2 <> "TO BE UPDATED" || $rc3 <> "TO BE UPDATED") {
						$tbu += 1;
					} else {
						$none += 1;
					}
				}
			}
		}

		return array(
			'R1' => $r1,
			'R2' => $r2,
			'R3' => $r3,
			'tbu' => $tbu,
			'none' => $none,
		);
	}

	public function get_dc_classAB()
	{
		$g0to30 = 0;
		$g30to40 = 0;
		$g40 = 0;
		$tbu = 0;
		$condition = "RDC_UTIL";
		if (empty($this->geo) && $this->geoarea === 'nat') {
			foreach ($this->regions as $keys => $values) {
				$this->db->select($condition . ' rs');
				$this->db->where('(SITE_CLASS = "A1" OR SITE_CLASS = "A2" OR SITE_CLASS = "B1")');
				$this->db->from('powerdb_'.$values);
				$result = $this->db->get();

				if ($result->num_rows() > 0) {
					foreach ($result->result() as $row) {
						$rs = $row->rs === null ? 0 : intval($row->rs);
						if ($rs === null) {
							$tbu += 1;
						} else if ($rs >= 0 && $rs <= 30) {
							$g0to30 += 1;
						} else if ($rs > 30 && $rs <= 40) {
							$g30to40 += 1;
						} else if ($rs > 40) {
							$g40 += 1;
						}
					}
				}
			}
		} else {
			$tbl_name = $this->geoarea === 'nat' ? 'powerdb_' . $this->geo : $this->tablename;
			$this->db->select($condition . ' rs');
			$this->db->where('(SITE_CLASS = "A1" OR SITE_CLASS = "A2" OR SITE_CLASS = "B1")');
			$this->db->from($tbl_name);
			$result = $this->db->get();

			if ($result->num_rows() > 0) {
				foreach ($result->result() as $row) {
					$rs = $row->rs === null ? 0 : intval($row->rs);
					if ($rs === null) {
						$tbu += 1;
					} else if ($rs >= 0 && $rs <= 30) {
						$g0to30 += 1;
					} else if ($rs > 30 && $rs <= 40) {
						$g30to40 += 1;
					} else if ($rs > 40) {
						$g40 += 1;
					}
				}
			}
		}

		return array(
			'g0to30' => $g0to30,
			'g30to40' => $g30to40,
			'g40' => $g40,
			'tbu' => $tbu
		);
	}

	public function get_dc_classBC()
	{
		$condition = "RDC_UTIL";
		$g0to60 = 0;
		$g60to75 = 0;
		$g75 = 0;
		$tbu = 0;
		$sc = 0;

		if (empty($this->geo) && $this->geoarea === 'nat') {
			foreach ($this->regions as $keys => $values) {
				$this->db->select($condition . ' rs');
				$this->db->where('(SITE_CLASS = "B2" OR SITE_CLASS = "B3" OR SITE_CLASS = "C1" OR SITE_CLASS = "C2" OR SITE_CLASS = "C3" OR SITE_CLASS = "C4")');
				$this->db->where("SITE_TYPE <>", 'SMALL CELL');
				$this->db->from('powerdb_'.$values);
				$result = $this->db->get();
				if ($result->num_rows() > 0) {
					foreach ($result->result() as $row) {
						$rs = $row->rs === null ? 0 : intval($row->rs);
						if ($rs === null) {
							$tbu += 1;
						} else if ($rs >= 0 && $rs <= 60) {
							$g0to60 += 1;
						} else if ($rs > 60 && $rs <= 75) {
							$g60to75 += 1;
						} else if ($rs > 75) {
							$g75 += 1;
						}
					}
				}

				#SMALL CELL
				$this->db->where("SITE_TYPE", 'SMALL CELL');
				$this->db->where("SITE_CLASS", 'C5');
				$sc += $this->db->get('powerdb_' . $values)->num_rows();
			}
		} else {
			$tbl_name = $this->geoarea === 'nat' ? 'powerdb_' . $this->geo : $this->tablename;
			$this->db->select($condition . ' rs');
			$this->db->where('(SITE_CLASS = "B2" OR SITE_CLASS = "B3" OR SITE_CLASS = "C1" OR SITE_CLASS = "C2" OR SITE_CLASS = "C3" OR SITE_CLASS = "C4")');
			$this->db->where("SITE_TYPE <>", 'SMALL CELL');
			$this->db->from($tbl_name);
			$result = $this->db->get();

			if ($result->num_rows() > 0) {
				foreach ($result->result() as $row) {
					$rs = $row->rs === null ? 0 : intval($row->rs);
					if ($rs === null) {
						$tbu += 1;
					} else if ($rs >= 0 && $rs <= 60) {
						$g0to60 += 1;
					} else if ($rs > 60 && $rs <= 75) {
						$g60to75 += 1;
					} else if ($rs > 75) {
						$g75 += 1;
					}
				}
			}
			#SMALL CELL
			$this->db->where("SITE_TYPE", 'SMALL CELL');
			$this->db->where("SITE_CLASS", 'C5');
			$sc = $this->db->get($tbl_name)->num_rows();
		}

		return array(
			'g0to60' => $g0to60,
			'g60to75' => $g60to75,
			'g75' => $g75,
			'tbu' => $tbu,
			'sc' => $sc
		);
	}

	public function get_genset_capacity()
	{
		$v1 = 0;
		$v2 = 0;
		$v3 = 0;
		$v4 = 0;
		$v5 = 0;
		$nn = 0;
		$tbu = 0;
		if (empty($this->geo) && $this->geoarea === 'nat') {
			#FOR NATIONAL
			foreach ($this->regions as $keys => $rows) {
				$this->db->select('GENSET_CAPACITY_KVA as gc');
				if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
				$this->db->from('powerdb_'.$rows);
				$result = $this->db->get();
				if ($result->num_rows() > 0) {
					foreach ($result->result() as $row) {
						#a. less 15 KVA (for identified genset less than 15)
						#b. 25 KVA
						#c. 40 KVA (37.5)
						#d. 50 KVA
						#e. > 50 KVA
						#f. None (indicates site has no GENSET)
						#g. Needs Validation (Filled up Genset is not correct - since declaration of Genset capacity is a fixed integral value eg. 15, 25, 40 etc)
						if ($row->gc == 'NA' || $row->gc == 'None')	{
							$nn += 1;
						} else if ($row->gc == 'To be Updated'){
							$tbu += 1;
						} else {
							$value = $row->gc === null ? null : floatval($row->gc);
							
							if ($value > 50) {
								$v1 += 1;
							} else if ($value == 50) {	
								$v2 += 1;
							} else if ($value >= 37.5 && $value <= 40) {
								$v3 += 1;
							} else if ($value == 25) {
								$v4 += 1;
							} else if ($value < 25) {
								$v5 += 1;
							}
						}					
					}
				}	
			}

		} else {
			$tbl_name = $this->geoarea === 'nat' ? 'powerdb_'.$this->input->post('geo_power') : $this->tablename;
			#FOR REGION
			$this->db->select('GENSET_CAPACITY_KVA as gc');
			if (!empty($this->sc)) {$this->db->where('SITE_CLASS ', $this->sc);}
			$result = $this->db->get($tbl_name);
			if ($result->num_rows() > 0) {
				foreach ($result->result() as $row) {
					#a. less 15 KVA (for identified genset less than 15)
					#b. 25 KVA
					#c. 40 KVA (37.5)
					#d. 50 KVA
					#e. > 50 KVA
					#f. None (indicates site has no GENSET)
					#g. Needs Validation (Filled up Genset is not correct - since declaration of Genset capacity is a fixed integral value eg. 15, 25, 40 etc)
					if ($row->gc == 'NA' || $row->gc == 'None')	{
						$nn += 1;
				 	} else if ($row->gc == 'To be Updated'){
						$tbu += 1;
					} else {
						$value = $row->gc === null ? null : floatval($row->gc);
						
						if ($value > 50) {
							$v1 += 1;
						} else if ($value == 50) {	
							$v2 += 1;
						} else if ($value >= 37.5 && $value <= 40) {
							$v3 += 1;
						} else if ($value == 25) {
							$v4 += 1;
						} else if ($value < 25) {
							$v5 += 1;
						}
					}					
				}
			}	

			#SMALL CELL
			$this->db->where("SITE_TYPE", 'SMALL CELL');
			$scc = $this->db->get($tbl_name)->num_rows();
		}

		$count = array(
			'v1' => $v1,
			'v2' => $v2,
			'v3' => $v3,
			'v4' => $v4,
			'v5' => $v5,
			'None' => $nn,
			'tbu' => $tbu
		);

		return $count;
	}

	// -----------------------------------------------------------------//

	public function get_rec()
	{
		$search = $this->input->post('search');
		$this->db->like('name', $search);
		$this->db->order_by('name', 'asc');
		return $this->db->get('power_rectifier')->result();
	}

	public function get_bat()
	{
		$search = $this->input->post('search');
		$this->db->like('name', $search);
		$this->db->order_by('name', 'asc');
		return $this->db->get('power_battery')->result();
	}

	public function get_rec_bat() {
		$this->db->order_by('name', 'asc');
		$rec = $this->db->get('power_rectifier')->result();

		$this->db->order_by('name', 'asc');
		$bat = $this->db->get('power_battery')->result();

		return array(
			'rec' => $rec,
			'bat' => $bat
		);
	}

	public function get_equip()
	{
		$this->db->order_by('item', 'asc');
		return $this->db->get('proposed_equipment')->result();
	}

	public function get_all_db_dropdowns() {
		$this->db->order_by('name', 'asc');
		$rect = $this->db->get('power_rectifier')->result();

		$this->db->order_by('name', 'asc');
		$batt = $this->db->get('power_battery')->result();

		$this->db->order_by('model', 'asc');
		$acu = $this->db->get('aircon_equipment')->result();

		$this->db->order_by('circuit_breaker', 'asc');
		$dropwire = $this->db->get('drop_wires_inventory')->result();
		
		return array(
			'rect' => $rect,
			'batt' => $batt,
			'acu' => $acu,
			'dropwire' => $dropwire
		);
	}

	public function add_rec()
	{
		$id = $this->input->post('id');
		$name = trim($this->input->post('name_rec'));
		$module_capacity = trim($this->input->post('module_capacity'));
		$no_of_modules = trim($this->input->post('no_of_modules'));
		$empty_modules = trim($this->input->post('empty_modules'));

		$data = array(
			'name' => $name,
			'module_capacity' => $module_capacity,
			'no_of_modules' => $no_of_modules,
			'empty_modules' => $empty_modules
		);

		if (empty($id)) {
			#CHECK IF NAME EXISTS
			$check = $this->db->get_where('power_rectifier', array('name' => $name))->num_rows();
			if ($check == 0) {
				$this->db->insert('power_rectifier', $data);
				$msg = 'Added';
			} else {
				$type = 'error';
				$title = 'Invalid!';
				$msg = 'Name already exists.';
				goto error1;
			}

		} else {
			$this->db->where('id', $id);
			$this->db->update('power_rectifier', $data);
			$msg = 'Updated';
		}

		$this->db->affected_rows();
		if ($this->db->affected_rows() === 1) {
			$type = 'success';
			$title = 'Success!';
			$msg = 'Data ' . $msg . '.';
		} else {
			$type = 'error';
			$title = 'Ooops!';
			$msg = 'Something went wrong.';
		}

		error1:
		return $data = array(
			'type' => $type,
			'title' => $title,
			'msg' => $msg
		);
	}

	public function add_equip()
	{
		$id = $this->input->post('id');
		$name = trim($this->input->post('name_equip'));
		$wattage = trim($this->input->post('wattage'));
		$data = array(
			'ITEM' => $name,
			'WATTAGE' => $wattage
		);

		if (empty($id)) {
			#CHECK IF NAME EXISTS
			$check = $this->db->get_where('proposed_equipment', array('ITEM' => $name))->num_rows();
			if ($check == 0) {
				$this->db->insert('proposed_equipment', $data);
				$msg = 'Added';
			} else {
				$type = 'error';
				$title = 'Invalid!';
				$msg = 'Name already exists.';
				goto error2;
			}
		} else {
			$this->db->where('id', $id);
			$this->db->update('proposed_equipment', $data);
			$msg = 'Updated';
		}

		$this->db->affected_rows();
		if ($this->db->affected_rows() === 1) {
			$type = 'success';
			$title = 'Success!';
			$msg = 'Data ' . $msg . '.';
		} else {
			$type = 'error';
			$title = 'Ooops!';
			$msg = 'Something went wrong.';
		}
		error2:
		return $data = array(
			'type' => $type,
			'title' => $title,
			'msg' => $msg
		);
	}

	public function del_equip()
	{
		$id = trim($this->input->post('id'));
		$this->db->where('id', $id);
		$this->db->delete('proposed_equipment');
		$this->db->affected_rows();
		if ($this->db->affected_rows() === 1) {
			$type = 'success';
			$title = 'Success!';
			$msg = 'Data deleted.';
		} else {
			$type = 'error';
			$title = 'Ooops!';
			$msg = 'Something went wrong.';
		}

		return $data = array(
			'type' => $type,
			'title' => $title,
			'msg' => $msg
		);
	}

	public function add_bat()
	{
		$id = $this->input->post('id');
		$name = trim($this->input->post('name_bat'));
//		$capacity = trim($this->input->post('capacity'));
		$data = array(
			'name' => $name,
//			'capacity' => $capacity
		);

		if (empty($id)) {
			#CHECK IF NAME EXISTS
			$check = $this->db->get_where('power_battery', array('name' => $name))->num_rows();
			if ($check == 0) {
				$this->db->insert('power_battery', $data);
				$msg = 'Added';
			} else {
				$type = 'error';
				$title = 'Invalid!';
				$msg = 'Name already exists.';
				goto error2;
			}
		} else {
			$this->db->where('id', $id);
			$this->db->update('power_battery', $data);
			$msg = 'Updated';
		}

		$this->db->affected_rows();
		if ($this->db->affected_rows() === 1) {
			$type = 'success';
			$title = 'Success!';
			$msg = 'Data ' . $msg . '.';
		} else {
			$type = 'error';
			$title = 'Ooops!';
			$msg = 'Something went wrong.';
		}
		error2:
		return $data = array(
			'type' => $type,
			'title' => $title,
			'msg' => $msg
		);
	}

	public function del_rec()
	{
		$id = trim($this->input->post('id'));
		$this->db->where('id', $id);
		$this->db->delete('power_rectifier');
		$this->db->affected_rows();
		if ($this->db->affected_rows() === 1) {
			$type = 'success';
			$title = 'Success!';
			$msg = 'Data deleted.';
		} else {
			$type = 'error';
			$title = 'Ooops!';
			$msg = 'Something went wrong.';
		}

		return $data = array(
			'type' => $type,
			'title' => $title,
			'msg' => $msg
		);
	}

	public function del_bat()
	{
		$id = trim($this->input->post('id'));
		$this->db->where('id', $id);
		$this->db->delete('power_battery');
		$this->db->affected_rows();
		if ($this->db->affected_rows() === 1) {
			$type = 'success';
			$title = 'Success!';
			$msg = 'New data inserted.';
		} else {
			$type = 'error';
			$title = 'Ooops!';
			$msg = 'Something went wrong.';
		}

		return $data = array(
			'type' => $type,
			'title' => $title,
			'msg' => $msg
		);
	}

	public function get_drop_wires() {
		$this->db->order_by('id', 'asc');
		return $this->db->get('drop_wires_inventory')->result();
	}

	public function get_prop_list() {
		#FOR NATIONAL
		if ($this->geoarea === 'nat') {
			#POWER
			#NLZ
			$this->db->select('id, batch_key as name, REGION');
			$this->db->from("tower_proposal_nlz");
			$tnlz = $this->db->get_compiled_select();
		
			#SLZ
			$this->db->select('id, batch_key as name, REGION');
			$this->db->from("tower_proposal_slz");
			$tslz = $this->db->get_compiled_select();

			#NCR
			$this->db->select('id, batch_key as name, REGION');
			$this->db->from("tower_proposal_ncr");
			$tncr = $this->db->get_compiled_select();

			#VIS
			$this->db->select('id, batch_key as name, REGION');
			$this->db->from("tower_proposal_vis");
			$tvis = $this->db->get_compiled_select();

			#MIN
			$this->db->select('id, batch_key as name, REGION');
			$this->db->from("tower_proposal_min");
			$tmin = $this->db->get_compiled_select();
		
			$tt = $this->db->query("SELECT * FROM (" . $tnlz." UNION ALL ".$tslz." UNION ALL " .$tncr. " UNION ALL " .$tvis. " UNION ALL " .$tmin. ") AS X ORDER BY id ASC");

			$tower = array();
			if ($tt->num_rows() > 0) {
				foreach($tt->result() as $row) {
					$tower[] = [
						'id' => $row->id,
						'title' => $row->REGION.'_'.$row->name,
						'name' => $row->name
					];
				}
			}
			//-----------------------------------------------------------------------------------------//	
			#POWER
			#NLZ
			$this->db->select('*');
			$this->db->from('power_proposal_tbl_nlz');
			$pnlz = $this->db->get_compiled_select();
			#SLZ
			$this->db->select('*');
			$this->db->from('power_proposal_tbl_slz');
			$pslz = $this->db->get_compiled_select();
			#NCR
			$this->db->select('*');
			$this->db->from('power_proposal_tbl_ncr');
			$pncr = $this->db->get_compiled_select();
			#VIS
			$this->db->select('*');
			$this->db->from('power_proposal_tbl_vis');
			$pvis = $this->db->get_compiled_select();
			#MIN
			$this->db->select('*');
			$this->db->from('power_proposal_tbl_min');
			$pmin =$this->db->get_compiled_select();

			$pp = $this->db->query("SELECT * FROM (" . $pnlz." UNION ALL ".$pslz." UNION ALL " .$pncr. " UNION ALL " .$pvis. " UNION ALL " .$pmin. ") AS X ORDER BY id ASC");
			$power = $pp->result();
		} else {
			#GET TOWER
			$this->db->order_by('id', 'asc');
			$this->db->select('id, batch_key as name, REGION');
			$this->db->from($this->tbl_tprop);
			$this->db->group_by('batch_key');
			$tt = $this->db->get();

			$tower = array();
			if ($tt->num_rows() > 0) {
				foreach($tt->result() as $row) {
					$tower[] = [
						'id' => $row->id,
						'title' => $row->REGION.'_'.$row->name,
						'name' => $row->name
					];
				}
			}

			#GET POWER
			$this->db->order_by('id', 'asc');
			$power = $this->db->get($this->tbl_prop)->result();
		}	

		
		return array(
			'tower' => $tower,
			'power' => $power,
		);
	}


	public function get_proposals() {
		$bid = $this->input->post('batch_key');
		$this->db->order_by('id', 'asc');
		$this->db->where('batch_key', $bid);
		return $this->db->get($this->power_propsal_mdb)->result();
	}

	public function del_prop() {
		$id = $this->input->post('id');
		$this->db->where('id', $id);
		$this->db->delete($this->tbl_prop);
		$this->db->where('batch_key', $id);
		$this->db->delete($this->power_propsal_mdb);
		return array(
			'type' => 'success',
			'title' => 'Success',
			'msg' => 'Proposal list removed.',
		);
	}

	public function del_all_prop() {
		$this->db->truncate($this->tbl_prop);
		$this->db->truncate($this->power_propsal_mdb);
		return array(
			'type' => 'success',
			'title' => 'Success',
			'msg' => 'All Proposal list removed.',
		);
	}

	public function edit_prop() {
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$this->db->where('id', $id);
		$this->db->set('name', $name);
		$this->db->update('power_proposal_tbl_'. $this->geoarea);
		return array(
			'type' => 'success',
			'title' => 'Success',
			'msg' => 'Proposal successfully renamed.',
		);
	}

	public function get_acu_model() {
		$this->db->order_by('id', 'asc');
		return $this->db->get('aircon_equipment')->result();
	}

	public function get_dc_util()
	{
		$condition = "(((RS1_ACTUAL_LOAD_ADC + RS2_ACTUAL_LOAD_ADC + RS3_ACTUAL_LOAD_ADC)*53.4) / ((RS1_MODULE_CAPACITY*RS1_NO_OF_MODULES)+(RS2_MODULE_CAPACITY*RS2_NO_OF_MODULES)+(RS3_MODULE_CAPACITY*RS3_NO_OF_MODULES)))*100";
		$l40 = $this->db->get_where($this->tablename, array($condition." < " => 40))->num_rows();
		$g40l60 = $this->db->get_where($this->tablename, array($condition .'>= ' => 40, $condition . ' < ' => 60))->num_rows();
		$g60l75 = $this->db->get_where($this->tablename, array($condition .'>= ' => 60, $condition . ' < ' => 75))->num_rows();
		$g75 = $this->db->get_where($this->tablename, array($condition .'>= ' => 75))->num_rows();

		return array(
			'l40' => $l40,
			'g40l60' => $g40l60,
			'g60l75' => $g60l75,
			'g75' => $g75
		);
	}

	function floatvalue($val){
		$val = str_replace(",",".",$val);
		$val = preg_replace('/\.(?=.*\.)/', '', $val);
		return floatval($val);
	}

	public function get_completion() {
		if ($this->geoarea == 'nat') {
			$completed = 0;
			$total = 0;
			$region = array();
			foreach ($this->regions as $keys => $values) {
				$this->db->select('COUNT(*) as completed, (SELECT COUNT(*) FROM powerdb_'.$values.') as total');
				$this->db->from('powerdb_'.$values);
				$this->db->where('completion', 100);
				$res = $this->db->get()->row();
				$completed += $res->completed;
				$total += $res->total;
			}

			return array(
				'completed' => $completed,
				'total' => $total
			);
		} else {
			$this->db->select('COUNT(*) as completed, (SELECT COUNT(*) FROM '.$this->tablename.') as total');
			$this->db->from($this->tablename);
			$this->db->where('completion', 100);
			return $this->db->get()->row();
		}
	}

	public function get_chart_completion() {
		$completed = 0;
		$total = 0;
		
		foreach ($this->regions as $keys => $values) {
			$this->db->select('COUNT(*) as completed, (SELECT COUNT(*) FROM powerdb_'.$values.') as total');
			$this->db->from('powerdb_'.$values);
			$this->db->where('completion', 100);
			$res = $this->db->get()->row();
			$region[$values] = array(
				'completed' => $res->completed,
				'total' => $res->total
			);
			$completed += $res->completed;
			$total += $res->total;
		}

		$region['overall'] = array(
			'completed' => $completed,
			'total' => $total
		);
		
		return $region;
	}

	public function get_assessments() {
		$pid = $this->input->post('pid');
		$tid = $this->input->post('tid');

		if ($this->geoarea == 'nat') {
			#FOR NATIONAL VIEW
			#POWER
			#NLZ
			$this->db->select('*');
			$this->db->from("tower_proposal_nlz");
			$tnlz = $this->db->get_compiled_select();
			#SLZ
			$this->db->select('*');
			$this->db->from("tower_proposal_slz");
			$tslz = $this->db->get_compiled_select();
			#NCR
			$this->db->select('*');
			$this->db->from("tower_proposal_ncr");
			$tncr = $this->db->get_compiled_select();
			#VIS
			$this->db->select('*');
			$this->db->from("tower_proposal_vis");
			$tvis = $this->db->get_compiled_select();
			#MIN
			$this->db->select('*');
			$this->db->from("tower_proposal_min");
			$tmin = $this->db->get_compiled_select();

			//-----------------------------------------------------------------------------------------//	
			#POWER
			#NLZ
			$this->db->select('*');
			$this->db->from('power_propsal_mdb_nlz');
			$pnlz = $this->db->get_compiled_select();
			#SLZ
			$this->db->select('*');
			$this->db->from('power_propsal_mdb_slz');
			$pslz = $this->db->get_compiled_select();
			#NCR
			$this->db->select('*');
			$this->db->from('power_propsal_mdb_ncr');
			$pncr = $this->db->get_compiled_select();
			#VIS
			$this->db->select('*');
			$this->db->from('power_propsal_mdb_vis');
			$pvis = $this->db->get_compiled_select();
			#MIN
			$this->db->select('*');
			$this->db->from('power_propsal_mdb_min');
			$pmin =$this->db->get_compiled_select();

			$result = $this->db->query("SELECT a.SITENAME, a.PLAID, a.REGION, a.TOWER_RECOMMENDATION, b.dc_recomendations, b.ac_recomendations FROM 
										(" . $tnlz." UNION ALL ".$tslz." UNION ALL " .$tncr. " UNION ALL " .$tvis. " UNION ALL " .$tmin. ") AS a
										LEFT JOIN
										(" . $pnlz." UNION ALL ".$pslz." UNION ALL " .$pncr. " UNION ALL " .$pvis. " UNION ALL " .$pmin. ") AS b
										ON a.PLAID = b.plaid
										WHERE b.batch_key = ? AND a.batch_key = ?
									   ", array($pid, $tid));
		} else {
			#POWER
			$this->db->select('a.SITENAME, a.PLAID, a.REGION, a.TOWER_RECOMMENDATION, b.dc_recomendations, b.ac_recomendations');
			$this->db->from($this->tbl_tprop.' a');
			$this->db->join($this->power_propsal_mdb.' b', 'a.PLAID = b.plaid');
			$this->db->where('b.batch_key', $pid);
			$this->db->where('a.batch_key', $tid);
			$this->db->where('a.REGION', $this->geoarea);
			$result = $this->db->get();
		}	


		$data = array();
		if ($result->num_rows() > 0) {

			foreach ($result->result() as $row) {
				$tower_rec = trim($row->TOWER_RECOMMENDATION);
				$ac = strtoupper(trim($row->ac_recomendations)) === 'READY TO BUILD' ? 'Sufficient' : 'For Upgrade';
				$dc = trim($row->dc_recomendations);
				$overall_power = 'For Upgrade';
				#GET OVERALL POWER RESULT
				if (trim(strtoupper($ac)) === 'SUFFICIENT' && trim(strtoupper($dc)) === 'SUFFICIENT') {
					$overall_power = 'Sufficient';
				}

				$simp_overall_ass = 'Requires Upgrade';
				#GET SIMPLIFIED OVERALL FACILITIES ASSESSMENT
				if (strtoupper($tower_rec) === 'PROCEED' && strtoupper($overall_power) === 'SUFFICIENT') {
					$simp_overall_ass = 'Ready to Build';
				} else if (strtoupper($tower_rec) === 'PROCEED_TOWER @ THRESHOLD' && strtoupper($overall_power) === 'SUFFICIENT') {
					$simp_overall_ass = 'Ready to Build_@ Threshold';
				}

				#GET FACILITIES UPGRADE SCOPE
				$fus = "No Upgrade Requirement";
				switch (strtoupper($tower_rec)) {
					case 'PROCEED':
						if (strtoupper($ac) === 'SUFFICIENT' && strtoupper($dc) === 'SUFFICIENT') {
							$fus = "No Upgrade Requirement";
						} else if (strtoupper($ac) === 'SUFFICIENT' && strtoupper($dc) === 'FOR UPGRADE') {
							$fus = "Power DC Upgrade";
						} else if (strtoupper($ac) === 'FOR UPGRADE' && strtoupper($dc) === 'SUFFICIENT') {
							$fus = "Power AC Upgrade";
						} else if (strtoupper($ac) === 'FOR UPGRADE' && strtoupper($dc) === 'FOR UPGRADE') {
							$fus = "Power AC & DC Upgrade";
						}
					break;
					case 'PROCEED_TOWER @ THRESHOLD':
						if (strtoupper($ac) == 'SUFFICIENT' && strtoupper($dc) == 'SUFFICIENT') {
							$fus = "Tower @ Threshold + No Power Upgrade Requirement";
						} else if (strtoupper($ac) == 'SUFFICIENT' && strtoupper($dc) == 'FOR UPGRADE') {
							$fus = "Tower @ Threshold + Power DC Upgrade";
						} else if (strtoupper($ac) == 'FOR UPGRADE' && strtoupper($dc) == 'SUFFICIENT') {
							$fus = "Tower @ Threshold + Power AC Upgrade";
						} else if (strtoupper($ac) == 'FOR UPGRADE' && strtoupper($dc) == 'FOR UPGRADE') {
							$fus = "Tower @ Threshold + Power AC & DC Upgrade";
						}
					break;
					case 'REQUIRES UPGRADE':
						if(strtoupper($ac) == 'SUFFICIENT' && strtoupper($dc) == 'SUFFICIENT') {
							$fus = "Tower Upgrade";
						} else if(strtoupper($ac) == 'SUFFICIENT' && strtoupper($dc) == 'FOR UPGRADE') {
							$fus = "Tower + Power DC Upgrade";
						} else if(strtoupper($ac) == 'FOR UPGRADE' && strtoupper($dc) == 'SUFFICIENT') {
							$fus = "Tower + Power AC Upgrade";
						} else if(strtoupper($ac) == 'FOR UPGRADE' && strtoupper($dc) == 'FOR UPGRADE') {
							$fus = "Tower + Power AC & DC Upgrade";
						}
					break;
				}

				#GET OVERALL FACILITIES ASSESSMENT
				$overall_fac_ass = '';
				if (strtoupper($tower_rec) === 'PROCEED' && strtoupper($overall_power) == 'SUFFICIENT') {
					$overall_fac_ass = "RFI";
				} else if (strtoupper($tower_rec) === 'PROCEED' && strtoupper($overall_power) == 'FOR UPGRADE') {
					if (strtoupper($ac) === 'FOR UPGRADE' && strtoupper($dc) === 'FOR UPGRADE') {
						$power = 'AC & DC';
				    } else if (strtoupper($ac) === 'SUFFICIENT' && strtoupper($dc) === 'FOR UPGRADE') {
						$power = 'DC';
					} else {
						$power = 'AC';
					}
					$overall_fac_ass = "Power ".$power." Requires Upgrade + Tower RFI";
				} else if (strtoupper($tower_rec) === 'REQUIRES UPGRADE' && strtoupper($overall_power) == 'SUFFICIENT') {
				 	$overall_fac_ass = "Power RFI, Tower for SI Upgrade";
				} else if(strtoupper($tower_rec) === 'PROCEED_TOWER @ THRESHOLD' && strtoupper($overall_power) == 'SUFFICIENT') {
					$overall_fac_ass = "Power RFI, Tower @ Threshold";
				} else if (strtoupper($tower_rec) === 'PROCEED_TOWER @ THRESHOLD' && strtoupper($overall_power) == 'FOR UPGRADE') {
					if (strtoupper($ac) === 'FOR UPGRADE' && strtoupper($dc) === 'FOR UPGRADE') {
						$power = 'AC & DC';
					} else if (strtoupper($ac) === 'SUFFICIENT' && strtoupper($dc) === 'FOR UPGRADE') {
						$power = 'DC';
					} else {
						$power = 'AC';
					}
					$overall_fac_ass = "Power ".$power." Requires Upgrade + Tower @ Threshold";
				} else if (strtoupper($tower_rec) === 'REQUIRES UPGRADE' && strtoupper($overall_power) == 'FOR UPGRADE') {
					if (strtoupper($ac) === 'FOR UPGRADE' && strtoupper($dc) === 'FOR UPGRADE') {
						$power = 'AC & DC';
					} else if (strtoupper($ac) === 'SUFFICIENT' && strtoupper($dc) === 'FOR UPGRADE') {
						$power = 'DC';
					} else {
						$power = 'AC';
					}
					$overall_fac_ass = "Power ".$power." Requires Upgrade & Tower for SI";
				}

				$data[] = array(
					'plaid' => $row->PLAID,
					'sitename' => $row->SITENAME,
					'region' => $row->REGION,
					'tower_ass' => $tower_rec,
					'ac_ass' => $ac,
					'dc_ass' => $dc,
					'overall_power_ass' => $overall_power,
					'simp_overall_ass' => $simp_overall_ass,
					'fac_upgrade_scope' => $fus,
					'overall_facilities_ass' => $overall_fac_ass
				);
			}
		}

		return $data;
	}

	public function get_power_reports()
	{
		$data = array();
		$total_tcpp = 0;
		$total_tlss = 0;
		$total_tg = 0;

		$total_ttcp = 0;
		$total_tl = 0;
		$total_tgp = 0;
		$total_tbu = 0;

		$total_sub_pps = 0;
		$total_sub_tps = 0;
		$total_total = 0;

		foreach ($this->regions as $keys => $values) {
			#TAP TO COMMERCIAL POWER PROVIDER
			$this->db->where('POWER_CONNECTION', 'Sole Transformer');
			$this->db->or_where('POWER_CONNECTION', 'Shared Transformer');
			$tcpp = $this->db->get('powerdb_'.$values)->num_rows();

			#TAP TO LESSOR VIA SPCA/Sub-Meter
			$this->db->where('POWER_CONNECTION', 'Tapped to Building');
			$tlss = $this->db->get('powerdb_'.$values)->num_rows();

			#TAP TO GENSET BY DESIGN
			$this->db->where('POWER_CONNECTION', 'Genset Prime');
			$tg = $this->db->get('powerdb_'.$values)->num_rows();

			#TEMPO TAP TO COMMERCIAL POWER
			$this->db->where('POWER_CONNECTION', 'Tempo to Coop');
			$ttcpp = $this->db->get('powerdb_'.$values)->num_rows();

			#TAP TO LESSOR
			$this->db->where('POWER_CONNECTION', 'Tapped to Neighbor');
			$tl = $this->db->get('powerdb_'.$values)->num_rows();

			#TAP TO GENSET
			$this->db->where('POWER_CONNECTION', 'Tapped to Genset');
			$tgp = $this->db->get('powerdb_'.$values)->num_rows();

			#TO BE UPDATED(PROGRESSIVE)
			$this->db->where('POWER_CONNECTION', 'To be updated');
			$tbu = $this->db->get('powerdb_'.$values)->num_rows();

			$total_tcpp += $tcpp;
			$total_tlss += $tlss; 
			$total_tg   += $tg;

			$total_ttcp += $ttcpp;
			$total_tl += $tl;
			$total_tgp += $tgp;
			$total_tbu += $tbu;

			$sub_pps = $tcpp + $tlss + $tg;
			$sub_tps = $ttcpp + $tl + $tgp;

			$total_sub_pps += $sub_pps;
			$total_sub_tps += $sub_tps;

			$total_total += $sub_pps + $sub_tps + $tbu;

			$data[$values] = array(
					'tcpp' => $tcpp,
					'tlss' => $tlss,
					'tg' => $tg,
					'sub_pps' => $sub_pps,

					'ttcp' => $ttcpp,
					'tl' => $tl,
					'tgp' => $tgp,
					'sub_tps' => $sub_tps,
	
					'tbu' => $tbu,
					'total' => $sub_pps + $sub_tps + $tbu
					
			);

		}

		return array(
			'region' => $data,
			'total_tccp' => $total_tcpp,
			'total_tlss' => $total_tlss,
			'total_tg' => $total_tg,
			'total_ttcp' => $total_ttcp,
			'total_tl' => $total_tl,
			'total_tgp' => $total_tgp,
			'total_sub_pps' => $total_sub_pps,
			'total_sub_tps' => $total_sub_tps,
			'total_tbu' => $total_tbu,
			'total_total' => $total_total
		);
	}

	public function get_power_profile() {
		$plaid = $this->input->post('plaid');
		$id = $this->input->post('id');
		$region = strtolower($this->input->post('region'));
		
		$data = array();

		$this->db->where('id', $id);
		$this->db->where('PLAID', $plaid);
		$res = $this->db->get('powerdb_'.$region);
		
		if ($res->num_rows() > 0) {
			foreach($res->result() as $row) {
				$data['basic_info'] = array(
					'SITENAME' => $row->SITENAME,
					'PLAID' => $row->PLAID,
					'SITE CLASS' => $row->SITE_CLASS,
					'SITE TYPE' => $row->SITE_TYPE,
					'CABIN QUANTITY' => $row->CABIN_QUANTITY,
					'CABIN TYPE' => $row->CABIN_TYPE,
					'POWER CONNECTION' => $row->POWER_CONNECTION,
					'PHASE' => $row->POWER_PHASE,
					'GRID STATUS' => $row->GRID_STATUS,
					'TRANSFORMER CAPACITY' => $row->TRANSFORMER_CAPACITY_KVA,
					'GENSET CAPACITY' => $row->GENSET_CAPACITY_KVA,
					'MDB / ECB' => $row->MDP_ECB_RATING_AT,
					'ACPDB RATING AT' => $row->ACPDB_RATING_AT,
					'ECB CLAMP READING AAC' => $row->ECB_CLAMP_READING_AAC,
					'ECB CLAMP READING AAC' => $row->ECB_CLAMP_READING_AAC,
					'DROP WIRES' => $row->DROP_WIRES,
					'SITE COMPLETION(%)' => $row->completion,
					'DATE UPDATED' => $row->date,
				);
				$data['acu'] = array(
					'ACU QUANTITY' => $row->ACU_QUANTITY,
					'ACU CAPACITY HP' => $row->ACU_CAPACITY_HP,
					'ACU TYPE' => $row->ACU_TYPE,
					'ACU APPROX LOAD' => $row->ACU_APPROX_LOAD,
					'OTHER AC LOAD AAC' => $row->OTHER_AC_LOAD_AAC,
				);
				$data['rectifier'] = array(
					'rec_1' => array(
						'NAME' => $row->RECTIFIER_1,
						'MODULE CAPACITY' => $row->RS1_MODULE_CAPACITY,
						'NO OF MODULES' => $row->RS1_NO_OF_MODULES,
						'NO OF EMPTY MODULES' => $row->RS1_NO_OF_EMPTY_MODULE,
						'ACTUAL LOAD' => $row->RS1_ACTUAL_LOAD_ADC,
						'BATTERY BRAND' => $row->RS1_BATTERY_BRAND,
						'BATTERY TYPE' => $row->RS1_BATTERY_TYPE,
						'BATTERY CAPACITY' => $row->RS1_BATTERY_CAPACITY
					),
					'rec_2' => array(
						'NAME' => $row->RECTIFIER_2,
						'MODULE CAPACITY' => $row->RS2_MODULE_CAPACITY,
						'NO OF MODULES' => $row->RS1_NO_OF_MODULES,
						'NO OF EMPTY MODULES' => $row->RS2_NO_OF_EMPTY_MODULE,
						'ACTUAL LOAD' => $row->RS2_ACTUAL_LOAD_ADC,
						'BATTERY BRAND' => $row->RS2_BATTERY_BRAND,
						'BATTERY TYPE' => $row->RS2_BATTERY_TYPE,
						'BATTERY CAPACITY' => $row->RS2_BATTERY_CAPACITY
					),
					'rec_3' => array(
						'NAME' => $row->RECTIFIER_3,
						'MODULE CAPACITY' => $row->RS3_MODULE_CAPACITY,
						'NO OF MODULES' => $row->RS3_NO_OF_MODULES,
						'NO OF EMPTY MODULES' => $row->RS3_NO_OF_EMPTY_MODULE,
						'ACTUAL LOAD' => $row->RS3_ACTUAL_LOAD_ADC,
						'BATTERY BRAND' => $row->RS3_BATTERY_BRAND,
						'BATTERY TYPE' => $row->RS3_BATTERY_TYPE,
						'BATTERY CAPACITY' => $row->RS3_BATTERY_CAPACITY
					),
				);
				$data['pat'] = array(
					'BUILD SCOPE - A' => $row->BUILD_SCOPE_A,
					'BUILD SCOPE - B' => $row->BUILD_SCOPE_B,
					'BUILD SCOPE - C' => $row->BUILD_SCOPE_C,
					'BUILD SCOPE - D' => $row->BUILD_SCOPE_D,
					'REMARKS' 		  => $row->BUILD_REMARKS,
					'COMPLETION (%)'  => $row->BUILD_COMPLETION,
					
				);
				$data['headroom'] = array(
					'BATTERY CHARGING COEFFICIENT' => $row->BATTERY_CHARGING_COEFFICIENT,
					'RS1 DC HEADROOM (W)' => $row->RS1_DC_HEADROOM_W,
					'RS1 BATTERY HOURS'   => $row->RS1_BATTERY_HOURS,
					'RS2 DC HEADROOM (W)' => $row->RS2_DC_HEADROOM_W,
					'RS2 BATTERY HOURS'   => $row->RS2_BATTERY_HOURS,
					'RS3 DC HEADROOM (W)' => $row->RS3_DC_HEADROOM_W,
					'RS3 BATTERY HOURS'   => $row->RS3_BATTERY_HOURS,
					'TOTAL AC LOAD (A)'   => $row->TOTAL_AC_LOAD_A,
					'TRANSFORMER HEADROOM (W)'   => $row->TRANSFORMER_HEADROOM_W,
					'ECB HEADROOM (A)'   => $row->ECB_HEADROOM_A,
					'ACPDB HEADROOM (A)'   => $row->ACPDB_HEADROOM_A,
					'GENSET HEADROOM (W)'   => $row->GENSET_HEADROOM_W,
					'REMAINING WATTAGE HEADROOM (DC)'   => $row->REMAINING_WATTAGE_HEADROOM_DC,
					'REMAINING WATTAGE HEADROOM (AC)'   => $row->REMAINING_WATTAGE_HEADROOM_AC,
				);
				
				$data['util'] = array(
					'RS1 MODULE UTIL' => $row->RS1_MODULE_UTIL,
					'RS2 MODULE UTIL' => $row->RS2_MODULE_UTIL,
					'RS3 MODULE UTIL' => $row->RS3_MODULE_UTIL,
					'TRANSFORMER UTIL'=> $row->TRANSFORMER_UTIL,
					'GENSET UTIL'     => $row->GENSET_UTIL,
					'ECB UTIL'        => $row->ECB_UTIL,
					'ACPDB UTIL'      => $row->ACPDB_UTIL,
					'SITE UTIL'       => $row->SITE_UTIL,
				);
			} 
		}

		#2G
		$this->db->where('PLAID', $plaid);
		$g2 = $this->db->get($this->tbl2g);

		#3G
		$this->db->where('PLAID', $plaid);
		$g3 = $this->db->get($this->tbl3g);

		#4G
		$this->db->where('PLAID', $plaid);
		$g4 = $this->db->get($this->tbl4g);

		#5G
		$this->db->where('PLAID', $plaid);
		$g5 = $this->db->get($this->tbl5g);

		$darr = [];
		$stats = 0;
		if ($g2->num_rows() > 0) {
			$res = $g2->result();
			$stats = 2;
		} else if ($g3->num_rows() > 0) {
			$res = $g3->result();
			$stats = 3;	
		} else if ($g4->num_rows() > 0) {
			$res = $g4->result();
			$stats = 4;
		} else {
			$res = $g5->result();
			$stats = 5;
		}

		foreach($res as $row){
			$darr = array(
				'SITENAME' => $stats == 5 ? $row->NR_Name : ($stats == 2 ? $row->BTS_NAME : $row->SITE_NAME),
				'PLAID' => '',
				'Site_Solution' => $row->Site_Solution,
				'CELL_COVERAGE_TYPE' => $row->CELL_COVERAGE_TYPE,
				'BARANGAY' => $row->BARANGAY,
				'TOWN' => $row->TOWN,
				'PROVINCE' => $row->PROVINCE,
				'REGIONAL_AREA' => $row->REGIONAL_AREA,
				'VENDOR' => $row->VENDOR,
				'STATUS' => $row->STATUS,
				'LATITUDE' => $row->LATITUDE,
				'LONGITUDE' => $row->LONGITUDE
			);
		}
		
		
		return array(
			'fe' => $data,
			'tech' => array(
				'prof' => $darr,
				'g2' => $g2->result(),
				'g3' => $g3->result(),
				'g4' => $g4->result(),
				'g5' => $g5->result()
			)
		);
	}
}
