<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Planning_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->geoarea = $this->session->userdata('geoarea');

        $this->tbl2g = "plan_2g";
		$this->tbl3g = "plan_3g";
		$this->tbl4g = "plan_4g";
		$this->tbl5g = "plan_5g";

        $this->column_search = array('PLAID','SITE_NAME','Site_Solution','CELL_NAME','GEOGRAPHICAL_AREA', 'REGIONAL_AREA', 'REGION', 'Antenna_Type', 'VENDOR'); 
        $this->order = array('SITE_NAME' => 'asc');
	}

    #2G Upload
    public function p2g() {	
        if (isset($_FILES["plan"]["name"])) {
            $path = $_FILES["plan"]["tmp_name"];
            $object= PHPExcel_IOFactory::load($path);

            #GET FORMAT HEADER OF EXCEL
            $cell_collection = $object->getActiveSheet()->getCellCollection();
            foreach ($cell_collection as $cell) {
                $column = $object->getActiveSheet()->getCell($cell)->getColumn();
                $row = $object->getActiveSheet()->getCell($cell)->getRow();
                $data_value = $object->getActiveSheet()->getCell($cell)->getValue();
                //The header will/should be in row 1 only. of course, this can be modified to suit your need.
                if ($row == 1) {
                    $header[$row][$column] = $data_value;
                }
            }

            #CHECK IF EXCEL IMPORT MATCHES TO DATABASE FORMAT OF EXCEL
            if (29 == count($header[1])) {
                foreach ($object->getWorksheetIterator() as $worksheet) {
                    $highestRow = $worksheet->getHighestRow();
                    for ($row = 2; $row <= $highestRow; $row++) {
                        $data[] = array(
                            'PLAID' => $worksheet->getCellByColumnAndRow(0, $row)->getValue(),
                            'SITE_NAME' => $worksheet->getCellByColumnAndRow(1, $row)->getValue(),
                            'Site_Solution' => $worksheet->getCellByColumnAndRow(2, $row)->getValue(),
                            'BTS_NAME' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
                            'CELL_NAME' => $worksheet->getCellByColumnAndRow(4, $row)->getValue(),
                            'FREQ_BAND' => $worksheet->getCellByColumnAndRow(5, $row)->getValue(),
                            'BSC' => $worksheet->getCellByColumnAndRow(6, $row)->getValue(),
                            'LAC' => $worksheet->getCellByColumnAndRow(7, $row)->getValue(),
                            'CELL_IDENTIFIER' => $worksheet->getCellByColumnAndRow(8, $row)->getValue(),
                            "ADMINISTRATIVE_STATE" => $worksheet->getCellByColumnAndRow(9, $row)->getValue(),
                            'CELL_COVERAGE_TYPE' => $worksheet->getCellByColumnAndRow(10, $row)->getValue(),
                            'BUILDING_NAME' => $worksheet->getCellByColumnAndRow(11, $row)->getValue(),
                            'GEOGRAPHICAL_AREA' => $worksheet->getCellByColumnAndRow(12, $row)->getValue(),
                            'REGIONAL_AREA' => $worksheet->getCellByColumnAndRow(13, $row)->getValue(),
                            'REGION' => $worksheet->getCellByColumnAndRow(14, $row)->getValue(),
                            'PROVINCE' => $worksheet->getCellByColumnAndRow(15, $row)->getValue(),
                            'TOWN' => $worksheet->getCellByColumnAndRow(16, $row)->getValue(),
                            'BARANGAY' => $worksheet->getCellByColumnAndRow(17, $row)->getValue(),
                            'SITE_ADDRESS' => $worksheet->getCellByColumnAndRow(18, $row)->getValue(),
                            'LONGITUDE' => $worksheet->getCellByColumnAndRow(19, $row)->getValue(),
                            'LATITUDE' => $worksheet->getCellByColumnAndRow(20, $row)->getValue(),
                            'STATUS' => $worksheet->getCellByColumnAndRow(21, $row)->getValue(),
                            'Antenna_Type' => $worksheet->getCellByColumnAndRow(22, $row)->getValue(),
                            'ACL' => $worksheet->getCellByColumnAndRow(23, $row)->getValue(),
                            'Azimuth' => $worksheet->getCellByColumnAndRow(24, $row)->getValue(),
                            'Mechanical_Tilt' => $worksheet->getCellByColumnAndRow(25, $row)->getValue(),
                            'Electrical_Tilt' => $worksheet->getCellByColumnAndRow(26, $row)->getValue(),
                            'NE_STATE' => $worksheet->getCellByColumnAndRow(27, $row)->getValue(),
                            'VENDOR' => $worksheet->getCellByColumnAndRow(28, $row)->getValue(),

                            'date' => date('Y-m-d H:i:s'),
                        );
                    }
                }

                #TRUNCATE TABLE BEFORE INSERT
                $this->db->truncate($this->tbl2g);
                #CLEAR DATA BEFORE INSERT
                $this->db->insert_batch($this->tbl2g, array_filter($data));
                if ($this->db->affected_rows() == 0) {
                    $json = array(
                        'title' => 'Oops!',
                        'msg'   => 'Upload failed.',
                        'type'  => 'error',
                    );
                } else {
                    #GET RANK WORST CELLS PER CELL NAMES OR SITE
                    $json = array(
                        'title' => 'Success',
                        'msg'   => 'Successfully uploaded.',
                        'type'  => 'success',
                    );
                }
            } else {
                $json = array(
                    'title' => 'Oops!',
                    'msg'   => 'Import file does not match to desired format.',
                    'type'  => 'error',
                );
            }
        } else {
            $json = array(
                'title' => 'Ooops!',
                'msg'   => 'Something went wrong.',
                'type'  => 'error',
                'date' => ''
            );
        }
			
		

		return $json;
	}

    #3G Upload
    public function p3g() {	
        if (isset($_FILES["plan"]["name"])) {
            $path = $_FILES["plan"]["tmp_name"];
            $object= PHPExcel_IOFactory::load($path);

            #GET FORMAT HEADER OF EXCEL
            $cell_collection = $object->getActiveSheet()->getCellCollection();
            foreach ($cell_collection as $cell) {
                $column = $object->getActiveSheet()->getCell($cell)->getColumn();
                $row = $object->getActiveSheet()->getCell($cell)->getRow();
                $data_value = $object->getActiveSheet()->getCell($cell)->getValue();
                //The header will/should be in row 1 only. of course, this can be modified to suit your need.
                if ($row == 1) {
                    $header[$row][$column] = $data_value;
                }
            }

            #CHECK IF EXCEL IMPORT MATCHES TO DATABASE FORMAT OF EXCEL
            if (34 == count($header[1])) {
                foreach ($object->getWorksheetIterator() as $worksheet) {
                    $highestRow = $worksheet->getHighestRow();
                    for ($row = 2; $row <= $highestRow; $row++) {
                        $data[] = array(
                            'PLAID' => $worksheet->getCellByColumnAndRow(0, $row)->getValue(),
                            'SITE_NAME' => $worksheet->getCellByColumnAndRow(1, $row)->getValue(),
                            'Site_Solution' => $worksheet->getCellByColumnAndRow(2, $row)->getValue(),
                            'NodeB_NAME' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
                            'CELL_NAME' => $worksheet->getCellByColumnAndRow(4, $row)->getValue(),
                            'BAND_INDICATOR' => $worksheet->getCellByColumnAndRow(5, $row)->getValue(),
                            'DL_UARFCN' => $worksheet->getCellByColumnAndRow(6, $row)->getValue(),
                            'RNC' => $worksheet->getCellByColumnAndRow(7, $row)->getValue(),
                            "DL_PRIMARY_SCRAMBLING_CODE" => $worksheet->getCellByColumnAndRow(8, $row)->getValue(),
                            'LAC' => $worksheet->getCellByColumnAndRow(9, $row)->getValue(),
                            'CELL_IDENTIFIER' => $worksheet->getCellByColumnAndRow(10, $row)->getValue(),
                            'VALIDATION_INDICATION' => $worksheet->getCellByColumnAndRow(11, $row)->getValue(),
                            'CELL_ADMINISTRATIVE_STATE' => $worksheet->getCellByColumnAndRow(12, $row)->getValue(),
                            'CELL_COVERAGE_TYPE' => $worksheet->getCellByColumnAndRow(13, $row)->getValue(),
                            'BUILDING_NAME' => $worksheet->getCellByColumnAndRow(14, $row)->getValue(),
                            'GEOGRAPHICAL_AREA' => $worksheet->getCellByColumnAndRow(15, $row)->getValue(),
                            'REGIONAL_AREA' => $worksheet->getCellByColumnAndRow(16, $row)->getValue(),
                            'REGION' => $worksheet->getCellByColumnAndRow(17, $row)->getValue(),
                            'PROVINCE' => $worksheet->getCellByColumnAndRow(18, $row)->getValue(),
                            'TOWN' => $worksheet->getCellByColumnAndRow(19, $row)->getValue(),
                            'BARANGAY' => $worksheet->getCellByColumnAndRow(20, $row)->getValue(),
                            'SITE_ADDRESS' => $worksheet->getCellByColumnAndRow(21, $row)->getValue(),
                            'LONGITUDE' => $worksheet->getCellByColumnAndRow(22, $row)->getValue(),
                            'LATITUDE' => $worksheet->getCellByColumnAndRow(23, $row)->getValue(),
                            'STATUS' => $worksheet->getCellByColumnAndRow(24, $row)->getValue(),
                            'Antenna_Type' => $worksheet->getCellByColumnAndRow(25, $row)->getValue(),
                            'ACL' => $worksheet->getCellByColumnAndRow(26, $row)->getValue(),
                            'Azimuth' => $worksheet->getCellByColumnAndRow(27, $row)->getValue(),
                            'Mechanical_Tilt' => $worksheet->getCellByColumnAndRow(28, $row)->getValue(),
                            'Electrical_Tilt' => $worksheet->getCellByColumnAndRow(39, $row)->getValue(),
                            'NE_STATE' => $worksheet->getCellByColumnAndRow(30, $row)->getValue(),
                            'VENDOR' => $worksheet->getCellByColumnAndRow(31, $row)->getValue(),
                            'DUAL_BEAM_TAGGING' => $worksheet->getCellByColumnAndRow(32, $row)->getValue(),
                            'SECTOR_NAME' => $worksheet->getCellByColumnAndRow(33, $row)->getValue(),

                            'date' => date('Y-m-d H:i:s'),
                        );
                    }
                }

                #TRUNCATE TABLE BEFORE INSERT
                $this->db->truncate($this->tbl3g);
                #CLEAR DATA BEFORE INSERT
                $this->db->insert_batch($this->tbl3g, array_filter($data));
                if ($this->db->affected_rows() == 0) {
                    $json = array(
                        'title' => 'Oops!',
                        'msg'   => 'Upload failed.',
                        'type'  => 'error',
                    );
                } else {
                    #GET RANK WORST CELLS PER CELL NAMES OR SITE
                    $json = array(
                        'title' => 'Success',
                        'msg'   => 'Successfully uploaded.',
                        'type'  => 'success',
                    );
                }
            } else {
                $json = array(
                    'title' => 'Oops!',
                    'msg'   => 'Import file does not match to desired format.',
                    'type'  => 'error',
                );
            }
        } else {
            $json = array(
                'title' => 'Ooops!',
                'msg'   => 'Something went wrong.',
                'type'  => 'error',
                'date' => ''
            );
        }
			
		

		return $json;
	}

    #4G Upload
    public function p4g() {	
        if (isset($_FILES["plan"]["name"])) {
            $path = $_FILES["plan"]["tmp_name"];
            $object= PHPExcel_IOFactory::load($path);

            #GET FORMAT HEADER OF EXCEL
            $cell_collection = $object->getActiveSheet()->getCellCollection();
            foreach ($cell_collection as $cell) {
                $column = $object->getActiveSheet()->getCell($cell)->getColumn();
                $row = $object->getActiveSheet()->getCell($cell)->getRow();
                $data_value = $object->getActiveSheet()->getCell($cell)->getValue();
                //The header will/should be in row 1 only. of course, this can be modified to suit your need.
                if ($row == 1) {
                    $header[$row][$column] = $data_value;
                }
            }

            #CHECK IF EXCEL IMPORT MATCHES TO DATABASE FORMAT OF EXCEL
            if (34 == count($header[1])) {
                $region = '';
                foreach ($object->getWorksheetIterator() as $worksheet) {
                    $highestRow = $worksheet->getHighestRow();
                    for ($row = 2; $row <= $highestRow; $row++) {
                       $region = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                        $data[] = array(
                            'PLAID' => $worksheet->getCellByColumnAndRow(0, $row)->getValue(),
                            'SITE_NAME' => $worksheet->getCellByColumnAndRow(1, $row)->getValue(),
                            'Site_Solution' => $worksheet->getCellByColumnAndRow(2, $row)->getValue(),
                            'eNodeBName' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
                            'CELL_NAME' => $worksheet->getCellByColumnAndRow(4, $row)->getValue(),
                            'LTE_BAND' => $worksheet->getCellByColumnAndRow(5, $row)->getValue(),
                            'AT_HOME_MOBILE' => $worksheet->getCellByColumnAndRow(6, $row)->getValue(),
                            "eNodeBID" => $worksheet->getCellByColumnAndRow(7, $row)->getValue(),
                            'CGI' => $worksheet->getCellByColumnAndRow(8, $row)->getValue(),
                            'Sector_ID' => $worksheet->getCellByColumnAndRow(9, $row)->getValue(),
                            'Local_Cell_ID' => $worksheet->getCellByColumnAndRow(10, $row)->getValue(),
                            'Cell_ID' => $worksheet->getCellByColumnAndRow(11, $row)->getValue(),
                            'PCI' => $worksheet->getCellByColumnAndRow(12, $row)->getValue(),
                            'TAC' => $worksheet->getCellByColumnAndRow(13, $row)->getValue(),
                            'CELL_ADMINISTRATIVE_STATE' => $worksheet->getCellByColumnAndRow(14, $row)->getValue(),
                            'CELL_COVERAGE_TYPE' => $worksheet->getCellByColumnAndRow(15, $row)->getValue(),
                            'VENDOR' => $worksheet->getCellByColumnAndRow(16, $row)->getValue(),
                            'GEOGRAPHICAL_AREA' => $worksheet->getCellByColumnAndRow(17, $row)->getValue(),
                            'REGIONAL_AREA' => $worksheet->getCellByColumnAndRow(18, $row)->getValue(),
                            'REGION' => $worksheet->getCellByColumnAndRow(19, $row)->getValue(),
                            'PROVINCE' => $worksheet->getCellByColumnAndRow(20, $row)->getValue(),
                            'TOWN' => $worksheet->getCellByColumnAndRow(21, $row)->getValue(),
                            'BARANGAY' => $worksheet->getCellByColumnAndRow(22, $row)->getValue(),
                            'SITE_ADDRESS' => $worksheet->getCellByColumnAndRow(23, $row)->getValue(),
                            'LONGITUDE' => $worksheet->getCellByColumnAndRow(24, $row)->getValue(),
                            'LATITUDE' => $worksheet->getCellByColumnAndRow(25, $row)->getValue(),
                            'STATUS' => $worksheet->getCellByColumnAndRow(26, $row)->getValue(),
                            'Antenna_Type' => $worksheet->getCellByColumnAndRow(27, $row)->getValue(),
                            'ACL' => $worksheet->getCellByColumnAndRow(28, $row)->getValue(),
                            'Azimuth' => $worksheet->getCellByColumnAndRow(29, $row)->getValue(),
                            'Mechanical_Tilt' => $worksheet->getCellByColumnAndRow(30, $row)->getValue(),
                            'Electrical_Tilt' => $worksheet->getCellByColumnAndRow(31, $row)->getValue(),
                            'DUAL_BEAM_TAGGING' => $worksheet->getCellByColumnAndRow(32, $row)->getValue(),
                            'SECTOR_NAME' => $worksheet->getCellByColumnAndRow(33, $row)->getValue(),
                
                            'date' => date('Y-m-d H:i:s'),
                        );
                    }
                }

                #TRUNCATE TABLE BEFORE INSERT
                $this->db->where('REGIONAL_AREA', $region);
                $this->db->delete($this->tbl4g);
                #CLEAR DATA BEFORE INSERT
                $this->db->insert_batch($this->tbl4g, array_filter($data));
                if ($this->db->affected_rows() == 0) {
                    $json = array(
                        'title' => 'Oops!',
                        'msg'   => 'Upload failed.',
                        'type'  => 'error',
                    );
                } else {
                    #GET RANK WORST CELLS PER CELL NAMES OR SITE
                    $json = array(
                        'title' => 'Success',
                        'msg'   => 'Successfully uploaded.',
                        'type'  => 'success',
                    );
                }
            } else {
                $json = array(
                    'title' => 'Oops!',
                    'msg'   => 'Import file does not match to desired format.',
                    'type'  => 'error',
                );
            }
        } else {
            $json = array(
                'title' => 'Ooops!',
                'msg'   => 'Something went wrong.',
                'type'  => 'error',
                'date' => ''
            );
        }
			
		

		return $json;
	}

    #5G Upload
    public function p5g() {	
        if (isset($_FILES["plan"]["name"])) {
            $path = $_FILES["plan"]["tmp_name"];
            $object= PHPExcel_IOFactory::load($path);

            #GET FORMAT HEADER OF EXCEL
            $cell_collection = $object->getActiveSheet()->getCellCollection();
            foreach ($cell_collection as $cell) {
                $column = $object->getActiveSheet()->getCell($cell)->getColumn();
                $row = $object->getActiveSheet()->getCell($cell)->getRow();
                $data_value = $object->getActiveSheet()->getCell($cell)->getValue();
                //The header will/should be in row 1 only. of course, this can be modified to suit your need.
                if ($row == 1) {
                    $header[$row][$column] = $data_value;
                }
            }

            #CHECK IF EXCEL IMPORT MATCHES TO DATABASE FORMAT OF EXCEL
            if (34 == count($header[1])) {
                foreach ($object->getWorksheetIterator() as $worksheet) {
                    $highestRow = $worksheet->getHighestRow();
                    for ($row = 2; $row <= $highestRow; $row++) {
                        $data[] = array(
                            'PLAID' => $worksheet->getCellByColumnAndRow(0, $row)->getValue(),
                            'SITE_NAME' => $worksheet->getCellByColumnAndRow(1, $row)->getValue(),
                            'CELL_NAME' => $worksheet->getCellByColumnAndRow(2, $row)->getValue(),
                            'NR_Name' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
                            'gNodeB_ID' => $worksheet->getCellByColumnAndRow(4, $row)->getValue(),
                            'CGI' => $worksheet->getCellByColumnAndRow(5, $row)->getValue(),
                            'Frequency_Band' => $worksheet->getCellByColumnAndRow(6, $row)->getValue(),
                            'Duplex_Mode' => $worksheet->getCellByColumnAndRow(7, $row)->getValue(),
                            'Cell_ID' => $worksheet->getCellByColumnAndRow(8, $row)->getValue(),
                            "Cell_Activate_State" => $worksheet->getCellByColumnAndRow(9, $row)->getValue(),
                            'Cell_Administration_State' => $worksheet->getCellByColumnAndRow(10, $row)->getValue(),
                            
                            'CELL_COVERAGE_TYPE' => $worksheet->getCellByColumnAndRow(11, $row)->getValue(),
                            'VENDOR' => $worksheet->getCellByColumnAndRow(12, $row)->getValue(),
                            'GEOGRAPHICAL_AREA' => $worksheet->getCellByColumnAndRow(13, $row)->getValue(),
                            'REGIONAL_AREA' => $worksheet->getCellByColumnAndRow(14, $row)->getValue(),
                            'REGION' => $worksheet->getCellByColumnAndRow(15, $row)->getValue(),
                            'PROVINCE' => $worksheet->getCellByColumnAndRow(16, $row)->getValue(),
                            'TOWN' => $worksheet->getCellByColumnAndRow(17, $row)->getValue(),
                            'BARANGAY' => $worksheet->getCellByColumnAndRow(18, $row)->getValue(),
                            'SITE_ADDRESS' => $worksheet->getCellByColumnAndRow(19, $row)->getValue(),
                            'LONGITUDE' => $worksheet->getCellByColumnAndRow(20, $row)->getValue(),
                           
                            'LATITUDE' => $worksheet->getCellByColumnAndRow(21, $row)->getValue(),
                            'STATUS' => $worksheet->getCellByColumnAndRow(22, $row)->getValue(),
                            'DLEARFCN' => $worksheet->getCellByColumnAndRow(23, $row)->getValue(),
                            'ULEARFCN' => $worksheet->getCellByColumnAndRow(24, $row)->getValue(),
                            'BandWidth' => $worksheet->getCellByColumnAndRow(25, $row)->getValue(),
                            'Antenna_Type' => $worksheet->getCellByColumnAndRow(26, $row)->getValue(),
                            'ACL' => $worksheet->getCellByColumnAndRow(27, $row)->getValue(),
                            'Azimuth' => $worksheet->getCellByColumnAndRow(28, $row)->getValue(),
                            'Mechanical_Tilt' => $worksheet->getCellByColumnAndRow(29, $row)->getValue(),
                            'Electrical_Tilt' => $worksheet->getCellByColumnAndRow(30, $row)->getValue(),

                            'AT_HOME_MOBILE' => $worksheet->getCellByColumnAndRow(31, $row)->getValue(),
                            'Site_Solution' => $worksheet->getCellByColumnAndRow(32, $row)->getValue(),
                            'Sector_Name' => $worksheet->getCellByColumnAndRow(33, $row)->getValue(),

                            'date' => date('Y-m-d H:i:s'),
                        );
                    }
                }

                #TRUNCATE TABLE BEFORE INSERT
                $this->db->truncate($this->tbl5g);
                #CLEAR DATA BEFORE INSERT
                $this->db->insert_batch($this->tbl5g, array_filter($data));
                if ($this->db->affected_rows() == 0) {
                    $json = array(
                        'title' => 'Oops!',
                        'msg'   => 'Upload failed.',
                        'type'  => 'error',
                    );
                } else {
                    #GET RANK WORST CELLS PER CELL NAMES OR SITE
                    $json = array(
                        'title' => 'Success',
                        'msg'   => 'Successfully uploaded.',
                        'type'  => 'success',
                    );
                }
            } else {
                $json = array(
                    'title' => 'Oops!',
                    'msg'   => 'Import file does not match to desired format.',
                    'type'  => 'error',
                );
            }
        } else {
            $json = array(
                'title' => 'Ooops!',
                'msg'   => 'Something went wrong.',
                'type'  => 'error',
                'date' => ''
            );
        }
			
		

		return $json;
	}

    public function get_pl2g() {
        $list = $this->get_datatables($this->tbl2g);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $db) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $db->PLAID;
            $row[] = $db->SITE_NAME;
            $row[] = $db->Site_Solution;
            $row[] = $db->BTS_NAME;
            $row[] = $db->CELL_NAME;
            $row[] = $db->FREQ_BAND;
            $row[] = $db->BSC;
            $row[] = $db->LAC;
            $row[] = $db->CELL_IDENTIFIER;
            $row[] = $db->ADMINISTRATIVE_STATE;
            $row[] = $db->CELL_COVERAGE_TYPE;
            $row[] = $db->BUILDING_NAME;
            $row[] = $db->GEOGRAPHICAL_AREA;
            $row[] = $db->REGIONAL_AREA;
            $row[] = $db->REGION;
            $row[] = $db->PROVINCE;
            $row[] = $db->TOWN;
            $row[] = $db->BARANGAY;
            $row[] = $db->SITE_ADDRESS;
            $row[] = $db->LONGITUDE;
            $row[] = $db->LATITUDE;
            $row[] = $db->STATUS;
            $row[] = $db->Antenna_Type;
            $row[] = $db->ACL;
            $row[] = $db->Azimuth;
            $row[] = $db->Mechanical_Tilt;
            $row[] = $db->Electrical_Tilt;
            $row[] = $db->NE_STATE;
            $row[] = $db->VENDOR;
            $row[] = $this->timestamp($db->date);
 
            $data[] = $row;
        }
 
        return array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->count_all($this->tbl2g),
                        "recordsFiltered" => $this->count_filtered($this->tbl2g),
                        "data" => $data,
                );
    }

    public function get_pl3g() {
        $list = $this->get_datatables($this->tbl3g);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $db) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $db->PLAID;
            $row[] = $db->SITE_NAME;
            $row[] = $db->Site_Solution;
            $row[] = $db->NodeB_NAME;
            $row[] = $db->CELL_NAME;;
            $row[] = $db->BAND_INDICATOR;
            $row[] = $db->DL_UARFCN;
            $row[] = $db->RNC;
            $row[] = $db->DL_PRIMARY_SCRAMBLING_CODE;
            $row[] = $db->LAC;
            $row[] = $db->CELL_IDENTIFIER;
            $row[] = $db->VALIDATION_INDICATION;
            $row[] = $db->CELL_ADMINISTRATIVE_STATE;
            $row[] = $db->CELL_COVERAGE_TYPE;
            $row[] = $db->BUILDING_NAME;
            $row[] = $db->GEOGRAPHICAL_AREA;
            $row[] = $db->REGIONAL_AREA;
            $row[] = $db->REGION;
            $row[] = $db->PROVINCE;
            $row[] = $db->TOWN;
            $row[] = $db->BARANGAY;
            $row[] = $db->SITE_ADDRESS;
            $row[] = $db->LONGITUDE;
            $row[] = $db->LATITUDE;
            $row[] = $db->STATUS;
            $row[] = $db->Antenna_Type;
            $row[] = $db->ACL;
            $row[] = $db->Azimuth;
            $row[] = $db->Mechanical_Tilt;
            $row[] = $db->Electrical_Tilt;
            $row[] = $db->NE_STATE;
            $row[] = $db->VENDOR;
            $row[] = $db->DUAL_BEAM_TAGGING;
            $row[] = $db->SECTOR_NAME;
            $row[] = $this->timestamp($db->date);
 
            $data[] = $row;
        }
 
        return array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->count_all($this->tbl3g),
                        "recordsFiltered" => $this->count_filtered($this->tbl3g),
                        "data" => $data,
                );
    }

    public function get_pl4g() {
        $list = $this->get_datatables($this->tbl4g);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $db) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $db->PLAID;
            $row[] = $db->SITE_NAME;
            $row[] = $db->Site_Solution;
            $row[] = $db->eNodeBName;
            $row[] = $db->CELL_NAME;
            $row[] = $db->LTE_BAND;
            $row[] = $db->AT_HOME_MOBILE;
            $row[] = $db->eNodeBID;
            $row[] = $db->CGI;

            $row[] = $db->Sector_ID;
            $row[] = $db->Local_Cell_ID;
            $row[] = $db->Cell_ID;
            $row[] = $db->PCI;
            $row[] = $db->TAC;
            $row[] = $db->CELL_ADMINISTRATIVE_STATE;
            $row[] = $db->CELL_COVERAGE_TYPE;
            $row[] = $db->VENDOR;
            $row[] = $db->GEOGRAPHICAL_AREA;
            $row[] = $db->REGIONAL_AREA;

            $row[] = $db->REGION;
            $row[] = $db->PROVINCE;
            $row[] = $db->TOWN;
            $row[] = $db->BARANGAY;
            $row[] = $db->SITE_ADDRESS;
            $row[] = $db->LONGITUDE;
            $row[] = $db->LATITUDE;
            $row[] = $db->STATUS;
            $row[] = $db->Antenna_Type;
            $row[] = $db->ACL;
            
            $row[] = $db->Azimuth;
            $row[] = $db->Mechanical_Tilt;
            $row[] = $db->Electrical_Tilt;    
            $row[] = $db->DUAL_BEAM_TAGGING;
            $row[] = $db->SECTOR_NAME;
            $row[] = $this->timestamp($db->date);
 
            $data[] = $row;
        }
 
        return array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->count_all($this->tbl4g),
                        "recordsFiltered" => $this->count_filtered($this->tbl4g),
                        "data" => $data,
                );
    }

    public function get_pl5g() {
        $list = $this->get_datatables($this->tbl5g);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $db) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $db->PLAID;
            $row[] = $db->SITE_NAME;
            $row[] = $db->CELL_NAME;
            $row[] = $db->NR_Name;
            $row[] = $db->gNodeB_ID;
            $row[] = $db->CGI;
            $row[] = $db->Frequency_Band;
            $row[] = $db->Duplex_Mode;
            $row[] = $db->Cell_ID;
            $row[] = $db->Cell_Activate_State;
            $row[] = $db->Cell_Administration_State;
            $row[] = $db->CELL_COVERAGE_TYPE;
            $row[] = $db->VENDOR;
            $row[] = $db->GEOGRAPHICAL_AREA;
            $row[] = $db->REGIONAL_AREA;
            $row[] = $db->REGION;
            $row[] = $db->PROVINCE;
            $row[] = $db->TOWN;
            $row[] = $db->BARANGAY;
            $row[] = $db->SITE_ADDRESS;
            $row[] = $db->LONGITUDE;
            $row[] = $db->LATITUDE;
            $row[] = $db->STATUS;
            $row[] = $db->DLEARFCN;
            $row[] = $db->ULEARFCN;
            $row[] = $db->BandWidth;
            $row[] = $db->Antenna_Type;
            $row[] = $db->ACL;
            $row[] = $db->Azimuth;
            $row[] = $db->Mechanical_Tilt;
            $row[] = $db->Electrical_Tilt;    
            $row[] = $db->AT_HOME_MOBILE;    
            $row[] = $db->Site_Solution;
            $row[] = $db->Sector_Name;
            $row[] = $this->timestamp($db->date);
 
            $data[] = $row;
        }
 
        return array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->count_all($this->tbl5g),
                        "recordsFiltered" => $this->count_filtered($this->tbl5g),
                        "data" => $data,
                );
    }

    function get_datatables($db)
    {
        $this->_get_datatables_query($db);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered($db)
    {
        $this->_get_datatables_query($db);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($db)
    {
        $this->db->from($db);
        return $this->db->count_all_results();
    }

    private function _get_datatables_query($db)
    {
         
        $this->db->from($db);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $query = $this->db->get($db);
            $field_array = $query->list_fields();

           
            $this->db->order_by($field_array[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_field_keys() {
        #2G
        $g2 = $this->db->get($this->tbl2g);
        $fk_2g = $g2->list_fields();

        #3G
        $g3 = $this->db->get($this->tbl3g);
        $fk_3g = $g3->list_fields();

        #4G
        $g4 = $this->db->get($this->tbl4g);
        $fk_4g = $g4->list_fields();

        #5G
        $g5 = $this->db->get($this->tbl5g);
        $fk_5g = $g5->list_fields();

        return array(
            'g2' => $fk_2g,
            'g3' => $fk_3g,
            'g4' => $fk_4g,
            'g5' => $fk_5g,
        );
    }

    public function timestamp($date)
	{
		$this->load->helper('date');

		// Declare timestamps
		$last = new DateTime($date);
		$now = new DateTime(date('d-m-Y H:i:s', time()));

		// Find difference
		$interval = $last->diff($now);
		$years = (int)$interval->format('%Y');
		$months = (int)$interval->format('%m');
		$days = (int)$interval->format('%d');
		$hours = (int)$interval->format('%H');
		$minutes = (int)$interval->format('%i');

		if ($years > 0) {
			return $years . ' Yr' .($years > 1 ? 's ' : ' ') . $months . ' Month' .($months > 1 ? 's ' : ' '). $days . ' Day'.($days>1 ? 's ' : ' ') . $hours . ' Hr'.($hours > 1 ? 's ' : ' ') . $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		} else if ($months > 0) {
			return $months . ' Month' .($months > 1 ? 's ' : ' ') . $days . ' Day'.($days>1 ? 's ' : ' ') . $hours . ' Hr'.($hours > 1 ? 's ' : ' ') . $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		} else if ($days > 0) {
			return $days . ' Day'.($days>1 ? 's ' : ' ') . $hours . ' hr'.($hours > 1 ? 's ' : ' ') . $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		} else if ($hours > 0) {
			return $hours . ' Hr'.($hours > 1 ? 's ' : ' ') . $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		} else {
			return $minutes == 0 ? ' Now' : $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		}
	}

    public function pl_remove() {
        $tech = $this->input->post('par');
        $tbl = '';

        switch($tech) {
            case '2g':
                $tbl = $this->tbl2g; break;
            case '3g':   
                $tbl = $this->tbl3g; break;
            case '4g':    
                $tbl = $this->tbl4g; break;
            case '5g':    
                $tbl = $this->tbl5g; break;
        }

        $this->db->truncate($tbl);

        return array(
			'type' => 'success',
			'title' => 'Success!',
			'msg' => 'Data deleted.'
		);
    }
}
