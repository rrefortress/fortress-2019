<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wireline_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->geoarea = $this->session->userdata('geoarea');

        $this->tbl = "wireline_mdb";

        $this->column_search = array('PLA_ID','Site_Name','Site_Class'); 
        $this->order = array('Site_Name' => 'asc');
	}


    public function wire_upload() {	
        if (isset($_FILES["wire_file"]["name"])) {
            $path = $_FILES["wire_file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load(strval($path));

            #GET FORMAT HEADER OF EXCEL
            $cell_collection = $object->getActiveSheet()->getCellCollection();
            foreach ($cell_collection as $cell) {
                $column = $object->getActiveSheet()->getCell($cell)->getColumn();
                $row = $object->getActiveSheet()->getCell($cell)->getRow();
                $data_value = $object->getActiveSheet()->getCell($cell)->getValue();
                //The header will/should be in row 1 only. of course, this can be modified to suit your need.
                if ($row == 1) {
                    $header[$row][$column] = trim($data_value);
                }
            }

            #CHECK IF EXCEL IMPORT MATCHES TO DATABASE FORMAT OF EXCEL
            if (49 == count($header[1])) {
              
                foreach ($object->getWorksheetIterator() as $worksheet) {
                    $highestRow = $worksheet->getHighestRow();
                    for ($row = 2; $row <= $highestRow; $row++) {
                        $data[] = array(
                            'Site_Name' => $worksheet->getCellByColumnAndRow(0, $row)->getValue(),
                            'PLA_ID' => $worksheet->getCellByColumnAndRow(1, $row)->getValue(),
                            'Region' => $worksheet->getCellByColumnAndRow(2, $row)->getValue(),
                            'Site_Class' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
                            'Grid_Class' => $worksheet->getCellByColumnAndRow(4, $row)->getValue(),
                            'Site_Type' => $worksheet->getCellByColumnAndRow(5, $row)->getValue(),
                            'Cabin_Type' => $worksheet->getCellByColumnAndRow(6, $row)->getValue(),
                            'PIC' => $worksheet->getCellByColumnAndRow(7, $row)->getValue(),
                            'Power_Connection' => $worksheet->getCellByColumnAndRow(8, $row)->getValue(),
                            "Power_Phase" => $worksheet->getCellByColumnAndRow(9, $row)->getValue(),
                            'Power_Metering' => $worksheet->getCellByColumnAndRow(10, $row)->getValue(),
                            'Transformer_Drop_Wire' => $worksheet->getCellByColumnAndRow(11, $row)->getValue(),
                            'Service_Entrance_Wire' => $worksheet->getCellByColumnAndRow(12, $row)->getValue(),
                            'RS1_Load_Adc' => $worksheet->getCellByColumnAndRow(13, $row)->getValue(),
                            'RS2_Load_Adc' => $worksheet->getCellByColumnAndRow(14, $row)->getValue(),
                            'RS3_Load_Adc' => $worksheet->getCellByColumnAndRow(15, $row)->getValue(),
                            'Existing_Load_W' => $worksheet->getCellByColumnAndRow(16, $row)->getValue(),
                            'BATTERY_TOTAL' => $worksheet->getCellByColumnAndRow(17, $row)->getValue(),
                            'Battery_Charging_Load_W' => $worksheet->getCellByColumnAndRow(18, $row)->getValue(),
                            'YR3_PLAN_MDB' => $worksheet->getCellByColumnAndRow(19, $row)->getValue(),
                            'Proposed_Load_W' => $worksheet->getCellByColumnAndRow(20, $row)->getValue(),
                            'Deductive_Loads' => $worksheet->getCellByColumnAndRow(21, $row)->getValue(),
                            'ACU_AMPS' => $worksheet->getCellByColumnAndRow(22, $row)->getValue(),
                            'No_of_ACU_Units' => $worksheet->getCellByColumnAndRow(23, $row)->getValue(),
                            'ACU_Load_W' => $worksheet->getCellByColumnAndRow(24, $row)->getValue(),
                            'Other_AC_Load_W' => $worksheet->getCellByColumnAndRow(25, $row)->getValue(),
                            'ADDITIONAL_BATT' => $worksheet->getCellByColumnAndRow(26, $row)->getValue(),
                            'Battery_Augmentation_Load' => $worksheet->getCellByColumnAndRow(27, $row)->getValue(),

                            'TRANSPORT_WATTS' => $worksheet->getCellByColumnAndRow(28, $row)->getValue(),
                            'MWAN_WATTS' => $worksheet->getCellByColumnAndRow(29, $row)->getValue(),
                            'Endstate_Load_W_Relative_Demand' => $worksheet->getCellByColumnAndRow(30, $row)->getValue(),
                            'Existing_Transformer_Rating_KVA' => $worksheet->getCellByColumnAndRow(31, $row)->getValue(),
                            'Service_Drop_Wire' => $worksheet->getCellByColumnAndRow(32, $row)->getValue(),
                            'Percent_A' => $worksheet->getCellByColumnAndRow(33, $row)->getValue(),
                            'Existing_Genset_Rating_KVA' => $worksheet->getCellByColumnAndRow(34, $row)->getValue(),
                            'GENSET_WIRE' => $worksheet->getCellByColumnAndRow(35, $row)->getValue(),
                            'Percent_B' => $worksheet->getCellByColumnAndRow(36, $row)->getValue(),
                            'ECBORMDP_Rating_AT' => $worksheet->getCellByColumnAndRow(37, $row)->getValue(),
                            'ECB_Drop_Wire' => $worksheet->getCellByColumnAndRow(38, $row)->getValue(),
                            'Percent_C' => $worksheet->getCellByColumnAndRow(39, $row)->getValue(),
                            'ACPDB_Rating_AT' => $worksheet->getCellByColumnAndRow(40, $row)->getValue(),

                            'Percent_D' => $worksheet->getCellByColumnAndRow(41, $row)->getValue(),
                            'AGILE_RECO' => $worksheet->getCellByColumnAndRow(42, $row)->getValue(),
                            'AGILE_REMARKS' => $worksheet->getCellByColumnAndRow(43, $row)->getValue(),
                            'SCOPE' => $worksheet->getCellByColumnAndRow(44, $row)->getValue(),
                            'AEPM_RECO' => $worksheet->getCellByColumnAndRow(45, $row)->getValue(),
                            'AEPM_REMARKS' => $worksheet->getCellByColumnAndRow(46, $row)->getValue(),
                            'FSCM_RemarksORcomments' => $worksheet->getCellByColumnAndRow(47, $row)->getValue(),
                            'date' => date('Y-m-d H:i:s')
                        );
                    }
                }

                #TRUNCATE TABLE BEFORE INSERT
                $this->db->truncate($this->tbl);
                #CLEAR DATA BEFORE INSERT
                $this->db->insert_batch($this->tbl, array_filter($data));
                if ($this->db->affected_rows() == 0) {
                    $json = array(
                        'title' => 'Oops!',
                        'msg'   => 'Upload failed.',
                        'type'  => 'error',
                    );
                } else {
                    #GET RANK WORST CELLS PER CELL NAMES OR SITE
                    $json = array(
                        'title' => 'Success',
                        'msg'   => 'Successfully uploaded.',
                        'type'  => 'success',
                    );
                }
            } else {
                $json = array(
                    'title' => 'Oops!',
                    'msg'   => 'Import file does not match to desired format.',
                    'type'  => 'error',
                );
            }
        } else {
            $json = array(
                'title' => 'Ooops!',
                'msg'   => 'Something went wrong.',
                'type'  => 'error',
                'date' => ''
            );
        }
			
		

		return $json;
	}


    public function get_wireline() {
        $list = $this->get_datatables($this->tbl);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $db) {
            $no++;
            $row = array();
            $row[] = $no;

            $row[] = $db->Site_Name;
            $row[] = $db->PLA_ID;
            $row[] = $db->Region;
            $row[] = $db->Site_Class;
            $row[] = $db->Grid_Class;
            $row[] = $db->Site_Type;
            $row[] = $db->Cabin_Type;
            $row[] = $db->PIC;
            $row[] = $db->Power_Connection;
            $row[] = $db->Power_Phase;
            $row[] = $db->Power_Metering;
            $row[] = $db->Transformer_Drop_Wire;
            $row[] = $db->Service_Entrance_Wire;
            $row[] = $db->RS1_Load_Adc;
            $row[] = $db->RS2_Load_Adc;
            $row[] = $db->RS3_Load_Adc;
            $row[] = $db->TOTAL_CURRENT;
            $row[] = $db->Existing_Load_W;
            $row[] = $db->BATTERY_TOTAL;
            $row[] = $db->Battery_Charging_Load_W;
            $row[] = $db->YR3_PLAN_MDB;
            $row[] = $db->Proposed_Load_W;
            $row[] = $db->Deductive_Loads;
            $row[] = $db->ACU_AMPS;
            $row[] = $db->No_of_ACU_Units;
            $row[] = $db->ACU_Load_W;
            $row[] = $db->Other_AC_Load_W;
            $row[] = $db->ADDITIONAL_BATT;
            $row[] = $db->Battery_Augmentation_Load;

            $row[] = $db->TRANSPORT_WATTS;
            $row[] = $db->MWAN_WATTS;
            $row[] = $db->Endstate_Load_W_Relative_Demand;
            $row[] = $db->Existing_Transformer_Rating_KVA;
            $row[] = $db->Service_Drop_Wire;
            $row[] = $db->Percent_A;
            $row[] = $db->Existing_Genset_Rating_KVA;
            $row[] = $db->GENSET_WIRE;
            $row[] = $db->Percent_B;
            $row[] = $db->ECBORMDP_Rating_AT;
            $row[] = $db->ECB_Drop_Wire;
            $row[] = $db->Percent_C;
            $row[] = $db->ACPDB_Rating_AT;
            $row[] = $db->Percent_D;

            $row[] = $db->AGILE_RECO;
            $row[] = $db->AGILE_REMARKS;
            $row[] = $db->SCOPE;
            $row[] = $db->AEPM_RECO;
            $row[] = $db->AEPM_REMARKS;
            $row[] = $db->FSCM_RemarksORcomments;

            $row[] = $db->date;
            
            $data[] = $row;
        }
 
        return array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->count_all($this->tbl),
                        "recordsFiltered" => $this->count_filtered($this->tbl),
                        "data" => $data,
                );
    }

    function get_datatables($db)
    {
        $this->_get_datatables_query($db);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered($db)
    {
        $this->_get_datatables_query($db);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($db)
    {
        $this->db->from($db);
        return $this->db->count_all_results();
    }

    private function _get_datatables_query($db)
    {
         
        $this->db->from($db);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $query = $this->db->get($db);
            $field_array = $query->list_fields();

           
            $this->db->order_by($field_array[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    public function timestamp($date)
	{
		$this->load->helper('date');

		// Declare timestamps
		$last = new DateTime($date);
		$now = new DateTime(date('d-m-Y H:i:s', time()));

		// Find difference
		$interval = $last->diff($now);
		$years = (int)$interval->format('%Y');
		$months = (int)$interval->format('%m');
		$days = (int)$interval->format('%d');
		$hours = (int)$interval->format('%H');
		$minutes = (int)$interval->format('%i');

		if ($years > 0) {
			return $years . ' Yr' .($years > 1 ? 's ' : ' ') . $months . ' Month' .($months > 1 ? 's ' : ' '). $days . ' Day'.($days>1 ? 's ' : ' ') . $hours . ' Hr'.($hours > 1 ? 's ' : ' ') . $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		} else if ($months > 0) {
			return $months . ' Month' .($months > 1 ? 's ' : ' ') . $days . ' Day'.($days>1 ? 's ' : ' ') . $hours . ' Hr'.($hours > 1 ? 's ' : ' ') . $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		} else if ($days > 0) {
			return $days . ' Day'.($days>1 ? 's ' : ' ') . $hours . ' hr'.($hours > 1 ? 's ' : ' ') . $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		} else if ($hours > 0) {
			return $hours . ' Hr'.($hours > 1 ? 's ' : ' ') . $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		} else {
			return $minutes == 0 ? ' Now' : $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		}
	}

    public function get_wire_fkeys() {
        $data = $this->db->get($this->tbl);
        return $data->list_fields();
    }

    public function wire_remove() {
        $this->db->truncate($this->tbl);

        return array(
			'type' => 'success',
			'title' => 'Success!',
			'msg' => 'Data deleted.'
		);
    }
}
