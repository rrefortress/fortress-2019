<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CLP_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->g9 = 3;
		$this->g18 = 5;
		$this->u9 = 2;
		$this->u21 = 2;
		$this->l18 = 1;
		$this->l21 = 1;
		$this->l23 = 2;
		$this->l26 = 2;
		$this->l26MM = 2;
		$this->l7 = 1;
		$this->l9 = 1;
		$this->nr26 = 1;
		$this->nr35 = 1;
	}

	public function generate_CID($site_name = '')
	{

		$site_name = $this->input->post('site_name');
		$sector = $this->input->post('sector') > 3 ? 2 : 1;

		#GENERATE 2G CID
		$data = $this->db->query('SELECT id, CELL_ID as cid FROM `2g_cid` WHERE SR_NAME = ? LIMIT 2', array($site_name));
		#CHECK IF SITE EXISTS IN 2G
		if ($data->num_rows() > 0) {
			$result_cid = $data->result();
		} else {
			#UPDATE
			$res = $this->db->query('SELECT CELL_ID as cid FROM `2g_cid` WHERE AREA = ? AND PROVINCE = ? LIMIT ?',
				array('', '', $sector));

			$result_cid = $res->result();

			foreach ($result_cid as $row) {
				$this->db->where('CELL_ID', $row->cid);
				$this->db->update('2g_cid', array('SR_NAME' => $site_name));
			}
		}

		#GENERATE 3G CID
		$limit = $this->input->post('sector') > 3 ? 4 : 2;
		$data = $this->db->query("SELECT id, CELL_ID as cid FROM `3g_cid` WHERE SR_NAME = ? LIMIT ?", array($site_name, $limit));
		#CHECK IF SITE EXISTS IN 3G
		if ($data->num_rows() > 0) {
			$result_nid = $data->result();
		} else {
			$res = $this->db->query("SELECT CELL_ID as cid FROM `3g_cid` WHERE AREA = ? AND PROVINCE = ? LIMIT ?",
				array('', '', $limit));

			$result_nid = $res->result();

			foreach ($result_nid as $row) {
				$this->db->where('CELL_ID', $row->cid);
				$this->db->update('3g_cid', array('SR_NAME' => $site_name));
			}
		}

		$arr = array(
			'CID' => $result_cid,
			'NID' => $result_nid
		);

		return $arr;
	}

	public function clp_upload() {
		$arrayHeader = array("A" => 'SEARCHRING NAME',
							"B" => "PLA ID",
							"C" => "No. of Sectors",
							"D" => "Planned Tech"
							);

		if (isset($_FILES["clp_file"]["name"])) {

			$path = $_FILES["clp_file"]["tmp_name"];
			$object= PHPExcel_IOFactory::load($path);

			#GET FORMAT HEADER OF EXCEL
			$cell_collection = $object->getActiveSheet()->getCellCollection();
			foreach ($cell_collection as $cell) {
				$column = $object->getActiveSheet()->getCell($cell)->getColumn();
				$row = $object->getActiveSheet()->getCell($cell)->getRow();
				$data_value = $object->getActiveSheet()->getCell($cell)->getValue();
				//The header will/should be in row 1 only. of course, this can be modified to suit your need.
				if ($row == 1) {
					$header[$row][$column] = $data_value;
				}
			}

			#CHECK IF EXCEL IMPORT MATCHES TO DATABASE FORMAT OF EXCEL
			if ($arrayHeader == $header[1]) {
				foreach ($object->getWorksheetIterator() as $worksheet) {
					$highestRow = $worksheet->getHighestRow();
					for ($row = 2; $row <= $highestRow; $row++) {
						$sitename = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
						$plaid = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
						$sectors = intval($worksheet->getCellByColumnAndRow(2, $row)->getValue());
						$tech =  $worksheet->getCellByColumnAndRow(3, $row)->getValue();

						$g9 = '';
						$g18 = '';
						$u9 = '';
						$u21 = '';
						$l18 = '';
						$l21 = '';
						$l7 = '';
						$l23 = '';
						$l26 = '';
						$l26mm = '';
						$l9 = '';
						$nr26 = '';
						$nr35 = '';

						$keys = explode(' | ', $tech);
						foreach ($keys as $keyword)
						{
							$keyword = trim($keyword);
							#G9
							if ($keyword == 'G9') {$g9 = $this->set_clp($this->g9, $sectors);}
							#G18
							if ($keyword == 'G18') {$g18 = $this->set_clp($this->g18, $sectors);}
							#U9
							if ($keyword == 'U9') {$u9 = $this->set_clp($this->u9, $sectors);}
							#U21
							if ($keyword == 'U21') {$u21 = $this->set_clp($this->u21, $sectors);}
							#L18
							if ($keyword == 'L18') {$l18 = $this->set_clp($this->l18, $sectors);}
							#L21
							if ($keyword == 'L21') {$l21 = $this->set_clp($this->l21, $sectors);}
							#L7
							if ($keyword == 'L21') {$l7 = $this->set_clp($this->l7, $sectors);}
							#L23
							if ($keyword == 'L23') {$l23 = $this->set_clp($this->l23, $sectors);}
							#L26
							if ($keyword == 'L26') {$l26 = $this->set_clp($this->l26, $sectors);}
							#L26MM
							if ($keyword == 'L26') {$l26mm = $this->set_clp($this->l26MM, $sectors);}
							#L9
							if ($keyword == 'L9') {$l9 = $this->set_clp($this->l9, $sectors);}
							#LNR26MM
							if ($keyword == 'LNR26MM') {$nr26 = $this->set_clp($this->nr26, $sectors);}
							#NR35MM
							if ($keyword == 'NR35MM') {$nr35 = $this->set_clp($this->nr35, $sectors);}
						}

						$json[] = array(
							'SEARCHRING NAME' => $sitename,
							'PLAID' => $plaid,
							'No. of Sectors' => $sectors,
							'Planned Tech' => $tech,
							'GSM900' => $g9,
							'GSM1800' => $g18,
							'UMTS900' => $u9,
							'UMTS2100' => $u21,
							'LTE700' => $l7,
							'LTE900' => $l9,
							'LTE1800' => $l18,
							'LTE2100' => $l21,
							'LTE2300' => $l23,
							'LTE2600' => $l26,
							'LTE2600MM' => $l26mm,
							'NR2600' => $nr26,
							'NR3500 (5G)' => $nr35
						);
					}
				}
			} else {
				$json = array(
					'title' => 'Ooops!',
					'msg'   => 'Format excel import does not match to desired Format.',
					'type'  => 'error'
				);
			}
		} else {
			$json = array(
				'title' => 'Ooops!',
				'msg'   => 'Something went wrong.',
				'type'  => 'error'
			);
		}
		return $json;
	}

	public function set_clp($val = 0, $sector = 0) {
		$ctr = 1;
		$value = '';
		$sec = $sector;
		for ($c = 0; $sec > $c; $sec--) {
			$value .= $val;
			if ($sec > 1) {
				for ($c1 = 0; $c1 < 1; $c1++) {
					$value .= '+';
				}
			}
			$ctr++;
		}

		return $value;
	}
}
