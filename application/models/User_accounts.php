<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_accounts extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('encrypt');
	}
	//Add user to database
	public function add_user($account_data, $id = "")
	{
		$_table_name = 'users';
		$data = array(
			"username"		=> $account_data['username'],
            "globe_id"      => $account_data['globe_id'],
			"email"     	=> $account_data['email'],
            "password"		=> $this->encrypt->encode($account_data['password']),
            "fname"			=> ucfirst($account_data['fname']),
            "lname"			=> ucfirst($account_data['lname']),
            "department"	=> $account_data['access_level'] == 'Sub-Admin' ? $account_data['access_level'] : $account_data['department'],
            "geoarea"		=> strtolower($account_data['geoarea']),
            "access_level"	=> $account_data['access_level'],
            "status"   		=> empty($id) ? 'activated' : $account_data['status'],
			"vendor"		=> $account_data['vendor'],
		);

		if (empty($id)) {
			#CHECK IF USER EXISTS
			$query = $this->db->get_where($_table_name, array('globe_id' => $account_data['globe_id']));
			if($query->num_rows() == 0){
				#INSERT USER
				$this->db->insert($_table_name, $data);
				$msg = 'Successfully inserted data.';
				$type = 'success';
				$title = 'Success!';
			} else {
				$msg = 'User already exists.';
				$type = 'error';
				$title = 'Invalid!';
			}
		} else {
			#UPDATE USER
			$this->db->where('id', $id);
			$this->db->update($_table_name, $data);
			$msg = 'Successfully updated data.';
			$type = 'success';
			$title = 'Success!';
		}

		$json = array(
			'type'  => $type,
			'title' => $title,
			'msg'   => $msg,
		);

		return $json;
	}
	//Check User
	public function check_user($email)
	{
		$this->db->where('email', $email);
		$query = $this->db->get('users');
		return $query;
	}

	//Get user from database
	public function get_users()
	{
		$geoarea = $this->session->userdata('geoarea');
		$this->db->order_by("fname", "asc");
		$this->db->where('access_level !=', 'Admin');
		return $this->db->get('users')->result();
	}
	//Delete user from database
	public function delete_user()
	{
		$id	= trim($this->input->post("id"));
		$this->db->where('id', $id);
		$this->db->delete('users');

		return $json = array(
			'title' => 'Success',
			'msg' => 'User removed.',
			'type' => 'success'
		);
	}

	public function reset_password()
	{
		$this->load->helper('string');
		$id	= trim($this->input->post("id"));
		$result = $this->db->get_where('users', array('id' => $id))->num_rows();

		if ($result > 0) {
			$random = random_string('alnum', 6);
			$data = array("password" => $this->encrypt->encode($random));
			$this->db->where('id', $id);
			$this->db->update('users', $data);

//			$this->send_mail();

			return $json = array(
				'title' => $random,
				'msg' => 'Password successfully reset, please copy the text above.',
				'type' => 'success'
			);
		} else {
			return $json = array(
				'title' => 'Invalid',
				'msg'   => 'User does not match.',
				'type'  => 'error'
			);
		}
	}

	//Fetch user from database
	public function fetch_user($data)
	{
		$res = '';
		$username = $data['username'];
		 $this->db->select('*')
			->from('users')
			->where('(users.globe_id = "'.$username.'" OR users.username = "'.$username.'" OR users.email = "'.$username.'")');
		$result = $this->db->get()->row();
		if ($result) {
			if ($this->encrypt->decode($result->password) == $data['password']) {
				$res = $result;
			}
		}

		return $res;
	}

	public function set_active($data, $id)
	{
		$geoarea = $this->session->userdata('geoarea');
		$this->db->where('id', $id);
		$this->db->where('geoarea', $geoarea);
		$this->db->where('globe_id !=', 0);
		$this->db->update('users', array('active' => $data));
	}

	//edit user to database
	public function edit_user($account_data, $id)
	{
		$geoarea = $this->session->userdata('geoarea');
		$this->db->where('id', $id);
		$this->db->where('geoarea', $geoarea);
		$this->db->update('users', $account_data);
	}

	public function getUsers() {
		$geoarea = $this->session->userdata('geoarea');
		return $this->db->query("SELECT
							(SELECT COUNT(*) from users WHERE geoarea = ?) as users,
							(SELECT count(status) FROM users WHERE status = ? AND geoarea = ?) as activated,
							(SELECT COUNT(status) FROM users WHERE status = ? AND geoarea = ?) as deactivated,
							(SELECT COUNT(status) FROM users WHERE active = ? AND geoarea = ?) as active
							 FROM users LIMIT 1", array($geoarea, 'activated',$geoarea,'deactivated',$geoarea,1,$geoarea))->row();
	}

	public function get_active() {
		$geoarea = $this->session->userdata('geoarea');
		$dep =  $this->session->userdata('department');
		$this->db->where('department', $dep);
		$this->db->where('geoarea', $geoarea);
		$this->db->where('active', 1);
		$result = $this->db->get('users')->num_rows();
		return $result;
	}
}

/* End of file User_accounts.php */
/* Location: ./application/models/User_accounts.php */
