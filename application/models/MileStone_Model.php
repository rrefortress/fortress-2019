<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MileStone_Model extends CI_Model
{

  public function set_config($total = 0, $func_name = '', $row_page = 0) {
    $config['num_links'] = 3;
    $config['base_url'] = base_url($func_name);
    $config['use_page_numbers'] = TRUE;
    $config['total_rows'] = $total;
    $config['per_page'] = $row_page;

    $config['full_tag_open']    = '<center><div class="padding"><nav><ul class="pagination text-center ms_track" style="justify-content: center;">';
    $config['full_tag_close']   = '</ul></nav></div>';
    $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
    $config['num_tag_close']    = '</span></li>';
    $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
    $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
    $config['next_tag_open']    = '<li class="page-item"><span class="page-link ">';
    $config['next_tag_close']   = '<span aria-hidden="true"></span></span></li>';
    $config['prev_tag_open']    = '<li class="page-item"><span class="page-link ">';
    $config['prev_tag_close']   = '</span></li>';
    $config['first_tag_open']   = '<li class="page-item"><span class="page-link ">';
    $config['first_tag_close']  = '</span></li>';
    $config['last_tag_open']    = '<li class="page-item"><span class="page-link ">';
    $config['last_tag_close']   = '</span></li></center>';

    // Initialize
    $this->pagination->initialize($config);

    return $config;
  }

  public function get_site_names($search = '', $row_num = 0) {
    $row_page = $this->input->get('row_page');
    $geoarea = $this->session->userdata('geoarea');
    $table_name = 'pmo_'.$geoarea;
    // $search = empty($search) ? '' : " WHERE SITE_NAME LIKE '%".$search."%'";

    $data = $this->db->query("SELECT * FROM ".$table_name);

    if($row_num != 0){
      $row_num = ($row_num-1) * $row_page;
    }

    $this->set_config($data->num_rows(), $func_name = 'get_site_names', $row_page);
    $array = array(
      'data'        => $this->get_details($table_name, $search, $row_num, $row_page),
      'total'			  => $data->num_rows(),
			'pagination'  => $this->pagination->create_links(),
    );

    return $array;
    // return $data->result();
  }

  public function search_site_track($name) {
    $geoarea = $this->session->userdata('geoarea');
    // $awe_tbl = 'awe_'.$geoarea;
    $pmo_tbl = 'pmo_'.$geoarea;
    #CHECK IF AWE TRACKER EXIST
    $pmo = $this->db->get_where($pmo_tbl, array('SITE_NAME' => $name))->num_rows();

    #CHECK IF AWE TRACKER EXIST IN PMO TRACKER
    // $pmo = 0;
    // $pmo_tbl = 'pmo_'.$geoarea;
    // if ($awe > 0) {
    //   $pmo = $this->db->query("SELECT a.* FROM ".$pmo_tbl." a JOIN ".$awe_tbl." b ON a.Serial_Number = b.SERIAL_NUMBER WHERE b.SITE_NAME = ?",
    //   array($name));
    //   $pmo = $pmo->num_rows();
    // }

    #CHECK IF PMO TRACKER EXISTS IN SAQ TRACKER
    $saq = 0;
    $saq_tbl = 'saq_'.$geoarea;
    if ($pmo > 0) {
      $saq = $this->db->query("SELECT a.* FROM ".$saq_tbl." a JOIN ".$pmo_tbl." b ON a.SEARCHRING_NAME = b.SITE_NAME WHERE b.SITE_NAME = ?",
      array($name));
      $saq = $saq->num_rows();
    }

    $array = [
        // '1' => $awe > 0 ? true : false, #AWE
        // '2' => $pmo > 0 ? true : false, #PMO
        '1' => $saq > 0 ? true : false, #SAQ
        '2' => false, #SR
        '3' => false, #JTS
        '4' => false, #RTB
        '5' => false, #CLP
        '6' => false, #DDD
        '7' => false, #Builds
        '8' => false, #TRFS
    ];

    return $array;
  }

  public function get_details($table_name, $search = '', $row_num = 0, $row_page = 0) {
    $this->db->select('*');
    $this->db->from($table_name);
    $this->db->like("SITE_NAME", $search);
    $this->db->order_by('id', 'ASC');
    $this->db->limit($row_page, $row_num);
    $query = $this->db->get();
    return $query->result();
  }
}
