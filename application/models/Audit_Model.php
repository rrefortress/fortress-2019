<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Audit_Model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		ini_set( 'memory_limit', '1G' );
		ini_set('upload_max_filesize', '1G');
		ini_set('post_max_size', '1G');
		ini_set('max_input_time', 600000);
		ini_set('max_execution_time', 600000);
		$this->geoarea = $this->session->userdata('geoarea');
	}

	public function set_config($total = 0, $func_name = '', $row_page = 0)
	{
		$config['base_url'] = base_url($func_name);
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $total;
		$config['per_page'] = $row_page;

		$config['full_tag_open'] = '<ul style="height: 0;" class="mg-top-5 pagination small text-center paginate_track">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['next_tag_close'] = '<span aria-hidden="true"></span></span></li>';
		$config['prev_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['prev_tag_close'] = '</span></li>';
		$config['first_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['first_tag_close'] = '</span></li>';
		$config['last_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['last_tag_close'] = '</span></li>';

		// Initialize
		$this->pagination->initialize($config);
		return $config;
	}

	// public function get_sites($row_num = 0)
	// {
	// 	$row_page = $this->input->post('row_page');
	// 	$geoarea = strtoupper($geo = $this->input->post('geo'));
	// 	$this->db->where('REGION', $geoarea);
	// 	$res = $this->db->get('repo_names');

	// 	if ($row_num != 0) {
	// 		$row_num = ($row_num - 1) * $row_page;
	// 	}

	// 	$this->set_config($res->num_rows(), $func_name = 'get_sites', $row_page);

	// 	$query = $this->get_details($row_num, $row_page);

	// 	return $arrays = array(
	// 		'result' => $query,
	// 		'pagination' => $this->pagination->create_links(),
	// 		'row_num' => $row_num,
	// 		'total' => $res->num_rows()
	// 	);
	// }

	public function get_details($row_num = 0, $row_page = 0)
	{
		// $geo = $this->input->post('geo');
		// $search = $this->input->post('search');
		// $sort = $this->input->post('sort');
		// $this->db->select('a.PLAID, a.SITENAME, COUNT(b.id) as files');
		// $this->db->from('repo_names a');
		// $this->db->limit($row_page, $row_num);
		// $this->db->where('a.REGION', $geo);
		// $this->db->where("(a.PLAID LIKE '%$search%'");
		// $this->db->or_where("a.SITENAME LIKE '%$search%')");
		// $this->db->order_by("a.SITENAME", $sort);
		// $this->db->group_by('a.SITENAME');
		// $this->db->join('repo_files b', 'b.SITENAME = a.SITENAME', 'left');
		// $res = $this->db->get();
		// return $res->result();
	}

	// public function getrecordCount() {
	// 	$query = $this->db->get('audit');
	// 	$result = $query->num_rows();
	// 	return $result;
	// }

	public function get_audit() {
		$this->db->order_by('id', 'DESC');
		$this->db->where('region', $this->geoarea);
		$res = $this->db->get('audit');

		$json = array();
		
		if ($res->num_rows() > 0) {
			foreach ($res->result() as $row) {
				$json[] = array(
					'id' => $row->id,
					'region' => strtoupper($this->geoarea),
					'plaid' => $row->plaid,
					'sitename' => $row->sitename,
					'height' => $row->height,
					'tech' => $row->tech,
					'tech_dir' => $row->tech_dir,
					'sectors' => $row->sectors,
					'azimuth' => $row->azimuth,
					'mtilt' => $row->mtilt,
					'etilt' => $row->etilt,
					'antenna' => $row->antenna,
					'dualbeam' => $row->dualbeam,
					'date_updated' => $row->date_updated
				);	
			}
		}
		return $json;
	}

	public function save_audit() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$region = trim(strtoupper($this->input->post('region')));
            $plaid = trim(strtoupper($this->input->post('plaid')));
            $sname = trim(strtoupper($this->input->post('sname')));
			$height = trim($this->input->post('height'));
			$vendor = trim($this->input->post('vendor'));

            $tech = $this->input->post('tech');

			#BUILD VALUE FOR DIRECTORY with no spaces
			$tech_dir = '';
			#BUILD VALUE FOR MDB with slash separated
			$tech_mdb = '';
			$count = 1;
			foreach( $tech as $row ) {
				$slash = $count < count($tech) ? ' / ' : '';
				$tech_dir .= $row;
				$tech_mdb .= $row.$slash;
				$count++;
			}


            $sectors = trim($this->input->post('sectors'));
            $db = trim($this->input->post('db'));
            
            #SECTOR 1
            $s1 = trim($this->input->post('sec-1'));
            $mt1 = trim($this->input->post('mt-1'));
			$et1 = trim($this->input->post('et-1'));
            $am1 = trim($this->input->post('am-1'));

            #SECTOR 2
            $s2 = trim($this->input->post('sec-2'));
            $mt2 = trim($this->input->post('mt-2'));
			$et2 = trim($this->input->post('et-2'));
            $am2 = trim($this->input->post('am-2'));

            #SECTOR 3
            $s3 = trim($this->input->post('sec-3'));
            $mt3 = trim($this->input->post('mt-3'));
			$et3 = trim($this->input->post('et-3'));
            $am3 = trim($this->input->post('am-3'));

            #SECTOR 4
            $s4 = trim($this->input->post('sec-4'));
            $mt4 = trim($this->input->post('mt-4'));
			$et4 = trim($this->input->post('et-4'));
            $am4 = trim($this->input->post('am-4'));

			$sec = $s1; 
			$mt = $mt1;

			#FILE DIRECTORY
			$path = FCPATH.'/assets/audit/'.$region.'/'.$sname.'/'.$tech_dir;

			$img1 = '';
			$img2 = '';
			$img3 = '';
			$img4 = '';

			switch($sectors) {
				case 2:
					$sec = $s1 .' / '.$s2;
					$mt = $mt1 .' / '.$mt2;
					$et = $et1 .' / '.$et2;

					$img1 = 'Sector';
					$img2 = 'Sector1';
					break;
				case 3:
					$sec = $s1 .' / '.$s2 .' / '.$s3;
					$mt = $mt1 .' / '.$mt2.' / '.$mt3;
					$et = $et1 .' / '.$et2.' / '.$et3;

					$img1 = 'Sector';
					$img2 = 'Sector1';
					$img3 = 'Sector2';
					break;	
				case 4:
					$sec = $s1 .' / '.$s2 .' / '.$s3 .' / '.$s4;
					$mt = $mt1 .' / '.$mt2.' / '.$mt3. ' / '.$mt4;
					$et = $et1 .' / '.$et2.' / '.$et3. ' / '.$et4;

					$img1 = 'Sector';
					$img2 = 'Sector1';
					$img3 = 'Sector2';
					$img4 = 'Sector3';
					break;
				default:
					$sec = $s1;
					$mt = $mt1;
					$et = $et1;

					$img1 = 'Sector';
					break;
			}
        
			// #------------- UPLOAD FOR SECTOR 1 --------------------#			
            $config = array(
                'upload_path' => $path,
                'allowed_types' => "gif|jpg|png|jpeg",
				'file_name' => 'Sector'
            );

			#CHECK IF SITENAME & TECH EXIST IN THE DB
			$this->db->where('sitename', $sname);
			$this->db->where('tech', $tech_mdb);
			$res = $this->db->get('audit');

			if ($res->num_rows() > 0) {
				#REMOVE FILES BEFORE UPLOAD
				$this->load->helper("file");
				delete_files($path, TRUE);
				unset($path);

				#REMOVE DATA INTO DB
				$this->db->where('sitename', $sname);
				$this->db->where('tech', $tech_dir);
				$this->db->delete('audit');
			} 

			#CREATE DIRECTORY
            $this->load->library('upload',$config);
            if(!file_exists($config['upload_path'])) {
                mkdir($config['upload_path'],0777, TRUE);
            }
			
			#-----------UPLOAD FILES INTO DIRECTORY---------------#
            #INSERT IN DIRECTORY INTO SECTOR 1
			if($this->upload->do_upload('capture-1')) {
				$cap1 = $this->upload->data('capture-1');
			}

			 #INSERT IN DIRECTORY INTO SECTOR 2
			 if($this->upload->do_upload('capture-2')) {
				$cap2 = $this->upload->data('capture-2');
			}

			 #INSERT IN DIRECTORY INTO SECTOR 1
			 if($this->upload->do_upload('capture-3')) {
				$cap3 = $this->upload->data('capture-3');
			}

			 #INSERT IN DIRECTORY INTO SECTOR 2
			 if($this->upload->do_upload('capture-4')) {
				$cap4 = $this->upload->data('capture-4');
			}
		
			#-----------INSERT DATA DB---------------#
			$data = array(
				'region' => $region,
				'plaid' => $region.$plaid,
				'height' => $height,
				'sitename' => strtoupper($sname),
				'tech' => $tech_mdb,
				'tech_dir' => $tech_dir,
				'sectors' => $sectors,
				'azimuth' => $sec,
				'dualbeam' => $db,
				'vendor' => $vendor,
				'mtilt' => $mt,
				'etilt' => $et,
				'antenna' => $am1,
				's1_img' => $img1,
				's2_img' => $img2,
				's3_img' => $img3,
				's4_img' => $img4,
				'date_updated' => date('Y-m-d H:i:s')
			);
			
			$this->db->insert('audit', $data);

			$json = array(
				'title' => 'Success',
				'msg'   => 'Successfully saved.',
				'type'  => 'success',
			);
        }  else {
			echo 1;
		}

        return $json;
    }

	public function export_audit() {
		$this->db->order_by('sitename', 'ASC');
		$this->db->select('plaid, sitename, height, tech, sectors, azimuth, dualbeam, mtilt, etilt, antenna, date_updated');
		$this->db->where('region', $this->geoarea);
		$result = $this->db->get('audit');

		$data = array(
			'result' => $result->result(),
			'geoarea' => strtoupper($this->geoarea)
		);

		return $data;
	}

	public function del_audit() {
		$id = $this->input->post('id');
		$tech = $this->input->post('tech');
		$sname = $this->input->post('sname');

		#CHECK IF EXIST
		#CHECK IF SITENAME & TECH EXIST IN THE DB
		$this->db->where('id', $id);
		$res = $this->db->get('audit');
		
		if ($res->num_rows() > 0) {
			#REMOVE FILES BEFORE UPLOAD
			
			#FILE DIRECTORY
			$path = FCPATH.'/assets/audit/'.$this->geoarea.'/'.$sname.'/'.$tech;

			$this->load->helper("file");
			$this->my_folder_delete($path);

			#REMOVE DATA INTO DB
			$this->db->where_in('id', $id);
			$this->db->delete('audit');

			$json = array(
				'title' => 'Success',
				'msg'   => 'Successfully removed.',
				'type'  => 'success',
			);
		} else {
			$json = array(
				'title' => 'Oops!',
				'msg'   => 'Data does not found.',
				'type'  => 'error',
			);
		}

		return $json;
	}

	function my_folder_delete($path) {
		if(!empty($path) && is_dir($path) ){
			$dir  = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS); //upper dirs are not included,otherwise DISASTER HAPPENS :)
			$files = new RecursiveIteratorIterator($dir, RecursiveIteratorIterator::CHILD_FIRST);
			foreach ($files as $f) {if (is_file($f)) {unlink($f);} else {$empty_dirs[] = $f;} } if (!empty($empty_dirs)) {foreach ($empty_dirs as $eachDir) {rmdir($eachDir);}} rmdir($path);
		}
	}

	public function timestamp($date)
	{
		$this->load->helper('date');

		// Declare timestamps
		$last = new DateTime($date);
		$now = new DateTime(date('d-m-Y H:i:s', time()));

		// Find difference
		$interval = $last->diff($now);
		$years = (int)$interval->format('%Y');
		$months = (int)$interval->format('%m');
		$days = (int)$interval->format('%d');
		$hours = (int)$interval->format('%H');
		$minutes = (int)$interval->format('%i');

		if ($years > 0) {
			return $years . ' Yr' .($years > 1 ? 's ' : ' ') . $months . ' mo' .($months > 1 ? 's ' : ' '). $days . ' d'.($days>1 ? 's ' : ' ') . $hours . ' hr'.($hours > 1 ? 's ' : ' ') . $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		} else if ($months > 0) {
			return $months . ' Mo' .($months > 1 ? 's ' : ' ') . $days . ' d'.($days>1 ? 's ' : ' ') . $hours . ' hr'.($hours > 1 ? 's ' : ' ') . $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		} else if ($days > 0) {
			return $days . ' D'.($days>1 ? 's ' : ' ') . $hours . ' Hr'.($hours > 1 ? 's ' : ' ') . $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		} else if ($hours > 0) {
			return $hours . ' Hr'.($hours > 1 ? 's ' : ' ') . $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		} else {
			return $minutes == 0 ? ' Now' : $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		}
	}
}


