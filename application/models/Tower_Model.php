<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tower_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->geoarea = $this->session->userdata('geoarea');
		$this->geo = $this->input->post('geo_tower');
		$this->status = $this->input->post('status');
		$this->tablename = "tower_" . $this->geoarea;
		$this->tower_propsal_tbl = "tower_proposal_" . $this->geoarea;
		$this->regions = array('min', 'slz', 'nlz', 'ncr', 'vis');

		$this->tbl2g = "plan_2g";
		$this->tbl3g = "plan_3g";
		$this->tbl4g = "plan_4g";
		$this->tbl5g = "plan_5g";
	}

	public function get_tower()
	{
		$geoarea = $this->session->userdata('geoarea');
		$tablename = "towerdb_" . $geoarea;

		$this->db->order_by('SITENAME', 'ASC');
		$result = $this->db->get($tablename);
		return $result->result();
	}
	public function get_antenna_epa_table()
	{
		// $geoarea = $this->session->userdata('geoarea');
		$tablename = "antennadb";

		// $this->db->order_by('SITENAME', 'ASC');
		$result = $this->db->get($tablename);
		return $result->result();
	}
	public function get_tower_epa_table()
	{
		// $geoarea = $this->session->userdata('geoarea');
		$tablename = "tower_epa";

		// $this->db->order_by('SITENAME', 'ASC');
		$result = $this->db->get($tablename);
		return $result->result();
	}

	public function get_project() {
		if (empty($value) && $this->geoarea === 'nat') {
			#TOWER
			$tc = 0;
			foreach ($this->regions as $keys => $value) {
				$this->db->where("SITE_TYPE", 'Greenfield');
				$this->db->or_where("SITE_TYPE", 'Rooftop');
				$this->db->or_where("SITE_TYPE", 'OD Coloc to IBS');
				$tc += $this->db->get('tower_'.$value)->num_rows();
			}

			#POWER
			$pc = 0;
			foreach ($this->regions as $keys => $value) {
				$pc += $this->db->get('powerdb'.'_' . $value)->num_rows();
			}

			$data = array(
				'tower' => $tc,
				'powerdb' => $pc
			);
		} else {
			$this->db->where("SITE_TYPE", 'Greenfield');
			$this->db->or_where("SITE_TYPE", 'Rooftop');
			$this->db->or_where("SITE_TYPE", 'OD Coloc to IBS');
			$tower = $this->db->get('tower_'.$this->geoarea)->num_rows();

			$data = array(
				'tower' => 	$tower,
				'powerdb' =>  $this->db->get('powerdb'.'_' . $this->geoarea)->num_rows()
			);
		}

		return $data;
	}

	public function get_on_aired() {
		$type = array('Not', 'ON-AIRED');
		$data = array();
		if (empty($value) && $this->geoarea === 'nat') {
			foreach ($type as $rows) {
				$counts = 0;
				$region = $this->regions;
				foreach ($region as $reg) {
					$this->db->select('COUNT(*) as total');
					$this->db->from('tower_'.$reg);
					$this->db->where("SITE_TYPE", 'GREENFIELD');
					$this->db->where("SITE_TYPE", 'ROOFTOP');
					$row = $this->db->get()->row();
					$counts += intval($row->total);
				}
				$data[$rows] = $counts;
			}
		} else {
			foreach ($type as $row) {
				$this->db->select('COUNT(*) as total');
				$this->db->from('tower_'.$this->geoarea);
				$this->db->where("SITE_TYPE", 'GREENFIELD');
				$this->db->where("SITE_TYPE", 'ROOFTOP');
				$rows = $this->db->get()->row();
				$data[$row] = intval($rows->total);
			}
		}
		return $data;
	}

	public function get_site_type()
	{
		$data = array();
		$type = array('GREENFIELD', 'ROOFTOP', 'IBS', 'COW LEGO TEMPO', 'SMALL CELL', 'OD COLOC TO IBS', 'SWAT', 'MACRO', 'DISMANTLED', 'to be updated');

		if (empty($value) && $this->geoarea === 'nat') {
			foreach ($type as $key => $value) {
				$counts = 0;
				foreach ($this->regions as $keys => $reg) {
					$this->db->select('COUNT(*) as total');
					$this->db->from('tower_'.$reg.' b');
					$this->db->where("b.SITE_TYPE", $value);
					$row = $this->db->get()->row();
					$counts += intval($row->total);
				}
				$data[$value] = $counts;
			}
		} else {
			foreach ($type as $key => $rows) {
				$this->db->select('COUNT(*) as total');
				$this->db->from('tower_'.$this->geoarea.' b');
				$this->db->where("b.SITE_TYPE", $rows);
				$row = $this->db->get()->row();
				$data[$rows] = intval($row->total);
			}
		}
		return $data;
	}

	public function get_gf_site() {
		$data = array();
		$type = array('SST', 'GUYED TOWER', 'MONOPOLE', 'LEGO', 'COW', 'OTHER GF TOWER', 'to be updated');
		if (empty($this->geo) && $this->geoarea === 'nat') {
			foreach ($type as $key => $row) {
				$counts = 0;
				foreach ($this->regions as $keys => $rows) {
					$this->db->where("SIMPLIFIED_TOWER_TYPE", $row);
					$this->db->where("SITE_TYPE", 'GREENFIELD');
					if (!empty($this->status)) {$this->db->where('ON_AIRED_SITES', 'to be updated');}
					$counts += $this->db->get('tower_'.$rows)->num_rows();
				}
				$data[$row] = $counts;
			}
		} else {
			$tbl_name = $this->geoarea === 'nat' ? 'tower_'.$this->geo : $this->tablename;
			foreach ($type as $key => $row) {
				$this->db->where("SIMPLIFIED_TOWER_TYPE", $row);
				$this->db->where("SITE_TYPE", 'GREENFIELD');
				if (!empty($this->status)) {$this->db->where('ON_AIRED_SITES', 'to be updated');}
				$data[$row] = $this->db->get($tbl_name)->num_rows();
			}
		}
		return $data;
	}

	public function get_rt_site() {
		$data = array();
		$type = array('TRIPOD', 'MINITOWER', 'ROOFTOP POLE', 'OTHER RT TOWER', 'to be updated');
		if (empty($this->geo) && $this->geoarea === 'nat') {
			foreach ($type as $key => $row) {
				$counts = 0;
				foreach ($this->regions as $keys => $rows) {
					$this->db->where("(SITE_TYPE = 'ROOFTOP' OR  SITE_TYPE = 'OD COLOC TO IBS') AND SIMPLIFIED_TOWER_TYPE = ", $row);
					if (!empty($this->status)) {$this->db->where('ON_AIRED_SITES', 'to be updated');}
					$counts += $this->db->get('tower_'.$rows)->num_rows();
				}
				$data[$row] = $counts;
			}
		} else {
			$tbl_name = $this->geoarea === 'nat' ? 'tower_'.$this->geo : $this->tablename;
			foreach ($type as $key => $row) {
				$this->db->where("(SITE_TYPE = 'ROOFTOP' OR  SITE_TYPE = 'OD COLOC TO IBS') AND SIMPLIFIED_TOWER_TYPE = ", $row);
				if (!empty($this->status)) {$this->db->where('ON_AIRED_SITES', 'to be updated');}
				$data[$row] = $this->db->get($tbl_name)->num_rows();
			}
		}
		return $data;
	}

	public function tower_capacity()
	{
		$data = array();
		$bc = 0;
		$fc = 0;
		$wh = 0;
		$none = 0;
		$tbu = 0;
		$column = 'NSCP7_COMPLIANCE_SUMMARY_CAPACITY_REMARKS';
		if (empty($this->geo) && $this->geoarea === 'nat') {
			foreach ($this->regions as $keys => $rows) {
				$this->db->select($column . ' nscp');
				$this->db->where("SITE_TYPE", 'Greenfield');
				$this->db->or_where("SITE_TYPE", 'Rooftop');
				$this->db->or_where("SITE_TYPE", 'OD Coloc to IBS');
				if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
				$this->db->from('tower_'.$rows);
				$result = $this->db->get();

				if ($result->num_rows() > 0) {
					foreach ($result->result() as $row) {
						$val = strtoupper($row->nscp);
						if ($val === 'WITH HEADROOM') {
							$wh += 1;
						} else if ($val === 'FULL CAPACITY') {
							$fc += 1;
						} else if ($val === 'BEYOND CAPACITY') {
							$bc += 1;	
						} else if ($val === 'NONE' || $val === 'NA') {
							$none += 1;
						} else {
							$tbu += 1;
						}
					}
				}
			}
		} else {
			$tbl_name = $this->geoarea === 'nat' ? 'tower_'.$this->geo : $this->tablename;
			$this->db->select($column . ' nscp');
			$this->db->where("SITE_TYPE", 'Greenfield');
			$this->db->or_where("SITE_TYPE", 'Rooftop');
			$this->db->or_where("SITE_TYPE", 'OD Coloc to IBS');
			if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
			$this->db->from($tbl_name);
			$result = $this->db->get();

			if ($result->num_rows() > 0) {
				foreach ($result->result() as $row) {
					$val = strtoupper($row->nscp);
					if ($val === 'WITH HEADROOM') {
						$wh += 1;
					} else if ($val === 'FULL CAPACITY') {
						$fc += 1;
					} else if ($val === 'BEYOND CAPACITY') {
						$bc += 1;
					} else if ($val === 'NONE' || $val === 'NA') {
						$none += 1;
					} else {
						$tbu += 1;
					}
				}
			}
		}
		return array(
			'wh' => $wh,
			'bc' => $bc,
			'fc' => $fc,
			'none' => $none,
			'tbu' => $tbu
		);
	}

	public function nscp_compliance() {
		$n7 = 0;
		$n6 = 0;
		$retro = 0;
		#GET NSCP 7 COMPLAINT
		$column = 'NSCP7_COMPLIANCE_SUMMARY_SI_AND_RETRO_NSCP7_COMPLIANCE_REMARKS';
		if (empty($this->geo) && $this->geoarea === 'nat') {
				foreach ($this->regions as $keys => $rows) {
					$this->db->select($column . ' nscp');
					$this->db->where("SITE_TYPE", 'Greenfield');
					$this->db->or_where("SITE_TYPE", 'Rooftop');
					$this->db->or_where("SITE_TYPE", 'OD Coloc to IBS');
					if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
					$this->db->from('tower_'.$rows);
					$result = $this->db->get();

					if ($result->num_rows() > 0) {
						foreach ($result->result() as $row) {
							$val = strtoupper($row->nscp);
							if ($val === 'NSCP 7 COMPLIANT') {
								$n7 += 1;
							} else if ($val === 'ONGOING SI / RETRO') {
								$retro += 1;
							} else {
								$n6 += 1;
							}
						}
					}
				}
		} else {
				$tbl_name = $this->geoarea === 'nat' ? 'tower_'.$this->geo : $this->tablename;
				$this->db->select($column . ' nscp');
				$this->db->where("SITE_TYPE", 'Greenfield');
				$this->db->or_where("SITE_TYPE", 'Rooftop');
				$this->db->or_where("SITE_TYPE", 'OD Coloc to IBS');
				if (!empty($this->sc)) {$this->db->where('SITE_CLASS', $this->sc);}
				$this->db->from($tbl_name);
				$result = $this->db->get();

				if ($result->num_rows() > 0) {
					foreach ($result->result() as $row) {
						$val = strtoupper($row->nscp);
						if ($val === 'NSCP 7 COMPLIANT') {
							$n7 += 1;
						} else if ($val === 'ONGOING SI / RETRO') {
							$retro += 1;
						} else {
							$n6 += 1;
						}
					}
				}
		}

		return array(
			'n7' => $n7,
			'n6' => $n6,
			'retro' => $retro
		);
	}

	public function tower_upload() {
		if (isset($_FILES["tower_file"]["name"])) {
			$path = $_FILES["tower_file"]["tmp_name"];
			$object= PHPExcel_IOFactory::load($path);

			#GET FORMAT HEADER OF EXCEL
			$cell_collection = $object->getActiveSheet()->getCellCollection();
			foreach ($cell_collection as $cell) {
				$column = $object->getActiveSheet()->getCell($cell)->getColumn();
				$row = $object->getActiveSheet()->getCell($cell)->getRow();
				$data_value = $object->getActiveSheet()->getCell($cell)->getValue();
				//The header will/should be in row 1 only. of course, this can be modified to suit your need.
				if ($row == 1) {
					$header[$row][$column] = $data_value;
				}
			}

			#CHECK IF EXCEL IMPORT MATCHES TO DATABASE FORMAT OF EXCEL
			if (102 == count($header[1])) {
				foreach ($object->getWorksheetIterator() as $worksheet) {
					$highestRow = $worksheet->getHighestRow();
					for ($row = 2; $row <= $highestRow; $row++) {
						$data[] = array(
							'SITENAME' => $worksheet->getCellByColumnAndRow(0, $row)->getValue(),
							'PLAID' => $worksheet->getCellByColumnAndRow(1, $row)->getValue(),
							'COMBINE_SITENAME' => $worksheet->getCellByColumnAndRow(2, $row)->getValue(),
							'OLD_SITENAME' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
							'RELAY_SITENAME' => $worksheet->getCellByColumnAndRow(4, $row)->getValue(),
							'REGION' => $worksheet->getCellByColumnAndRow(5, $row)->getValue(),
							'SITE_TYPE' => $worksheet->getCellByColumnAndRow(6, $row)->getValue(),
							'DETAILED_TOWER_TYPE' => $worksheet->getCellByColumnAndRow(7, $row)->getValue(),
							'SIMPLIFIED_TOWER_TYPE' => $worksheet->getCellByColumnAndRow(8, $row)->getValue(),
							'TOWER_HEIGHT' => $worksheet->getCellByColumnAndRow(9, $row)->getValue(),
							'BLDG_HEIGHT' => $worksheet->getCellByColumnAndRow(10, $row)->getValue(),
							'SIMPLIFIED_TOWER_HEIGHT' => $worksheet->getCellByColumnAndRow(11, $row)->getValue(),

							'TOWER_HEIGHT_AND_TYPE' => $worksheet->getCellByColumnAndRow(12, $row)->getValue(),
							'IBS_SITES' => $worksheet->getCellByColumnAndRow(13, $row)->getValue(),
							'ON_AIRED_SITES' => $worksheet->getCellByColumnAndRow(14, $row)->getValue(),
							'SITES_INSIDE_GROUPLIST' => $worksheet->getCellByColumnAndRow(15, $row)->getValue(),
							'SITE_AND_TOWER_INFO_REMARKS' => $worksheet->getCellByColumnAndRow(16, $row)->getValue(),
							'TRANSPORT_FROM_EFREN' => $worksheet->getCellByColumnAndRow(17, $row)->getValue(),
							'TRANSPORT_REMARKS_FOC' => $worksheet->getCellByColumnAndRow(18, $row)->getValue(),
							'RRE_SWAT_TYPE' => $worksheet->getCellByColumnAndRow(19, $row)->getValue(),
							'RRE_TEMPORARY_SITE' => $worksheet->getCellByColumnAndRow(20, $row)->getValue(),

							'RRE_DISMANTLED_SITES' => $worksheet->getCellByColumnAndRow(21, $row)->getValue(),
							'RRE_BALUARTE' => $worksheet->getCellByColumnAndRow(22, $row)->getValue(),
							'RRE_SOLUTION_TYPE' => $worksheet->getCellByColumnAndRow(23, $row)->getValue(),
							'RRE_COMBINED_SOLUTION_TYPE' => $worksheet->getCellByColumnAndRow(24, $row)->getValue(),
							'RRE_SITE_TYPE' => $worksheet->getCellByColumnAndRow(25, $row)->getValue(),
							'RRE_COMBINED_SITE_TYPE' => $worksheet->getCellByColumnAndRow(26, $row)->getValue(),
							'RRE_COVERAGE_TYPE' => $worksheet->getCellByColumnAndRow(27, $row)->getValue(),
							'RRE_COMBINED_COVERAGE_TYPE' => $worksheet->getCellByColumnAndRow(28, $row)->getValue(),
							'RRE_ALL_TECH' => $worksheet->getCellByColumnAndRow(29, $row)->getValue(),
							'RRE_COMBINED_ALL_TECH' => $worksheet->getCellByColumnAndRow(30, $row)->getValue(),
							'RRE_WITH_2G' => $worksheet->getCellByColumnAndRow(31, $row)->getValue(),
							'RRE_WITH_3G' => $worksheet->getCellByColumnAndRow(32, $row)->getValue(),
							'RRE_WITH_4G' => $worksheet->getCellByColumnAndRow(33, $row)->getValue(),
							'RRE_WITH_5G' => $worksheet->getCellByColumnAndRow(34, $row)->getValue(),
							'RRE_PROVINCE' => $worksheet->getCellByColumnAndRow(35, $row)->getValue(),
							'RRE_TOWN' => $worksheet->getCellByColumnAndRow(36, $row)->getValue(),
							'RRE_BARANGAY' => $worksheet->getCellByColumnAndRow(37, $row)->getValue(),
							'RRE_SITE_ADDRESS' => $worksheet->getCellByColumnAndRow(38, $row)->getValue(),
							'RRE_LONGITUDE' => $worksheet->getCellByColumnAndRow(39, $row)->getValue(),
							'RRE_LATITUDE' => $worksheet->getCellByColumnAndRow(40, $row)->getValue(),

							'TSSR_PTA_UPDATE_PROJECT' => $worksheet->getCellByColumnAndRow(41, $row)->getValue(),
							'TSSR_PTA_UPDATE_LATEST_PTA_UPDATE' => $worksheet->getCellByColumnAndRow(42, $row)->getValue(),
							'TSSR_PTA_UPDATE_TSSR_PTA_TOWER_UR_EXISITING' => $worksheet->getCellByColumnAndRow(43, $row)->getValue(),
							'TSSR_PTA_UPDATE_SIMPLIFIED_UR_EXISITNG' => $worksheet->getCellByColumnAndRow(44, $row)->getValue(),
							'TSSR_PTA_UPDATE_TSSR_PTA_TOWER_UR_EXISTING_PLUS_PROPOSED' => $worksheet->getCellByColumnAndRow(45, $row)->getValue(),
							'TSSR_PTA_UPDATE_SIMPLIFIED_UR_EXISTING_PLUS_PROPOSED' => $worksheet->getCellByColumnAndRow(46, $row)->getValue(),
							'TSSR_PTA_UPDATE_TSSR_PTA_REMARKS' => $worksheet->getCellByColumnAndRow(47, $row)->getValue(),
							'TSSR_PTA_UPDATE_USING_NEW_PTA_TEMPLATE' => $worksheet->getCellByColumnAndRow(48, $row)->getValue(),
							'TSSR_PTA_UPDATE_NSCP7_DESIGN_WIND_SPEED' => $worksheet->getCellByColumnAndRow(49, $row)->getValue(),

							'SI_UPDATE_ONGOING_SI' => $worksheet->getCellByColumnAndRow(50, $row)->getValue(),
							'SI_UPDATE_SI_DONE' => $worksheet->getCellByColumnAndRow(51, $row)->getValue(),
							'SI_UPDATE_LATEST_SI_OWNER_SI_DONE_ONGOING_SI' => $worksheet->getCellByColumnAndRow(52, $row)->getValue(),
							'SI_UPDATE_SI_SUMMARY' => $worksheet->getCellByColumnAndRow(53, $row)->getValue(),
							'SI_UPDATE_SI_REFERENCE_AND_RESULT' => $worksheet->getCellByColumnAndRow(54, $row)->getValue(),
							'SI_UPDATE_NO_OF_SI_DONE_ON_SITE' => $worksheet->getCellByColumnAndRow(55, $row)->getValue(),
							'SI_UPDATE_LATEST_SI_DONE_ON_SITE' => $worksheet->getCellByColumnAndRow(56, $row)->getValue(),
							'SI_UPDATE_RESULT_OF_LAST_SI' => $worksheet->getCellByColumnAndRow(57, $row)->getValue(),
							'SI_UPDATE_LATEST_NSCP_CODE_USED_FOR_SI' => $worksheet->getCellByColumnAndRow(58, $row)->getValue(),
							'SI_UPDATE_UR_BEFORE_RETRO' => $worksheet->getCellByColumnAndRow(59, $row)->getValue(),
							'SI_UPDATE_UR_AFTER_RETRO' => $worksheet->getCellByColumnAndRow(60, $row)->getValue(),
							'SI_UPDATE_SI_REFERENCE_AND_RESULT_FOR_NSCP7_COMPLIANCE' => $worksheet->getCellByColumnAndRow(61, $row)->getValue(),
							'SI_UPDATE_NSCP7_COMPLIANCE_BASED_ON_LATEST_SI' => $worksheet->getCellByColumnAndRow(62, $row)->getValue(),
							'SI_UPDATE_REMARKS' => $worksheet->getCellByColumnAndRow(63, $row)->getValue(),

							'RETRO_UPDATE_RETRO_STATUS' => $worksheet->getCellByColumnAndRow(64, $row)->getValue(),
							'RETRO_UPDATE_LATEST_RETRO_OWNER_RETRO_DONE_ONGOING_RETRO' => $worksheet->getCellByColumnAndRow(65, $row)->getValue(),
							'RETRO_UPDATE_NO_OF_RETRO_DONE_ON_SITE' => $worksheet->getCellByColumnAndRow(66, $row)->getValue(),
							'RETRO_UPDATE_DATE_OF_LAST_RETROFITTING_DONE_ON_SITE' => $worksheet->getCellByColumnAndRow(67, $row)->getValue(),
							'RETRO_UPDATE_LATEST_NSCP_CODE_USED_FOR_RETRO' => $worksheet->getCellByColumnAndRow(68, $row)->getValue(),
							'RETRO_UPDATE_APPROVAL_DATE_OF_SI_REFERENCE_USED_FOR_RETRO' => $worksheet->getCellByColumnAndRow(69, $row)->getValue(),
							'RETRO_UPDATE_REMARKS' => $worksheet->getCellByColumnAndRow(70, $row)->getValue(),
							'RETRO_UPDATE_NSCP7_COMPLIANCE' => $worksheet->getCellByColumnAndRow(71, $row)->getValue(),
							'TOWER_CONVERSION_SITES_FOR_TOWER_CONVERSION' => $worksheet->getCellByColumnAndRow(72, $row)->getValue(),
							'TOWER_CONVERSION_REMARKS' => $worksheet->getCellByColumnAndRow(73, $row)->getValue(),
							'TOWER_CONVERSION_OPTIMIZATION_RESULT' => $worksheet->getCellByColumnAndRow(74, $row)->getValue(),
							'TOWER_CONVERSION_FINAL_REMARKS' => $worksheet->getCellByColumnAndRow(75, $row)->getValue(),

							'NSCP7_COMPLIANCE_SUMMARY_ONGOING_TOWER_STATUS' => $worksheet->getCellByColumnAndRow(76, $row)->getValue(),
							'NSCP7_COMPLIANCE_SUMMARY_SI_AND_RETRO_NSCP7_COMPLIANCE_REMARKS' => $worksheet->getCellByColumnAndRow(77, $row)->getValue(),
							'NSCP7_COMPLIANCE_SUMMARY_LATEST_TOWER_STATUS_SI_AND_RETRO' => $worksheet->getCellByColumnAndRow(78, $row)->getValue(),
							'NSCP7_COMPLIANCE_SUMMARY_NSCP7_COMPLIANCE_GOVERNED_BY' => $worksheet->getCellByColumnAndRow(79, $row)->getValue(),
							'NSCP7_COMPLIANCE_SUMMARY_CAPACITY_REMARKS' => $worksheet->getCellByColumnAndRow(80, $row)->getValue(),
							'NSCP7_COMPLIANCE_SUMMARY_W_TOWER_UPGRADE_DONE_ALREADY' => $worksheet->getCellByColumnAndRow(81, $row)->getValue(),

							'MILESTONE_SUMMARY_UPDATE_BASED_ON_LATEST_RETRO_SI_TSSR_PTA' => $worksheet->getCellByColumnAndRow(82, $row)->getValue(),
							'TOWER_UPDATE_REMARKS_FE_TOWER_REMARKS' => $worksheet->getCellByColumnAndRow(83, $row)->getValue(),
							'TOWER_UPDATE_REMARKS_SITES_REQUIRING_PTA_UPDATING_VERIFICATION' => $worksheet->getCellByColumnAndRow(84, $row)->getValue(),
							'TOWER_UPDATE_REMARKS_RFE_TOWER_REMARKS' => $worksheet->getCellByColumnAndRow(85, $row)->getValue(),

							'CABIN_ODU_AND_GENSET_PAD_TYPHOON_PATH' => $worksheet->getCellByColumnAndRow(86, $row)->getValue(),
							'CABIN_ODU_AND_GENSET_PAD_CABIN_TYPE' => $worksheet->getCellByColumnAndRow(87, $row)->getValue(),
							'CABIN_ODU_AND_GENSET_PAD_W_ODU_PAD' => $worksheet->getCellByColumnAndRow(88, $row)->getValue(),
							'CABIN_ODU_AND_GENSET_PAD_NO_OF_ODU_PAD' => $worksheet->getCellByColumnAndRow(89, $row)->getValue(),
							'CABIN_ODU_AND_GENSET_PAD_NO_OF_EXISITNG_CABINET' => $worksheet->getCellByColumnAndRow(90, $row)->getValue(),
							'CABIN_ODU_AND_GENSET_PAD_NO_OF_AVAILABLE_CABINET_SPACE_SPARE' => $worksheet->getCellByColumnAndRow(91, $row)->getValue(),
							'CABIN_ODU_AND_GENSET_PAD_W_GENSET_PAD' => $worksheet->getCellByColumnAndRow(92, $row)->getValue(),

							'AMB_SOLUTIONS_LEG_TYPE_FOR_AMB_ALLOCATION' => $worksheet->getCellByColumnAndRow(93, $row)->getValue(),
							'AMB_SOLUTIONS_TYPE_OF_AMB_INSTALLED_ON_TOWER' => $worksheet->getCellByColumnAndRow(94, $row)->getValue(),
							'AMB_SOLUTIONS_W_GANTRY' => $worksheet->getCellByColumnAndRow(95, $row)->getValue(),
							'AMB_SOLUTIONS_NO_OF_ANTENNA_INSTALLED_AT_GANTRY' => $worksheet->getCellByColumnAndRow(96, $row)->getValue(),
							'AMB_SOLUTIONS_NO_OF_SPARE_AT_GANTRY' => $worksheet->getCellByColumnAndRow(97, $row)->getValue(),
							'TSSR_DATE_OF_LAST_TSSR_APPROVAL' => $worksheet->getCellByColumnAndRow(98, $row)->getValue(),

							'TOWER_BUILD_SCOPE' => $worksheet->getCellByColumnAndRow(99, $row)->getValue(),
							'BUILD_REMARKS' => $worksheet->getCellByColumnAndRow(100, $row)->getValue(),
							'BUILD_COMPLETION' => $worksheet->getCellByColumnAndRow(101, $row)->getValue(),
						);
					}
				}

				#CLEAR DATA BEFORE INSERT
				$this->db->truncate($this->tablename);
				$this->db->insert_batch($this->tablename, array_filter($data));

				#GET COMPLETION
				$this->db->select('id');
				$tobe_update = $this->db->get($this->tablename)->result();
				$tower_id = array();
				foreach ($tobe_update as $key)
				{
					array_push($tower_id, $key->id);
				}

				$this->update_completion_data($tower_id);

				if ($this->db->affected_rows() == 0) {
					$json = array(
						'title' => 'Oops!',
						'msg'   => 'Upload failed.',
						'type'  => 'error',
					);
				} else {
					$json = array(
						'title' => 'Success',
						'msg'   => 'Successfully uploaded.',
						'type'  => 'success',
					);
				}

			} else {
				$json = array(
					'title' => 'Oops!',
					'msg'   => 'Import file does not match to desired format.',
					'type'  => 'error'
				);
			}
		} else {
			$json = array(
				'title' => 'Ooops!',
				'msg'   => 'Something went wrong.',
				'type'  => 'error'
			);
		}
		return $json;
	}

	public function update_completion_data($id)
	{
		set_time_limit(0);
		foreach ($id as $value)
		{
			$add_array = $this->Site_database->getSingle($this->tablename, array("id" => $value), "SITENAME, PLAID, SITE_TYPE, DETAILED_TOWER_TYPE, TOWER_HEIGHT, TSSR_PTA_UPDATE_TSSR_PTA_TOWER_UR_EXISTING_PLUS_PROPOSED, NSCP7_COMPLIANCE_SUMMARY_SI_AND_RETRO_NSCP7_COMPLIANCE_REMARKS");

			$tobeupdatedcellcount = 0;
			$tempcount = 1;
			foreach ($add_array as $valuetobe)
			{
				if (strtolower($valuetobe) == "to be updated" || $valuetobe == null || $valuetobe == " ")
				{
					$tobeupdatedcellcount++;
				}
				if ($tempcount >= 7)
				{
					break;
				}
				$tempcount++;
			}
			$add_array->completion = number_format((7 - $tobeupdatedcellcount) * (100 / 7), 2);

			$add_array->date = date('Y-m-d');

			$this->db->update($this->tablename, $add_array, array("id" => $value));
		}
		return $this->db->affected_rows();
	}

	public function get_completion() {
		if ($this->geoarea == 'nat') {
			$completed = 0;
			$total = 0;
			foreach (array('min', 'slz', 'nlz', 'ncr', 'vis') as $keys => $values) {
				$this->db->select('COUNT(*) as completed');
				$this->db->from('tower_'.$values);
				$this->db->where('completion', 100);
				// $this->db->where("SITE_TYPE", 'Greenfield');
				// $this->db->or_where("SITE_TYPE", 'Rooftop');
				// $this->db->or_where("SITE_TYPE", 'OD Coloc to IBS');
				$res = $this->db->get()->row();
				$completed += $res->completed;

				#TOTAL	
				// $this->db->where("SITE_TYPE", 'Greenfield');
				// $this->db->or_where("SITE_TYPE", 'Rooftop');
				// $this->db->or_where("SITE_TYPE", 'OD Coloc to IBS');
				$totals = $this->db->get('tower_'.$values)->num_rows();
				
				$total += $totals;
			}

			
			
		} else {
			$this->db->select('COUNT(*) as completed');
			$this->db->from($this->tablename);
			$this->db->where('completion', 100);
			// $this->db->where("SITE_TYPE", 'Greenfield');
			// $this->db->or_where("SITE_TYPE", 'Rooftop');
			// $this->db->or_where("SITE_TYPE", 'OD Coloc to IBS');
			$res = $this->db->get()->row();
			$completed = $res->completed;

			#TOTAL	
			// $this->db->where("SITE_TYPE", 'Greenfield');
			// $this->db->or_where("SITE_TYPE", 'Rooftop');
			// $this->db->or_where("SITE_TYPE", 'OD Coloc to IBS');
			$total = $this->db->get($this->tablename)->num_rows();
		}

		return array(
			'completed' => $completed,
			'total' => $total
		);
	}

	public function get_chart_completion() {
		$completed = 0;
		$total = 0;

		foreach ($this->regions as $keys => $values) {
			$this->db->select('COUNT(*) as completed, (SELECT COUNT(*) FROM tower_'.$values.') as total');
			$this->db->from('tower_'.$values);
			$this->db->where('completion', 100);
			$res = $this->db->get()->row();
			$region[$values] = array(
				'completed' => $res->completed,
				'total' => $res->total
			);
			$completed += $res->completed;
			$total += $res->total;
		}

		$region['overall'] = array(
			'completed' => $completed,
			'total' => $total
		);
		
		return $region;
	}

	public function get_tower_list() {
		$this->db->order_by('SITENAME', 'ASC');
		return $this->db->get($this->tablename)->result();
	}

	function save_proposal()
	{ 
		$this->db->insert_batch($this->tower_propsal_tbl,$this->input->post('prop_data'));
		return $this->db->affected_rows();
	}
	public function get_proposal() {
		$bid = $this->input->post('batch_key');
		$this->db->order_by('date', 'desc');
		$this->db->where('batch_key', $bid);
		return $this->db->get($this->tower_propsal_tbl)->result();
	}
	public function get_proposals_list() {
		$this->db->select('id,batch_key,date');
		$this->db->order_by('date', 'asc');
		$this->db->where('REGION', $this->geoarea);
		$result = $this->db->get($this->tower_propsal_tbl)->result();

	    // return $result;
		$temp_array = array();
	    $i = 0;
	    $key_array = array();
	   	$key = 'batch_key';
	    foreach($result as $val) {
	        if (!in_array($val->$key, $key_array)) {
	            $key_array[$i] = $val->$key;
	            // $temp_array[$i] = $val;
	            array_push($temp_array,$val);
	        }
	        $i++;
	    }
	    return $temp_array;
	}

	public function export_tower()
	{
		$geoarea = $this->session->userdata('geoarea');
		if ($this->geoarea === 'nat') {
			$result = array();

			$this->db->select('*');
			$this->db->distinct();
			$this->db->from('tower_nlz');
			$nlz = $this->db->get_compiled_select();

			$this->db->select('*');
			$this->db->distinct();
			$this->db->from('tower_slz');
			$slz = $this->db->get_compiled_select();

			$this->db->select('*');
			$this->db->distinct();
			$this->db->from('tower_ncr');
			$ncr = $this->db->get_compiled_select();

			$this->db->select('*');
			$this->db->distinct();
			$this->db->from('tower_vis');
			$vis = $this->db->get_compiled_select();

			$this->db->select('*');
			$this->db->distinct();
			$this->db->from('tower_min');
			$this->db->order_by('SITENAME', 'ASC');
			$min = $this->db->get_compiled_select();

			$result = $this->db->query("SELECT * FROM (" . $nlz." UNION ALL ".$slz." UNION ALL " .$ncr. " UNION ALL " .$vis. " UNION ALL " .$min.") AS X ORDER BY SITENAME ASC");
		} else {
			$tablename = "tower_" . $geoarea;
			$this->db->order_by('SITENAME', 'ASC');
			$this->db->select('*');
			$result = $this->db->get($tablename);
		}

		$data = array(
			'result' => $result->result(),
			'geoarea' => strtoupper($geoarea)
		);

		return $data;
	}

	public function get_tower_profile() {
		$plaid = $this->input->post('plaid');
		$id = $this->input->post('id');
		$region = strtolower($this->input->post('region'));
		
		$data = array();

		$this->db->where('id', $id);
		$this->db->where('PLAID', $plaid);
		$res = $this->db->get('tower_'.$region);

		if ($res->num_rows() > 0) {
			foreach($res->result() as $row) {
				$data['basic_info'] = array(
					'SITENAME' => $row->SITENAME,
					'PLAID' => $row->PLAID,
					'COMBINE SITENAME' => $row->COMBINE_SITENAME,
					'OLD SITENAME' => $row->OLD_SITENAME,
					'REGION' => $row->REGION,
					'SITE TYPE' => $row->SITE_TYPE,
					'DETAILED TOWER TYPE' => $row->DETAILED_TOWER_TYPE,
					'SIMPLIFIED TOWER TYPE' => $row->SIMPLIFIED_TOWER_TYPE,
					'TOWER HEIGHT' => $row->TOWER_HEIGHT,
					'BLDG HEIGHT' => $row->BLDG_HEIGHT,
					'SIMPLIFIED TOWER HEIGHT' => $row->SIMPLIFIED_TOWER_HEIGHT,
					'TOWER HEIGHT AND TYPE' => $row->TOWER_HEIGHT_AND_TYPE,
					'IBS SITES' => $row->IBS_SITES,
					'ON AIRED SITES' => $row->ON_AIRED_SITES,
					'SITES INSIDE GROUPLIST' => $row->SITES_INSIDE_GROUPLIST,
					'SITE AND TOWER INFO REMARKS' => $row->SITE_AND_TOWER_INFO_REMARKS,
					'TRANSPORT FROM EFREN' => $row->TRANSPORT_FROM_EFREN,
					'TRANSPORT REMARKS FOC' => $row->TRANSPORT_REMARKS_FOC,
					'SITE COMPLETION(%)' => $row->completion,
					'DATE UPDATED' => $row->date
				);

				$data['rre'] = array(
					'SWAT TYPE' => $row->RRE_SWAT_TYPE,
					'TEMPORARY SITE' => $row->RRE_TEMPORARY_SITE,
					'DISMANTLED SITES' => $row->RRE_DISMANTLED_SITES,
					'BALUARTE' => $row->RRE_BALUARTE,
					'SOLUTION_TYPE' => $row->RRE_SOLUTION_TYPE,
					'COMBINED SOLUTION TYPE' => $row->RRE_COMBINED_SOLUTION_TYPE,
					'SITE TYPE' => $row->RRE_SITE_TYPE,
					'COMBINED SITE TYPE' => $row->RRE_COMBINED_SITE_TYPE,
					'COVERAGE TYPE' => $row->RRE_COVERAGE_TYPE,
					'COMBINED COVERAGE TYPE' => $row->RRE_COMBINED_COVERAGE_TYPE,
					'ALL TECH' => $row->RRE_ALL_TECH,
					'COMBINED ALL TECH' => $row->RRE_COMBINED_ALL_TECH,
					'WITH_2G' => $row->RRE_WITH_2G,
					'WITH_3G' => $row->RRE_WITH_3G,
					'WITH_4G' => $row->RRE_WITH_4G,
					'WITH_5G' => $row->RRE_WITH_5G,
					'PROVINCE' => $row->RRE_PROVINCE,
					'TOWN' => $row->RRE_TOWN,
					'BARANGAY' => $row->RRE_BARANGAY,
					'SITE ADDRESS' => $row->RRE_SITE_ADDRESS,
					'LONGITUDE' => $row->RRE_LONGITUDE,
					'LATITUDE' => $row->RRE_LATITUDE,
				);

				$data['tssr'] = array(
					'PROJECT' => $row->TSSR_PTA_UPDATE_PROJECT,
					'LATEST PTA UPDATE' => $row->TSSR_PTA_UPDATE_LATEST_PTA_UPDATE,
					'TSSR PTA TOWER UR EXISITING' => $row->TSSR_PTA_UPDATE_TSSR_PTA_TOWER_UR_EXISITING,
					'SIMPLIFIED UR EXISITNG' => $row->TSSR_PTA_UPDATE_SIMPLIFIED_UR_EXISITNG,
					'TSSR PTA TOWER UR EXISTING PLUS PROPOSED' => $row->TSSR_PTA_UPDATE_TSSR_PTA_TOWER_UR_EXISTING_PLUS_PROPOSED,
					'SIMPLIFIED UR EXISTING PLUS PROPOSED' => $row->TSSR_PTA_UPDATE_SIMPLIFIED_UR_EXISTING_PLUS_PROPOSED,
					'TSSR PTA REMARKS' => $row->TSSR_PTA_UPDATE_TSSR_PTA_REMARKS,
					'USING NEW PTA TEMPLATE' => $row->TSSR_PTA_UPDATE_USING_NEW_PTA_TEMPLATE,
					'NSCP7 DESIGN WIND SPEED' => $row->TSSR_PTA_UPDATE_NSCP7_DESIGN_WIND_SPEED,
					'DATE OF LAST TSSR APPROVAL' => $row->TSSR_DATE_OF_LAST_TSSR_APPROVAL
				);

				$data['si'] = array(
					'ONGOING SI' => $row->SI_UPDATE_ONGOING_SI,
					'SI DONE' => $row->SI_UPDATE_SI_DONE,
					'LATEST SI OWNER SI DONE ONGOING SI' => $row->SI_UPDATE_LATEST_SI_OWNER_SI_DONE_ONGOING_SI,
					'SI SUMMARY' => $row->SI_UPDATE_SI_SUMMARY,
					'SI REFERENCE AND RESULT' => $row->SI_UPDATE_SI_REFERENCE_AND_RESULT,
					'NO OF SI DONE ON SITE' => $row->SI_UPDATE_NO_OF_SI_DONE_ON_SITE,
					'LATEST SI DONE ON SITE' => $row->SI_UPDATE_LATEST_SI_DONE_ON_SITE,
					'RESULT OF LAST SI' => $row->SI_UPDATE_RESULT_OF_LAST_SI,
					'LATEST NSCP CODE USED FOR SI' => $row->SI_UPDATE_LATEST_NSCP_CODE_USED_FOR_SI,
					'UR BEFORE RETRO' => $row->SI_UPDATE_UR_BEFORE_RETRO,
					'UR AFTER RETRO' => $row->SI_UPDATE_UR_AFTER_RETRO,
					'SI REFERENCE AND RESULT FOR NSCP7 COMPLIANCE' => $row->SI_UPDATE_SI_REFERENCE_AND_RESULT_FOR_NSCP7_COMPLIANCE,
					'NSCP7 COMPLIANCE BASED ON LATEST SI' => $row->SI_UPDATE_NSCP7_COMPLIANCE_BASED_ON_LATEST_SI,
					'REMARKS' => $row->SI_UPDATE_REMARKS,
				);

				$data['retro'] = array(
					'RETRO STATUS' => $row->RETRO_UPDATE_RETRO_STATUS,
					'LATEST RETRO_ WNER RETRO DONE ONGOING RETRO' => $row->RETRO_UPDATE_LATEST_RETRO_OWNER_RETRO_DONE_ONGOING_RETRO,
					'RETRO STATUS' => $row->RETRO_UPDATE_RETRO_STATUS,
					'NO OF RETRO DONE ON SITE' => $row->RETRO_UPDATE_NO_OF_RETRO_DONE_ON_SITE,
					'DATE OF LAST RETROFITTING DONE ON SITE' => $row->RETRO_UPDATE_DATE_OF_LAST_RETROFITTING_DONE_ON_SITE,
					'LATEST NSCP CODE USED FOR RETRO' => $row->RETRO_UPDATE_LATEST_NSCP_CODE_USED_FOR_RETRO,
					'APPROVAL DATE OF SI REFERENCE USED FOR RETRO' => $row->RETRO_UPDATE_APPROVAL_DATE_OF_SI_REFERENCE_USED_FOR_RETRO,
					'REMARKS' => $row->RETRO_UPDATE_REMARKS,
					'NSCP7 COMPLIANCE' => $row->RETRO_UPDATE_NSCP7_COMPLIANCE,
				);

				$data['tc'] = array(
					'SITES FOR TOWER CONVERSION' => $row->TOWER_CONVERSION_SITES_FOR_TOWER_CONVERSION,
					'REMARKS' => $row->TOWER_CONVERSION_REMARKS,
					'OPTIMIZATION RESULT' => $row->TOWER_CONVERSION_OPTIMIZATION_RESULT,
					'FINAL REMARKS' => $row->TOWER_CONVERSION_FINAL_REMARKS,
				);

				$data['nscp'] = array(
					'SUMMARY_ONGOING_TOWER_STATUS' => $row->NSCP7_COMPLIANCE_SUMMARY_ONGOING_TOWER_STATUS,
					'SI AND RETRO NSCP7 COMPLIANCE REMARKS' => $row->NSCP7_COMPLIANCE_SUMMARY_SI_AND_RETRO_NSCP7_COMPLIANCE_REMARKS,
					'SUMMARY LATEST TOWER STATUS SI AND RETRO' => $row->NSCP7_COMPLIANCE_SUMMARY_LATEST_TOWER_STATUS_SI_AND_RETRO,
					'SUMMARY NSCP7 COMPLIANCE GOVERNED_BY' => $row->NSCP7_COMPLIANCE_SUMMARY_NSCP7_COMPLIANCE_GOVERNED_BY,
					'SUMMARY CAPACITY REMARKS' => $row->NSCP7_COMPLIANCE_SUMMARY_CAPACITY_REMARKS,
					'SUMMARY W/ TOWER UPGRADE DONE ALREADY' => $row->NSCP7_COMPLIANCE_SUMMARY_W_TOWER_UPGRADE_DONE_ALREADY,
					'MILESTONE SUMMARY UPDATE BASED ON LATEST RETRO SI TSSR PTA' => $row->MILESTONE_SUMMARY_UPDATE_BASED_ON_LATEST_RETRO_SI_TSSR_PTA
				);

				$data['tu'] = array(
					'FE TOWER REMARKS' => $row->TOWER_UPDATE_REMARKS_FE_TOWER_REMARKS,
					'SITES REQUIRING PTA UPDATING VERIFICATION' => $row->TOWER_UPDATE_REMARKS_SITES_REQUIRING_PTA_UPDATING_VERIFICATION,
					'REMARKS RFE TOWER REMARKS' => $row->TOWER_UPDATE_REMARKS_RFE_TOWER_REMARKS,
				);

				$data['cabin'] = array(
					'TYPHOON PATH' => $row->CABIN_ODU_AND_GENSET_PAD_TYPHOON_PATH,
					'CABIN TYPE' => $row->CABIN_ODU_AND_GENSET_PAD_CABIN_TYPE,
					'W/ ODU PAD' => $row->CABIN_ODU_AND_GENSET_PAD_W_ODU_PAD,
					'NO OF ODU PAD' => $row->CABIN_ODU_AND_GENSET_PAD_NO_OF_ODU_PAD,
					'NO OF EXISITNG CABINET' => $row->CABIN_ODU_AND_GENSET_PAD_NO_OF_EXISITNG_CABINET,
					'NO OF AVAILABLE CABINET SPACE SPARE' => $row->CABIN_ODU_AND_GENSET_PAD_NO_OF_AVAILABLE_CABINET_SPACE_SPARE,
					'W/ GENSET PAD' => $row->CABIN_ODU_AND_GENSET_PAD_W_GENSET_PAD,
				);

				$data['amb'] = array(
					'LEG TYPE FOR AMB ALLOCATION' => $row->AMB_SOLUTIONS_LEG_TYPE_FOR_AMB_ALLOCATION,
					'TYPE OF AMB INSTALLED ON TOWER' => $row->AMB_SOLUTIONS_TYPE_OF_AMB_INSTALLED_ON_TOWER,
					'W/ GANTRY' => $row->AMB_SOLUTIONS_W_GANTRY,
					'NO OF ANTENNA INSTALLED AT GANTRY' => $row->AMB_SOLUTIONS_NO_OF_ANTENNA_INSTALLED_AT_GANTRY,
					'NO OF SPARE AT GANTRY' => $row->AMB_SOLUTIONS_NO_OF_SPARE_AT_GANTRY,
				);

				$data['pat'] = array(
					'BUILD SCOPE' => $row->TOWER_BUILD_SCOPE,
					'BUILD REMARKS' => $row->BUILD_REMARKS,
					'BUILD COMPLETION' => $row->BUILD_COMPLETION,
				);
			}	
		}	

		#2G
		$this->db->where('PLAID', $plaid);
		$g2 = $this->db->get($this->tbl2g);

		#3G
		$this->db->where('PLAID', $plaid);
		$g3 = $this->db->get($this->tbl3g);

		#4G
		$this->db->where('PLAID', $plaid);
		$g4 = $this->db->get($this->tbl4g);

		#5G
		$this->db->where('PLAID', $plaid);
		$g5 = $this->db->get($this->tbl5g);

		$darr = [];
		$stats = 0;
		if ($g2->num_rows() > 0) {
			$res = $g2->result();
			$stats = 2;
		} else if ($g3->num_rows() > 0) {
			$res = $g3->result();
			$stats = 3;	
		} else if ($g4->num_rows() > 0) {
			$res = $g4->result();
			$stats = 4;
		} else {
			$res = $g5->result();
			$stats = 5;
		}

		foreach($res as $row){
			$darr = array(
				'SITENAME' => $stats == 5 ? $row->NR_Name : ($stats == 2 ? $row->BTS_NAME : $row->SITE_NAME),
				'PLAID' => '',
				'Site_Solution' => $row->Site_Solution,
				'CELL_COVERAGE_TYPE' => $row->CELL_COVERAGE_TYPE,
				'BARANGAY' => $row->BARANGAY,
				'TOWN' => $row->TOWN,
				'PROVINCE' => $row->PROVINCE,
				'REGIONAL_AREA' => $row->REGIONAL_AREA,
				'VENDOR' => $row->VENDOR,
				'STATUS' => $row->STATUS,
				'LATITUDE' => $row->LATITUDE,
				'LONGITUDE' => $row->LONGITUDE
			);
		}
		
		
		return array(
			'fe' => $data,
			'tech' => array(
				'prof' => $darr,
				'g2' => $g2->result(),
				'g3' => $g3->result(),
				'g4' => $g4->result(),
				'g5' => $g5->result()
			)
		);
	}

	public function get_tower_reports()
	{
		return array();
		// $data = array();
		// $total_tcpp = 0;
		// $total_tlss = 0;
		// $total_tg = 0;

		// $total_ttcp = 0;
		// $total_tl = 0;
		// $total_tgp = 0;
		// $total_tbu = 0;

		// $total_sub_pps = 0;
		// $total_sub_tps = 0;
		// $total_total = 0;

		// foreach ($this->regions as $keys => $values) {
		// 	#TAP TO COMMERCIAL POWER PROVIDER
		// 	$this->db->where('POWER_CONNECTION', 'Sole Transformer');
		// 	$this->db->or_where('POWER_CONNECTION', 'Shared Transformer');
		// 	$tcpp = $this->db->get('powerdb_'.$values)->num_rows();

		// 	#TAP TO LESSOR VIA SPCA/Sub-Meter
		// 	$this->db->where('POWER_CONNECTION', 'Tapped to Building');
		// 	$tlss = $this->db->get('powerdb_'.$values)->num_rows();

		// 	#TAP TO GENSET BY DESIGN
		// 	$this->db->where('POWER_CONNECTION', 'Genset Prime');
		// 	$tg = $this->db->get('powerdb_'.$values)->num_rows();

		// 	#TEMPO TAP TO COMMERCIAL POWER
		// 	$this->db->where('POWER_CONNECTION', 'Tempo to Coop');
		// 	$ttcpp = $this->db->get('powerdb_'.$values)->num_rows();

		// 	#TAP TO LESSOR
		// 	$this->db->where('POWER_CONNECTION', 'Tapped to Neighbor');
		// 	$tl = $this->db->get('powerdb_'.$values)->num_rows();

		// 	#TAP TO GENSET
		// 	$this->db->where('POWER_CONNECTION', 'Tapped to Genset');
		// 	$tgp = $this->db->get('powerdb_'.$values)->num_rows();

		// 	#TO BE UPDATED(PROGRESSIVE)
		// 	$this->db->where('POWER_CONNECTION', 'To be updated');
		// 	$tbu = $this->db->get('powerdb_'.$values)->num_rows();

		// 	$total_tcpp += $tcpp;
		// 	$total_tlss += $tlss; 
		// 	$total_tg   += $tg;

		// 	$total_ttcp += $ttcpp;
		// 	$total_tl += $tl;
		// 	$total_tgp += $tgp;
		// 	$total_tbu += $tbu;

		// 	$sub_pps = $tcpp + $tlss + $tg;
		// 	$sub_tps = $ttcpp + $tl + $tgp;

		// 	$total_sub_pps += $sub_pps;
		// 	$total_sub_tps += $sub_tps;

		// 	$total_total += $sub_pps + $sub_tps + $tbu;

		// 	$data[$values] = array(
		// 			'tcpp' => $tcpp,
		// 			'tlss' => $tlss,
		// 			'tg' => $tg,
		// 			'sub_pps' => $sub_pps,

		// 			'ttcp' => $ttcpp,
		// 			'tl' => $tl,
		// 			'tgp' => $tgp,
		// 			'sub_tps' => $sub_tps,
	
		// 			'tbu' => $tbu,
		// 			'total' => $sub_pps + $sub_tps + $tbu
					
		// 	);

		// }

		// return array(
		// 	'region' => $data,
		// 	'total_tccp' => $total_tcpp,
		// 	'total_tlss' => $total_tlss,
		// 	'total_tg' => $total_tg,
		// 	'total_ttcp' => $total_ttcp,
		// 	'total_tl' => $total_tl,
		// 	'total_tgp' => $total_tgp,
		// 	'total_sub_pps' => $total_sub_pps,
		// 	'total_sub_tps' => $total_sub_tps,
		// 	'total_tbu' => $total_tbu,
		// 	'total_total' => $total_total
		// );
	}
}
