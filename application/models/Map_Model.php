<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function generate_kml($data_array,$config,$option,$info)
	{
		$body = "";
		$style = "";
		$duplicate_catcher ="";
		switch ($option)
		{
			case 'indoor':
			case 'outdoor':
				$i=0;
				//$data = json_decode(json_encode($data_array),true);
				foreach ($data_array as $value)
				{
					if($duplicate_catcher != $config[$i]['model'])
					{
						$style .= $this->kml_polygon_style($config[$i]);
						$duplicate_catcher = $config[$i]['model'];
					}
					$i++;
				}

				foreach ($data_array as $value)
				{
					foreach ($value as $key)
					{

						$conf = $this->configFinder($config,$key->tech,'model');
						$body .= $this->kml_body($key,$conf,$option,$info);
					}
				}

				break;
			//insert loop here to remove the duplicate of config for polygon

			case 'site'://FIX HERE
				$i=0;
				//fix error if config is empty
				if($config==null)
				{
					$style ='';
					$body ='';
				}
				else
				{
					$style .= $this->kml_pin_style($config[0]);
					$lists = array();
					foreach ($data_array as $value)
					{
						array_push($lists, $value[0]->sitename);
					}
					$new_lists = array_unique($lists);
					foreach ($data_array as $value)
					{
						foreach ($new_lists as $key)
						{
							if($key==$value[0]->sitename)
							{
								$body .= $this->kml_body($value,$config[0],$option,$info);
								array_shift($new_lists);
								break;
							}
						}
					}
				}
				break;
		}

		return $this->kml_assembly($this->kml_head($option),$style,$body,$this->kml_footer());

	}

	function kml_head($title)
	{
		return '<?xml version="1.0" encoding="UTF-8"?>
				<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
				<Document>
					<name>'.$title.'</name>
						<open>1</open>';
	}

	function kml_pin_style($config)
	{
		$new_color = str_replace("#","",(string)$config['color']);
		$new_color = str_split($new_color, 2);
		$new_solid_color = 'FF'.$new_color[2].$new_color[1].$new_color[0];
		$icon = 'circle';
		return '<Style id="s_ylw-'.$icon.$new_solid_color.'">
					<IconStyle>
						<color>'.$new_solid_color.'</color>
						<scale>1</scale>
						<Icon>
							<href>http://maps.google.com/mapfiles/kml/shapes/placemark_'.$icon.'.png</href>
						</Icon>
					</IconStyle>
					<ListStyle>
					</ListStyle>
				</Style>
				<Style id="s_ylw-'.$icon.$new_solid_color.'">
					<IconStyle>
						<color>'.$new_solid_color.'</color>
						<scale>1</scale>
						<Icon>
							<href>http://maps.google.com/mapfiles/kml/shapes/placemark_'.$icon.'.png</href>
						</Icon>
					</IconStyle>
					<ListStyle>
					</ListStyle>
				</Style>
				<StyleMap id="m_ylw-'.$icon.$new_solid_color.'">
					<Pair>
						<key>normal</key>
						<styleUrl>#s_ylw-'.$icon.$new_solid_color.'</styleUrl>
					</Pair>
					<Pair>
						<key>highlight</key>
						<styleUrl>#s_ylw-'.$icon.$new_solid_color.'</styleUrl>
					</Pair>
				</StyleMap>
				<LookAt>
					<longitude>124.2700907492825</longitude>
					<latitude>11.7707669046371</latitude>
					<altitude>0</altitude>
					<heading>0.09848382966134732</heading>
					<tilt>0</tilt>
					<range>2176402.502001758</range>
					<gx:altitudeMode>relativeToSeaFloor</gx:altitudeMode>
				</LookAt>';
	}

	function kml_polygon_style($config)
	{
		$new_color = str_replace("#","",(string)$config['color']);
		$new_color = str_split($new_color, 2);
		$new_solid_color = 'FF'.$new_color[2].$new_color[1].$new_color[0];
		return '
			<Style id="inline1-'.$config['model'].$new_solid_color.'">
				<LineStyle>
					<color>ff000000</color>
					<width>2</width>
				</LineStyle>
				<PolyStyle>
					<color>'.$new_solid_color.'</color>
				</PolyStyle>
			</Style>
			<StyleMap id="inline-'.$config['model'].$new_solid_color.'">
				<Pair>
					<key>normal</key>
					<styleUrl>#inline0-'.$config['model'].$new_solid_color.'</styleUrl>
				</Pair>
				<Pair>
					<key>highlight</key>
					<styleUrl>#inline1-'.$config['model'].$new_solid_color.'</styleUrl>
				</Pair>
			</StyleMap>
			<Style id="inline0-'.$config['model'].$new_solid_color.'">
				<LineStyle>
					<color>ff000000</color>
					<width>2</width>
				</LineStyle>
				<PolyStyle>
					<color>'.$new_solid_color.'</color>
				</PolyStyle>
			</Style>'
		;
	}

	function kml_body($detail, $config, $option, $info)
	{
		$icon = 'circle';
		$placemark ='';
		$altitude = array();
		switch ($option)
		{
			case 'indoor':
				$altitude = array(
					'g900'	=> 29,
					'g1800'	=> 29,
					'u900'	=> 29,
					'u2100' => 29,
					'l700'	=> 29,
					'l1800' => 29,
					'l2300' => 29,
					'l2600' => 29,
				);
				break;
			case 'outdoor':
				$altitude = array(
					'g900'	=> 21,
					'g1800'	=> 22,
					'u900'	=> 23,
					'u2100' => 24,
					'l700'	=> 25,
					'l1800' => 26,
					'l2300' => 27,
					'l2600' => 28,
				);
				break;
			default:
				# code...
				break;
		}
		switch ($option) {
			case 'indoor':
			case 'outdoor':
				switch ($info)
				{
					case 'true':

						//foreach ($details as $detail)
						//{
							$new_color = str_replace("#","",(string)$config['color']);
							$new_color = str_split($new_color, 2);
							$new_solid_color = 'FF'.$new_color[2].$new_color[1].$new_color[0];
							$description = $this->kml_description($detail);
							//$infodetail = json_decode(json_encode($detail),true);
							$coordinates = $this->draw_coordinates($detail->latitude,$detail->longitude,$detail->azimuth,$altitude[strtolower($detail->tech)],$option,$config['radius'],$detail->petal,$detail->cellname);
							$placemark .='<Placemark>
											<name>'.$detail->cellname.'</name>
											<description>'.$description.'</description>
											<styleUrl>#inline-'.$config['model'].$new_solid_color.'</styleUrl>
											<Polygon>
												<tessellate>1</tessellate>
												<altitudeMode>relativeToGround</altitudeMode>
												<outerBoundaryIs>
													<LinearRing>
														<coordinates>
															'.$coordinates.'
														</coordinates>
													</LinearRing>
												</outerBoundaryIs>
											</Polygon>
										</Placemark>';
						break;
					case 'false':
							$new_color = str_replace("#","",(string)$config['color']);
							$new_color = str_split($new_color, 2);
							$new_solid_color = 'FF'.$new_color[2].$new_color[1].$new_color[0];
							//$infodetail = json_decode(json_encode($detail),true);
							$coordinates = $this->draw_coordinates($detail->latitude,$detail->longitude,$detail->azimuth,$altitude[strtolower($detail->tech)],$option,$config['radius'],$detail->petal,$detail->cellname);
							$placemark .='<Placemark>
											<name>'.$detail->cellname.'</name>
											<styleUrl>#inline-'.$config['model'].$new_solid_color.'</styleUrl>
											<Polygon>
												<tessellate>1</tessellate>
												<altitudeMode>relativeToGround</altitudeMode>
												<outerBoundaryIs>
													<LinearRing>
														<coordinates>
															'.$coordinates.'
														</coordinates>
													</LinearRing>
												</outerBoundaryIs>
											</Polygon>
										</Placemark>';
						break;
					default:
						break;
				}
				break;

			case 'site':
				switch ($info)
				{
					case 'true':
							$new_color = str_replace("#","",(string)$config['color']);
							$new_color = str_split($new_color, 2);
							$new_solid_color = 'FF'.$new_color[2].$new_color[1].$new_color[0];

							$description = $this->kml_description($detail[0]);
							$coordinates = $detail[0]->longitude.' '.$detail[0]->latitude.', 10';
							$placemark .='<Placemark>
											<name>'.$detail[0]->sitename.'</name>
											<description>'.$description.'</description>
											<styleUrl>#m_ylw-'.$icon.$new_solid_color.'</styleUrl>
											<Point>
												<altitudeMode>relativeToGround</altitudeMode>
												<coordinates>'.$coordinates.'</coordinates>
											</Point>
										</Placemark>';
						break;
					case 'false':

							$new_color = str_replace("#","",(string)$config['color']);
							$new_color = str_split($new_color, 2);
							$new_solid_color = 'FF'.$new_color[2].$new_color[1].$new_color[0];

							$coordinates = $detail[0]->longitude.' '.$detail[0]->latitude.', 10';
							$placemark .='<Placemark>
											<name>'.$detail[0]->sitename.'</name>
											<styleUrl>#m_ylw-'.$icon.$new_solid_color.'</styleUrl>
											<Point>
												<altitudeMode>relativeToGround</altitudeMode>
												<coordinates>'.$coordinates.'</coordinates>
											</Point>
										</Placemark>';
						break;
					default:
						break;
				}
			default:
				break;
		}



		 return $placemark;
	}

	function kml_footer()
	{
		return '</Document></kml>';
	}

	function kml_description($detail)
	{
		$body='';
		foreach ($detail as $info => $value)
		{
			$body .= '<tr><td>'.$info.'</td><td>'.$value.'</td></tr>';
		}
		return '<![CDATA[<table border="1" padding="0">'.$body.'</table>]]>';
	}

	function kml_assembly($head,$style,$body,$footer)
	{
		return $head.$style.$body.$footer;
	}

	function draw_coordinates($latitude,$longitude,$azimuth,$altitude,$option,$distance,$petal,$cellname)
	{
		$distance = round($distance/1000,2);
		$lobe_inner;
		$lobe_outer;
		switch ($option)
		{

			case 'outdoor':
				switch (strtolower($petal))
				{
					case 'yes':
						$lobe_inner = 7.5;
						$lobe_outer = 15;
						break;
					case 'no' :
						$lobe_inner = 15;
						$lobe_outer = 30;
						break;
				}
				if(strpos($cellname, '-R')!== false){
					$azimuth = $azimuth + 30;
				}
				if(strpos($cellname, '-L')!== false){
					$azimuth = $azimuth - 30;
				}
				$point = $longitude.','.$latitude.','.$altitude;
				$left30 = $this->get_gps_distance($latitude,$longitude,$distance,$this->aziLobeLeft($azimuth, $lobe_outer),$altitude);
				$left15 = $this->get_gps_distance($latitude,$longitude,$distance,$this->aziLobeLeft($azimuth, $lobe_inner),$altitude);
				$middle = $this->get_gps_distance($latitude,$longitude,$distance,$azimuth,$altitude);
				$right15 = $this->get_gps_distance($latitude,$longitude,$distance,$this->aziLobeRight($azimuth, $lobe_inner),$altitude);
				$right30 = $this->get_gps_distance($latitude,$longitude,$distance,$this->aziLobeRight($azimuth, $lobe_outer),$altitude);
				return $point.' '.$left30.' '.$left15.' '.$middle.' '.$right15.' '.$right30.' '.$point;
				break;
			case 'indoor':
				$azimuth = 0;
				$turnleft = '';
				$start = $this->get_gps_distance($latitude,$longitude,$distance,$azimuth,$altitude);
				for ($i=0; $i < 24 ; $i++)
				{
					$turnleft .= $this->get_gps_distance($latitude,$longitude,$distance,$this->aziLobeLeft($azimuth, 15*$i),$altitude).' ';
				}
				$end = $this->get_gps_distance($latitude,$longitude,$distance,$azimuth,$altitude);
				return $start.' '.$turnleft.$end;
				break;
			default:
				# code...
				break;
		}
	}

	function get_gps_distance($latitude1,$longitude1,$distance,$angle,$height)
	{
	    # Earth Radious in KM
	    $R = 6378.14;

	    # Degree to Radian
	    $latitude1 = $latitude1 * (M_PI/180);
	    $longitude1 = $longitude1 * (M_PI/180);
	    $brng = $angle * (M_PI/180);

	    $latitude2 = asin(sin($latitude1)*cos($distance/$R) + cos($latitude1)*sin($distance/$R)*cos($brng));
	    $longitude2 = $longitude1 + atan2(sin($brng)*sin($distance/$R)*cos($latitude1),cos($distance/$R)-sin($latitude1)*sin($latitude2));

	    # back to degrees
	    $latitude2 = $latitude2 * (180/M_PI);
	    $longitude2 = $longitude2 * (180/M_PI);

	    # 6 decimal for Leaflet and other system compatibility
	   $lat2 = round($latitude2,6);
	   $long2 = round($longitude2,6);

	   return $long2.','.$lat2.','.$height;
	 }

	function distance($latitude1, $longitude1, $latitude2, $longitude2, $unit)
	{
		$theta = $longitude1 - $longitude2;
		$dist = sin(deg2rad($latitude1)) * sin(deg2rad($latitude2)) +  cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);

	  	if ($unit == "K")
	  	{
	    	return ($miles * 1.609344);
	  	}
	  	else if ($unit == "N")
	  	{
	    	return ($miles * 0.8684);
	    }
	    else
	    {
	    	return $miles;
	    }
	}

	function aziLobeLeft($azimuth, $degrees)
	{
		$azifinal = $azimuth - $degrees;
		if($azifinal>0)
		{
			$azifinal = $azifinal+360;
		}
		return $azifinal;
	}

	function aziLobeRight($azimuth, $degrees)
	{
		$azifinal = $azimuth + $degrees;
		if($azifinal>359)
		{
			$azifinal = $azifinal-360;
		}
		return $azifinal;
	}

	function configFinder($config,$str,$find)
	{
		foreach ($config as $key)
		{
			if($str == $key[$find])
			{
				return $key;
			}
		}
	}

}

/* End of file Map_Model.php */
/* Location: ./application/models/Map_Model.php */
