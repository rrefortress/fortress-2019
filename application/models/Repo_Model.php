<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Repo_Model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		ini_set( 'memory_limit', '1G' );
		ini_set('upload_max_filesize', '1G');
		ini_set('post_max_size', '1G');
		ini_set('max_input_time', 600000);
		ini_set('max_execution_time', 600000);

	}

	public function set_config($total = 0, $func_name = '', $row_page = 0)
	{
		$config['base_url'] = base_url($func_name);
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $total;
		$config['per_page'] = $row_page;

		$config['full_tag_open'] = '<ul style="height: 0;" class="mg-top-5 pagination small text-center paginate_track">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['next_tag_close'] = '<span aria-hidden="true"></span></span></li>';
		$config['prev_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['prev_tag_close'] = '</span></li>';
		$config['first_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['first_tag_close'] = '</span></li>';
		$config['last_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['last_tag_close'] = '</span></li>';

		// Initialize
		$this->pagination->initialize($config);
		return $config;
	}

	public function get_sites($row_num = 0)
	{
		$row_page = $this->input->post('row_page');
		$geoarea = strtoupper($geo = $this->input->post('geo'));
		$this->db->where('REGION', $geoarea);
		$res = $this->db->get('repo_names');

		if ($row_num != 0) {
			$row_num = ($row_num - 1) * $row_page;
		}

		$this->set_config($res->num_rows(), $func_name = 'get_sites', $row_page);

		$query = $this->get_details($row_num, $row_page);

		return $arrays = array(
			'result' => $query,
			'pagination' => $this->pagination->create_links(),
			'row_num' => $row_num,
			'total' => $res->num_rows()
		);
	}

	public function get_details($row_num = 0, $row_page = 0)
	{
		$geo = $this->input->post('geo');
		$search = $this->input->post('search');
		$sort = $this->input->post('sort');
		$this->db->select('a.PLAID, a.SITENAME, COUNT(b.id) as files');
		$this->db->from('repo_names a');
		$this->db->limit($row_page, $row_num);
		$this->db->where('a.REGION', $geo);
		$this->db->where("(a.PLAID LIKE '%$search%'");
		$this->db->or_where("a.SITENAME LIKE '%$search%')");
		$this->db->order_by("a.SITENAME", $sort);
		$this->db->group_by('a.SITENAME');
		$this->db->join('repo_files b', 'b.SITENAME = a.SITENAME', 'left');
		$res = $this->db->get();
		return $res->result();
	}

	public function getrecordCount() {
		$query = $this->db->get('repo_names');
		$result = $query->num_rows();
		return $result;
	}

	public function repo_upload() {
		$geoarea = strtoupper($this->session->userdata('geoarea'));
		$site_name = $this->input->post('site_name');

		#FILE NAME
		$name = trim($_FILES['repo_file']['name']);
		#GET EXTENSION OF FILE
		$array = explode('.', $name);
		$extension = end($array);

		#GET 0 - PLAID | 1 - SITE | 2 - PLANS
		$pieces = explode("_", $name);

		#CHECK IF SESSION EXPIRE
		if(empty($geoarea)) {redirect(base_url('login'));}

		#CHECK IF EMPTY SITE NAME
		if (!empty($site_name)) {
			#CHECK IF FILE IS EXACT IN SITE NAME FOLDER
			if ($site_name <> trim($pieces[1])) {
				return array(
					'type' 	=> 'error',
					'title' => 'Invalid!',
					'msg' 	=> 'This file/s does not belong to this folder.'
				);
			}
		}

		#GET PLANS
		$plans = explode(".", trim($pieces[2]));
		$content_folder_name = trim(strtoupper($plans[0]));
		#IDENTIFY TYPE IF ENGINEERING PLANS OR IMPLEMENTATION
		$folder_name = '';
		switch ($content_folder_name) {
			case 'APPROVED-DDD':
			case 'APPROVED-DDD(NEW-SITE)':
			case 'APPROVED-DDD(AC-UPGRADE)':
			case 'APPROVED-DDD(TOWER-RETRO)':
			case 'APPROVED-TOWER-RETRO':
			case 'APPROVED-TSSR(EXPANSION)':
				$folder_name = 'ENGINEERING PLANS';
				break;
			case 'AS-BUILT-PLANS':
			case 'AS-BUILT-PLANS(NEW-SITE)':
			case 'AS-BUILT-PLANS(AC-UPGRADE)':
			case 'AS-BUILT-PLANS(TOWER-RETRO)':
			case 'ACCEPTANCE-CHECKLIST':
			case 'ACCEPTANCE-CHECKLIST(NEW-SITE)':
			case 'ACCEPTANCE-CHECKLIST(AC-UPGRADE)':
			case 'ACCEPTANCE-CHECKLIST(TOWER-RETRO)':
				$folder_name = 'IMPLEMENTATION';
				break;
		}

		#CHECK IF FILE PASS ON FILE CONVENTION STANDARDS
		if (!empty($folder_name)) {
		#CHECK IF FILE EXISTS
		$this->db->where('SITENAME', trim($pieces[1]));
		$this->db->where('PLAID', trim($pieces[0]));
		$site = $this->db->get('repo_names');
		#IF 0 INSERT : NOT
		if ($site->num_rows() == 0) {
			$data = array(
				'REGION' => $geoarea,
				'SITENAME' => $pieces[1],
				'PLAID' => $pieces[0]
			);
			$this->db->insert('repo_names', $data);
		}

		#MAKE DIRECTORY FOR SITES
		$config['upload_path'] = FCPATH.'/Repo_files/'.$geoarea.'/'.$pieces[1];
		$config['allowed_types'] = 'pdf|dwg';
		$this->load->library('upload',$config);
		if(!file_exists($config['upload_path'])) {
			mkdir($config['upload_path'],0777, TRUE);
		}

			#INSERT IN DIRECTORY PER SITE
			if($this->upload->do_upload('repo_file')) {
				#INSERT INTO DIRECTORY FILE
				$filename = $this->upload->data('file_name');
				$size = filesize($config['upload_path'].'/'.$filename);
				#INSERT DATA INTO DB
					$data = array(
						'REGION' => $geoarea,
						'SITENAME' => $pieces[1],
						'folder_name' => $folder_name,
						'content_folder_name' => $content_folder_name,
						'file_name' => $filename,
						'file_size' => $size,
						'file_type' => $extension,
					);
					$this->db->set('date_uploaded', 'NOW()', FALSE);
					$this->db->insert('repo_files', $data);

					$json = array(
						'type' 	=> 'success',
						'title' => 'Success!',
						'msg' 	=> 'File successfully uploaded.'
					);
			}
		}

		return $json;
	}

	public function get_content() {
		$folder_name = $this->input->post('folder_name');
		$sitename = $this->input->post('sitename');
		$content_folder = $this->input->post('filter');
		$file_type = $this->input->post('filetype');
		#CHECK IF EMPTY FILE TYPE RESULT
		if (!empty($file_type)) {$this->db->where('file_type', $file_type);}
		#CHECK IF EMPTY CONTENT FOLDER RESULT
		if (!empty($content_folder)) {$this->db->where("content_folder_name LIKE '%$content_folder%'");}
		$this->db->where('SITENAME', $sitename);
		$this->db->where('folder_name', $folder_name);
		$res = $this->db->get('repo_files');
		$data = array();
		if ($res->num_rows() > 0) {
			foreach ($res->result() as $row) {
				$data[] = array(
					'id' => $row->id,
					'file_name' => $row->file_name,
					'file_size' => $this->filesize_formatted($row->file_size),
					'date_upload' => date("M-d-Y", strtotime($row->date_uploaded)),
					'file_type' => $row->file_type,
				);
			}
		}

		#TOTAL SIZE
		$this->db->select('COUNT(id) as count, SUM(file_size) as file_size');
		$this->db->from('repo_files');
		$total = $this->db->get()->row();

		return  array(
			'result' => $data,
			'total' => $total->file_size == NULL ? 0 : $this->filesize_formatted($total->file_size),
			'count' => $total->count
		);
	}

	public function get_region_count() {
		#NLZ
		$this->db->where('REGION', 'NLZ');
		$nlz = $this->db->get('repo_names')->num_rows();
		#COUNT TOTAL NLZ SIZE
		$this->db->where('REGION', 'NLZ');
		$this->db->select('COUNT(id) as count, SUM(file_size) as file_size');
		$this->db->from('repo_files');
		$nlz_count = $this->db->get()->row();

		#SLZ
		$this->db->where('REGION', 'SLZ');
		$slz = $this->db->get('repo_names')->num_rows();
		#COUNT TOTAL SLZ SIZE
		$this->db->where('REGION', 'SLZ');
		$this->db->select('COUNT(id) as count, SUM(file_size) as file_size');
		$this->db->from('repo_files');
		$slz_count = $this->db->get()->row();

		#NCR
		$this->db->where('REGION', 'NCR');
		$ncr = $this->db->get('repo_names')->num_rows();
		#COUNT TOTAL NCR SIZE
		$this->db->where('REGION', 'NCR');
		$this->db->select('COUNT(id) as count, SUM(file_size) as file_size');
		$this->db->from('repo_files');
		$ncr_count = $this->db->get()->row();

		#VIS
		$this->db->where('REGION', 'VIS');
		$vis = $this->db->get('repo_names')->num_rows();
		#COUNT TOTAL VIS SIZE
		$this->db->where('REGION', 'VIS');
		$this->db->select('COUNT(id) as count, SUM(file_size) as file_size');
		$this->db->from('repo_files');
		$vis_count = $this->db->get()->row();

		#MIN
		$this->db->where('REGION', 'MIN');
		$min = $this->db->get('repo_names')->num_rows();
		#COUNT TOTAL MIN SIZE
		$this->db->where('REGION', 'MIN');
		$this->db->select('COUNT(id) as count, SUM(file_size) as file_size');
		$this->db->from('repo_files');
		$min_count = $this->db->get()->row();

		#TOTAL SIZE
		$this->db->select('COUNT(id) as count, SUM(file_size) as file_size');
		$this->db->from('repo_files');
		$total = $this->db->get()->row();

		return $data = array(
			'size' => array(
				0 => array(
					'label' => 'SOUTH LUZON',
					'name' => 'SLZ',
					'total' => $slz,
					'size' => $slz_count->file_size == NULL ? 0 : $this->filesize_formatted($slz_count->file_size),
					'count' => $slz_count->count
				),
				1 => array (
					'label' => 'NORTH LUZON',
					'name' => 'NLZ',
					'total' => $nlz,
					'size' => $nlz_count->file_size == NULL ? 0 : $this->filesize_formatted($nlz_count->file_size),
					'count' => $nlz_count->count
				),
				2 => array(
					'label' => 'NATIONAL CAPITAL REGION',
					'name' => 'NCR',
					'total' => $ncr,
					'size' => $ncr_count->file_size == NULL ? 0 : $this->filesize_formatted($ncr_count->file_size),
					'count' => $ncr_count->count
				),
				3 => array(
					'label' => 'VISAYAS',
					'name' => 'VIS',
					'total' => $vis,
					'size' => $vis_count->file_size == NULL ? 0 : $this->filesize_formatted($vis_count->file_size),
					'count' => $vis_count->count
				),
				4 => array(
					'label' => 'MINDANAO',
					'name' => 'MIN',
					'total' => $min,
					'size' => $min_count->file_size == NULL ? 0 : $this->filesize_formatted($min_count->file_size),
					'count' => $min_count->count
				),
			),
			'total' => $total->file_size == NULL ? 0 : $this->filesize_formatted($total->file_size),
			'count' => $total->count
		);
	}

	public function delete_content() {
		$id = $this->input->post('id');
		$geoarea = strtoupper($this->session->userdata('geoarea'));
		if(empty($geoarea)) {redirect(base_url('login'));}
		#CHECK IF FILE EXISTS
		$check = $this->db->get_where('repo_files',array('id'=>$id));

		if ($check->num_rows() > 0) {
			$row = $check->row();
			#DELETE FILE ON DIRECTORY
			$path = FCPATH.'/Repo_files/'.$geoarea.'/'.$row->SITENAME.'/'.$row->file_name;
			if(file_exists($path)){
				unlink($path);
			}
			$this->db->where('id', $id);
			$res = $this->db->delete('repo_files');
		}

		return $res;
	}

	public function filesize_formatted($size) {
		$units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
		$power = $size > 0 ? floor(log($size, 1024)) : 0;
		return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
	}

	public function get_file_count() {
		$site_name = $this->input->post('site_name');

		#ENGINEERING PLANS
		$ep = array('SITENAME'=>$site_name, 'folder_name' => 'ENGINEERING PLANS');
		$engr_plans = $this->db->get_where('repo_files', $ep);

		#IMPLEMENTATION
		$im = array('SITENAME'=>$site_name, 'folder_name' => 'IMPLEMENTATION');
		$imple = $this->db->get_where('repo_files', $im);

		#TOTAL SIZE
		$this->db->select('COUNT(id) as count, SUM(file_size) as file_size');
		$this->db->from('repo_files');
		$total = $this->db->get()->row();

		$data = array(
			'result' => array(
				0 => array(
					'name' => 'ENGINEERING PLANS',
					'count' => $engr_plans->num_rows(),
				),
				1 => array(
					'name' => 'IMPLEMENTATION',
					'count' => $imple->num_rows()
				),
			),
			'total' => $total->file_size == NULL ? 0 : $this->filesize_formatted($total->file_size),
			'count' => $total->count
		);
		return $data;
	}

	public function get_repo_chart() {
		#GET SITE FILES
		$files = array();
		$sites = array();
		$ep = 0;
		$im = 0;
		foreach (array('nlz', 'slz', 'ncr', 'vis', 'min') as $key => $geo) {
			#GET FILES
			$this->db->where("REGION", $geo);
			$files[$geo] = $this->db->get('repo_files')->num_rows();
			#SITES
			$this->db->where("REGION", $geo);
			$sites[$geo] = $this->db->get('repo_names')->num_rows();
			#ENGR. PLANS
			$this->db->where("folder_name", "ENGINEERING PLANS");
			$ep += $this->db->get('repo_files')->num_rows();
			#IMPLEMENTATION
			$this->db->where("folder_name", "IMPLEMENTATION");
			$im += $this->db->get('repo_files')->num_rows();
		}
		return array(
			'files' => $files,
			'sites' => $sites,
			'plans' => array(
				'ep' => $ep,
				'im' => $im
			)
		);
	}
}


