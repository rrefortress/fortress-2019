<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory_Model extends CI_Model
{
	# ITEM STATUS VALUE
	#1 - cart, 2 - requests, 3 - accepted, 4 - rejected, 5 - return
	public function save_item($items, $id = 0)
	{
		if (empty($id)) {
	 		$this->db->insert('inventory', $items);
			$id = $this->db->insert_id();
		} else {
			$this->db->where('id', $id);
			$this->db->update('inventory', $items);

			#CLEAR SERIAL DATA
			$this->db->delete('inventory_serial', array('iid' => $id));
		}

		$inputs = array();
		for ($count = 0; $count <	count($_POST["serial"]); $count++) {
			$inputs[] = array(
				'serial_number' => $_POST['serial'][$count],
				'iid'           => $id,
			);
		}

		$this->db->insert_batch('inventory_serial', $inputs);
	}

	public function get_items($search = '') {
		$result = array();
		$search = empty($search) ? '' : " WHERE a.name LIKE '%".$search."%'";
		$data = $this->db->query('SELECT a.*, b.item_status FROM inventory a LEFT JOIN inventory_status b ON a.id = b.iid'.$search);

		// $data = $this->db->get('inventory');
		if ($data->num_rows() > 0) {
			foreach ($data->result() as $row) {
				#CHECK IF ITEM IS NULL OR RETURNED
				if ($row->item_status == NULL || $row->item_status == 5) {
					$result[] = array(
							'id' => $row->id,
							'image' => $row->image,
							'item_code' => $row->item_code,
							'name' => $row->name,
							'quantity' => $row->quantity,
							'date_created' => $row->date_created,
							#GET INVENTORY SERIAL
							'serials' => $this->db->get_where('inventory_serial', array('iid' => $row->id))->result()
					);
				}
			}
		}

		return $result;
	}

	public function del_items($data) {
		$this->db->where_in('iid', $data);
		$this->db->delete('inventory_serial');

		$this->db->where_in('id', $data);
		return $this->db->delete('inventory');
	}

	public function save_to_cart($data) {
		#SAVE CART TO INVENTORY STATUS
		foreach ($data as $row) {
			$this->db->insert('inventory_status', array('uid' => $row['uid'], 'iid' => $row['iid'], 'qty' => $row['qty'], 'item_status' => 1));
		}

		#UPDATE STOCK IN INVENTORY TABLE
		foreach ($data as $row) {
			$this->db->where('id', $row['iid']);
			$this->db->update('inventory', array('quantity' => $row['stocks']));
		}

		$json = array(
			'type' 	=> 'success',
			'title' => 'Success!',
			'msg' 	=> 'Item successfully saved to cart.'
		);
		return $json;
	}

	public function get_item_cart() {
		$data = $this->db->query("SELECT b.id, a.image, a.item_code, a.name, b.qty FROM inventory a JOIN inventory_status b ON a.id = b.iid WHERE b.uid = ? AND item_status = ?" , array($this->session->userdata('id'), 1));
		return $data->result();
	}

	public function save_to_requests($data) {
		#UPDATE ITEM STATUS IN INVENTORY STATUS

		foreach ($data as $row) {
			#UPDATE INVENTORY STATUS TABLE
			$this->db->where('id', $row['id']);
			$this->db->where('uid', $this->session->userdata('id'));
			$this->db->update('inventory_status', array('item_status' => 2, "request_date"  => date('Y-m-d'), 'purpose' => $row['purpose']));
		}

		$json = array(
			'type' 	=> 'success',
			'title' => 'Success!',
			'msg' 	=> 'Your selected item will be sent to process.'
		);
		return $json;
	}

	public function get_requested_lists($search = '', $date = '') {
		$result = array();
		$search = empty($search) ? '' : " AND a.name LIKE '%".$search."%' OR b.item_status = '".$search."'";
		$dates = empty($date) ? '' : " AND b.request_date = '".$date."'";
		$data = $this->db->query("SELECT a.id, a.image, a.item_code, a.name, b.qty, b.request_date, b.item_status FROM inventory a JOIN inventory_status b ON a.id = b.iid WHERE b.uid = ? ".$dates." AND (b.item_status = ? || b.item_status = ? || b.item_status = ?) " . $search . " ORDER BY b.id DESC",
		array($this->session->userdata('id'), 2, 3, 4));

		if ($data->num_rows() > 0) {
			foreach ($data->result() as $row) {
				#CHECK IF ITEM IS NULL OR RETURNED
					$result[] = array(
							'id' => $row->id,
							'image' => $row->image,
							'item_code' => $row->item_code,
							'name' => $row->name,
							'qty' => $row->qty,
							'request_date' => $row->request_date,
							'item_status' => $row->item_status,
							#GET INVENTORY SERIAL
							'serials' => $this->db->get_where('inventory_serial', array('iid' => $row->id))->result()
					);
			}
		}
		return $result;
		// return $data->result();
	}

	public function get_requested_item($search, $date) {
		$search = empty($search) ? '' : " AND b.uid LIKE '%".$search."%' OR a.name LIKE '%".$search."%'";
		$dates = empty($date) ? '' : " AND b.request_date = '".$date."'";
		$data = $this->db->query("SELECT b.id, a.image, a.item_code, a.name, b.qty, b.request_date, b.purpose, b.item_status, c.fname
			FROM inventory a JOIN inventory_status b ON a.id = b.iid JOIN users c ON b.uid = c.id
			WHERE b.item_status = ? " . $search .' '. $dates. " ORDER BY b.request_date DESC",
		array(2));

		return $data->result();
	}

	public function accept_request($data) {
		#UPDATE ITEM STATUS IN INVENTORY STATUS
		foreach ($data as $row) {
			#UPDATE INVENTORY STATUS TABLE
			$this->db->where('id', $row['id']);
			$this->db->update('inventory_status', array('item_status' => 3, "approved_date" => date('Y-m-d')));
		}

		$json = array(
			'type' 	=> 'success',
			'title' => 'Success!',
			'msg' 	=> 'List has been accepted.'
		);
		return $json;
	}

	public function reject_request($data) {
		#UPDATE ITEM STATUS IN INVENTORY STATUS
		foreach ($data as $row) {
			#UPDATE INVENTORY STATUS TABLE
			$this->db->where('id', $row['iid']);
			$this->db->update('inventory_status', array('item_status' => 4, "approved_date" => date('Y-m-d')));
		}

		$json = array(
			'type' 	=> 'success',
			'title' => 'Success!',
			'msg' 	=> 'List has been rejected.'
		);
		return $json;
	}

	public function return_item($data) {
		#UPDATE ITEM STATUS IN INVENTORY STATUS
		foreach ($data as $row) {
			#UPDATE INVENTORY STATUS TABLE
			$this->db->where('iid', $row['iid']);
			$this->db->update('inventory_status', array('item_status' => 5, "return_date" => date('Y-m-d')));

			#CHECK IF ITEM EXISTS
			$this->db->where('id', $row['iid']);
			$chk_exist = $this->db->get('inventory');

			if ($chk_exist->num_rows() > 0) {
					$item = $chk_exist->row();

					#SAVE TO RETURN AND CALCULATE BACK EXACT ITEMS
					$to_return = $row['qty'] + $item->quantity;

					#UPDATE INVENTORY TO RETURN ITEM
					$this->db->where('id', $row['iid']);
					$this->db->update('inventory', array('quantity' => $to_return));
			}
		}

		$json = array(
			'type' 	=> 'success',
			'title' => 'Success!',
			'msg' 	=> 'Items has been returned.'
		);
		return $json;
	}

	public function get_users() {
		$this->db->order_by("fname", "asc");
		$this->db->where('globe_id <>', 0);
		return $this->db->get('users')->result();
	}

	public function remove_cart_lists($data) {
		#CHECK AND CALCULATE THE TOTAL VALUE TO UNDO
		$result = $this->db->query("SELECT a.id, a.quantity + b.qty as num FROM inventory a JOIN inventory_status b ON a.id = b.iid WHERE b.id = ?", array($data))->row();

		#UPDATE TO RETURN EXACT
		$this->db->where('id', $result->id);
		$this->db->update('inventory', array('quantity' => $result->num));

		#REMOVE
		$this->db->where_in('id', $data);
		return $this->db->delete('inventory_status');
	}
}
