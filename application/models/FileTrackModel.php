<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FileTrackModel extends CI_Model
{
	public function insertDataByBatch($data, $table_name)
	{
		#CLEAR DATA BEFORE INSERT
		$this->db->truncate($table_name);
		$this->db->insert_batch($table_name, array_filter($data));
	}

	public function save_nom_track($data, $status)
	{
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'nominations_' . $geoarea;
		if ($status == 0) {
			$status = false;
			foreach ($data as $row) {
				$query = $this->db->get_where($table_name, array('SITE_NAME' => $row['SITE_NAME']));
				if ($query->num_rows() > 0) {
					$status = true;
					break;
				}
			}

			if ($status) {
				$json = array(
					'type' => 'error',
					'title' => 'Invalid!',
					'msg' => 'Duplicate Site Name.'
				);
			} else {
				$this->db->insert_batch($table_name, $data);
				$msg = 'Data Inserted.';

				$json = array(
					'type' => 'success',
					'title' => 'Success!',
					'msg' => $msg,
					'data' => $this->get_details($table_name)
				);
			}
		} else {
			foreach ($data as $row) {
				$this->db->where('id', $row['id']);
				$this->db->set('SITE_NAME', $row['SITE_NAME']);
				$this->db->set('REGION', $row['REGION']);
				$this->db->set('PROVINCE', $row['PROVINCE']);
				$this->db->set('TOWN_CITY', $row['TOWN_CITY']);
				$this->db->set('Latitude', $row['Latitude']);
				$this->db->set('Longitude', $row['Longitude']);
				$this->db->set('Tagging', $row['Tagging']);
				$this->db->set('Site_Type', $row['Site_Type']);
				$this->db->set('Remarks', $row['Remarks']);
				$this->db->update($table_name);
			}
			$msg = 'Data Updated.';

			$json = array(
				'type' => 'success',
				'title' => 'Success!',
				'msg' => $msg
			);
		}
		return $json;
	}

	public function get_details($table_name, $search = '', $row_num = 0, $row_page = 0)
	{
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->like("SITE_NAME", $search);
		$this->db->order_by('id', 'ASC');
		$this->db->limit($row_page, $row_num);
		$query = $this->db->get();
		return $query->result();
	}

	public function save_awe_track($data, $status)
	{
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'awe_' . $geoarea;
		if ($status == 0) {
			$status = false;
			foreach ($data as $row) {
				$query = $this->db->get_where($table_name, array('SITE_NAME' => $row['SITE_NAME']));
				if ($query->num_rows() > 0) {
					$status = true;
					break;
				}
			}

			if ($status) {
				$json = array(
					'type' => 'error',
					'title' => 'Invalid!',
					'msg' => 'Duplicate Site Name.'
				);
			} else {
				$this->db->insert_batch($table_name, $data);
				$msg = 'Data Inserted.';

				$json = array(
					'type' => 'success',
					'title' => 'Success!',
					'msg' => $msg
				);
			}
		} else {
			foreach ($data as $row) {
				$this->db->where('id', $row['id']);
				$this->db->set('SERIAL_NUMBER', $row['SERIAL_NUMBER']);
				$this->db->set('SITE_NAME', $row['SITE_NAME']);
				$this->db->set('REGION', $row['REGION']);
				$this->db->set('PROVINCE', $row['PROVINCE']);
				$this->db->set('Town_Priority', $row['Town_Priority']);
				$this->db->set('Latitude', $row['Latitude']);
				$this->db->set('Longitude', $row['Longitude']);
				$this->db->set('VENDOR', $row['VENDOR']);
				$this->db->set('Program_Tag', $row['Program_Tag']);
				$this->db->set('PLANNED_TECH', $row['PLANNED_TECH']);
				$this->db->set('Tagging', $row['Tagging']);
				$this->db->set('Site_Type', $row['Site_Type']);
				$this->db->update($table_name);
			}
			$msg = 'Data Updated.';

			$json = array(
				'type' => 'success',
				'title' => 'Success!',
				'msg' => $msg
			);
		}
		return $json;
	}

	public function save_pmo_track($data, $status)
	{
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'pmo_' . $geoarea;
		if ($status == 0) {
			$status = false;
			foreach ($data as $row) {
				$query = $this->db->get_where($table_name, array('SITE_NAME' => $row['SITE_NAME']));
				if ($query->num_rows() > 0) {
					$status = true;
					break;
				}
			}

			if ($status) {
				$json = array(
					'type' => 'error',
					'title' => 'Invalid!',
					'msg' => 'Duplicate Site Name.'
				);
			} else {
				$this->db->insert_batch($table_name, $data);
				$msg = 'Data Inserted.';

				$json = array(
					'type' => 'success',
					'title' => 'Success!',
					'msg' => $msg
				);
			}
		} else {
			foreach ($data as $row) {
				$this->db->where('id', $row['id']);
				$this->db->set('SERIAL_NUMBER', $row['Serial_Number']);
				$this->db->set('SITE_NAME', $row['SITE_NAME']);
				$this->db->set('REGION', $row['REGION']);
				$this->db->set('Budget_Tagging', $row['Budget_Tagging']);
				$this->db->set('Program', $row['Program']);
				$this->db->set('Project_Phase', $row['Project_Phase']);
				$this->db->set('Lead_Vendor', $row['Lead_Vendor']);
				$this->db->set('Solution', $row['Solution']);
				$this->db->set('Coverage', $row['Coverage']);
				$this->db->set('Scope', $row['Scope']);
				$this->db->set('Wireless_Plan', $row['Wireless_Plan']);
				$this->db->set('No_of_Sectors', $row['No_of_Sectors']);
				$this->db->update($table_name);
			}
			$msg = 'Data Updated.';

			$json = array(
				'type' => 'success',
				'title' => 'Success!',
				'msg' => $msg
			);
		}
		return $json;
	}

	public function save_survey_track()
	{
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'survey_' . $geoarea;

		$insert = array();
		$update = array();
		$json = array();
		$status = false;

		for ($count = 0; $count < count($_POST["survey_site_name"]); $count++) {
			$id = $_POST['survey_iid'][$count];
			if ($id == 0) {
				$insert[] = array(
					'SITE_NAME' => $_POST['survey_site_name'][$count],
					'Survey_Date' => $_POST['survey_date'][$count],
					'Project' => $_POST['survey_project'][$count],
					'latitude' => $_POST['survey_latitude'][$count],
					'longtitude' => $_POST['survey_longtitude'][$count],
					'RRE_Representative' => $_POST['survey_rre'][$count],
					'RFE_Representative' => $_POST['survey_rfe'][$count],
					'SAQ_Representative' => $_POST['survey_saq'][$count],
					'Category' => $_POST['survey_category'][$count],
					'Vendor' => $_POST['survey_vendor'][$count],
					'Site_Status' => $_POST['survey_status'][$count],
					'Remarks' => $_POST['survey_remarks'][$count]
				);
			} else {
				$update[] = array(
					'SITE_NAME' => $_POST['survey_site_name'][$count],
					'Survey_Date' => $_POST['survey_date'][$count],
					'Project' => $_POST['survey_project'][$count],
					'RRE_Representative' => $_POST['survey_rre'][$count],
					'RFE_Representative' => $_POST['survey_rfe'][$count],
					'SAQ_Representative' => $_POST['survey_saq'][$count],
					'Category' => $_POST['survey_category'][$count],
					'Vendor' => $_POST['survey_vendor'][$count],
					'Site_Status' => $_POST['survey_status'][$count],
					'Remarks' => $_POST['survey_remarks'][$count],
					'id' => $_POST['survey_iid'][$count]
				);
			}
		}

		#CHECK BOTH
		if (!empty($insert) && !empty($update)) {
			foreach ($insert as $row) {
				$this->db->insert_batch($table_name, $insert);
				$msg = 'Data Inserted.';
			}

			foreach ($update as $row) {
				$this->db->where('id <', $row['id']);
				$this->db->where('SITE_NAME', $row['SITE_NAME']);
				$query = $this->db->get($table_name);
				if ($query->num_rows() > 0) {
					$status = true;
					break;
				}

				if ($status) {
					$type = 'error';
					$title = 'Invalid!';
					$msg = 'Duplicate Site Name.';
					goto exit_loop;
				} else {
					$this->db->where('id', $row['id']);
					$this->db->set('SITE_NAME', $row['SITE_NAME']);
					$this->db->set('Survey_Date', $row['Survey_Date']);
					$this->db->set('Project', $row['Project']);
					$this->db->set('RRE_Representative', $row['RRE_Representative']);
					$this->db->set('RFE_Representative', $row['RFE_Representative']);
					$this->db->set('SAQ_Representative', $row['SAQ_Representative']);
					$this->db->set('Category', $row['Category']);
					$this->db->set('Vendor', $row['Vendor']);
					$this->db->set('Site_Status', $row['Site_Status']);
					$this->db->set('Remarks', $row['Remarks']);
					$this->db->update($table_name);
				}
			}

			$type = 'success';
			$title = 'Success!';
			$msg = 'Data is updated while inserted new data.';

			#CHECK IF INSERT
		} else if (!empty($insert)) {
			$this->db->insert_batch($table_name, $insert);
			$type = 'success';
			$title = 'Success!';
			$msg = 'Data Inserted.';
			#CHECK IF UPDATE
		} else if (!empty($update)) {
			foreach ($update as $row) {
				$this->db->where('id', $row['id']);
				$this->db->set('SITE_NAME', $row['SITE_NAME']);
				$this->db->set('Survey_Date', $row['Survey_Date']);
				$this->db->set('Project', $row['Project']);
				$this->db->set('RRE_Representative', $row['RRE_Representative']);
				$this->db->set('RFE_Representative', $row['RFE_Representative']);
				$this->db->set('SAQ_Representative', $row['SAQ_Representative']);
				$this->db->set('Category', $row['Category']);
				$this->db->set('Vendor', $row['Vendor']);
				$this->db->set('Site_Status', $row['Site_Status']);
				$this->db->set('Remarks', $row['Remarks']);
				$this->db->update($table_name);
			}

			$type = 'success';
			$title = 'Success!';
			$msg = 'Data Updated.';
		}

		exit_loop:

		$json = array(
			'type' => $type,
			'title' => $title,
			'msg' => $msg,
		);
		return $json;
	}

	public function remove_track($data, $status, $table_name)
	{
		// if ($status == 'true') {
		// 	#TRUNCATE ALL
		// 	// $this->db->truncate($table_name);
		// } else {
		#DELETE Nomination DATA
		foreach ($data as $row) {
			$this->db->where('id', $row['id']);
			$this->db->delete($table_name);
		}
		// }

		$json = array(
			'type' => 'success',
			'title' => 'Success!',
			'msg' => 'List has been removed.'
		);
		return $json;
	}

	public function get_file_tracking()
	{
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'consolidate_tracker_' . $geoarea;
		#CHECK ACCESS LEVEL
		if ($this->session->userdata('access_level') == 'Admin') {
			$json = array();

			#GET QUERY ACCORDING TO USER TASKS
			$query = $this->db->get_where($table_name, array('Year' => date("Y")));

			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					#GET USER NAME
					$user = $this->db->get_where('users', array('globe_id' => $row->assignee))->row_array();
					$assignee = $user['globe_id'] == 0 ? NULL : $user['fname'];

					$json[] = array(
						'id' => $row->id,
						'globe_id' => $row->assignee,
						'subject_name' => $row->subject_name,
						'date_endorsed' => $row->date_endorsed,
						'date_assessed' => $row->date_assessed,
						'track_name' => $row->track_name,
						'status' => $row->status,
						'remarks' => $row->remarks,
						'fname' => $assignee
					);

				}
			}
			return $json;
		} else {
			$data = $this->db->query("SELECT a.id, a.subject_name, a.date_endorsed, a.date_assessed, a.track_name, a.status, a.remarks, b.fname, b.globe_id FROM " . $table_name . " a JOIN users b WHERE a.assignee = b.globe_id AND b.id = ? AND a.Year =  ? ORDER BY a.date_assessed ASC",
				array($this->session->userdata('id'), date("Y")));
			return $data->result();
		}
	}

	public function get_IBS_Track($filter = '', $search = '', $row_num = 0)
	{
		$row_page = $this->input->get('row_page');

		$filter = empty($filter) ? '' : " AND PROVINCE LIKE '%" . $filter . "%'";
		$array = [];
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'ibs_' . $geoarea;

		$sql = "SELECT count(id) as total,
		(SELECT count(Building_Type) FROM " . $table_name . " WHERE Building_Type = \"AIRPORT\" " . $filter . ") as airport,
		(SELECT count(Building_Type) FROM " . $table_name . " WHERE Building_Type = \"CONDOMINIUM\" " . $filter . ") as condominium,
		(SELECT count(Building_Type) FROM " . $table_name . " WHERE Building_Type = \"CORPORATE OFFICE\" " . $filter . ") as corporate_office,
		(SELECT count(Building_Type) FROM " . $table_name . " WHERE Building_Type = \"GLOBE STORE\" " . $filter . ") as globe_store,
		(SELECT count(Building_Type) FROM " . $table_name . " WHERE Building_Type = \"HOSPITAL\" " . $filter . ") as hospital,
		(SELECT count(Building_Type) FROM " . $table_name . " WHERE Building_Type = \"HOTEL\" " . $filter . ") as hotel,
		(SELECT count(Building_Type) FROM " . $table_name . " WHERE Building_Type = \"MALL\" " . $filter . ") as mall,
		(SELECT count(Building_Type) FROM " . $table_name . " WHERE Building_Type = \"OFFICE\" " . $filter . ") as office,
		(SELECT count(Building_Type) FROM " . $table_name . " WHERE Building_Type = \"RESORT\" " . $filter . ") as resort

		FROM " . $table_name . " LIMIT 1";

		$query = $this->db->query($sql)->result();

		if ($row_num != 0) {
			$row_num = ($row_num - 1) * $row_page;
		}

		$array = [];

		$this->set_config($query[0]->total, $func_name = 'get_IBS_Track', $row_page);

		$array = array(
			'details' => $this->get_details($table_name, $search, $row_num, $row_page),
			'pname' => $this->get_province($table_name),
			'total' => $query[0]->total,
			'pagination' => $this->pagination->create_links(),
			'row_num' => $row_num,
			'Building_Type' => array(
				'airport' => $query[0]->airport,
				'condominium' => $query[0]->condominium,
				'corporate_office' => $query[0]->corporate_office,
				'globe_store' => $query[0]->globe_store,
				'hospital' => $query[0]->hospital,
				'hotel' => $query[0]->hotel,
				'mall' => $query[0]->mall,
				'office' => $query[0]->office,
				'resort' => $query[0]->resort,
			),
		);

		return $array;
	}

	public function set_config($total = 0, $func_name = '', $row_page = 0)
	{
		$config['base_url'] = base_url($func_name);
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $total;
		$config['per_page'] = $row_page;

		$config['full_tag_open'] = '<center><div class="padding"><nav><ul class="pagination text-center paginate_track" style="justify-content: center;">';
		$config['full_tag_close'] = '</ul></nav></div>';
		$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['next_tag_close'] = '<span aria-hidden="true"></span></span></li>';
		$config['prev_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['prev_tag_close'] = '</span></li>';
		$config['first_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['first_tag_close'] = '</span></li>';
		$config['last_tag_open'] = '<li class="page-item"><span class="page-link ">';
		$config['last_tag_close'] = '</span></li></center>';

		// Initialize
		$this->pagination->initialize($config);

		return $config;
	}

	public function get_province($table_name)
	{
		$pname = [];
		$prov = "SELECT id, PROVINCE FROM  " . $table_name . " GROUP BY PROVINCE";
		$province = $this->db->query($prov);

		if ($province->num_rows() > 0) {
			foreach ($province->result() as $row) {
				$pname[] = array(
					'id' => $row->id,
					'name' => $row->PROVINCE
				);
			}
		}
		return $pname;
	}

	public function get_Nominations_Track($filter = '', $search = '', $row_num = 0)
	{
		$row_page = $this->input->get('row_page');

		$filter = empty($filter) ? '' : " AND PROVINCE LIKE '%" . $filter . "%'";
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'nominations_' . $geoarea;

		$sql = "SELECT count(id) as total,
		(SELECT count(Site_Type) FROM " . $table_name . " WHERE Site_Type = \"MACRO\" " . $filter . ") as MACRO,
		(SELECT count(Site_Type) FROM " . $table_name . " WHERE Site_Type = \"SMALLCELL\" " . $filter . ") as SMALLCELL,
		(SELECT count(Tagging) FROM " . $table_name . " WHERE Tagging = \"Capacity\" " . $filter . ") as Capacity,
		(SELECT count(Tagging) FROM " . $table_name . " WHERE Tagging = \"Coverage\" " . $filter . ") as Coverage
		FROM " . $table_name . " LIMIT 1";

		$query = $this->db->query($sql)->result();

		if ($row_num != 0) {
			$row_num = ($row_num - 1) * $row_page;
		}

		$this->set_config($query[0]->total, $func_name = 'get_Nominations_Track', $row_page);

		$array = array(
			'data' => $query,
			'pname' => $this->get_province($table_name),
			'details' => $this->get_details($table_name, $search, $row_num, $row_page),
			'total' => $query[0]->total,
			'pagination' => $this->pagination->create_links(),
			'row_num' => $row_num
		);

		return $array;
	}

	public function get_AWE_TRACK($filter = '', $search = '', $row_num = 0)
	{
		$row_page = $this->input->get('row_page');

		$filter = empty($filter) ? '' : " AND PROVINCE LIKE '%" . $filter . "%'";
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'awe_' . $geoarea;

		#Baluarte Tagging
		$sql = "SELECT count(id) as total,
		(SELECT count(Baluarte_Tagging) FROM " . $table_name . " WHERE Baluarte_Tagging = \"Baluarte\" " . $filter . ") as Baluarte,
		(SELECT count(Baluarte_Tagging) FROM " . $table_name . " WHERE Baluarte_Tagging = \"BALUARTE - 2018 Priority\" " . $filter . ") as Baluarte_2018,
		(SELECT count(Baluarte_Tagging) FROM " . $table_name . " WHERE Baluarte_Tagging = \"Non-baluarte\" " . $filter . ") as Non,
		#Priority Tagging
		(SELECT count(Priority_Tag) FROM " . $table_name . " WHERE Priority_Tag = \"LOW INVESTMENT\" " . $filter . ") as low_investment,
		(SELECT count(Priority_Tag) FROM " . $table_name . " WHERE Priority_Tag = \"NO BUILD-SELL\" " . $filter . ") as no_build_sell,
		(SELECT count(Priority_Tag) FROM " . $table_name . " WHERE Priority_Tag = \"No Tag\" " . $filter . ") as no_tag,
		(SELECT count(Priority_Tag) FROM " . $table_name . " WHERE Priority_Tag = \"PRIO 1: BUILD\" " . $filter . ") as prio_1_build,
		(SELECT count(Priority_Tag) FROM " . $table_name . " WHERE Priority_Tag = \"PRIO 2: BUILD\" " . $filter . ") as prio_2_build,
		(SELECT count(Priority_Tag) FROM " . $table_name . " WHERE Priority_Tag = \"PRIO 3:BUILD\" " . $filter . ") as prio_3_build,
		#Town Priority
		(SELECT count(Town_Priority) FROM " . $table_name . " WHERE Town_Priority = \"Not Priority\" " . $filter . ") as not_priority,
		(SELECT count(Town_Priority) FROM " . $table_name . " WHERE Town_Priority = \"Priority \" " . $filter . ") as priority,
		#Program Tag
		(SELECT count(Program_Tag) FROM " . $table_name . " WHERE Program_Tag = \"BAYMAX\" " . $filter . ") as baymax,
		(SELECT count(Program_Tag) FROM " . $table_name . " WHERE Program_Tag = \"BAYMAX REFRESHED_add \" " . $filter . ") as baymax_refresh_add,
		(SELECT count(Program_Tag) FROM " . $table_name . " WHERE Program_Tag = \"Capacity \" " . $filter . ") as capacity,
		(SELECT count(Program_Tag) FROM " . $table_name . " WHERE Program_Tag = \"Capacity Sites \" " . $filter . ") as capacity_sites,
		(SELECT count(Program_Tag) FROM " . $table_name . " WHERE Program_Tag = \"EG \" " . $filter . ") as eg,
		(SELECT count(Program_Tag) FROM " . $table_name . " WHERE Program_Tag = \"LGU/VIP \" " . $filter . ") as lgu_vip,
		(SELECT count(Program_Tag) FROM " . $table_name . " WHERE Program_Tag = \"MASS \" " . $filter . ") as mass,
		(SELECT count(Program_Tag) FROM " . $table_name . " WHERE Program_Tag = \"NTG Driven \" " . $filter . ") as ntg_driven,
		(SELECT count(Program_Tag) FROM " . $table_name . " WHERE Program_Tag = \"RRE Additional Capacity Nominations \" " . $filter . ") as rre_additionals,
		(SELECT count(Program_Tag) FROM " . $table_name . " WHERE Program_Tag = \"RRE Nomination \" " . $filter . ") as rre_nominations,
		(SELECT count(Program_Tag) FROM " . $table_name . " WHERE Program_Tag = \"Victory List \" " . $filter . ") as victory_list

		FROM " . $table_name . " LIMIT 1";

		$query = $this->db->query($sql)->result();

		if ($row_num != 0) {
			$row_num = ($row_num - 1) * $row_page;
		}

		// $array = [];
		// echo $table_name . ' ' . $search . ' ' . $row_num . ' ' . $row_page;
		$this->set_config($query[0]->total, $func_name = 'get_AWE_TRACK', $row_page);

		// if ($query->num_rows() > 0) {
		// $query = $query->result();
		$array = array(
			'details' => $this->get_details($table_name, $search, $row_num, $row_page),
			'pname' => $this->get_province($table_name),
			'total' => $query[0]->total,
			'row_num' => $row_num,
			'pagination' => $this->pagination->create_links(),
			'Baluarte_Tagging' => array(
				'Baluarte' => $query[0]->Baluarte,
				'Baluarte_2018' => $query[0]->Baluarte_2018,
				'Non' => $query[0]->Non
			),
			'Priority_Tag' => array(
				'low_investment' => $query[0]->low_investment,
				'no_build_sell' => $query[0]->no_build_sell,
				'no_tag' => $query[0]->no_tag,
				'prio_1_build' => $query[0]->prio_1_build,
				'prio_2_build' => $query[0]->prio_2_build,
				'prio_3_build' => $query[0]->prio_3_build,
			),
			'Town_Priority' => array(
				'not_priority' => $query[0]->not_priority,
				'priority' => $query[0]->priority,
			),
			'Program_Tag' => array(
				'baymax' => $query[0]->baymax,
				'baymax_refresh_add' => $query[0]->baymax_refresh_add,
				'capacity' => $query[0]->capacity,
				'capacity_sites' => $query[0]->capacity_sites,
				'eg' => $query[0]->eg,
				'lgu_vip' => $query[0]->lgu_vip,
				'mass' => $query[0]->mass,
				'ntg_driven' => $query[0]->ntg_driven,
				'rre_additionals' => $query[0]->rre_additionals,
				'rre_nominations' => $query[0]->rre_nominations,
				'victory_list' => $query[0]->victory_list,
			),
		);
		// }

		return $array;
	}

	public function get_PMO_TRACK($search = '', $row_num = 0)
	{
		$row_page = $this->input->get('row_page');

		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'pmo_' . $geoarea;

		$sql = "SELECT count(id) as total,
		(SELECT count(Budget_Tagging) FROM " . $table_name . " WHERE Budget_Tagging = \"Full Build\") as Full_Build,
		(SELECT count(Budget_Tagging) FROM " . $table_name . " WHERE Budget_Tagging = \"T&F\") as TF,

		(SELECT count(Clutter_Type) FROM " . $table_name . " WHERE Clutter_Type = \"Dense Urban\") as Dense_Urban,
		(SELECT count(Clutter_Type) FROM " . $table_name . " WHERE Clutter_Type = \"Rural\") as Rural,
		(SELECT count(Clutter_Type) FROM " . $table_name . " WHERE Clutter_Type = \"Urban\") as Urban,
		(SELECT count(Clutter_Type) FROM " . $table_name . " WHERE Clutter_Type = NULL) as NA,

		(SELECT count(Program) FROM " . $table_name . " WHERE Program = \"2019 Capacity\") as capacity_2019,
		(SELECT count(Program) FROM " . $table_name . " WHERE Program = \"Masterplan\") as Masterplan,
		(SELECT count(Program) FROM " . $table_name . " WHERE Program = \"NPD\") as NPD,
		(SELECT count(Program) FROM " . $table_name . " WHERE Program = \"NTG Driven\") as NTG_Driven,
		(SELECT count(Program) FROM " . $table_name . " WHERE Program = \"Project Fly 1\") as Project_Fly_1,

		(SELECT count(Lead_Vendor) FROM " . $table_name . " WHERE Lead_Vendor = \"Guevent\") as Guevent,
		(SELECT count(Lead_Vendor) FROM " . $table_name . " WHERE Lead_Vendor = \"HT\") as HT,
		(SELECT count(Lead_Vendor) FROM " . $table_name . " WHERE Lead_Vendor = \"Nokia\") as Nokia,

		(SELECT count(Solution) FROM " . $table_name . " WHERE Solution = \"Easy Macro\") as Easy_Macro,
		(SELECT count(Solution) FROM " . $table_name . " WHERE Solution = \"MACRO\") as MACRO,
		(SELECT count(Solution) FROM " . $table_name . " WHERE Solution = \"Micro RRH\") as Micro_RRH,
		(SELECT count(Solution) FROM " . $table_name . " WHERE Solution = \"Mini-Macro\") as Mini_Macro,

		(SELECT count(Scope) FROM " . $table_name . " WHERE Scope = \"Full Build\") as Full_Build,
		(SELECT count(Scope) FROM " . $table_name . " WHERE Scope = \"Transport and Facilities\") as Transport_and_Facilities,

		(SELECT count(No_of_Sectors) FROM " . $table_name . " WHERE No_of_Sectors = \"1\") as sec1,
		(SELECT count(No_of_Sectors) FROM " . $table_name . " WHERE No_of_Sectors = \"2\") as sec2,
		(SELECT count(No_of_Sectors) FROM " . $table_name . " WHERE No_of_Sectors = \"3\") as sec3,
		(SELECT count(No_of_Sectors) FROM " . $table_name . " WHERE No_of_Sectors = \"4\") as sec4,

		(SELECT count(NTG_or_BUSINESS) FROM " . $table_name . " WHERE NTG_or_BUSINESS = \"BIZ\") as BIZ,
		(SELECT count(NTG_or_BUSINESS) FROM " . $table_name . " WHERE NTG_or_BUSINESS = \"NTG\") as NTG,
		(SELECT count(NTG_or_BUSINESS) FROM " . $table_name . " WHERE NTG_or_BUSINESS = NULL) as NA

		FROM " . $table_name . " LIMIT 1";

		$query = $this->db->query($sql)->result();

		if ($row_num != 0) {
			$row_num = ($row_num - 1) * $row_page;
		}

		$this->set_config($query[0]->total, $func_name = 'get_PMO_TRACK', $row_page);

		$array = array(
			'details' => $this->get_details($table_name, $search, $row_num, $row_page),
			'total' => $query[0]->total,
			'pagination' => $this->pagination->create_links(),
			'row_num' => $row_num,
			'Budget_Tagging' => array(
				'Full_Build' => $query[0]->Full_Build,
				'TF' => $query[0]->TF
			),
			'Clutter_Type' => array(
				'Dense_Urban' => $query[0]->Dense_Urban,
				'Rural' => $query[0]->Rural,
				'Urban' => $query[0]->Urban,
				'NA' => $query[0]->NA,
			),
			'Program' => array(
				'capacity_2019' => $query[0]->capacity_2019,
				'Masterplan' => $query[0]->Masterplan,
				'NPD' => $query[0]->NPD,
				'NTG_Driven' => $query[0]->NTG_Driven,
				'Project_Fly_1' => $query[0]->Project_Fly_1,
			),
			'Lead_Vendor' => array(
				'Guevent' => $query[0]->Guevent,
				'HT' => $query[0]->HT,
				'Nokia' => $query[0]->Nokia,
			),
			'Solution' => array(
				'Easy_Macro' => $query[0]->Easy_Macro,
				'MACRO' => $query[0]->MACRO,
				'Micro_RRH' => $query[0]->Micro_RRH,
				'Mini_Macro' => $query[0]->Mini_Macro,
			),
			'Scope' => array(
				'sec1' => $query[0]->sec1,
				'sec2' => $query[0]->sec2,
				'sec3' => $query[0]->sec3,
				'sec4' => $query[0]->sec4,
			),
		);

		return $array;
	}

	public function save_ibs_track($data, $status)
	{
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'ibs_' . $geoarea;
		if ($status == 0) {
			$status = false;
			foreach ($data as $row) {
				$query = $this->db->get_where($table_name, array('SITE_NAME' => $row['SITE_NAME']));
				if ($query->num_rows() > 0) {
					$status = true;
					break;
				}
			}

			if ($status) {
				$json = array(
					'type' => 'error',
					'title' => 'Invalid!',
					'msg' => 'Duplicate Site Name.'
				);
			} else {
				$this->db->insert_batch($table_name, $data);
				$msg = 'Data Inserted.';

				$json = array(
					'type' => 'success',
					'title' => 'Success!',
					'msg' => $msg
				);
			}
		} else {
			foreach ($data as $row) {
				$this->db->where('id', $row['id']);
				$this->db->set('SITE_NAME', $row['SITE_NAME']);
				$this->db->set('Building_Name', $row['Building_Name']);
				$this->db->set('Building_Type', $row['Building_Type']);
				$this->db->set('PLA_ID', $row['PLA_ID']);
				$this->db->set('Longitude', $row['Longitude']);
				$this->db->set('Latitude', $row['Latitude']);
				$this->db->set('Region', $row['Region']);
				$this->db->set('Province', $row['Province']);
				$this->db->set('Municipality_City', $row['Municipality_City']);
				$this->db->set('address', $row['address']);
				$this->db->update($table_name);
			}
			$msg = 'Data Updated.';

			$json = array(
				'type' => 'success',
				'title' => 'Success!',
				'msg' => $msg
			);
		}
		return $json;
	}

	public function get_TSSR_Track($search = '', $row_num = 0)
	{
		$row_page = $this->input->get('row_page');

		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'tssr_' . $geoarea;

		$sql = "SELECT count(id) as total,
		(SELECT count(Coverage) FROM " . $table_name . " WHERE Coverage = 'Indoor') as Indoor,
		(SELECT count(Coverage) FROM " . $table_name . " WHERE Coverage = 'Indoor-Outdoor') as Indoor_Outdoor,
		(SELECT count(Coverage) FROM " . $table_name . " WHERE Coverage = 'Outdoor') as Outdoor,
		(SELECT count(Coverage) FROM " . $table_name . " WHERE Coverage = NULL) as NA,

		(SELECT count(Clutter_Type) FROM " . $table_name . " WHERE Clutter_Type = 'Rural') as Rural,
		(SELECT count(Clutter_Type) FROM " . $table_name . " WHERE Clutter_Type = 'Urban') as Urban

		FROM " . $table_name . " LIMIT 1";

		$query = $this->db->query($sql);

		if ($row_num != 0) {
			$row_num = ($row_num - 1) * $row_page;
		}

		if ($query->num_rows() > 0) {
			$query = $query->result();
			$this->set_config($query[0]->total, $func_name = 'get_TSSR_Track', $row_page);

			$array = array(
				// 'pname' => $this->get_province($table_name),
				'details' => $this->get_details($table_name, $search, $row_num, $row_page),
				'total' => $query[0]->total,
				'pagination' => $this->pagination->create_links(),
				'row_num' => $row_num,
				'Coverage' => array(
					'Indoor' => $query[0]->Indoor,
					'Indoor_Outdoor' => $query[0]->Indoor_Outdoor,
					'Outdoor' => $query[0]->Outdoor,
					'NA' => $query[0]->NA,
				),
				'Clutter_Type' => array(
					'Rural' => $query[0]->Rural,
					'Urban' => $query[0]->Urban,
				),
			);
		}

		return $array;
	}

	public function save_tssr_track($data, $status)
	{
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'tssr_' . $geoarea;
		if ($status == 0) {
			$status = false;
			foreach ($data as $row) {
				$query = $this->db->get_where($table_name, array('site_name' => $row['site_name']));
				if ($query->num_rows() > 0) {
					$status = true;
					break;
				}
			}

			if ($status) {
				$json = array(
					'type' => 'error',
					'title' => 'Invalid!',
					'msg' => 'Duplicate Site Name.'
				);
			} else {
				$this->db->insert_batch($table_name, $data);
				$msg = 'Data Inserted.';

				$json = array(
					'type' => 'success',
					'title' => 'Success!',
					'msg' => $msg
				);
			}
		} else {
			foreach ($data as $row) {
				$this->db->where('id', $row['id']);
				$this->db->set('site_name', $row['site_name']);
				$this->db->set('pla_id', $row['pla_id']);
				$this->db->set('coverage', $row['coverage']);
				$this->db->set('location', $row['location']);
				$this->db->set('province', $row['province']);
				$this->db->set('town', $row['town']);
				$this->db->set('brgy', $row['brgy']);
				$this->db->set('town_psgc', $row['town_psgc']);
				$this->db->set('latitude', $row['latitude']);
				$this->db->set('longitude', $row['longitude']);
				$this->db->set('baluarte_tagging', $row['baluarte_tagging']);
				$this->db->set('clutter_type', $row['clutter_type']);
				$this->db->update($table_name);
			}
			$msg = 'Data Updated.';

			$json = array(
				'type' => 'success',
				'title' => 'Success!',
				'msg' => $msg
			);
		}
		return $json;
	}

	public function set_calendar($sid = "")
	{
		$json = array();
		$id = $this->session->userdata('globe_id');
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'survey_' . $geoarea;
		if ($this->session->userdata('access_level') == 'Admin') {
			if ($sid == 'All' || $id == 0) {
				all:
				#FOR ADMIN
				$dates = $this->db->query("SELECT a.Survey_Date, b.fname FROM " . $table_name . " a JOIN users b ON a.RRE_Representative = b.globe_id WHERE b.globe_id <> 0 GROUP BY a.Survey_Date");
			} else {
				goto user;
			}

		} else {

			if ($sid == 'All' || $id == 0) {
				goto all;
			}

			user:
			#FOR USER
			$dates = $this->db->query("SELECT a.Survey_Date, b.fname FROM " . $table_name . " a JOIN users b ON a.RRE_Representative = b.globe_id WHERE b.globe_id <> 0 AND b.globe_id = ? GROUP BY a.Survey_Date",
				array($id));
		}

		if ($dates->num_rows() > 0) {
			foreach ($dates->result() as $row1) {
				if ($this->session->userdata('access_level') == 'Admin') {
					#FOR ADMIN
					if ($sid == 'All' || $id == 0) {
						select_all:
						$names = $this->db->query("SELECT a.RRE_Representative, b.fname, a.Survey_Date FROM " . $table_name . " a JOIN users b ON a.RRE_Representative = b.globe_id WHERE a.Survey_Date = ? GROUP BY b.fname",
							array($row1->Survey_Date));
					} else {
						goto details;
					}

				} else {

					if ($sid == 'All' || $id == 0) {
						goto select_all;
					}

					details:
					#FOR USERS
					$names = $this->db->query("SELECT a.RRE_Representative, b.fname, a.Survey_Date FROM " . $table_name . " a JOIN users b ON a.RRE_Representative = b.globe_id WHERE a.Survey_Date = ? AND b.globe_id = ? GROUP BY b.fname",
						array($row1->Survey_Date, $id));
				}

				$name = array();
				foreach ($names->result() as $row2) {
					$subject = array();
					$data = $this->db->query("SELECT a.id, a.SITE_NAME, a.Survey_Date, b.fname, a.Site_Status, a.Category FROM " . $table_name . " a JOIN users b ON a.RRE_Representative = b.globe_id WHERE b.globe_id = ? AND a.Survey_Date = ? ORDER BY a.Survey_Date ASC",
						array($row2->RRE_Representative, $row1->Survey_Date));
					foreach ($data->result() as $row3) {
						$subject[] = array(
							'id' => $row3->id,
							'name' => $row3->fname,
							'subject_name' => $row3->SITE_NAME,
							'Survey_Date' => $row3->Survey_Date,
							'status' => $row3->Site_Status,
							'category' => $row3->Category,
						);
					}

					$name[] = array(
						'name' => $row2->fname,
						'subject' => $subject
					);
				}

				$json[] = array(
					'dates' => date("Y-m-d", strtotime($row1->Survey_Date)),
					'names' => $name
				);
			}
		}

		return $json;
	}

	public function getTrackUsers()
	{
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'users';
		#IF ADMIN
		if ($this->session->userdata('access_level') == 'Admin') {
			$data = $this->db->query("SELECT globe_id, fname FROM " . $table_name . " WHERE access_level = ? ORDER BY fname ASC", array('Standard'));
		} else {
			$data = $this->db->query("SELECT globe_id, fname FROM " . $table_name . " WHERE id = ? ORDER BY fname ASC", array($this->session->userdata('id')));
		}

		return $data->result();
	}

	public function getTrackStatus()
	{
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'consolidate_tracker_' . $geoarea;
		$query = $this->db->query("SELECT status FROM " . $table_name . " GROUP by status");

		return $query->result();
	}

	public function getTrackName()
	{
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'consolidate_tracker_' . $geoarea;
		$query = $this->db->query("SELECT track_name FROM " . $table_name . " WHERE status <> \"#N/A\" GROUP by track_name");

		return $query->result();
	}

	public function delete_track($id)
	{
		$this->db->where('id', $id);
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'consolidate_tracker_' . $geoarea;
		return $this->db->delete($table_name);
	}

	public function save_track($data, $id)
	{
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'consolidate_tracker_' . $geoarea;
		if (empty($id)) {
			$status = false;
			foreach ($data as $row) {
				$query = $this->db->get_where($table_name, array('subject_name' => $row['subject_name']));
				if ($query->num_rows() > 0) {
					$status = true;
					break;
				}
			}

			if ($status) {
				$json = array(
					'type' => 'error',
					'title' => 'Invalid!',
					'msg' => 'Duplicate subject name.'
				);
			} else {
				$this->db->insert_batch($table_name, $data);
				$msg = 'Data inserted.';

				$json = array(
					'type' => 'success',
					'title' => 'Success!',
					'msg' => $msg
				);
			}
		} else {
			$this->db->where('id', $id);
			$this->db->update($table_name, $data);
			$msg = 'Data updated.';

			$json = array(
				'type' => 'success',
				'title' => 'Success!',
				'msg' => $msg
			);
		}
		return $json;
	}

	public function get_Surveys_Track($name = "", $project = "", $search = '', $row_num = 0)
	{
		$row_page = $this->input->get('row_page');

		$data = array();

		$project1 = empty($project) || $project == '#N/A' ? '' : " AND Project = '" . $project . "'";
		$project2 = empty($project) || $project == '#N/A' ? '' : " WHERE Project = '" . $project . "'";

		$name1 = empty($name) || $project == '#N/A' ? '' : " AND RRE_Representative = '" . $name . "'";
		$name2 = empty($name) || $project == '#N/A' ? '' : (empty($project) ? " WHERE RRE_Representative = '" . $name . "'" : " AND RRE_Representative = '" . $name . "'");
		// $name2 = empty($name) ? '' : " WHERE RRE_Representative = '".$name."'";

		// echo $project .' '. $project1 . ' ' . $project2 . ' ' . $name1 . ' ' . $name2;

		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'survey_' . $geoarea;

		$sql = "SELECT
		#SITE STATUS
		(SELECT count(Site_Status) FROM " . $table_name . " WHERE Site_Status = ? " . $project1 . $name1 . ") as artb,
		(SELECT count(Site_Status) FROM " . $table_name . " WHERE Site_Status = ? " . $project1 . $name1 . ") as cancel,
		(SELECT count(Site_Status) FROM " . $table_name . " WHERE Site_Status = ? " . $project1 . $name1 . ") as for_build,
		(SELECT count(Site_Status) FROM " . $table_name . " WHERE Site_Status = ? " . $project1 . $name1 . ") as implemented,
		(SELECT count(Site_Status) FROM " . $table_name . " WHERE Site_Status = ? " . $project1 . $name1 . ") as rtb,
		(SELECT count(Site_Status) FROM " . $table_name . " WHERE Site_Status = ? " . $project1 . $name1 . ") as surveyed,
		(SELECT count(Site_Status) FROM " . $table_name . " WHERE Site_Status = ? " . $project1 . $name1 . ") as sNA,
		#CATEGORY
		(SELECT count(Category) FROM " . $table_name . " WHERE Category = ? " . $project1 . $name1 . ") as ewo,
		(SELECT count(Category) FROM " . $table_name . " WHERE Category = ? " . $project1 . $name1 . ") as master_plan,
		(SELECT count(Category) FROM " . $table_name . " WHERE Category = ? " . $project1 . $name1 . ") as new_site,
		(SELECT count(Category) FROM " . $table_name . " WHERE Category = ? " . $project1 . $name1 . ") as owo,
		(SELECT count(Category) FROM " . $table_name . " WHERE Category = ? " . $project1 . $name1 . ") as re_survey,
		(SELECT count(Category) FROM " . $table_name . " WHERE Category = ? " . $project1 . $name1 . ") as cNA,
		#VENDOR
		(SELECT count(Vendor) FROM " . $table_name . " WHERE Vendor = ? " . $project1 . $name1 . ") as argus,
		(SELECT count(Vendor) FROM " . $table_name . " WHERE Vendor = ? " . $project1 . $name1 . ") as bspt,
		(SELECT count(Vendor) FROM " . $table_name . " WHERE Vendor = ? " . $project1 . $name1 . ") as gk,
		(SELECT count(Vendor) FROM " . $table_name . " WHERE Vendor = ? " . $project1 . $name1 . ") as huawei,
		(SELECT count(Vendor) FROM " . $table_name . " WHERE Vendor = ? " . $project1 . $name1 . ") as mohave,
		(SELECT count(Vendor) FROM " . $table_name . " WHERE Vendor = ? " . $project1 . $name1 . ") as power_acts,
		(SELECT count(Vendor) FROM " . $table_name . " WHERE Vendor = ? " . $project1 . $name1 . ") as richworld,
		(SELECT count(Vendor) FROM " . $table_name . " WHERE Vendor = ? " . $project1 . $name1 . ") as vNA,

		#GET 624 TARGET SITES
		(SELECT count(id) FROM " . $table_name . " " . $project2 . $name2 . ") as total";

		$param = array(
			"ARTB'd",
			"Cancelled",
			"For Build",
			"Implemented",
			"RTB'd",
			"Surveyed",
			"",
			"EWO",
			"Master Plan",
			"New Site",
			"OWO",
			"Re-survey",
			"",
			"Argus",
			"BSPT",
			"GK",
			"Huawei",
			"Mohave",
			"Power Acts",
			"Richworld",
			"",
		);

		$array = [];

		$query = $this->db->query($sql, $param);

		$project = $this->db->query("SELECT project FROM " . $table_name . " GROUP BY project");
		$user_tbl = 'users';

		if ($row_num != 0) {
			$row_num = ($row_num - 1) * $row_page;
		}

		$this->db->select('a.*, b.fname as rre_name');
		$this->db->from($table_name . ' a');
		$this->db->join($user_tbl . ' b ', 'a.RRE_Representative = b.globe_id', 'left');
		if (!empty($name)) {
			$this->db->WHERE("RRE_Representative", $name);
		}
		$this->db->like("SITE_NAME", $search);
		$this->db->order_by('a.id', 'ASC');
		$this->db->limit($row_page, $row_num);
		$details = $this->db->get()->result();

		#ADMIN
		if ($this->session->userdata('access_level') == 'Admin') {
			$coordinates = $this->db->query("SELECT a.*, CONCAT(b.fname, ' ', b.lname) as rre FROM " . $table_name . " a JOIN " . $user_tbl . " b ON a.RRE_Representative = b.globe_id");
		} else {
			#USER
			$coordinates = $this->db->query("SELECT a.*, CONCAT(b.fname, ' ', b.lname) as rre FROM " . $table_name . " a JOIN " . $user_tbl . " b ON a.RRE_Representative = b.globe_id WHERE a.RRE_Representative = ?",
				array($this->session->userdata('globe_id')));
		}

		if ($query->num_rows() > 0) {
			$query = $query->result();
			$this->set_config($query[0]->total, $func_name = 'get_Surveys_Track', $row_page);

			$array = array(
				'coordinates' => $coordinates->result(),
				'project' => $project->result(),
				'user_name' => $this->User_accounts->get_users(),
				'details' => $details,
				'total' => $query[0]->total,
				'pagination' => $this->pagination->create_links(),
				'row_num' => $row_num,
				'site_status' => array(
					'artb' => $query[0]->artb,
					'cancel' => $query[0]->cancel,
					'for_build' => $query[0]->for_build,
					'implemented' => $query[0]->implemented,
					'rtb' => $query[0]->rtb,
					'surveyed' => $query[0]->surveyed,
					'sNA' => $query[0]->sNA,
				),
				'category' => array(
					'ewo' => $query[0]->ewo,
					'master_plan' => $query[0]->master_plan,
					'new_site' => $query[0]->new_site,
					'owo' => $query[0]->owo,
					're_survey' => $query[0]->re_survey,
					'cNA' => $query[0]->cNA,
				),
				'vendor' => array(
					'argus' => $query[0]->argus,
					'bspt' => $query[0]->bspt,
					'gk' => $query[0]->gk,
					'huawei' => $query[0]->huawei,
					'mohave' => $query[0]->mohave,
					'power_acts' => $query[0]->power_acts,
					'richworld' => $query[0]->richworld,
					'vNA' => $query[0]->vNA,
				),
			);
		}

		return $array;
	}
}
