<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transport_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->geoarea = $this->session->userdata('geoarea');

        $this->tbl = "transport_mdb";

        $this->column_search = array('PLA_ID','SITE_NAME','Solution_Type'); 
        $this->order = array('SITE_NAME' => 'asc');
	}


    public function trans_upload() {	
        if (isset($_FILES["trans_file"]["name"])) {
            $path = $_FILES["trans_file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load(strval($path));

            #GET FORMAT HEADER OF EXCEL
            $cell_collection = $object->getActiveSheet()->getCellCollection();
            foreach ($cell_collection as $cell) {
                $column = $object->getActiveSheet()->getCell($cell)->getColumn();
                $row = $object->getActiveSheet()->getCell($cell)->getRow();
                $data_value = $object->getActiveSheet()->getCell($cell)->getValue();
                //The header will/should be in row 1 only. of course, this can be modified to suit your need.
                if ($row == 1) {
                    $header[$row][$column] = trim($data_value);
                }
            }

            #CHECK IF EXCEL IMPORT MATCHES TO DATABASE FORMAT OF EXCEL
            if (79 == count($header[1])) {
              
                foreach ($object->getWorksheetIterator() as $worksheet) {
                    $highestRow = $worksheet->getHighestRow();
                    for ($row = 2; $row <= $highestRow; $row++) {
                        $data[] = array(
                            'sn' => $worksheet->getCellByColumnAndRow(0, $row)->getValue(),
                            'Source' => $worksheet->getCellByColumnAndRow(1, $row)->getValue(),
                            'PLA_ID' => $worksheet->getCellByColumnAndRow(2, $row)->getValue(),
                            'Regional_Area' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
                            'SITE_NAME' => $worksheet->getCellByColumnAndRow(4, $row)->getValue(),
                            'Solution_Type' => $worksheet->getCellByColumnAndRow(5, $row)->getValue(),
                            'Site_Type_Report' => $worksheet->getCellByColumnAndRow(6, $row)->getValue(),
                            'Solution_Actual' => $worksheet->getCellByColumnAndRow(7, $row)->getValue(),
                            'Equipment_Actual' => $worksheet->getCellByColumnAndRow(8, $row)->getValue(),
                            "Solution" => $worksheet->getCellByColumnAndRow(9, $row)->getValue(),
                            'Equipment' => $worksheet->getCellByColumnAndRow(10, $row)->getValue(),
                            'BW' => $worksheet->getCellByColumnAndRow(11, $row)->getValue(),
                            'MW_BW_Sum' => $worksheet->getCellByColumnAndRow(12, $row)->getValue(),
                            'Remarks_BW' => $worksheet->getCellByColumnAndRow(13, $row)->getValue(),
                            'Source_of_Info' => $worksheet->getCellByColumnAndRow(14, $row)->getValue(),
                            'Plan_Solution_Jedi2022' => $worksheet->getCellByColumnAndRow(15, $row)->getValue(),
                            'Plan_Solution_EO2021' => $worksheet->getCellByColumnAndRow(16, $row)->getValue(),
                            'NSB_20_Actual' => $worksheet->getCellByColumnAndRow(17, $row)->getValue(),
                            'NSB_20_Plan' => $worksheet->getCellByColumnAndRow(18, $row)->getValue(),
                            'NSB_21_Actual' => $worksheet->getCellByColumnAndRow(19, $row)->getValue(),
                            'NSB_21_Plan' => $worksheet->getCellByColumnAndRow(20, $row)->getValue(),
                            'FOC_2021_Projectized' => $worksheet->getCellByColumnAndRow(21, $row)->getValue(),
                            'MW_Plan_2021' => $worksheet->getCellByColumnAndRow(22, $row)->getValue(),
                            'FOC_Plan_2021' => $worksheet->getCellByColumnAndRow(23, $row)->getValue(),
                            'remove_from_FOC_Plan_2021' => $worksheet->getCellByColumnAndRow(24, $row)->getValue(),
                            'MW_RFS' => $worksheet->getCellByColumnAndRow(25, $row)->getValue(),
                            'Agile_RFS' => $worksheet->getCellByColumnAndRow(26, $row)->getValue(),
                            'Aviat_RFS' => $worksheet->getCellByColumnAndRow(27, $row)->getValue(),
                            'Huawei_RFS' => $worksheet->getCellByColumnAndRow(28, $row)->getValue(),

                            'UDB_Scope' => $worksheet->getCellByColumnAndRow(29, $row)->getValue(),
                            'UDB_Config' => $worksheet->getCellByColumnAndRow(30, $row)->getValue(),
                            'UDB_Cap' => $worksheet->getCellByColumnAndRow(31, $row)->getValue(),
                            'UDB_Hop_Name' => $worksheet->getCellByColumnAndRow(32, $row)->getValue(),
                            'hops_Reallocation' => $worksheet->getCellByColumnAndRow(33, $row)->getValue(),
                            'UDB_RFS_Plan' => $worksheet->getCellByColumnAndRow(34, $row)->getValue(),
                            'MW_Availment_JPF' => $worksheet->getCellByColumnAndRow(35, $row)->getValue(),
                            'JPF_Scope' => $worksheet->getCellByColumnAndRow(36, $row)->getValue(),
                            'JPF_Hop_Name' => $worksheet->getCellByColumnAndRow(37, $row)->getValue(),
                            'JPF_Config' => $worksheet->getCellByColumnAndRow(38, $row)->getValue(),
                            'JPF_Cap' => $worksheet->getCellByColumnAndRow(39, $row)->getValue(),
                            'FTTH' => $worksheet->getCellByColumnAndRow(40, $row)->getValue(),
                            'Fiberization_Priority' => $worksheet->getCellByColumnAndRow(41, $row)->getValue(),

                            'June_2021TRX_SOLN' => $worksheet->getCellByColumnAndRow(42, $row)->getValue(),
                            'June_2021_REMARKS' => $worksheet->getCellByColumnAndRow(43, $row)->getValue(),
                            'BASED_ON_MDB' => $worksheet->getCellByColumnAndRow(44, $row)->getValue(),
                            'REMARKS_MW' => $worksheet->getCellByColumnAndRow(45, $row)->getValue(),
                            'FOC_Doability' => $worksheet->getCellByColumnAndRow(46, $row)->getValue(),
                            'MW_to_FOC_Y2021' => $worksheet->getCellByColumnAndRow(47, $row)->getValue(),
                            'MW_Availment_JPF' => $worksheet->getCellByColumnAndRow(48, $row)->getValue(),
                            'Plan_MW_TO_FOC' => $worksheet->getCellByColumnAndRow(49, $row)->getValue(),
                            'MW_distance' => $worksheet->getCellByColumnAndRow(50, $row)->getValue(),
                            'MW_linkup' => $worksheet->getCellByColumnAndRow(51, $row)->getValue(),
                            'No_of_DEPENDENT_SITES' => $worksheet->getCellByColumnAndRow(52, $row)->getValue(),
                            'Hops_to_AN' => $worksheet->getCellByColumnAndRow(53, $row)->getValue(),
                            'MW_Type' => $worksheet->getCellByColumnAndRow(54, $row)->getValue(),

                            'Nationwide_Transport_Database' => $worksheet->getCellByColumnAndRow(56, $row)->getValue(),
                            'MW_to_FOC_Jedi22_From_Glen' => $worksheet->getCellByColumnAndRow(57, $row)->getValue(),
                            'x_2023_plan_FOC' => $worksheet->getCellByColumnAndRow(58, $row)->getValue(),
                            'MW_2022_Plan' => $worksheet->getCellByColumnAndRow(59, $row)->getValue(),
                            'MW_Jedi_22' => $worksheet->getCellByColumnAndRow(60, $row)->getValue(),
                            'Final_MW_Jedi22' => $worksheet->getCellByColumnAndRow(61, $row)->getValue(),
                            'Reassessment' => $worksheet->getCellByColumnAndRow(62, $row)->getValue(),
                            'MW_Jedi22_Assessment' => $worksheet->getCellByColumnAndRow(63, $row)->getValue(),
                            'MW_Jedi22_Solution' => $worksheet->getCellByColumnAndRow(64, $row)->getValue(),
                            'MW_Jedi_Priority' => $worksheet->getCellByColumnAndRow(65, $row)->getValue(),
                            'Planned_Capacity' => $worksheet->getCellByColumnAndRow(66, $row)->getValue(),
                            'MW_Jedi_Criteria' => $worksheet->getCellByColumnAndRow(67, $row)->getValue(),
                            'LM_Intermediate' => $worksheet->getCellByColumnAndRow(68, $row)->getValue(),

                            'Five5G' => $worksheet->getCellByColumnAndRow(69, $row)->getValue(),
                            'BW_Requirement' => $worksheet->getCellByColumnAndRow(70, $row)->getValue(),
                            'from_AEPM' => $worksheet->getCellByColumnAndRow(71, $row)->getValue(),
                            'WFM' => $worksheet->getCellByColumnAndRow(72, $row)->getValue(),
                            'Jedi_22' => $worksheet->getCellByColumnAndRow(73, $row)->getValue(),
                            'MW_Tagging' => $worksheet->getCellByColumnAndRow(74, $row)->getValue(),
                            'Actual_BW' => $worksheet->getCellByColumnAndRow(75, $row)->getValue(),
                            'Utilization' => $worksheet->getCellByColumnAndRow(76, $row)->getValue(),
                            'Equipment_Type' => $worksheet->getCellByColumnAndRow(77, $row)->getValue(),
                            'Linkup_based_on_Mycomm' => $worksheet->getCellByColumnAndRow(78, $row)->getValue(),
                            'Lookup_using' => $worksheet->getCellByColumnAndRow(79, $row)->getValue(),
                            'Source_and_Date' => $worksheet->getCellByColumnAndRow(80, $row)->getValue(),
                            'mUDB' => $worksheet->getCellByColumnAndRow(81, $row)->getValue(),

                            'date' => date('Y-m-d H:i:s'),
                        );
                    }
                }

                #TRUNCATE TABLE BEFORE INSERT
                $this->db->truncate($this->tbl);
                #CLEAR DATA BEFORE INSERT
                $this->db->insert_batch($this->tbl, array_filter($data));
                if ($this->db->affected_rows() == 0) {
                    $json = array(
                        'title' => 'Oops!',
                        'msg'   => 'Upload failed.',
                        'type'  => 'error',
                    );
                } else {
                    #GET RANK WORST CELLS PER CELL NAMES OR SITE
                    $json = array(
                        'title' => 'Success',
                        'msg'   => 'Successfully uploaded.',
                        'type'  => 'success',
                    );
                }
            } else {
                $json = array(
                    'title' => 'Oops!',
                    'msg'   => 'Import file does not match to desired format.',
                    'type'  => 'error',
                );
            }
        } else {
            $json = array(
                'title' => 'Ooops!',
                'msg'   => 'Something went wrong.',
                'type'  => 'error',
                'date' => ''
            );
        }
			
		

		return $json;
	}


    public function get_transport() {
        $list = $this->get_datatables($this->tbl);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $db) {
            $no++;
            $row = array();
            $row[] = $no;

            $row[] = $db->sn;
            $row[] = $db->Source;
            $row[] = $db->PLA_ID;
            $row[] = $db->Regional_Area;
            $row[] = $db->SITE_NAME;
            $row[] = $db->Solution_Type;
            $row[] = $db->Site_Type_Report;
            $row[] = $db->Solution_Actual;
            $row[] = $db->Equipment_Actual;
            $row[] = $db->Solution;
            $row[] = $db->Equipment;
            $row[] = $db->BW;
            $row[] = $db->MW_BW_Sum;
            $row[] = $db->Remarks_BW;
            $row[] = $db->Source_of_Info;
            $row[] = $db->Plan_Solution_Jedi2022;
            $row[] = $db->Plan_Solution_EO2021;
            $row[] = $db->NSB_20_Actual;
            $row[] = $db->NSB_20_Plan;
            $row[] = $db->NSB_21_Actual;
            $row[] = $db->NSB_21_Plan;
            $row[] = $db->FOC_2021_Projectized;
            $row[] = $db->MW_Plan_2021;
            $row[] = $db->FOC_Plan_2021;
            $row[] = $db->remove_from_FOC_Plan_2021;
            $row[] = $db->MW_RFS;
            $row[] = $db->Agile_RFS;
            $row[] = $db->Aviat_RFS;
            $row[] = $db->Huawei_RFS;

            $row[] = $db->UDB_Scope;
            $row[] = $db->UDB_Config;
            $row[] = $db->UDB_Cap;
            $row[] = $db->UDB_Hop_Name;
            $row[] = $db->hops_Reallocation;
            $row[] = $db->UDB_RFS_Plan;
            $row[] = $db->MW_Availment_JPF;
            $row[] = $db->JPF_Scope;
            $row[] = $db->JPF_Hop_Name;
            $row[] = $db->JPF_Config;
            $row[] = $db->JPF_Cap;
            $row[] = $db->FTTH;
            $row[] = $db->Fiberization_Priority;
            $row[] = $db->June_2021TRX_SOLN;

            $row[] = $db->June_2021_REMARKS;
            $row[] = $db->BASED_ON_MDB;
            $row[] = $db->REMARKS_MW;
            $row[] = $db->FOC_Doability;
            $row[] = $db->MW_to_FOC_Y2021;
            $row[] = $db->Plan_MW_TO_FOC;
            $row[] = $db->MW_Availment_JPF;
            $row[] = $db->MW_distance;
            $row[] = $db->MW_linkup;
            $row[] = $db->No_of_DEPENDENT_SITES	;
            $row[] = $db->Hops_to_AN;
            $row[] = $db->MW_Type;
            $row[] = $db->Nationwide_Transport_Database;
            $row[] = $db->MW_to_FOC_Jedi22_From_Glen;

            $row[] = $db->x_2023_plan_FOC;
            $row[] = $db->MW_2022_Plan;
            $row[] = $db->MW_Jedi_22;
            $row[] = $db->Final_MW_Jedi22;
            $row[] = $db->Reassessment;
            $row[] = $db->MW_Jedi22_Assessment;
            $row[] = $db->MW_Jedi22_Solution;
            $row[] = $db->MW_Jedi_Priority;
            $row[] = $db->Planned_Capacity;
            $row[] = $db->MW_Jedi_Criteria	;
            $row[] = $db->LM_Intermediate;
            $row[] = $db->Five5G;
            $row[] = $db->from_AEPM;
            $row[] = $db->WFM;

            $row[] = $db->Jedi_22;
            $row[] = $db->MW_Tagging;
            $row[] = $db->Actual_BW;
            $row[] = $db->Utilization;
            $row[] = $db->Equipment_Type;
            $row[] = $db->Linkup_based_on_Mycomm;
            $row[] = $db->Lookup_using;
            $row[] = $db->Source_and_Date;
            $row[] = $db->Planned_Capacity;
            $row[] = $db->mUDB;

            $row[] = $db->date;
 
            $data[] = $row;
        }
 
        return array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->count_all($this->tbl),
                        "recordsFiltered" => $this->count_filtered($this->tbl),
                        "data" => $data,
                );
    }

    function get_datatables($db)
    {
        $this->_get_datatables_query($db);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered($db)
    {
        $this->_get_datatables_query($db);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($db)
    {
        $this->db->from($db);
        return $this->db->count_all_results();
    }

    private function _get_datatables_query($db)
    {
         
        $this->db->from($db);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $query = $this->db->get($db);
            $field_array = $query->list_fields();

           
            $this->db->order_by($field_array[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    public function timestamp($date)
	{
		$this->load->helper('date');

		// Declare timestamps
		$last = new DateTime($date);
		$now = new DateTime(date('d-m-Y H:i:s', time()));

		// Find difference
		$interval = $last->diff($now);
		$years = (int)$interval->format('%Y');
		$months = (int)$interval->format('%m');
		$days = (int)$interval->format('%d');
		$hours = (int)$interval->format('%H');
		$minutes = (int)$interval->format('%i');

		if ($years > 0) {
			return $years . ' Yr' .($years > 1 ? 's ' : ' ') . $months . ' Month' .($months > 1 ? 's ' : ' '). $days . ' Day'.($days>1 ? 's ' : ' ') . $hours . ' Hr'.($hours > 1 ? 's ' : ' ') . $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		} else if ($months > 0) {
			return $months . ' Month' .($months > 1 ? 's ' : ' ') . $days . ' Day'.($days>1 ? 's ' : ' ') . $hours . ' Hr'.($hours > 1 ? 's ' : ' ') . $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		} else if ($days > 0) {
			return $days . ' Day'.($days>1 ? 's ' : ' ') . $hours . ' hr'.($hours > 1 ? 's ' : ' ') . $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		} else if ($hours > 0) {
			return $hours . ' Hr'.($hours > 1 ? 's ' : ' ') . $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		} else {
			return $minutes == 0 ? ' Now' : $minutes . ' min'.($minutes > 1 ? 's' : '').' ago';
		}
	}

    public function get_trans_fkeys() {
        $data = $this->db->get($this->tbl);
        return $data->list_fields();
    }

    public function trans_remove() {
        $this->db->truncate($this->tbl);

        return array(
			'type' => 'success',
			'title' => 'Success!',
			'msg' => 'Data deleted.'
		);
    }
}
