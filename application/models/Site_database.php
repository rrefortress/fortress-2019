<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_database extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	//get single site from db
	public function getSingle($database_table, $siteid, $select)
	{
		$this->db->select($select);
		return $this->db->get_where($database_table, $siteid)->row();
	}
	//get some site from db
	public function getSome($database_table, $id, $select)
	{
		$this->db->select($select);
		return $this->db->get_where($database_table, $id)->result();
	}
	//get some like from db
	public function getLike($database_table, $id, $select)
	{
		$this->db->select($select);
		$this->db->like($id);
		return $this->db->get($database_table)->result();
	}
	//get all data from db
	public function getAll($database_table, $select)
	{
		$this->db->select($select);
		return $this->db->get($database_table)->result();
	}
	//get table number of rows
	public function getLastDBID($database_table, $select)
	{
		// $this->db->select($select);
		// // $this->db->limit(1);////////////////////////REPAIR THIS, DONT USE LAST_ROW TO REUSE DB ID DELETED
		// $query = $this->db->get($database_table);
		// $row = $query->last_row();// To get last record form the table
		$table = substr($database_table, 0, -4);
		if($table=='tower')
		{
			// return $row->siteid;
			return $last_row=$this->db->select('*')->order_by('siteid',"desc")->limit(1)->get($database_table)->row();
		}else
		{
			// return $row->dbcellid;
			return $last_row=$this->db->select('*')->order_by('dbcellid',"desc")->limit(1)->get($database_table)->row();
		}
	}

	// public function addSite($data,$database_table, $geoarea)
	// {
	// 	$_table_name = $database_table.'_'.$geoarea;
	// 	$this->db->insert($_table_name, $data);

	// 	return $this->db->get_where($_table_name, array('id' => $data['id']))->row();
	// }
	public function getSite($filterarray,$database_table)
	{
		// $this->db->select('*');
		//$filterarray = array('id' => $filter);
		return $this->db->get_where($database_table,$filterarray)->row();
	}
	// public function insertSite($data,$database_table)
	// {
	// 	$this->db->insert($database_table, $data);
	// 	// $dec =	json_decode($data['sitename'],true);
	// 	// $filterarray = array('sitename' => $dec->{'sitename'});
	// 	//$this->db->select($data);
	// 	$id = $this->db->last_query();
	// 	return $id;
	// }
	public function insertSingleDB($data,$database_table)
	{
		$table = substr($database_table, 0, -4);
		if($table=='tower')
		{
			$dbnow = 'siteid';
		}else if ($table=='mdbupdates') 
		{
			$dbnow = 'id';
		}
		else
		{
			$dbnow = 'dbcellid';
		}
		$this->db->insert($database_table, $data);
		// $dec =	json_decode($data['sitename'],true);
		// $filterarray = array('sitename' => $dec->{'sitename'});
		// return $this->db->last_query();//<<returns id of approvals
		// $id = $data[$dbnow];
		// $id = $this->db->insert_id();
		// $q = $this->db->get_where($database_table, array($dbnow => $id));
		// // return $q->row()->$dbnow;
		// return $id;

		if ($table=='mdbupdates') 
		{
			return $this->db->insert_id();
		}
		else
		{
			$q = $this->db->get_where($database_table, array($dbnow => $data[$dbnow]));
			return $q->row()->$dbnow;
		}
	}
	public function updateSite($data,$database_table)
	{
		$this->db->where('id', $data['id']);
		$this->db->update($database_table, $data);

		//$dec =	json_decode($data['detailsafter']);
		// $filterarray = array('sitename' => $dec->{'sitename'});
		// $dec =	json_decode($this->db->get_where($_table_name,$filterarray)->row());
		//return $dec;
	}
	public function updateSingleDB($data,$database_table)
	{
		$table = substr($database_table, 0, -4);
		if($table=='tower')
		{
			$dbnow = 'siteid';
		}else if ($table=='mdbupdates') 
		{
			$dbnow = 'id';
		}
		else
		{
			$dbnow = 'dbcellid';
		}
		$this->db->where($dbnow, $data[$dbnow]);
		$this->db->update($database_table, $data);
		
		$id = $data[$dbnow];
		$q = $this->db->get_where($database_table, array($dbnow => $id));
		return $q->row()->$dbnow;
		//$dec =	json_decode($data['detailsafter']);
		// $filterarray = array('sitename' => $dec->{'sitename'});
		// $dec =	json_decode($this->db->get_where($_table_name,$filterarray)->row());
		//return $dec;
	}
	public function deleteSingleDB($data,$database_table)
	{
		$table = substr($database_table, 0, -4);
		if($table=='tower')
		{
			$dbnow = 'siteid';
		}else
		{
			$dbnow = 'dbcellid';
		}
		// $this->db->where($dbnow, $data[$dbnow]);
		$this->db->delete($database_table, $data);
		// $id = $this->db->update_id();
		return $data[$dbnow];
		//$dec =	json_decode($data['detailsafter']);
		// $filterarray = array('sitename' => $dec->{'sitename'});
		// $dec =	json_decode($this->db->get_where($_table_name,$filterarray)->row());
		//return $dec;
	}
	// public function updateSingleDB($data,$database_table)
	// {
	// 	$this->db->where('id', $data['id']);
	// 	$this->db->update($database_table, $data);
	// }


	public function getCount($database_table, $id, $select)
	{
		$this->db->select($select);
		return $this->db->get_where($database_table, $id)->num_rows();
	}

	//insert batch data
	public function batchInsertRRE($database_table, $filename)
	{
		//echo json_encode("a");
		$rawdata_array = array();
		$handle = fopen(site_url('upload/').$filename, "r");
		set_time_limit(0);
		$filesop = fgetcsv($handle,1000,',');

		//insert header checker here
		//insert header checker here
		//insert header checker here

		$list_tower = array('siteid','sitename','plaid','geoarea','longitude','latitude','sitetype','towertype','towerheight','region','province','municipality','barangay','address','audited','status','permanent');

		$list_2G = array('dbcellid','siteid','homing','sitename','cellname','cid','azimuth','longitude','latitude','sitetype','acl','mtilt','etilt','tech','lac','rac','ncc','bcc','bcch','vendor','petal','orientation','solution','audited','status');

		$list_3G = array('dbcellid','siteid','homing','sitename','cellname','nodebname','cid','rncid','azimuth','longitude','latitude','sitetype','acl','mtilt','etilt','tech','band','carrier','uplinkuarfcn','downlinkuarfcn','psc','lac','rac','mtpowerofcell','pcpichtp','vendor','petal','orientation','solution','audited','status');

		$list_4G = array('dbcellid','siteid','homing','sitename','cellname','duplex','tech','band','carrier','pcid','enodebid','tac','cid','longitude','latitude','sitetype','acl','azimuth','mtilt','etilt','earfcn','bw','vendor','petal','orientation','solution','audited','status');
		$list_annex = array('region','province','province_code','town','town_code');

		$tech = substr($database_table, 0, -4);
		switch ($tech)
		{
			case 'tower':
			$list = $list_tower;
			
			break;
			case '2g':
			$list = $list_2G;
			break;
			case '3g':
			$list = $list_3G;
			break;
			case '4g':
			$list = $list_4G;
			break;
			case 'annex':
			$list = $list_annex;
			break;
			default:
				# code...
			break;
		}

		//echo 'Please wait while updating the database..';
		$i=0;
		$count_update = 0;
		//echo 1;
		while(($filesop = fgetcsv($handle,1000,',')) !== false)
		{
			$x=0;
			foreach ($list as $value)
			{
				$rawdata_array[$i][$value] = utf8_encode($filesop[$x]);///////i add utf8_encode
				$x++;
				//echo json_encode($list);
			}
			// $rawdata_array[$i]['g'.$technology.'_created'] = date('Y-m-d H:i:s');
			// $rawdata_array[$i]['g'.$technology.'_modified'] = date('Y-m-d H:i:s');
			$i++;
		}
		//$this->delete_alldata($technology, $region);
//		$this->db->empty_table($_table_name);
		$this->db->insert_batch($database_table, $rawdata_array);

		return (($this->db->error())["code"]);
		//return (($this->db->error()));
	}

function update($database_table,$eachdata)
{ 
	$this->db->update_batch($database_table,$eachdata, 'id');
	return $this->db->affected_rows();
}
function delete($database_table,$selections)
{
	$field = empty($this->input->post('fe')) ? 'id' : 'plaid';
	foreach ($selections as $value)
	{
		$this->db->where($field, $value);
		$this->db->delete($database_table);
	}
	return $this->db->affected_rows();
}
function truncate($database_table)
{
	$this->db->truncate($database_table);
	return $this->db->affected_rows();
}
function makeQuery($database_table, $param = '')
    {

    	$db = substr($database_table, 0, -4);

    	${$db.'select_column'} = $this->getTableFields($database_table);

    	$fields_list = $this->getTableFields($database_table);
    	$select_column = ${$db.'select_column'};
    	$search_value =  $_POST["search"]["value"];

//		$this->check_table($select_column);
		$tbl_name = $this->input->post('displayto');
		$geoarea = $this->session->userdata('geoarea');
		$regions = array('nlz','ncr','slz','vis','min');

		
		if ($geoarea == 'nat') {
			#FOR NATIONAL
			$db = substr($database_table, 0, -4);
			$database_table = $db.'_vis';
			$fields_list = $this->getTableFields($database_table);
			foreach ($regions as $region)
			{
				if(isset($search_value))
				{
					$this->db->like("plaid", $search_value);
					foreach ($fields_list as $value)
					{
						$this->db->or_like($value, $search_value);
					}
				}
				$this->db->select($select_column);
				$this->db->from($db."_".$region);
				${$region} = $this->db->get_compiled_select();
			}

			// #NLZ
			// if(isset($search_value))
			// {
			// 	$this->db->like("plaid", $search_value);
			// 	foreach (${$db.'select_column'} as $value)
			// 	{
			// 		$this->db->or_like($value, $search_value);
			// 	}
			// }
			// $this->db->select($select_column);
			// $this->db->from('powerdb_nlz');
			// $nlz = $this->db->get_compiled_select();

			// #SLZ
			// if(isset($search_value))
			// {
			// 	$this->db->like("plaid", $search_value);
			// 	foreach (${$db.'select_column'} as $value)
			// 	{
			// 		$this->db->or_like($value, $search_value);
			// 	}
			// }
			// $this->db->select($select_column);
			// $this->db->from('powerdb_slz');
			// $slz = $this->db->get_compiled_select();
			// #NCR
			// if(isset($search_value))
			// {
			// 	$this->db->like("plaid", $search_value);
			// 	foreach (${$db.'select_column'} as $value)
			// 	{
			// 		$this->db->or_like($value, $search_value);
			// 	}
			// }
			// $this->db->select($select_column);
			// $this->db->from('powerdb_ncr');
			// $ncr = $this->db->get_compiled_select();
			// #VIS
			// if(isset($search_value))
			// {
			// 	$this->db->like("plaid", $search_value);
			// 	foreach (${$db.'select_column'} as $value)
			// 	{
			// 		$this->db->or_like($value, $search_value);
			// 	}
			// }
			// $this->db->select($select_column);
			// $this->db->from('powerdb_vis');
			// $vis = $this->db->get_compiled_select();
			// #MIN
			// if(isset($search_value))
			// {
			// 	$this->db->like("plaid", $search_value);
			// 	foreach (${$db.'select_column'} as $value)
			// 	{
			// 		$this->db->or_like($value, $search_value);
			// 	}
			// }
			// $this->db->select($select_column);
			// $this->db->from('powerdb_min');
			// $min = $this->db->get_compiled_select();

			$limit = '';
			if (!empty($param)) {
				if($_POST["length"] != -1)
				{
					$length = $this->input->post('length');
					$start = $this->input->post('start');
					$limit = 'LIMIT '. $start.', '.$length;
				}
			}

			if(isset($_POST["order"]))
			{
				$order = $select_column[$_POST['order']['0']['column']] ." ". $_POST['order']['0']['dir'];
			} else {
				$order = ${$db.'select_column'}[0] . ' ASC';
			}

			$query = $this->db->query("SELECT * FROM (" . $nlz." UNION ALL ".$slz." UNION ALL " .$vis. " UNION ALL " .$min. " UNION ALL " .$ncr.") as X ORDER BY ".$order." ".$limit);
		} else {
			//FOR OTHER
			$this->db->from($tbl_name.'_'.$geoarea);
			$this->db->select($fields_list);
			
			if ( isset( $_POST['columns'] ) || isset($search_value)) {
	            for ( $i=0, $ien=count($_POST['columns']) ; $i<$ien ; $i++ ) {
	                $str = $_POST['columns'][$i]['search']['value'];
	                if ($str != '' ) {
	                	$this->db->like($fields_list[$i], $str);
	                }
	                if ($search_value != '' ) {
	                	$this->db->or_like($fields_list[$i], $search_value);
	                }
	            }
	        }

			if(isset($_POST["order"]))
			{
				$this->db->order_by($select_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			}else
			{
				$this->db->order_by($fields_list[0], 'ASC');
			}
			$query = $this->db->get();
		}
//				$query = $this->db->get();
				return $query;
    }

//    function check_table($select_column) {
//		$tbl_name = $this->input->post('displayto');
//		$geoarea = $this->session->userdata('geoarea');
//		if ($geoarea == 'nat') {
//			#FOR NATIONAL
//			#NLZ
//			$this->db->select($select_column);
//			$this->db->from('powerdb_nlz');
//			$nlz = $this->db->get_compiled_select();
//			#SLZ
//			$this->db->select($select_column);
//			$this->db->from('powerdb_slz');
//			$slz = $this->db->get_compiled_select();
//			#NCR
//			$this->db->select($select_column);
//			$this->db->from('powerdb_ncr');
//			$ncr = $this->db->get_compiled_select();
//			#VIS
//			$this->db->select($select_column);
//			$this->db->from('powerdb_vis');
//			$vis = $this->db->get_compiled_select();
//			#MIN
//			$this->db->select($select_column);
//			$this->db->from('powerdb_min');
//			$min = $this->db->get_compiled_select();
//
//			return $this->db->query("SELECT * FROM (" . $nlz." UNION ALL ".$slz." UNION ALL " .$vis. " UNION ALL " .$min. " UNION ALL " .$ncr.") as X ORDER BY id ASC")->result();
//		} else {
//			#FOR OTHER
//			$this->db->from($tbl_name.'_'.$geoarea);
//			return $this->db->select($select_column);
//		}
//	}

    function getTableFields($database_table){

		$geoarea = $this->session->userdata('geoarea');
		if ($geoarea == 'nat') {
			$db = substr($database_table, 0, -4);
			$database_table = $db.'_vis';
		}
		return $this->db->list_fields($database_table);
//		return $this->check_table('*');
    }
    function makeDatatables($database_table){
		$geoarea = $this->session->userdata('geoarea');
		$param = '';
		if ($geoarea != 'nat') {
			if($_POST["length"] != -1)
			{
				$this->db->limit($_POST['length'], $_POST['start']);
			}
		} else {
			$param = 1;
		}

		$query = $this->makeQuery($database_table, $param);
    	return $query->result();
    }
    function getFilteredData($database_table){
    	$query = $this->makeQuery($database_table, '');
    	return $query->num_rows();
    }
    function getAllData($database_table)
    {
		$geoarea = $this->session->userdata('geoarea');
		if ($geoarea == 'nat') {
			$this->db->select("*");
			$this->db->from("powerdb_vis");
			return $this->db->count_all_results();
		} else {
			$this->db->select("*");
			$this->db->from($database_table);
			return $this->db->count_all_results();
		}
    }

    function get_join($filter,$condition,$joinID,$tablefrom,$tableto,$direct)
    {
    	$this->db->select($filter);
		$this->db->from($tablefrom);
		$this->db->join($tableto, $joinID,$direct);
		$this->db->where($condition);

		return $this->db->get()->result();
    }

    function get_site($filter,$tablefrom)
    {
    	$this->db->select($filter);
		$this->db->from($tablefrom);
		return $this->db->get()->result();
    }
}

/* End of file Site_database.php */
/* Location: ./application/models/Site_database.php */
