<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getActivites() {
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'survey_'.$geoarea;
		$json = array();

		$user_tbl = 'users';

		if ($this->session->userdata('access_level') == 'Admin') {
			#FOR ADMIN
			// $dates = $this->db->query("SELECT a.Survey_Date, b.fname FROM ".$table_name." a JOIN ".$user_tbl." b ON a.RRE_Representative = b.globe_id WHERE MONTH(a.Survey_Date) = MONTH(CURDATE()) AND DATE(a.Survey_Date) = CURDATE() GROUP BY a.Survey_Date");
			$dates = $this->db->query("SELECT a.Survey_Date, b.fname FROM ".$table_name." a JOIN  ".$user_tbl." b ON a.RRE_Representative = b.globe_id WHERE DATE(a.Survey_Date) >= CURDATE() GROUP BY a.Survey_Date");
		} else {
			#FOR USER
			// $dates = $this->db->query("SELECT a.Survey_Date, b.fname FROM ".$table_name." a JOIN ".$user_tbl." b ON a.RRE_Representative = b.globe_id WHERE MONTH(a.Survey_Date) = MONTH(CURDATE()) AND DATE(a.Survey_Date) = CURDATE() AND b.id = ? GROUP BY a.Survey_Date",
			// array($this->session->userdata('id')));
			$dates = $this->db->query("SELECT a.Survey_Date, b.fname FROM ".$table_name." a JOIN  ".$user_tbl." b ON a.RRE_Representative = b.globe_id WHERE DATE(a.Survey_Date) >= CURDATE() AND b.id = ? GROUP BY a.Survey_Date",
			array($this->session->userdata('id')));
		}

		if ($dates->num_rows() > 0) {
			foreach ($dates->result() as $row1) {
				if ($this->session->userdata('access_level') == 'Admin') {
					#FOR ADMIN
					$names = $this->db->query("SELECT a.RRE_Representative, b.fname, a.Survey_Date FROM ".$table_name." a JOIN ".$user_tbl." b ON a.RRE_Representative = b.globe_id WHERE a.Survey_Date = ? GROUP BY b.fname",
						array($row1->Survey_Date));
				} else {
					#FOR USERS
					$names = $this->db->query("SELECT a.RRE_Representative, b.fname, a.Survey_Date FROM ".$table_name." a JOIN ".$user_tbl." b ON a.RRE_Representative = b.globe_id WHERE a.Survey_Date = ? AND b.id = ? GROUP BY b.fname",
						array($row1->Survey_Date, $this->session->userdata('id')));
				}

				$name = array();
				foreach ($names->result() as $row2) {
					$subject = array();
					$data = $this->db->query("SELECT a.id, a.SITE_NAME, a.Survey_Date, b.fname FROM ".$table_name." a JOIN ".$user_tbl." b ON a.RRE_Representative = b.globe_id WHERE b.globe_id = ? AND a.Survey_Date = ? ORDER BY a.Survey_Date ASC",
						array($row2->RRE_Representative, $row1->Survey_Date));
					foreach ($data->result() as $row3) {
						$subject[] = array(
							'id'  => $row3->id,
							'name' => $row3->fname,
							'subject_name' => $row3->SITE_NAME,
							'day' => date('d', strtotime($row3->Survey_Date)),
							'month' => date('M', strtotime($row3->Survey_Date)),
							'date_assessed' => $row3->Survey_Date,
							'year' => ''
						);
					}

					$name[] = array(
						'name' => $row2->fname,
						'subject' => $subject
					);
				}


				$json[] = array(
					'dates' 			=> $row1->Survey_Date,
					'names'  			=> $name
				);
			}
		}

		return $json;
	}

	public function getCoordinates() {
		$type = $this->input->get('type');
		$name = $this->input->get('id');

		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'survey_'.$geoarea;
		$user_tbl = 'users';

		if ($type == 'all') {
			$name = empty($name) ? '' : ' WHERE a.RRE_Representative = ' . $name;
			#ADMIN
			if ($this->session->userdata('access_level') == 'Admin') {
				$coordinates = $this->db->query("SELECT a.*, CONCAT(b.fname, ' ', b.lname) as rre FROM ".$table_name." a JOIN ".$user_tbl." b ON a.RRE_Representative = b.globe_id" . $name);
			} else {
				#USER
				$coordinates = $this->db->query("SELECT a.*, CONCAT(b.fname, ' ', b.lname) as rre FROM ".$table_name." a JOIN ".$user_tbl." b ON a.RRE_Representative = b.globe_id WHERE a.RRE_Representative = ?",
				array($this->session->userdata('globe_id')));
			}
		} else {
			$name = empty($name) ? '' : ' AND a.RRE_Representative = ' . $name;
			#GET COORDINATES
			if ($this->session->userdata('access_level') == 'Admin') {
				#FOR ADMIN
				$coordinates = $this->db->query("SELECT a.*, CONCAT(b.fname, ' ', b.lname) as rre FROM ".$table_name." a JOIN  ".$user_tbl." b ON a.RRE_Representative = b.globe_id WHERE DATE(a.Survey_Date) >= CURDATE()" . $name);
			} else {
				#FOR USER
				$coordinates = $this->db->query("SELECT a.*, CONCAT(b.fname, ' ', b.lname) as rre FROM ".$table_name." a JOIN  ".$user_tbl." b ON a.RRE_Representative = b.globe_id WHERE DATE(a.Survey_Date) >= CURDATE() AND b.id = ?",
				array($this->session->userdata('id')));
			}
		}
		return $coordinates->result();
	}

	public function getSurveysTrack() {
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'survey_'.$geoarea;
		$sql = "SELECT count(id) as total,
						(SELECT count(Category) FROM ".$table_name." WHERE Category = 'New Site') as surveyed
						FROM ".$table_name." LIMIT 1";
		$query = $this->db->query($sql);
		return $query->row_array();
	}

	public function getTSSRTrack() {
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'tssr_'.$geoarea;
		$sql = "SELECT count(id) as total,
						(SELECT count(Site_Status) FROM ".$table_name.") as surveyed
						FROM ".$table_name." LIMIT 1";
		$query = $this->db->query($sql);
		return $query->row_array();
	}

	public function get_menus($dep = '') {
		#GET HEADER MENU
		$this->db->order_by("Orders", "asc");
		if (!empty($dep)) {$this->db->where('User_Type', $dep);}

		$get_head_menu = $this->db->get('menus')->result();

		$header_menu = array();
		foreach ($get_head_menu as $row) {
			#GET SUBMENUS
			$sub_menu = [];
			$sub_menu_name = [];
			$this->db->order_by("Order", "asc");
			$this->db->where('url', $row->Base_url);
			$get_head_menu = $this->db->get('menus_sub')->result();

			foreach ($get_head_menu as $rows) {
				$sub_menu[] = array(
					'id' => $rows->id,
					'name' => $rows->Name,
					'icon' => $rows->Icon,
					'base_url' => $rows->Base_url,
					'order' => $rows->Order,
					'dep' => $row->User_Type,
				);
				$sub_menu_name[] = $rows->Base_url;
			}

			$header_menu[] = array(
				'id' => $row->id,
				'order' => $row->Orders,
				'name' => $row->Name,
				'icon' => $row->Icon,
				'base_url' => $row->Base_url,
				'dep' => $row->User_Type,
				'sub_menu' => $sub_menu,
				'sub_menu_name' => $sub_menu_name,
			);
		}

		return $header_menu;
	}

	public function new_menu() {
		#CHECK IF ID IS NOT EMPTY
		$data = array(
			"Orders"		=> $this->input->post('order'),
			"Name"      	=> $this->input->post('name'),
			"Icon"		    => $this->input->post('icon'),
			"Base_url"		=> $this->input->post('base_url'),
			"User_Type"	    => $this->input->post('department'),
		);

		$id = $this->input->post('mid');
		if (empty($id)) {
			#INSERT
			$this->db->insert('menus', $data);
			$msg = 'Successfully inserted data.';
		} else {
			#UPDATE
			$this->db->where('id', $id);
			$this->db->update('menus', $data);
			$msg = 'Successfully updated data.';
		}

		$type = 'success';
		$title = 'Success!';

		$json = array(
			'type'  => $type,
			'title' => $title,
			'msg'   => $msg,
		);

		return $json;
	}

	public function sub_menus() {
		#CHECK IF ID IS NOT EMPTY
		$data = array(
			"Order"		=> $this->input->post('order'),
			"Name"      	=> $this->input->post('name'),
			"Icon"		    => $this->input->post('icon'),
			"Base_url"		=> $this->input->post('base_url'),
		);
		#CHECK IF NOT EMPTY RESULT
		$id = $this->input->post('mid');
		$result = $this->db->get_where('menus_sub', array('id' => $id))->num_rows();
		if ($result > 0) {
			$this->db->where('id', $id);
			$this->db->update('menus_sub', $data);
			return $json = array(
				'title' => 'Success',
				'msg'   => 'Successfully updated.',
				'type'  => 'success'
			);
		} else {
			return $json = array(
				'title' => 'Invalid',
				'msg'   => 'No record found for update.',
				'type'  => 'error'
			);
		}
	}

	public function del_menu() {
		$this->db->where('id', $this->input->post('id'));
		$this->db->delete('menus');

		$json = array(
			'type'  => 'success',
			'title' => 'Success!',
			'msg'   => 'Successfully updated data.',
		);

		return $json;
	}

	public function sub_menu($data) {
		#SAVE CART TO INVENTORY STATUS
		foreach ($data as $row) {
			$this->db->insert('menus_sub', array('url' => $row['url'], 'Order' => $row['Order'], 'Name' => $row['Name'], 'Icon' => $row['Icon'], 'Base_url' => $row['Base_url']));
		}

		$json = array(
			'type'  => 'success',
			'title' => 'Success!',
			'msg'   => 'Successfully updated data.',
		);

		return $json;
	}

	public function main_dash_counts() {
		$geoarea = $this->session->userdata('geoarea');

		if ($geoarea === 'nat') {
			return $data = array(
				'counts' => array(
					'rre' => 0,
					'rfe' => 0,
					'rgpm' => 0,
					'rwe' => 0,
					'rcne' => 0
				),
				'rre'    => 0,
				'rfe'    => 0,
				'rwe'    => 0,
				'rcne'   => 0,
				'rgpm'   => 0
			);
		} else {
			$rfe = $this->db->get('rfe_'.$geoarea);

			$rgpm = $this->db->get('rgpm_'.$geoarea);

			$rcne = $this->db->get('rcne_milo_'.$geoarea);

			$rwe = $this->db->get('rwe_'.$geoarea);

			$rre = $this->db->get('rre_'.$geoarea);

			$counts = array(
				'rre'      => $rre->num_rows(),
				'rfe'      => $rfe->num_rows(),
				'rgpm'     => $rgpm->num_rows(),
				'rcne'     => $rcne->num_rows(),
				'rwe'      => $rwe->num_rows()
			);

			return $data = array(
				'counts' => $counts,
				'rre'    => $this->rre_dash_counts(),
				'rfe'    => $this->rfe_dash_counts(),
				'rwe'    => $this->rwe_dash_counts(),
				'rcne'   => $this->rcne_dash_counts(),
				'rgpm'   => $this->rgpm_dash_counts()
			);
		}


	}

	public function rre_dash_counts() {
		$geoarea = $this->session->userdata('geoarea');
		#SOLUTION
		//---------------------------------------------------//
		#EVENTS
		$this->db->group_by('BTS_Name');
		$this->db->where('Solution_Type', 'EVENTS');
		$events = $this->db->get('rre_'.$geoarea);
		#PERMANENT
		$this->db->group_by('BTS_Name');
		$this->db->where('Solution_Type', 'PERMANENT');
		$per = $this->db->get('rre_'.$geoarea);
		#TEMPORARY
		$this->db->group_by('BTS_Name');
		$this->db->where('Solution_Type', 'TEMPORARY');
		$temp = $this->db->get('rre_'.$geoarea);
		//---------------------------------------------------//
		#2G
		#PHYSICAL
		$this->db->where('FREQ_BAND !=', '0');
		$this->db->group_by('BTS_Name');
		$physical_2g = $this->db->get('2g_rre_'.$geoarea);
		#LOGICAL
		$this->db->where('FREQ_BAND', 'G900');
		$this->db->group_by('BTS_Name');
		$g9 = $this->db->get('2g_rre_'.$geoarea);

		$this->db->where('FREQ_BAND', 'G1800');
		$this->db->group_by('BTS_Name');
		$g18 = $this->db->get('2g_rre_'.$geoarea);

		#INDOOR
		$this->db->where('Site_Type', 'Indoor');
		$this->db->group_by('BTS_Name');
		$in_2g = $this->db->get('2g_rre_'.$geoarea);

		#OUTDOOR
		$this->db->where('Site_Type', 'Outdoor');
		$this->db->group_by('BTS_Name');
		$in_out_2g = $this->db->get('2g_rre_'.$geoarea);

		#INDOOR - OUTDOOR
		$this->db->where('Site_Type', 'Indoor-Outdoor');
		$this->db->group_by('BTS_Name');
		$out_2g = $this->db->get('2g_rre_'.$geoarea);
		//---------------------------------------------------//
		#3G
		#PHYSICAL
		$this->db->where('FREQ_BAND !=', '0');
		$this->db->where('FREQ_BAND !=', '#N/A');
		$this->db->group_by('BTS_Name');
		$physical_3g = $this->db->get('3g_rre_'.$geoarea);
		#LOGICAL
		$this->db->where('FREQ_BAND', 'U900');
		$this->db->where('FREQ_BAND !=', '#N/A');
		$this->db->group_by('BTS_Name');
		$u9 = $this->db->get('3g_rre_'.$geoarea);

		$this->db->where('FREQ_BAND', 'U2100');
		$this->db->where('FREQ_BAND !=', '#N/A');
		$this->db->group_by('BTS_Name');
		$u21 = $this->db->get('3g_rre_'.$geoarea);

		#INDOOR
		$this->db->where('Site_Type', 'Indoor');
		$this->db->where('Site_Type !=', '0');
		$this->db->group_by('BTS_Name');
		$in_3g = $this->db->get('3g_rre_'.$geoarea);

		#OUTDOOR
		$this->db->where('Site_Type', 'Outdoor');
		$this->db->where('Site_Type !=', '0');
		$this->db->group_by('BTS_Name');
		$out_3g = $this->db->get('3g_rre_'.$geoarea);

		#INDOOR-OUTDOOR
		$this->db->where('Site_Type', 'Indoor-Outdoor');
		$this->db->where('Site_Type !=', '0');
		$this->db->group_by('BTS_Name');
		$in_out_3g = $this->db->get('3g_rre_'.$geoarea);
		//---------------------------------------------------//
		#LTE / 4G

		#NOKIA
		$total_ns = $this->db->get('4gns_rre_'.$geoarea)->num_rows();
		#PHYSICAL
		$this->db->where('FREQ_BAND !=', '0');
		$this->db->group_by('BTS_Name');
		$physical_4gns = $this->db->get('4gns_rre_'.$geoarea);
		#L700
		$this->db->where('FREQ_BAND', 'L700');
		$this->db->group_by('BTS_Name');
		$l7_ns = $this->db->get('4gns_rre_'.$geoarea);
		#L1800
		$this->db->where('FREQ_BAND', 'L1800');
		$this->db->group_by('BTS_Name');
		$l18_ns = $this->db->get('4gns_rre_'.$geoarea);
		#L2300
		$this->db->where('FREQ_BAND', 'L2300');
		$this->db->group_by('BTS_Name');
		$l23_ns = $this->db->get('4gns_rre_'.$geoarea);
		#L2600
		$this->db->where('FREQ_BAND', 'L2600');
		$this->db->group_by('BTS_Name');
		$l26_ns = $this->db->get('4gns_rre_'.$geoarea);

		#HUAWEI
		$total_ht = $this->db->get('4ght_rre_'.$geoarea)->num_rows();
		#PHYSICAL
		$this->db->where('FREQ_BAND !=', '0');
		$this->db->group_by('BTS_Name');
		$physical_4ht = $this->db->get('4ght_rre_'.$geoarea);
		#L700
		$this->db->where('FREQ_BAND', 'L700');
		$this->db->group_by('BTS_Name');
		$l7_ht = $this->db->get('4ght_rre_'.$geoarea);
		#L1800
		$this->db->where('FREQ_BAND', 'L1800');
		$this->db->group_by('BTS_Name');
		$l18_ht = $this->db->get('4ght_rre_'.$geoarea);
		#L2300
		$this->db->where('FREQ_BAND', 'L2300');
		$this->db->group_by('BTS_Name');
		$l23_ht = $this->db->get('4ght_rre_'.$geoarea);
		#L2600
		$this->db->where('FREQ_BAND', 'L2600');
		$this->db->group_by('BTS_Name');
		$l26_ht = $this->db->get('4ght_rre_'.$geoarea);

		$two_g = array(
			'total' => $this->db->get('2g_rre_'.$geoarea)->num_rows(),
			'physical' => $physical_2g->num_rows(),
			'g9' => $g9->num_rows(),
			'g18' => $g18->num_rows(),
			'indoor' => $in_2g->num_rows(),
			'outdoor' => $out_2g->num_rows(),
			'inddoor_outdoor' => $in_out_2g->num_rows()
		);

		$three_g = array(
			'total' => $this->db->get('3g_rre_'.$geoarea)->num_rows(),
			'physical' => $physical_3g->num_rows(),
			'u9' => $u9->num_rows(),
			'u21' => $u21->num_rows(),
			'indoor' => $in_3g->num_rows(),
			'outdoor' => $out_3g->num_rows(),
			'inddoor_outdoor' => $in_out_3g->num_rows()
		);

		$lte = array(
			'total' => $total_ns + $total_ht,
			'physical' => $physical_4gns->num_rows() + $physical_4ht->num_rows(),
			'l7' => $l7_ns->num_rows() + $l7_ht->num_rows(),
			'l18' => $l18_ns->num_rows() + $l18_ht->num_rows(),
			'l23' => $l23_ns->num_rows() + $l23_ht->num_rows(),
			'l26' => $l26_ns->num_rows() + $l26_ht->num_rows(),
		);

		$solution = array(
			'events' => $events->num_rows(),
			'permanent' => $per->num_rows(),
			'temporary' => $temp->num_rows()
		);

		return $rre_arr = array(
			'two_g'    => $two_g,
			'three_g'  => $three_g,
			'four_g'   => $lte,
			'solution' => $solution,
		);
	}

	public function rwe_dash_counts()
	{
		$geoarea = $this->session->userdata('geoarea');
		$rwe = $this->db->get('rwe_'.$geoarea);

		#ASDL
		$this->db->select('rwe_'.$geoarea.'.*');
		$this->db->from('rwe_'.$geoarea);
		$this->db->like('MSANTECH', 'ADSL');
		$asdl = $this->db->get();
		#VSDL
		$this->db->select('rwe_'.$geoarea.'.*');
		$this->db->from('rwe_'.$geoarea);
		$this->db->like('MSANTECH', 'VDSL');
		$vsdl = $this->db->get();
		#H248
		$this->db->select('rwe_'.$geoarea.'.*');
		$this->db->from('rwe_'.$geoarea);
		$this->db->like('MSANTECH', 'H248');
		$h248 = $this->db->get();
		#GPSON
		$this->db->select('rwe_'.$geoarea.'.*');
		$this->db->from('rwe_'.$geoarea);
		$this->db->like('MSANTECH', 'GPON');
		$gpson = $this->db->get();

		return $arr = array(
			'rwe'    => $rwe->num_rows(),
			'ASDL'   => $asdl->num_rows(),
			'VSDL'   => $vsdl->num_rows(),
			'H248'   => $h248->num_rows(),
			'GPON'   => $gpson->num_rows(),
		);
	}

	public function rcne_dash_counts() {
		$geoarea = $this->session->userdata('geoarea');

		#MILO
		$this->db->select('*');
		$this->db->from('rcne_milo_'.$geoarea);
		$milo = $this->db->get();

		#NAP AUGMENTED
		$this->db->select('*');
		$this->db->from('rcne_nap_'.$geoarea);
		$nap = $this->db->get();

		return $arr = array(
			'rcne'   => $milo->num_rows() + $nap->num_rows(),
			'milo'    => $milo->num_rows(),
			'nap'     => $nap->num_rows()
		);
	}

	public function rgpm_dash_counts() {
		$geoarea = $this->session->userdata('geoarea');
		$rgpm = $this->db->get('rgpm_'.$geoarea);

		#TRFS
		$trfs = $this->db-> get_where('rgpm_'.$geoarea, array('TRFS !=' => NULL));
		#CRFS
		$crfs = $this->db-> get_where('rgpm_'.$geoarea, array('CRFS !=' => NULL));
		#APPROVED
		$approved = $this->db-> get_where('rgpm_'.$geoarea, array('STATUS =' => 'APPROVED'));
		#CONDITIONAL
		$conditional = $this->db-> get_where('rgpm_'.$geoarea, array('STATUS =' => 'CONDITIONALLY APPROVED'));
		#REJECT
		$reject = $this->db-> get_where('rgpm_'.$geoarea, array('STATUS =' => 'REJECT'));
		return $arr = array(
			'rgpm'         => $rgpm->num_rows(),
			'trfs'         => $trfs->num_rows(),
			'crfs'         => $crfs->num_rows(),
			'approved'     => $approved->num_rows(),
			'conditional'  => $conditional->num_rows(),
			'rej'          => $reject->num_rows()
		);
	}

	public function rfe_dash_counts() {
		$geoarea = $this->session->userdata('geoarea');
		$rfe = $this->db->get('rfe_'.$geoarea);

		#GREENFIELD
		$this->db->select('rfe_'.$geoarea.'.*');
		$this->db->from('rfe_'.$geoarea);
		$this->db->where('SITE_TYPE', 'Greenfield');
		$gf = $this->db->get();
		#IBS
		$this->db->select('rfe_'.$geoarea.'.*');
		$this->db->from('rfe_'.$geoarea);
		$this->db->where('SITE_TYPE', 'IBS');
		$ibs = $this->db->get();
		#ROOFTOP
		$this->db->select('rfe_'.$geoarea.'.*');
		$this->db->from('rfe_'.$geoarea);
		$this->db->where('SITE_TYPE', 'Rooftop');
		$rooftop = $this->db->get();
		#SMALL CELL
		$this->db->select('rfe_'.$geoarea.'.*');
		$this->db->from('rfe_'.$geoarea);
		$this->db->where('SITE_TYPE', 'Small Cell');
		$sm = $this->db->get();
		#SMALL CELL
		$this->db->select('rfe_'.$geoarea.'.*');
		$this->db->from('rfe_'.$geoarea);
		$this->db->where('SITE_TYPE', 'Greenfield- SWAT');
		$this->db->or_where('SITE_TYPE', 'Rooftop-SWAT');
		$swat = $this->db->get();
		return $arr = array(
			'rfe'      => $rfe->num_rows(),
			'gf'       => $gf->num_rows(),
			'ibs'      => $ibs->num_rows(),
			'rooftop'  => $rooftop->num_rows(),
			'sm'       => $sm->num_rows(),
			'swat'     => $swat->num_rows()
		);
	}

	public function get_tables() {
//		$search = empty($s) ? '' : $this->input->post('search');
		$array = array();
		$this->load->dbutil();
		if ($this->dbutil->database_exists('gt_fortress')) {
			$tables =  $this->db->list_tables();
			foreach ($tables as $table)
			{
				$array[] = array(
					'name' => $table,
					'count' => $this->db->get($table)->num_rows(),
					'fields' => $this->db->get($table)->num_fields()
				);

			}
		} else {
			$array = array(
				'title' => 'Oops.',
				'msg'   => 'No database found or imported.',
				'type'  => 'error'
			);
		}
		return $array;
	}

	public function back_up_db() {
		$this->load->library('zip');
		$this->load->dbutil();
		$type = $this->input->get('type');
		if ($this->dbutil->database_exists('gt_fortress'))
		{
			$backup = $this->dbutil->backup();
			$dbname='gt-fortress_('.date("Y-m-d").').'.$type;
			$save = './backup/'.$dbname;
			$this->load->helper('file');
			write_file($save, $backup);

			if(file_exists($save)) {
				$this->load->helper('download');
				force_download($dbname, $save);
			} else {
				return $json = array(
					'title' => 'Invalid',
					'msg'   => 'File does not exist.',
					'type'  => 'error'
				);
			}
		} else {
			return $json = array(
				'title' => 'Invalid',
				'msg'   => 'Database gt_fortress does not found.',
				'type'  => 'error'
			);
		}
	}

	public function download_db() {
		$name = $this->input->post('name');
		$type = $this->input->post('type');
		$this->load->dbutil();

		if ($this->db->table_exists($name))
		{
			if ($type == 'csv') {
				return $this->db->get($name)->result();
			} else {
				$backup = $this->dbutil->backup();
				$dbname = $name.'-('.date("Y-m-d").').'.$type;
				$save = './backup/'.$dbname;
				$this->load->helper('file');
				write_file($save, $backup);
				if(file_exists($save)) {
					$this->load->helper('download');
					force_download($dbname, $save);
				} else {
					return $json = array(
						'title' => 'Invalid',
						'msg'   => 'File does not exist.',
						'type'  => 'error'
					);
				}
			}
		} else {
			return $json = array(
				'title' => 'Invalid',
				'msg'   => 'Table '.$name.' does not found.',
				'type'  => 'error'
			);
		}

	}

	public function delete_db() {
		$dbname = $this->input->post('file');
		if(file_exists($dbname)) {
			unlink($dbname);
		} else {
			return $json = array(
				'title' => 'Invalid',
				'msg'   => 'File does not exist.',
				'type'  => 'error'
			);
		}

	}
}
