
<div class="mg-top-77">
	<div class="">
		<div class="container-fluid">
			<nav aria-label="breadcrumb mg-top-10">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#" onclick="history.back()">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page"><?= $this->session->userdata('department'); ?> </li>
				</ol>
			</nav>
			<div class="row">
				<div class="col-12">
					<div class="card-group dash-blocks text-center rounded shadow-lg"></div>
				</div>

				<div class="col-9 mg-bot-10">
					<div class="row">
						<div class="col-12 pad-right-0 mg-top-10">
							<?php require_once 'map.php'; ?>
						</div>

						<div class="col-12 pad-right-0 mg-top-10">
							<?php require_once 'projects.php'; ?>
						</div>
					</div>
				</div>

				<div class="col-3 mg-top-10 mg-bot-10">
					<?php require_once 'Activies.php'; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('/layout/footer'); ?>
<script src="<?= base_url('action/map.js'); ?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJiNdQZEwhcv8Hep7SF8qDo2SJDM4Ttdw" async defer></script>
<script src="<?= base_url('action/rre_dashboard.js'); ?>"></script>
<script src="<?= base_url('action/calendar.js'); ?>"></script>
