	<div class="container-fluid mg-top-77" style="background-color: rgba(0, 0, 0, 0.2);">
		<div class="col">
			<div class="row" style="font-size: 0.9rem">
				<div class="col-md col-lg col-sm-12 col-xs-12 p-2 my-2 bg-light rounded">
					<div class="row mb-3 align-items-center mx-1 p-1 bg-dark text-light rounded-top">
						<div class="col text-left">
							<a href="<?php echo base_url('sites'); ?>">
								<button class="btn btn-outline-success btn-sm" type="button">
									<img class="icon_size" src="<?php echo base_url().'style/icons/database-5-64.png'; ?>"> Back to Sites
								</button>
							</a>
						</div>
						<div class="col text-left mx-auto mt-1"><h4>Site Profile</h4></div>
					</div>
<!-- 					<div class="col m-2 mx-auto input-group" style="width: 30rem">
						<input type="text" name="searchquery" id="searchquery" class="form-control form-control-sm border border-secondary" aria-label="Text input with dropdown button" hidden>
						<div class="input-group-append"	>
							<button class="btn btn-outline-secondary btn-sm" name="searchquery_btn" id="searchquery_btn" type="button" hidden>Search Site</button>
						</div>
					</div> -->
					<div class="row m-1">
						<div class="col-4">
							<div class="row">
								<div class="shadow col-md col-lg col-sm-12 col-xs-12 p-2 mr-2 bg-light border border-secondary rounded">
									<fieldset>
										<h5 class="font-weight-normal text-left">Virtual Map</h5>
										<div id="main_map" style="height:350px;"></div>
									</fieldset>
								</div>
							</div>
						</div>
						<div class="col">
							<div class="row">
								<div class="col-8">
									<div class="row">
										<div class="shadow col-md col-lg col-sm-12 col-xs-12 p-2 mx-2 bg-light border border-success rounded" style="height: 400px">
											<div class="row mb-1">
												<h5 class="font-weight-normal text-left ml-3 mb-0 mt-1 col-2">Sitename: </h5>
												<h4 id="sitename" class="text-left ml-4 mb-1 mt-0 pl-0 col-6"></h4>
												<div class="col-3 text-right p-0 pr-4">
													<button id="siteedit" class="btn form-control-xs p-1 border"><i class="fas fa-edit"></i></button>
												</div>
											</div>
											<div id="siteprofilediv" class="col">

											</div>
										</div>
									</div>
								</div>
								<div class="col-4">
									<div class="row">
										<div class="shadow col-md col-lg col-sm-12 col-xs-12 p-2 ml-2 bg-light border border-secondary rounded" style="height: 220px">
											<h5 class="font-weight-normal text-left">RF Configuration</h5>
											<div class="col">
												<table class="table table-responsive table-striped table-hover table-borderless table-xs">
													<thead>
													</thead>
													<tbody>
														<tr>
															<th scope="row">GSM900: </th>
															<td>2+2+2+0+0+0</td>
														</tr>
														<tr>
															<th scope="row">GSM1800: </th>
															<td>2+2+2+0+0+0</td>
														</tr>
														<tr>
															<th scope="row">UMTS900: </th>
															<td>2+2+2+0+0+0</td>
														</tr>
														<tr>
															<th scope="row">UMTS2100: </th>
															<td>2+2+2+0+0+0</td>
														</tr>
														<tr>
															<th scope="row">L700: </th>
															<td>2+2+2+0+0+0</td>
														</tr>
														<tr>
															<th scope="row">L1800: </th>
															<td>2+2+2+0+0+0</td>
														</tr>
														<tr>
															<th scope="row">L2600: </th>
															<td>2+2+2+0+0+0</td>
														</tr>
														<tr>
															<th scope="row">L2600 MM: </th>
															<td>2+2+2+0+0+0</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="row" style="height: 180px">
										<div class="shadow col-md col-lg col-sm-12 col-xs-12 p-2 ml-2 mt-3 bg-light border border-danger rounded">
											<h5 class="font-weight-normal text-left">Site History</h5>
											<div>
												Something here. . . .
											</div>

										</div>
									</div>
								</div>

							</div>

						</div>
						</div>
						<div class="col mt-3 p-1">
							<div class="shadow col-md col-lg col-sm-12 col-xs-12 p-2 px-3 bg-light border border-primary rounded">
								<div id="celltabs" class="row">
									<h5 class="font-weight-normal text-left mb-0 mt-1 col-10">Antenna Details: </h5>
									<div class="col-2 text-right p-0 pr-4">
										<button id="celledit" class="btn form-control-xs p-1 border"><i class="fas fa-edit"></i></button>
									</div>
									<ul class="nav nav-tabs ml-3 mt-2" style="width: 20%;" role="tablist" aria-label="First group">
										<li class="nav-item text-center" style="width: 30%;">
											<a id="2g" class="nav-link text-dark py-1 px-2 show active" data-toggle="tab" href="#">2G</a>
										</li>
										<li class="nav-item text-center" style="width: 30%;">
											<a id="3g" class="nav-link text-dark py-1 px-2" data-toggle="tab" href="#">3G</a>
										</li>
										<li class="nav-item text-center" style="width: 30%;">
											<a id="4g" class="nav-link text-dark py-1 px-2" data-toggle="tab" href="#">4G</a>
										</li>
									</ul>
								</div>
								<div id="celltabarea" class="table-responsive fade in show col p-0" style="overflow-x:auto; white-space: nowrap;">

								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade pad-00" data-backdrop="static" data-keyboard="false" id="siteprofilemodal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog mg-top-0" role="document">
			<div class="modal-content">
				<div class="modal-header p-1">
					<!-- <h5 class="modal-title" id="siteprofilemodal"></h5> -->
					<div id="modalheaderdiv">


					</div>
					<!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button> -->
				</div>
				<div class="modal-body mx-2 py-0" id="" style="overflow-x:auto; white-space: nowrap;">


<!-- 						<table class="m-0 table table-striped table-hover table-sm table-light table-responsive">
							<thead class="thead-dark" >

							</thead>
							<tbody>

						</table> -->
					</div>
					<div class="modal-footer p-2" id="siteprofilemodalfooter">

					</div>

				</div>
			</div>
		</div>


	</body>
	<?php $this->load->view('/layout/footer'); ?>
	<script src="action/siteprofile_actions.js"></script>
	<script src="action/map_google.js"></script>
	</html>
