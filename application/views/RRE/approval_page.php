<?php $this->load->view('/layout/header'); ?>
<?php $this->load->view('/layout/side_menu'); ?>
	<div class="container-fluid mg-top-77">
			<nav class="mg-top-10 p-3 bg-secondary rounded navbar navbar-expand-lg navbar-dark indigo mb-4 text-white" id="checkbox">

				<!-- Navbar brand -->
				<h3 class="navbar-brand">Approvals </h3>

				<div class="form-check form-check-inline ">
					<input class="form-check-input " type="checkbox" id="unapproved" checked>
					<label class="form-check-label" for="unapproved">Unapproved</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="forreview">
					<label class="form-check-label" for="forreview">For Review</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="approved">
					<label class="form-check-label" for="approved">Approved</label>
				</div>

				<div class="form-check form-check-inline">
					<div class="col-4">
						<input class="rowperpage-input form-control" value="10" min="0" type="number" id="rowperpage-input">
					</div>
					<label for="rowperpage-input" class="form-check-label">row per page</label>
				</div>
				<!-- Collapsible content -->
				<div class="collapse navbar-collapse" id="navbarSupportedContent">

					<form class="form-inline ml-auto">
						<div class="md-form my-0">
							<input class="form-control" type="text" placeholder="Search" aria-label="Search">
						</div>
						<button href="#!" class="btn btn-outline-white btn-md my-0 ml-sm-2" type="submit">Search</button>
					</form>

				</div>
				<!-- Collapsible content -->

			</nav>
			<div class="mx-4">
				<center>
					<div class="emptyAprrov">
						<i class="far fa-folder-open fs-9"></i>
					</div>
				</center>
				<table class="table table-striped table-xs table-layout-fixed table-bordererd text-center" id="approvalpaginationtable">
					<thead>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>

		<center>
			<div style='margin-top: 33px;' class="text-center" id='approvalpagination'> </div>
		</center>

<!-- 		<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Large modal</button> -->

		<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-xl">
				<div class="modal-content">
					<div class="modal-header">
						<h6 class="modal-title" id="exampleModalLabel">Approval Details</h6>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body row">
						<table id="approvalmodal1" class="table table-striped table-xs">
							<thead>
							</thead>
							<tbody>

							</tbody>
						</table>
						<div class="col-10">
							<table id="approvalmodal2" class="table table-striped table-reponsive table-xxs">
								<thead>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
						<div id="remarks-dialog-div" class="form-group col-2">
							<label class="font-weight-bold"><h5>Remarks</h5></label>
							<blockquote class="blockquote text-left">
									<p id="remarks-dialog-p"><small></small></p>
								<footer class="blockquote-footer">by Admin <cite id="cite"title="Source Title">date</cite></footer>
							</blockquote>
							<textarea class="form-control" id="remarks-text-area" rows="1" placeholder="enter new remarks here"></textarea>
						</div>
					</div>

					<div id="modal-footer" class="modal-footer">
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="inlineRadioOptions" id="forreview-radio" value="option1">
							<label class="form-check-label" for="forreview-radio">For Review</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="inlineRadioOptions" id="approve-radio" value="option2">
							<label class="form-check-label" for="approve-radio">Approve</label>
						</div>
						<button id="submitapprovalbutton" type="button" class="btn btn-primary btn-sm">Submit</button>
						<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
<?php $this->load->view('/layout/footer'); ?>
	<script src="action/admin_actions.js"></script>
</html>
