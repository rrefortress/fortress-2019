<?php if ($this->session->userdata('access_level') == 'Standard') { ?>
	<div class="mg-top-77">
		<div class="container-fluid hm-520 shadow-lg border border-secondary">
			<div class="row">
				<div class="col-5 col-sm-5 col-md-5 col-xl-5 pad-00 border-right border-secondary hm-520">
						<div id="items_lists" class="tab-pane">
							<div class="col-12 bg-primary text-uppercase text-center text-white mg-bot-10">
								<b>Inventory Lists</b>
							</div>
							<div class="col-12">
								<div class="row">
									<div class="col-2">
										<button class="btn btn-outline-info" id="view_cart" data-toggle="tooltip" data-placement="top" title="View on cart"><i class="fas fa-cart-plus"></i></button>
									</div>
									<div class="col-6">
										<div class="input-group mb-2 mr-sm-2">
											<div class="input-group-prepend">
												<div class="input-group-text"><i class="fas fa-search"></i></div>
											</div>
											<input type="text" class="form-control" id="search_items" placeholder="Search Item Name">
										</div>
									</div>
									<div class="col-4">
											<div class="row">
												<div class="col-5"></div>
												<div class="col-3">
													<div class="text-right btn-add">
														<button class="btn btn-outline-primary" id="btnAddInventory" data-toggle="tooltip" data-placement="top" title="Add Item"><i class="fas fa-plus"></i></button>
													</div>
												</div>
												<div class="col-3">
													<div class="text-right btn-add">
														<button class="btn btn-outline-success" id="btnImportInventory" data-toggle="tooltip" data-placement="top" title="Import Item"><i class="fas fa-file-excel"></i></button>
													</div>
												</div>
											</div>

											<div class="row btn-options d-none">
												<div class="col-5"></div>
												<div class="col-3">
													<div class="text-right btn-edit">
														<button class="btn btn-outline-warning" id="btnEditInventory" data-toggle="tooltip" data-placement="top" title="Edit Item"><i class="fas fa-edit"></i></button>
													</div>
												</div>
												<div class="col-3">
													<div class="text-right btn-del">
														<button class="btn btn-outline-danger" id="btnDelInventory" data-toggle="tooltip" data-placement="top" title="Import Excel File"><i class="fas fa-trash-alt"></i></button>
													</div>
												</div>
											</div>

									</div>
								</div>
							</div>

							<div class="col-12 pad-00 border-secondary border-bottom">
								<div class="inventory_list">
									<table class="table table-hover table-sm">
										<thead>
										<tr>
											<th scope="col" style="width: 10%">
												<div class="custom-control pad-left-rem custom-checkbox">
													<input type="checkbox" class="custom-control-input" id="chk_item">
													<label class="custom-control-label" for="chk_item"></label>
												</div>
											</th>
											<th scope="col" class="text-uppercase text-muted text-center" style="width: 10%">No.</th>
											<th scope="col" class="text-uppercase text-muted text-center">Item Code</th>
											<th scope="col" class="text-uppercase text-muted text-center" style="width: 20%">Image</th>
											<th scope="col" class="text-uppercase text-muted text-center">Name</th>
											<th scope="col" class="text-uppercase text-muted text-right">Quantity</th>
										</tr>
										</thead>
										<tbody id="table_item_lists"></tbody>
									</table>
								</div>
							</div>
							<div class="col-12 pad-00">
								<button class="btn btn-primary btn-block br-none text-uppercase" id="save_to_cart" disabled><i class="fas fa-cart-plus"></i> Add to Cart</button>
							</div>
						</div>
				</div>

				<div class="col-7 col-sm-7 col-md-7 col-xl-7 pad-00">
					<div id="items_lists" class="tab-pane">
						<div class="col-12 bg-success text-uppercase text-center text-white mg-bot-10">
							<b>Item Requested</b>
						</div>
						<div class="col-12">
							<div class="row">
								<div class="col-4">
									<div class="input-group mb-2 mr-sm-2">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-search"></i></div>
										</div>
										<input type="text" class="form-control" id="search_req_items" placeholder="Search">
									</div>
								</div>

								<div class="col-3">
									 <select class="form-control" id="inventory_status">
										 <option disabled selected>Select Status</option>
										 <option value="">All</option>
										 <option value="2">Pending</option>
										 <option value="3">Accept</option>
										 <option value="4">Reject</option>
									 </select>
								</div>

								<div class="col-3">
									<div class="text-right">
										<div class="input-group">
											<input type="text" class="form-control" placeholder="Select Date" id="date_select">
											<div class="input-group-append">
												<span class="input-group-text">
													<a href="#" class="text-muted" id="select_date"><i class="fas fa-calendar-alt"></i></a>
												</span>
											</div>
										</div>
									</div>
								</div>

								<div class="col-2">
										<a type="button" class="btn btn-success btn-sm small" href="#" id="export_as_excel" disabled><i class="fas fa-file-excel"></i> Export</a>
								</div>
							</div>
						</div>

						<div class="col-12 pad-00 border-secondary border-bottom">
							<div class="inventory_list">
								<table class="table table-hover table-sm" id="tbl_requests_lists">
									<thead>
									<tr>
										<th scope="col" style="width: 10%">
											<div class="custom-control pad-left-rem custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="chk_item_req">
												<label class="custom-control-label" for="chk_item_req"></label>
											</div>
										</th>
										<th scope="col" class="text-uppercase text-muted text-center" style="width: 10%">No.</th>
										<th scope="col" class="text-uppercase text-muted text-center">Details</th>
										<th scope="col" class="text-uppercase text-muted text-right" style="width: 5%">Status</th>
										<th scope="col" class="text-uppercase text-muted text-right" style="width: 25%">Date</th>
									</tr>
									</thead>
									<tbody id="table_requests_lists"></tbody>
								</table>
							</div>
						</div>
						<div class="col-12 pad-00">
							<button class="btn btn-success btn-block br-none text-uppercase" id="return_item" disabled><i class="fas fa-undo-alt"></i> Return Item</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="inventory_modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal_title text-uppercase text-muted">Add Item</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form id="save_item" method="POST">
						<input type="hidden" id="id" name="id">

						<div class="row">
							<div class="col-6 border-right">
								<div class="form-group">
								<center>
									<div class="col-sm-6 imgUp text-center">
										<img src="<?= base_url('./style/images/no_product.png') ?>" class="img-thumbnail imagePreview">
										<label class="btn border w-155 text-uppercase text-muted">
											<i class="fas fa-camera"></i> Upload
											<input type="file" class="uploadFile" name="image_file" accept="image/*" style="width: 0px;height: 0px;overflow: hidden;">
										</label>
									</div>
								</center>
								</div>
								<!-- <div class="form-group">
									<textarea name="purpose" class="form-control" rows="3" cols="80" placeholder="Enter Area of deployment / Purpose" required></textarea>
								</div> -->
							</div>
							<div class="col-6">
								<div class="form-group">
									<input type="text" name="item_code" class="form-control form-control-sm text-center bg-white border-0" id="item_code" readonly>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-9">
											<input type="text" name="name" class="form-control form-control-sm" id="name" placeholder="Enter item name" required>
										</div>
										<div class="col-3 mx-md-n3">
											<input type="number" name="quantity" class="form-control form-control-sm" min="0" max="10" id="quantity" placeholder="0" required>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="serial_append"></div>
								</div>
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary" id="btn_save">Submit</button>
				</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="cart_preview_modal" tabindex="-1" role="dialog" data-backdrop="static"  aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<form id="save_append_items">
					<div class="modal-header">
						<h5 class="modal-title modal_title text-uppercase text-muted">Preview Items</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="col">
							<div class="appendItems"></div>
						</div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<button type="submit" class="btn btn-primary" id="btn_save">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="import_view_modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-center text-uppercase" id="exampleModalLabel">Import</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form method="post" id="file-upload" enctype="multipart/form-data">
							<div class="files">
								<p class="text-center lead h4">Upload File <i>(accepts .csv, .xlsx & .xls files only)</i></p>
								<input type="file" name="userfile" id="filename" class="form-control btn" accept=".csv, .xlsx, .xls" required>
							</div>

							<div class="container">
								<div class='progress bg-secondary' id="progressDivId">
									<div class="progress-bar" id="progressBar"></div>
									<div class="percent text-white" id="percent">0%</div>
								</div>
							</div>
					</div>
						<div class="modal-footer">
							<div class="col-12 text-center">
								<button class="btn btn-outline-danger text-center" type="button" id="clear_data_file"><i class="fas fa-eraser"></i> Remove</button>
								<button class="btn btn-outline-primary text-center" id="submitButton" type="submit" id="inputGroupFileAddon04"><i class="fas fa-file-upload"></i> Upload</button>
							</div>
						</div>
					</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="cart_view_modal" tabindex="-1" role="dialog" data-backdrop="static"  aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<form id="save_requests_items">
					<div class="modal-header">
						<h5 class="modal-title modal_title text-uppercase text-muted">Cart Items</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="col">
							<div id="cart_items"></div>
							<div class="form-group">
						    <textarea class="form-control" id="purpose" name="purpose" rows="3" placeholder="Purpose or Area of deployment" required></textarea>
						  </div>
						</div>
					</div>
					<div class="modal-footer modal-cart-footer d-none">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<button type="submit" class="btn btn-primary" id="btn_save">Request</button>
					</div>
				</form>
			</div>
		</div>
	</div>
<?php } else { ?>
	<div class="mg-top-77">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-xl-12 pad-00">
					<div id="items_lists" class="tab-pane hm-520">
						<div class="col-12 bg-success text-uppercase text-center text-white">
							<b>Requested Lists</b>
						</div>
						<div class="col-12">
							<div class="row border-secondary">
								<div class="col-2"></div>
								<div class="col-8 border-secondary shadow-sm border-left border-right">
									<div class="row">
										<div class="col-4 mg-top-10">
											<div class="input-group mb-2 mr-sm-2">
												<div class="input-group-prepend">
													<div class="input-group-text"><i class="fas fa-search"></i></div>
												</div>
												<input type="text" class="form-control" id="search_req_items" placeholder="Search item name">
											</div>
										</div>

										<div class="col-3 mg-top-10">
											<div class="form-group">
										     <select class="form-control" id="name">
													 <option disabled selected>Select Name</option>
										     </select>
										   </div>
										</div>

										<div class="col-4 mg-top-10">
											<div class="row">
												<div class="col-8">
													<div class="input-group">
														<input type="text" class="form-control" placeholder="Select Date" id="date_select">
														<div class="input-group-append">
															<span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
														</div>
													</div>
												</div>
												<div class="col-2">
													<button class="btn btn-outline-success" id="accept_request" data-toggle="tooltip" data-placement="top" title="Accept" disabled><i class="fas fa-check"></i></button>
												</div>
												<div class="col-2">
													<button class="btn btn-outline-danger" id="reject_request" data-toggle="tooltip" data-placement="top" title="Reject" disabled><i class="fas fa-times"></i></button>
												</div>
												<!-- <div class="col-2">
													<button class="btn btn-outline-info" id="view_request" data-toggle="tooltip" data-placement="top" title="View Details" disabled><i class="fas fa-eye"></i></button>
												</div> -->
											</div>
										</div>
									</div>
								</div>
								<div class="col-2"></div>
							</div>
						</div>

						<div class="col-12 border-secondary">
							<div class="row">
								<div class="col-2"></div>
								<div class="col-8 shadow-sm pad-00 border-secondary border-left border-right">
									<div class="inventory_list_admin">
										<table class="table table-hover table-sm" id="tbl_requests_lists">
											<thead>
											<tr>
												<th scope="col" style="width: 10%">
													<div class="custom-control pad-left-rem custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="chk_item">
														<label class="custom-control-label" for="chk_item"></label>
													</div>
												</th>
												<th scope="col" class="text-uppercase text-muted text-center" style="width: 10%">No.</th>
												<th scope="col" class="text-uppercase text-muted text-center">Details</th>
											</tr>
											</thead>
											<tbody id="table_requests_lists"></tbody>
										</table>
									</div>
								</div>
								<div class="col-2"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
