	<div class="container-fluid mg-top-77">
		<div class="shadow-lg border border-secondary col-12">
			<table class="table  table-striped table-xs compact table-hover" id="mdbupdates">
					<thead>
						<tr>
							<th scope="col">Update ID</th>
							<th scope="col">User ID</th>
							<th scope="col">Update Type</th>
							<th scope="col">Database</th>
							<th scope="col">Date Created</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
		</div>

		<center>
			<div style='margin-top: 33px;' class="text-center" id='approvalpagination'> </div>
		</center>

		<div id="mdbupdatesmodal"class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-xl">
				<div class="modal-content">
					<div class="modal-header">
						<h6 class="modal-title" id="exampleModalLabel">Update Details</h6>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body row py-0">
						<div class="col-12">
							<table id="approvalmodal1" class="table table-striped table-xxs">
							<thead>
							</thead>
							<tbody>

							</tbody>
						</table>
						</div>
						<div class="col-12">
							<table id="approvalmodal2" class="table table-striped table-xxxs table-responsive">
								<thead>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
<!-- 						<div id="remarks-dialog-div" class="form-group col-2">
							<label class="font-weight-bold"><h5>Remarks</h5></label>
							<blockquote class="blockquote text-left">
									<p id="remarks-dialog-p"><small></small></p>
								<footer class="blockquote-footer">by Admin <cite id="cite"title="Source Title">date</cite></footer>
							</blockquote>
							<textarea class="form-control" id="remarks-text-area" rows="1" placeholder="enter new remarks here"></textarea>
						</div> -->
					</div>

					<div id="modal-footer" class="modal-footer">
						<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
