<?php $this->load->view('/layout/header'); ?>
<?php $this->load->view('/layout/side_menu'); ?>
	<div class="mg-top-77 shadow-lg border border-secondary">
    <div class="container-fluid">
      <div class="row">
        <div class="col-3 border-secondary border-right pad-00 MS-520">
          <div class="row">
            <div class="col-12 sticky-top mg-top-10 bg-white">
              <div class="row">
                <div class="container">
                  <div class="col-12">
                    <div class="input-group mb-2 mr-sm-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-search"></i></div>
                      </div>
                      <input type="text" class="form-control" id="nomSearch" placeholder="Search Site Name">
                    </div>
                  </div>
                </div>
              </div>
            </div>


            <div class="col-12 hm-406">
              <table class="table table-striped table-sm table-hover ms_lists">
                <thead>
                  <tr>
                    <th scope="col" style="width:15%" class="text-center">#</th>
                    <th scope="col">Name</th>
                  </tr>
                </thead>
                <tbody id="nom_datas"></tbody>
              </table>
            </div>

            <div class="col-12">
              <center>
                <div class="text-center" id='nom_pagination'> </div>
              </center>
            </div>
          </div>
        </div>

        <div class="col-9 ms_hm">
					<div class="row">
						<div class="col-12 sticky-top bg-white shadow-sm border-bottom border-secondary">
							<div id="main_map"></div>
						</div>

						<div class="col-12">
              <div class="container">
                <div class="row row-cols-2 text-center px-2">
                    <div class="col">Column</div>
                    <div class="col">Column</div>
                    <div class="col">Column</div>
                    <div class="col">Column</div>
                </div>
                <div class="row row-cols-2 text-center px-2">
                    <div class="col">Column</div>
                    <div class="col">Column</div>
                    <div class="col">Column</div>
                    <div class="col">Column</div>
                </div>
                <div class="row row-cols-2 text-center px-2">
                    <div class="col">Column</div>
                    <div class="col">Column</div>
                    <div class="col">Column</div>
                    <div class="col">Column</div>
                </div>
                <div class="row row-cols-2 text-center px-2">
                    <div class="col">Column</div>
                    <div class="col">Column</div>
                    <div class="col">Column</div>
                    <div class="col">Column</div>
                </div>
              </div>
						</div>
            <div class="col-12 border-secondary border-top text-center">
              <button type="button" class="btn btn-primary">Save to AWE</button>
            </div>

					</div>
				</div>
      </div>

    </div>
</div>
<?php $this->load->view('/layout/footer'); ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJiNdQZEwhcv8Hep7SF8qDo2SJDM4Ttdw"
	async defer></script>
<script src="<?= base_url('action/nominations.js'); ?>"></script>
<script src="<?= base_url('action/calendar.js'); ?>"></script>
