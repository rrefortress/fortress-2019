<div class="container-fluid">
	<div class="row pad-00">
		<div class="emptyTracks d-none">
			<div class="content">
				<h5 class="text-center text-white">
					<i class="fas fa-chart-area display-1 text-dark"></i><br>
					<h4 class="text-dark text-center">Empty Data.</h4>
					<label class="text-dark text-center">No data has been uploaded yet.</label>
				</h5>
			</div>
		</div>

		<div class="col-12 pad-00">
			<input type="hidden" id="img-color" value="blue">
			<ul class="nav nav-pills nav-justified bg-white shadow-lg mg-top-10 border border-top border-left-0 border-right-0 border-secondary" id="trackTab" role="tablist">
				<li class="nav-item border-left">
					<a class="nav-link active small" id="nom-tab" data-toggle="pill" href="#nom" role="tab" aria-controls="nom"
						 aria-selected="true">Nominations</a>
				</li>

				<li class="nav-item border-left">
					<a class="nav-link small" id="awe-tab" data-toggle="pill" href="#awe" role="tab" aria-controls="awe"
						 aria-selected="true">AWE</a>
				</li>

				<li class="nav-item border-left">
					<a class="nav-link small" id="pmo-tab" data-toggle="pill" href="#pmo" role="tab" aria-controls="pmo"
						 aria-selected="true">PMO</a>
				</li>

				<li class="nav-item border-right border-left">
					<a class="nav-link small" id="surveys-tab" data-toggle="pill" href="#surveys" role="tab" aria-controls="surveys"
					   aria-selected="false">Surveys</a>
				</li>

				<li class="nav-item border-right">
					<a class="nav-link small" id="ddd-tab" data-toggle="pill" href="#ddd" role="tab" aria-controls="ddd"
					   aria-selected="false">DDD</a>
				</li>

				<li class="nav-item border-right">
					<a class="nav-link small" id="ibs-tab" data-toggle="pill" href="#ibs" role="tab" aria-controls="ibs"
					   aria-selected="false">IBS</a>
				</li>

				<li class="nav-item border-right">
					<a class="nav-link small" id="tssr-tab" data-toggle="pill" href="#tssr" role="tab" aria-controls="tssr"
					   aria-selected="false">TSSR</a>
				</li>

				<li class="nav-item border-right">
					<a class="nav-link small" id="clp-tab" data-toggle="pill" href="#clp" role="tab" aria-controls="clp"
					   aria-selected="false">CLP</a>
				</li>

				<li class="nav-item border-right">
					<a class="nav-link small" id="salesmap-tab" data-toggle="pill" href="#salesmap" role="tab" aria-controls="salesmap"
					   aria-selected="false">Sales Map</a>
				</li>

				<li class="nav-item border-right">
					<a class="nav-link small" id="complaints-tab" data-toggle="pill" href="#complaints" role="tab" aria-controls="complaints"
					   aria-selected="false">Complaints</a>
				</li>

				<li class="nav-item border-right">
					<a class="nav-link small" id="events-tab" data-toggle="pill" href="#events" role="tab" aria-controls="events"
					   aria-selected="false">Events</a>
				</li>
			</ul>
		</div>

		<div class="loader center">
			<div class="content vertical-center">
				<h5 class="text-center text-white">
					<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>
					<h4 class="text-dark text-center">Loading</h4>
				</h5>
			</div>
		</div>

		<div class="tab-content mg-top-10 border border-secondary" id="myTabContent">
				<div class="tab-pane fade shadow-sm show active" id="nom" role="tabpanel" aria-labelledby="nom-tab">
					<?php require_once('ChartTrack/nominations_page.php')?>
				</div>

				<div class="tab-pane fade shadow-sm" id="awe" role="tabpanel" aria-labelledby="awe-tab">
					<?php require_once('ChartTrack/awe_page.php')?>
				</div>

				<div class="tab-pane fade shadow-sm" id="pmo" role="tabpanel" aria-labelledby="pmo-tab">
					<?php require_once('ChartTrack/pmo_page.php')?>
				</div>

				<div class="tab-pane fade shadow-sm" id="surveys" role="tabpanel" aria-labelledby="surveys-tab">
					<?php require_once('ChartTrack/survey_track_page.php')?>
				</div>

				<div class="tab-pane fade shadow-sm" id="ddd" role="tabpanel" aria-labelledby="ddd-tab">
					<?php require_once('ChartTrack/ddd_tracker_page.php')?>
				</div>

				<div class="tab-pane fade shadow-sm" id="ibs" role="tabpanel" aria-labelledby="ibs-tab">
					<?php require_once('ChartTrack/ibs_page.php')?>
				</div>

				<div class="tab-pane fade shadow-sm" id="tssr" role="tabpanel" aria-labelledby="tssr-tab">
					<?php require_once('ChartTrack/tssr_page.php')?>
				</div>

				<div class="tab-pane fade shadow-sm" id="clp" role="tabpanel" aria-labelledby="clp-tab">
					<?php require_once('ChartTrack/clp_page.php')?>
				</div>

				<div class="tab-pane fade shadow-sm" id="salesmap" role="tabpanel" aria-labelledby="salesmap-tab">
					<?php require_once('ChartTrack/sales_map_page.php')?>
				</div>

				<div class="tab-pane fade shadow-sm" id="complaints" role="tabpanel" aria-labelledby="complaints-tab">
					<?php require_once('ChartTrack/complaints_page.php')?>
				</div>

				<div class="tab-pane fade shadow-sm" id="events" role="tabpanel" aria-labelledby="events-tab">
					<?php require_once('ChartTrack/events_page.php')?>
				</div>
		</div>
	</div>
</div>
</div>
