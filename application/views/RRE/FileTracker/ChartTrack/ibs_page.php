<div class="ibsTracks col-12">
	<div class="row">

				<div class="col-2 mg-top-10 pad-lef-right-0 shadow-lg">
					<ul class="list-group nav flex-column nav-pills trackPills" id="ibs-pills-tab" role="tablist" aria-orientation="vertical">
						<li class="nav-item">
							<a class="nav-link text-dark active tracks small" id="ibs-pills-dash-tab" data-toggle="pill" href="#ibs-pills-dash" role="tab" aria-controls="ibs-pills-dash" aria-selected="true"><i class="fas fa-chart-bar"></i> Charts</a>
						</li>

						<li class="nav-item">
							<a class="nav-link tracks small <?= $this->session->userdata('access_level') == 'Admin' ? 'text-dark' : 't-disabled' ?>" id="ibs-pills-lists-tab" data-toggle="pill" href="#ibs-pills-lists" role="tab" aria-controls="ibs-pills-lists" aria-selected="false"><i class="fas fa-list-alt"></i> Details &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="small badge badge-primary badge-pill text-right pull-right total_ibs_details">0</span></a>
						</li>
						<?php if ($this->session->userdata('access_level') == 'Admin') { ?>
						<?php } ?>
					</ul>
				</div>

				<div class="col-10 border-left border-secondary">
					<div class="tab-content" id="ibs-pills-tabContent">

						<div class="tab-pane fade show active" id="ibs-pills-dash" role="tabpanel" aria-labelledby="ibs-pills-dash-tab">
								<div class="emptyibsChartTrack d-none">
									<?php require('emptyTrack.php')?>
								</div>

								<div class="row">
									<div class="col-12 nomFilter d-none mg-top-10">
									</div>
									<div class="col-12">
										<div id="chartsIBS"></div>
									</div>
								</div>
								<!-- <div class="container">
									<div class="row">
										<div class="col-12 aweFilter mg-top-10">
											<div class="row">
												<div class="col-12"></div>
												<div class="col-3">
													<div class="form-group">
														<select class="form-control" id="aweProvince"></select>
													</div>
												</div>
											</div>
										</div>
										<div class="col-12">
											<div id="chartsIBS"></div>
										</div>
									</div>
								</div> -->
						</div>

						<div class="tab-pane fade" id="ibs-pills-lists" role="tabpanel" aria-labelledby="ibs-pills-lists-tab">
							<div class="container">
								<div class="row">
									<div class="emptyibsTrack d-none">
										<?php require('emptyTrack.php')?>
									</div>

									<div class="col-12 pad-00 mg-top-10">
										<div class="row">
											<div class="col-8 pad-00">
												<button type="button" class="btn btn-primary pull-right mg-left-13" id="ibsAddTrack" data-toggle="tooltip" data-placement="top" title="Add"><i class="fas fa-plus"></i> Add</button>
												<button type="button" class="btn btn-success pull-right" id="ibsImportTrack" data-toggle="tooltip" data-placement="top" title="Import"><i class="fas fa-file-excel"></i> Import</button>
												<button type="button" class="btn btn-outline-warning pull-right mg-left-13 d-none" id="ibsEditTrack" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-edit"></i> Edit</button>
												<button type="button" class="btn btn-outline-danger pull-right d-none" id="ibsDelTrack" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-trash-alt"></i> Delete</button>
												<button type="button" class="btn btn-outline-info pull-right d-none" id="ibsViewTrack" data-toggle="tooltip" data-placement="top" title="View"><i class="fas fa-eye"></i> View</button>
											</div>

											<div class="col-4">
												<div class="row">
													<div class="col-4">
														<select class="form-control" id="ibs_sort">
																<option selected>10</option>
																<option>25</option>
																<option>50</option>
																<option>100</option>
														</select>
													</div>
													<div class="col-8">
														<input type="text" class="form-control" id="ibsSearch" placeholder="Search Site Name">
													</div>
												</div>
											</div>

										</div>
									</div>

									<div class="col-12 pad-00 ibsListsTrack mg-top-10 hm-406">
											<table class="table table-hover table-striped table-bordered table-repsonsive table-sm" id="ibs_lists">
												<thead>
												<tr>
													<th scope="col" style="width:5%">
														<div class="custom-control pad-left-rem custom-checkbox">
															<input type="checkbox" class="custom-control-input" id="chk_ibs_item">
															<label class="custom-control-label" for="chk_ibs_item"></label>
														</div>
													</th>
													<th style="width:7% !important;" class="text-center text-uppercase text-muted" scope="col"><small>No.</small></th>
													<th class="text-center text-uppercase text-muted" scope="col"><small>Site Name</small></th>
													<th class="text-center text-uppercase text-muted" scope="col"><small>Building Type</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>Building Name</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>PLA ID</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>Longtitude</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>Latitude</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>Region</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>Province</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>Mun. / City</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>Address</small></th>
											</tr>
												</thead>
												<tbody id="ibs_datas"></tbody>
											</table>
									</div>
									<div class="col-12">
										<center>
											<div class="text-center" id='ibs_pagination'> </div>
										</center>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
	</div>
</div>

<div class="modal fade" id="import_ibs_modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-center text-uppercase">Import</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form method="post" id="file-ibs-upload" enctype="multipart/form-data">
						<div class="files">
							<p class="text-center lead h4">Upload File <i>(accepts .csv, .xlsx & .xls files only)</i></p>
							<input type="file" name="ibs_file" id="ibs_file" class="form-control btn" accept=".csv, .xlsx, .xls" required>
						</div>

						<div class="container">
							<div class='progress bg-secondary' id="progressIbsId">
								<div class="progress-bar" id="progressBarIbs"></div>
								<div class="percent_ibs text-white" id="percentIbs">0%</div>
							</div>
						</div>
				</div>
					<div class="modal-footer">
						<div class="col-12 text-center">
							<button class="btn btn-outline-danger text-center" id="removeibsButton" type="button"><i class="fas fa-eraser"></i> Remove</button>
							<button class="btn btn-outline-primary text-center" id="submitibsButton" type="submit"><i class="fas fa-file-upload"></i> Upload</button>
						</div>
					</div>
				</form>
		</div>
	</div>
</div>

<div class="modal fade pad-00 oy-hidden" id="add_ibs_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-full mg-top-0" role="document">
		<form id="edd_ibs_Track" method="POST">
		<div class="modal-content rounded-0 border-0 	">
			<div class="modal-header text-uppercase text-muted rounded-0">
				<h5 class="modal-title" id="ibsTitleTrack">Add IBS Track</h5>
				<button type="button" class="close ibs_close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body mh-400">
					<input type="hidden" id="ibs_status" name="ibs_status" value="0">
					<div class="table-responsive">
						<table class="table table-bordered table-responsive table-sm" id="eddibstable">
							<thead>
								<tr>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Site Name</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Bldg Name</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Bldg Type</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>PLAD ID</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Latitude</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Longtitude</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Region</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Province</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Municipality/City</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Address</small></th>
									<th class="text-center text-uppercase text-muted" style="width:5%"><button type="button" class="btn btn-primary btn-sm append_ibs_track"><i class="fas fa-plus"></i></button></th>
								</tr>
							</thead>
							<tbody id="ibs_track_tables">
								<tr class="ibs_rows-0">
									<td>
										<input type="hidden" name="ibs_iid[]" class="form-control ibs-iid" id="ibs-iid">
										<input type="text" name="ibs_site_name[]" class="form-control ibs_site_name" id="ibs_site_name" placeholder="Site Name" required>
									</td>
									<td><input type="text" name="ibs_bldg_name[]" class="form-control ibs_bldg_name" id="ibs_bldg_name" placeholder="Building Name" required></td>
									<td><input type="text" name="ibs_bldg_type[]" class="form-control ibs_bldg_type" id="ibs_bldg_type" placeholder="Building Type" required></td>
									<td><input type="text" name="ibs_pla_id[]" class="form-control ibs_pla_id" id="ibs_pla_id" placeholder="PLA ID" required></td>
									<td><input type="text" name="ibs_longtitude[]" class="form-control ibs_longtitude" id="ibs_longtitude" placeholder="Longtitude" required></td>
									<td><input type="text" name="ibs_latitude[]" class="form-control ibs_latitude" id="ibs_latitude" placeholder="Latitude" required></td>
									<td><input type="text" name="ibs_region[]" class="form-control ibs_region" id="ibs_region" placeholder="Region" required></td>
									<td><input type="text" name="ibs_province[]" class="form-control ibs_province" id="ibs_province" placeholder="Province" required></td>
									<td><input type="text" name="ibs_mun_city[]" class="form-control ibs_mun_city" id="ibs_mun_city" placeholder="Municipality / City" required></td>
									<td><input type="text" name="ibs_address[]" class="form-control ibs_address" id="ibs_address" placeholder="Address" required></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
			</div>
			<div class="modal-footer rounded-0 text-center">
				<div class="col-12">
					<button type="button" class="btn btn-outline-secondary text-center text-uppercase ibs_close" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-outline-primary text-center text-uppercase">Submit</button>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade pad-00" id="viewIbsModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="viewTrackModal" aria-hidden="true">
  <div class="modal-dialog modal-full mg-top-0" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">View IBS Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
						<table class="table table-bordered  text-center table-sm" id="viewibstable">
							<thead>
								<tr>
									<th class="text-center text-uppercase text-muted" scope="col" style="width:3%"><small>No.</small></th>
									<th class="text-center text-uppercase text-muted" scope="col" style="width:20%"><small>SITE NAME</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>BUILDING TYPE</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>BUILDING NAME</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>PLA ID</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>LONGTITUDE</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>LATITUDE</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>REGION</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>PROVINCE</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>MUN. / CITY</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>ADDRESS</small></th>
								</tr>
							</thead>
							<tbody id="view_ibs_build"></tbody>
						</table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
