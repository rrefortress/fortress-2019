<div class="aweTracks col-12">
	<div class="row">

		<div class="col-2 mg-top-10 pad-lef-right-0 position-static shadow-lg">
			<ul class="list-group nav flex-column nav-pills trackPills" id="awe-pills-tab" role="tablist" aria-orientation="vertical">
				<li class="nav-item">
					<a class="nav-link text-dark active tracks small" id="awe-pills-dash-tab" data-toggle="pill" href="#awe-pills-dash" role="tab" aria-controls="awe-pills-dash" aria-selected="true"><i class="fas fa-chart-bar"></i> Charts</a>
				</li>

				<li class="nav-item">
					<a class="nav-link tracks small <?= $this->session->userdata('access_level') == 'Admin' ? 'text-dark' : 't-disabled' ?>" id="awe-pills-lists-tab" data-toggle="pill" href="#awe-pills-lists" role="tab" aria-controls="awe-pills-lists" aria-selected="false"><i class="fas fa-list-alt"></i> Details &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="small badge badge-primary badge-pill text-right pull-right total_awe_details">0</span></a>
				</li>
			</ul>
		</div>

		<div class="col-10 border-left border-secondary">
			<div class="tab-content" id="awe-pills-tabContent">

				<div class="tab-pane fade show active" id="awe-pills-dash" role="tabpanel" aria-labelledby="awe-pills-dash-tab">
						<div class="emptyAweChartTrack d-none">
							<?php require('emptyTrack.php')?>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-12 aweFilter d-none mg-top-10">
									<div class="row">
										<div class="col-9"></div>
										<div class="col-3">
											<div class="form-group">
												<select class="form-control" id="aweProvince"></select>
											</div>
										</div>
									</div>
								</div>
								<div class="col-12">
									<div id="chartsAWE"></div>
								</div>
							</div>
						</div>
				</div>

				<div class="tab-pane fade" id="awe-pills-lists" role="tabpanel" aria-labelledby="awe-pills-lists-tab">
					<div class="container">
						<div class="row">
							<div class="emptyAweTrack d-none">
								<?php require('emptyTrack.php')?>
							</div>

							<div class="col-12 pad-00 mg-top-10">
								<div class="row">
									<div class="col-8 pad-00">
										<button type="button" class="btn btn-primary pull-right mg-left-13" id="aweAddTrack" data-toggle="tooltip" data-placement="top" title="Add"><i class="fas fa-plus"></i> Add</button>
										<button type="button" class="btn btn-success pull-right" id="aweImportTrack" data-toggle="tooltip" data-placement="top" title="Import"><i class="fas fa-file-excel"></i> Import</button>
										<button type="button" class="btn btn-outline-warning pull-right d-none mg-left-13" id="aweEditTrack" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-edit"></i> Edit</button>
										<button type="button" class="btn btn-outline-danger pull-right d-none" id="aweDelTrack" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-trash-alt"></i> Delete</button>
										<button type="button" class="btn btn-outline-info pull-right d-none" id="aweViewTrack" data-toggle="tooltip" data-placement="top" title="View"><i class="fas fa-eye"></i> View</button>
									</div>
									<div class="col-4">
										<div class="row">
											<div class="col-4">
												<select class="form-control" id="awe_sort">
														<option selected>10</option>
														<option>25</option>
														<option>50</option>
														<option>100</option>
												</select>
											</div>
											<div class="col-8">
												<input type="text" class="form-control" id="aweSearch" placeholder="Search Site Name">
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-12 pad-00 aweListsTrack mg-top-10 hm-406">
									<table class="table table-hover table-striped table-bordered table-repsonsive table-sm" id="awe_lists">
										<thead>
										<tr>
											<th scope="col" style="width:5%">
												<div class="custom-control pad-left-rem custom-checkbox">
													<input type="checkbox" class="custom-control-input" id="chk_awe_item">
													<label class="custom-control-label" for="chk_awe_item"></label>
												</div>
											</th>
											<th style="width:7% !important;" class="text-center text-uppercase text-muted" scope="col"><small>No.</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>SERIAL NUMBER</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Site Name</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Region</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Province</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Town / City</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Town Priority</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Latitude</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Longtitude</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>VENDOR</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Program </small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>PLAN TECH</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Tag</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Site Type</small></th>
										</tr>
										</thead>
										<tbody id="awe_datas"></tbody>
									</table>
							</div>
							<div class="col-12">
								<center>
									<div class="text-center" id='awe_pagination'> </div>
								</center>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="modal fade oy-hidden" id="import_awe_modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-center text-uppercase">Import</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form method="post" id="file-awe-upload" enctype="multipart/form-data">
						<div class="files">
							<p class="text-center lead h4">Upload File <i>(accepts .csv, .xlsx & .xls files only)</i></p>
							<input type="file" name="awe_file" id="awe_file" class="form-control btn" accept=".csv, .xlsx, .xls" required>
						</div>

						<div class="container">
							<div class='progress bg-secondary' id="progressAweId">
								<div class="progress-bar" id="progressBarAwe"></div>
								<div class="percent_awe text-white" id="percentAwe">0%</div>
							</div>
						</div>
				</div>
					<div class="modal-footer">
						<div class="col-12 text-center">
							<button class="btn btn-outline-danger text-center" id="removeAweButton" type="button"><i class="fas fa-eraser"></i> Remove</button>
							<button class="btn btn-outline-primary text-center" id="submitAweButton" type="submit"><i class="fas fa-file-upload"></i> Upload</button>
						</div>
					</div>
				</form>
		</div>
	</div>
</div>

<div class="modal fade pad-00" id="add_awes_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-full mg-top-0" role="document">
		<div class="modal-content rounded-0 border-0 	">
			<div class="modal-header text-uppercase text-muted rounded-0">
				<h5 class="modal-title" id="aweTitleTrack">Add AWE Track</h5>
				<button type="button" class="close awe_close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body mh-400">
				<form id="edd_awe_Track" method="POST">
					<input type="hidden" id="awe_status" name="awe_status" value="0">
					<div class="table-responsive">
						<table class="table table-bordered table-responsive table-sm" id="eddawetable">
							<thead>
								<tr>
									<th class="text-center text-uppercase text-muted" scope="col"><small>SERIAL NUMBER</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>SITE NAME</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>REGION</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>PROVINCE</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Town / City</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Town Priority</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Latitude</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Longitude</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>VENDOR</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Program Tag</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>PLANNED TECH</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Tagging</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Site Type</small></th>
									<th class="text-center text-uppercase text-muted" style="width:5%"><button type="button" class="btn btn-primary btn-sm append_awe_track"><i class="fas fa-plus"></i></button></th>
								</tr>
							</thead>
							<tbody id="awe_track_tables">
								<tr class="awe_rows-0">
									<td>
										<input type="hidden" name="awe_iid[]" class="form-control awe-iid" id="awe-iid">
										<input type="text" name="awe_serial_number[]" class="form-control awe_serial_number" id="awe_serial_number" placeholder="Serial Number" required>
									</td>
									<td><input type="text" name="awe_site_name[]" class="form-control awe_site_name" id="awe_site_name" placeholder="Site Name" required></td>
									<td><input type="text" name="awe_region[]" class="form-control awe_region" id="awe_region" placeholder="Region" required></td>
									<td><input type="text" name="awe_town_city[]" class="form-control awe_town_city" id="awe_town_city" placeholder="Town / City" required></td>
									<td><input type="text" name="awe_province[]" class="form-control awe_province" id="awe_province" placeholder="Province" required></td>
									<td><input type="text" name="awe_town_priority[]" class="form-control awe_town_priority" id="awe_town_priority" placeholder="Town Priority" required></td>
									<td><input type="text" name="awe_latitude[]" class="form-control awe_latitude" id="awe_latitude" placeholder="Latitude" required></td>
									<td><input type="text" name="awe_longtitude[]" class="form-control awe_longtitude" id="awe_longtitude" placeholder="Longtitude" required></td>
									<td><input type="text" name="awe_vendor[]" class="form-control awe_vendor" id="awe_vendor" placeholder="Vendor" required></td>
									<td><input type="text" name="awe_program_tag[]" class="form-control awe_program_tag" id="awe_program_tag" placeholder="Program Tag" required></td>
									<td><input type="text" name="awe_planned_tech[]" class="form-control awe_planned_tech" id="awe_planned_tech" placeholder="Planned Tech" required></td>
									<td><input type="text" name="awe_tagging[]" class="form-control awe_tagging" id="awe_tagging" placeholder="Tagging" required></td>
									<td><input type="text" name="awe_site_type[]" class="form-control awe_site_type" id="awe_site_type" placeholder="Site Type" required></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>

			</div>
			<div class="modal-footer rounded-0 text-center">
				<div class="col-12">
					<button type="button" class="btn btn-outline-secondary text-center text-uppercase awe_close" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-outline-primary text-center text-uppercase">Submit</button>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade pad-00" id="viewAweModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="viewTrackModal" aria-hidden="true">
  <div class="modal-dialog modal-full mg-top-0" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">View Awe Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
						<table class="table table-bordered  text-center table-sm" id="viewawetable">
							<thead>
								<tr>
									<th class="text-center text-uppercase text-muted" scope="col" style="width:3%"><small>No.</small></th>
									<th class="text-center text-uppercase text-muted" scope="col" style="width:8%"><small>SR</small></th>
									<th class="text-center text-uppercase text-muted" scope="col" style="width:30%"><small>SITE NAME</small></th>
									<th class="text-center text-uppercase text-muted" scope="col" style="width:5%"><small>Region</small></th>
									<th class="text-center text-uppercase text-muted" scope="col" style="width:5%"><small>Province</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Town/City</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Latitude</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Longtitude</small></th>
									<th class="text-center text-uppercase text-muted" scope="col" style="width:5%"><small>Vendor</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Program</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Plan.Tech</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Tagging</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Site Type</small></th>
								</tr>
							</thead>
							<tbody id="view_awe_build"></tbody>
						</table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
