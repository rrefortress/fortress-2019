<div class="tssrTracks col-12">
	<div class="row">
				<div class="col-2 mg-top-10 pad-lef-right-0 position-static">
					<ul class="list-group nav flex-column nav-pills trackPills" id="tssr-pills-tab" role="tablist" aria-orientation="vertical">
						<li class="nav-item">
							<a class="nav-link text-dark active tracks small" id="tssr-pills-dash-tab" data-toggle="pill" href="#tssr-pills-dash" role="tab" aria-controls="tssr-pills-dash" aria-selected="true"><i class="fas fa-chart-bar"></i> Charts</a>
						</li>

						<li class="nav-item">
							<a class="nav-link tracks small <?= $this->session->userdata('access_level') == 'Admin' ? 'text-dark' : 't-disabled' ?>" id="tssr-pills-lists-tab" data-toggle="pill" href="#tssr-pills-lists" role="tab" aria-controls="tssr-pills-lists" aria-selected="false"><i class="fas fa-list-alt"></i> Details &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="small badge badge-primary badge-pill text-right pull-right total_tssr_details">0</span></a>
						</li>
					</ul>
				</div>

				<div class="col-10 h-100 border-left border-secondary h-519 shadow-lg">
					<div class="tab-content" id="tssr-pills-tabContent">

						<div class="tab-pane fade show active" id="tssr-pills-dash" role="tabpanel" aria-labelledby="tssr-pills-dash-tab">
								<div class="emptyTssrChartTrack d-none">
									<?php require_once('emptyTrack.php')?>
								</div>

								<div class="row">
									<div class="col-12 nomFilter d-none mg-top-10"></div>
									<div class="col-12">
										<div id="chartsTssr"></div>
									</div>
								</div>
						</div>

						<div class="tab-pane fade" id="tssr-pills-lists" role="tabpanel" aria-labelledby="tssr-pills-lists-tab">
							<div class="container">
								<div class="row">
									<div class="emptyTssrTrack d-none">
										<?php require('emptyTrack.php')?>
									</div>

									<div class="col-12 pad-00 mg-top-10">
										<div class="row">
											<div class="col-8 pad-00">
												<button type="button" class="btn btn-primary pull-right mg-left-13" id="tssrAddTrack" data-toggle="tooltip" data-placement="top" title="Add"><i class="fas fa-plus"></i> Add</button>
												<button type="button" class="btn btn-success pull-right" id="tssrImportTrack" data-toggle="tooltip" data-placement="top" title="Import"><i class="fas fa-file-excel"></i> Import</button>
												<button type="button" class="btn btn-outline-warning pull-right mg-left-13 d-none" id="tssrEditTrack" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-edit"></i> Edit</button>
												<button type="button" class="btn btn-outline-danger pull-right d-none" id="tssrDelTrack" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-trash-alt"></i> Delete</button>
												<button type="button" class="btn btn-outline-info pull-right d-none" id="tssrViewTrack" data-toggle="tooltip" data-placement="top" title="View"><i class="fas fa-eye"></i> View</button>
											</div>

											<div class="col-4">
												<div class="row">
													<div class="col-4">
														<select class="form-control" id="tssr_sort">
																<option selected>10</option>
																<option>25</option>
																<option>50</option>
																<option>100</option>
														</select>
													</div>
													<div class="col-8">
														<input type="text" class="form-control" id="tssrSearch" placeholder="Search Site Name">
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-12 pad-00 tssrListsTrack mg-top-10 hm-406">
											<table class="table table-hover table-striped table-bordered table-repsonsive table-sm" id="tssr_lists">
												<thead>
												<tr>
													<th scope="col" style="width:5%">
														<div class="custom-control pad-left-rem custom-checkbox">
															<input type="checkbox" class="custom-control-input" id="chk_tssr_item">
															<label class="custom-control-label" for="chk_tssr_item"></label>
														</div>
													</th>
													<th style="width:7% !important;" class="text-center text-uppercase text-muted" scope="col"><small>No.</small></th>
													<th class="text-center text-uppercase text-muted" scope="col"><small>SITE NAME</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>REGION</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>PLA ID</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>Coverage</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>Address</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>Province</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>Town</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>Barangay</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>Town PSGC</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>Longitude</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>Latitude</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>Baluarte Tagging</small></th>
                          <th class="text-center text-uppercase text-muted" scope="col"><small>Clutter Type</small></th>
                      </tr>
												</thead>
												<tbody id="tssr_datas"></tbody>
											</table>
									</div>
									<div class="col-12">
										<center>
											<div class="text-center" id='tssr_pagination'> </div>
										</center>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

	</div>
</div>

<div class="modal fade" id="import_tssr_modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-center text-uppercase">Import</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form method="post" id="file-Tssrs-upload" enctype="multipart/form-data">
						<div class="files">
							<p class="text-center lead h4">Upload File <i>(accepts .csv, .xlsx & .xls files only)</i></p>
							<input type="file" name="tssrs_file" id="tssrs_file" class="form-control btn" accept=".csv, .xlsx, .xls" required>
						</div>

						<div class="container">
							<div class='progress bg-secondary' id="progressTssrId">
								<div class="progress-bar" id="progressBarTssr"></div>
								<div class="percent_tssr text-white" id="percentTssr">0%</div>
							</div>
						</div>
				</div>
					<div class="modal-footer">
						<div class="col-12 text-center">
							<button class="btn btn-outline-danger text-center" id="removeTssrButton" type="button"><i class="fas fa-eraser"></i> Remove</button>
							<button class="btn btn-outline-primary text-center" id="submitTssrButton" type="submit"><i class="fas fa-file-upload"></i> Upload</button>
						</div>
					</div>
				</form>
		</div>
	</div>
</div>

<div class="modal fade pad-00 oy-hidden" id="add_tssr_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-full mg-top-0" role="document">
		<form id="edd_tssr_Track" method="POST">
		<div class="modal-content rounded-0 border-0 	">
			<div class="modal-header text-uppercase text-muted rounded-0">
				<h5 class="modal-title" id="tssrTitleTrack">Add TSSR Track</h5>
				<button type="button" class="close tssr_close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body mh-400">
					<input type="hidden" id="tssr_status" name="tssr_status" value="0">
					<div class="table-responsive">
						<table class="table table-bordered table-responsive table-sm" id="eddtssrtable">
							<thead>
								<tr>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Site Name</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Region</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>PLAD ID</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>coverage</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Address</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Province</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>town</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Brgy</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>psgc</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Latitude</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Longtitude</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Baluarte Tagging</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Clutter Type</small></th>
									<th class="text-center text-uppercase text-muted" style="width:5%"><button type="button" class="btn btn-primary btn-sm append_tssr_track"><i class="fas fa-plus"></i></button></th>
								</tr>
							</thead>
							<tbody id="tssr_track_tables">
								<tr class="tssr_rows-0">
									<td>
										<input type="hidden" name="tssr_iid[]" class="form-control tssr-iid" id="tssr-iid">
										<input type="text" name="tssr_site_name[]" class="form-control tssr_site_name" id="tssr_site_name" placeholder="Site Name" required>
									</td>
									<td><input type="text" name="tssr_region[]" class="form-control tssr_region" id="tssr_region" placeholder="Region" required></td>
									<td><input type="text" name="tssr_pla_id[]" class="form-control tssr_pla_id" id="tssr_pla_id" placeholder="PLA ID" required></td>
									<td><input type="text" name="tssr_coverage[]" class="form-control tssr_coverage" id="tssr_coverage" placeholder="Coverage" required></td>
									<td><input type="text" name="tssr_address[]" class="form-control tssr_address" id="tssr_address" placeholder="Address" required></td>
									<td><input type="text" name="tssr_province[]" class="form-control tssr_province" id="tssr_province" placeholder="Province" required></td>
									<td><input type="text" name="tssr_town[]" class="form-control tssr_town" id="tssr_town" placeholder="Town" required></td>
									<td><input type="text" name="tssr_brgy[]" class="form-control tssr_brgy" id="tssr_brgy" placeholder="Brgy" required></td>
									<td><input type="text" name="tssr_psgc[]" class="form-control tssr_psgc" id="tssr_psgc" placeholder="PSGC" required></td>
									<td><input type="text" name="tssr_latitude[]" class="form-control tssr_latitude" id="tssr_latitude" placeholder="Latitude" required></td>
									<td><input type="text" name="tssr_longtitude[]" class="form-control tssr_longtitude" id="tssr_longtitude" placeholder="Longitude" required></td>
									<td><input type="text" name="tssr_baluarte_tagging[]" class="form-control tssr_baluarte_tagging" id="tssr_baluarte_tagging" placeholder="Baluarted Tagging" required></td>
									<td><input type="text" name="tssr_clutter_type[]" class="form-control tssr_clutter_type" id="tssr_clutter_type" placeholder="Clutter Type" required></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>

			</div>
			<div class="modal-footer rounded-0 text-center">
				<div class="col-12">
					<button type="button" class="btn btn-outline-secondary text-center text-uppercase tssr_close" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-outline-primary text-center text-uppercase">Submit</button>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade pad-00" id="viewTssrModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="viewTrackModal" aria-hidden="true">
  <div class="modal-dialog modal-full mg-top-0" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">View TSSR Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
						<table class="table table-bordered  text-center table-sm" id="viewtssrtable">
							<thead>
								<tr>
									<th class="text-center text-uppercase text-muted" scope="col" style="width:3%"><small>No.</small></th>
									<th class="text-center text-uppercase text-muted" scope="col" style="width:20%"><small>SITE NAME</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>REGION</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>PLA ID</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>COVERAGE</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>ADDRESS</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>PROVINCE</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>TOWN</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>BARANGAY</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>TOWN PSGC</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>LONGITUDE</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>LATITUDE</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>BALUARTE TAGGING</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>CLUTTER TYPE</small></th>
								</tr>
							</thead>
							<tbody id="view_tssr_build"></tbody>
						</table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
