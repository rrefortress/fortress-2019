<div class="container-fluid pad-00">
	<div class="nomTracks col-12 border">
			<div class="row d-none">

				<div class="col-2 mg-top-10 pad-lef-right-0 shadow-lg">
					<ul class="list-group nav flex-column nav-pills trackPills" id="nom-pills-tab" role="tablist" aria-orientation="vertical">
						<li class="nav-item">
							<a class="nav-link text-dark active tracks small" id="nom-pills-dash-tab" data-toggle="pill" href="#nom-pills-dash" role="tab" aria-controls="nom-pills-dash" aria-selected="true"><i class="fas fa-chart-bar"></i> Charts</a>
						</li>

							<li class="nav-item">
								<a class="nav-link tracks small <?= $this->session->userdata('access_level') == 'Admin' ? 'text-dark' : 't-disabled' ?>" id="nom-pills-lists-tab" data-toggle="pill" href="#nom-pills-lists" role="tab" aria-controls="nom-pills-lists" aria-selected="false"><i class="fas fa-list-alt"></i> Details &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="small badge badge-primary badge-pill text-right pull-right total_nom_details">0</span></a>
							</li>
					</ul>
				</div>

				<div class="col-10 border-left border-secondary">
					<div class="tab-content" id="nom-pills-tabContent">

						<div class="tab-pane fade show active" id="nom-pills-dash" role="tabpanel" aria-labelledby="nom-pills-dash-tab">
								<div class="emptyNomChartTrack d-none">
									<?php require('emptyTrack.php')?>
								</div>

								<div class="row">
									<div class="col-12 nomFilter d-none mg-top-10">
										<div class="row">
											<div class="col-9"></div>
											<div class="col-3">
												<div class="form-group">
													<select class="form-control" id="nomProvince"></select>
												</div>
											</div>
										</div>
									</div>
									<div class="col-12">
										<div id="chartsNom"></div>
									</div>
								</div>
						</div>

						<div class="tab-pane fade" id="nom-pills-lists" role="tabpanel" aria-labelledby="nom-pills-lists-tab">
							<div class="container">
								<div class="row">
									<div class="emptyNomTrack d-none">
										<?php require('emptyTrack.php')?>
									</div>

									<div class="col-12 pad-00 mg-top-10">
										<div class="row">
											<div class="col-8 pad-00">
												<button type="button" class="btn btn-primary pull-right mg-left-13" id="nomAddTrack" data-toggle="tooltip" data-placement="top" title="Add"><i class="fas fa-plus"></i> Add</button>
												<button type="button" class="btn btn-success pull-right" id="nomImportTrack" data-toggle="tooltip" data-placement="top" title="Import"><i class="fas fa-file-excel"></i> Import</button>
												<button type="button" class="btn btn-outline-warning pull-right mg-left-13 d-none" id="nomEditTrack" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-edit"></i> Edit</button>
												<button type="button" class="btn btn-outline-danger pull-right d-none" id="nomDelTrack" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-trash-alt"></i> Delete</button>
												<button type="button" class="btn btn-outline-info pull-right d-none" id="nomViewTrack" data-toggle="tooltip" data-placement="top" title="View"><i class="fas fa-eye"></i> View</button>
											</div>
											<div class="col-4">
												<div class="row">
													<div class="col-4">
														<select class="form-control" id="nom_sort">
																<option selected>10</option>
																<option>25</option>
																<option>50</option>
																<option>100</option>
														</select>
													</div>
													<div class="col-8">
														<input type="text" class="form-control" id="nomSearch" placeholder="Search Site Name">
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-12 pad-00 nomListsTrack mg-top-10 hm-406">
											<table class="table table-hover table-striped table-bordered table-repsonsive table-sm" id="nom_lists">
												<thead>
												<tr>
													<th scope="col" style="width:5%">
														<div class="custom-control pad-left-rem custom-checkbox">
															<input type="checkbox" class="custom-control-input" id="chk_nom_item">
															<label class="custom-control-label" for="chk_nom_item"></label>
														</div>
													</th>
													<th style="width:7% !important;" class="text-center text-uppercase text-muted" scope="col"><small>No.</small></th>
													<th class="text-center text-uppercase text-muted" scope="col"><small>Site Name</small></th>
													<th class="text-center text-uppercase text-muted" scope="col"><small>Region</small></th>
													<th class="text-center text-uppercase text-muted" scope="col"><small>Province</small></th>
													<th class="text-center text-uppercase text-muted" scope="col"><small>Town/City</small></th>
													<th class="text-center text-uppercase text-muted" scope="col"><small>Latitude</small></th>
													<th class="text-center text-uppercase text-muted" scope="col"><small>Longtitude</small></th>
													<th class="text-center text-uppercase text-muted" scope="col"><small>Tagging</small></th>
													<th class="text-center text-uppercase text-muted" scope="col"><small>Site Type</small></th>
													<th class="text-center text-uppercase text-muted" scope="col"><small>Remarks</small></th>
												</tr>
												</thead>
												<tbody id="nom_datas"></tbody>
											</table>
									</div>
									<div class="col-12">
										<center>
											<div class="text-center" id='nom_pagination'> </div>
										</center>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
</div>


<div class="modal fade pad-00" id="add_nom_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-full mg-top-0" role="document">
		<div class="modal-content rounded-0 border-0 	">
			<div class="modal-header text-uppercase text-muted rounded-0">
				<h5 class="modal-title" id="NomTitleTrack">Add Nomination Track</h5>
				<button type="button" class="close nom_close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body mh-400">
				<form id="edd_nom_Track" method="POST">
					<input type="hidden" id="nom_status" name="nom_status" value="0">
					<div class="table-responsive">
						<table class="table table-bordered table-responsive table-sm" id="nom_table">
							<thead>
								<tr>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Site Name</small></th>
									<th class="text-center text-uppercase text-muted" scope="col" style="width:7%"><small>Region</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Province</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Town/City</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Latitude</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Longtitude</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Tagging</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Site Type</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Remarks</small></th>
									<th class="text-center text-uppercase text-muted" style="width:5%"><button type="button" class="btn btn-primary btn-sm append_nom_track"><i class="fas fa-plus"></i></button></th>
								</tr>
							</thead>
							<tbody id="nom_track_tables">
								<tr class="nom_rows-0">
									<td>
										<input type="hidden" name="nom_iid[]" class="form-control nom-iid" id="nom-iid">
										<input type="text" name="nom_site_name[]" class="form-control nom_site_name" id="nom_site_name" placeholder="Site Name" required>
									</td>
									<td><input type="text" name="nom_region[]" class="form-control nom_region" id="nom_region" placeholder="Region" required></td>
									<td><input type="text" name="nom_province[]" class="form-control nom_province" id="nom_province" placeholder="Province" required></td>
									<td><input type="text" name="nom_town_city[]" class="form-control nom_town_city" id="nom_town_city" placeholder="Town/City" required></td>
									<td><input type="text" name="nom_latitude[]" class="form-control nom_latitude" id="nom_latitude" placeholder="Latitude" required></td>
									<td><input type="text" name="nom_longtitude[]" class="form-control nom_longtitude" id="nom_longtitude" placeholder="Longtitude" required></td>
									<td><input type="text" name="nom_tagging[]" class="form-control nom_tagging" id="nom_tagging" placeholder="Tagging" required></td>
									<td><input type="text" name="nom_site_type[]" class="form-control nom_site_type" id="nom_site_type" placeholder="Site Type" required></td>
									<td><input type="text" name="nom_remarks[]" class="form-control nom_remarks" id="nom_remarks" placeholder="Remarks" required></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>

			</div>
			<div class="modal-footer rounded-0 text-center">
				<div class="col-12">
					<button type="button" class="btn btn-outline-secondary text-center text-uppercase nom_close" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-outline-primary text-center text-uppercase">Submit</button>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="import_nom_modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-center text-uppercase">Import</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form method="post" id="file-Nom-upload" enctype="multipart/form-data">
						<div class="files">
							<p class="text-center lead h4">Upload File <i>(accepts .csv, .xlsx & .xls files only)</i></p>
							<input type="file" name="nom_file" id="nom_file" class="form-control btn" accept=".csv, .xlsx, .xls" required>
						</div>

						<div class="container">
							<div class='progress bg-secondary' id="progressNomId">
								<div class="progress-bar" id="progressBarNom"></div>
								<div class="percent_nom text-white" id="percentNom">0%</div>
							</div>
						</div>
				</div>
					<div class="modal-footer">
						<div class="col-12 text-center">
							<button class="btn btn-outline-danger text-center" id="remove_awe_Button" type="button"><i class="fas fa-eraser"></i> Remove</button>
							<button class="btn btn-outline-primary text-center" id="submit_awe_Button" type="submit"><i class="fas fa-file-upload"></i> Upload</button>
						</div>
					</div>
				</form>
		</div>
	</div>
</div>

<div class="modal fade pad-00" id="viewNomModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="viewTrackModal" aria-hidden="true">
  <div class="modal-dialog modal-full mg-top-0" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">View Nomination Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">\
						<table class="table table-bordered  text-center table-sm" id="viewsurveytable">
							<thead>
								<tr>
									<th class="text-center text-uppercase text-muted" scope="col" style="width:3%"><small>No.</small></th>
									<th class="text-center text-uppercase text-muted" scope="col" style="width:30%"><small>SITE NAME</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Region</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Province</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Town/City</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Latitude</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Longtitude</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Tagging</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Site Type</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Remarks</small></th>
								</tr>
							</thead>
							<tbody id="view_nom_build"></tbody>
						</table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
