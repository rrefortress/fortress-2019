<div class="pmoTracks col-12">
		<div class="row">
					<div class="col-2 mg-top-10 pad-lef-right-0 shadow-lg">
						<ul class="list-group nav flex-column nav-pills trackPills" id="pmo-pills-tab" role="tablist" aria-orientation="vertical">
							<li class="nav-item">
								<a class="nav-link text-dark active tracks small" id="pmo-pills-dash-tab" data-toggle="pill" href="#pmo-pills-dash" role="tab" aria-controls="pmo-pills-dash" aria-selected="true"><i class="fas fa-chart-bar"></i> Charts</a>
							</li>

							<li class="nav-item">
								<a class="nav-link tracks small <?= $this->session->userdata('access_level') == 'Admin' ? 'text-dark' : 't-disabled' ?>" id="pmo-pills-lists-tab" data-toggle="pill" href="#pmo-pills-lists" role="tab" aria-controls="pmo-pills-lists" aria-selected="false"><i class="fas fa-list-alt"></i> Details &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="small badge badge-primary badge-pill text-right pull-right total_pmo_details">0</span></a>
							</li>

						</ul>
					</div>


					<div class="col-10 border-left border-secondary">
						<div class="tab-content" id="pmo-pills-tabContent">

							<div class="tab-pane fade show active" id="pmo-pills-dash" role="tabpanel" aria-labelledby="pmo-pills-dash-tab">
								<div class="emptyPmoChartTrack d-none">
									<?php require('emptyTrack.php')?>
								</div>

								<div class="row">
									<div class="col-12 nomFilter d-none mg-top-10"></div>
									<div class="col-12">
										<div id="chartsPMO"></div>
									</div>
								</div>
							</div>

							<div class="tab-pane fade" id="pmo-pills-lists" role="tabpanel" aria-labelledby="pmo-pills-lists-tab">
								<div class="container">
									<div class="row">
										<div class="emptyPmoTrack d-none">
											<?php require('emptyTrack.php')?>
										</div>

										<div class="col-12 pad-00 mg-top-10">
											<div class="row">
												<div class="col-8 pad-00">
													<button type="button" class="btn btn-primary pull-right mg-left-13" id="pmoAddTrack" data-toggle="tooltip" data-placement="top" title="Add"><i class="fas fa-plus"></i> Add</button>
													<button type="button" class="btn btn-success pull-right" id="pmoImportTrack" data-toggle="tooltip" data-placement="top" title="Import"><i class="fas fa-file-excel"></i> Import</button>
													<button type="button" class="btn btn-outline-warning pull-right mg-left-13 d-none" id="pmoEditTrack" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-edit"></i> Edit</button>
													<button type="button" class="btn btn-outline-danger pull-right d-none" id="pmoDelTrack" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-trash-alt"></i> Delete</button>
													<button type="button" class="btn btn-outline-info pull-right d-none" id="pmoViewTrack" data-toggle="tooltip" data-placement="top" title="View"><i class="fas fa-eye"></i> View</button>
												</div>

												<div class="col-4">
													<div class="row">
														<div class="col-4">
															<select class="form-control" id="pmo_sort">
																<option selected>10</option>
																<option>25</option>
																<option>50</option>
																<option>100</option>
															</select>
														</div>
														<div class="col-8">
															<input type="text" class="form-control" id="pmoSearch" placeholder="Search Site Name">
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="col-12 pad-00 pmoListsTrack mg-top-10 hm-406">
												<table class="table table-hover table-striped table-bordered table-repsonsive table-sm" id="pmo_lists">
													<thead>
													<tr>
														<th scope="col" style="width:5%">
															<div class="custom-control pad-left-rem custom-checkbox">
																<input type="checkbox" class="custom-control-input" id="chk_pmo_item">
																<label class="custom-control-label" for="chk_pmo_item"></label>
															</div>
														</th>
														<th style="width:7% !important;" class="text-center text-uppercase text-muted" scope="col"><small>No.</small></th>
														<th class="text-center text-uppercase text-muted" scope="col"><small>SERIAL NUMBER</small></th>
														<th class="text-center text-uppercase text-muted" scope="col"><small>Site Name</small></th>
														<th class="text-center text-uppercase text-muted" scope="col"><small>Region</small></th>
														<th class="text-center text-uppercase text-muted" scope="col"><small>Budget Tagging</small></th>
														<th class="text-center text-uppercase text-muted" scope="col"><small>Program</small></th>
														<th class="text-center text-uppercase text-muted" scope="col"><small>Project Phase</small></th>
														<th class="text-center text-uppercase text-muted" scope="col"><small>Lead Vendor</small></th>
														<th class="text-center text-uppercase text-muted" scope="col"><small>Solution</small></th>
														<th class="text-center text-uppercase text-muted" scope="col"><small>Coverage </small></th>
														<th class="text-center text-uppercase text-muted" scope="col"><small>Scope</small></th>
														<th class="text-center text-uppercase text-muted" scope="col"><small>Wireless Plan</small></th>
														<th class="text-center text-uppercase text-muted" scope="col"><small># Sectors</small></th>
												</tr>
													</thead>
													<tbody id="pmo_datas"></tbody>
												</table>
										</div>
										<div class="col-12">
											<center>
												<div class="text-center" id='pmo_pagination'> </div>
											</center>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>


		</div>
	</div>



<div class="modal fade" id="import_pmo_modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-center text-uppercase">Import</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form method="post" id="file-pmo-upload" enctype="multipart/form-data">
						<div class="files">
							<p class="text-center lead h4">Upload File <i>(accepts .csv, .xlsx & .xls files only)</i></p>
							<input type="file" name="pmo_file" id="pmo_file" class="form-control btn" accept=".csv, .xlsx, .xls" required>
						</div>

						<div class="container">
							<div class='progress bg-secondary' id="progressPmoId">
								<div class="progress-bar" id="progressBarPmo"></div>
								<div class="percent_pmo text-white" id="percentPmo">0%</div>
							</div>
						</div>
				</div>
					<div class="modal-footer">
						<div class="col-12 text-center">
							<button class="btn btn-outline-danger text-center" id="removepmoButton" type="button"><i class="fas fa-eraser"></i> Remove</button>
							<button class="btn btn-outline-primary text-center" id="submitpmoButton" type="submit"><i class="fas fa-file-upload"></i> Upload</button>
						</div>
					</div>
				</form>
		</div>
	</div>
</div>

<div class="modal fade pad-00" id="add_pmo_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-full mg-top-0" role="document">
		<form id="edd_pmo_Track" method="POST">
		<div class="modal-content rounded-0 border-0 	">
			<div class="modal-header text-uppercase text-muted rounded-0">
				<h5 class="modal-title" id="pmoTitleTrack">Add PMO Track</h5>
				<button type="button" class="close pmo_close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body mh-400">
					<input type="hidden" id="pmo_status" name="pmo_status" value="0">
					<div class="table-responsive">
						<table class="table table-bordered table-responsive table-sm" id="eddpmotable">
							<thead>
								<tr>
									<th class="text-center text-uppercase text-muted" scope="col"><small>SERIAL NUMBER</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>SITE NAME</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>REGION</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Budget Tagging</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Program</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Project Phase</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Lead Vendor</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Solution</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Coverage</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Scope</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Wireless Plan</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Sectors</small></th>
									<th class="text-center text-uppercase text-muted" style="width:5%"><button type="button" class="btn btn-primary btn-sm append_pmo_track"><i class="fas fa-plus"></i></button></th>
								</tr>
							</thead>
							<tbody id="pmo_track_tables">
								<tr class="pmo_rows-0">
									<td>
										<input type="hidden" name="pmo_iid[]" class="form-control pmo-iid" id="pmo-iid">
										<input type="text" name="pmo_serial_number[]" class="form-control pmo_serial_number" id="pmo_serial_number" placeholder="Serial Number" required>
									</td>
									<td><input type="text" name="pmo_site_name[]" class="form-control pmo_site_name" id="pmo_site_name" placeholder="Site Name" required></td>
									<td><input type="text" name="pmo_region[]" class="form-control pmo_region" id="pmo_region" placeholder="Region" required></td>
									<td><input type="text" name="pmo_budget_tagging[]" class="form-control pmo_budget_tagging" id="pmo_budget_tagging" placeholder="Budget Tagging" required></td>
									<td><input type="text" name="pmo_program[]" class="form-control pmo_program" id="pmo_program" placeholder="Program" required></td>
									<td><input type="text" name="pmo_project_phase[]" class="form-control pmo_project_phase" id="pmo_project_phase" placeholder="Project Phase" required></td>
									<td><input type="text" name="pmo_lead_vendor[]" class="form-control pmo_lead_vendor" id="pmo_lead_vendor" placeholder="Lead Vendor" required></td>
									<td><input type="text" name="pmo_solution[]" class="form-control pmo_solution" id="pmo_solution" placeholder="Solution" required></td>
									<td><input type="text" name="pmo_coverage[]" class="form-control pmo_coverage" id="pmo_coverage" placeholder="Coverage" required></td>
									<td><input type="text" name="pmo_scope[]" class="form-control pmo_scope" id="pmo_scope" placeholder="Scope" required></td>
									<td><input type="text" name="pmo_wireless_plan[]" class="form-control pmo_wireless_plan" id="pmo_wireless_plan" placeholder="Wireless Plan" required></td>
									<td><input type="text" name="pmo_sectors[]" class="form-control pmo_sectors" id="pmo_sectors" placeholder="Sectors" required></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
			</div>
			<div class="modal-footer rounded-0 text-center">
				<div class="col-12">
					<button type="button" class="btn btn-outline-secondary text-center text-uppercase pmo_close" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-outline-primary text-center text-uppercase">Submit</button>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade pad-00" id="viewPmoModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="viewTrackModal" aria-hidden="true">
  <div class="modal-dialog modal-full mg-top-0" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">View Awe Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
						<table class="table table-bordered  text-center table-sm" id="viewawetable">
							<thead>
								<tr>
									<th class="text-center text-uppercase text-muted" scope="col" style="width:3%"><small>No.</small></th>
									<th class="text-center text-uppercase text-muted" scope="col" style="width:8%"><small>SR</small></th>
									<th class="text-center text-uppercase text-muted" scope="col" style="width:30%"><small>SITE NAME</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>REGION</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Budget Tagging</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Program</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Project Phase</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Lead Vendor</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Solution</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Coverage</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Scope</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Wireless Plan</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Sectors</small></th>
								</tr>
							</thead>
							<tbody id="view_pmo_build"></tbody>
						</table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
