<div class="surveyTracks col-12">
	<div class="row">

		<div class="hm-450 col-2 col-md-2 col-sm-2 col-xl-2 mg-top-10 pad-lef-right-0 position-static">
			<ul class="list-group nav flex-column nav-pills trackPills" id="survey-pills-tab" role="tablist" aria-orientation="vertical">
				<li class="nav-item">
					<a class="nav-link text-dark active tracks small" id="survey-pills-dash-tab" data-toggle="pill" href="#survey-pills-dash" role="tab" aria-controls="survey-pills-dash" aria-selected="true"><i class="fas fa-chart-bar"></i> Charts</a>
				</li>

				<li class="nav-item">
					<a class="nav-link text-dark tracks small <?= $this->session->userdata('access_level') == 'Admin' ? 'text-dark' : 't-disabled' ?>" id="survey-pills-lists-tab" data-toggle="pill" href="#survey-pills-lists" role="tab" aria-controls="survey-pills-lists" aria-selected="false"><i class="fas fa-list-alt"></i> Details &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="small badge badge-primary badge-pill text-right pull-right total_survey_details">0</span></a>
				</li>

				<li class="nav-item">
					<a class="nav-link text-dark tracks small" id="calendar-pills-tab" data-toggle="pill" href="#calendar-pills" role="tab" aria-controls="calendar-pills" aria-selected="false"><i class="fas fa-calendar-alt"></i> Calendar</a>
				</li>

				<li class="nav-item">
					<a class="nav-link text-dark tracks small" id="locate-pills-tab" data-toggle="pill" href="#locate-pills" role="tab" aria-controls="locate-pills" aria-selected="false"><i class="fas fa-map-marker-alt"></i> Locations</a>
				</li>
			</ul>
		</div>

		<div class="col-10 border-left border-secondary pad-00 w-1023">
			<div class="tab-content" id="survey-pills-tabContent">

				<div class="tab-pane fade show active" id="survey-pills-dash" role="tabpanel" aria-labelledby="survey-pills-dash-tab">
						<div class="emptySurveyChartTrack d-none">
							<?php require('emptyTrack.php')?>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-12 pad-00 mg-top-10">
									<div class="container">
										<div class="form-row ">
											<div class="col-6"></div>
											<div class="col-3">
												<select class="form-control" id="surveyProject"></select>
											</div>
											<div class="col-3">
												<select class="form-control" id="surveyName"></select>
											</div>
										</div>
									</div>
								</div>
								<div class="col-12 mg-top-10">
									<div id="chartsSurvey"></div>
								</div>
							</div>
						</div>
				</div>

				<div class="tab-pane fade" id="survey-pills-lists" role="tabpanel" aria-labelledby="survey-pills-lists-tab">
					<div class="container">
						<div class="row">
							<div class="emptySurveyTrack d-none">
								<?php require('emptyTrack.php')?>
							</div>

							<div class="col-12 col-md-12 col-sm-12 col-xl-12 pad-00 mg-top-10">
								<div class="container">
									<div class="row">
										<div class="col-6 pad-00">
											<button type="button" class="btn btn-primary pull-right mg-left-13" id="surveyAddTrack" data-toggle="tooltip" data-placement="top" title="Add"><i class="fas fa-plus"></i> Add</button>
											<button type="button" class="btn btn-success pull-right" id="surveyImportTrack" data-toggle="tooltip" data-placement="top" title="Import"><i class="fas fa-file-excel"></i> Import</button>
											<button type="button" class="btn btn-outline-warning pull-right mg-left-13 d-none" id="surveyEditTrack" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-edit"></i> Edit</button>
											<button type="button" class="btn btn-outline-danger pull-right d-none" id="surveyDelTrack" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-trash-alt"></i> Delete</button>
											<button type="button" class="btn btn-outline-info pull-right d-none" id="surveyViewTrack" data-toggle="tooltip" data-placement="top" title="View"><i class="fas fa-eye"></i> View</button>
										</div>

										<div class="col-6">
												<div class="row">
													<div class="col-3">
														<select class="form-control" id="surveyNameDetails"></select>
													</div>
													<div class="col-3">
														<select class="form-control" id="survey_sort">
																<option selected>10</option>
																<option>25</option>
																<option>50</option>
																<option>100</option>
														</select>
													</div>
													<div class="col-6">
														<input type="text" class="form-control" id="surveySearch" placeholder="Search Site Name">
													</div>
												</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-12 col-md-12 col-sm-12 col-xl-12 pad-00 surveyListsTrack mg-top-10 hm-520">
								<div class="container table-responsive">
									<table class="table table-hover table-striped table-bordered table-repsonsive table-sm" id="survey_lists">
										<thead>
										<tr>
                      <th scope="col" style="width:5%">
												<div class="custom-control pad-left-rem custom-checkbox">
													<input type="checkbox" class="custom-control-input" id="chk_survey_item">
													<label class="custom-control-label" for="chk_survey_item"></label>
												</div>
											</th>
                      <th style="width:7% !important;" class="text-center text-uppercase text-muted" scope="col"><small>No.</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Site Name</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Survey Date</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Latitude</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Longitude</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Project</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>RRE</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>RFE</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>SAQ</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Category</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Vendor</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Status</small></th>
											<th class="text-center text-uppercase text-muted" scope="col"><small>Remarks</small></th>
										</tr>
										</thead>
										<tbody id="survey_datas"></tbody>
									</table>
								</div>
							</div>
							<div class="col-12">
								<center>
									<div class="text-center" id='survey_pagination'> </div>
								</center>
							</div>
						</div>
					</div>
				</div>

				<div class="tab-pane fade" id="calendar-pills" role="tabpanel" aria-labelledby="calendar-pills-tab">
					<div class="loaders d-none">
						<h5 class="text-center text-white">
							<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>
							<h4 class="text-dark text-center">Loading</h4>
						</h5>
					</div>
					<div class="col-12">
						<div class="container">
							<div class="row">
								<div class="col-12">
									<div class='my-legend'>
										<div class='legend-title text-mute'>Legends: Indicates the status of a Site.</div>
											<div class='legend-scale'>
												<ul class='legend-labels'>
													<li><span style='background:#f6da63;'></span>For Survey</li>
													<li><span style='background:#46b3e6;'></span>Surveyed</li>
													<li><span style='background:#ed8240;'></span>Re-Survey</li>
													<li><span style='background:#ea5e5e;'></span>Master Plan</li>
													<li><span style='background:#d89cf6;'></span>EWO / OWO</li>
													<li><span style='background:#dedef0;'></span>#N/A</li>
												</ul>
											</div>
									</div>
								</div>
								<div class="col-12">
									<div class="calendar mg-top-10"></div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="tab-pane fade" id="locate-pills" role="tabpanel" aria-labelledby="locate-pills-tab">
					<div class="loaders d-none">
						<h5 class="text-center text-white">
							<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>
							<h4 class="text-dark text-center">Loading</h4>
						</h5>
					</div>

					<div class="col-12">
						<div class="container">
							<div class="row">
								<div class="col-12">
									<div class='my-legend'>
										<div class='legend-title text-mute'>Legends: Indicates the status of a Site.</div>
											<div class='legend-scale'>
												<ul class='legend-labels'>
													<li><span style='background:#f6da63;'></span>For Survey</li>
													<li><span style='background:#46b3e6;'></span>Surveyed</li>
													<li><span style='background:#ed8240;'></span>Re-Survey</li>
													<li><span style='background:#ea5e5e;'></span>Master Plan</li>
													<li><span style='background:#d89cf6;'></span>EWO / OWO</li>
													<li><span style='background:#dedef0;'></span>#N/A</li>
												</ul>
											</div>
									</div>
								</div>
								<div class="col-12">
									<div class="track_map mg-top-10"></div>
								</div>
							</div>
						</div>
					</div>

					<!-- <div class="col-12 pad-00">
						<div class="container pad-00">
							<div class="row">
								<div class="col-12">
									<div class="container">
										<div class='my-legend'>
											<div class='legend-title text-mute'>Legends: Indicates the status of a Site.</div>
												<div class='legend-scale'>
													<ul class='legend-labels'>
														<li><span style='background:#f6da63;'></span>For Survey</li>
														<li><span style='background:#46b3e6;'></span>Surveyed</li>
														<li><span style='background:#ed8240;'></span>Re-Survey</li>
														<li><span style='background:#ea5e5e;'></span>Master Plan</li>
														<li><span style='background:#d89cf6;'></span>EWO / OWO</li>
														<li><span style='background:#dedef0;'></span>#N/A</li>
													</ul>
												</div>
										</div>
									</div>
								</div>
								<div class="col-12">
									<div id="track_map" class="hm-450"></div>
								</div>
							</div>
						</div>
					</div> -->
				</div>

			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="import_survey_modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-center text-uppercase">Import</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form method="post" id="file-survey-upload" enctype="multipart/form-data">
						<div class="files">
							<p class="text-center lead h4">Upload File <i>(accepts .csv, .xlsx & .xls files only)</i></p>
							<input type="file" name="survey_file" id="survey_file" class="form-control btn" accept=".csv, .xlsx, .xls" required>
						</div>

						<div class="container">
							<div class='progress bg-secondary' id="progresSurveyId">
								<div class="progress-bar" id="progressBarSurvey"></div>
								<div class="percent_survey text-white" id="percentSurvey">0%</div>
							</div>
						</div>
				</div>
					<div class="modal-footer">
						<div class="col-12 text-center">
							<button class="btn btn-outline-danger text-center" id="removesurveyButton" type="button"><i class="fas fa-eraser"></i> Remove</button>
							<button class="btn btn-outline-primary text-center" id="submitsurveyButton" type="submit"><i class="fas fa-file-upload"></i> Upload</button>
						</div>
					</div>
				</form>
		</div>
	</div>
</div>

<div class="modal fade pad-00" id="add_survey_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-full mg-top-0" role="document">
		<div class="modal-content rounded-0 border-0 	">
			<div class="modal-header text-uppercase text-muted rounded-0">
				<h5 class="modal-title" id="surveyTitleTrack">Add Survey Track</h5>
				<button type="button" class="close survey_close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body mh-400">
				<form id="edd_survey_Track" method="POST">
					<input type="hidden" id="s_status" name="s_status" value="0">
					<div class="table-responsive">
						<table class="table table-bordered table-responsive table-sm w-2000" id="eddsurveytable">
							<thead>
								<tr>
									<th class="text-center text-uppercase text-muted" scope="col" style="width:20%"><small>SITE NAME</small></th>
									<th class="text-center text-uppercase text-muted" scope="col" style="width:2%"><small>Survey Date</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Latitude</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Longtitude</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>Project</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>RRE</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>RFE</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>SAQ</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>category</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>vendor</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>status</small></th>
									<th class="text-center text-uppercase text-muted" scope="col"><small>remarks</small></th>
									<th class="text-center text-uppercase text-muted"><button type="button" class="btn btn-primary btn-sm append_survey_track"><i class="fas fa-plus"></i></button></th>
								</tr>
							</thead>
							<tbody id="survey_track_tables">
								<tr class="survey_rows-0">
									<td>
										<input type="hidden" name="survey_iid[]" class="form-control survey-iid" id="survey-iid">
										<input type="text" name="survey_site_name[]" class="form-control survey_site_name input-sm" id="survey_site_name" placeholder="Site Name" required>
									</td>
									<td><input type="date" name="survey_date[]" class="form-control survey_date input-sm" id="survey_date" placeholder="Survey Date" required></td>
									<td><input type="text" name="survey_latitude[]" class="form-control survey_latitude input-sm" id="survey_latitude" placeholder="Latitude" required></td>
									<td><input type="text" name="survey_longtitude[]" class="form-control survey_longtitude input-sm" id="survey_longtitude" placeholder="Longtitude" required></td>
									<td><input type="text" name="survey_project[]" class="form-control survey_project input-sm" id="survey_project" placeholder="Project" required></td>
									<td><select class="form-control survey_rre" name="survey_rre[]" id="survey_rre input-sm"></select></td>
									<td><input type="text" name="survey_rfe[]" class="form-control survey_rfe input-sm" id="survey_rfe" placeholder="RFE" required></td>
									<td><input type="text" name="survey_saq[]" class="form-control survey_saq input-sm" id="survey_saq" placeholder="SAQ" required></td>
									<td><input type="text" name="survey_category[]" class="form-control survey_category input-sm" id="survey_category" placeholder="Category" required></td>
									<td><input type="text" name="survey_vendor[]" class="form-control survey_vendor input-sm" id="survey_vendor" placeholder="Vendor" required></td>
									<td><input type="text" name="survey_status[]" class="form-control survey_status input-sm" id="survey_status" placeholder="Status" required></td>
									<td><input type="text" name="survey_remarks[]" class="form-control survey_remarks input-sm" id="survey_remarks" placeholder="Remarks" required></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>

			</div>
			<div class="modal-footer rounded-0 text-center">
				<div class="col-12">
					<button type="button" class="btn btn-outline-secondary text-center text-uppercase survey_close" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-outline-primary text-center text-uppercase">Submit</button>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade pad-00" id="viewsurveyModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="viewTrackModal" aria-hidden="true">
  <div class="modal-dialog modal-full mg-top-0" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">View Survey Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
					<table class="table table-bordered  text-center table-sm" id="viewsurveytable">
						<thead>
							<tr>
								<th class="text-center text-uppercase text-muted" scope="col" style="width:3%"><small>No.</small></th>
								<th class="text-center text-uppercase text-muted" scope="col" style="width:20%"><small>SITE NAME</small></th>
								<th class="text-center text-uppercase text-muted" scope="col"><small>Survey Date</small></th>
								<th class="text-center text-uppercase text-muted" scope="col"><small>Latitude</small></th>
								<th class="text-center text-uppercase text-muted" scope="col"><small>Longtitude</small></th>
								<th class="text-center text-uppercase text-muted" scope="col"><small>Project</small></th>
								<th class="text-center text-uppercase text-muted" scope="col"><small>RRE</small></th>
								<th class="text-center text-uppercase text-muted" scope="col"><small>RFE</small></th>
								<th class="text-center text-uppercase text-muted" scope="col"><small>SAQ</small></th>
								<th class="text-center text-uppercase text-muted" scope="col"><small>category</small></th>
								<th class="text-center text-uppercase text-muted" scope="col"><small>vendor</small></th>
								<th class="text-center text-uppercase text-muted" scope="col"><small>status</small></th>
								<th class="text-center text-uppercase text-muted" scope="col"><small>remarks</small></th>
							</tr>
						</thead>
						<tbody id="view_survey_build"></tbody>
					</table>
      </div>
    </div>
  </div>
</div>
