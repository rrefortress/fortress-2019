
<div class="card border shadow-lg h-511">
	<div class="card-header border-0 bg-white">
		<div class="row align-items-center">
			<div class="col">
				<h6 class="card-title text-uppercase text-muted mb-0">Activities</h6>
			</div>
			<div class="col-sm col-md col-xl text-right">
				<input type="hidden" id="surveyNewNames" value="All">
				<button type="button" id="view_modal" class="btn btn-sm btn-outline-warning c-see-all" data-toggle="tooltip" data-placement="top" title="View Calendar"><i class="fas fa-calendar"></i> <small>Calendar</small></button>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="act-list mg-bot-10 text-center" style="max-width: 540px;"></div>
			</div>
		</div>
	</div>
</div>
