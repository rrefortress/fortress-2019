<div class="card card-stats border border-secondary shadow-lg">
	<div id="tracks" class="container-fluid">
			<div class="chart-container" style="position: relative;">
				<canvas height="2vh" width=10vw" id="projects_chart"></canvas>
			</div>

			<div class="middle">
				<a type="button" href="<?= base_url("file_tracking"); ?>" class="btn btn-xm btn-info view_all shadow-lg text-uppercase "><small>View All</small></a>
			</div>
	</div>
</div>


