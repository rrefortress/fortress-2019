<?php $this->load->view('/layout/header'); ?>
	<title>Fortress - Project Management</title>
</head>
<body>
	<?php $this->load->view('/layout/side_menu'); ?>
	<div class="container-fluid mg-top-77">
		<div class="h-520">
			<table class="table">
			  <thead>
			  	<tr>
			  		<th colspan="5">List of Users</th>
			  		<th><button class="btn btn-success" data-toggle="modal" data-target="#addUserModal"><span><img class="icon_size" src="<?php echo base_url().'style/icons/add-user-2-64.png'; ?>"></span> Add User</button></th>
			  	</tr>
			    <tr>
			      <th scope="col">#Username</th>
			      <th scope="col">Name</th>
			      <th scope="col">Department</th>
			      <th scope="col">Access Level</th>
			      <th scope="col">Status</th>
			      <th scope="col">Actions</th>
			    </tr>
			  </thead>
			  <tbody id="users_list">
			  	<!-- User List Here -->
			  </tbody>
			</table>
		</div>


</body>
<?php $this->load->view('/layout/footer'); ?>
