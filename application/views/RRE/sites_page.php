	<div class="container-fluid mg-top-77">
		<!-- <div class="col"> -->
			<!-- <div class="row"> -->
				<div class="col-md col-lg col-sm-12 col-xs-12 p-2 my-2 bg-white rounded shadow-lg border border-secondary">
					<div class="row">
						<div class="col px-0">
							<div id="sitestitle" class="row align-items-center mx-1 p-1 text-light rounded-top">
								<!-- <div class="col text-right mx-auto mt-1"><h4>Sites</h4></div> -->
								<div class="col text-right">
									<a href="<?php echo base_url('add_site'); ?>">
										<button class="btn btn-outline-success btn-sm" type="button">
											<img class="icon_size" src="<?php echo base_url().'style/icons/add-database-64.png'; ?>"> Add Site
										</button>
										</a>
									</div>
							</div>
							<div class="row mx-4 my-2">
								<!-- <div class="col-8 mb-1 input-group">
									<input type="text" name="search" id="search" class="col-5 form-control form-control-sm border border-secondary" aria-label="Text input with dropdown button">
									<select class="col-2 custom-select custom-select-sm border border-secondary" id="inputGroupSelect04">
										<option selected disabled text-muted>Search Sites by...</option>
										<option value="1">Site Name</option>
										<option value="2">PLAID</option>
										<option value="3">Area</option>
									</select>
									<div class="input-group-append">
										<button class="btn btn-outline-secondary btn-sm" name="search_btn" id="search_btn" type="button">Search Site</button>
									</div>
								</div> -->
<!-- 								<div class="col text-right">
									<a href="<?php echo base_url('addsitepage'); ?>">
										<button class="btn btn-outline-success btn-sm" type="button">
											<img class="icon_size" src="<?php echo base_url().'style/icons/add-database-64.png'; ?>"> Add Site
										</button>
										</a>
									</div> -->
							</div>
						</div>
						</div>
						<table id="sites" class="table table-xxxs table-responsive table-hover table-striped text-center">
							<thead>
								<tr>
									<th scope="col">DB ID</th>
									<th scope="col">Site Name</th>
									<th scope="col">PLA ID</th>
									<th scope="col">Area</th>
									<th scope="col">Coordinates</th>
									<th scope="col">Site Type</th>
									<th scope="col">Tower type</th>
									<th scope="col">Tower Height</th>
									<th scope="col">Region</th>
									<th scope="col">Site Address</th>
									<th scope="col">Audited</th>
									<th scope="col">Permanent</th>
									<th scope="col">Status</th>
								</tr>
							</thead>
<!-- 							<tbody id="site_list">
								User List Here
							</tbody> -->
						</table>
					</div>
				<!-- </div> -->
			<!-- </div> -->

		</div>

		<div class="modal fade" id="addSiteModal" tabindex="-1" role="dialog"  aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Add Site</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form>
							<div class="form-row">
								<div class="col-md-4 mb-3">
									<label>Province:</label>
									<select class="form-control form-control-sm" id="addprovince_sitename" required>
										<option value selected disabled>--Please Select Province--</option>
									</select>
								</div>
								<div class="col-md-4 mb-3">
									<label>Town:</label>
									<select class="form-control form-control-sm" id="addtown_sitename" required>
										<option value selected disabled>--Please Select Province First--</option>
									</select>
								</div>
								<div class="col-md-4 mb-3">
									<label>Barangay:</label>
									<input type="text" class="form-control form-control-sm" id="addbarangay_sitename" placeholder="Enter Barangay" required>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-4 mb-3">
									<label>Street:</label>
									<input type="text" class="form-control form-control-sm" id="addstreet_sitename" placeholder="Enter Street" required>
									<div class="invalid-feedback">
										Please provide a valid street name.
									</div>
								</div>
								<div class="col-md-4 mb-3">
									<label>Building Name (if applicable):</label>
									<input type="text" class="form-control form-control-sm" id="addbldg_sitename" placeholder="Enter Building Name">
								</div>
							</div>
							<h6>Coordinates:</h6>
							<div class="form-row">
								<div class="col-md-4 mb-3">
									<label>Latitude:</label>
									<input type="text" class="form-control form-control-sm" id="addlatitude_sitename" placeholder="Enter Latitude" required>
									<div class="invalid-feedback">
										Please provide a valid latitude.
									</div>
								</div>
								<div class="col-md-4 mb-3">
									<label>Longitude:</label>
									<input type="text" class="form-control form-control-sm" id="addlongitude_sitename" placeholder="Enter Longitude" required>
									<div class="invalid-feedback">
										Please provide a valid longitude.
									</div>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-4 mb-3">
									<label>PLA ID:</label>
									<input type="text" class="form-control form-control-sm" id="addplaid_sitename" placeholder="Enter PLA ID" required>
									<div class="invalid-feedback">
										Please provide a valid PLA ID.
									</div>
								</div>
							</div>

							<div class="col-md-6 mx-auto"><h5>Create Site Name:</h5></div>
							<div class="col-md-6 input-group mx-auto my-3">
								<div class="input-group-prepend">
									<span class="input-group-text"><b>Site Name</b></span>
								</div>
								<input type="text" class="form-control" placeholder="Enter Site Name" aria-label="Site Name" id="suggested_sitename" required>
								<div class="input-group-append">
									<button class="btn btn-success" type="submit" id="suggestbutton_sitename">Suggest Name</button>
								</div>
							</div>
							<div class="row">
								<div class="mx-auto">
									<span>Choose Technology:</span>
									<div class="checkbox text-middle">
										<label class="mx-2"><input type="checkbox" id="G900"> G900</label>
										<label class="mx-2"><input type="checkbox" id="G1800"> G1800</label>
										<label class="mx-2"><input type="checkbox" id="U900"> U900</label>
										<label class="mx-2"><input type="checkbox" id="U2100"> U2100</label>
										<label class="mx-2"><input type="checkbox" id="L700"> L700</label>
										<label class="mx-2"><input type="checkbox" id="L1800"> L1800</label>
										<label class="mx-2"><input type="checkbox" id="L2300"> L2300</label>
										<label class="mx-2"><input type="checkbox" id="L2600"> L2600</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="mx-auto">
									<button type="submit" class="btn btn-primary" id="create_site">Create Site</button>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" data-dismiss="modal" id="btn_addsite">Add Site</button>
					</div>
				</div>
			</div>
		</div>

	<div class="modal fade" id="modalsitespage" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<div class="modal-header p-2">
					<h5 class="modal-title"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="">
					<table class="m-0 table table-striped table-hover table-sm table-light table-responsive">
						<thead class="thead-dark" >

						</thead>
						<tbody id="">
							<!-- 2G more details -->
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
<!-- 					<button id="acceptapprovalbutton" type="button" class="btn btn-primary">Submit</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
				</div>

			</div>
		</div>
	</div>
