<?php $this->load->view('/layout/header'); ?>
	<title>Fortress - Site Profile</title>
</head>
<body>
	<div class="container-fluid">
		<div>
			<?php $this->load->view('/layout/side_menu'); ?>
		</div>
		<div>
			<table class="table">
			  <thead>
			  	<tr>
			  		<th class="text-center" style="font-size: 1.5rem" colspan="8">Site Profile</th>
			  	</tr>
			  </thead>
			 </table>
		 <div class="mx-auto" style="margin-top: 1.5rem; width: 30rem">
			<div class="input-group">
			  <input type="text" class="form-control border border-secondary" aria-label="Text input with dropdown button">
			  <div class="input-group-append">
			    <button class="btn btn-outline-secondary" type="button">Search Site</button>
			  </div>
			</div>
		 </div>
		</div>

		<div class="container" style="margin-bottom: 3rem; margin-top: 2rem; font-size: 0.95rem">
		  <div class="row">

			<div class="col">
			  <div class="p-3 mb-2 bg-dark text-white border border-primary rounded-sm">
				<div class="col-12 text-center p-1 mb-3 bg-primary text-white border rounded-sm" style="font-size: 1.1rem">Site Information</div>

				<div class="col">
				<table>
				<tbody id="site_list">
					<tr>
				      <th scope="row">Site Name :</th>
				      <td>LAHUG</td>
				      <td><button class="btn btn-outline-light border border-primary" style="padding-top: 0.001rem; padding-left: 0.35rem; padding-right: 0.24rem; padding-bottom: 0.18rem" data-toggle="modal" data-target="#editSiteModal"><span><img class="icon_size" src="<?php echo base_url().'style/icons/edit-property-64.png'; ?>"></span></button></td>
				    </tr>
				    <tr>
				      <th scope="row">PLA ID :</th>
				      <td>VIS148</td>
				    </tr>
				    <tr>
				      <th scope="row">Geo Area :</th>
				      <td>VIS</td>
				    </tr>
				    <tr>
				      <th class="align-baseline" scope="row" style="width: 8rem">Site Address :</th>
				      <td>Asiatown IT Park, Salinas Drive, Brgy. Lahug (Pob.), Cebu City, Cebu</td>
				    </tr>
				    <tr>
				      <th scope="row">Province :</th>
				      <td>Cebu</td>
				    </tr>
				    <tr>
				      <th scope="row">Town :</th>
				      <td>Cebu City</td>
				    </tr>
				    <tr>
				      <th scope="row">Barangay :</th>
				      <td>Lahug (Pob.)</td>
				    </tr>
				    <tr>
				      <th scope="row">Coordinates :</th>
				      <td>123.908111, 10.329969</td>
				    </tr>
				    <tr>
				      <th scope="row">Coverage :</th>
				      <td>Outdoor</td>
				    </tr>
					  </tbody>
				</table>
				</div>
			</div>
			<div class="p-3 mb-2 bg-dark text-white border rounded-sm" style="margin-top: 1.5rem">
				<div class="col-12 text-center p-1 mb-3 bg-primary text-white border rounded-sm" style="font-size: 1.1rem">Site History</div>

				<div class="col">
				<table>
				<tbody id="site_list">
					<tr>
				      <th scope="row">Something :</th>
				      <td>SOMETHING</td>
				    </tr>
				    <tr>
				      <th scope="row">Something 1 :</th>
				      <td>SOMETHING!</td>
				    </tr>
					  </tbody>
				</table>
				</div>
			</div>
		</div>

		<div class="col">
			<div class="p-3 mb-2 bg-dark text-white border border-primary rounded-sm">
				<div class="col-12 text-center p-1 mb-3 bg-primary text-white border rounded-sm" style="margin-bottom: 2rem; font-size: 1.1rem">Antenna Details</div>

				<div class="col" style="margin-bottom: 1rem">
					<div class="btn-group nav nav-pills" role="group" aria-label="First group">
				    	<button class="btn btn-outline-secondary text-white border border-secondary show active" style="padding: 0.2px" data-toggle="pill" href="#g2">2G</button>
				    	<button class="btn btn-outline-secondary text-white border border-secondary" style="padding: 0.2px" data-toggle="pill" href="#g3">3G</button>
				    	<button class="btn btn-outline-secondary text-white border border-secondary" style="padding: 0.2px" data-toggle="pill" href="#g4">4G</button>
				 	</div>
			 	</div>
				<div class="tab-content">
				    <div id="g2" class="tab-pane fade in show active">
				     <div class="row">
				      <div class="col" style="margin-bottom: 1.5rem"><h3>2G Technology</h3></div>
				      <div class="col text-right" style="margin-top: 1.5rem; margin-right: 1.5rem"><button class="btn btn-outline-light border border-primary" style="padding-top: 0.001rem; padding-left: 0.35rem; padding-right: 0.24rem; padding-bottom: 0.18rem" data-toggle="modal" data-target="#editAntennaModal"><span><img class="icon_size" src="<?php echo base_url().'style/icons/edit-property-64.png'; ?>"></span></button></div>
				  	</div>
				      <table class="table table-hover table-sm table-dark">
						  <thead>
						    <tr>
						      <th scope="col">Cellname</th>
						      <th scope="col">Cell ID</th>
						      <th scope="col">Ant. Height (m)</th>
						      <th scope="col">Azimuth</th>
						      <th scope="col">Coverage</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr>
						      <th scope="row">LAHUG-1</th>
						      <td>13204</td>
						      <td class="text-center">30</td>
						      <td>60°</td>
						      <td>Outdoor</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUG-2</th>
						      <td>23204</td>
						      <td class="text-center">30</td>
						      <td>160°</td>
						      <td>Outdoor</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUG-3</th>
						      <td>33204</td>
						      <td class="text-center">43</td>
						      <td>260°</td>
						      <td>Outdoor</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUGX-1</th>
						      <td>43204</td>
						      <td class="text-center">30</td>
						      <td>60°</td>
						      <td>Outdoor</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUGX-2</th>
						      <td>53204</td>
						      <td class="text-center">30</td>
						      <td>160°</td>
						      <td>Outdoor</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUGX-3</th>
						      <td>3204</td>
						      <td class="text-center">43</td>
						      <td>260°</td>
						      <td>Outdoor</td>
						    </tr>
						    <tr>

							</tr>
						  </tbody>
						</table>
					<div class="text-right">
						<button class="btn btn-outline-light border border-light" style="padding-top: 0.001rem; padding-left: 0.35rem; padding-right: 0.24rem; padding-bottom: 0.18rem" data-toggle="modal" data-target="#moreSiteDetailsModal">More Site Details >></button>
					</div>
					<div>
						<hr style="border-color: white"></hr>
						<div style="margin-bottom: 1.2rem"><h4>RF Configuration</h4></div>
						<table class="table table-hover table-borderless table-sm table-dark">
						  <thead>
						    <tr>
						      <th style="font-size: 1.2rem" scope="col">Technology</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr>
						      <th scope="col">GSM900: </th>
						      <td>4+4+4+0+0+0</td>
						    </tr>
						    <tr>
						      <th scope="row">GSM1800: </th>
						      <td>6+7+8+0+0+0</td>
						    </tr>
						  </tbody>
						</table>
						</div>
				    </div>
				    <div id="g3" class="tab-pane fade in">
				     <div class="row">
				      <div class="col" style="margin-bottom: 1.5rem"><h3>3G Technology</h3></div>
				      <div class="col text-right" style="margin-top: 1.5rem; margin-right: 1.5rem"><button class="btn btn-outline-light border border-primary" style="padding-top: 0.001rem; padding-left: 0.35rem; padding-right: 0.24rem; padding-bottom: 0.18rem" data-toggle="modal" data-target="#editAntennaModal"><span><img class="icon_size" src="<?php echo base_url().'style/icons/edit-property-64.png'; ?>"></span></button></div>
				  	</div>
				      <table class="table table-hover table-sm table-dark">
						  <thead>
						    <tr>
						      <th scope="col">Cellname</th>
						      <th scope="col">Cell ID</th>
						      <th scope="col">Ant. Height (m)</th>
						      <th scope="col">Azimuth</th>
						      <th scope="col">Coverage</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr>
						      <th scope="row">LAHUG-1</th>
						      <td>13204</td>
						      <td class="text-center">30</td>
						      <td>60°</td>
						      <td>Outdoor</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUG-2</th>
						      <td>23204</td>
						      <td class="text-center">30</td>
						      <td>160°</td>
						      <td>Outdoor</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUG-3</th>
						      <td>33204</td>
						      <td class="text-center">43</td>
						      <td>260°</td>
						      <td>Outdoor</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUGX-1</th>
						      <td>43204</td>
						      <td class="text-center">30</td>
						      <td>60°</td>
						      <td>Outdoor</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUGX-2</th>
						      <td>53204</td>
						      <td class="text-center">30</td>
						      <td>160°</td>
						      <td>Outdoor</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUGX-3</th>
						      <td>3204</td>
						      <td class="text-center">43</td>
						      <td>260°</td>
						      <td>Outdoor</td>
						    </tr>
						  </tbody>
						</table>
							<div class="text-right">
							<button class="btn btn-outline-light border border-light" style="padding-top: 0.001rem; padding-left: 0.35rem; padding-right: 0.24rem; padding-bottom: 0.18rem" data-toggle="modal" data-target="#moreSiteDetailsModal">More Site Details >></button>
							</div>
						<div>
						<hr style="border-color: white"></hr>
						<div style="margin-bottom: 1.2rem"><h4>RF Configuration</h4></div>
						<table class="table table-hover table-borderless table-sm table-dark">
						  <thead>
						    <tr>
						      <th style="font-size: 1.2rem" scope="col">Technology</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr>
						      <th scope="row">UMTS900: </th>
						      <td>2+2+2+0+0+0</td>
						    </tr>
						    <tr>
						      <th scope="row">UMTS2100: </th>
						      <td>2+2+2+2+2+2</td>
						    </tr>
						  </tbody>
						</table>
						</div>
				    </div>
				   <div id="g4" class="tab-pane fade in">
				     <div class="row">
				      <div class="col" style="margin-bottom: 1.5rem"><h3>4G Technology</h3></div>
				      <div class="col text-right" style="margin-top: 1.5rem; margin-right: 1.5rem"><button class="btn btn-outline-light border border-primary" style="padding-top: 0.001rem; padding-left: 0.35rem; padding-right: 0.24rem; padding-bottom: 0.18rem" data-toggle="modal" data-target="#editAntennaModal"><span><img class="icon_size" src="<?php echo base_url().'style/icons/edit-property-64.png'; ?>"></span></button></div>
				  	</div>
				      <table class="table table-hover table-sm table-dark">
						  <thead>
						    <tr>
						      <th scope="col">Cellname</th>
						      <th scope="col">Cell ID</th>
						      <th scope="col">Ant. Height (m)</th>
						      <th scope="col">Azimuth</th>
						      <th scope="col">Coverage</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr>
						      <th scope="row">LAHUG-1</th>
						      <td>13204</td>
						      <td class="text-center">30</td>
						      <td>60°</td>
						      <td>Outdoor</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUG-2</th>
						      <td>23204</td>
						      <td class="text-center">30</td>
						      <td>160°</td>
						      <td>Outdoor</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUG-3</th>
						      <td>33204</td>
						      <td class="text-center">43</td>
						      <td>260°</td>
						      <td>Outdoor</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUGX-1</th>
						      <td>43204</td>
						      <td class="text-center">30</td>
						      <td>60°</td>
						      <td>Outdoor</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUGX-2</th>
						      <td>53204</td>
						      <td class="text-center">30</td>
						      <td>160°</td>
						      <td>Outdoor</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUGX-3</th>
						      <td>3204</td>
						      <td class="text-center">43</td>
						      <td>260°</td>
						      <td>Outdoor</td>
						    </tr>
						  </tbody>
						</table>
							<div class="text-right">
								<button class="btn btn-outline-light border border-light" style="padding-top: 0.001rem; padding-left: 0.35rem; padding-right: 0.24rem; padding-bottom: 0.18rem" data-toggle="modal" data-target="#moreSiteDetailsModal">More Site Details >></button>
							</div>
						<div>
						<hr style="border-color: white"></hr>
						<div style="margin-bottom: 1.2rem"><h4>RF Configuration</h4></div>
						<table class="table table-hover table-borderless table-sm table-dark">
						  <thead>
						    <tr>
						      <th style="font-size: 1.2rem" scope="col">Technology</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr>
						      <th scope="col">L700: </th>
						      <td>1+1+1+0+0+0</td>
						    </tr>
						    <tr>
						      <th scope="row">L1800: </th>
						      <td>2+0+2+0+2+0</td>
						    </tr>
						    <tr>
						      <th scope="row">L2300: </th>
						      <td>Not found</td>
						    </tr>
						    <tr>
						      <th scope="row">L2600: </th>
						      <td>2+3+2+0+0+0</td>
						    </tr>
						    <tr>
						      <th scope="row">L2600 MM: </th>
						      <td>2+0+2+0+0+0</td>
						    </tr>
						  </tbody>
						</table>
						</div>
				    </div>
				    </div>
				</div>
			</div>
		</div>

	</div>
  </div>
</div>

<div class="modal fade" id="editSiteModal" tabindex="-1" role="dialog"  aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title">Edit Site</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <form>
		          <!-- Input UserName -->
				  <div class="form-group">
				    <label for="sitename">Site Name</label>
				    <input type="text" class="form-control" id="sitename" placeholder="Enter Site Name">
				  </div>
				  <!-- Input First Name -->
				  <div class="form-group">
				  	<label for="plaid">PLA ID</label>
				    <input type="text" class="form-control" id="plaid" placeholder="Enter PLA ID">
				  </div>
				  <!-- Select Area -->
				  <div class="form-group">
				    <label for="area">Area</label>
				    <select class="form-control" id="area">
				      <option value="VIS">VIS</option>
				      <option value="MIN">MIN</option>
				      <option value="NLZ">NLZ</option>
				      <option value="SLZ">SLZ</option>
				      <option value="NCR">NCR</option>
				    </select>
				  </div>
				  <!-- Input Last Name -->
				  <div class="form-group">
				  	<label for="siteaddress">Site Address</label>
				    <input type="text" class="form-control" id="siteaddress" placeholder="Enter Site Address">
				  </div>
				  <!-- Select Assigned Area -->
				  <div class="form-group">
				    <label for="province">Province</label>
				    <select class="form-control" id="province">
				      <option value="VIS">Cebu</option>
				      <option value="MIN">Negros Oriental</option>
				      <option value="NLZ">Bohol</option>
				      <option value="SLZ">Siquijor</option>
				      <option value="NCR">Leyte</option>
				    </select>
				  </div>
				  <!-- Input Last Name -->
				  <div class="form-group">
				  	<label for="town">Town</label>
				    <input type="text" class="form-control" id="town" placeholder="Enter Town">
				  </div>
				  <!-- Input Last Name -->
				  <div class="form-group">
				  	<label for="barangay">Barangay</label>
				    <input type="text" class="form-control" id="barangay" placeholder="Enter Barangay">
				  </div>
				  <div class="form-group">
				    <label for="coverage">Coverage</label>
				    <select class="form-control" id="coverage">
				      <option value="Outdoor">Outdoor</option>
				      <option value="Indoor">Indoor</option>
				      <option value="Indoor/Outdoor">Indoor/Outdoor</option>
				    </select>
				  </div>
				  <div class="dropdown-divider"></div>
				  	<label><b>Coordinates:</b></label>
				  <!-- Input Last Name -->
				  <div class="form-group">
				  	<label for="titude">Latitude</label>
				    <input type="text" class="form-control" id="latitude" placeholder="Enter Latitude">
				  </div>
				  <!-- Input Last Name -->
				  <div class="form-group">
				  	<label for="longitude">Longitude</label>
				    <input type="text" class="form-control" id="longitude" placeholder="Enter Longitude">
				  </div>
				  <div class="dropdown-divider"></div>
				  <!-- Select Assigned Area -->

				</form>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary" data-dismiss="modal" id="btn_editsite">Edit Site</button>
		      </div>
		    </div>
		  </div>
		</div>

<div class="modal fade" id="editAntennaModal" tabindex="-1" role="dialog"  aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title">Edit Antenna Details</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <form>
		          <!-- Input UserName -->
				  <div class="form-group">
				    <label for="sitename">Cellname</label>
				    <input type="text" class="form-control" id="cellname" placeholder="Enter Cellname">
				  </div>
				  <!-- Input First Name -->
				  <div class="form-group">
				  	<label for="plaid">Cell ID</label>
				    <input type="text" class="form-control" id="cellid" placeholder="Enter Cell ID">
				  </div>
				  <!-- Select Area -->
				  <div class="form-group">
				    <label for="area">Antenna Height</label>
						<input type="text" class="form-control" id="antennaheight" placeholder="Enter Antenna Height">
				  </div>
				  <!-- Input Last Name -->
				  <div class="form-group">
				  	<label for="siteaddress">Azimuth</label>
				    <input type="text" class="form-control" id="azimuth" placeholder="Enter Azimuth">
				  </div>
				  <div class="form-group">
				    <label for="coverage">Coverage</label>
				    <select class="form-control" id="cellcoverage">
				      <option value="Outdoor">Outdoor</option>
				      <option value="Indoor">Indoor</option>
				    </select>
				  </div>
				</form>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary" data-dismiss="modal" id="btn_editantenna">Edit Site</button>
		      </div>
		    </div>
		  </div>
		</div>

<div class="modal fade" id="moreSiteDetailsModal" tabindex="-1" role="dialog" aria-hidden="true">
		  <div class="modal-dialog modal-xl" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title">More Site Details</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		     <div class="modal-body">
		     	<table class="table table-hover table-sm table-light">
						  <thead>
						    <tr>
						      <th scope="col">Cellname</th>
						      <th scope="col">Antenna Model</th>
						      <th scope="col">M-tilt</th>
						      <th scope="col">E-tilt</th>
						      <th scope="col">MCC</th>
						      <th scope="col">MNC</th>
						      <th scope="col">LAC</th>
						      <th scope="col">RAC</th>
						      <th scope="col">NCC</th>
						      <th scope="col">BCC</th>
						      <th scope="col">BCCH</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr>
						      <th scope="row">LAHUG-1</th>
						      <td>K739630</td>
						      <td>0</td>
						      <td>0</td>
						      <td>515</td>
						      <td>02</td>
						      <td>23204</td>
						      <td>125</td>
						      <td>6</td>
						      <td>5</td>
						      <td>21</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUG-2</th>
						      <td>K739630</td>
						      <td>0</td>
						      <td>0</td>
						      <td>515</td>
						      <td>02</td>
						      <td>23204</td>
						      <td>125</td>
						      <td>6</td>
						      <td>5</td>
						      <td>21</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUG-3</th>
						      <td>K739630</td>
						      <td>0</td>
						      <td>0</td>
						      <td>515</td>
						      <td>02</td>
						      <td>23204</td>
						      <td>125</td>
						      <td>6</td>
						      <td>5</td>
						      <td>21</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUGX-1</th>
						      <td>K739630</td>
						      <td>0</td>
						      <td>0</td>
						      <td>515</td>
						      <td>02</td>
						      <td>23204</td>
						      <td>125</td>
						      <td>6</td>
						      <td>5</td>
						      <td>21</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUGX-2</th>
						      <td>K739630</td>
						      <td>0</td>
						      <td>0</td>
						      <td>515</td>
						      <td>02</td>
						      <td>23204</td>
						      <td>125</td>
						      <td>6</td>
						      <td>5</td>
						      <td>21</td>
						    </tr>
						    <tr>
						      <th scope="row">LAHUGX-3</th>
						      <td>K739630</td>
						      <td>0</td>
						      <td>0</td>
						      <td>515</td>
						      <td>02</td>
						      <td>23204</td>
						      <td>125</td>
						      <td>6</td>
						      <td>5</td>
						      <td>21</td>
						    </tr>
						  </tbody>
						</table>
		    </div>
		    <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		    </div>
		  </div>
		</div>

</body>
<?php $this->load->view('/layout/footer'); ?>
