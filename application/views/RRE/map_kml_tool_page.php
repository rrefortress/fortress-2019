<div class="mg-top-77">
	<div class="container-fluid hm-520 shadow-lg border border-secondary">
		<div class="row">
			<div class="col-3 pad-00 border-right border-secondary hm-520">
				<ul class="nav nav-pills nav-justified border-bottom border-secondary" id="kml-tab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active text-primary" id="pills-home-tab" data-toggle="pill" href="#home" role="tab" aria-controls="pills-home" aria-selected="true"><i class="fas fa-database"></i> Data</a>
					</li>
					<li class="nav-item">
						<a class="nav-link text-success" id="pills-profile-tab" data-toggle="pill" href="#excel" role="tab" aria-controls="pills-profile" aria-selected="false"><i class="fas fa-file-csv"></i> Excel</a>
					</li>
				</ul>

				<div class="tab-content">
					<div id="home" class="tab-pane in active">
						<div class="col-12">
							<div class="input-group mb-2 mr-sm-2 mg-top-10">
								<div class="input-group-prepend">
									<div class="input-group-text"><i class="fas fa-search"></i></div>
								</div>
								<input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Search">
							</div>
						</div>

						<div class="col-12 pad-00">
							<div class="overflowMap1">
								<table class="table table1-lists table-hover">
									<thead>
									<tr>
										<th scope="col" style="width: 20%">
											<div class="custom-control custom-checkbox my-1 mr-sm-2">
												<input type="checkbox" class="custom-control-input" id="chk_data">
												<label class="custom-control-label" for="chk_data"></label>
											</div>
										</th>
										<th scope="col">Name</th>
									</tr>
									</thead>
									<tbody id="table-lists-data"></tbody>
								</table>
							</div>
						</div>
						<div class="col-12 pad-00">
							<button class="btn btn-primary btn-block br-none" id="move_data" ><i class="fas fa-hand-point-right"></i> Move Data</button>
						</div>
					</div>
					<div id="excel" class="tab-pane">
						<div class="col-12">
							<div class="row">
								<div class="col-10">
									<div class="input-group mb-2 mr-sm-2 mg-top-10">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-search"></i></div>
										</div>
										<input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Search">
									</div>
								</div>
								<div class="col-2">
									<div class="mg-top-10 mg-left-24-">
										<button class="btn btn-outline-success" id="btnImportExcel" data-toggle="tooltip" data-placement="top" title="Import Excel File"><i class="fas fa-plus"></i></button>
									</div>
								</div>
							</div>
						</div>

						<div class="col-12 pad-00">
							<div class="overflowMap1">
								<table class="table table-hover" id="tbl_excel">
									<thead>
									<tr>
										<th scope="col" style="width: 20%">
											<div class="custom-control custom-checkbox my-1 mr-sm-2">
												<input type="checkbox" class="custom-control-input" id="chk_excel">
												<label class="custom-control-label" for="chk_excel"></label>
											</div>
										</th>
										<th scope="col">Name</th>
									</tr>
									</thead>
									<tbody id="table-lists-excel"></tbody>
								</table>
							</div>
						</div>
						<div class="col-12 pad-00">
							<button class="btn btn-success btn-block br-none" id="move_excel" disabled><i class="fas fa-hand-point-right"></i> Move Excel</button>
						</div>
					</div>
				</div>
			</div>

			<div class="col-3 pad-00 border-right border-secondary">
				<div class="col-12">
					<div class="input-group mb-2 mr-sm-2 mg-top-10">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="fas fa-search"></i></div>
						</div>
						<input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Search">
					</div>
				</div>
				<div class="col-12 pad-00">
					<div class="overflowMap">
						<table class="table table-hover" id="table_selected">
							<thead>
							<tr>
								<th scope="col" style="width: 20%;">
									<div class="custom-control custom-checkbox my-1 mr-sm-2">
										<input type="checkbox" class="custom-control-input" id="chk_sel">
										<label class="custom-control-label" for="chk_sel"></label>
									</div>
								</th>
								<th scope="col">Name</th>
							</tr>
							</thead>
							<tbody id="table_selected_datas">
							</tbody>
						</table>
					</div>

				</div>
				<div class="col-12 pad-00">
					<button class="btn btn-danger btn-block br-none" id="move_back" disabled><i class="fas fa-hand-point-left"></i> Move Back</button>
				</div>
			</div>

			<div class="col-6  pad-00">
				<div class="col-12  pad-00 border-bottom border-secondary h-451">
					<div class="overflowMap2">
						<table class="table table-hover" id="table_kml">
							<thead>
							<tr>
								<th scope="col" style="width: 20%;">
									<div class="custom-control custom-checkbox my-1 mr-sm-2">
										<input type="checkbox" class="custom-control-input" id="chk_view">
										<label class="custom-control-label" for="chk_view"></label>
									</div>
								</th>
								<th scope="col">Model</th>
								<th>Color</th>
								<th>Radius</th>
							</tr>
							</thead>
							<tbody id="table-viewer-lists"></tbody>
						</table>
					</div>
				</div>
				<div class="col-12 h-31 pad-00 border-bottom border-secondary">
					<div class="container">
						<center class="pad-top-3">
							<div class="row">
								<div class="col-2">
									<div class="custom-control custom-checkbox custom-control-inline">
										<input type="checkbox" class="custom-control-input " id="info" name="info">
										<label class="custom-control-label" for="info">Info</label>
									</div>
								</div>
								<div class="col-8">
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" class="custom-control-input" id="site" name="requesttype" value="site" checked>
										<label class="custom-control-label" for="site">Site only</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" class="custom-control-input" id="indoor" name="requesttype" value="indoor">
										<label class="custom-control-label" for="indoor">Indoor</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" class="custom-control-input" id="outdoor" name="requesttype" value="outdoor">
										<label class="custom-control-label" for="outdoor">Outdoor</label>
									</div>
								</div>
							</div>

						</center>
					</div>
				</div>
				<div class="col-12 pad-00">
					<button class="btn btn-warning btn-block br-none" id="download_kml" disabled><i class="fas fa-download"></i> Download</button>
				</div>

			</div>

		</div>
	</div>
</div>

<div class="modal fade" id="showImportExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Import File</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="text-center ">
					<form method="post" id="file-upload" enctype="multipart/form-data">
						<div class="form-group files">
							<p class="text-center lead h4">Upload File <i>(accepts .csv, .xlsx & .xls files only)</i></p>
							<input type="file" name="userfile" id="filename" class="form-control btn" accept=".csv, .xlsx, .xls" required>
						</div>

						<div class="container">
							<div class='progress bg-secondary' id="progressDivId">
								<div class="progress-bar" id="progressBar"></div>
								<div class="percent text-white" id="percent">0%</div>
							</div>
						</div>

						<div class="form-group mg-top-16">
							<button class="btn btn-outline-primary text-center" id="submitButton" type="submit" id="inputGroupFileAddon04"><i class="fas fa-file-upload"></i> Upload</button>
							<button class="btn btn-outline-danger text-center" type="button" id="clear_data_file"><i class="fas fa-eraser"></i> Remove</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
