<div class="container-fluid mg-top-77">
	<ul class="nav nav-pills" id="myTab" role="tablist">
		<li class="nav-item" role="presentation">
			<a class="nav-link text-uppercase teal active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fas fa-subscript"></i> Manual</a>
		</li>
		<li class="nav-item" role="presentation">
			<a class="nav-link text-uppercase teal" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><i class="far fa-list-alt"></i> Bulk</a>
		</li>
	</ul>
	<div class="tab-content" id="myTabContent">
		<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
			<div class="col-12 mg-top-10">
				<div class="row">
					<div class="col-4 border rounded shadow">
						<form id="generate">
							<div class="form-group">
								<label for="site" class="small text-muted">Planned Tech <small>(Separated with commas only)</small></label>
								<input type="text" class="form-control planned-tech text-center clp" placeholder="Enter Planned Tech" autofocus required>
							</div>

							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="sectors" class="small text-muted">No. of Sectors</label>
									<select class="custom-select" id="sectors">
										<option>2</option>
										<option selected>3</option>
										<option>4</option>
									</select>
								</div>
								<div class="form-group col-md-6">
									<label for="acl" class="small text-muted">ACL</label>
									<input type="number" class="form-control clp" id="acl" placeholder="Enter ACL" required>
								</div>
							</div>
							<button class="btn btn-primary lead btn-block generate" type="submit">GENERATE</button>
						</form>
						<button class="mg-top-10 btn btn-danger lead btn-block clear" type="button">CLEAR</button>
					</div>
					<div class="col-8">
						<div class="col-12 shadow bg-white border rounded">
							<div class="row">
								<div class="col-4">
									<form>
										<div class="form">
											<div class="form-group col-md-12">
												<label for="prev_sectors" class="small text-muted">No. Sectors</label>
												<input type="text" class="form-control form-control-sm text-center bg-light clp" id="prev_sectors" placeholder="Sectors" readonly>
											</div>
											<div class="form-group col-md-12">
												<label for="prev_acl" class="small text-muted">ACL</label>
												<input type="text" class="form-control form-control-sm text-center bg-light clp" id="prev_acl" placeholder="ACL" readonly>
											</div>
											<div class="form-group col-md-12">
												<label for="prev_cid" class="small text-muted">CID (G900 / G1800)</label>
												<div class="small container text-center border clp config rounded bg-light" id="prev_cid" style="height: 116px; padding: 20px">

												</div>
											</div>
											<div class="form-group col-md-12">
												<label for="prev_nid" class="small text-muted">NODE B ID (U2100 / U900)</label>
												<div class="text-center container small clp config border rounded bg-light" id="prev_nid" style="height: 116px"></div>
											</div>
										</div>
									</form>
								</div>
								<div class="col-8 mg-top-10">
									<form>
										<div class="form-group">
											<fieldset class="border rounded p-2">
												<legend  class="w-auto small">2G</legend>
												<div class="form-row">
													<div class="form-group col-md-6">
														<label for="prev_g9" class="small text-muted">G900</label>
														<input type="text" class="form-control form-control-sm clp config text-center bg-light" id="prev_g9" placeholder="G900" readonly>
													</div>
													<div class="form-group col-md-6">
														<label for="prev_u9" class="small text-muted">G1800</label>
														<input type="text" class="form-control form-control-sm clp config text-center bg-light" id="prev_g18" placeholder="G1800" readonly>
													</div>
												</div>
											</fieldset>
											<fieldset class="border rounded p-2">
												<legend  class="w-auto small">3G</legend>
												<div class="form-row">
													<div class="form-group col-md-6">
														<label for="prev_u9" class="small text-muted">U900</label>
														<input type="text" class="form-control form-control-sm clp config text-center bg-light" id="prev_u9" placeholder="U900" readonly>
													</div>
													<div class="form-group col-md-6">
														<label for="prev_u21" class="small text-muted">U2100</label>
														<input type="text" class="form-control form-control-sm clp config text-center bg-light" id="prev_u21" placeholder="U2100" readonly>
													</div>
												</div>
											</fieldset>
											<fieldset class="border rounded p-2">
												<legend  class="w-auto small">4G / LTE</legend>
												<div class="form-row">
													<div class="form-group col-md-6">
														<label for="prev_l7" class="small text-muted">L700</label>
														<input type="text" class="form-control form-control-sm clp config text-center bg-light" id="prev_l7" placeholder="L700" readonly>
													</div>
													<div class="form-group col-md-6">
														<label for="prev_l18" class="small text-muted">L1800</label>
														<input type="text" class="form-control form-control-sm clp config text-center bg-light" id="prev_l18" placeholder="L1800" readonly>
													</div>

													<div class="form-group col-md-6">
														<label for="prev_l23" class="small text-muted">L2300</label>
														<input type="text" class="form-control form-control-sm clp config text-center bg-light" id="prev_l23" placeholder="L2300" readonly>
													</div>

													<div class="form-group col-md-6">
														<label for="prev_l26" class="small text-muted">L2600</label>
														<input type="text" class="form-control form-control-sm clp config text-center bg-light" id="prev_l26" placeholder="L2600" readonly>
													</div>
												</div>
											</fieldset>
										</div>
									</form>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
				<div class="btn-group  mg-top-10 btn-group-sm" role="group">
					<button type="button" class="btn btn-outline-success bulk-upload_clp"><i class="fas fa-upload"></i> UPLOAD</button>
					<button type="button" class="btn btn-outline-secondary export_clp"><i class="fas fa-download"></i> EXPORT</button>
					<button type="button" class="btn btn-sm btn-outline-danger clear_clp"><i class="fas fa-eraser"></i> CLEAR</button>
					<a download href="./Formats/CLP.csv" type="button" class="btn btn-sm btn-outline-primary"><i class="fas fa-file-download"></i> DOWNLOAD FORMAT</a>
				</div>
				<div class="w-100 border mg-top-10 shadow clp-div p-2" style="min-height: 65vh;"></div>
		</div>
	</div>
</div>

<div class="modal fade" data-backdrop="static" id="bulk-upload-clp" tabindex="-1" aria-labelledby="bulk_response-power" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header bg-primary text-white">
				<h5 class="modal-title text-white">UPLOAD FILE</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="text-center">
					<div class="dropzone dropzone_clp pad-00" style="border: 1px dashed #0087F7;">
						<div class="dz-message" style="text-align: center;" >
							<p class="text-center lead h4 text-muted">Upload File <i>(accepts .CSV files only)</i></p>
							<a class="btn btn-primary btn-sm"  href="#" role="button">
								<i class="fa fa-upload" aria-hidden="true"></i> Click to Upload
							</a>
							<p class="text-center lead h5 text-muted mg-top-10">Or drag files here.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
