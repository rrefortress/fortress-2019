		<div class="col-md col-lg col-sm-12 col-xs-12 p-2 my-3">
<!-- 						<div class="col text-left">
							<a href="<?php echo base_url('sitespage'); ?>">
								<button class="btn btn-outline-success btn-sm" type="button">
									<img class="icon_size" src="<?php echo base_url().'style/icons/database-5-64.png'; ?>"> Back to Sites
								</button>
							</a>
						</div> -->
<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="bcfname-tab" data-toggle="tab" href="#" role="tab" aria-controls="bcfnametab" aria-selected="true">BCF Name</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="clpinput-tab" data-toggle="tab" href="#" role="tab" aria-controls="clpinputtab" aria-selected="false">CLP Input</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="rfdetails-tab" data-toggle="tab" href="#" role="tab" aria-controls="rfdetailstab" aria-selected="false">RF Details</a>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="bcfnametab" role="tabpanel" aria-labelledby="bcfname-tab">
			<div id="subdiv" class="row m-0 p-0">
				<div class="container" style="max-width: 1250px;">
					<table id="jtable" class="display nowrap"></table>
				</div>
			</div>
  </div>
  <div class="tab-pane fade" id="clpinputtab" role="tabpanel" aria-labelledby="clpinput-tab">...</div>
  <div class="tab-pane fade" id="rfdetailstab" role="tabpanel" aria-labelledby="rfdetails-tab">...</div>
  
</div>
			<form id="formarea" onsubmit="return false">
				<!-- <div class="p-3 col bg-light rounded form-group" required> -->
<!-- 					<div>
						<div> -->

							<form id="addsiteform" class="col-sm mx-auto">
								<div class="row">
									<div class="col px-3">
										<label class="col-2 p-0">Coordinate(s):</label>
										<div class="form-row">

											<div class="col-md">
												<div class="form-row px-1">
													<textarea type="text" class="col" style="overflow-y: auto; resize: none;" id="coordstextarea" required>SAMPLE ONLY
11.59265	123.33035	BATCH INPUT IS POSSIBLE
10.89963	123.335682
11.120892	124.66849	segplan
10.7187	122.57315	acdes
10.305507	123.913106	assds
		
10.324987	123.952746	assds
		</textarea>
													<button class="col-1.5 btn btn-secondary btn-sm align-self-start" type="button" id="latlongloadbtn">load</button>
													<button  type="button" id="clearbtn" class="col-0.5 btn btn-light btn-xs align-self-start"><i class="fa fa-redo"></i></button>
													
												</div>
												<div id="brgydetails" class="mt-2">
													<table class="table table-xxs responsive table-borderless col">
														<tbody>
															<tr>
																<td colspan="1"><label style="font-size: 13px;" class="">Brgy PSGC:</label></td>
																<td id="coordpsgc" colspan="3"><label style="font-size: 14px;" class=""></label></td>
															</tr>
															<tr>
																<td colspan="1"><label style="font-size: 15px;" class="">Annex address:</label></td>
																<td colspan="3" id="coordaddress"><label style="font-size: 15px;" class=""></label></td>
															</tr>
															<tr>
																<td colspan="1"><label style="font-weight: bold;" class="">Homing:</label></td>
																<td colspan="3"><label style="font-size: 14px;" class=""></label></td>
																
															</tr>
															<tr>
																<td colspan="1"><label style="font-size: 14px;" class="">BSC:</label></td>
																<td id="coordhomebsc" colspan="3"><label style="font-size: 14px;" class=""></label></td>
																
															</tr><tr>
																<td colspan="1"><label style="font-size: 14px;" class="">RNC:</label></td>
																<td id="coordhomernc" colspan="3"><label style="font-size: 14px;" class=""></label></td>
																
															</tr><tr>
																<td colspan="1"><label style="font-size: 14px;" class="">LTE:</label></td>
																<td id="coordhomelte" colspan="3"><label style="font-size: 14px;" class=""></label></td>
																
															</tr><tr>
																<td colspan="1"><label style="font-size: 14px;" class="">PSC:</label></td>
																<td id="coordhomelte" colspan="3"><label style="font-size: 14px;" class=""></label></td>
																
															</tr>
														</tbody>
													</table>
												</div>
											</div>
											<div class="col-md p-0">
												<div class="col">
													<div class="row">
														<label style="font-size: 14px;" class="col pr-0" >Total Coordinates: </label>
														<label style="font-size: 14px;" class="col pr-0" id="totalCoordinates"></label>
														<div id="loadspinner" style="display:block;" class="spinner-border text-primary spinner-border-sm" role="status">
															<span class="sr-only">Loading...</span>
														</div>
													</div>
													<div class="row">
														<div class="col-8 pr-0">
															<select class="form-control form-control-xs" id="coordinatesOption" oninvalid="this.setCustomValidity('Please Provide Tower Type')" oninput="setCustomValidity('')" required>
															</select>
														</div>
														<button class="col-1.5 btn btn-secondary form-control-xs mx-1" type="button" id="loadcoordsbtn">load</button>
														<button  type="button" id="downcoord" class="col-0.5 btn btn-secondary form-control-xs "><i class="fa fa-caret-down"></i></button>
														<button  type="button" id="upcoord" class="col-0.5 btn btn-secondary form-control-xs "><i class="fa fa-caret-up"></i></button>
													</div>
												</div>

												<div class="col px-0 mt-2" style="height:237.25px;">
													<!-- <div class="col-md mb-2">
														<label for="plaid" class="m-0 mr-1" >PLA ID :</label>
														<input type="text" style="text-transform: uppercase;" class="form-control form-control-xs col" placeholder="Enter PLA ID" aria-label="PLA ID" id="addplaid_sitename" required>
													</div> -->
<!-- 													<div class="col-md mt-3 mb-2">
														<div class="form-row p-1">
															<label for="plaid" class="mr-1" style="font-weight: bold;">PLAID :</label>
															<input type="text" style="text-transform: uppercase;" class="form-control form-control-xs col" id="addplaid_sitename" required>
														</div>
													</div> -->
													<div class="col-md mt-3  mb-2">
														<label class="my-0">Building</label>
														<input id="addbldg_sitename" type="text" class="form-control form-control-xs" id="">
													</div>
													<div class="col-md mb-2">
														<label class="my-0">Street</label>
														<input type="text" class="form-control form-control-xs" id="">
													</div>


												</div>
											</div>
										</div>
										
<!-- 										<div class="form-row">
											<div class="col-md mb-2">
												<label class="my-0">Province</label>
												<input class="annexaddress form-control form-control-xs" id="addprovince_sitename" placeholder="Enter Province" list="addprovince_datalists" required>
												<datalist id="addprovince_datalists">
													
												</datalist>
											</div>
											<div class="col-md mb-2">
												<label class="my-0">Town</label>
												<input class="annexaddress form-control form-control-xs" id="addtown_sitename" placeholder="Enter Town"  list="addtown_datalists" required>
												<datalist id="addtown_datalists">
													
												</datalist>
											</div>
											<div class="col-md mb-2">
												<label class="my-0">Barangay</label>
												<input class="annexaddress form-control form-control-xs" id="addbarangay_sitename" placeholder="Enter Barangay"  list="addbarangay_datalists" required>
												<datalist id="addbarangay_datalists">
													
												</datalist>
											</div>
												
										</div> -->
<!-- 										<div class="form-row">
											<div class="col-md mb-2">
												<label class="my-0">Building</label>
												<input id="addbldg_sitename" type="text" class="form-control form-control-xs" id="">
											</div>
											<div class="col-md mb-2">
												<label class="my-0">Street</label>
												<input type="text" class="form-control form-control-xs" id="">
											</div>
												
										</div> -->
<!-- 										<div class="form-row mt-1">
											<label class="col pr-0"><b>Affix :</b></label>
										</div> -->
										<div class="form-row mt-0">
											<div class="col-3 mb-2">
												<label class="my-0">BCF Name Prefix</label>
												<input id="bcfprefix"type="text" class="form-control form-control-xs font-weight-bold" id="">
											</div>
											<div class="col-2 mb-2">
												<label class="my-0">Town Code</label>
												<b id="towncode" class="col">CEBU</b>
											</div>
											<div class="col-3 mb-2">
												<label class="my-0">Province Code</label>
												<b id="provincecode" class="col">CEB</b>
											</div>
											<div class="col-4 mb-2">
												<div class="form-row">
													<label class="my-0">Site Type</label>
													<div class="col pl-1">
														<select class="form-control form-control-xs" id="" placeholder="Enter Tower Type" oninvalid="this.setCustomValidity('Please Provide Tower Type')" oninput="setCustomValidity('')" required>
															<option value="" selected>Macro (Outdoor)</option>
															<option value="ID">ID Indoor</option>
															<option value="IO">Outdoor</option>
															<option disabled>--Small Cells--</option>
							                                <option value="FW">Flexi Zone Mini Macro (NOKIA)</option>
							                                <option value="AS">Air Scale Micro RRH (NOKIA)</option>
							                                <option value="AC">Atom Cell (HUAWEI)</option>
							                                <option value="LS">Lamp Site (HUAWEI)</option>
							                                <option value="EM">Easy Macro (HUAWEI)</option>
							                                <option value="PC">Pico Cell (HUAWEI)</option>
							                                <option value="PNP">Plug N' Play (HUAWEI)</option>
							                                <option value="BBR">Book RRU (HUAWEI)</option>
														</select>
													</div>
												</div>
												<b id="suffix" class="col" >IO</b>
												<!-- <b id="suffix" class="col" >IO</b> -->
											</div>
										</div>

<!-- 										<div class="form-row input-group mx-0 m-3">
											<div class="col-9 d-flex justify-content-center align-items-center m-0 p-0  borde ">
												<div class="input-group-prepend">
													<span class="input-group-text form-control form-control-sm" ><b>Site Name</b></span>
												</div>

												<input type="text" class="form-control form-control-sm col-9" id="addsuggested_sitename" style="text-transform: uppercase; font-weight: bold;"placeholder="( e.g. ''LAHUGPCEBUCEBID'' )"  maxlength="22" required>
												<h5 class="col-3 p-0 m-0 mr-1">BCF Name:</h5>
												<h4  class="col p-0 m-0"><b id="addsuggested_sitename">TABUCSCALESILO</b></h4>
											</div>
										</div> -->
									
<!-- 									<div class="">
										<div class="">
											<div class="form-group row mt-3 mb-2">
												<label class="col-2 pr-0">Coordinates :</label>
												<div class="col-4 pl-0">
													<input value=""type="text" class="form-control form-control-xs" id="addlatlong_sitename" placeholder="comma '','' separated Lat & Long"  required>
												</div>
												<label  id="latlonglabel"class="form-check-label" for="addlatlong_sitename">
												</label>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-3">Site Type:</label>
											<div class="col-2 px-0">
												<div class="p-0">
													<input type="radio" class="form-check-input sitetype" id="indoor" name="sitetyperadio">
												  	<label class="form-check-label label-warning" for="indoor">Indoor</label>
												</div>
												<div class="p-0">
												  <input type="radio" class="form-check-input sitetype" id="outdoor" name="sitetyperadio" checked>
												  <label class="form-check-label" for="outdoor">Outdoor</label>
												</div>
												
												<div class="p-0">
													<input type="radio" class="form-check-input sitetype" id="indooroutdoor" name="sitetyperadio">
												  	<label class="form-check-label" for="indooroutdoor">Indoor/Oudoor</label>
												</div>
												<div class="p-0">
													<input type="radio" class="form-check-input sitetype" id="smallcell" name="sitetyperadio">
												  	<label class="form-check-label" for="smallcell">Small Cell</label>
												</div>
											</div>
											<label for="towertype" class="col-3 pr-0">Tower Type:</label>
											<div class="col-4 pl-1">
												<select class="form-control form-control-xs" id="towertype" placeholder="Enter Tower Type" oninvalid="this.setCustomValidity('Please Provide Tower Type')" oninput="setCustomValidity('')" required>
												</select>
											</div>
										</div>

										<div class="row mt-1 mb-2">
											<label for="permanent" class="col-3 pr-0">Solution Type:</label>
											<div class="col-2 px-0">
												<div class="p-0">
												  <input type="radio" class="form-check-input solutiontype" id="permanent" name="solutiontyperadio" checked>
												  <label class="form-check-label label-warning" for="permanent">Permanent</label>
												</div>
												<div class="p-0">
												  <input type="radio" class="form-check-input solutiontype" id="temporary" name="solutiontyperadio">
												  <label class="form-check-label" for="temporary">Temporary</label>
												</div>
											</div>
											<label for="towerheight" class="col-3 pr-0">Tower Height:</label>
											<div class="col-4 pl-0">
												<input class="rowperpage-input form-control form-control-xs" min="0" max="100" type="number" id="towerheight" placeholder="(meters)" oninput="this.value = this.value.replace(/[^0-9. ]/g, '');" required>
											</div>
										</div>
									</div>
 -->
								</div>
								<div class="col px-3">
									<!-- <div id="" style="height:300px;"> -->
										<!-- <fieldset> -->
											<div id="addsiteMap" class="my-1 col
											"style= "height: 300px !important; "></div>
										<!-- </fieldset> -->
									<!-- </div> -->
									<!-- <label class="col-3">Configuration:</label> -->
									<!-- <div class="form-row">

												<label style="font-size: 14px; font-size: 14px;" class="col-2 pt-1 pl-4">No. of Sectors:</label>
												<input class="rowperpage-input form-control form-control-xs col-1" value="3" min="1" max="12" type="number" id="sectoradjust">

									</div> -->
<!-- 									<div class="form-row">
										<div class="col">
											<table class="table table-xs table-hover thead-dark table-striped table-borderless col">
												<thead>
													<td colspan="10">
														<div class="form-row">
														<label style="font-size: 14px;" class="col-6 pt-1 px-0 pl-3">No. of Sectors:</label>
														<input class="rowperpage-input form-control form-control-xs col-2" value=0 min="0" max="6" type="number" id="sectoradjust">
														</div>
													</td>

												</thead>
												<thead>
													<td colspan="3" scope="row"></td>
													<td colspan="10">
														<div class="d-flex justify-content-end">
															<label style="font-size: 13px;" class="text-center col-10 p-0 m-0 pt-2">CLP Configuration</label>
															<label style="font-size: 10px;" class="col-2 p-0 m-0">No. of Carrier/s:</label>
														</div>
													</td>

												</thead>
												<tbody>
													<tr>
														<td colspan="3" scope="row">G900: </td>
														<td colspan="10">
															<div class="d-flex justify-content-end">
																<input type="text" class="col-10 form-control form-control-xs tech" id="g900_config" placeholder="Enter Configuration" oninput="this.value = this.value.replace(/[^0-9+()/ ]/g, '').replace(/(\..*)\./g, '$1').replace(/\s+/g,'+');" >
																<input class="rowperpage-input form-control form-control-xs col-2  carrier" value=0 min="0" max="1" type="number" id="g900_configx">
															</div>
														</td>
													</tr>
													<tr>
														<td colspan="3" scope="row">G1800: </td>
														<td colspan="10">
														<div class="d-flex justify-content-end">
															<input type="text" value="0+0+0" class=" col-10 form-control form-control-xs tech" id="g1800_config" placeholder="Enter Configuration" oninput="this.value = this.value.replace(/[^0-9+ ]/g, '').replace(/(\..*)\./g, '$1').replace(/\s+/g,'+');">
															<input class="rowperpage-input form-control form-control-xs col-2 carrier" value=0 min="0" max="1" type="number" id="g1800_configx">
														</div>
														</td>
													</tr>
													<tr>
														<td colspan="3" scope="row">U900: </td>
														<td colspan="10">
														<div class="d-flex justify-content-end">
															<input type="text" value="0+0+0" class="col-10 form-control form-control-xs tech" id="u900_config" placeholder="Enter Configuration" oninput="this.value = this.value.replace(/[^0-9+ ]/g, '').replace(/(\..*)\./g, '$1').replace(/\s+/g,'+');">
															<input class="rowperpage-input form-control form-control-xs col-2 carrier" value=0 min="0" max="2" type="number" id="u900_configx">
														</div>
														</td>
													</tr>
													<tr>
														<td colspan="3" scope="row">U2100: </td>
														<td colspan="10">
														<div class="d-flex justify-content-end">
															<input type="text" value="0+0+0" class="col-10 mx-0 form-control form-control-xs tech" id="u2100_config" placeholder="Enter Configuration" oninput="this.value = this.value.replace(/[^0-9+ ]/g, '').replace(/(\..*)\./g, '$1').replace(/\s+/g,'+');">
															<input class="rowperpage-input form-control form-control-xs col-2 carrier" value=0 min="0" max="3" type="number" id="u2100_configx">
														</div>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="col">
											<table class="table table-xs table-hover thead-dark table-striped table-borderless col">
												<thead>
													<tr class="mx-0"><td >&nbsp;</td></tr>
													<tr><td>&nbsp;</td></tr>

												</thead>
												<tbody>
													<tr>
														<td colspan="3" scope="row">L700: </td>
														<td colspan="9"><input type="text" class="form-control form-control-xs" id="l700_config tech" placeholder="Enter Configuration" oninput="this.value = this.value.replace(/[^0-9+ ]/g, '').replace(/(\..*)\./g, '$1').replace(/\s+/g,'+');" required disabled></td>
													</tr>
													<tr>
														<td colspan="3" scope="row">L1800: </td>
														<td colspan="9"><input type="text" class="form-control form-control-xs tech" id="l1800_config" placeholder="Enter Configuration" oninput="this.value = this.value.replace(/[^0-9+ ]/g, '').replace(/(\..*)\./g, '$1').replace(/\s+/g,'+');" disabled required></td>
													</td>
												</tr>
												<tr>
													<td colspan="3" scope="row">L2300: </td>
													<td colspan="9"><input type="text" class="form-control form-control-xs tech" id="l2300_config" placeholder="Enter Configuration" oninput="this.value = this.value.replace(/[^0-9+ ]/g, '').replace(/(\..*)\./g, '$1').replace(/\s+/g,'+');" disabled required></td>
												</td>
											</tr>
											<tr>
												<td colspan="3" scope="row">L2600: </td>
												<td colspan="9"><input type="text" class="form-control form-control-xs tech" id="l2600_config" placeholder="Enter Configuration" oninput="this.value = this.value.replace(/[^0-9+ ]/g, '').replace(/(\..*)\./g, '$1').replace(/\s+/g,'+');" disabled required></td>
											</tr>
											<tr>
												<td colspan="3" scope="row" style="font-size:13px;">L2600 MM: </td>
												<td colspan="9"><input type="text" class="form-control form-control-xs tech" id="l2600mm_config" placeholder="Enter Configuration" oninput="this.value = this.value.replace(/[^0-9+ ]/g, '').replace(/(\..*)\./g, '$1').replace(/\s+/g,'+');" disabled required></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div> -->
<!-- 							<table class="table table-hover table-borderless table-sm col-12">
								<thead>
								</thead>
								<tbody>

									<tr>
										<td colspan="2" scope="row">Azimuth: </td>
										<td colspan="3">
										<div class="d-flex justify-content-end">
											<input type="text" value="0" step="5" class="form-control form-control-xs" id="azimuth" placeholder="e.g. ``360/270/110``  " oninput="this.value = this.value.replace(/[^0-9/ ]/g, '').replace(/(\..*)\./g, '$1').replace(/\s+/g,'/');" required>
										</div>
										</td>
										<td colspan="1">
											<button  type="button" id="checkazimuth" class="btn btn-secondary form-control-xs ">Check</button>

										</td>
										<td colspan="6">
											<label  id="azimuthcount"class="form-check-label" for="checkazimuth">
											</label>

										</td>
									</tr>
									<tr>
										<td colspan="3" scope="row">PSC Radius (km): </td>
										<td colspan="4">
											<input type="range" class="form-control-range form-control-xs" id="radius" min="1" max="10" step="1" value="1">
										</td>
										<td colspan="1">
											<label  id="radiuslabel"class="form-check-label" for="radius"></label>
										</td>
										<td colspan="3">
										</td>
									</tr>
									<tr>
										<td colspan="3" scope="row">BTS ID : </td>
										<td colspan="5">
											<label id="btsidlabel" class="my-0" for="generatebtsid"></label>
										</td>
										<td colspan="1">
											<button  type="button" id="generatebtsid" class="btn btn-secondary form-control-xs">Generate</button>
										</td>
									</tr>
									<tr>
										<td colspan="3" scope="row">NodeB ID : </td>
										<td colspan="5">
											<label id="nodebidlabel" class="my-0" for="generatenodebid"></label>
										</td>
										<td colspan="1">
											<button type="button" id="generatenodebid" class="btn btn-secondary btn-sm form-control-xs">Generate</button>
										</td>
									</tr>
									<tr>
										<td colspan="3" scope="row">Scrambling Code : </td>
										<td colspan="5">
											<label id="psclabel" class="my-0" for="generatepsc"></label>
										</td>
										<td colspan="1">
											<button  type="button" id="generatepsc" class="btn btn-secondary form-control-xs">Generate</button>
										</td>
									</tr>

								</tbody>
							</table> -->

						</div>
					</div>

					<!-- <div class="col-md-6 mx-auto"></div> -->
					<div class="">
								<div class="table-responsive">
									<table class="table table-xxxs table-bordered">
									<thead class="thead-light" align="center">
										<tr>
											<th>AREA</th>
											<th>Search Ring/ Project Name</th>
											<th>BCF Name</th>
											<th>CID (G9)</th>
											<th>Node B Name</th>
											<th>Node B ID(U21/U9)</th>
											<th>SC</th>
											<th colspan="5">Remarks</th>
										</tr>
									</thead>
									<tbody id="temptable">
										<tr >
											<td rowspan="10" style="vertical-align : middle;">VIS
											</td>
											<td rowspan=10 style="vertical-align : middle;">ASDF_SA</td>
											<td id="bcfname" rowspan=10 style="vertical-align : middle;">BCFCEBUCEB</td>
											<td rowspan=10 style="vertical-align : middle;">G9:5555</td>
											<td rowspan=10 style="vertical-align : middle;">BCFCEBUCEBZ</td>
											<td rowspan=10 style="vertical-align : middle;">U21 F1/ U9F1: 5556</td>
											<td rowspan=10 style="vertical-align : middle;">101/102/103</td>
											
												<tr>
													<th>Address</th>
													<td colspan=4>Cebu,  Cebu, Cebu</td>
												</tr>
												<tr>
													<th>Coordinates</th>
													<td>10.222</td>
													<td>121.223</td>
													<th>PLAID</th>
													<td>VIS0000</td>
												</tr>
												<tr>
													<th>Azimuth</th>
													<td colspan=2>0 / 70 / 180</td>
													<th>ACL</th>
													<td>30</td>
												</tr>
												<tr>
													<th>G900</th>
													<td colspan=2>0 / 70 / 180</td>
													<th>U900</th>
													<td>2+2+2</td>
												</tr>
												<tr>
													<th>U2100</th>
													<td colspan=2>0 / 70 / 180</td>
													<th>L700</th>
													<td>10.222</td>
												</tr>
												<tr>
													<th>L1800</th>
													<td colspan=2>0 / 70 / 180</td>
													<th>L2300</th>
													<td>10.222</td>
												</tr>
												<tr>
													<th>L2600</th>
													<td colspan=2>0 / 70 / 180</td>
													<th>Antenna Model</th>
													<td>10.222</td>
												</tr>
												<tr>
													<th>BSC Homing</th>
													<td colspan=2>0 / 70 / 180</td>
													<th>RNC Homing</th>
													<td>10.222</td>
												</tr>
												<tr>
													<th>LTE Homing</th>
													<td colspan=2>0 / 70 / 180</td>
													<th>Integration Date</th>
													<td>10.222</td>
												</tr>
											</td>
										</tr>
									</tbody>
								</table>
								</div>
								
							</div>
					<div class="col-md-6 input-group mx-auto my-3">
								<!-- <div class="input-group-prepend">
									<span class="input-group-text"><b>Site Name</b></span>
								</div>
								<input type="text" class="form-control" placeholder="Enter Site Name" aria-label="Site Name" id="addsuggested_sitename" required>
							</div>
							<div class="row"> -->
								<div class="mx-auto">
									<button type="submit" id="create_site" class="btn btn-success btn">View all</button>
									<!-- <button  class="btn btn-success" id="create_site">Create Site</button> -->
									<!-- data-toggle=modal data-target=.main_modal bd-example-modal-lg -->
								</div>
							</div>

							<div class="modal fade main_modal bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-xl">
									<div class="modal-content">
										<div class="modal-header px-4 py-2">
											<h5 class="modal-title" id="exampleModalLabel">Site Details</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body py-0">
											<table id="addsitemodal1" class="table table-striped table-xxs mb-2">
												<thead>
												</thead>
												<tbody>

												</tbody>
											</table>
											<table id="addsitemodal2" class="table table-striped table-xxs">
												<thead>
												</thead>
												<tbody>

												</tbody>
											</table>
											<table id="addsitemodal3" class="table table-striped table-xxs">
												<thead>
												</thead>
												<tbody>

												</tbody>
											</table>
										</div>
										<div id="modal-footer" class="modal-footer">
											<!-- 											<h6 class="m-0">Would you like to add Cell ID? </h6> -->
											<button type="submit" class="btn btn-primary btn-sm" id="createsitesubmit">Submit</button>
											<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
										</div>
									</div>
								</div>
							</div>


							<!-- </form> -->
<!-- 						</div>
					</div> -->
				<!-- </div> -->
			</form>
		<!-- </div> -->
	</div>
</body>
<?php $this->load->view('/layout/footer'); ?>
<script src="action/addsite_actions.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJiNdQZEwhcv8Hep7SF8qDo2SJDM4Ttdw&callback=initMap"
	async defer></script>
</html>
