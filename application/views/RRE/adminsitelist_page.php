<?php $this->load->view('/layout/header'); ?>
<?php $this->load->view('/layout/side_menu'); ?>
	<div class="hm-520">
				<div>
					<table class="table" id="paginationtable">
						<thead>
							<tr>
								<th colspan="5">Admin Sitelist</th>
							</tr>
							<tr>
								<th scope="col">CellName</th>
								<th scope="col">SiteName</th>
								<th scope="col">Audited</th>
								<th scope="col">Status</th>
							</tr>
	<!--  						<tr>
								<td scope="col">08/01/19</td>
								<td scope="col">ZA0021</td>
								<td scope="col">aaa</td>
								<td scope="col">Standard</td>
								<td scope="col">Add</td>
								<td scope="col">Add Site: BRGYTALAM</td>
								<td scope="col">
									<span><img class="icon_size" src="<?php echo base_url()."style/icons/approve-64.png"; ?>"></span>
									<span><img class="icon_size" src="<?php echo base_url()."style/icons/disapprove-64.png"; ?>"></span>
								</td>
							</tr> -->
						</thead>
						<tbody>
							<!-- site lists goes here -->
						</tbody>
					</table>
					<!-- Paginate -->
					<div style='margin-top: 20px;' id='pagination'> </div>
				</div>
			<!-- </div> -->
		</div>
<?php $this->load->view('/layout/footer'); ?>
<script src="action/admin_actions.js"></script>
