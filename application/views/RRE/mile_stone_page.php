<?php $this->load->view('/layout/header'); ?>
<body style="overflow:hidden">
		<?php $this->load->view('/layout/side_menu'); ?>
		<div class="mg-top-77 shadow-lg border border-secondary">
		<div class="container-fluid">

			<div class="row">
				<div class="col-3 border-secondary border-right pad-00 MS-520">
					<div class="row">
						<div class="col-12 sticky-top mg-top-10 bg-white">
							<div class="row">
								<div class="container">
									<div class="col-12">
										<div class="input-group mb-2 mr-sm-2">
											<div class="input-group-prepend">
												<div class="input-group-text"><i class="fas fa-search"></i></div>
											</div>
											<input type="text" class="form-control" id="search_items" placeholder="Search Item Name">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-12 hm-406">

							<table class="table table-striped table-sm table-hover ms_lists">
								<thead>
									<tr>
										<th scope="col" style="width:15%" class="text-center">#</th>
										<th scope="col">Name</th>
										<!-- <th class="text-center" scope="col">Track</th> -->
									</tr>
								</thead>
								<tbody class="ms_table"></tbody>
							</table>
						</div>

						<div class="col-12">
							<center>
								<div class="text-center" id='ms_pagination'> </div>
							</center>
						</div>

					</div>

				</div>

				<div class="col-9 ms_hm">
					<div class="row">
						<div class="col-12 sticky-top bg-white shadow-sm border-bottom border-secondary">
							<div class="row">
								<div class="col-12 fullwidth mg-top-10">
									<div class="separator">
										<h4 class="site_title text-uppercase">SELECT TITLE</h4>
										<ul class="progress-tracker progress-tracker--text progress-tracker--center" id="progress_tracks"></ul>
									</div>
								</div>
							</div>
						</div>

						<div class="col-12 ms-h-366">

							<div class="accordion mg-top-10 check_lists_accordion" id="check_lists_accordion">

							</div>

						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

</body>

<div class="modal fade" id="import_view_modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-center text-uppercase" id="exampleModalLabel">Import</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form method="post" id="file-upload" enctype="multipart/form-data">
						<div class="files">
							<p class="text-center lead h4">Upload File <i>(accepts .csv, .xlsx & .xls files only)</i></p>
							<input type="file" name="userfile" id="filename" class="form-control btn" accept=".csv, .xlsx, .xls" required>
						</div>

						<div class="container">
							<div class='progress bg-secondary' id="progressDivId">
								<div class="progress-bar" id="progressBar"></div>
								<div class="percent text-white" id="percent">0%</div>
							</div>
						</div>
				</div>
					<div class="modal-footer">
						<div class="col-12 text-center">
							<button class="btn btn-outline-danger text-center" type="button" id="clear_data_file"><i class="fas fa-eraser"></i> Remove</button>
							<button class="btn btn-outline-primary text-center" id="submitButton" type="submit" id="inputGroupFileAddon04"><i class="fas fa-file-upload"></i> Upload</button>
						</div>
					</div>
				</form>
		</div>
	</div>
</div>

<?php $this->load->view('/layout/footer'); ?>
<script src="<?= base_url('action/mile_stone.js'); ?>"></script>
