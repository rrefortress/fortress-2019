<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="row">
					<div class="col-9">
						<div class="row">
							<div class="col-12">
								<div class="card-deck" id="cards-dash"></div>
							</div>
							<div class="col-12 mg-top-10 ">
								<div class="container border rounded shadow">
										<div class="col-12 mg-top-10">
											<div class="row">
												<div class="col-6">
													<div class="input-group input-group-sm">
														<input type="text" class="form-control form-control-sm" placeholder="Search" id="search" autocomplete="off">
														<div class="input-group-append">
														<span class="input-group-text">
															<a href="#" class="text-muted"><i class="fas fa-search"></i></a>
														</span>
														</div>
													</div>
												</div>
												<div class="col-3"></div>
												<div class="col-3">
													<div class="input-group input-group-sm">
														<input type="text" class="form-control form-control-sm" placeholder="Select Date" id="date_select" autocomplete="off">
														<div class="input-group-append">
														<span class="input-group-text">
															<a href="#" class="text-muted" id="select_date"><i class="fas fa-calendar-alt"></i></a>
														</span>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-12 mg-top-10" style="height: 43vh; overflow-y: auto" >
											<ul class="list-group logs-lists list-group-flush"></ul>
										</div>
										<div class="col-12">
											<center>
												<div class="text-center" id='logs-pagination'> </div>
											</center>
										</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-3">
						<?php require_once 'Activies.php'; ?>
					</div>
				</div>

			</div>

		</div>
	</div>
