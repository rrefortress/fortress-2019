<div class="pad-00 card card-stats mb-4 mb-xl-0 border-secondary border rounded shadow-lg mg-top-10-">
  <div class="row">
	<div class="col-12">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-8 col-lg-8 col-md-8">
					<div class="row">
						<div class="col-8">
							<div class="custom-control custom-radio custom-control-inline mg-top-10 mg-left-10">
								<input type="radio" id="radio_survey1" name="radio_survey" class="radio_survey custom-control-input" checked value="all">
								<label class="custom-control-label" for="radio_survey1">All</label>
							</div>

							<div class="custom-control custom-radio custom-control-inline">
								<input type="radio" id="radio_survey2" name="radio_survey" class="radio_survey custom-control-input" value="surveys">
								<label class="custom-control-label text-muted" for="radio_survey2">For Surveys</label>
							</div>
						</div>
						<div class="col-4">
							<div class="form-check form-check-inline">
								<select class="custom-select input-sm" id="dash_names"></select>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-lg-4 col-md-4 text-right mg-top-5">
					<div class="btn-group">
						<button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-download"></i> Export
						</button>
						<div class="dropdown-menu dropdown-menu-right">
							<h5 class="dropdown-header">Select to proceed</h5>
							<a class="dropdown-item " href="#" id="download_as_excel"><i class="fas fa-file-excel"></i> as Excel</a>
<!--							<div class="dropdown-divider"></div>-->
<!--							<a class="dropdown-item " href="#" id="download_as_kml"><i class="fas fa-map-marked-alt"></i> as KML</a>-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	  <div class="col-12">
        <div id="main_map"></div>
    </div>
  </div>
</div>
