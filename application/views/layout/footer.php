<script> let url = '<?= base_url();?>'; </script>
<?php foreach ($this->config->item('scripts') as $row){ ?>
	<script src="assets/style/js/<?=$row;?>.js"></script>
<?php } ?>

<?php foreach ($this->config->item('actions') as $row) { ?>
	<script src="assets/action/<?=$row;?>.js"></script>
<?php } ?>

<?php foreach ($js as $row) { ?>
	<script src="assets/action/<?=$row;?>.js"></script>
<?php } ?>

<script src="https://apis.google.com/js/client:platform.js?onload=renderButton" async defer></script>
<script src="https://cdn.tiny.cloud/1/ftor9zv3ex4iplrl088jlvueuyn74ywz1vhs089k91l1quy3/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>