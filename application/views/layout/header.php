<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="google-signin-scope" content="profile email">
	<meta name="google-signin-client_id" content="964406776551-u3mepo7cqf7r8rp46c7dk1gk409m29s6.apps.googleusercontent.com">
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<link rel="icon" href="<?= base_url('assets/style/favicon.png');?>" type="image/gif">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
	<?php foreach ($this->config->item('css') as $row) { ?>
		<link rel="stylesheet" type="text/css" href="assets/style/css/<?=$row;?>.css">
	<?php } ?>

	<title>Fortress - <?= $title; ?></title>
</head>
