<a href="javascript:" id="return-to-top"><i class="fas fa-arrow-up"></i></a>
<div class="se-pre-con"></div>
<div class="wrapper d-flex align-items-stretch">
	<nav id="sidebar" class="active">
		<div class="custom-menu">
			<button type="button" id="sidebarCollapse" class="btn btn-primary">
				<i class="fa fa-bars"></i>
				<span class="sr-only">Toggle Menu</span>
			</button>
		</div>
		<div class="p-3 d-flex justify-content-center">
			<img src="<?= base_url('assets/style/FortressBig.png') ?>" class="rounded-circle logo img-fluid img-thumbnail shadow-lg" alt="Fortress">
		</div>
		<ul class="list-unstyled components mb-2 nav-menu">
					<?php if (empty($this->session->userdata('vendor'))) { ?>
						<?php foreach ($menus as $row) { ?>
							<?php if (empty($row['sub_menu'])) { ?>
							<li class="nav-item <?= $this->uri->segment(1) == $row['base_url'] ? "active" : ""; ?>">
								<a class="nav-link " href="<?= base_url($row['base_url']); ?>"><span
										class="<?= $row['icon']; ?> mr-11"></span>&nbsp;<?= $row['name'];?></a>
							</li>
							<?php } else { ?>
								<li class="nav-item dropdown">
									<a href="#<?=$row['name'];?>" id="dbs" data-toggle="collapse" aria-expanded="false"
									class="nav-link dropdown-toggle <?= in_array($this->uri->segment(1), $row['sub_menu_name']) ? 'active' : '' ?>">
										<span class="<?= $row['icon']; ?> mr-11"></span>&nbsp;<?= $row['name']; ?>
									</a>
									<ul class="collapse list-unstyled" id="<?=$row['name'];?>">
										<?php foreach ($row['sub_menu'] as $rows) { ?>
											<li>
												<a class="dropdown-item <?= $this->uri->segment(1) == $rows['base_url'] ? "active" : "" ?>" href="<?= base_url($rows['base_url']); ?>">
													<span class="<?= $rows['icon']; ?>"></span>&nbsp;<?=$rows['name'];?></a>
											</li>
										<?php } ?>
									</ul>
								</li>
							<?php } ?>
						<?php } ?>	
					<?php } else { ?> 
						<li class="nav-item">
							<a class="nav-link <?= $this->uri->segment(1) == "acceptance" ? "active" : ""; ?>"
							href="<?= base_url("acceptance"); ?>"><span class="fas fa-file-contract mr-11"></span>&nbsp;Acceptance</a>
						</li>			
					<?php } ?> 

					<li class="nav-item">
						<a class="nav-link <?= $this->uri->segment(1) == "about" ? "active" : ""; ?>"
						   href="<?= base_url("about"); ?>"><span class="fas fa-exclamation-circle mr-11"></span>&nbsp;About</a>
					</li>

					<li class="nav-item">
						<a class="nav-link <?= $this->uri->segment(1) == "sign_out" ? "active" : ""; ?>"
						   href="#" id="signout"><span class="fas fa-sign-out-alt mr-11"></span>&nbsp;Logout</a>
					</li>
				</ul>
		<div class="footer">
			<p class="small text-center">
				<span><?= $this->config->item('site_name'); ?></span>
				<span><?= $this->config->item('site_version'); ?></span></br>
				<span><?= $this->config->item('site_author'); ?></span>
				<span><?= $this->config->item('site_department'); ?></span>
				<span><?= $this->config->item('site_reserved'); ?></span>
			</p>
		</div>
	</nav>

	<div id="content" class="p-3 z-index-1">
		<nav class="navbar navbar-expand-lg navbar-light shadow-none bg-none mb-0 sticky-top">
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="nav navbar-nav ml-auto shadow-lg border border-secondary p-1 bg-white rounded-pill">
					<li class="mg-top-10"><small>Welcome, </small></li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
						   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<img
								src="<?= empty($this->session->userdata('profile')) ? base_url('assets/pictures/default.jpg') : $this->session->userdata('profile'); ?>"
								width="30" height="30" class="rounded-circle">
							<?php $account =  empty($this->session->userdata('vendor')) ? strtoupper($this->session->userdata('access_level')) == 'GUEST' ? 'GUEST' :  $this->session->userdata('geoarea') : $this->session->userdata('vendor'); ?>
							<small><b><?= ucfirst($this->session->userdata('fname')); ?></b> <span class="badge badge-primary rounded-circle p-2"><em><?= strtoupper($account); ?></em></span></small>
						</a>
						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
							<?php if ($this->session->userdata('access_level') == 'Admin') { ?>
								<a class="dropdown-item" href="<?= base_url('settings'); ?>"><i
									class="fas fa-user-cog"></i> Settings</a>

							<?php } else {  ?>
								<a class="dropdown-item" href="<?= base_url('accounts'); ?>"><i
										class="fas fa-user-cog"></i> Account Settings</a>
							<?php } ?>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="#" id="signout"><i
									class="fas fa-sign-out-alt"></i> Log out</a>
						</div>
					</li>
				</ul>
			</div>
		</nav>

		<div class="modal fade view_sched pad-00 oy-hidden" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" data-backdrop="static" aria-hidden="true">
			<div class="modal-dialog modal-full mg-top-0">
				<div class="modal-content">
					<div class="modal-header text-uppercase text-muted rounded-0 border-0">
						<h5 class="modal-title text-center" id="exampleModalLabel"><i class="fas fa-clipboard-list"></i> Schedule</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">

						<div class="row">
							<div class="col-12">
								<div class='my-legend'>
									<div class='legend-scale'>
										<ul class='legend-labels'>
											<li><span style='background:#f6da63;'></span>For Survey</li>
											<li><span style='background:#46b3e6;'></span>Surveyed</li>
											<li><span style='background:#ed8240;'></span>Re-Survey</li>
											<li><span style='background:#ea5e5e;'></span>Master Plan</li>
											<li><span style='background:#d89cf6;'></span>EWO / OWO</li>
											<li><span style='background:#dedef0;'></span>#N/A</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col loaders d-none mg-top-100">
								<h5 class="text-center text-white">
									<i class="fas fa-spinner fa-spin display-3 text-dark"></i><br>
									<h4 class="text-dark text-center">Loading</h4>
								</h5>
							</div>
							<div class="col-12">
								<div class='calendar'></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
