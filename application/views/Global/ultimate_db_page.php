<div class="container-fluid mg-top-77">
	<div class="col-12">
		<div class="container">
			<div class="row">
				<div class="col-3">

				</div>
				<div class="col-6">
					<div class="form-group has-search">
						<span class="fa fa-search form-control-feedback"></span>
						<input type="search" class="form-control form-control-lg rounded-pill" id="search" placeholder="Search..." onkeypress="return AvoidSpace(event)" autocomplete="off" autofocus>
					</div>
						<div class="list-group" id="results"></div>
				</div>
				<div class="col-3"></div>
			</div>

		</div>
	</div>
	<div class="card-group" id="u_cards"></div
</div>

<div class="modal fade oy-hidden pad-00" id="view_all_details" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
	<div class="modal-dialog modal-full modal-dialog-scrollable mg-top-0">
		<div class="modal-content">
			<div class="modal-header udb-header text-uppercase text-muted border-bottom border-secondary rounded-0">
				<h5 class="modal-title text-center text-white" id="title_udb"></h5>
				<button type="button" class="close" id="close_modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="all_details"></div>
			</div>
		</div>
	</div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJiNdQZEwhcv8Hep7SF8qDo2SJDM4Ttdw" async defer></script>
