<div class="container-fluid mg-top-77 pad-00">
	<input type="hidden" id="sector" value="<?= $this->session->userdata('department'); ?>"/>
	<div class="col-12">
		<div class="row">
			<div class="col-12">
				<div class="card-deck" id="main_cards"></div>
			</div>

			<div class="col mg-top-10">
				<div class="row">
					<div class="col-3 pad-lef-0">
						<div class="rre_techs"></div>
					</div>
					<div class="col-9">
						<div class="container-fluid">
							<div class="row card-charts"></div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

