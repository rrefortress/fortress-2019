<div class="container">
	<div class="col-md-8 text-center file-center mg-top-30">
		<form method="post" id="file-upload" enctype="multipart/form-data">
			<input type="hidden" id="dep" value="<?= $this->session->userdata('department') ?>">
			<div class="form-group files">
				<p class="text-center lead h4">Upload File <i>(accepts .csv, .xlsx & .xls files only)</i></p>
				<input type="file" name="udbfile" id="udbfile" class="form-control btn" accept=".csv, .xlsx, .xls" required>
			</div>

			<div class="container-fluid">
				<div class='progress-upload' id="progressUpload">
					<div class='progress-bar-upload' id='progressBarUpload'></div>
					<div class='percentUpload' id='percentUpload'>0%</div>
				</div>
			</div>

			<div class="form-group mg-top-16">
				<button class="btn btn-outline-primary text-center" id="submitButton" type="submit" disabled><i class="fas fa-file-upload"></i> Upload</button>

				<button class="btn btn-outline-danger text-center" type="button" id="clear_data_file" disabled><i class="fas fa-eraser"></i> Remove</button>
				<a type="button" id="upload-link" class="btn btn-outline-success"><i class="fas fa-file-download"></i> Download Format</a>
			</div>

			<div id='outputImage'></div>
		</form>
	</div>
</div>

<div class="modal fade" id="upload_loader" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header" style="background: #3445b4">
				<p class="text-upload-title modal-title text-white text-center" id="staticBackdropLabel">Please wait a moment, your data was extracted responsively.</p>
			</div>
			<div class="modal-body bg-light text-center">
				<img src="<?= base_url('style/Spin-1s-200px.svg');?>" width="200" height="200" class="rounded-circle"><br>
				<em class="text-muted text-process display-5">Processing...</em>
			</div>
		</div>
	</div>
</div>
