<div class="container-fluid mg-top-77">
		<div>
			<div class="card text-center">
  				<div class="card-header">
			  	<div class="nav nav-tabs card-header-tabs" id="myList" role="tablist">
				  <a class="nav-link text-dark active" data-toggle="list" href="#profile" role="tab">Profile</a>
				  <a class="nav-link text-dark" data-toggle="list" href="#messages" role="tab">Change Password</a>
				</div>
			</div>

				<div class="tab-content">
				  <div class="tab-pane fade show active" id="profile" role="tabpanel">
				  	<div class="my-3">
						<form method="post" id="update_user" enctype="multipart/form-data">
						<input type="hidden" name="uid" value="<?= $this->session->userdata('id');?>">
						<div class="container">

							<div class="row">
								<div class="col-2"></div>
								<div class="col-4 mg-bot-10-">
									<div class="img-con">
										<img src="<?= empty($this->session->userdata('profile')) ? base_url('./assets/style/default.jpg') : $this->session->userdata('profile'); ?>" alt="Avatar" class="imagePreview img-thumbnail"  height="200" width="200" style="width:100%">
										<div class="img-middle">
											<label class="btn btn-lg btn-primary w-197">
												<i class="fas fa-camera"></i> <br>UPLOAD IMAGE
												<input type="file" id="uploadFile" class="uploadFile" name="image_file" accept="image/*"  style="width: 0px;height: 0px;overflow: hidden;">
											</label>
										</div>
									</div>
								</div>

								<div class="col-6 mg-top-10">
									<div class="container">
										<div class="col-8">
											<div class="form-group">
												<label class="float-left" for="oldpassword">Username</label>
												<input type="text" id="uname"  class="form-control" name="username" placeholder="Enter Username" value="<?= $this->session->userdata('username'); ?>" required autofocus>
											</div>

											<div class="form-group">
												<label class="float-left" for="oldpassword">Firstname</label>
												<input type="text" id="fname" class="form-control" name="fname" placeholder="Enter Firstname"  value="<?= $this->session->userdata('fname'); ?>" required>
											</div>

											<div class="form-group">
												<label class="float-left" for="oldpassword">Lastname</label>
												<input type="text" id="lname" class="form-control" name="lname" placeholder="Enter Lastname" value="<?= $this->session->userdata('lname'); ?>"  required>
											</div>
										</div>
									</div>


								</div>
							</div>

							<div class="form-group">
								<hr>
								<div class="text-center">
									<button type="submit" class="btn btn-primary">Submit</button>
									<button type="button" class="btn btn-danger" id="btn_clear">Clear</button>
								</div>
							</div>
						</div>
						</form>
				    </div>
				  </div>
				  <div class="tab-pane fade" id="messages" role="tabpanel">
				  	<div class="col-4 mx-auto my-5">
				  		<form method="post" enctype="multipart/form-data" id="change_password">
				  		<div class="form-group">
						    <label class="float-left" for="oldpassword">Old password</label>
							<input type="password" class="form-control" name="oldpassword" id="oldpassword" placeholder="Enter old password" required autofocus>
						</div>
						<div class="form-group">
						    <label class="float-left" for="newpassword">New password</label>
							<input type="password" class="form-control" name="newpassword" id="newpassword" placeholder="Enter new password" required>
						</div>
						<div class="form-group">
						    <label class="float-left" for="confirmpassword">Confirm password</label>
							<input type="password" class="form-control" name="confirmpassword" id="confirmpassword" placeholder="Enter confirm password" required>
						</div>
						<button type="submit" class="btn btn-primary" id="submit_password">Submit</button>
						</form>
						<span id="status"></span>
				  	</div>
				  </div>
				</div>
		</div>
	</div>
</div>
