<div class="container-fluid mg-top-77">
		<div class="row mg-bot-10 mg-top-10">
			<div class="col-4">
				<div class="row">
					<div class="col-12">
						<div class="card border border-secondary shadow-lg">
							<div class="card-body">
								<h5 class="card-title text-uppercase text-muted">Mission</h5>
								<p class="card-text">The Fortress, as prime source of unified data, will provide accurate, reliable and timely information pertaining to the state of the network.</p>
							</div>
						</div>
					</div>
					<div class="col-12 mg-top-10">
						<div class="card border border-secondary shadow-lg">
							<div class="card-body">
								<h5 class="card-title text-uppercase text-muted">Vision</h5>
								<p class="card-text">The Fortress will be Globe’s universal database that will provide integrated data and holistic view of the network.</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-4 pad-00">
				<div class="container text-center">
					<div class="card-deck pad-00">
						<div class="card border border-secondary shadow-lg">
							<div class="container">
								<img src="<?= base_url('assets/style/FortressBig.png') ?>" style="height: 395px;" class="card-img-top rounded-circle img-thumbnail mg-top-10 shadow-lg" alt="Fortress">
							</div>
							<div class="card-body text-center">
								<h5 class="card-title text-uppercase t-12">Fortress</h5>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-4 pad-00">
				<div class="col d-flex justify-content-center">
					<div class="col-6">
						<div class="card border border-secondary shadow-lg">
							<div class="container">
								<img src="<?= base_url('assets/style/About/3.jpg') ?>" class="card-img-top rounded-circle mg-top-10 img-thumbnail shadow-lg" alt="Joel Arvy Aragon">
							</div>
							<div class="card-body text-center">
								<h5 class="card-title text-uppercase t-12">Joel Aragon</h5>
								<p class="card-text text-muted small">Lead Developer</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 pt-1">
					<div class="container">
						<div class="form-row">
							<div class="col-6">
								<div class="card border border-secondary shadow-lg">
									<div class="container">
										<img src="<?= base_url('assets/style/About/4.jpg') ?>" class="card-img-top rounded-circle mg-top-10 img-thumbnail shadow-lg" alt="Bryan Espina">
									</div>
									<div class="card-body text-center">
										<h5 class="card-title text-uppercase t-12">Bryan Espina</h5>
										<p class="card-text text-muted small">Developer</p>
									</div>
								</div>
							</div>
							<div class="col-6">
								<div class="card border border-secondary shadow-lg">
									<div class="container">
										<img src="<?= base_url('assets/style/About/1.jpg') ?>" class="card-img-top rounded-circle mg-top-10 img-thumbnail shadow-lg" alt="Darius Paul Bacate">
									</div>
									<div class="card-body text-center">
										<h5 class="card-title text-uppercase t-12">Darius Bacate</h5>
										<p class="card-text text-muted small">Developer</p>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</div>
