<div class="container-fluid">
	<div class="row pad-00">
		<div class="col-9 slide-pic shadow-lg pad-00">
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<?php foreach ($this->config->item('sliders')  as $key => $row) {  ?>
						<li data-target="#carouselExampleIndicators" data-slide-to="<?=$key;?>" class="<?= $key === 0 ? 'active' : '' ?>"></li>
					<?php } ?>
				</ol>
				<div class="carousel-inner">
					<?php foreach ($this->config->item('sliders')  as $key => $row) {  ?>
						<div class="carousel-item <?= $key === 0 ? 'active' : '' ?> shadow-lg">
							<img src="<?= base_url('assets/style/Slider/'.$row.'.jpg') ?>" class="d-block w-100 slider-h-598 img-fluid" alt="<?=$row?>">
						</div>
					<?php } ?>
				</div>
			</div>
		</div>

		<div class="col-3 login-page">
			<div class="container">
				<form class="mg-top-70" method="post" id="login">
					<div class="form-group text-center">
						<div class="text-center">
							<img src="<?= base_url('assets/style/FortressBig.png') ?>" class="rounded img-fluid login-h-160" alt="Responsive image">
						</div>
						<img src="<?= base_url('assets/style/fortress.svg') ?>" class="img-responsive" alt="road-rage-font" border="0" width="186">
					</div>

					<div class="form-group">
						<label class="sr-only" for="Username">Username</label>
						<div class="input-group mb-2">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="fas fa-user-tie"></i></div>
							</div>
							<input type="text" name="username" id="username" class="form-control" id="Username" placeholder="Username / ID" required autofocus>
						</div>
					</div>
					<div class="form-group">
						<label class="sr-only" for="Password">Password</label>
						<div class="input-group mb-2">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="fas fa-key"></i></div>
							</div>
							<input type="password" name="password" id="password" class="form-control" id="Password" placeholder="Password" required>
						</div>
					</div>

					<div class="col-auto">
						<div class="form-check mb-2">
							<input class="form-check-input" id="remember" type="checkbox">
							<label class="form-check-label" for="remember">
								Remember me
							</label>
						</div>
					</div>

					<button type="submit" class="btn btn-primary btn-login btn-block pull-right">Submit</button>
					<style>.abcRioButton {width: auto !important;}</style>
					<div id="gSignIn" class="mg-top-5"></div>
					<div id="message"></div>
				</form>
				<div class="text-center small">
					<span><?= $this->config->item('site_name'); ?></span>
					<span><?= $this->config->item('site_version'); ?></span></br>
					<span><?= $this->config->item('site_author'); ?></span>
					<span><?= $this->config->item('site_department'); ?></span>
					<span><?= $this->config->item('site_reserved'); ?></span>
				</div>
			</div>
		</div>
	</div>
</div>
