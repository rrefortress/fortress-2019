<div class="container-fluid mg-top-77">
	<div class="col-12 m-0 p-0">
<!-- 		<div class="row">
			<nav class="col navbar navbar-expand-lg navbar-light bg-light mb-2">
			  <a class="navbar-brand" href="#"></a>
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon"></span>
			  </button>
			  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			    <div class="navbar-nav">
			      <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
			      <a class="nav-item nav-link" href="#">Features</a>
			      <a class="nav-item nav-link" href="#">Pricing</a>
			      <a class="nav-item nav-link disabled" href="#">Disabled</a>
			    </div>
			  </div>
			</nav>
		</div> -->
		<!-- style="max-width: 1250px; max-height: 630px;" -->
		<div class="row" style="max-height: 550px;">
			<div id="jdiv" class="row m-0 p-0">
				<div class="container" style="max-width: 1250px;">
					<table id="jtable" class="display nowrap"></table>
				</div>
			</div>
			<div id="mapdiv" class="row m-0 p-0">
				<div class="container" style="max-width: 1250px;">
					<div id="map" class="row p-1 shadow" style="width: 1200px; height: 180px; margin: 0 auto;">
						
					</div>
				</div>
			</div>
			<div class="row m-0 p-0"  >
					<div id="maindiv" class="container" style="max-width: 1230px; height: 550px">
						<table id="dtable" class="force-fit display nowrap table-xxs table-hover table-striped table-bordered compact mt-3">	
						</table>
					</div>
			</div>
<!-- 			<div id="subdiv" class="row m-0 p-0">
				<div class="container" style="max-width: 1250px;">
					<table id="jtable" class="display nowrap"></table>
				</div>
					<div class="container" style="max-width: 1230px; height: 100px;">
						<table id="dtable" class="display nowrap table-xxs table-hover table-striped table-bordered compact mt-3">
							<tbody></tbody>
						</table>

					</div>
			</div> -->

		</div>

	</div>
</div>
</div>
</div>	

	</body>
	<?php $this->load->view('/layout/footer'); ?>
	<script src="../../../action/sites_actions.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJiNdQZEwhcv8Hep7SF8qDo2SJDM4Ttdw&callback=initMap"
	async defer></script>
	</html>
