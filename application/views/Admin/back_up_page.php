<div class="container-fluid">
	<div class="col-12">
		<div class="row">
			<div class="col-4">
				<p class="lead">No. of Tables: <b id="count_tbl"></b></p>
			</div>

			<div class="col-4">
				<div class="input-group input-group-sm">
					<input type="text" class="form-control form-control-sm" placeholder="Search" id="search" autocomplete="off">
					<div class="input-group-append">
					<span class="input-group-text">
						<a href="#" class="text-muted"><i class="fas fa-search"></i></a>
					</span>
					</div>
				</div>
			</div>
			<div class="col-4 text-right">
<!--				<button type="button" class="btn btn-outline-success btn-sm btn-backup" style="margin-right: 30px"><i class="fas fa-database"></i> Back-up all</button>-->
				<div class="dropdown">
					<button class="btn btn-outline-success btn-sm dropdown-toggle" style="margin-right: 30px" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fas fa-database"></i> Back-up All
					</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						<button class="dropdown-item btn-backup" data-type='sql'><i class="fas fa-database"></i> as .SQL</button>
						<div class="dropdown-divider"></div>
						<button class="dropdown-item btn-backup" data-type='zip'><i class="fas fa-file-archive"></i> as .ZIP</button>
						<div class="dropdown-divider"></div>
						<button class="dropdown-item btn-backup" data-type='gz'><i class="fas fa-box-open"></i> as .GZ</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="card_datas" class="col row"></div>

	<div class="col-12">
		<center>
			<div class="text-center" id='db-pagination'> </div>
		</center>
	</div>
</div>

<div class="modal fade" id="download_loader" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header" style="background: #3445b4">
				<p class="text-upload-title modal-title text-white text-center" id="staticBackdropLabel">Please wait a moment.</p>
			</div>
			<div class="modal-body bg-light text-center">
				<img src="<?= base_url('style/Spin-1s-200px.svg');?>" width="200" height="200" class="rounded-circle"><br>
				<em class="text-muted text-process display-5">Downloading...</em>
			</div>
		</div>
	</div>
</div>




