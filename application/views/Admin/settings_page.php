<div class="container-fluid mg-top-77">

				<div class="col-12">
					<div class="row">
						<div class="col-3">
							<button type="button" class="btn btn-outline-primary" id="add_new"><i class="fas fa-plus"></i> Add New</button>
						</div>
						<div class="col-6">

						</div>
						<div class="col-3">
							<div class="form-group row">
								<label for="sort_menu" class="text-muted col-sm-4 col-form-label">Sort by:</label>
								<div class="col-sm-8">
									<select class="form-control" id="sort_menu">
										<option selected value="">All</option>
										<option>Admin</option>
										<option>Guest</option>
										<option>RRE</option>
										<option>RFE</option>
										<option>RCNE</option>
										<option>RGPM</option>
										<option>RWE</option>
									</select>
								</div>

							</div>
						</div>
					</div>

				</div>

				<div class="col-12">
					<table class="table table-bordered table-sm" id="menu_tbl" style="border-collapse:collapse;">
						<caption>List of Menus</caption>
						<thead>
						<tr>
							<th align="center" style="width: 50px;"></th>
							<th class="text-center" style="width: 70px;" scope="col">Order</th>
							<th scope="col">Name</th>
							<th scope="col">Icon</th>
							<th scope="col">Base Url</th>
							<th scope="col">User</th>
							<th class="text-center" style="width: 115px;" scope="col">Action</th>
						</tr>
						</thead>
						<tbody id="tbl_admin_menus"></tbody>
					</table>
				</div>

</div>

<div class="modal fade" id="add_edit_menu" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered" role="document">
		<div class="modal-content">

			<div class="modal-header">
				<h5 class="modal-title modal_title">Add New Menu</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="save_menu" method="POST">
					<input type="hidden" name="type" id="type">
					<input type="hidden" name="mid" id="mid">
					<div class="col-12">
							<div class="form-group">
								<label for="order">Order</label>
								<input type="number" name="order" class="form-control" id="order" placeholder="Enter Order" required>
							</div>

							<div class="form-group">
								<label for="name">Menu Name</label>
								<input type="text" name="name" class="form-control" id="name" placeholder="Enter Menu Name" required>
							</div>

							<div class="form-group">
								<label for="icon">Icon Name</label>
								<input type="text" name="icon" class="form-control" id="icon" placeholder="Enter Icon Name" required>
							</div>

							<div class="form-group">
								<label for="base_url">Base URL</label>
								<input type="text" name="base_url" class="form-control" id="base_url" placeholder="Enter Base Url" required>
							</div>

							<div class="form-group">
								<label for="department">Department</label>
								<select class="form-control" name="department" id="department">
									<option value="Admin">Admin</option>
									<option value="Guest">Guest</option>
									<option value="RRE">RRE</option>
									<option value="RgPM">RgPM</option>
									<option value="RFE">RFE</option>
									<option value="RCNE">RCNE</option>
									<option value="RWE">RWE</option>
								</select>
							</div>
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" id="btn_register">Submit</button>
			</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="add_sub_menu" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog modal-dialog-scrollable modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal_title">Add Sub Menu</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="save_sub_menu" method="POST">
					<input type="hidden" name="url" id="url">
					<div class="col-12">

							<div class="form-group">
								<label for="order">Enter number of sub-menu</label>
								<input type="number" name="no_sub_menu" class="form-control" id="no_sub_menu" placeholder="Enter sub-menu" required>
							</div>

						<div id="num_of_menus"></div>
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" id="btn_register">Submit</button>
			</div>
			</form>
		</div>
	</div>
</div>
