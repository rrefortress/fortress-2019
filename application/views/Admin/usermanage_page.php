<div class="container-fluid mg-top-77 pad-00">
	<div class="col-12">
		<div class="container-fluid">
				<div class="mg-top-10">
					<button class="btn btn-outline-primary pull-left" id="addUser"><i class="fas fa-user-plus"></i> Add User</button>
					<button class="btn btn-outline-warning pull-left d-none" id="editUser"><i class="fas fa-user-edit"></i> Edit User</button>
					<button class="btn btn-outline-danger pull-left d-none" id="delUser"><i class="fas fa-user-times"></i> Delete User</button>
					<button class="btn btn-outline-secondary pull-left d-none" id="resetUser"><i class="fas fa-user-cog"></i> Reset Password</button>
				</div>
			<div class="border shadow mg-top-10">
				<div id="maindiv" class="p-1"></div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="addUserModal" tabindex="-1" role="dialog"  aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal_title">Add User</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form id="SaveUser" method="POST">
						<input type="hidden" id="uid" name="uid">

						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label for="username">Username</label>
									<input type="text" name="username" class="form-control" id="username" placeholder="Enter username" required>
								</div>

								<div class="form-group">
									<label for="id">ID</label>
									<input type="number" name="globe_id" class="form-control" id="globe_id" placeholder="Enter ID" onKeyDown="if(this.value.length==10) return false;" required>
								</div>

								<div class="form-group">
									<label for="email">Email</label>
									<input type="email" name="email" class="form-control" id="email" placeholder="Enter email" required>
								</div>

								<div class="form-group">
									<label for="fname">First Name</label>
									<input type="text" name="fname" class="form-control" id="fname" placeholder="Enter First Name" required>
								</div>

								<div class="form-group">
									<label for="lname">Last Name</label>
									<input type="text" name="lname" class="form-control" id="lname" placeholder="Enter Last Name" required>
								</div>
							</div>
							<div class="col-6">

								<div class="form-group">
									<label for="department">Department</label>
									<select class="form-control" name="department" id="department">
										<option value="RRE">RRE</option>
										<option value="RgPM">RgPM</option>
										<option value="RFE">RFE</option>
										<option value="RCNE">RCNE</option>
										<option value="RWE">RWE</option>
									</select>
								</div>

								<div class="form-group">
									<label for="geoarea">Assigned Area</label>
									<select class="form-control" name="geoarea" id="geoarea">
										<option value="national">NATIONAL</option>
										<option value="vis">VIS</option>
										<option value="min">MIN</option>
										<option value="nlz">NLZ</option>
										<option value="slz">SLZ</option>
										<option value="ncr">NCR</option>
									</select>
								</div>

								<div class="form-group">
									<label for="access_level">Access Level</label>
									<select class="form-control" name="access_level" id="access_level">
										<option>Standard</option>
										<option>Admin</option>
										<option>Guest</option>
									</select>
								</div>

								<div class="form-group">
									<label for="vendor">Vendor</label>
									<select class="form-control" name="vendor" id="vendor">
										<option value='' selected>None</option>
										<option>Nokia</option>
										<option>Huawei</option>
										<option>BAU</option>
									</select>
								</div>

								<div class="form-group" id="optionsEdit">
									<div class="col-12">
										<div class="row">
											<input type="hidden" name="status" id="status">
											<div class="col-12">
												<label for="status">Status</label><br>
												<input type="checkbox" id="chk_status" class="form-control" data-on="Activated" data-off="Deactivated" data-width="120" data-height="38" data-toggle="toggle" data-onstyle="outline-primary" data-offstyle="outline-danger">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary" id="btn_register">Submit</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
