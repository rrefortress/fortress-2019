<div class="container-fluid mg-top-77">

	<div class="col-12">
		<div class="row">
			<div class="col-3">

			</div>
			<div class="col-6">

			</div>
			<div class="col-3">
				<div class="form-group row">
					<label for="sort_menu" class="text-muted col-sm-4 col-form-label">Sort by:</label>
					<div class="col-sm-8">
						<select class="form-control" id="sort_menu">
							<option selected value="">All</option>
							<option>Admin</option>
							<option>RRE</option>
							<option>RFE</option>
							<option>RCNE</option>
							<option>RGPM</option>
							<option>RWE</option>
						</select>
					</div>

				</div>
			</div>
		</div>

	</div>

	<div class="col-12">
		<table class="table table-bordered table-sm" id="menu_tbl" style="border-collapse:collapse;">
			<caption>List of Menus</caption>
			<thead>
			<tr>
				<th align="center" style="width: 50px;"></th>
				<th class="text-center" style="width: 70px;" scope="col">Order</th>
				<th scope="col">Name</th>
				<th scope="col">Icon</th>
				<th scope="col">Base Url</th>
				<th scope="col">User</th>
				<th class="text-center" style="width: 80px;" scope="col">Action</th>
			</tr>
			</thead>
			<tbody id="tbl_admin_menus">

			</tbody>
		</table>
	</div>


	<!--			</div>-->
	<!--			<div class="tab-pane fade" id="nav-user" role="tabpanel" aria-labelledby="nav-user-tab">-->
	<!--				<div class="container-fluid mg-top-10">-->
	<!--					<div class="row">-->
	<!--						<div class="col-3">-->
	<!--							<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">-->
	<!--								<a class="nav-link active" id="v-pills-rre-tab" data-toggle="pill" href="#v-rre-home" role="tab" aria-controls="v-rre-home" aria-selected="true">RRE</a>-->
	<!--								<a class="nav-link" id="v-pills-rcne-tab" data-toggle="pill" href="#v-pills-rcne" role="tab" aria-controls="v-pills-rcne" aria-selected="false">RCNE</a>-->
	<!--								<a class="nav-link" id="v-pills-rfe-tab" data-toggle="pill" href="#v-pills-rfe" role="tab" aria-controls="v-pills-rfe" aria-selected="false">RFE</a>-->
	<!--								<a class="nav-link" id="v-pills-rgpm-tab" data-toggle="pill" href="#v-pills-rgpm" role="tab" aria-controls="v-pills-rgpm" aria-selected="false">RGPM</a>-->
	<!--							</div>-->
	<!--						</div>-->
	<!--						<div class="col-9">-->
	<!--							<div class="tab-content" id="v-pills-tabContent">-->
	<!--								<div class="tab-pane fade show active" id="v-pills-rre" role="tabpanel" aria-labelledby="v-pills-rre-tab">rre</div>-->
	<!--								<div class="tab-pane fade" id="v-pills-rcne" role="tabpanel" aria-labelledby="v-pills-rcne-tab">rcne</div>-->
	<!--								<div class="tab-pane fade" id="v-pills-rfe" role="tabpanel" aria-labelledby="v-pills-rfe-tab">rfe</div>-->
	<!--								<div class="tab-pane fade" id="v-pills-rgpm" role="tabpanel" aria-labelledby="v-pills-rgpm-tab">RGPM</div>-->
	<!--							</div>-->
	<!--						</div>-->
	<!--					</div>-->
	<!--				</div>-->
	<!---->
	<!--			</div>-->
	<!--		</div>-->

</div>
</div>
</div>

<!--<div class="modal fade" id="add_edit_menu" tabindex="-1" role="dialog"  aria-hidden="true">-->
<!--	<div class="modal-dialog modal-sm modal-dialog-centered" role="document">-->
<!--		<div class="modal-content">-->
<!--			<div class="modal-header">-->
<!--				<h5 class="modal-title modal_title">Add New Menu</h5>-->
<!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--					<span aria-hidden="true">&times;</span>-->
<!--				</button>-->
<!--			</div>-->
<!--			<div class="modal-body">-->
<!--				<form id="save_menu" method="POST">-->
<!--					<input type="hidden" name="mid" id="mid">-->
<!--					<div class="col-12">-->
<!--						<div class="form-group">-->
<!--							<label for="order">Order</label>-->
<!--							<input type="number" name="order" class="form-control" id="order" placeholder="Enter Order" required>-->
<!--						</div>-->
<!---->
<!--						<div class="form-group">-->
<!--							<label for="name">Menu Name</label>-->
<!--							<input type="text" name="name" class="form-control" id="name" placeholder="Enter Menu Name" required>-->
<!--						</div>-->
<!---->
<!--						<div class="form-group">-->
<!--							<label for="icon">Icon Name</label>-->
<!--							<input type="text" name="icon" class="form-control" id="icon" placeholder="Enter Icon Name" required>-->
<!--						</div>-->
<!---->
<!--						<div class="form-group">-->
<!--							<label for="base_url">Base URL</label>-->
<!--							<input type="text" name="base_url" class="form-control" id="base_url" placeholder="Enter Base Url" required>-->
<!--						</div>-->
<!---->
<!--						<div class="form-group">-->
<!--							<label for="department">Department</label>-->
<!--							<select class="form-control" name="department" id="department">-->
<!--								<option value="Admin">Admin</option>-->
<!--								<option value="RRE">RRE</option>-->
<!--								<option value="RgPM">RgPM</option>-->
<!--								<option value="RFE">RFE</option>-->
<!--								<option value="RCNE">RCNE</option>-->
<!--								<option value="RWE">RWE</option>-->
<!--							</select>-->
<!--						</div>-->
<!---->
<!--					</div>-->
<!---->
<!---->
<!--			</div>-->
<!--			<div class="modal-footer">-->
<!--				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
<!--				<button type="submit" class="btn btn-primary" id="btn_register">Submit</button>-->
<!--			</div>-->
<!--			</form>-->
<!--		</div>-->
<!--	</div>-->
<!--</div>-->

<?php $this->load->view('/layout/footer'); ?>
<script src="<?= base_url('action/logs.js'); ?>"></script>
