
<div class="col-12 pad-00 border-bottom pb-2">
    <a type="button" class="btn btn-sm btn-outline-primary upload_wire" href="#"><i class="fas fa-upload"></i> UPLOAD</a>
    <a type="button" class="btn btn-sm btn-outline-warning" href="./Formats/Wireline_MDB.csv"><i class="fas fa-file-csv"></i> DOWNLOAD FORMAT</a>       
    <button type="button" class="delete_wire btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i> DELETE ALL</button>   
</div>
<div class="col-12 p-1 shadow-sm border mt-1">
   <div id="maindiv" class="w-100 p1">
        <div class="d-dis d-wireline"></div>	
   </div>
</div>

<div class="modal fade" data-backdrop="static" id="upload_wire" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header global_color text-white">
                <h5 class="modal-title text-white">UPLOAD FILE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="dropzone wireline pad-00" style="border: 1px dashed #0087F7;">
                        <div class="dz-message" style="text-align: center;" >
                            <p class="text-center lead h4 text-muted">Upload File <i>(accepts .CSV file only)</i></p>
                            <a class="btn btn-primary btn-sm" href="#" role="button">
                                <i class="fa fa-upload" aria-hidden="true"></i> Click to Upload
                            </a>
                            <p class="text-center lead h5 text-muted mg-top-10">Or drag files here.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>