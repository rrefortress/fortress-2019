<input type="hidden" id="ktype" value="<?= strtoupper($title); ?>">
<ul class="nav nav-pills small nav-justified bg-white shadow-sm mg-top-10 border border-top border-bottom-0 border-left-0 border-right-0 tech_tab" id="trackTab" role="tablist">
	<?php foreach (array('2G', '3G', '4G', '5G') as $key => $row) {  ?>
		<li class="nav-item" role="presentation">
			<a class="nav-link <?=$key === 0 ? 'active' : '';?>" id="P<?=strtoupper($row);?>-tab" data-toggle="tab" href="#P<?=strtoupper($row);?>" role="tab" aria-controls="P<?=$row;?>" aria-selected="true"><?=strtoupper($row);?></a>
		</li>
	<?php } ?>
</ul>
<div class="tab-content pt-1 shadow-sm border" id="myTabContent">
	<?php foreach (array('P2G', 'P3G', 'P4G', 'P5G') as $key=>$row) { ?>
		<div class="tab-pane fade show <?=$key == 0 ? 'active' : '';?>" id="<?=$row?>" role="tabpanel" aria-labelledby="P<?=$row?>-tab">
            <div class="col-12 pad-00 border-bottom pb-2">
				<a type="button" class="btn btn-sm btn-outline-primary <?=$row?>" href="#"><i class="fas fa-upload"></i> UPLOAD</a>
				<a type="button" class="btn btn-sm btn-outline-warning" href="./Formats/Plan/<?=$row;?>.csv"><i class="fas fa-file-csv"></i> DOWNLOAD FORMAT</a>       
				<button type="button" class="btn btn-sm btn-outline-danger dl-<?=$row?>"><i class="fas fa-trash"></i> DELETE ALL</button>   
			</div>
			<div class="col-12 p-1">
				<div class="d-dis d-<?=$row?>"></div>			
			</div>
		</div>
	<?php } ?>
</div>

<?php foreach (array('2g', '3g', '4g', '5g') as $row) {  ?>
	<div class="modal fade" data-backdrop="static" id="p_<?=$row;?>" tabindex="-1" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header global_color text-white">
					<h5 class="modal-title text-white">UPLOAD FILE FOR <?=strtoupper($row);?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="text-center">
						<div class="dropzone drp_<?=$row;?> pad-00" style="border: 1px dashed #0087F7;">
							<div class="dz-message" style="text-align: center;" >
								<p class="text-center lead h4 text-muted">Upload File <i>(accepts .CSV file only)</i></p>
								<a class="btn btn-primary btn-sm" href="#" role="button">
									<i class="fa fa-upload" aria-hidden="true"></i> Click to Upload
								</a>
								<p class="text-center lead h5 text-muted mg-top-10">Or drag files here.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<?php } ?>	