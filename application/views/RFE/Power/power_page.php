<div class="col-12">
	<div class="row">
		<div class="col-7 pl-0">
			<?php if ($this->session->userdata('geoarea') <> 'nat' && $level <> 'GUEST') { ?>
				<div class="btn-group mg-top-10 btn-group-sm" role="group">
					<button type="button" class="btn btn-outline-primary add_new_power"><i class="fas fa-plus"></i> ADD NEW</button>
					<button type="button" class="btn btn-outline-success bulk-upload_power"><i class="fas fa-upload" data-toggle="tooltip" data-placement="top" title="BULK UPLOAD"></i></button>
					<button type="button" class="btn btn-outline-secondary export_power"><i class="fas fa-download" data-toggle="tooltip" data-placement="top" title="EXPORT"></i></button>
					<a download href="./Formats/POWER.csv" type="button" class="btn btn-sm btn-outline-warning" data-toggle="tooltip" data-placement="top" title="DOWNLOAD FORMAT"><i class="fas fa-file-download"></i></a>
				</div>
				
			<?php } else { ?>
				<button type="button" class="btn btn-sm mg-top-10 btn-outline-secondary export_power"><i class="fas fa-download"></i> EXPORT</button>
			<?php } ?>
			<div class="btn-group mg-top-10 btn-group-sm" role="group">
					<button type="button" class="btn btn-outline-primary power-site" disabled><i class="fas fa-eye"></i> VIEW SITE</button>
					<button type="button" class="btn btn-outline-danger plan-tagging" disabled><i class="fas fa-drafting-compass"></i> RAN TAGGING</button>
					<button type="button" class="btn btn-outline-dark gene-power" data-toggle="tooltip" data-placement="top" title="GENERATE REPORT"><i class="fas fa-newspaper"></i></button>
			</div>
		</div>
		<div class="col-5">
			<div class="row pt-3">
				<div class="col-6 text-right">
					<b class="text-muted">SITES COMPLETED </b> :
				</div>
				<div class="col-4 bg-success text-center text-white border-right">
					<b class="underline cmp"> 0 / 0 </b>
				</div>
				<div class="col-2 bg-warning text-center">
					<b class="crate">0 %</b>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-12 mg-top-5 border shadow">
	<div id="maindiv" class="w-100 p1">
		<table id="power_tbl" class="power_tbl display nowrap table-sm small text-center table-responsive-sm table-hover table-bordered compact mt-1"></table>
	</div>
</div>

<div class="modal fade oy-hidden pad-00" data-backdrop="static" id="add_new-power" tabindex="-1" aria-labelledby="add_new" aria-hidden="true">
	<div class="modal-dialog modal-full modal-dialog-scrollable modal-dialog-centered">
		<form id="add_powerdb" method="POST">
			<input type="hidden" name="GEOAREA" value="<?=$this->session->userdata('geoarea');?>">
			<div class="modal-content">
				<div class="modal-header bg-primary text-white">
					<h5 class="modal-title text-white lead">Add New Site</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row" style="margin-top: -16px;margin-bottom: -20px">
						<div class="col-3 border-right">
							<div class="basic row"></div>
						</div>

						<div class="col-9">
							<div class="row rs">
								<div class="col-4 text-center border-right">
									<div class="rs1 text-left row"></div>
									<div class="text-center justify-content-center text-uppercase lead badge badge-danger p-1 text-wrap">Initial Equipped</div>
									<button data-rs="1" class="btn btn-1 btn-block btn-outline-primary rs-add-btn btn-sm"><i class="fas fa-plus"></i> Add Rectifier</button>
								</div>
								<div class="col-8 rec-form">
									<div class="col-12 mg-top-135">
										<h5 class="text-center text-white">
											<i class="fas fa-box display-2 text-muted"></i></br>
											<small class="text-center text-muted mg-top-10"><em>Click + Add button to new Rectifier form</em></small>
										</h5>
									</div>
								</div>
							</div>
						</div>

						<div class="col-12 border-top mg-top-5">
							<div class="load row"></div>
						</div>
					</div>
				</div>
				<div class="modal-footer p-0 d-flex justify-content-center">
					<button type="button" class="btn btn-danger btn-sm mg-top-5" data-dismiss="modal"><i class="fas fa-times"></i> Close</button>
					<button type="submit" class="btn btn-primary btn-sm mg-top-5"><i class="fas fa-check"></i> Save changes</button>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="modal fade" data-backdrop="static" id="bulk-upload-power" tabindex="-1" aria-labelledby="bulk_response-power" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header bg-primary text-white">
				<h5 class="modal-title text-white">UPLOAD FILE</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="text-center">
					<div class="dropzone dropzone_power pad-00" style="border: 1px dashed #0087F7;">
						<div class="dz-message" style="text-align: center;" >
							<p class="text-center lead h4 text-muted">Upload File <i>(accepts .CSV files only)</i></p>
							<a class="btn btn-primary btn-sm"  href="#" role="button">
								<i class="fa fa-upload" aria-hidden="true"></i> Click to Upload
							</a>
							<p class="text-center lead h5 text-muted mg-top-10">Or drag files here.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" data-backdrop="static" id="bulk-response-power" tabindex="-1" aria-labelledby="bulk_upload-power" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
		<div class="modal-content">
			<div class="modal-header bg-primary text-white">
				<h5 class="modal-title text-white">RESPONSE</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body small lead">
				<div class="col-12">
					<div class="row">
						<div class="col-6">TOTAL INSERTED: <b class="insert">0</b></div>
						<div class="col-6">TOTAL DUPLICATE: <b class="duplicate">0</b></div>
					</div>
				</div>
				<div class="col-12">
					<table class="table table-bordered table-sm">
						<thead>
						<tr>
							<th scope="col" style="width: 10%" class="text-center">#</th>
							<th scope="col">PLAID</th>
							<th scope="col">SITENAME</th>
						</tr>
						</thead>
						<tbody class="res_lists"></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade pad-00" data-backdrop="static" id="gene-report" tabindex="-1" aria-labelledby="gene-report" aria-hidden="true">
	<div class="modal-dialog modal-full modal-dialog-centered modal-dialog-scrollable">
		<div class="modal-content">
			<div class="modal-header global_color text-white">
				<h5 class="modal-title text-white">Generated Report</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body p-1 small lead bg-light">
				<div class="container-fluid">
					<div class="row">
						<div class="col-8 bg-white shadow p-0 table-bordered gene-tbl-report">
					
						</div>
						<div class="col-4 bg-white border shadow display-5 p-2">
							<div class="form-group">
								<textarea class="form-control" id="insights" placeholder="Enter your insights here..." rows="19"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 pt-2 text-right pr-0">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade pad-00" data-backdrop="static" id="power-item" tabindex="-1" aria-labelledby="gene-report" aria-hidden="true">
	<div class="modal-dialog modal-full modal-dialog-centered modal-dialog-scrollable">
		<div class="modal-content">
			<div class="modal-header global_color text-white">
				<h5 class="modal-title text-white lead text-uppercase">Site Profile</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body p-0 bg-light small lead bg-light">
				<div class="container-fluid">
					<div class="row">
						<div class="col-3 p-0">
							<img src="<?= base_url('assets/pictures/profile.jpg') ?>" class="img-fluid" alt="tower-profile">
						</div>
						<div class="col-9 pl-0">
							<div class="card-columns p-2 c-profiles">

							</div>
						</div>
					</div>
				</div>
				<div class="col-12 pt-1 text-right border-top">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade pad-00" data-backdrop="static" id="plan-tagging" tabindex="-1" aria-labelledby="gene-report" aria-hidden="true">
	<div class="modal-dialog modal-full modal-dialog-centered modal-dialog-scrollable">
		<div class="modal-content">
			<div class="modal-header global_color text-white">
				<h5 class="modal-title text-white lead text-uppercase"><i class="fas fa-drafting-compass"></i> RAN Tagging</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body p-0">
				<div class="col mt-2">
					<div class="p_tagging"></div>
				</div>
			</div>
		</div>
	</div>
</div>