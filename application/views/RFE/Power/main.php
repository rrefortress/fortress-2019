<div class="container-fluid mg-top-77">
	<input type="hidden" id="lvl" value="<?=$level ?>">
	<input type="hidden" id="geo" value="<?=$this->session->userdata('geoarea');?>">
	<ul class="nav nav-tabs" id="myTab" role="tablist">
		<li class="nav-item" role="presentation">
			<a class="nav-link active" id="power-tab" data-toggle="tab" href="#power" role="tab" aria-controls="power" aria-selected="true"><i class="fas fa-power-off"></i> Power</a>
		</li>
		<?php if($level  <> 'GUEST' ) { ?>
			<li class="nav-item" role="presentation">
				<a class="nav-link" id="pro-tab" data-toggle="tab" href="#pro" role="tab" aria-controls="pro" aria-selected="false"><i class="far fa-lightbulb"></i> Proposals</a>
			</li>
			<li class="nav-item" role="presentation">
				<a class="nav-link" id="brands-tab" data-toggle="tab" href="#brands" role="tab" aria-controls="brands" aria-selected="false"><i class="fas fa-list"></i> Brands</a>
			</li>
		<?php } ?>

	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="power" role="tabpanel" aria-labelledby="power-tab">
			<?php require_once 'power_page.php'; ?>
		</div>
		<div class="tab-pane fade" id="pro" role="tabpanel" aria-labelledby="pro-tab">
			<?php require_once 'proposal_page.php'; ?>
		</div>
		<div class="tab-pane fade" id="brands" role="tabpanel" aria-labelledby="brands-tab">
			<?php require_once 'brands_page.php'; ?>
		</div>
	</div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJiNdQZEwhcv8Hep7SF8qDo2SJDM4Ttdw" async defer></script>
