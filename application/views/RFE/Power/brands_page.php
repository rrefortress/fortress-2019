<div class="container-fluid mg-top-10">
	<input type="hidden" id="geo" value="<?=$this->session->userdata('geoarea');?>">
	<div class="row pad-lef-right-0">
		<div class="col-3 pad-lef-right-0">
			<div class="nav flex-column nav-pills rounded-0" id="v-pills-tab" role="tablist" aria-orientation="vertical">
				<a class="nav-link active border-0 rounded-0" id="v-pills-rec-tab" data-toggle="pill" href="#v-pills-rec" role="tab" aria-controls="v-pills-rec" aria-selected="true"><i class="fas fa-box"></i> Rectifier</a>
				<a class="nav-link border-0 rounded-0" id="v-pills-bat-tab" data-toggle="pill" href="#v-pills-bat" role="tab" aria-controls="v-pills-bat" aria-selected="false"><i class="fas fa-battery-full"></i> Battery</a>
				<a class="nav-link border-0 rounded-0" id="v-pills-equip-tab" data-toggle="pill" href="#v-pills-equip" role="tab" aria-controls="v-pills-equip" aria-selected="false"><i class="fas fa-toolbox"></i> Equipments</a>
			</div>
		</div>
		<div class="col-9 border-left" style="height: 75vh;overflow: auto">
			<div class="tab-content" id="v-pills-tabContent">
				<div class="tab-pane fade show active" id="v-pills-rec" role="tabpanel" aria-labelledby="v-pills-rec-tab">
					<button type="button" class="col-2 btn btn-sm btn-outline-primary add_new_rec"><i class="fas fa-plus"></i> ADD NEW</button>
					<div class="tbl_rec mg-top-10"></div>
				</div>
				<div class="tab-pane fade" id="v-pills-bat" role="tabpanel" aria-labelledby="v-pills-bat-tab">
					<button type="button" class="col-2 btn btn-sm btn-outline-primary add_new_bat"><i class="fas fa-plus"></i> ADD NEW</button>
					<div class="tbl_bat mg-top-10"></div>
				</div>
				<div class="tab-pane fade" id="v-pills-equip" role="tabpanel" aria-labelledby="v-pills-equip-tab">
					<button type="button" class="col-2 btn btn-sm btn-outline-primary add_new_equip"><i class="fas fa-plus"></i> ADD NEW</button>
					<div class="tbl_equip mg-top-10"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="add_new_rec" data-backdrop="static" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<form class="add_rec" method="post">
			<input type="hidden" name="id" id="rec_id">
			<div class="modal-header">
				<h5 class="modal-title">Add New Rectifier</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
					<div class="form-group">
						<label for="name_rec">Name</label>
						<input type="text" name="name_rec" class="form-control" id="name_rec" placeholder="Enter Rectifier Name" required autofocus>
					</div>
					<div class="form-group">
						<label for="module_capacity">Module Capacity</label>
						<input type="text" name="module_capacity" class="form-control" placeholder="Enter Module Capacity" id="module_capacity" required>
					</div>

					<div class="form-group">
						<label for="no_of_modules">No. Maximium of Modules</label>
						<input type="number" name="empty_modules" class="form-control" placeholder="Enter No of Empty Modules" id="empty_modules" required>
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="add_new_bat" data-backdrop="static" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<form class="add_bat" method="post">
			<input type="hidden" name="id" id="bat_id">
			<div class="modal-header">
				<h5 class="modal-title">Add New Battery</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="name_bat">Name</label>
					<input type="text" name="name_bat" class="form-control" id="name_bat" placeholder="Enter Battery Name" required autofocus>
				</div>
<!--				<div class="form-group">-->
<!--					<label for="capacity">Battery Capacity</label>-->
<!--					<input type="number" name="capacity" class="form-control" placeholder="Enter Battery Capacity" id="capacity" required>-->
<!--				</div>-->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="add_new_equip" data-backdrop="static" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<form class="add_equip" method="post">
				<input type="hidden" name="id" id="equip_id">
				<div class="modal-header">
					<h5 class="modal-title">Add New Equipment</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="name_rec">Item Name</label>
						<input type="text" name="name_equip" class="form-control" id="name_equip" placeholder="Enter Item Name" required autofocus>
					</div>
					<div class="form-group">
						<label for="module_capacity">Wattage</label>
						<input type="number" name="wattage" class="form-control" placeholder="Enter Wattage" id="wattage" required>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>
