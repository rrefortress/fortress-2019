<div class="row mg-top-10">
	<div class="col-10">
		<div class="btn-group btn-group-sm" role="group">
			<button type="button" class="btn btn-sm btn-outline-success proposal"><i class="far fa-lightbulb"></i> UPLOAD PROPOSAL</button>
			<button type="button" class="btn btn-sm btn-outline-secondary export_pro"><i class="fas fa-download"></i> EXPORT</button>
			<button type="button" class="btn btn-sm btn-outline-danger clear_pro"><i class="fas fa-eraser"></i> CLEAR</button>
			<a download href="./Formats/PROPOSAL.csv" type="button" class="btn btn-sm btn-outline-warning"><i class="fas fa-file-download"></i> DOWNLOAD FORMAT</a>
		</div>
	</div>
	<div class="col-2 d-flex justify-content-end">
		<button type="button" class="btn btn-sm btn-outline-primary view-proposal"><i class="far fa-eye"></i> VIEW PROPOSALS</button>
	</div>
</div>

<div class="col-12 mg-top-10 border shadow">
	<div id="pro_div" class="mg-top-10 mb-1" style="width: 100%"></div>
</div>

<div class="modal fade" data-backdrop="static" id="bulk-proposal" tabindex="-1" aria-labelledby="bulk_upload" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header bg-warning text-dark">
				<h5 class="modal-title text-white">UPLOAD PROPOSAL FILE</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="text-center">
					<div class="dropzone dropzone_proposal pad-00" style="border: 1px dashed #0087F7;">
						<div class="dz-message" style="text-align: center;" >
							<p class="text-center lead h4 text-muted">Upload File <i>(accepts .CSV files only)</i></p>
							<a class="btn btn-primary btn-sm"  href="#" role="button">
								<i class="fa fa-upload" aria-hidden="true"></i> Click to Upload
							</a>
							<p class="text-center lead h5 text-muted mg-top-10">Or drag files here.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" data-backdrop="static" id="view-pro" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered modal-xl">
		<div class="modal-content">
			<div class="modal-header bg-warning">
				<h5 class="modal-title text-uppercase lead">Proposal Chart</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="pro_charts row"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Download</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" data-backdrop="static" id="save-pro" tabindex="-1">
	<div class="modal-dialog modal-sm modal-dialog-centered ">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Save Proposal</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form id="save_proposal">
				<div class="modal-body">
					<div class="form-group">
						<label for="save_name">Name</label>
						<input type="text" name="save_name" placeholder="Enter Name" class="form-control" id="save_name" aria-describedby="name" required>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" data-backdrop="static" id="view-pro-list" tabindex="-1">
	<div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
		<div class="modal-content">
			<div class="modal-header global_color">
				<h5 class="modal-title text-white text-uppercase">Proposal List</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="prop-lists"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger del_all">Delete All</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" data-backdrop="static" id="edit-pro-list" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered modal-sm">
		<div class="modal-content">
			<form id="rename_prop">
				<div class="modal-header global_color text-center">
					<h5 class="modal-title text-white text-uppercase">Rename Proposal</h5>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<input type="hidden" id="id" name="id">
						<label for="prop_name">Proposal Name</label>
						<input type="text" class="form-control" id="prop_name" name="name" placeholder="Enter Proposal Name" required>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save</button>
					<button type="submit" class="btn btn-danger cancel_prop">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>
