<div class="mg-top-77">
	<div class="container-fluid ">
		<div class="select-proposal shadow-sm p-5 h-75 border">
			<div class="col-12 text-center align-self-center">
				<div class="row">
					<?php foreach (array('tower', 'power') as $key => $row) { ?>
						<div class="col-6 p-5 <?= $row == 'tower' ? 'bg-danger' : 'bg-success' ?>">
							<h1 class="display-1 text-white"><i class="fas <?= $row == 'tower' ? 'fa-broadcast-tower' : 'fas fa-power-off' ?>"></i></h1>
							<h1 class="display-4 text-white lead"><?= strtoupper($row)?></h1>
							<select class="form-control form-control-lg text-center" id="<?= $row?>-prop"></select>
						</div>
					<?php } ?>
				</div>
			</div>
			<div class="col-12 pt-3 d-flex justify-content-center">
				<button class="btn btn-block w-25 btn-lg global_color text-white text-uppercase ass-generate">generate</button>
			</div>
		</div>
		<div class="col-12 ass-tbl d-none">
			<form class="form-inline border-bottom mb-2">
				<?php foreach (array('tower', 'power') as $key => $row) { ?>
					<div class="form-group mb-2 <?= $key > 0 ? 'pl-3' : ''?>">
						<label for="<?= $row?>-prop" class="pr-2 text-uppercase lead small text-muted"><?= strtoupper($row)?> Proposal: </label>
						<h6 class="list-<?= $row?> pt-2"></h6>
					</div>
				<?php } ?>
				<button type="button" class="btn btn-outline-danger btn-sm ml-3 text-uppercase clear-ass" style="margin-top: -5px"><i class="fas fa-eraser"></i> Clear</button>
				<button type="button" class="btn btn-outline-secondary btn-sm ml-2 export_ass" style="margin-top: -5px"><i class="fas fa-download"></i> EXPORT</button>
				<button type="button" class="btn btn-outline-info btn-sm ml-2 scenario" data-toggle="modal" data-target="#scenario" style="margin-top: -5px"><i class="fas fa-exclamation-circle"></i> SCENARIO</button>
			</form>

			<div class="assessment"></div>
		</div>
	</div>
</div>

<div class="modal fade pad-00 oy-hidden" id="scenario" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-full mg-top-0">
		<div class="modal-content">
			<div class="modal-header global_color">
				<h5 class="modal-title text-white" id="staticBackdropLabel">SCENARIO GUIDE</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body bg-dark">
				<img src="<?= base_url('assets/style/scene.png') ?>" class="img-responsive border shadow" alt="Scenarios">
			</div>
			<div class="modal-footer global_color">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
