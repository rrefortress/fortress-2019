<div class="mg-top-77">
	<input type="hidden" id="geo" value="<?=$this->session->userdata('geoarea');?>">
		<div class="container-fluid">
			<div class="nav-container dash-bread">
				<ol class="breadcrumb" style="margin-bottom:-5px">
					<li class="breadcrumb-item"><a href="#" onclick="history.back()">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Facilities</li>
				</ol>
				<button class="btn btn-sm btn-outline-primary rounded-pill rfe-dash"><i class="fas fa-chart-pie"></i> View Dashboard</button>
			</div>
			<ul class="nav nav-tabs mg-top-10" id="fe_tabs" role="tablist"></ul>
			<div class="tab-content shadow-sm" id="fe_tab_content"></div>
		</div>
</div>

<div class="modal fade view-dash-rfe pad-00 oy-hidden" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" data-backdrop="static" aria-hidden="true">
	<div class="modal-dialog modal-full mg-top-0">
		<div class="modal-content">	
			<div class="modal-body p-0 bg-dark">
				<div class="col-12 pl-0 pr-0  border-bottom">
					<div id="dash-carousel" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner">
							<?php foreach (array(0, 1) as $key => $row) { ?>
								<?php $active = $key == 0 ? 'active' : '';?>
								<div class="carousel-item <?=$active?>" style="min-height:94vh">
									<div class="row">
											<div class="col-3 border-right pr-0">
												<div class="border-0 shadow">
													<div class="card-body p-3 bg-primary">
														<div class="container">
															<div class="row">
																<div class="col-12 text-center">
																	<em class="card-subtitle display-5 text-white"><?=$key === 0 ? '<i class="fas fa-cube"></i> Site Type' : '<i class="fas fa-cubes"></i> Tower Capacity (UR)';?></em>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="c2g-dis pt-1 text-center">
													<div class="col-12">
															<div class="card shadow">
																<div class="chart-loader">
																	<div class="chart-container c-dis d-none">
																		<canvas height="1vh" width="1vw" id="dash_<?=$key === 0 ? 'st' : 'tc';?>_chart"></canvas>
																	</div>
																</div>
															</div>
													</div>
													<?php if ($key === 0) { ?>
													<div class="col-12 mt-1">
														<div class="shadow rounded text-white">
																<small class="text-uppercase text-white">MDB Progress Update</small>
																<table class="table table-sm small text-white" style="font-size: 10px;">
																	<thead>
																		<tr>
																		<th scope="col">Region</th>
																		<th class="bg-danger" scope="col">Tower</th>
																		<th class="bg-success" scope="col">Power</th>
																		</tr>
																	</thead>
																	<tbody>
																		<tr>
																			<th scope="row">NCR</th>
																			<td class="tncr-count bg-danger">0</td>
																			<td class="pncr-count bg-success">0</td>
																		</tr>
																		<tr>
																			<th scope="row">SLZ</th>
																			<td class="tslz-count bg-danger">0</td>
																			<td class="pslz-count bg-success">0</td>
																		</tr>
																		<tr>
																			<th scope="row">NLZ</th>
																			<td class="tnlz-count bg-danger">0</td>
																			<td class="pnlz-count bg-success">0</td>
																		</tr>
																		<tr>
																			<th scope="row">VIS</th>
																			<td class="tvis-count bg-danger">0</td>
																			<td class="pvis-count bg-success">0</td>
																		</tr>
																		<tr>
																			<th scope="row">MIN</th>
																			<td class="tmin-count bg-danger">0</td>
																			<td class="pmin-count bg-success">0</td>
																		</tr>
																		<tr class="global_color">
																			<th scope="row">Overall</th>
																			<td class="tover-count bg-danger">0</td>
																			<td class="pover-count bg-success">0</td>
																		</tr>
																	</tbody>
																	</table>
																<!-- <p class="card-text pl-3 text-left small">
																	Tower : <b class="tcmp"></b> (<b class="tcom">0</b>) Completed <br>
																	Power : <b class="pcmp"></b> (<b class="pcom">0</b>) Completed <br>
																</p> -->
														</div>
													</div>
													<?php } ?>

												</div>
											</div>

										<?php if ($key === 0) { ?>
											<div class="col-9 p-0">
												<div class="row">
														<?php foreach ($this->config->item('rfe-type') as $key => $row) { ?>
															<?php
															$data = $this->config->item($row['name'] === 'Tower' ? 'tower-dash': 'power-dash');
															$bor = $row['id'] === 1 ? 'border-right' : '';
															?>

															<div class="col-6 <?=$bor?> <?= $key == 0 ? 'pr-0 ' : 'pl-0 '; ?>">
																<div class="border-0 shadow">
																	<div class="card-body p-1 <?=$row['color']?>">
																		<div class="container">
																			<div class="row">
																				<div class="col-10">
																					<p class="card-title display-6 mb-0 text-white counter <?= $row['name'];?>-count">0</p>
																					<em class="card-subtitle text-white">in total <?=$row['name']?></em>
																				</div>
																				<div class="col-2 text-center mt-2">
																					<div class="container">
																						<i class="text-white h2 <?=$row['icon']?>"></i>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-12 pl-0 pr-0">
																		<div class="row justify-content-center d-flex pt-1">
																			<?php foreach ($data as $row) { ?>
																					<div class="card shadow <?= $key == 0 ? 'cs-w1 ' : 'cs-w2'; ?>">
																						<div class="chart-loader">
																							<div class="chart-container c-dis d-none">
																								<canvas height="1vh" width="1vw" id="dash_<?=$row['name']?>_chart"></canvas>
																								<p class="text-center global_color text-white mb-0 text-uppercase border border-light small p-0 font-weight-bold rounded shadow"><?=$row['title']?></p>
																							</div>
																						</div>
																					</div>
																			<?php } ?>
																		</div>
																</div>

															</div>


														<?php } ?>
													</div>
											</div>
										<?php } else { ?>
											<div class="col-6 pr-0 pl-0">
												<div class="border-0 shadow">
													<div class="border-0 shadow">
														<div class="card-body p-3 bg-success">
															<div class="container text-center">
																<div class="row">
																	<div class="col-12 text-dark">
																		<em class="card-subtitle display-5 text-white"><i class="fas fa-truck-loading"></i> Rectifier Capacity Utilization</em>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-12 pt-1">
													<div class="container-fluid">
														<div class="row">
															<?php foreach (array('DCAB', 'DCBC') as $key => $row) { ?>
																<div class="card col-6 pl-0 pr-0 shadow">
																	<div class="chart-loader">
																		<div class="chart-container c-dis d-none">
																			<canvas height="1vh" width="1vw" id="dash_<?=$row?>_chart"></canvas>
																			<p class="text-center mb-0 global_color text-white text-uppercase border border-light small p-0 font-weight-bold rounded shadow">Rectifier Systems in <?=$key === 0 ? 'Class A - B1' : 'Class B2 - C';?></p>
																		</div>
																	</div>
																</div>
															<?php } ?>
														</div>	
													</div>
												</div>
											</div>
											<div class="col-3 bg-light pr-0 pl-0 border-left">
												<div class="border-0 shadow">
													<div class="card-body p-3 bg-warning">
														<div class="container text-center">
															<div class="row">
																<div class="col-12">
																	<em class="card-subtitle display-5"><i class="fas fa-lightbulb"></i> INSIGHTS</em>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-12 pl-0">
													<textarea class="form-control" id="insights" placeholder="Enter your insights here..." rows="21" autofocus></textarea>
												</div>

											</div>
										<?php } ?>
									</div>
								</div>

							<?php } ?>
						</div>
					</div>
				</div>
				<div class="col-12 global_color text-white pb-1">
					<div class="row pt-1">
						<div class="col-4">
							<div class="text-uppercase lead"><i class="fas fa-chart-pie"></i> FEPM Dashboard Report <?php  ?></div>										
						</div>
						<div class="col-4">
							<ol class="carousel-indicators mt-2" id="dash-carou">
								<li data-target="#dash-carousel" data-slide-to="0" class="slide-0 active"></li>
								<li data-target="#dash-carousel" data-slide-to="1" class="slide-1"></li>
							</ol>
						</div>
						<div class="col-4 text-right">
							<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fas fa-times"></i> Close</button>															
						</div>									
					</div>
										
				</div>
			</div>
	</div>
</div>
