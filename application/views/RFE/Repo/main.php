<div class="container-fluid">
	<input type="hidden" id="lvl" value="<?=$level ?>">
	<input type="hidden" id="geo" value="<?=$this->session->userdata('geoarea');?>">
	<div class="row">
		<div class="col-4 align-right">
			<div class="row">
				<div class="col-12">
					<div class="row">
						<div class="col-4 justify-content-end"></div>
						<div class="col-8 justify-content-end"></div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-12" style="height: 80vh;overflow: auto">
			<div class="crumbs"></div>
			<div class="header_div"></div>
			<div class="row folder d-flex"></div>
		</div>

		<div class="col-12 text-center page_site mg-top-5">
			<div class="container-fluid border-top">
				<div class="row">
					<div class="col-3 text-left text-muted" style="padding: 5px"">
						OVERALL FILES: <b class="total-files"></b>
					</div>
					<div class="col-6">
						<div class="d-flex justify-content-center" id='folder_paginate'></div>
					</div>
					<div class="col-3 text-right text-muted" style="padding: 5px">
						USED STORAGE: <b class="storage"></b>
					</div>
				</div>
			</div>
		</div>
	</div>

<div class="modal fade" data-backdrop="static" id="upload_repo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">
		<div class="modal-content">
			<div class="modal-header global_color">
				<div class="modal-title text-white display-5" id="exampleModalLabel">Upload Files&nbsp</div> <div class="sname text-white"></div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="dropzone pad-00" style="border: 1px dashed #0087F7;">
					<div class="dz-message" style="text-align: center;" >
						<p class="text-center lead h4 text-muted">Upload File <i>(accepts .pdf & .CAD files only)</i></p>
						<a class="btn btn-primary btn-sm"  href="#" role="button">
							<i class="fa fa-upload" aria-hidden="true"></i> Click to Upload
						</a>
						<p class="text-center lead h5 text-muted mg-top-10">Or drag files here.</p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" data-backdrop="static" id="view_format" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header global_color">
				<h5 class="modal-title text-white" id="exampleModalLabel">Filename Convention</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body p-0">
				<img src="<?= base_url('assets/style/repo-format.png') ?>" class="img-fluid border shadow " alt="Filename convention">
			</div>
		</div>
	</div>
</div>

<div class="modal fade repo-chart pad-00 oy-hidden" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" data-backdrop="static" aria-hidden="true">
	<div class="modal-dialog modal-full mg-top-0">
		<div class="modal-content">
			<div class="modal-header bg-primary text-uppercase text-muted rounded-0 border-0">
				<h5 class="modal-title text-center text-white">Repository Chart</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="col-12 border">
						<div class="chart-container" style="position: relative;">
							<canvas height="2vh" width=10vw" id="region_chart"></canvas>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-6 border">
								<div class="chart-container" style="position: relative;">
									<canvas id="sites_chart"></canvas>
								</div>
							</div>
							<div class="col-6 border">
								<div class="col-12">
									<div class="chart-container" style="position: relative;">
										<canvas id="plan_chart"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		</div>
	</div>
</div>
