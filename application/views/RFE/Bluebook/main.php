<div class="container-fluid">
	<input type="hidden" id="geo" value="<?=$this->session->userdata('geoarea');?>">

	<div class="row">
		<div class="col-4 align-right">
			<div class="row">
				<div class="col-12">
					<div class="row">
						<div class="col-4 justify-content-end"></div>
						<div class="col-8 justify-content-end"></div>
					</div>
				</div>
			</div>
		</div>

		<br/>

		<div class="col-12" style="height: 77vh">
			<div class="crumbs"></div>
			<div class="header_div"></div>
			<div class="container-fluid">
				<div class="row folder d-flex"></div>
			</div>
		</div>

		<div class="col-12 text-center page_site mg-top-5">
			<div class="container-fluid border-top">
				<div class="row">
					<div class="col-3 text-left text-muted" style="padding: 5px"">
						OVERALL FILES: <b class="total-files"></b>
					</div>
					<div class="col-6">
						<div class="d-flex justify-content-center" id='folder_paginate'></div>
					</div>
					<div class="col-3 text-right text-muted" style="padding: 5px">
						USED STORAGE: <b class="storage"></b>
					</div>
				</div>
			</div>
		</div>
	</div>

<div class="modal fade" data-backdrop="static" id="upload_bb" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">
		<div class="modal-content">
			<div class="modal-header global_color">
				<div class="modal-title text-white display-5" id="exampleModalLabel">Upload Files&nbsp</div> <div class="sname text-white"></div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="dropzone pad-00" style="border: 1px dashed #0087F7;">
					<div class="dz-message" style="text-align: center;" >
						<p class="text-center lead h4 text-muted">Upload File <i>(accepts .pdf files only)</i></p>
						<a class="btn btn-primary btn-sm"  href="#" role="button">
							<i class="fa fa-upload" aria-hidden="true"></i> Click to Upload
						</a>
						<p class="text-center lead h5 text-muted mg-top-10">Or drag files here.</p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" data-backdrop="static" id="bb-add-folder" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		
			<div class="modal-content">
				<div class="modal-header global_color">
					<div class="modal-title text-white display-5" id="exampleModalLabel">Add New Folder</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form class="edit-accp">
					<div class="form-group">
						<input type="text" class="form-control" id="edit-file-name" aria-describedby="file-name" placeholder="Enter Folder Name" required autofocus>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn global_color text-white"><i class="fas fa-check"></i> Submit</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Close</button>
				</div>
				</form>	
			</div>
		
	</div>
</div>

