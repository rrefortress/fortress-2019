<div class="container-fluid mg-top-77">
	<input type="hidden" id="lvl" value="<?=$level ?>">
	<input type="hidden" id="geo" value="<?=$this->session->userdata('geoarea');?>">
	<input type="hidden" id="user_id" value="<?=$this->session->userdata('id');?>">

	<ul class="nav nav-tabs" id="myTab" role="tablist">
		<li class="nav-item" role="presentation">
			<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fas fa-broadcast-tower"></i> Tower</a>
		</li>
		<li class="nav-item" role="presentation">
			<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><i class="far fa-lightbulb"></i> Proposals</a>
		</li>
	</ul>

	<div class="tab-content" id="myTabContent">
		<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
			<?php require_once 'tower_page.php'; ?>
		</div>
		<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
			<?php require_once 'proposal_page.php'; ?>
		</div>
	</div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJiNdQZEwhcv8Hep7SF8qDo2SJDM4Ttdw" async defer></script>