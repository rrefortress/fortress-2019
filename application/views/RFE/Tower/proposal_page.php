<div class="row mg-top-10">
	<div class="col-6">
		<div class="btn-group btn-group-sm" role="group">
			<button type="button" class="btn btn-sm btn-outline-success existing"><i class="far fa-lightbulb"></i> UPLOAD EXISTING</button>
			<button type="button" class="btn btn-outline-warning proposal"><i class="fas fa-chart-pie"></i> UPLOAD PROPOSAL</button>
			<button type="button" class="btn btn-sm btn-outline-secondary" id="btnExport" onclick="fnExcelReport(this);"><i class="fas fa-download"></i> EXPORT</button>
			<iframe id="txtArea1" style="display:none"></iframe>
			<!-- <button id="btnExport" onclick="fnExcelReport();"> EXPORT </button> -->
			<!-- <button type="button" class="btn btn-sm btn-outline-danger clear_pro"><i class="fas fa-eraser"></i> CLEAR</button> -->
		</div>
	</div>
	<div class="col-4">
		<div class="row text-right">
			<div class="col-8 pr-2">
				<input type="text" name="Proposal Name" class="form-control form-control-sm" id="tower_proposal_name" placeholder="Proposal Name">
			</div>
			<div class="col-auto pl-0">
				<button type="button" class="btn btn-sm btn-outline-success save-proposal"><i class="far fa-save"></i> SAVE</button>
			</div>
		</div>
	</div>
	<div class="col-2 text-right">
		<button type="button" class="btn btn-sm btn-outline-primary view-proposal"><i class="far fa-eye"></i> VIEW PROPOSALS</button>
	</div>
</div>

<div class="col-12 mg-top-10 border shadow">
	<div id="pro_div" class="w-100 p-1" style="overflow-x: auto">
		<table id="proposal_tower_tbl" class="display nowrap text-center table-sm small table-responsive-sm table-hover table-striped table-bordered compact mt-3">
			<div class="col-12 mg-top-135 t-msg" style="height: 45vh;">
				<h5 class="text-center text-white">
					<i class="fas fa-table display-2 text-muted"></i></br>
					<small class="text-center text-muted mg-top-10"><em>Please upload file first to display proposal lists.</em></small>
				</h5>
			</div>
		</table>
	</div>
</div>


<div class="modal fade" data-backdrop="static" id="existing" tabindex="-1" aria-labelledby="existing" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header bg-success text-dark">
				<h5 class="modal-title text-white">UPLOAD EXISTING FILE</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="text-center">
					<div class=" pad-00" style="border: 1px dashed #0087F7;">
						<!-- <div class="dz-message" style="text-align: center;" > -->
							<!-- <div class="file-upload-wrapper">
							  <input type="file" id="input-file-now" class="file-upload" />
							</div> -->
							<div class="file-upload-wrapper">
							    Select a text file:
							    <input type="file" id="fileInput" />
							  </div>
							  <pre id="fileDisplayAreaexisting"></pre>
							  <pre id="csvData"></pre>
							 
							<!-- <p class="text-center lead h4 text-muted">Upload File <i>(accepts .CSV files only)</i></p>
							<a class="btn btn-primary btn-sm"  href="#" role="button">
								<i class="fa fa-upload" aria-hidden="true"></i> Click to Upload
							</a>
							<p class="text-center lead h5 text-muted mg-top-10">Or drag files here.</p> -->
						<!-- </div> -->
					</div>
				</div>
			</div>
			<div class="modal-footer">
				 <button class="btn btn-primary" onClick="upload_existing()">Upload</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" data-backdrop="static" id="proposal" tabindex="-1" aria-labelledby="proposal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header bg-warning text-dark">
				<h5 class="modal-title text-white">UPLOAD PROPOSAL FILE</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="text-center">
					<div class=" pad-00" style="border: 1px dashed #0087F7;">
						<!-- <div class="dz-message" style="text-align: center;" > -->
							<div class="file-upload-wrapper">
							    Select a text file:
							    <input type="file" id="fileInputProp" />
							</div>
							<pre id="fileDisplayAreaproposed"></pre>
							<pre id="csvData"></pre>
							
							<!-- <p class="text-center lead h4 text-muted">Upload File <i>(accepts .CSV files only)</i></p>
							<a class="btn btn-primary btn-sm"  href="#" role="button">
								<i class="fa fa-upload" aria-hidden="true"></i> Click to Upload
							</a>
							<p class="text-center lead h5 text-muted mg-top-10">Or drag files here.</p> -->
						<!-- </div> -->
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" onClick="upload_proposal()">Upload</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" data-backdrop="static" id="view-pro" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered modal-xl">
		<div class="modal-content">
			<div class="modal-header bg-warning">
				<h5 class="modal-title text-uppercase lead">Proposal Chart</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="pro_charts row"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Download</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" data-backdrop="static" id="save-pro" tabindex="-1">
	<div class="modal-dialog modal-sm modal-dialog-centered ">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Save Proposal</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form id="save_proposal">
				<div class="modal-body">
					<div class="form-group">
						<label for="save_name">Name</label>
						<input type="text" name="save_name" placeholder="Enter Name" class="form-control" id="save_name" aria-describedby="name" required>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" data-backdrop="static" id="view-pro-list" tabindex="-1">
	<div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
		<div class="modal-content">
			<div class="modal-header global_color">
				<h5 class="modal-title text-white text-uppercase">Proposal List</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="prop-lists"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger del_all">Delete All</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
