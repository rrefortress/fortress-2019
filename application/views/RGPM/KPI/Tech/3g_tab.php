<div class="col-12 pad-00 border-bottom">
	<div class="form-row container-fluid">
		<div class="col-12 pb-1">
			<div class="row">
				<div class="col-5">
					<a type="button" class="btn btn-sm btn-outline-primary kpi3g_upload" href="#"><i class="fas fa-upload"></i> UPLOAD</a>
					<a type="button" class="btn btn-sm btn-outline-warning" href="./Formats/KPI/KPI.rar"><i class="fas fa-file-csv"></i> DOWNLOAD FORMAT</a>
					<a type="button" class="btn btn-sm btn-outline-danger clear3g" href="#"><i class="fas fa-eraser"></i> DELETE</a>
					<a type="button" class="btn btn-sm btn-outline-secondary view_3g_calendar" href="#" data-toggle="tooltip" data-placement="top" title="CALENDAR"><i class="far fa-calendar-alt"></i></a>
				</div>
				<div class="col-7">
					<p class="small text-right pt-2 mb-0"><em><b>Note:</b> Upload file only accepts the standard filename <b>3G_DATE(YYYY-MM-DD)</b>, ex: <b>3G_2021-01-01</b>.</em></p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-12 pt-0 pl-0 pr-0">
	<div class="form-row">
		<div class="col">
			<div class="col-12 pl-0 pr-0 border-bottom">
				<ul class="nav nav-pills small nav-justified bg-white 3g_tab" id="trackTab" role="tablist">
					<?php foreach (array('voice', 'hsdpa') as $key => $row) {  ?>
						<li class="nav-item" role="presentation">
							<a class="nav-link small <?=$key === 0 ? 'active' : '';?>" id="<?=$row;?>-tab" data-toggle="tab" href="#<?=$row;?>" role="tab" aria-controls="<?=$row;?>" aria-selected="true"><i class="fas <?= strtoupper($row) === 'VOICE' ? 'fa-microphone-alt' : 'fa-satellite-dish' ?>"></i> <?=strtoupper($row);?></a>
						</li>
					<?php } ?>
				</ul>
			</div>

			<div class="tab-content" id="myTabContent">
				<?php foreach (array('voice', 'hsdpa') as $key => $row) {  ?>
					<div class="tab-pane fade <?=$key === 0 ? 'show ' : ' ';?> <?=$key === 0 ? 'active' : ' ';?>" id="<?=$row;?>" role="tabpanel" aria-labelledby="<?=$row;?>-tab">
						<div class="row">
							<div class="col-2 pr-0">
								<div class="nav flex-column nav-pills trackPills <?=$row;?>_tab" id="<?=$row;?>-pills-tab" role="tablist" aria-orientation="vertical">
									<?php foreach (strtoupper($row) === 'VOICE' ? array('csfr', 'dcr', 'sms') : array('csfr', 'dcr') as $keys => $rows) {  ?>
										<a class="nav-link <?=$keys === 0 ? 'active' : ' ';?>" id="<?=$row;?>-pills-<?=$rows;?>-tab" data-toggle="pill" href="#<?=$row;?>_<?=$rows;?>" role="tab" aria-controls="<?=$row;?>_<?=$rows;?>" aria-selected="true"><i class="fas <?= strtoupper($rows) === 'CSFR' ? 'fa-bezier-curve' : (strtoupper($rows) === 'DCR' ? 'fa-link' : 'fa-sms'); ?>"></i> <?=strtoupper($rows);?> <span class="float-right small badge badge-primary badge-pill text-right pull-right <?=$row;?>_<?=$rows;?>_total">0</span></a>
									<?php } ?>
								</div>
							</div>
							<div class="col-10 border-left p1" style="min-height: 65vh;">
								<div class="tab-content" id="<?=$row;?>-pills-tabContent">
									<?php foreach (strtoupper($row) === 'VOICE' ? array('csfr', 'dcr', 'sms') : array('csfr', 'dcr') as $keys => $rows) {  ?>
										<div class="tab-pane fade show <?=$keys === 0 ? 'active' : ' ';?>" id="<?=$row;?>_<?=$rows;?>" role="tabpanel" aria-labelledby="<?=$row;?>-pills-<?=$rows;?>-tab">
											<div class="col-12 pt-2 border-bottom">
												<div class="row">
													<div class="col-3 pl-0">
														<a class="btn btn-outline-success btn-sm export_<?=$row;?>_<?=$rows;?>_3g" type="button" href="#"><i class="fas fa-download"></i> EXPORT</a>
													</div>
													<div class="col-3">
														<div class="row">
															<p class="text-muted mt-1 small">TARGET KPI: </p>
															<div class="col">
																<div class="input-group input-group-sm">
																	<input type="number" class="form-control form-control-sm text-center" id="<?=$row;?>_<?=$rows;?>_3g" min="0" max="100000" value="0.45" step = "0.01">
																	<div class="input-group-append">
																		<span class="input-group-text">%</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="col-3"></div>
													<div class="col-3">
														<div class="input-group input-group-sm">
															<input type="text" class="form-control form-control-sm" placeholder="Select Date" value="<?= date('Y-m-d'); ?>" id="date<?=$row == 'voice' ? 'v' : 'h';?>_<?=$rows;?>_select">
															<div class="input-group-append">
															<span class="input-group-text">
																<a href="#" class="text-muted text-center" id="select_date"><i class="fas fa-calendar-alt"></i></a>
															</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="<?=$rows;?>_<?=$row;?>_tbl pr-1 loader_3g mt-1">CSFR</div>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
