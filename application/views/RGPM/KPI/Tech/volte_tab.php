<input type="hidden" class="volte_tab">

<div class="col-12 pad-00 border-bottom">
	<div class="form-row container-fluid">
		<div class="col-12 pb-1">
			<div class="row">
				<div class="col-4">
					<a type="button" class="btn btn-sm btn-outline-primary kpivolte_upload" href="#"><i class="fas fa-upload"></i> UPLOAD</a>
					<a type="button" class="btn btn-sm btn-outline-warning" href="./Formats/KPI/KPI.rar"><i class="fas fa-file-csv"></i> DOWNLOAD</a>
					<a type="button" class="btn btn-sm btn-outline-danger clearvolte" href="#"><i class="fas fa-eraser"></i> DELETE</a>
					<a type="button" class="btn btn-sm btn-outline-secondary view_volte_calendar" href="#" data-toggle="tooltip" data-placement="top" title="CALENDAR"><i class="far fa-calendar-alt"></i></a>
				</div>
				<div class="col-8">
					<p class="small text-right pt-2 mb-0"><em><b>Note:</b> Upload file only accepts the standard filename <b>VOLTE-(TDD/FDD)_DATE(YYYY-MM-DD)</b>, ex: <b>VOLTE-TDD_2021-01-01</b>.</em></p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-12 pt-0 pl-0 pr-0">
	<div class="form-row">
		<div class="col">
			<div class="col-12 pl-0 pr-0 border-bottom">
				<ul class="nav nav-pills small nav-justified bg-white volte_tab" id="trackTab" role="tablist">
					<?php foreach (array('tdd', 'fdd') as $key => $row) {  ?>
						<li class="nav-item" role="presentation">
							<a class="nav-link small <?=$key === 0 ? 'active' : '';?>" id="volte-<?=$row;?>-tab" data-toggle="tab" href="#volte-<?=$row;?>" role="tab" aria-controls="volte-<?=$row;?>" aria-selected="true"><i class="<?= strtoupper($row) === 'TDD' ? 'far fa-clock' : 'fas fa-wave-square'; ?>"></i> <?= strtoupper($row) === 'TDD' ? 'TIME' : 'FREQUENCY' ;?> DIVISION DUPLEX (<?=strtoupper($row);?>)</a>
						</li>
					<?php } ?>
				</ul>
			</div>

			<div class="tab-content" id="myTabContent">
				<?php foreach (array('tdd', 'fdd') as $key => $row) {  ?>
					<div class="tab-pane fade <?=$key === 0 ? 'show ' : ' ';?> <?=$key === 0 ? 'active' : '';?>" id="volte-<?=$row;?>" role="tabpanel" aria-labelledby="volte-<?=$row;?>-tab">
						<div class="row">
							<div class="col-2 pr-0">
								<div class="nav flex-column nav-pills trackPills volte-<?=$row;?>_tab" id="volte-<?=$row;?>-pills-tab" role="tablist" aria-orientation="vertical">
									<div class="col-12 text-center text-white bg-success text-uppercase small"><i class="fas fa-sort-amount-up"></i> Success Rate</div>
									<?php foreach (array('estab', 'erab', 'sdvcc') as $keys => $rows) {  ?>
										<a class="nav-link <?=$keys === 0 ? 'active' : '';?>" id="volte-<?=$row;?>-pills-<?=$rows;?>-tab" data-toggle="pill" href="#volte-<?=$row;?>_<?=$rows;?>" role="tab" aria-controls="volte-<?=$row;?>_<?=$rows;?>" aria-selected="true"><i class="fas <?= strtoupper($rows) === 'ESTAB' ? 'fa-chart-line' : (strtoupper($rows) === 'ERAB' ? 'fa-external-link-square-alt' : 'fa-paper-plane'); ?>"></i> <?=strtoupper($rows);?> <span class="float-right small badge badge-primary badge-pill text-right pull-right volte-<?=$row;?>_<?=$rows;?>_total">0</span></a>
									<?php } ?>
									<div class="col-12 text-center text-white bg-danger text-uppercase small"><i class="fas fa-sort-amount-down"></i> Failure Rate</div>
									<a class="nav-link" id="volte-<?=$row;?>-pills-dcr-tab" data-toggle="pill" href="#volte-<?=$row;?>_dcr" role="tab" aria-controls="volte-<?=$row;?>_dcr" aria-selected="false"><i class="fas fa-link"></i> DCR <span class="float-right small badge badge-primary badge-pill text-right pull-right volte-<?=$row;?>_dcr_total">0</span></a>
								</div>
							</div>
							<div class="col-10 border-left p1" style="min-height: 65vh;">
								<div class="tab-content" id="<?=$row;?>-pills-tabContent">
									<?php foreach (array('estab', 'erab', 'sdvcc', 'dcr') as $keys => $rows) {  ?>
										<div class="tab-pane fade <?=$keys === 0 ? 'show ' : ' ';?> <?=$keys === 0 ? 'active' : '';?>" id="volte-<?=$row;?>_<?=$rows;?>" role="tabpanel" aria-labelledby="volte-<?=$row;?>-pills-<?=$rows;?>-tab">
											<div class="col-12 pt-2 border-bottom">
												<div class="row">
													<div class="col-3 pl-0">
														<a class="btn btn-outline-success btn-sm volte-export_<?=$row;?>_<?=$rows;?>" type="button" href="#"><i class="fas fa-download"></i> EXPORT</a>
													</div>
													<div class="col-3">
														<div class="row">
															<p class="text-muted mt-1 small">TARGET KPI: </p>
															<div class="col">
																<div class="input-group input-group-sm">
																	<input type="number" class="form-control form-control-sm text-center" id="volte-target_<?=$row;?>_<?=$rows;?>" min="0" max="100000" value="99.4" step = "0.01">
																	<div class="input-group-append">
																		<span class="input-group-text">%</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="col-3"></div>
													<div class="col-3">
														<div class="input-group input-group-sm">
															<input type="text" class="form-control form-control-sm" placeholder="Select Date" value="<?= date('Y-m-d'); ?>" id="volte-date_<?=$row;?>_<?=$rows;?>_select">
															<div class="input-group-append">
															<span class="input-group-text">
																<a href="#" class="text-muted text-center" id="select_date"><i class="fas fa-calendar-alt"></i></a>
															</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="volte-<?=$row;?>_<?=$rows;?>_tbl volte-loader_<?=$row;?> loader_volte pr-1 mt-1"></div>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
