<div class="col-12 pad-00 border-bottom">
	<div class="form-row container-fluid">
		<div class="col-12 pb-1">
			<div class="row">
				<div class="col-5">
					<a type="button" class="btn btn-sm btn-outline-primary kpi2g_upload" href="#"><i class="fas fa-upload"></i> UPLOAD</a>
					<a type="button" class="btn btn-sm btn-outline-warning" href="./Formats/KPI/KPI.rar"><i class="fas fa-file-csv"></i> DOWNLOAD FORMAT</a>
					<a type="button" class="btn btn-sm btn-outline-danger clear2g" href="#"><i class="fas fa-eraser"></i> DELETE</a>
					<a type="button" class="btn btn-sm btn-outline-secondary view_2g_calendar" href="#" data-toggle="tooltip" data-placement="top" title="CALENDAR"><i class="far fa-calendar-alt"></i></a>
				</div>
				<div class="col-7">
					<p class="small text-right pt-2 mb-0"><em><b>Note:</b> Upload file only accepts the standard filename <b>2G_DATE(YYYY-MM-DD)</b>, ex: <b>2G_2021-01-01</b>.</em></p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-12 pt-0 pl-0 pr-0">
	<div class="form-row">
		<div class="col-2 pr-0">
			<div class="nav flex-column nav-pills trackPills 2g_tab" id="2g-pills-tab" role="tablist" aria-orientation="vertical">
				<?php foreach (array('csfr', 'dcr', 'iafr') as $key => $row) {  ?>
					<a class="nav-link  <?=$key === 0 ? 'active' : '';?>" id="2g-pills-'<?=$row;?>'-tab" data-toggle="pill" href="#<?=$row;?>" role="tab" aria-controls="<?=$row;?>" aria-selected="true"><i class="fas <?= strtoupper($row) === 'CSFR' ? 'fa-bezier-curve' : (strtoupper($row) === 'DCR' ? 'fa-link' : 'fa-phone-volume') ?> "></i> <?=strtoupper($row);?> <span class="float-right small badge badge-primary badge-pill text-right pull-right <?=$row;?>_2g_total">0</span></a>
				<?php } ?>
			</div>
		</div>
		<div class="col-10 border-left p1" style="min-height: 65vh;">
			<div class="tab-content" id="2g-pills-tabContent">
				<?php foreach (array('csfr', 'dcr', 'iafr') as $key => $row) {  ?>
				<div class="tab-pane fade <?=$key === 0 ? 'show ' : ' ';?> <?=$key === 0 ? 'active' : ' ';?>" id="<?=$row;?>" role="tabpanel" aria-labelledby="2g-pills-<?=$row;?>-tab">
					<div class="col-12 pt-2 border-bottom">
						<div class="row">
							<div class="col-3 pl-0">
								<a class="btn btn-outline-success btn-sm export_<?=$row;?>_2g" type="button" href="#"><i class="fas fa-download"></i> EXPORT</a>
							</div>
							<div class="col-3">
								<div class="row">
									<p class="text-muted mt-1 small">TARGET KPI: </p>
									<div class="col">
										<div class="input-group input-group-sm">
											<input type="number" class="form-control form-control-sm text-center" id="target_<?=$row;?>_2g" min="0" max="100000" value="1.1" step = "0.01">
											<div class="input-group-append">
												<span class="input-group-text">%</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-3"></div>
							<div class="col-3">
								<div class="input-group input-group-sm">
									<input type="text" class="form-control form-control-sm" placeholder="Select Date" value="<?= date('Y-m-d'); ?>" id="date2g_<?=$row;?>_select">
									<div class="input-group-append">
										<span class="input-group-text">
											<a href="#" class="text-muted text-center" id="select_date"><i class="fas fa-calendar-alt"></i></a>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="<?=$row;?>_2g_tbl pr-1 loader_2g mt-1"></div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

