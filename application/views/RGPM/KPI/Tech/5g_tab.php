<div class="col-12 pad-00 border-bottom">
	<div class="form-row container-fluid">
		<div class="col-12 pb-1">
			<div class="row">
				<div class="col-5">
					<a type="button" class="btn btn-sm btn-outline-primary kpi5g_upload" href="#"><i class="fas fa-upload"></i> UPLOAD</a>
					<a type="button" class="btn btn-sm btn-outline-warning" href="./Formats/KPI/KPI.rar"><i class="fas fa-file-csv"></i> DOWNLOAD FORMAT</a>
					<a type="button" class="btn btn-sm btn-outline-danger clear5g" href="#"><i class="fas fa-eraser"></i> DELETE</a>
					<a type="button" class="btn btn-sm btn-outline-secondary view_5g_calendar" href="#" data-toggle="tooltip" data-placement="top" title="CALENDAR"><i class="far fa-calendar-alt"></i></a>
				</div>
				<div class="col-7">
					<p class="small text-right pt-2 mb-0"><em><b>Note:</b> Upload file only accepts the standard filename <b>5G_DATE(YYYY-MM-DD)</b>, ex: <b>5G_2021-01-01</b>.</em></p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-12 pt-0 pl-0 pr-0">
	<div class="form-row">
		<div class="col-3 pr-0">
			<div class="nav flex-column nav-pills trackPills 5g_tab" id="5g-pills-tab" role="tablist" aria-orientation="vertical">
				<?php foreach ($this->config->item('5g-tabs') as $key => $row) {  ?>
					<?php if($key === 0) { ?>
						<div class="col-12 text-center text-white bg-success text-uppercase small"><i class="fas fa-sort-amount-up"></i>SGNB Success Rate</div>
					<?php } else if($key === 2) { ?>
						<div class="col-12 text-center text-white bg-danger text-uppercase small"><i class="fas fa-sort-amount-down"></i>SGNB Failure Rate</div>
					<?php } ?>

					<a class="nav-link  <?=$key === 0 ? 'active' : '';?>" id="5g-pills-'<?=$row['name'];?>'-tab" data-toggle="pill" href="#<?=$row['name'];?>" role="tab" aria-controls="<?=$row['name'];?>" aria-selected="true"><?=strtoupper($row['title']);?> <span class="float-right small badge badge-primary badge-pill text-right pull-right <?=$row['name'];?>_5g_total">0</span></a>
				<?php } ?>
			</div>
		</div>
		<div class="col-9 border-left p1" style="min-height: 65vh;">
			<div class="tab-content" id="5g-pills-tabContent">
				<?php foreach ($this->config->item('5g-tabs') as $key => $row) {  ?>
					<div class="tab-pane fade <?=$key === 0 ? 'show ' : ' ';?> <?=$key === 0 ? 'active' : ' ';?>" id="<?=$row['name'];?>" role="tabpanel" aria-labelledby="5g-pills-<?=$row['name'];?>-tab">
						<div class="col-12 pt-2 border-bottom">
							<div class="row">
								<div class="col-3 pl-0">
									<a class="btn btn-outline-success btn-sm export_<?=$row['name'];?>_5g" type="button" href="#"><i class="fas fa-download"></i> EXPORT</a>
								</div>
								<div class="col-3">
									<div class="row">
										<p class="text-muted mt-1 small">TARGET KPI: </p>
										<div class="col">
											<div class="input-group input-group-sm">
												<input type="number" class="form-control form-control-sm text-center" id="target_<?=$row['name'];?>_5g" min="0" max="100000" value="1.1" step = "0.01">
												<div class="input-group-append">
													<span class="input-group-text">%</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-3"></div>
								<div class="col-3">
									<div class="input-group input-group-sm">
										<input type="text" class="form-control form-control-sm" placeholder="Select Date" value="<?= date('Y-m-d'); ?>" id="date5g_<?=$row['name'];?>_select">
										<div class="input-group-append">
										<span class="input-group-text">
											<a href="#" class="text-muted text-center" id="select_date"><i class="fas fa-calendar-alt"></i></a>
										</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="<?=$row['name'];?>_5g_tbl pr-1 loader_5g mt-1"></div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

