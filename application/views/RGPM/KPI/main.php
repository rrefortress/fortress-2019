<input type="hidden" id="ktype" value="<?= strtoupper($title); ?>">
<ul class="nav nav-pills small nav-justified bg-white shadow-sm mg-top-10 border border-top border-bottom-0 border-left-0 border-right-0 tech_tab" id="trackTab" role="tablist">
	<?php foreach (array('2G', '3G', '4G', 'volte', '5G') as $key => $row) {  ?>
		<li class="nav-item" role="presentation">
			<a class="nav-link <?=$key === 0 ? 'active' : '';?>" id="K<?=strtoupper($row);?>-tab" data-toggle="tab" href="#K<?=strtoupper($row);?>" role="tab" aria-controls="K<?=$row;?>" aria-selected="true"><?=strtoupper($row);?></a>
		</li>
	<?php } ?>
</ul>
<div class="tab-content pt-1 shadow-sm border" id="myTabContent">
	<?php foreach (array('K2G', 'K3G', 'K4G', 'KVOLTE', 'K5G') as $key=>$row) { ?>
		<div class="tab-pane fade show <?=$key == 0 ? 'active' : '';?>" id="<?=$row?>" role="tabpanel" aria-labelledby="<?=$row?>-tab">
			<?php
			switch ($row) {
				case 'K2G': $tab = '2g'; break;
				case 'K3G': $tab = '3g'; break;
				case 'K4G': $tab = '4g'; break;
				case 'KVOLTE': $tab = 'volte'; break;
				case 'K5G': $tab = '5g'; break;
			}
			require_once 'Tech/'.strtolower($tab).'_tab.php';
			?>
		</div>
	<?php } ?>
</div>

<?php foreach (array('2g', '3g', '4g', 'volte', '5g') as $row) {  ?>
	<div class="modal fade" data-backdrop="static" id="kpi<?=$row;?>_upload" tabindex="-1" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header global_color text-white">
					<h5 class="modal-title text-white">UPLOAD FILE FOR <?=strtoupper($row);?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="text-center">
						<div class="dropzone dropzone<?=$row;?>_KPI pad-00" style="border: 1px dashed #0087F7;">
							<div class="dz-message" style="text-align: center;" >
								<p class="text-center lead h4 text-muted">Upload File <i>(accepts .CSV file only)</i></p>
								<a class="btn btn-primary btn-sm" href="#" role="button">
									<i class="fa fa-upload" aria-hidden="true"></i> Click to Upload
								</a>
								<p class="text-center lead h5 text-muted mg-top-10">Or drag files here.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade kpi-<?=$row;?>-sched pad-00 oy-hidden" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" data-backdrop="static" aria-hidden="true">
		<div class="modal-dialog modal-full mg-top-0">
			<div class="modal-content">
				<div class="modal-header text-uppercase text-muted rounded-0 border-0">
					<h5 class="modal-title text-center" id="exampleModalLabel"><i class="fas fa-clipboard-list"></i> Date Uploads</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col loaders d-none pt-5">
							<div class="col-12 mg-top-100">
								<h5 class="text-center text-white">
									<img src="style/Spin-1s-200px.svg" width="150" height="150" class="rounded-circle" alt="Loader"><br>
									<small class="text-center text-muted pt-4"><em>Loading data...</em></small>
								</h5>
							</div>
						</div>
						<div class="col-12">
							<div class='kpi-calendar'></div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>

	<div class="modal fade view-<?=$row;?>-analyzer pad-00 oy-hidden" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" data-backdrop="static" aria-hidden="true">
		<div class="modal-dialog modal-full mg-top-0">
			<div class="modal-content">
				<div class="modal-header text-uppercase global_color text-muted rounded-0 border-0">
					<h5 class="modal-title text-white ana-cell-name"> </h5>
					<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body p-0">
					<div class="col-12 pt-0 border shadow-sm" style="max-height: 85vh;overflow-x: hidden;">
						<?php if ($row === '2g') { ?>
							<div class="col-12">
								<div class="form-row">
									<?php foreach (array('time', 'date') as $key => $row) {  ?>
										<div class="col-6">
											<select name="<?=$row;?>-2g[]" data-type="<?=$row;?>" data-tech="2g" multiple id="<?=$row;?>-2g" class="form-control"></select>
										</div>
									<?php } ?>
								</div>
							</div>
							<div class="col-12 p-0">
								<div class="row">
									<div class="col-12 bg-light">
										<div class="row">
											<?php foreach ($this->config->item('2g-1-report') as $row) { ?>
												<div class="col-4 chart-loader border p-2 pr-0">
													<div class="c2g-dis d-none">
														<div class="chart-container">
															<canvas id="<?=$row['name'];?>-wcl2g_chart"></canvas>
														</div>
														<div class="text-center text-uppercase border border-primary small p-0 font-weight-bold rounded shadow"><?=$row['title'];?></div>
													</div>
												</div>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-12 p-0">
								<div class="row">
									<?php foreach ($this->config->item('2g-2-report') as $row) { ?>
										<div class="bg-light col-<?= in_array($row['name'], array('tch', 'cb','dlb','ulb','ta','sc')) ? 6 : ($row['name'] == 'tcdc' ? 6 : 4);?> chart-2g chart-loader border p-2">
											<div class="c2g-dis d-none">
												<div class="chart-container">
													<canvas id="<?=$row['name'];?>-wcl2g_chart"></canvas>
												</div>
												<div class="text-center text-uppercase border border-primary p-0 font-weight-bold small lead rounded shadow"><?=$row['title'];?></div>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
						<?php } else if ($row === '3g') { ?>
							<div class="col-12">
								<div class="form-row">
									<?php foreach (array('time', 'date') as $key => $row) {  ?>
										<div class="col-6">
											<select name="<?=$row;?>-3g[]" data-type="<?=$row;?>" data-tech="3g" multiple id="<?=$row;?>-3g" class="form-control"></select>
										</div>
									<?php } ?>
								</div>
							</div>
							<div class="col-12 p-0">
								<div class="row">
									<div class="col-12 bg-light">
										<div class="row">
											<?php foreach ($this->config->item('3g-1-report') as $row) { ?>
												<div class="col-6 chart-loader border p-2 pr-0">
													<div class="c3g-dis d-none">
														<div class="chart-container">
															<canvas id="<?=$row['name'];?>-wcl3g_chart"></canvas>
														</div>
														<div class="text-center text-uppercase border border-primary small p-0 font-weight-bold rounded shadow"><?=$row['title'];?></div>
													</div>
												</div>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-12 p-0">
								<div class="row">
									<?php foreach ($this->config->item('3g-2-report') as $row) { ?>
										<div class="bg-light col-<?= in_array($row['name'], array('avail','amr_trf','dch')) ? 4 : 6;?>  chart-loader border p-2">
											<div class="c3g-dis d-none">
												<div class="chart-container">
													<canvas id="<?=$row['name'];?>-wcl3g_chart"></canvas>
												</div>
												<div class="text-center text-uppercase border border-primary small p-0 font-weight-bold rounded shadow"><?=$row['title'];?></div>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
						<?php } else if ($row === '4g') { ?>
							<div class="col-12">
								<div class="form-row">
									<?php foreach (array('time', 'date') as $key => $row) {  ?>
										<div class="col-6">
											<select name="<?=$row;?>-4g[]" data-type="<?=$row;?>" data-tech="4g" multiple id="<?=$row;?>-4g" class="form-control"></select>
										</div>
									<?php } ?>
								</div>
							</div>
							<div class="col-12 p-0">
								<div class="row">
									<div class="col-12 bg-light">
										<div class="row">
											<?php foreach ($this->config->item('4g-report') as $row) { ?>
												<div class="col-6 chart-loader border p-2 pr-0">
													<div class="c4g-dis d-none">
														<div class="chart-container">
															<canvas id="<?=$row['name'];?>-wcl4g_chart"></canvas>
														</div>
														<div class="text-center text-uppercase border border-primary small p-0 font-weight-bold rounded shadow"><?=$row['title'];?></div>
													</div>
												</div>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
						<?php } else if ($row === 'volte') { ?>

						<?php } else if ($row === '5g') { ?>
							<div class="col-12">
								<div class="form-row">
									<?php foreach (array('time', 'date') as $key => $row) {  ?>
										<div class="col-6">
											<select name="<?=$row;?>-5g[]" data-type="<?=$row;?>" data-tech="5g" multiple id="<?=$row;?>-5g" class="form-control"></select>
										</div>
									<?php } ?>
								</div>
							</div>
							<div class="col-12 p-0">
								<div class="row">
									<div class="col-12 bg-light">
										<div class="row">
											<?php foreach ($this->config->item('5g-1-report') as $row) { ?>
												<div class="col-6 chart-loader border p-2 pr-0">
													<div class="c5g-dis d-none">
														<div class="chart-container">
															<canvas id="<?=$row['name'];?>-wcl5g_chart"></canvas>
														</div>
														<div class="text-center text-uppercase border border-primary small p-0 font-weight-bold rounded shadow"><?=$row['title'];?></div>
													</div>
												</div>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
<?php } ?>
