<section class="riggers-step-container bg-secodary">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-12">
                    <div class="form-group text-center">
                        <div class="text-center">
                            <img src="<?= base_url('assets/style/FortressBig.png') ?>" class="rounded img-fluid login-h-50" alt="Responsive image">
                        </div>
                    </div>
                </div> 
                <div class="col-md-4">
                    <div class="wizard">
                        <div class="wizard-inner">
                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" aria-expanded="true"><span class="round-tab">1 </span> <i>Step 1</i></a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" aria-expanded="false"><span class="round-tab">2</span> <i>Step 2</i></a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab"><span class="round-tab">3</span> <i>Step 3</i></a>
                                </li>
                            </ul>
                        </div>
        
                        <form method="post" id="audit_form" enctype="multipart/form-data">
                            <div class="tab-content">
                                 <!-- STEP 1 -->
                                <div class="tab-pane active" role="tabpanel" id="step1">
                                    <h4 class="text-center small lead">Site Details</h4>
                                    <hr>
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="text-muted text-uppercase">REGION <b class="text-danger">*</b></label>
                                                <select name="region" class="custom-select custom-select-lg text-center rounded-pill border border-secondary" id="region">
                                                    <option value="" selected="selected" disabled>Select Region</option>
                                                    <option value="VIS">Visayas</option>
                                                    <option value="MIN">Mindanao</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="text-muted">PLAID <b class="text-danger">*</b></label> 
                                                <input class="form-control border border-secondary form-control-lg rounded-pill" id="plaid" type="number" name="plaid" placeholder="Enter PLAID" autocomplete="off" autofocus> 
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="text-muted">SITENAME <b class="text-danger">*</b></label> 
                                                <input class="form-control border border-secondary form-control-lg rounded-pill" id="sname" type="text" name="sname" placeholder="Enter Sitename" autocomplete="off"> 
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="list-inline text-center">
                                        <li><button type="button" class="next-step btn btn-outline-success btn-lg rounded-pill stg1" disabled>NEXT <i class="fas fa-angle-right"></i></button></li>
                                    </ul>
                                </div>

                                <!-- STEP 2 -->
                                <div class="tab-pane" role="tabpanel" id="step2">
                                    <h4 class="text-center lead small">Azimuth & Tilting</h4>
                                    <hr>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="text-muted text-uppercase">Technology <b class="text-danger">*</b></label> 
                                                    <select class="tech" name="tech[]" multiple="multiple">
                                                        <optgroup label="2G">
                                                            <option value="G9">G900</option>
                                                            <option value="G18">G1800</option>
                                                        </optgroup>
                                                        <optgroup label="3G">
                                                            <option value="U9">U900</option>
                                                            <option value="U21">U2100</option>
                                                        </optgroup>   
                                                        <optgroup label="4G/LTE">
                                                            <option value="L7">L700</option>
                                                            <option value="L9">L900</option>
                                                            <option value="L18">L1800</option>
                                                            <option value="L21">L2100</option>
                                                            <option value="L23">L2300</option>
                                                            <option value="L26">L2600</option>
                                                            <option value="L26MM">L26MIMO</option>
                                                        </optgroup>  
                                                        <optgroup label="5G">
                                                            <option value="NR2G">NR2600</option>
                                                            <option value="NR35">NR3500</option>
                                                            <option value="NR7">NR700</option>
                                                        </optgroup>     
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="text-muted text-uppercase">Height <b class="text-danger">*</b></label> 
                                                    <input class="form-control border border-secondary rounded-pill height" type="number" name="height" placeholder="Enter Height" min="0" maxlength="2" oninput="this.value=this.value.slice(0,this.maxLength||0/0);this.value=(this.value   < 1) ? (0/0) : this.value;" autocomplete="off"> 
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="text-muted text-uppercase">Dual Beam <b class="text-danger">*</b></label> 
                                                    <select name="db" class="custom-select text-center rounded-pill border border-secondary" id="db">
                                                        <option selected="selected">NO</option>
                                                        <option>YES</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="text-muted text-uppercase">VENDOR <b class="text-danger">*</b></label> 
                                                    <select name="vendor" class="custom-select text-center rounded-pill border border-secondary" id="vendor">
                                                        <option selected="selected" value="NSB">NOKIA</option>
                                                        <option value="HT">HUAWEI</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="text-muted text-uppercase">No. of Sectors <b class="text-danger">*</b></label>
                                                    <select name="sectors" class="custom-select text-center rounded-pill border border-secondary" id="sectors">
                                                        <option selected="selected" disabled>Select number of sectors</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="accordion col-md-12 mb-10" id="sectoring"></div>        
                                    </div>

                                    <br>
                                    <ul class="list-inline text-center">
                                        <li><button type="button" class="prev-step btn btn-outline-danger btn-lg rounded-pill"><i class="fas fa-angle-left"></i> Back</button></li>
                                        <li><button type="button" class="next-step btn btn-outline-success btn-lg rounded-pill stg2" disabled>Next <i class="fas fa-angle-right"></i></button></li>
                                    </ul>
                                </div>

                                <!-- STEP 3 -->
                                <div class="tab-pane" role="tabpanel" id="step3">
                                    <h4 class="text-center lead small">Capture Image</h4>
                                    <hr>
                                     
                                    <div class="row">
                                        <?php for ($x = 1; $x <= 4; $x++) { ?>
                                            <div class="col-md-12 s<?=$x?> mt-2 <?=$x > 1 ? ' d-none' : '';?>">
                                                <label class="text-muted text-uppercase"><i class="fas fa-camera"></i> Capture file for sector <?= $x; ?> <i class="text-mute small">(Optional)</i></label>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="capture-<?=$x?>" id="capture-<?=$x?>">
                                                    <label class="custom-file-label cap-audit border border-secondary" for="capture-<?=$x?>" name="capture-<?=$x?>" id="capture-<?=$x?>" accept="image/*" capture>Tap here...</label>
                                                </div>
                                                <img id="img-<?=$x?>" class="img-fluid img-thumbnail mt-2 d-none" alt="Capture image">
                                            </div>  
                                            <hr>       
                                        <?php } ?>
                                    </div>
                                    <hr>
                                    
                                    <ul class="list-inline text-center">
                                        <li><button type="button" class="prev-step btn btn-outline-danger btn-lg rounded-pill btn-back"><i class="fas fa-angle-left"></i> Back</button></li>
                                        <li><button type="submit" class="next-step btn btn-outline-success btn-lg rounded-pill btn-audit">Save</button></li>
                                    </ul>
                                </div>
                               
                                <div class="clearfix"></div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>