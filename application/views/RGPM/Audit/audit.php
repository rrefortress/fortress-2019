<div class="col-12">
	<div class="row">
		<div class="col-7 pl-0">	
			<button type="button" class="btn btn-sm btn-outline-secondary rounded-pill export_audit"><i class="fas fa-download"></i> EXPORT</button>
			<!-- <div class="btn-group mg-top-10 btn-group-sm" role="group"> -->
			<button type="button" class="btn btn-sm btn-outline-primary rounded-pill audit-site" disabled><i class="fas fa-eye"></i> VIEW SITE</button>
			<button type="button" class="btn btn-sm btn-outline-danger rounded-pill del-site" disabled><i class="fas fa-eye"></i> DELETE SITE</button>
            <!-- </div> -->
		</div>
	</div>
</div>

<div class="col-12 mg-top-5 border shadow">
   <div class="mb-2"></div>
	<div id="maindiv" class="w-100 p1">
         
	</div>
</div>

<div class="modal fade pad-00" data-backdrop="static" id="audit-profile" tabindex="-1" aria-labelledby="audit-profile" aria-hidden="true">
	<div class="modal-dialog modal-full modal-dialog-centered modal-dialog-scrollable">
		<div class="modal-content">
			<div class="modal-header global_color text-white">
				<h5 class="modal-title text-white lead text-uppercase"><i class="fas fa-drafting-compass"></i> Site Profile</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body p-0">
                    <div class="container-fluid">
                        <div class="row">
                                <div class="col-4 border-right border-secondary">

                                 <table class="table table-bordered nowrap tabler-hover small mt-2">
                                    <tbody>
                                        <tr>
                                            <td style="width:40%" class="global_color text-white text-right" scope="row">PLAID</td>
                                            <td id="plaid"></td>
                                        </tr>
                                        <tr>
                                            <td style="width:40%" class="global_color text-white text-right" scope="row">SITENAME</td>
                                            <td id="sitename"></th>
                                        </tr>
                                       
                                        <tr>
                                            <td style="width:40%" class="global_color text-white text-right" scope="row">HEIGHT</td>
                                            <td id="height"></td>
                                        </tr>

                                        <tr>
                                            <td style="width:40%" class="global_color text-white text-right" scope="row">NO. OF SECTORS</td>
                                            <td id="sectors"></th>
                                        </tr>

                                        <tr>
                                            <td style="width:40%" class="global_color text-white text-right" scope="row">TECH</td>
                                            <td id="tech"></td>
                                        </tr>
                                    
                                        <tr>
                                            <td style="width:40%" class="global_color text-white text-right" scope="row">AZIMUTH</td>
                                            <td id="azimuth"></th>
                                        </tr>
                                        <tr>
                                            <td style="width:40%" class="global_color text-white text-right" scope="row">M-TILT</th>
                                            <td id="mtilt"></td>
                                        </tr>
                                        <tr>
                                            <td style="width:40%" class="global_color text-white text-right" scope="row">E-TILT</th>
                                            <td id="etilt"></td>
                                        </tr>
                                        <tr>
                                            <td style="width:40%" class="global_color text-white text-right" scope="row">DUAL BEAM</td>
                                            <td id="dualbeam"></td>
                                        </tr>
                                        <tr>
                                            <td style="width:40%" class="global_color text-white text-right" scope="row">ANTENNA MODEL</td>
                                            <td id="antmodel"></td>
                                        </tr>
                                    </tbody>
                                </table>

                                </div>

                                <div class="col-8">
                                        <div class="row text-center text-center aud-img">
                                
                                        </div>
                                </div>
                        </div>

                    </div>
				
              
			</div>
            <div class="modal-footer border border-top border-secondary">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
		</div>
	</div>
</div>