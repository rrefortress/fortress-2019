<input type="hidden" id="ktype" value="<?= strtoupper($title); ?>">
<ul class="nav nav-pills small nav-justified bg-white shadow-sm mg-top-10 border border-top border-bottom-0 border-left-0 border-right-0 tech_tab" id="trackTab" role="tablist">
	<?php foreach (array('2G', '3G', '4G', 'VOLTE', '5G') as $key => $row) {  ?>
		<li class="nav-item" role="presentation">
			<a class="nav-link <?=$key === 0 ? 'active' : '';?>" id="K<?=$row;?>-tab" data-toggle="tab" href="#K<?=$row;?>" role="tab" aria-controls="K<?=$row;?>" aria-selected="true"><?=$row;?></a>
		</li>
	<?php } ?>
</ul>
<div class="tab-content pt-1 shadow-sm border" id="myTabContent">

	<?php foreach (array('K2G', 'K3G', 'K4G', 'KVOLTE', 'K5G') as $key=>$row) { ?>
		<div class="tab-pane fade show <?=$key == 0 ? 'active' : '';?>" id="<?=$row?>" role="tabpanel" aria-labelledby="<?=$row?>-tab">
			<?php
			switch ($row) {
				case 'K2G': $tab = '2g'; break;
				case 'K3G': $tab = '3g'; break;
				case 'K4G': $tab = '4g'; break;
				case 'KVOLTE': $tab = 'volte'; break;
				case 'K5G': $tab = '5g'; break;
			}
			require_once 'Tech/'.strtolower($tab).'_tab.php';
			?>
		</div>
	<?php } ?>
</div>

<?php foreach (array('2g', '3g', '4g', 'volte', '5g') as $row) {  ?>
	<div class="modal fade" data-backdrop="static" id="kpi<?=$row;?>_upload" tabindex="-1" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header global_color text-white">
					<h5 class="modal-title text-white">UPLOAD FILE FOR <?=strtoupper($row);?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="text-center">
						<div class="dropzone dropzone<?=$row;?>_KPI pad-00" style="border: 1px dashed #0087F7;">
							<div class="dz-message" style="text-align: center;" >
								<p class="text-center lead h4 text-muted">Upload File <i>(accepts .CSV file only)</i></p>
								<a class="btn btn-primary btn-sm" href="#" role="button">
									<i class="fa fa-upload" aria-hidden="true"></i> Click to Upload
								</a>
								<p class="text-center lead h5 text-muted mg-top-10">Or drag files here.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

<?php } ?>
