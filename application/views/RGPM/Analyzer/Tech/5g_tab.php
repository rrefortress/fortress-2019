<div class="col-12 pad-00 border-bottom">
	<div class="form-row container-fluid">
		<div class="col-12 pb-1 bg-light">
			<div class="row">
				<div class="col-3 pl-1 pr-1">
					<a type="button" class="btn btn-sm btn-outline-primary kpi5g_upload" href="#"><i class="fas fa-upload"></i> UPLOAD</a>
					<a type="button" class="btn btn-sm btn-outline-warning" href="./Formats/5gKPI.csv"><i class="fas fa-file-csv"></i> FORMAT</a>
				</div>
				<div class="col-7">
					<div class="form-row">
						<?php foreach (array('cell', 'time', 'date') as $key => $row) {  ?>
							<div class="col-4">
								<select name="<?=$row;?>-5g[]" data-type="<?=$row;?>" data-tech="5g" multiple id="<?=$row;?>-5g" class="form-control"></select>
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="col-2">
					<p class="small text-right pt-2 mb-0"><em><b>5G KPI ANALYZER</b></em></p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-12 pt-0 pl-0 pr-0 overflow-auto" style="max-height: 68vh;">
	<div class="container-fluid">
		<div class="col-12 p-0">
			<div class="row">
				<div class="col-12 bg-light">
					<div class="row">
						<?php foreach ($this->config->item('5g-1-report') as $row) { ?>
							<div class="col-6 chart-loader border p-2 pr-0">
								<div class="c5g-dis d-none">
									<div class="chart-container">
										<canvas id="<?=$row['name'];?>_chart"></canvas>
									</div>
									<div class="text-center text-uppercase border border-primary small p-0 font-weight-bold rounded shadow"><?=$row['title'];?></div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
