<div class="col-12">
    <div class="row">
        <div class="col-10">
            <button type="button" class="btn btn-sm btn-outline-primary rounded-pill upload"><i class="fas fa-upload"></i> Upload</button>
            <button type="button" class="btn btn-sm btn-outline-success export_ssv rounded-pill"><i class="fas fa-download"></i> Export</button>    
            <a type="button" class="btn btn-sm btn-outline-warning rounded-pill" href="./Formats/SSV_TRACKER_<?= strtoupper($this->session->userdata('geoarea'))?>.csv"><i class="fas fa-file-csv"></i> DOWNLOAD FORMAT</a>  
        </div>
        <div class="col-2">
            <select class="custom-select mx-sm-2" id="region">
                <option value="all" selected>All</option>
                <option value="vis">Visayas</option>
                <option value="min">Mindanao</option>
            </select>
        </div>
    </div>	
</div>

<div class="col-12 mg-top-5 border bg-dark shadow">
   <div class="mb-2"></div>
   <div class="container-fluid">
        <div class="row">
            <div class="col-6 p-2"> 
                <h6 class="text-warning teal text-center"><i class="fas fa-crosshairs"></i> SSV ACCEPTANCE SCOREBOARD</h6>
                    <table class="table table-bordered text-center small table-sm table-striped table-dark">
                        <thead>
                            <tr>
                                <th scope="col" class="small align-middle">Sub-Tracks</th>
                                <th scope="col" class="small align-middle">EXPANSION </br>(absolute)</th>
                                <th scope="col" class="small align-middle">EXPANSION </br> (21 and 8 days SLA percentage)</th>
                                <th scope="col" class="small align-middle">NSB </br> (absolute)</th>
                                <th scope="col" class="small align-middle">NSB </br> (21 and 8 days SLA percentage)</th>
                            </tr>
                        </thead>
                        <tbody class="small">
                            <tr>
                                <th rowspan="2" class="align-middle" scope="row">TRFS to SSV Submission</th>
                                <td class="trfs-exp-ab">0%</td>
                                <td class="trfs-sla-perc">0%</td>
                                <td class="trfs1-nsb-ab">0%</td>
                                <td class="trfs-nsb-sla">0%</td>
                            </tr>
                            <tr>
                                <td class="trfs-remarks"></td>
                                <td><i class="shadow far fa-circle text-danger bg-danger rounded-circle"></i></td>
                                <td class="trfs1-remarks"></td>
                                <td><i class="shadow far fa-circle text-danger bg-danger rounded-circle"></i></td>
                            </tr>
                            <tr>
                                <th rowspan="2" class="align-middle" scope="row">SSV Submission to </br>SSV approval</th>
                                <td class="sub-exp-ab">0%</td>
                                <td class="sub-sla-perc">0%</td>
                                <td class="sub1-nsb-ab">0%</td>
                                <td class="sub-nsb-sla">0%</td>
                            </tr>
                            <tr>
                                <td class="sub-remarks"></td>
                                <td><i class="shadow far fa-circle text-danger bg-danger rounded-circle"></i></td>
                                <td class="sub1-remarks"></td>
                                <td><i class="shadow far fa-circle text-danger bg-danger rounded-circle"></i></td>
                            </tr>
                        </tbody>
                    </table>
                 <div>
                    <div class="text-white small">
                        Legend: 
                        <small class="text-success"><i class="shadow far fa-circle text-success bg-success rounded-circle"></i> (>= 75%)</small>
                        <small class="text-warning"><i class="shadow far fa-circle text-warning bg-warning rounded-circle"></i> (< 75%)</small>
                        <small class="text-danger"><i class="shadow far fa-circle text-danger bg-danger rounded-circle"></i> (< 50%)</small>
                    </div>
                 </div>   
            </div>
            <div class="col-6 p-2 text-right">
                <h6 class="text-warning teal text-center"><i class="fas fa-bullhorn"></i> SSV CALL-OUTS</h6>
                <textarea class="form-control bg-dark lead small text-white border-secondary" id="callouts" placeholder="Enter your callouts here..." rows="6" onkeyup="EnableDisable(this)"></textarea>
                
                <button type="button" id="btnSubmit" class="btn btn-outline-info text-right btn-sm mt-1" disabled="disabled"><i class="far fa-save"></i> Save</button>   
                
            </div>
            <div class="col-6 p-2 bg-white border-secondary border-top">
                <h6 class="text-dark teal text-center"><i class="fas fa-tools"></i> EXPANSION SSV MILESTONE</h6>
                
                <div class="col-12">
                    <div class="container">
                         <div class="row">
                            <div style="width: 25%"></div>
                            <div class="col-1 border small exp-pd-perc w-50 text-center text-white bg-danger p-1 border shadow-sm rounded-pill">1</div>
                            <div style="width: 16%"></div>
                            <div class="col-1 border small exp-ssv-perc w-50 text-center text-white bg-danger p-1 border shadow-sm rounded-pill">2</div>
                            <div style="width: 16%"></div>
                            <div class="col-1 border small exp-sub-perc w-50 text-center text-white bg-danger p-1 border shadow-sm rounded-pill">3</div>
                            <div class="col"></div>  
                        </div>
                         <div class="row">
                            <div style="width: 25%"></div>
                            <div class="col-1 font-weight-bold small exp-pd w-50 text-center text-dark">1</div>
                            <div style="width: 15%"></div>
                            <div class="col-1 font-weight-bold small exp-ssv w-50 text-center text-dark">2</div>
                            <div style="width: 16%"></div>
                            <div class="col-1 font-weight-bold small exp-sub w-50 text-center text-dark">3</div>
                            <div class="col"></div>  
                        </div>
                    </div>
                </div>

                <div class="chart-container" style="position: relative;">
				    <canvas id="expansion_chart"></canvas>
				</div>
            </div>
            <div class="col-6 p-2 bg-white border-secondary border-top">
                 <h6 class="text-dark teal text-center"><i class="fas fa-broadcast-tower"></i> NEW SITE SSV MILESTONE</h6>
                 <div class="col-12">
                 <div class="row">
                    <div class="container">
                        <div class="row">
                            <div style="width: 25%"></div>
                            <div class="col-1 border small new-pd-perc w-50 text-center text-white bg-danger p-1 border shadow-sm rounded-pill">1</div>
                            <div style="width: 15%"></div>
                            <div class="col-1 border small new-ssv-perc w-50 text-center text-white bg-danger p-1 border shadow-sm rounded-pill">2</div>
                            <div style="width: 15%"></div>
                            <div class="col-1 border small new-sub-perc w-50 text-center text-white bg-danger p-1 border shadow-sm rounded-pill">3</div>
                            <div class="col"></div> 
                        </div>
                         <div class="row">
                            <div style="width: 25%"></div>
                            <div class="col-1 font-weight-bold small new-pd w-50 text-center text-dark">1</div>
                            <div style="width: 14%"></div>
                            <div class="col-1 font-weight-bold small new-ssv w-50 text-center text-dark">2</div>
                            <div style="width: 16%"></div>
                            <div class="col-1 font-weight-bold small new-sub w-50 text-center text-dark">3</div>
                            <div class="col"></div> 
                        </div> 
                    </div>
                   
                 </div>
                    
                </div>
                 <div class="chart-container" style="position: relative;">
				    <canvas id="milestone_chart"></canvas>
				</div>
            </div>
            <div class="col-6 p-2 border-secondary border-top">
                 <h6 class="text-warning text-center"><i class="fas fa-user"></i> VENDOR VIEW</h6>
                 
                 <table class="table table-bordered text-center small table-sm table-striped table-dark">
                        <thead>
                            <tr>
                            <th scope="col" class="small align-middle">Vendors</th>
                                <th scope="col" class="small align-middle">PD TRFS APPROVE </br> (EXPANSION)</th>
                                <th scope="col" class="small align-middle">READY FOR SSV</th>
                                <th scope="col" class="small align-middle">SSV SUBMISSION</th>
                                <th scope="col" class="small align-middle">SSV APPROVAL</th>
                            </tr>
                        </thead>
                        <tbody class="small">
                            <tr>
                                <th class="align-middle" scope="row">AMDOCS</th>
                                <td class="exp-amc-pd">0</td>
                                <td class="exp-amc-ssv">0</td>
                                <td class="exp-amc-sub">0</td>
                                <td class="exp-amc-appr">0</td>
                            </tr>
            
                            <tr>
                                <th class="align-middle" scope="row">HUAWEI</th>
                                <td class="exp-ht-pd">0</td>
                                <td class="exp-ht-ssv">0</td>
                                <td class="exp-ht-sub">0</td>
                                <td class="exp-ht-appr">0</td>
                            </tr>

                            <tr>
                                <th class="align-middle" scope="row">NOKIA</th>
                                <td class="exp-nsb-pd">0</td>
                                <td class="exp-nsb-ssv">0</td>
                                <td class="exp-nsb-sub">0</td>
                                <td class="exp-nsb-appr">0</td>
                            </tr>

                            <tr>
                                <th class="align-middle" scope="row">GT</th>
                                <td class="exp-gt-pd">0</td>
                                <td class="exp-gt-ssv">0</td>
                                <td class="exp-gt-sub">0</td>
                                <td class="exp-gt-appr">0</td>
                            </tr>

                            <tr>
                                <th class="align-middle" scope="row">GRAND TOTAL</th>
                                <td class="exp-total-pd font-weight-bold">0</td>
                                <td class="exp-total-ssv font-weight-bold">0</td>
                                <td class="exp-total-sub font-weight-bold">0</td>
                                <td class="exp-total-appr font-weight-bold">0</td>
                            </tr>

                            
                        </tbody>
                    </table>
            </div>
            <div class="col-6 p-2 border-secondary border-top">
                 <h6 class="text-warning text-center"><i class="fas fa-user"></i> VENDOR VIEW</h6>
                 <table class="table table-bordered text-center small table-sm table-striped table-dark">
                        <thead>
                            <tr>
                            <th scope="col" class="small align-middle"><i class="far fa-user"></i> Vendors</th>
                                <th scope="col" class="small align-middle">PD TRFS APPROVE </br> (EXPANSION)</th>
                                <th scope="col" class="small align-middle">READY FOR SSV</th>
                                <th scope="col" class="small align-middle">SSV SUBMISSION</th>
                                <th scope="col" class="small align-middle">SSV APPROVAL</th>
                            </tr>
                        </thead>
                        <tbody class="small">
                            <tr>
                                <th class="align-middle" scope="row">AMDOCS</th>
                                <td class="new-amc-pd">0</td>
                                <td class="new-amc-ssv">0</td>
                                <td class="new-amc-sub">0</td>
                                <td class="new-amc-appr">0</td>
                            </tr>
            
                            <tr>
                                <th class="align-middle" scope="row">HUAWEI</th>
                                <td class="new-ht-pd">0</td>
                                <td class="new-ht-ssv">0</td>
                                <td class="new-ht-sub">0</td>
                                <td class="new-ht-appr">0</td>
                            </tr>

                            <tr>
                                <th class="align-middle" scope="row">NOKIA</th>
                                <td class="new-nsb-pd">0</td>
                                <td class="new-nsb-ssv">0</td>
                                <td class="new-nsb-sub">0</td>
                                <td class="new-nsb-appr">0</td>
                            </tr>

                            <tr>
                                <th class="align-middle" scope="row">GT</th>
                                <td class="new-gt-pd">0</td>
                                <td class="new-gt-ssv">0</td>
                                <td class="new-gt-sub">0</td>
                                <td class="new-gt-appr">0</td>
                            </tr>

                            <tr>
                                <th class="align-middle" scope="row">GRAND TOTAL</th>
                                <td class="new-total-pd font-weight-bold">0</td>
                                <td class="new-total-ssv font-weight-bold">0</td>
                                <td class="new-total-sub font-weight-bold">0</td>
                                <td class="new-total-appr font-weight-bold">0</td>
                            </tr>

                            
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" data-backdrop="static" id="bulk-upload-ssv" tabindex="-1" aria-labelledby="bulk_response-ssv" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header global_color text-white">
				<h5 class="modal-title text-white">UPLOAD FILE</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="text-center">
					<div class="dropzone dropzone_ssv pad-00" style="border: 1px dashed #0087F7;">
						<div class="dz-message" style="text-align: center;" >
							<p class="text-center lead h4 text-muted">Upload File <i>(accepts .CSV files only)</i></p>
							<a class="btn btn-primary btn-sm"  href="#" role="button">
								<i class="fa fa-upload" aria-hidden="true"></i> Click to Upload
							</a>
							<p class="text-center lead h5 text-muted mg-top-10">Or drag files here.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>