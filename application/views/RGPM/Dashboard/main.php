
<div class="col-12 pt-0 pl-0 pr-0 overflow-auto border" style="max-height: 82vh;">
	<div class="col-12">
		<table class="table shadow-sm table-bordered small text-center table-hover">
			<thead class="thead-dark">
			<tr>
				<th scope="col">AGGR. Hour of Day(6) <br> 22 Feb 21</th>
				<th scope="col">NP 2G CSFR</th>
				<th scope="col">NP 2G DCR</th>
				<th scope="col">NP 2G HOSR</th>
				<th scope="col">NP 2G IAFR MO</th>
				<th scope="col">NP 2G AVE UL VQI</th>
			</tr>
			</thead>
			<tbody>
				<tr>
					<th scope="row">F_2G_VIS_ALL_SITES</th>
					<td class="table-success">0.999100</td>
					<td class="table-success">0.62276</td>
					<td class="table-success">97.64</td>
					<td class="table-danger">2.03</td>
					<td class="table-success">2.80</td>
				</tr>
			</tbody>
		</table>

		<table class="table shadow-sm table-bordered small text-center table-hover">
			<thead class="thead-dark">
			<tr>
				<th scope="col">AGGR. Hour of Day(6) <br> 22 Feb 21</th>
				<th scope="col">NP 3G CSFR</th>
				<th scope="col">NP 3G DCR</th>
				<th scope="col">NP 3G HOSR</th>
				<th scope="col">NP 3G IAFR MO</th>
				<th scope="col">NP 3G AVE UL VQI</th>
			</tr>
			</thead>
			<tbody>
				<tr>
					<th scope="row">E_3G_VIS_ALL_SITES</th>
					<td class="table-success">0.35</td>
					<td class="table-success">0.34</td>
					<td class="table-success">0.79</td>
					<td class="table-success">0.58</td>
					<td class="table-success">3.34</td>
				</tr>
			</tbody>
		</table>

		<table class="table shadow-sm table-bordered small text-center table-hover">
			<thead class="thead-dark">
			<tr>
				<th scope="col">AGGR. Hour of Day(6) <br> AGGR. Day(1)</th>
				<th scope="col">NP LTE RRC SR</th>
				<th scope="col">NP LTE ERAB SR</th>
				<th scope="col">NP LTE PS DCR</th>
				<th scope="col">NP LTE INTRA FREQ HO SR</th>
				<th scope="col">NP LTE CSFB FR Execution</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<th scope="row">13 LTE VIS</th>
				<td class="table-success">99.77</td>
				<td class="table-success">99.70</td>
				<td class="table-success">0.23</td>
				<td class="table-success">99.31</td>
				<td class="table-success">0.15</td>
			</tr>
			<tr>
				<th scope="row">14 TDD VIS</th>
				<td class="table-success">99.81</td>
				<td class="table-success">99.74</td>
				<td class="table-success">0.23</td>
				<td class="table-success">99.26</td>
				<td class="table-success">0.15</td>
			</tr>
			<tr>
				<th scope="row">15 TDD VIS</th>
				<td class="table-success">99.73</td>
				<td class="table-success">99.64</td>
				<td class="table-success">0.24</td>
				<td class="table-success">99.37</td>
				<td class="table-success">0.15</td>
			</tr>
			</tbody>
		</table>

		<table class="table shadow-sm table-bordered small text-center table-hover">
			<thead class="thead-dark">
			<tr>
				<th scope="col">AGGR. Hour of Day(6) <br> AGGR. Day(1)</th>
				<th scope="col">NP LTE RRC SR</th>
				<th scope="col">NP LTE ERAB SR</th>
				<th scope="col">NP LTE PS DCR</th>
				<th scope="col">NP LTE INTRA FREQ HO SR</th>
				<th scope="col">NP LTE CSFB FR Execution</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<th scope="row">13 LTE VIS</th>
				<td class="table-success">99.77</td>
				<td class="table-success">99.70</td>
				<td class="table-success">0.23</td>
				<td class="table-danger">99.31</td>
				<td class="table-danger">0.15</td>
			</tr>
			<tr>
				<th scope="row">14 TDD VIS</th>
				<td class="table-success">99.81</td>
				<td class="table-success">99.74</td>
				<td class="table-success">0.23</td>
				<td class="table-danger">99.26</td>
				<td class="table-danger">0.15</td>
			</tr>
			<tr>
				<th scope="row">15 TDD VIS</th>
				<td class="table-success">99.73</td>
				<td class="table-success">99.64</td>
				<td class="table-success">0.24</td>
				<td class="table-danger">99.37</td>
				<td class="table-danger">0.15</td>
			</tr>
			</tbody>
		</table>

		<table class="table shadow-sm table-bordered small text-center table-hover">
			<thead class="thead-dark">
			<tr>
				<th scope="col">AGGR. Hour of Day(6) <br> AGGR. Day(1)</th>
				<th scope="col">HT VOLTE ERAB Establishment SR</th>
				<th scope="col">HT VOLTE ERAB SETUP SR</th>
				<th scope="col">HT VOLTE CALL DROP RATE</th>
				<th scope="col">HT VOLTE SRVCC HOSR_LTE TO WCDMA</th>
				<th scope="col">HT VOLTE VQI</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<th scope="row">13 LTE VIS</th>
				<td class="table-success">99.53</td>
				<td class="table-success">99.65</td>
				<td class="table-success">0.26</td>
				<td class="table-danger">88.67</td>
				<td class="table-danger">5.14</td>
			</tr>
			<tr>
				<th scope="row">14 TDD VIS</th>
				<td class="table-success">99.49</td>
				<td class="table-success">99.89</td>
				<td class="table-success">0.29</td>
				<td class="table-danger">90.00</td>
				<td class="table-danger">2.15</td>
			</tr>
			<tr>
				<th scope="row">15 TDD VIS</th>
				<td class="table-success">99.58</td>
				<td class="table-success">99.90</td>
				<td class="table-success">0.23</td>
				<td class="table-danger">90.00</td>
				<td class="table-danger">0.14</td>
			</tr>
			</tbody>
		</table>

		<table class="table shadow-sm table-bordered small text-center table-hover">
			<thead class="thead-dark">
			<tr>
				<th scope="col">AGGR. Hour of Day(6) <br> AGGR. Day(1)</th>
				<th scope="col">HT 5G SGNB SUCCESS RATE</th>
				<th scope="col">HT 5G SGNB ABNORMAL RELEASE RATIO_RADIO</th>
				<th scope="col">HT 5G SGNB ABNORMAL RELEASE RATE_OVERALL</th>
				<th scope="col">HT 5G CELL AVAILABILITY</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<th scope="row">13 5G VIS</th>
				<td class="table-danger">97.56</td>
				<td class="table-success">1.09</td>
				<td class="table-success">1.20</td>
				<td class="table-danger">99.53</td>
			</tr>
			</tbody>
		</table>
	</div>
</div>
