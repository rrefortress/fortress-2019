<?php $this->load->view('/layout/header'); ?>
<body class="bg-danger">
	<div class="restrict">
		<div class="container-fluid">
			<div class="content">
					<h5 class="text-center text-white">
						<i class="fas fa-exclamation-triangle fs-3"></i><br><br>
						<h1 class="text-white text-center">Oops!</h1>
						<label class="text-white text-center">You are not allowed to access this page.</label>
					</h5>
					<center>
						<a Type="button" class="btn btn-outline-danger text-center text-white" onclick="history.back()"><i class="fas fa-arrow-left"></i> Back to previous</a>
					</center>
			</div>
		</div>
	</div>

</body>
