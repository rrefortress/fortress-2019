<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
        ini_set('memory_limit', '512M');
		$this->load->model('Admin_Model');
		$this->load->model('Logs_Model');
	}
	//navigate to user homepage with current-session checker
	public function home()
	{
		$access_level = $this->session->userdata('access_level');
		$dep = $this->session->userdata('department');
		$title = $access_level == 'Admin' || empty($dep) ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['title'] = 'Home Dashboard Page';
		$data['js'] = $access_level == 'Admin' ? array('Admin/admin_dashboard', 'Admin/calendar') : array('main_dashboard');
		
		if ($access_level == 'Admin') {
			$data['js'] = array('Admin/admin_dashboard', 'Admin/calendar');
		} else {
			if ($dep == 'RgPM') {
				$data['js'] = array('RGPM/SSV/ssv', 'RGPM/SSV/dropzone_ssv');
			} else {	
				$data['js'] = array('main_dashboard');
			}
		}
		
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			if ($dep == 'RgPM') {
				$this->load->view('RGPM/SSV/ssv', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				$this->load->view($access_level == 'Admin' ? 'RRE/admin_page' : 'Global/home_page', $data);
				$this->load->view('/layout/footer', $data);
			}
		}
	}

	public function sections() {
		$access_level = $this->session->userdata('access_level');
		$dep = $this->session->userdata('department');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['title'] =  $title .' Dashboard Page';
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);


		if($access_level === 'Admin' || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			
					if (empty($dep)) {
						redirect(base_url('login'));
					} else {
						#FOR USER
						switch ($dep) {
							case 'RRE' :case 'Sub-Admin' :
								$data['js'] = array('map','rre_dashboard', 'calendar');
								$this->load->view('RRE/Dashboard', $data);
								break;
							case 'RCNE':
								$data['js'] = array();
								$this->load->view('RCNE/Dashboard', $data); break;
							case 'RFE' :
								$data['js'] = array('FE/rfe_dashboard', 'FE/Tower/tower_chart', 'FE/Power/power_chart');
								$this->load->view('RFE/Dashboard', $data);  break;
							case 'RWE' :
								$data['js'] = array();
								$this->load->view('RWE/Dashboard', $data);  break;
							case 'RGPM':
								$data['js'] = array();
								$this->load->view('RGPM/Dashboard', $data);  break;
						}
						$this->load->view('/layout/footer', $data);
					}	
			
		}	
	}

	public function adminsitelistpage()
	{
		$data['title'] = 'Admin Sitelist';
		$access_level = 'Admin';
		$data['menus'] = $this->Admin_Model->get_menus($access_level);
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);
		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			$this->load->view('RRE/sites_page', $data);
		}
	}
	//navigate to login page with flashdata for error message and page redirection to its respective page if current-session is active
	public function loginPage()
	{
		$data['title'] = 'Log In';
		$data['error'] = $this->session->flashdata('error');
		$data['js'] = [];
		
		switch ($this->session->userdata('access_level'))
		{
			case 'Standard': case 'Admin': case 'Guest':
				if (empty($this->session->userdata('vendor'))) {
					redirect(base_url('home'));
				} else {
					redirect(base_url('acceptance'));
				}	
				break;
			default:
				$this->load->view('/layout/header', $data);
				$this->load->view('Global/login_page',$data);
				$this->load->view('/layout/footer', $data);
				break;
		}			
	}
	//session checker and page redirection to loginpage if there is no active current-session
	public function sessionChecker($access_level)
	{

		if($this->session->userdata('access_level')!='')
		{
			if($this->session->userdata('access_level')==$access_level)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			$id = $this->session->userdata('id');
			$this->User_accounts->set_active(0, $id);
			redirect(base_url('login'));
		}

	}
	//Navigate to User Management
	public function userManagePage()
	{
		$data['title'] = 'User Management';
		$access_level = 'Admin';
		$data['menus'] = $this->Admin_Model->get_menus($access_level);
		$data['js'] = array('Admin/users');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);
		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			$this->load->view('Admin/usermanage_page', $data);
			$this->load->view('/layout/footer', $data);
		}
	}
	//navigate to restricted page
	public function restrictedPage()
	{
		$data['title'] = 'Blocked';
		$this->load->view('errors/blocked_page', $data);
	}
	//navigate to File Upload page
	public function fileUploadPage()
	{
		$data['title'] = 'File Upload';
		$dep = $this->session->userdata('department');
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('file_upload');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);
		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor'))) {
			redirect(base_url('restrictedPage'));
		} else {
			$this->load->view(!$this->sessionchecker($access_level) ? 'upload_file_tracking' : 'Global/fileupload_page', $data);
			$this->load->view('/layout/footer', $data);
		}

	}
	//navigate to approvals page
	public function mdbupdatesPage()
	{
		$data['title'] = 'MDB Updates';
		$access_level = 'Admin';
		$data['menus'] = $this->Admin_Model->get_menus($access_level);
		$data['js'] = array('Admin/mdbupdates_actions');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			$this->load->view('RRE/mdbupdates_page', $data);
			$this->load->view('/layout/footer', $data);
		}
	}

	//Navigate to Site Management
	public function siteProfilePage()
	{
		$data['title'] = 'Site Profile';
		$arr_level = array('Admin',"Standard");

		$access_level = $this->session->userdata('access_level');
		$dep = $this->session->userdata('department');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if ($access_level == 'Admin' || $dep == 'RRE') {
			foreach ($arr_level as $value) {
				if($this->sessionchecker($value))
				{
					$this->load->view('RRE/siteprofile_page', $data);
					return;
				}
			}
		}

		redirect(base_url('restrictedPage'));
	}
	//Navigate to Project Management
	public function projectManagePage()
	{
		$access_level = 'Standard';
		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			$this->load->view('projectmanagement_page');
		}
	}
	public function sitespage()
	{
		$data['title'] = 'Sites';
		$access_level = $this->session->userdata('access_level');
		$dep = $this->session->userdata('department');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('sites_actions');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);
		if ($dep == 'RRE' || $access_level == 'Admin') {
			$this->load->view('RRE/sites_page', $data);
			$this->load->view('/layout/footer', $data);
		} else {
			redirect(base_url('restrictedPage'));
		}

	}

	public function userAccountPage()
	{
		$data['title'] = 'User Account';
		$dep = $this->session->userdata('department');
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('user_update', 'user_actions');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);
		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			$this->load->view('Global/useraccount_page', $data);
			$this->load->view('/layout/footer', $data);
		}
	}

	public function aboutPage()
	{
		$data['title'] = 'About';
		$dep = $this->session->userdata('department');
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = [];
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);
		if(!$this->sessionchecker($access_level))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			$this->load->view('Global/about_page', $data);
			$this->load->view('/layout/footer', $data);
		}
	}

	public function recordspage()
	{
		$data['title'] = 'Records Page';
		$access_level = $this->session->userdata('access_level');
		$dep = $this->session->userdata('department');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);
		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			if ($dep !== 'admin') {
				$this->load->view($dep.'/records_page', $data);
			} else {
				redirect(base_url('restrictedPage'));
			}
		}

	}

	public function addSitePage()
	{
		$data['title'] = 'Add Site';
		$user_level = array('Admin',"Standard");

		$dep = $this->session->userdata('department');
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('addsite_actions');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		foreach ($user_level as $value) {
			if($this->sessionchecker($value))
			{
				$this->load->view('RRE/addsite_page',$data);
				$this->load->view('/layout/footer', $data);
				return;
			}
		}
		redirect(base_url('restrictedPage'));
	}
	public function mapkmlPage()
	{
		$data['title'] = 'Map KML';
		$access_level = 'Standard';
		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			$this->load->view('mapkml_page', $data);
		}
	}

	public function mapKmlTool()
	{
		$data['title'] = 'Map KML Tool';
		$dep = $this->session->userdata('department');
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('mapKML');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);
		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			if ($dep == 'RRE' || $access_level == 'admin') {
				$this->load->view('RRE/map_kml_tool_page', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				redirect(base_url('restrictedPage'));
			}
		}
	}

	public function ultimate_db()
	{
		$data['title'] = 'Ultimate DB';
		$dep = $this->session->userdata('department');
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('map', 'ultimate_db');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			if ($dep !== 'admin') {
				$this->load->view('Global/ultimate_db_page', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				redirect(base_url('restrictedPage'));
			}
		}
	}

	public function power()
	{
		$data['title'] = ' Power';
		$dep = $this->session->userdata('department');
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('map','FE/Power/power_inputs', 'FE/Power/power', 'FE/Power/dropzone_power', 'FE/Power/proposal', 'FE/Power/proposal_chart', 'FE/Planning/ran_tagging');
		$data['level'] = strtoupper($access_level);
 		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			if ($dep === 'RFE') {
				$this->load->view('RFE/Power/main', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				redirect(base_url('restrictedPage'));
			}
		}
	}

	public function tower()
	{
		$data['title'] = ' Tower';
		$dep = $this->session->userdata('department');
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('map', 'FE/Tower/tower', 'FE/Tower/dropzone_tower', 'FE/Tower/proposal_tower', 'FE/Planning/ran_tagging');
		$data['level'] = strtoupper($access_level);
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			if ($dep === 'RFE') {
				$this->load->view('RFE/Tower/main', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				redirect(base_url('restrictedPage'));
			}
		}
	}

	public function back_up()
	{
		$data['title'] = 'Back Up DB';
		$dep = $this->session->userdata('department');
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('Admin/back_up');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			if ($dep !== 'admin') {
				$this->load->view('Admin/back_up_page', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				redirect(base_url('restrictedPage'));
			}
		}
	}

	public function repo()
	{
		$data['title'] = 'Repository';
		$dep = $this->session->userdata('department');
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('FE/Repo/repo', 'FE/Repo/dropzone_repo', 'FE/Repo/repo_chart');
		$data['level'] = strtoupper($access_level);
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			if ($dep == 'RFE') {
				$this->load->view('RFE/Repo/main', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				redirect(base_url('restrictedPage'));
			}
		}
	}

	

	public function main_dash_counts() {
		$result = $this->Admin_Model->main_dash_counts();
		echo json_encode($result);
	}

	public function dash_counts() {
		$dep = $this->session->userdata('department');
		if ($dep === 'RFE') {
			#RFE
			$result = $this->Admin_Model->rfe_dash_counts();
		} else if ($dep == 'RCNE') {
			#RCNE
			$result = $this->Admin_Model->rcne_dash_counts();
		} else if ($dep == 'RWE') {
			#RWE
			$result = $this->Admin_Model->rwe_dash_counts();
		} else {
			#RGPM
			$result = $this->Admin_Model->rgpm_dash_counts();
		}

		echo json_encode($result);
	}

	public function map_kml() {
		 $data_array = $this->Admin_Model->getCoordinates();
		 $kml =  $this->Map_Model->survey_generate_kml($data_array,$data_config,$option,$info);

		 $kmlfile= "Surveys_MDB";
		 header('Content-Type: text; charset=utf-8');
		 header('Content-Disposition: attachment; filename='.$kmlfile.'.kml');
		 header("Cache-Control: no-cache, no-store, must-revalidate");
		 header("Pragma: no-cache");
		 header("Expires: 0");
		 $output = fopen('php://output', 'w');
		 fputs($output, $kml);
		 fclose($output);
	}

	public function dashboard()
	{
		$data['title'] = 'Dashboard';
		$dep =  strtoupper($this->session->userdata('department'));
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			if ($dep == 'RGPM') {
				$this->load->view('RGPM/Dashboard/main', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				redirect(base_url('restrictedPage'));
			}
		}
	}

	public function macro()
	{
		$data['title'] = 'Macro';
		$dep = strtoupper($this->session->userdata('department'));
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('KPI/kpi/kpi', 'KPI/kpi/2g', 'KPI/kpi/3g', 'KPI/kpi/4g', 'KPI/kpi/5g', 'KPI/kpi/volte',
							'KPI/analyzer/analyzer', 'KPI/analyzer/2g', 'KPI/analyzer/3g', 'KPI/analyzer/4g', 'KPI/analyzer/5g', 'KPI/analyzer/volte');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);
		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			if ($dep == 'RGPM') {
				$this->load->view('RGPM/KPI/main', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				redirect(base_url('restrictedPage'));
			}
		}
	}

	public function ibs()
	{
		$data['title'] = 'IBS';
		$dep =  strtoupper($this->session->userdata('department'));
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('KPI/kpi/kpi', 'KPI/kpi/2g', 'KPI/kpi/3g', 'KPI/kpi/4g', 'KPI/kpi/5g', 'KPI/kpi/volte');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			if ($dep == 'RGPM') {
				$this->load->view('RGPM/KPI/main', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				redirect(base_url('restrictedPage'));
			}
		}
	}

	public function analyzer()
	{
		$data['title'] = 'Analyzer';
		$dep =  strtoupper($this->session->userdata('department'));
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('KPI/analyzer/uploads', 'KPI/analyzer/analyzer', 'KPI/analyzer/2g', 'KPI/analyzer/3g', 'KPI/analyzer/4g', 'KPI/analyzer/5g', 'KPI/analyzer/volte');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			if ($dep == 'RGPM') {
				$this->load->view('RGPM/Analyzer/main', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				redirect(base_url('restrictedPage'));
			}
		}
	}

	public function assessment()
	{
		$data['title'] = ' Assessment';
		$dep = $this->session->userdata('department');
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('FE/Power/assessment');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			if ($dep === 'RFE') {
				$this->load->view('RFE/Assessments/main', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				redirect(base_url('restrictedPage'));
			}
		}
	}

	public function bluebook()
	{
		$data['title'] = ' Bluebook';
		$dep = $this->session->userdata('department');
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('FE/Bluebook/bluebook', 'FE/Bluebook/dropzone');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if(!$this->sessionchecker($access_level))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			if ($dep === 'RFE') {
				$this->load->view('RFE/Bluebook/main', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				redirect(base_url('restrictedPage'));
			}
		}
	}

	public function planning()
	{
		$data['title'] = ' Planning';
		$dep = $this->session->userdata('department');
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('FE/Planning/planning', 'FE/Planning/2g', 'FE/Planning/3g', 'FE/Planning/4g', 'FE/Planning/5g');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if(!$this->sessionchecker($access_level))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			if ($dep === 'RFE') {
				$this->load->view('RFE/Planning/main', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				redirect(base_url('restrictedPage'));
			}
		}
	}

	public function transport()
	{
		$data['title'] = ' Transport';
		$dep = $this->session->userdata('department');
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('FE/Transport/transport', 'FE/Transport/dropzone_transport');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if(!$this->sessionchecker($access_level))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			if ($dep === 'RFE') {
				$this->load->view('RFE/Transport/main', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				redirect(base_url('restrictedPage'));
			}
		}
	}

	public function wireline()
	{
		$data['title'] = ' Wireline';
		$dep = $this->session->userdata('department');
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('FE/Wireline/wireline', 'FE/Wireline/dropzone_wireline');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if(!$this->sessionchecker($access_level))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			if ($dep === 'RFE') {
				$this->load->view('RFE/Wireline/main', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				redirect(base_url('restrictedPage'));
			}
		}
	}

	public function rigger()
	{
		$data['title'] = ' Rigger';
		$data['js'] = array('RGPM/Audit/riggers');
		$this->load->view('/layout/header', $data);
		$this->load->view('RGPM/Rigger/form', $data);
		$this->load->view('/layout/footer', $data);
	}

	public function audit()
	{
		$data['title'] = 'Macro';
		$dep = strtoupper($this->session->userdata('department'));
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('RGPM/Audit/audit');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);
		if(!$this->sessionchecker($access_level) || !empty($this->session->userdata('vendor')))
		{
			redirect(base_url('restrictedPage'));
		}
		else
		{
			if ($dep == 'RGPM') {
				$this->load->view('RGPM/Audit/audit', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				redirect(base_url('restrictedPage'));
			}
		}
	}
}

/* End of file Main.php */
/* Location: ./application/controllers/Main.php */
