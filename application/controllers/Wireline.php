<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wireline extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', -1);
        set_time_limit(0);
		$this->load->model('Wireline_Model');
        $this->load->library('excel');
	}

	public function wire_upload() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Wireline_Model->wire_upload();
			echo json_encode($result);
		}
	}

    
	public function get_wireline() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Wireline_Model->get_wireline();
			echo json_encode($result);
		}
	}

    public function get_wire_fkeys() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Wireline_Model->get_wire_fkeys();
			echo json_encode($res);
		}
	}

    public function wire_remove() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Wireline_Model->wire_remove();
			echo json_encode($res);
		}
	}
}
