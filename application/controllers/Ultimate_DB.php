<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ultimate_DB extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', '512M');
		$this->load->model('UDB_Model');
	}

	public function search_keys() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->UDB_Model->search_keys();
			echo json_encode($res);
		}
	}

	public function search_data() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->UDB_Model->search_data();
			echo json_encode($res);
		}
	}
}
