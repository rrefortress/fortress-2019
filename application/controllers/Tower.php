<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tower extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Tower_Model');
		$this->load->model('Site_database');
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', '-1');
		$this->load->library('excel');

	}

	public function tower_upload() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Tower_Model->tower_upload();
			echo json_encode($result);
		}
	}

	public function get_tower_list() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Tower_Model->get_tower_list();
			echo json_encode($result);
		}
	}
	public function get_antenna_epa_table() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Tower_Model->get_antenna_epa_table();
			echo json_encode($result);
		}
	}
	
	public function get_tower_epa_table() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Tower_Model->get_tower_epa_table();
			echo json_encode($result);
		}
	}
	public function save_proposal()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			echo json_encode($this->Tower_Model->save_proposal());
		}
	}
	public function get_proposal() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Tower_Model->get_proposal();
			echo json_encode($result);
		}
	}
	public function get_proposals_list() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Tower_Model->get_proposals_list();
			echo json_encode($result);
		}
	}

	public function export_tower()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Tower_Model->export_tower();
			echo json_encode($res);
		}
	}

	public function get_tower_completion() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Tower_Model->get_completion();
			echo json_encode($result);
		}
	}

	public function get_tchart_completion() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Tower_Model->get_chart_completion();
			echo json_encode($result);
		}
	}

	public function get_tower_reports() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Tower_Model->get_tower_reports();
			echo json_encode($result);
		}
	}

	public function get_tower_profile() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Tower_Model->get_tower_profile();
			echo json_encode($result);
		}
	}
}
