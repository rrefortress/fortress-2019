<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', '512M');
		$this->load->library('encrypt');
	}

	public function changePassword()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$newpassword = trim($this->input->post("newpassword"));
			$oldpassword = trim($this->input->post("oldpassword"));
			$uid = $this->session->userdata('id');

			$id_data = array(
				"username" => $this->session->userdata("username"),
				"password" => $oldpassword
			);

			$userdata = $this->User_accounts->fetch_user($id_data);

			if (empty($userdata)) {
				$res = array(
					"type" => "error",
					"title" => "Error!",
					"msg" => "Invalid Account."
				);
			} else {
				$userpassword = $this->encrypt->decode($userdata->password);

				if ($userpassword === $oldpassword) {
					$data = array(
						'id' => $this->session->userdata('id'),
						'fname' => $this->session->userdata('fname'),
						'lname' => $this->session->userdata('lname'),
						'access_level' => $this->session->userdata('access_level'),
					);

					$result = $this->Logs_Model->save_logs($data, 'Changed Password');
					$data = array("password" => $this->encrypt->encode($newpassword));
					$this->User_accounts->edit_user($data, $uid);
					$res = array(
						"type" => "success",
						"title" => "Success!",
						"msg" => "Update successful.",
						"logs" => $result
					);
				} else {
					$res = array(
						"type" => "error",
						"title" => "Error!",
						"msg" => "Invalid Password."
					);
				}
			}

			echo json_encode($res);
		}

	}

	public function updateUser()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$name_file = $_FILES['image_file']['name'];
			$config['upload_path'] = './assets/pictures/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$config['file_name'] = $name_file;
			$this->load->library('upload', $config);

			#CHECK IF FILE EXIST
			if (file_exists($config['upload_path'] . $name_file)) {
				#IF EXISTS
				goto else_jump;
			} else {
				#IF NOT
				if (!$this->upload->do_upload('image_file')) {
					$errors = array('error' => $this->upload->display_errors());
					// This image is uploaded by deafult if the selected image in not uploaded
					$res = array(
						"type" => "error",
						"title" => "Oops!",
						"message" => "Something went wrong."
					);
				} else {
					$data = array('upload_data' => $this->upload->data());
					else_jump:
					$datas = array(
						"profile" => empty($name_file) ? '' : base_url() . 'assets/pictures/' . $name_file,
						"username" => $this->input->post('username'),
						"fname" => $this->input->post('fname'),
						"lname" => $this->input->post('lname')
					);

					$this->User_accounts->edit_user($datas, $this->input->post('uid'));

					$data = array(
						'id' => $this->session->userdata('id'),
						'fname' => $this->session->userdata('fname'),
						'lname' => $this->session->userdata('lname'),
						'access_level' => $this->session->userdata('access_level'),
					);

					$logs = $this->Logs_Model->save_logs($data, 'Update Profile');
					$res = array(
						"type" => "success",
						"title" => "Success!",
						"message" => "Account successfully updated",
						"logs" => $logs
					);

				}
			}
			echo json_encode($res);
		}
	}

	public function getUsers()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$data = $this->User_accounts->getUsers();
			echo json_encode($data);
		}

	}

	public function delete_user()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$data = $this->User_accounts->delete_user();
			echo json_encode($data);
		}

	}

	public function reset_password()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$data = $this->User_accounts->reset_password();
			echo json_encode($data);
		}
	}
}



/* End of file User.php */
/* Location: ./application/controllers/User.php */
