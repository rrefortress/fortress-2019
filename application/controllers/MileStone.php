<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MileStone extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', '512M');
		$this->load->model('MileStone_Model');
	}

	public function mile_stone_page() {
		$data['title'] = 'Mile Stone';
		$access_level = $this->session->userdata('access_level');
		if(empty($access_level))
		{
			redirect(base_url('loginpage'));
		}
		else
		{
			$this->load->view('mile_stone_page', $data);
		}
	}

	public function nom_page() {
		$data['title'] = 'Nominations Page';
		$access_level = $this->session->userdata('access_level');
		if(empty($access_level))
		{
			redirect(base_url('loginpage'));
		}
		else
		{
			$this->load->view('nominations_page', $data);
		}
	}

	public function get_site_names($row_num=0) {
		$search = $this->input->get('search');
		$data = $this->MileStone_Model->get_site_names($search, $row_num);
		echo json_encode($data);
	}

	public function search_site_track() {
		$name = $this->input->post('name');
		$data = $this->MileStone_Model->search_site_track($name);
		echo json_encode($data);
	}
}
