<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Planning extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', -1);
        set_time_limit(0);
		$this->load->model('Planning_Model');
		$this->load->library('excel');
	}

	public function p2g() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Planning_Model->p2g();
			echo json_encode($res);
		}
	}

    public function p3g() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Planning_Model->p3g();
			echo json_encode($res);
		}
	}

    public function p4g() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Planning_Model->p4g();
			echo json_encode($res);
		}
	}

    public function p5g() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Planning_Model->p5g();
			echo json_encode($res);
		}
	}

    public function get_P2G() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Planning_Model->get_pl2g();
			echo json_encode($res);
		}
	}

	public function get_P3G() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Planning_Model->get_pl3g();
			echo json_encode($res);
		}
	}

	public function get_P4G() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Planning_Model->get_pl4g();
			echo json_encode($res);
		}
	}

	public function get_P5G() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Planning_Model->get_pl5g();
			echo json_encode($res);
		}
	}

	public function get_field_keys() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Planning_Model->get_field_keys();
			echo json_encode($res);
		}
	}

	public function pl_remove() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Planning_Model->pl_remove();
			echo json_encode($res);
		}
	}
}
