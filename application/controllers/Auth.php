<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', '512M');
		$this->load->model('Logs_Model');
		$this->load->helper('url');
		$this->load->helper('file');
		$this->load->library('encrypt');
		$this->org_email = array("globe.com.ph", "asticom.com.ph", "finsi.com.ph");
	}

	public function social_login() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$profile = trim($this->input->post('profile'));
			$fname = trim($this->input->post('firstname'));
			$lname = trim($this->input->post('lastname'));
			$email = trim($this->input->post('email'));

			$pieces = explode("@", $email);
			#CHECK IF EMAIL IS IN ORGANIZATION/Company
			if (in_array($pieces[1], $this->org_email)) {

				#CHECK IF NAME EXISTS
				$check_exist = $this->User_accounts->check_user($email);
				if ($check_exist->num_rows() > 0) {
					$profiles = $check_exist->row();
					$session_data = array(
						'username' => $profiles->username,
						'fname' => $profiles->fname,
						'profile' => $profile,
						'lname' => $profiles->lname,
						'department' => $profiles->department,
						'geoarea' => $profiles->geoarea,
						'access_level' => $profiles->access_level,
						'vendor' => $profiles->vendor,
						'id' => $profiles->id,
						'globe_id' => $profiles->globe_id
					);
					$res = $this->set_session($session_data, $profiles);
				} else {
					$res = array(
						"type" => "error",
						"title" => "Ooops!",
						"message" => "This account does not registered to Fortress Website please contact or message Admin for assistance.",
					);
				}
			} else {
				$res = array(
					"type" => "error",
					"title" => "Invalid!",
					"message" => "Globe emails are only allowed to enter.",
				);
			}

		}

		echo json_encode($res);
	}

	//login action for form method of login page
	public function sign_in()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			//get input from field using name attribute.
			$username = trim($this->input->post('username'));
			$password = trim($this->input->post('password'));
			//the query data
			$data = array(
				'username' => $username,
				'password' => $password
			);
			//fetch single data from database based on the query
			$profiles = $this->User_accounts->fetch_user($data);

			//check if there is data existed.
			if (!empty($profiles)) {
				//all data gathered in single key array;
				$session_data = array(
					'username' => $username,
					'fname' => $profiles->fname,
					'profile' => $profiles->profile,
					'lname' => $profiles->lname,
					'department' => $profiles->department,
					'geoarea' => $profiles->geoarea,
					'access_level' => $profiles->access_level,
					'vendor' => $profiles->vendor,
					'id' => $profiles->id,
					'globe_id' => $profiles->globe_id
				);
				$res = $this->set_session($session_data, $profiles);
			} else {
				$res = array(
					"type" => "error",
					"title" => "Oops!",
					"message" => "Invalid Username or Password."
				);
			}

			echo json_encode($res);
		}
	}

	public function set_session($session_data, $profiles) {
		//set the session
		$this->session->set_userdata($session_data);
		$this->User_accounts->set_active(1, $profiles->id);
		$logs =  $this->Logs_Model->save_logs($session_data, 'Logged In');
		$res = array(
			"type" => "success",
			"title" => "Success!",
			"message" => "Successfully login",
			"logs" => $logs,
			'active' => $this->User_accounts->get_active(),
		);

		return $res;
	}

	//logout all sessions, redirecting to loginpage
	public function sign_out()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$id = $this->session->userdata('id');
			#CHECK IF SESSION EXPIRED OR EXIST
			if (!empty($id)) {
				$data = array(
					'id' => $this->session->userdata('id'),
					'fname' => $this->session->userdata('fname'),
					'lname' => $this->session->userdata('lname'),
					'access_level' => $this->session->userdata('access_level'),
				);

				$logs = $this->Logs_Model->save_logs($data, 'Logged Out');
				$this->User_accounts->set_active(0, $id);
				$array_items = array('id', 'profile', 'username', 'fname', 'lname', 'lname', 'department', 'geoarea', 'access_level', 'vendor');
				$this->session->unset_userdata($array_items);
				$this->session->sess_destroy();

				$res = array(
					"type" => "success",
					"title" => "Success!",
					"message" => "Successfully login",
					"logs" => $logs,
					'active' => $this->User_accounts->get_active(),
				);
			} else {
				$res = array(
					"type" => "error",
					"title" => "Oops!",
					"message" => "Something went wrong."
				);
			}

			echo json_encode($res);
		}
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */
