<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transport extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', -1);
        set_time_limit(0);
		$this->load->model('Transport_Model');
        $this->load->library('excel');
	}

	public function trans_upload() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Transport_Model->trans_upload();
			echo json_encode($result);
		}
	}

    
	public function get_transport() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Transport_Model->get_transport();
			echo json_encode($result);
		}
	}

    public function get_trans_fkeys() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Transport_Model->get_trans_fkeys();
			echo json_encode($res);
		}
	}

    public function trans_remove() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Transport_Model->trans_remove();
			echo json_encode($res);
		}
	}


	// public function get_content() {
	// 	if (!$this->input->is_ajax_request()) {
	// 		exit('No direct script access allowed');
	// 	} else {
	// 		$res = $this->Repo_Model->get_content();
	// 		echo json_encode($res);
	// 	}
	// }

	// public function delete_content() {
	// 	if (!$this->input->is_ajax_request()) {
	// 		exit('No direct script access allowed');
	// 	} else {
	// 		$res = $this->Repo_Model->delete_content();
	// 		echo json_encode($res);
	// 	}
	// }

	// public function get_file_count() {
	// 	if (!$this->input->is_ajax_request()) {
	// 		exit('No direct script access allowed');
	// 	} else {
	// 		$res = $this->Repo_Model->get_file_count();
	// 		echo json_encode($res);
	// 	}
	// }

	// public function get_repo_chart() {
	// 	if (!$this->input->is_ajax_request()) {
	// 		exit('No direct script access allowed');
	// 	} else {
	// 		$res = $this->Repo_Model->get_repo_chart();
	// 		echo json_encode($res);
	// 	}
	// }
}
