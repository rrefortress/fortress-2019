<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CLP extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', '512M');
		$this->load->model('CLP_Model');
		$this->load->model('Admin_Model');
		$this->load->library('excel');
	}

	public function clp_page() {
		$data['title'] = 'CLP (2 + 2 + 2)';
		$access_level = $this->session->userdata('access_level');
		$dep = $this->session->userdata('department');

		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('clp', 'dropzone_clp');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if(empty($access_level))
		{
			redirect(base_url('login'));
		}
		else
		{
			if ($dep == 'RRE' || $access_level == 'admin') {
				$this->load->view('RRE/clp_page', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				redirect(base_url('restricted'));
			}

		}
	}

	public function get_CID() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->CLP_Model->generate_CID();
			echo json_encode($res);
		}
	}

	public function clp_upload() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->CLP_Model->clp_upload();
			echo json_encode($res);
		}
	}


}
