<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FileUpload extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		ini_set('max_execution_time', 0);
		ini_set('memory_limit','2048M');
		set_time_limit(0);
		$this->load->model('UDB_Model');
	}

	public function udb_upload() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->UDB_Model->batch_insert();
			echo json_encode($result);
		}
	}

	public function save_upload_logs() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$dep = $this->session->userdata('department');
			$data = array(
				'id'         => $this->session->userdata('id'),
				'fname'      => $this->session->userdata('fname'),
				'lname'      => $this->session->userdata('lname'),
				'access_level'  => $this->session->userdata('access_level'),
			);

			$result = $this->Logs_Model->save_logs($data, 'Upload '.strtoupper($dep).' Tracker');
			echo json_encode($result);
		}
	}

	public function truncate_data() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->UDB_Model->truncate_data();
			echo json_encode($result);
		}
	}
}
