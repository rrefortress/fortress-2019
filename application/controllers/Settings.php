<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', '512M');
		$this->load->model('Admin_Model');
	}

	public function settings_page() {
		$data['title'] = 'Settings';
		$dep = $this->session->userdata('department');
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('Admin/settings');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);
		if(empty($access_level))  {
			redirect(base_url('login'));
		} else if ($access_level == 'Admin') {
			$this->load->view('Admin/settings_page', $data);
			$this->load->view('/layout/footer', $data);
		} else  {
			redirect(base_url('restricted'));
		}
	}

	public function get_all_menus() {
		$sort = $this->input->get('sort');
		$data = $this->Admin_Model->get_menus($sort);
		echo json_encode($data);
	}

	public function new_menu() {
		$type = $this->input->post('type');
		if ($type == 'menu' || empty($type)) {
			$data = $this->Admin_Model->new_menu();
		} else {
			$data = $this->Admin_Model->sub_menus();
		}
		echo json_encode($data);
	}

	public function del_menu() {
		$data = $this->Admin_Model->del_menu();
		echo json_encode($data);
	}

	public function sub_menu() {
		$url = $this->input->post('url');
		for ($count = 0; $count < count($_POST["Name"]); $count++) {
			$inputs[] = array(
				'url'           => $url,
				'Order'         => $count,
				'Name'   		=> $_POST['Name'][$count],
				'Icon' 			=> $_POST['Icon'][$count],
				'Base_url'     	=> $_POST['Base_url'][$count]
			);
		}

		$data = $this->Admin_Model->sub_menu($inputs);

		echo json_encode($data);
	}

}
