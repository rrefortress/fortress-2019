<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Admin_Model');
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', '512M');
		$this->load->model('Logs_Model');
	}


	//Add User from html modal input to model
	public function addUser()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$id = $this->input->post('uid');
			$data = array(
				"username"		=> trim($this->input->post('username')),
				"globe_id"		=> trim($this->input->post('globe_id')),
				"email"		    => trim($this->input->post('email')),
				"password"		=> trim($this->input->post('username')),
				"fname"			=> trim($this->input->post('fname')),
				"lname"			=> trim($this->input->post('lname')),
				"department"	=> trim($this->input->post('department')),
				"geoarea"		=> trim($this->input->post('geoarea')),
				"access_level"	=> trim($this->input->post('access_level')),
				"status"        => trim($this->input->post('status')),
				"vendor"        => trim($this->input->post('vendor')),
			);
			$data = $this->User_accounts->add_user($data, $id);
			echo json_encode($data);
		}
	}
	//Get all Users from model and echo the data in json format for ajax success
	public function getUsers()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$data = $this->User_accounts->get_users();
			echo json_encode($data);
		}
	}

	public function getadminsitelist()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$database_table = '4g';
			$select = '*';
			$geoarea = $this->session->userdata('geoarea');
			$database_table =$database_table.'_'.$geoarea;
			echo json_encode($this->Site_database->getAll($database_table, $select));
		}
	}

	//Delete a User from model, will not return anydata.
	public function deleteUser()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$data = array(
				"id"		=> $this->input->post('id'),
			);

			$this->User_accounts->delete_user($data);
		}
	}
	//Fetch a single User from model
	public function fetchUser()
	{
		$id_data = array(
			"id"		=> $this->input->post('id'),
		);
		echo json_encode($this->User_accounts->fetch_user($id_data));
	}
	//Edit a User from input html to model, will not return anything
	public function editUser()
	{
		$data = array(
			"username"		=> $this->input->post('username'),
			"password"		=> md5($this->input->post('username')),
			"fname"			=> $this->input->post('fname'),
			"lname"			=> $this->input->post('lname'),
			"department"	=> $this->input->post('department'),
			"geoarea"		=> $this->input->post('geoarea'),
			"access_level"	=> $this->input->post('access_level'),
			"vendor"		=> $this->input->post('vendor')
		);
		$id = array(
			"id" => $this->input->post('id')
		);
		$this->User_accounts->edit_user($data, $id);
	}
////load approvals
	public function approvalloadRecord($rowno=0){
		//print_r($this->input->get());
		// Row per page
		$rowperpage = $this->input->get('rowperpage');
		//	get filters from checkbox
		$data[0] = $this->input->get('0');
		$data[1] = $this->input->get('1');
		$data[2] = $this->input->get('2');
		// Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		// All records count
		$allcount = $this->Site_database->approvalgetData(Null,Null,$data,1);
		// Get records
		$users_record = $this->Site_database->approvalgetData($rowno,$rowperpage,$data,0);

		// Pagination Configuration
		$config['base_url'] = base_url('approvalloadRecord');
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $allcount;
		$config['per_page'] = $rowperpage;

		$config['full_tag_open']    = '<div class="padding text-right"><nav><ul class="pagination">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active "><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link ">';
		$config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link ">';
		$config['prev_tag_close']  = '</span></li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link ">';
		$config['first_tag_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link ">';
		$config['last_tag_close']  = '</span></li>';

	}

	public function download_db() {
		$json = $this->Admin_Model->download_db();
		echo json_encode($json);
	}

	public function backup_db() {
		$json = $this->Admin_Model->back_up_db();
		echo json_encode($json);
	}

	public function delete_db() {
		$json = $this->Admin_Model->delete_db();
		echo json_encode($json);
	}

	public function get_datas() {
		$json = $this->Admin_Model->get_tables();
		echo json_encode($json);
	}

	public function get_dashboard() {
		$json = array(
			'activities'  => $this->Admin_Model->getActivites(),
			'users' 	  => $this->User_accounts->getUsers(),
			'surveys'     => $this->Admin_Model->getSurveysTrack(),
			// 'tssr'        => $this->Admin_Model->getTSSRTrack(),
			'logs' 		  => $this->Logs_Model->read_logs()
		);

		echo json_encode($json);
	}

	public function get_logs($row_num=0) {
		$data = array(
			'dep'    => $this->input->post('department'),
			'search' => $this->input->get('search'),
			'date'   => $this->input->get('date')
		);
		$data   = $this->Logs_Model->read_logs($data, $row_num);
		echo json_encode($data);
	}

	public function get_coordinates() {
		$json = array(
			'coordinates' => $this->Admin_Model->getCoordinates(),
		);
		echo json_encode($json);
	}
}
