<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KPI extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', -1);
		ini_set('max_execution_time', 3600);
		$this->load->model('KPI_Model');
		$this->load->library('excel');
	}

	public function kpi2g_upload() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->kpi2g_upload();
			echo json_encode($res);
		}
	}

	public function kpi3g_upload() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->kpi3G_upload();
			echo json_encode($res);
		}
	}

	public function get_2g_kpi() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_2g_kpi();
			echo json_encode($res);
		}
	}

	public function get_2g_csfr() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_2g_csfr();
			echo json_encode($res);
		}
	}

	public function get_2g_dcr() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_2g_dcr();
			echo json_encode($res);
		}
	}

	public function get_2g_iafr() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_2g_iafr();
			echo json_encode($res);
		}
	}

	public function get_3g_kpi() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_3g_kpi();
			echo json_encode($res);
		}
	}

	public function get_voice_csfr() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_voice_csfr();
			echo json_encode($res);
		}
	}

	public function get_voice_dcr() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_voice_dcr();
			echo json_encode($res);
		}
	}

	public function get_voice_sms() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_voice_sms();
			echo json_encode($res);
		}
	}

	public function get_hsdpa_csfr() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_hsdpa_csfr();
			echo json_encode($res);
		}
	}

	public function get_hsdpa_dcr() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_hsdpa_dcr();
			echo json_encode($res);
		}
	}

	public function clear_2g() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->clear_2g();
			echo json_encode($res);
		}
	}

	public function clear_3g() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->clear_3g();
			echo json_encode($res);
		}
	}

	public function kpi4g_upload() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->kpi4g_upload();
			echo json_encode($res);
		}
	}

	public function get_4g_kpi() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_4g_kpi();
			echo json_encode($res);
		}
	}

	public function get_volte_kpi() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_volte_kpi();
			echo json_encode($res);
		}
	}

	public function clear_4g() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->clear_4g();
			echo json_encode($res);
		}
	}

	public function get_rrc() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_rrc();
			echo json_encode($res);
		}
	}

	public function get_erab() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_erab();
			echo json_encode($res);
		}
	}

	public function get_intra() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_intra();
			echo json_encode($res);
		}
	}

	public function get_4g_dcr() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_4g_dcr();
			echo json_encode($res);
		}
	}

	public function get_2g_calendar() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_2g_calendar();
			echo json_encode($res);
		}
	}

	public function get_3g_calendar() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_3g_calendar();
			echo json_encode($res);
		}
	}

	public function get_4g_calendar() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_4g_calendar();
			echo json_encode($res);
		}
	}

	public function kpi_chart_2g_upload() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->kpi_chart_2g_upload();
			echo json_encode($res);
		}
	}

	public function get_filter_menus() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_filters_menus();
			echo json_encode($res);
		}
	}

	public function kpi_chart_3g_upload() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->kpi_chart_3g_upload();
			echo json_encode($res);
		}
	}

	public function kpi_chart_4g_upload() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->kpi_chart_4g_upload();
			echo json_encode($res);
		}
	}

	public function kpi_chart_volte_upload() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->kpi_chart_volte_upload();
			echo json_encode($res);
		}
	}

	public function kpi_chart_5g_upload() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->kpi_chart_5g_upload();
			echo json_encode($res);
		}
	}

	public function kpi5g_upload() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->kpi5g_upload();
			echo json_encode($res);
		}
	}

	public function clear_5g() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->clear_5g();
			echo json_encode($res);
		}
	}

	public function get_5g_kpi() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_5g_kpi();
			echo json_encode($res);
		}
	}

	public function get_sgnb_sr() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_sgnb_sr();
			echo json_encode($res);
		}
	}

	public function get_sgnb_rd() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_sgnb_rd();
			echo json_encode($res);
		}
	}

	public function get_sgnb_ro() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_sgnb_ro();
			echo json_encode($res);
		}
	}

	public function kpivolte_upload() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->kpivolte_upload();
			echo json_encode($res);
		}
	}

	public function clear_volte() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->clear_volte();
			echo json_encode($res);
		}
	}

	public function get_volte_estab() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_volte_estab();
			echo json_encode($res);
		}
	}

	public function get_volte_erab() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_volte_erab();
			echo json_encode($res);
		}
	}

	public function get_volte_sdvcc() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_volte_sdvcc();
			echo json_encode($res);
		}
	}

	public function get_volte_dcr() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_volte_dcr();
			echo json_encode($res);
		}
	}

	function get_2g_graph() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_2g_graph();
			echo json_encode($res);
		}
	}

	function get_3g_graph() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_3g_graph();
			echo json_encode($res);
		}
	}

	function get_4g_graph() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_4g_graph();
			echo json_encode($res);
		}
	}

	function get_5g_graph() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->KPI_Model->get_5g_graph();
			echo json_encode($res);
		}
	}
}
