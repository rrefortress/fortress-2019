<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SSV extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', '512M');
		$this->load->model('SSV_Model');
		$this->load->library('excel');
	}

	public function get_ssv() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->SSV_Model->get_ssv();
			echo json_encode($res);
		}
	}

	public function ssv_upload() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->SSV_Model->ssv_upload();
			echo json_encode($res);
		}
	}

	public function ssv_callouts() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->SSV_Model->ssv_callouts();
			echo json_encode($res);
		}
	}

	public function get_callouts() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->SSV_Model->get_callouts();
			echo json_encode($res);
		}
	}

	public function export_ssv() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->SSV_Model->export_ssv();
			echo json_encode($res);
		}
	}



}
