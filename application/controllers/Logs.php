<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', '512M');
		$this->load->model('Admin_Model');
		$this->load->model('Admin_Model');
	}

	public function logs_page() {
		$data['title'] = 'Logs';
		$dep = $this->session->userdata('department');
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if(empty($access_level))  {
			redirect(base_url('login'));
		} else if ($access_level == 'Admin') {
			$this->load->view('logs_page', $data);
		} else  {
			redirect(base_url('restricted'));
		}
	}

}
