<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MapKML extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Site_database');
		$this->load->model('Map_Model');
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', '512M');
		$this->output->set_header('Cache-control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	}

	public function sitetokml()
	{
		//array data
		$database_table = 'tower';

		// $alldata = $this->input->post('data');
		$geoarea = $this->session->userdata('geoarea');
		$filter = '*';
		$config = $this->input->post('config'); //model color radius
		$option = $this->input->post('option'); // indoor outdoor siteonly
		$info = $this->input->post('info'); //true false
		$lists = $this->input->post('lists'); //list of requested sites
		$joinID = array(
			'"'.$database_table.'_'.$geoarea.'.SITENAME"="2g_'.$geoarea.'.sitename"',
			'"'.$database_table.'_'.$geoarea.'.SITENAME"="3g_'.$geoarea.'.sitename"',
			'"'.$database_table.'_'.$geoarea.'.SITENAME"="4g_'.$geoarea.'.sitename"'
		);
		$tableto = array("2g_".$geoarea,"3g_".$geoarea,"4g_".$geoarea,"tower_".$geoarea);
		$data_array = array();
		$data_config = array();

/*		if($option == 'site')
		{
			foreach ($lists as $list)
			{
				$data = $this->Site_database->get_site('*',array('sitename' => $list),$database_table.'_'.$geoarea);
				if(count($data) > 0)
				{
					array_push($data_array, $data);
				}
			}
			$kml =  $this->Map_Model->generate_kml($data_array,array($config),$option,$info);
		}
		else
		{*/
			foreach ($config as $value)
			{
				foreach ($lists as $list)
				{
					switch ($value['model'])
					{
						case 'G900':
						case 'G1800':
							$database_table = $tableto[0];
							break;
						case 'U900':
						case 'U2100':
							$database_table = $tableto[1];
							break;
						case 'L700':
						case 'L1800':
						case 'L2300':
						case 'L2600':
							$database_table = $tableto[2];
							break;
						default:
							break;
					}
					if($option == 'site')
					{
						$data = $this->Site_database->get_join('tower_'.$geoarea.'.*',array('tower_'.$geoarea.'.SITENAME'=> $list, $database_table.'.tech'=> $value['model']),'tower_'.$geoarea.'.SITENAME = '.$database_table.'.SITENAME',$database_table,'tower_'.$geoarea,'right');
					}
					else
					{
						$data = $this->Site_database->get_join($database_table.'.*',array('tower_'.$geoarea.'.SITENAME'=> $list, $database_table.'.tech'=> $value['model'], $database_table.'.sitetype' => $option),'tower_'.$geoarea.'.SITENAME = '.$database_table.'.SITENAME','tower_'.$geoarea,$database_table,'left');
					}


					if(count($data) > 0)
					{
						array_push($data_array, $data);
						array_push($data_config, array('model' => $value['model'], 'color' => $value['color'], 'radius' => $value['radius']));
					}
					else
					{
						continue;
					}
				}
			}

			$kml =  $this->Map_Model->generate_kml($data_array,$data_config,$option,$info);
		//}
		//print_r($data_array);
		$with_info = '';
		if($info=='true')
		{
			$with_info = '_info';
		}
		$kmlfile= $option."_MDB".$with_info;
		header('Content-Type: text; charset=utf-8');
		header('Content-Disposition: attachment; filename='.$kmlfile.'.kml');
		header("Cache-Control: no-cache, no-store, must-revalidate");
		header("Pragma: no-cache");
		header("Expires: 0");
		$output = fopen('php://output', 'w');
		fputs($output, $kml);
		fclose($output);
	}

	function getsitelist()
	{
		$geoarea = $this->session->userdata('geoarea');
		echo json_encode($this->Site_database->get_site('sitename','tower_'.$geoarea));
	}
}

/* End of file MapKML.php */
/* Location: ./application/controllers/MapKML.php */
