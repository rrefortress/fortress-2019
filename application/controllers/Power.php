<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Power extends CI_Controller
{
	public function __construct()
	{
		date_default_timezone_set('Asia/Manila');
		parent::__construct();
		$this->load->model('Power_Model');
		$this->load->model('Tower_Model');
		$this->load->model('Site_database');
		$this->load->library('excel');
		ini_set('memory_limit', -1);
		ini_set('max_execution_time', 3600);
	}

	public function rre_equip_powertable($tech)
	{
		switch ($tech)
		{
			case "G900":
				return 1140;
				break;
			case "G1800":
				return 1362;
				break;
			case "U2100":
				return 903;
				break;
			case "U21DB":
				return 1092;
				break;
			case "L700":
				return 1188;
				break;
			case "L1800":
				return 902;
				break;
			case "L18DB":
				return 1742;
				break;
			case "L2100":
				return 1098;
				break;
			case "L2300":
				return 1342.5;
				break;	
			case "L2600":
				return 1342.5;
				break;	
			case "L26MM":
				return 1342.5;
				break;	
			case "BBU5G":
				return 0;
				break;	
			default:
				return 0;
				break;
		}
	}

	public function insert_site() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$input_data = array(
				"PLAID"	=> $this->input->post("plaid"),
				"SITE_TYPE"	=> $this->input->post("site_type"),
				"SITE_NAME"	=> $this->input->post("sitename"),
				"SITE_CLASS"	=> $this->input->post("site_class"),
				"GRID_STATUS"	=> $this->input->post("grid_status"),
				"CABIN_TYPE"	=> $this->input->post("cabin_type"),
				"POWER_CONNECTION"	=> $this->input->post("power_connection"),
				"PHASE"	=> $this->input->post("phase"),
				"CABIN_COUNT"	=> $this->input->post("cabin"),
				"GENSET"	=> $this->input->post("genset"),
				"ACU_WATTAGE"	=> $this->input->post("ac_unit"),
				"OTHERAC_LOAD"	=> $this->input->post("other_ac_load"),
				"ACTUALRECTIFIER_LOAD"	=> $this->input->post("actual_rec_load"),
				"ECB_CLAMP_READING"	=> $this->input->post("ecb_clamp"),
				"BATTERY_CAPACITY"	=> $this->input->post("bat_capacity"),
				"CHARGING_RATE"	=> $this->input->post("char_rate"),
				"TRANSFORMER_CAPACITY"	=> $this->input->post("transformer_capacity"),
				"GENSET_CAPACITY"	=> $this->input->post("genset_capacity"),
				"ECB_CAPACITY"	=> $this->input->post("ecb_capacity"),
				"ACPDB_CAPACITY"	=> $this->input->post("acpdb_capacity")
			);
			$this->Power_Model->insert_powerdb($input_data,"planB");
			$res = array(
				"type"    => "success",
				"title"   => "Success!",
				"message" => "Site successfully added."
			);
			echo json_encode($res);
		}
	}

	public function get_power() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Power_Model->get_power();
			echo json_encode($result);
		}
	}

	public function get_tower() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Tower_Model->get_tower();
			echo json_encode($result);
		}
	}

	public function power_upload()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Power_Model->power_upload();
			echo json_encode($result);
		}
//		$name_file = $_FILES['target_file']['name'];
//
//		$config['upload_path']          = './upload/';
//		$config['allowed_types']        = 'csv';
//		$config['file_name']			= $name_file;
//
//		$this->load->library('upload', $config);
//		$this->upload->initialize($config);
//		if ( ! $this->upload->do_upload('target_file'))
//		{
//			$error = array('error' => $this->upload->display_errors());
//			print_r($error);
//		}
//		else
//		{
//			$this->upload->data();
//			$this->Power_Model->power_upload($name_file);
//		}
	}

	public function proposal_upload()
	{
		$name_file = $_FILES['proposed_file']['name'];
		$config['upload_path']          = './upload/';
		$config['allowed_types']        = 'csv';
		$config['file_name']			= $name_file;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('proposed_file'))
		{
			$error = array('error' => $this->upload->display_errors());

			print_r($error);
		}
		else
		{
			$this->upload->data();
			$result = $this->Power_Model->batch_proposal($name_file);
			echo json_encode($result);
		}
	}

	public function proposal_save_mdb()
	{
		$savefile_name = trim($this->input->post('save_name'));

		$feedback = $this->Power_Model->insert_power_proposal();

		if($feedback > 0)
		{
			$res = array(
				"type"    => "success",
				"title"   => "Saved!",
				"message" => $savefile_name." was successfully saved.(".$feedback." entries)"
			);
		}
		else
		{
			$res = array(
				"type"    => "failed",
				"title"   => "Failed",
				"message" => $savefile_name." was not save."
			);
		}
		
		echo json_encode($res);
	}

	public function get_chart() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = array(
				'master' => array(
					'project' => $this->Tower_Model->get_project(),
					'site_type' => $this->Tower_Model->get_site_type(),
					'on_aired' => $this->Tower_Model->get_on_aired(),
				),
				'tower' => array(
					'gf' => $this->Tower_Model->get_gf_site(),
					'rt' => $this->Tower_Model->get_rt_site(),
					'tc' => $this->Tower_Model->tower_capacity(),
					'nscp' => $this->Tower_Model->nscp_compliance(),
				),
				'power' => array(
					'pc' => $this->Power_Model->power_connection(),
					'bat' => $this->Power_Model->get_rectifier_bat_hour(),
					'ac' => $this->Power_Model->get_ac_capacity(),
					'dc' => $this->Power_Model->get_dc_rec(),
					'site_class' => $this->Power_Model->get_site_class(),
					'grid_status' => $this->Power_Model->grid_status(),
					'bat_type' => $this->Power_Model->battery_type(),
					'rec_site' => $this->Power_Model->rectifier_site(),
					//'rwdc' => $this->Power_Model->remain_wattage_dc(),
					'ct' => $this->Power_Model->cabin_type(),
//					'dc_util' => $this->Power_Model->get_dc_util(),
					'dc_ab' => $this->Power_Model->get_dc_classAB(),
					'dc_bc' => $this->Power_Model->get_dc_classBC(),
					'trc' => $this->Power_Model->get_transformer_capacity(),
					'gnc' => $this->Power_Model->get_genset_capacity(),
				)

			);
			echo json_encode($result);
		}
	}

	public function add_powerdb()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			#CHECK IF SITENAME EXISTS
			$check = $this->Power_Model->check_site_name();
			if ($check) {
				$data = array(
					"type"    => "error",
					"title"   => "INVALID!",
					"message" => "PLAID or SITENAME are already exists."
				);
			} else {
				$input_data = $this->input->post();
				$res = $this->Power_Model->add_data_powerdb($input_data);
				if ($res > 0) {
					$data = array(
						"type"    => "success",
						"title"   => "Success!",
						"message" => "Item successfully updated"
					);
				} else {
					$data = array(
						"ty`pe"    => "error",
						"title"   => "Opps!",
						"message" => "Something went wrong."
					);
				}
			}


			echo json_encode($data);
		}
	}

	public function export_power()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Power_Model->export_power();
			echo json_encode($res);
		}
	}

	public function calculate_all() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			#CALCULATE ALL UTIL BEFORE LOAD
			$this->Power_Model->recalc_all();
			echo json_encode(array('type' => 'success'));
		}
	}

	public function get_rec() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			#GET RECTIFIER
			$result = $this->Power_Model->get_rec();
			echo json_encode($result);
		}
	}

	public function get_equip() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			#GET RECTIFIER
			$result = $this->Power_Model->get_equip();
			echo json_encode($result);
		}
	}

	public function get_bat() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			#GET BATTERY
			$result = $this->Power_Model->get_bat();
			echo json_encode($result);
		}
	}

	public function get_drop_wires() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			#GET BATTERY
			$result = $this->Power_Model->get_drop_wires();
			echo json_encode($result);
		}
	}

	public function add_rec() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			#ADD REC
			$result = $this->Power_Model->add_rec();
			echo json_encode($result);
		}
	}

	public function add_bat() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			#ADD BAT
			$result = $this->Power_Model->add_bat();
			echo json_encode($result);
		}
	}

	public function add_equip() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			#ADD BAT
			$result = $this->Power_Model->add_equip();
			echo json_encode($result);
		}
	}

	public function del_equip() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			#DELETE REC
			$result = $this->Power_Model->del_equip();
			echo json_encode($result);
		}
	}

	public function del_rec() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			#DELETE REC
			$result = $this->Power_Model->del_rec();
			echo json_encode($result);
		}
	}

	public function del_bat() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			#DELETE BATTERY
			$result = $this->Power_Model->del_bat();
			echo json_encode($result);
		}
	}

	public function get_rec_bat() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			#GET RECTIFIER MODEL & BATTERY BRAND
			$result = $this->Power_Model->get_rec_bat();
			echo json_encode($result);
		}
	}
	
	public function get_all_db_dropdowns() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			#GET RECTIFIER MODEL & BATTERY BRAND
			$result = $this->Power_Model->get_all_db_dropdowns();
			echo json_encode($result);
		}
	}

	public function get_prop_list() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Power_Model->get_prop_list();
			echo json_encode($result);
		}
	}

	public function get_proposals() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Power_Model->get_proposals();
			echo json_encode($result);
		}
	}

	public function get_acu_model() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Power_Model->get_acu_model();
			echo json_encode($result);
		}
	}

	public function del_prop() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Power_Model->del_prop();
			echo json_encode($result);
		}
	}

	public function del_all_prop() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Power_Model->del_all_prop();
			echo json_encode($result);
		}
	}

	public function edit_prop() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Power_Model->edit_prop();
			echo json_encode($result);
		}
	}

	public function get_power_completion() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Power_Model->get_completion();
			echo json_encode($result);
		}
	}

	public function get_pchart_completion() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Power_Model->get_chart_completion();
			echo json_encode($result);
		}
	}

	public function get_assessments() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Power_Model->get_assessments();
			echo json_encode($result);
		}
	}

	public function get_power_reports() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Power_Model->get_power_reports();
			echo json_encode($result);
		}
	}

	public function get_power_profile() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Power_Model->get_power_profile();
			echo json_encode($result);
		}
	}
}
