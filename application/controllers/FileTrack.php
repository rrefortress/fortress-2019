<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FileTrack extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', '512M');
		$this->load->model('FileTrackModel');
		$this->load->model('Admin_Model');
		$this->load->library('excel');
	}

	public function file_tracking() {
		$data['title'] = 'Tracker';
		$access_level = $this->session->userdata('access_level');
		$dep = $this->session->userdata('department');

		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = array('map',
							'Trackers/maps',
							'Trackers/fileTracker',
							'Trackers/awe',
							'Trackers/nominationsTracker',
							'Trackers/pmoTracker',
							'Trackers/surveyTracker',
							'Trackers/ibsTracker',
							'Trackers/tssrTracker',
							'calendar'
							);
 		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if(empty($access_level))
		{
			redirect(base_url('login'));
		}
		else
		{
			if ($dep == 'RRE' || $access_level == 'admin') {
				$this->load->view('RRE/FileTracker/file_tracker_page', $data);
				$this->load->view('/layout/footer', $data);
			} else {
				redirect(base_url('restricted'));
			}

		}
	}

	public function set_calendar() {
		$sid = $this->input->post('id');
		$data = $this->FileTrackModel->set_calendar($sid);
		echo json_encode($data);
	}

	public function getTrackUsers()
	{
		$data = $this->FileTrackModel->getTrackUsers();
		echo json_encode($data);
	}

	public function get_tracks() {
		$json = array(
				'surveys'     => $this->FileTrackModel->get_Surveys_Track(),
				'nominations' => $this->FileTrackModel->get_Nominations_Track(),
				'awe'         => $this->FileTrackModel->get_AWE_TRACK(),
				'pmo'         => $this->FileTrackModel->get_PMO_TRACK(),
				'DDD'         => [],
				'IBS'         => $this->FileTrackModel->get_IBS_Track(),
				'TSSR'        => $this->FileTrackModel->get_TSSR_Track(),
				'CLP'         => array(),
				'Baseline'    => array(),
				'Sales_Map'   => array(),
				'Complaints'  => array(),
				'Events'      => array(),
		);
		echo json_encode($json);
	}

	public function get_Nominations_Track($row_num=0) {
		$filter = $this->input->post('name');
		$search = $this->input->get('search');
		$data = $this->FileTrackModel->get_Nominations_Track($filter, $search, $row_num);
		echo json_encode($data);
	}

	public function get_AWE_TRACK($row_num=0) {
		$filter = $this->input->post('name');
		$search = $this->input->get('search');
		$data = $this->FileTrackModel->get_AWE_TRACK($filter, $search, $row_num);
		echo json_encode($data);
	}

	public function get_PMO_TRACK($row_num=0) {
		$filter = $this->input->post('name');
		$search = $this->input->get('search');
		$data = $this->FileTrackModel->get_PMO_TRACK($search, $row_num);
		echo json_encode($data);
	}

	public function get_Surveys_Track($row_num=0) {
		$filter = $this->input->get('name');
		$search = $this->input->get('search');
		$project = $this->input->post('project');
		$data = $this->FileTrackModel->get_Surveys_Track($filter, $project, $search, $row_num);
		echo json_encode($data);
	}

	public function get_IBS_TRACK($row_num=0) {
		$filter = $this->input->post('name');
		$search = $this->input->get('search');
		$data = $this->FileTrackModel->get_IBS_TRACK($filter, $search, $row_num);
		echo json_encode($data);
	}

	public function get_TSSR_Track($row_num=0) {
		$search = $this->input->get('search');
		$data = $this->FileTrackModel->get_TSSR_Track($search, $row_num);
		echo json_encode($data);
	}

	public function save_nomination_track() {
		$status = $this->input->post('nom_status');
		$inputs = array();

		for ($count = 0; $count < count($_POST["nom_site_name"]); $count++) {

			if ($status == 0) {
				$inputs[] = array(
					'SITE_NAME'   => $_POST['nom_site_name'][$count],
					'REGION'     => $_POST['nom_region'][$count],
					'PROVINCE'  => $_POST['nom_province'][$count],
					'TOWN_CITY'  => $_POST['nom_town_city'][$count],
					'Latitude' 	     => $_POST['nom_latitude'][$count],
					'Longitude'        => $_POST['nom_longtitude'][$count],
					'Tagging'         => $_POST['nom_tagging'][$count],
					'Site_Type'         => $_POST['nom_site_type'][$count],
					'Remarks'        => $_POST['nom_remarks'][$count]
				);
			} else {
				$inputs[] = array(
					'SITE_NAME'   => $_POST['nom_site_name'][$count],
					'REGION'     => $_POST['nom_region'][$count],
					'PROVINCE'  => $_POST['nom_province'][$count],
					'TOWN_CITY'  => $_POST['nom_town_city'][$count],
					'Latitude' 	     => $_POST['nom_latitude'][$count],
					'Longitude'        => $_POST['nom_longtitude'][$count],
					'Tagging'         => $_POST['nom_tagging'][$count],
					'Site_Type'         => $_POST['nom_site_type'][$count],
					'Remarks'        => $_POST['nom_remarks'][$count],
					'id'             => $_POST['nom_iid'][$count]
				);
			}
		}
		$data = $this->FileTrackModel->save_nom_track($inputs, $status);
		echo json_encode($data);
	}

	public function nom_upload() {
		$arrayHeader = array("A" => 'SITE NAME',
												 "B" => "REGION",
												 "C" => 'PROVINCE',
												 "D" => 'TOWN/CITY',
												 "E" => "Latitude",
												 "F" => 'Longitude',
												 "G" => "Tagging",
												 "H" => 'Site Type',
												 "I" => 'Remarks');

		if (isset($_FILES["nom_file"]["name"])) {

			$path = $_FILES["nom_file"]["tmp_name"];
			$object= PHPExcel_IOFactory::load($path);

			#GET FORMAT HEADER OF EXCEL
			$cell_collection = $object->getActiveSheet()->getCellCollection();
			foreach ($cell_collection as $cell) {
				$column = $object->getActiveSheet()->getCell($cell)->getColumn();
				$row = $object->getActiveSheet()->getCell($cell)->getRow();
				$data_value = $object->getActiveSheet()->getCell($cell)->getValue();
				//The header will/should be in row 1 only. of course, this can be modified to suit your need.
				if ($row == 1) {
					$header[$row][$column] = $data_value;
				}
			}

			#CHECK IF EXCEL IMPORT MATCHES TO DATABASE FORMAT OF EXCEL
			if ($arrayHeader == $header[1]) {
				foreach ($object->getWorksheetIterator() as $worksheet) {
					$highestRow = $worksheet->getHighestRow();
					$highestColumn = $worksheet->getHighestColumn();
					for ($row = 2; $row <= $highestRow; $row++) {
						$data[] = array(
							 'SITE_NAME' => $worksheet->getCellByColumnAndRow(0, $row)->getValue(),
						   'REGION' => $worksheet->getCellByColumnAndRow(1, $row)->getValue(),
						   'PROVINCE' => $worksheet->getCellByColumnAndRow(2, $row)->getValue(),
							 'TOWN_CITY' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
							 'Latitude' => $worksheet->getCellByColumnAndRow(4, $row)->getValue(),
							 'Longitude' => $worksheet->getCellByColumnAndRow(5, $row)->getValue(),
							 'Tagging' => $worksheet->getCellByColumnAndRow(6, $row)->getValue(),
							 'Site_Type' => $worksheet->getCellByColumnAndRow(7, $row)->getValue(),
							 'Remarks' => $worksheet->getCellByColumnAndRow(8, $row)->getValue()
						);
					}
				}

				$geoarea = $this->session->userdata('geoarea');
				$table_name = 'nominations_'.$geoarea;

				$this->FileTrackModel->insertDataByBatch($data, $table_name);
				$json = array(
					'title' => 'Success',
					'msg'   => 'Successfully inserted.',
					'type'  => 'success',
					'data'  => $this->FileTrackModel->get_details($table_name)
 				);
			} else {
				$json = array(
					'title' => 'Ooops!',
					'msg'   => 'Format excel import does not match to Database Format.',
					'type'  => 'error'
				);
			}
		} else {
			$json = array(
				'title' => 'Ooops!',
				'msg'   => 'Something went wrong.',
				'type'  => 'error'
			);
		}
		echo json_encode($json);
	}

	public function del_nom_track() {
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'nominations_'.$geoarea;
		$status = $this->input->post('status');
		for ($count = 0; $count < count($_POST["id"]); $count++) {
			$inputs[] = array(
				'id' => $_POST['id'][$count]
			);
		}

		$data = $this->FileTrackModel->remove_track($inputs, $status, $table_name);
		echo json_encode($data);
	}

	public function get_Surveys_Track_name() {
		$name = $this->input->post('name');
		$project = $this->input->post('project');
		$data = $this->FileTrackModel->get_Surveys_Track($name, $project);
		echo json_encode($data);
	}

	public function save_awe_track() {
		$status = $this->input->post('awe_status');
		$inputs = array();
		for ($count = 0; $count < count($_POST["awe_site_name"]); $count++) {
				if ($status == 0) {

						$inputs[] = array(
							'SERIAL_NUMBER'    => $_POST['awe_serial_number'][$count],
							'SITE_NAME'    		 => $_POST['awe_site_name'][$count],
							'REGION' 					  => $_POST['awe_region'][$count],
							'PROVINCE'  			  => $_POST['awe_province'][$count],
							'TOWN_CITY'        => $_POST['awe_town_city'][$count],
							'Town_Priority'         => $_POST['awe_town_priority'][$count],
							'Latitude'        => $_POST['awe_latitude'][$count],
							'Longitude'        => $_POST['awe_longtitude'][$count],
							'VENDOR'        => $_POST['awe_vendor'][$count],
							'Program_Tag'        => $_POST['awe_program_tag'][$count],
							'PLANNED_TECH'        => $_POST['awe_planned_tech'][$count],
							'Tagging'        => $_POST['awe_tagging'][$count],
							'Site_Type'        => $_POST['awe_site_type'][$count]
						);

				} else {
						$inputs[] = array(
							'SERIAL_NUMBER'    => $_POST['awe_serial_number'][$count],
							'SITE_NAME'    		 => $_POST['awe_site_name'][$count],
							'REGION' 					  => $_POST['awe_region'][$count],
							'PROVINCE'  			  => $_POST['awe_province'][$count],
							'TOWN_CITY'        => $_POST['awe_town_city'][$count],
							'Town_Priority'         => $_POST['awe_town_priority'][$count],
							'Latitude'        => $_POST['awe_latitude'][$count],
							'Longitude'        => $_POST['awe_longtitude'][$count],
							'VENDOR'        => $_POST['awe_vendor'][$count],
							'Program_Tag'        => $_POST['awe_program_tag'][$count],
							'PLANNED_TECH'        => $_POST['awe_planned_tech'][$count],
							'Tagging'        => $_POST['awe_tagging'][$count],
							'Site_Type'        => $_POST['awe_site_type'][$count],
							'id'             => $_POST['awe_iid'][$count]
						);
				}
		}

		$data = $this->FileTrackModel->save_awe_track($inputs, $status);
		echo json_encode($data);
	}

	public function del_awe_track() {
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'awe_'.$geoarea;
		$status = $this->input->post('status');
		for ($count = 0; $count < count($_POST["id"]); $count++) {
			$inputs[] = array(
				'id' => $_POST['id'][$count]
			);
		}

		$data = $this->FileTrackModel->remove_track($inputs, $status, $table_name);
		echo json_encode($data);
	}

	public function awe_upload() {
		$arrayHeader = array("A" => 'SERIAL NUMBER',
												 "B" => "SITE NAME",
												 "C" => 'REGION',
												 "D" => 'PROVINCE',
												 "E" => "TOWN/CITY",
												 "F" => 'Baluarte Tagging',
												 "G" => "Priority Tag",

												 "H" => 'Town Priority',
												 "I" => 'Latitude',
												 "J" => 'Longitude',
												 "K" => 'VENDOR',
												 "L" => 'Program Tag',
												 "M" => 'PLANNED TECH',
												 "N" => 'Tagging',
												 "O" => 'Site Type',
												 "P" => 'Final Rank Town Ranking',
											 );

		if (isset($_FILES["awe_file"]["name"])) {

			$path = $_FILES["awe_file"]["tmp_name"];
			$object= PHPExcel_IOFactory::load($path);

			#GET FORMAT HEADER OF EXCEL
			$cell_collection = $object->getActiveSheet()->getCellCollection();
			foreach ($cell_collection as $cell) {
				$column = $object->getActiveSheet()->getCell($cell)->getColumn();
				$row = $object->getActiveSheet()->getCell($cell)->getRow();
				$data_value = $object->getActiveSheet()->getCell($cell)->getValue();
				//The header will/should be in row 1 only. of course, this can be modified to suit your need.
				if ($row == 1) {
					$header[$row][$column] = $data_value;
				}
			}

			#CHECK IF EXCEL IMPORT MATCHES TO DATABASE FORMAT OF EXCEL
			if ($arrayHeader == $header[1]) {
				foreach ($object->getWorksheetIterator() as $worksheet) {
					$highestRow = $worksheet->getHighestRow();
					$highestColumn = $worksheet->getHighestColumn();
					for ($row = 2; $row <= $highestRow; $row++) {
						$data[] = array(
							 'SERIAL_NUMBER' => $worksheet->getCellByColumnAndRow(0, $row)->getValue(),
							 'SITE_NAME' => $worksheet->getCellByColumnAndRow(1, $row)->getValue(),
							 'REGION' => $worksheet->getCellByColumnAndRow(2, $row)->getValue(),
							 'PROVINCE' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
							 'TOWN_CITY' => $worksheet->getCellByColumnAndRow(4, $row)->getValue(),
							 'Baluarte_Tagging' => $worksheet->getCellByColumnAndRow(5, $row)->getValue(),
							 'Priority_Tag' => $worksheet->getCellByColumnAndRow(6, $row)->getValue(),

							 'Town_Priority' => $worksheet->getCellByColumnAndRow(7, $row)->getValue(),
							 'Latitude' => $worksheet->getCellByColumnAndRow(8, $row)->getValue(),
							 'Longitude' => $worksheet->getCellByColumnAndRow(9, $row)->getValue(),
							 'VENDOR' => $worksheet->getCellByColumnAndRow(10, $row)->getValue(),
							 'Program_Tag' => $worksheet->getCellByColumnAndRow(11, $row)->getValue(),
							 'PLANNED_TECH' => $worksheet->getCellByColumnAndRow(12, $row)->getValue(),
							 'Tagging' => $worksheet->getCellByColumnAndRow(13, $row)->getValue(),
							 'Site_Type' => $worksheet->getCellByColumnAndRow(14, $row)->getValue(),
							 'Final_Rank_Town_Ranking' => $worksheet->getCellByColumnAndRow(15, $row)->getValue(),
						);
					}
				}

				$geoarea = $this->session->userdata('geoarea');
				$table_name = 'awe_'.$geoarea;

				$this->FileTrackModel->insertDataByBatch($data, $table_name);
				$json = array(
					'title' => 'Success',
					'msg'   => 'Successfully inserted.',
					'type'  => 'success',
					'data'  => $this->FileTrackModel->get_details($table_name)
				);
			} else {
				$json = array(
					'title' => 'Ooops!',
					'msg'   => 'Format excel import does not match to Database Format.',
					'type'  => 'error'
				);
			}
		} else {
			$json = array(
				'title' => 'Ooops!',
				'msg'   => 'Something went wrong.',
				'type'  => 'error'
			);
		}
		echo json_encode($json);
	}

	public function pmo_upload() {
		$arrayHeader = array("A" => 'Serial Number',
												 "B" => "Budget Tagging",
												 "C" => 'REGION',
												 "D" => 'SEARCHRING NAME',
												 "E" => "Baluarte Tagging",
												 "F" => 'Clutter Type',
												 "G" => "Priority Tagging",
												 "H" => 'Program',
												 "I" => 'Project Phase',
												 "J" => 'Batch (AEPM)',
												 "K" => 'Batch (PMO)',
												 "L" => 'Budget Scope',
												 "M" => 'Budget Code (Access)',
												 "N" => 'Budget Code (Facilities)',
												 "O" => 'Budget Code (Transport)',
												 "P" => 'Lead Vendor',
												 "Q" => 'Solution',
												 "R" => 'Coverage',
												 "S" => 'Scope',
												 "T" => 'Wireless Plan',
												 "U" => 'No. of Sectors',
												 "V" => 'NTG or BUSINESS?',
												 "W" => 'Nomination Driver',
											 );

		if (isset($_FILES["pmo_file"]["name"])) {

			$path = $_FILES["pmo_file"]["tmp_name"];
			$object= PHPExcel_IOFactory::load($path);

			#GET FORMAT HEADER OF EXCEL
			$cell_collection = $object->getActiveSheet()->getCellCollection();
			foreach ($cell_collection as $cell) {
				$column = $object->getActiveSheet()->getCell($cell)->getColumn();
				$row = $object->getActiveSheet()->getCell($cell)->getRow();
				$data_value = $object->getActiveSheet()->getCell($cell)->getValue();
				//The header will/should be in row 1 only. of course, this can be modified to suit your need.
				if ($row == 1) {
					$header[$row][$column] = $data_value;
				}
			}

			#CHECK IF EXCEL IMPORT MATCHES TO DATABASE FORMAT OF EXCEL
			if ($arrayHeader == $header[1]) {
				foreach ($object->getWorksheetIterator() as $worksheet) {
					$highestRow = $worksheet->getHighestRow();
					$highestColumn = $worksheet->getHighestColumn();
					for ($row = 2; $row <= $highestRow; $row++) {
						$data[] = array(
							 'Serial_Number' => $worksheet->getCellByColumnAndRow(0, $row)->getValue(),
							 'Budget_Tagging' => $worksheet->getCellByColumnAndRow(1, $row)->getValue(),
							 'REGION' => $worksheet->getCellByColumnAndRow(2, $row)->getValue(),
							 'SITE_NAME' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
							 'Baluarte_Tagging' => $worksheet->getCellByColumnAndRow(4, $row)->getValue(),
							 'Clutter_Type' => $worksheet->getCellByColumnAndRow(5, $row)->getValue(),
							 'Priority_Tagging' => $worksheet->getCellByColumnAndRow(6, $row)->getValue(),
							 'Program' => $worksheet->getCellByColumnAndRow(7, $row)->getValue(),
							 'Project_Phase' => $worksheet->getCellByColumnAndRow(8, $row)->getValue(),
							 'Batch_AEPM' => $worksheet->getCellByColumnAndRow(9, $row)->getValue(),
							 'Batch_PMO' => $worksheet->getCellByColumnAndRow(10, $row)->getValue(),
							 'Budget_Scope' => $worksheet->getCellByColumnAndRow(11, $row)->getValue(),
							 'Budget_Code_Access' => $worksheet->getCellByColumnAndRow(12, $row)->getValue(),
							 'Budget_Code_Facilities' => $worksheet->getCellByColumnAndRow(13, $row)->getValue(),
							 'Budge_Code_Transport' => $worksheet->getCellByColumnAndRow(14, $row)->getValue(),
							 'Lead_Vendor' => $worksheet->getCellByColumnAndRow(15, $row)->getValue(),
							 'Solution' => $worksheet->getCellByColumnAndRow(16, $row)->getValue(),
							 'Coverage' => $worksheet->getCellByColumnAndRow(17, $row)->getValue(),
							 'Scope' => $worksheet->getCellByColumnAndRow(18, $row)->getValue(),
							 'Wireless_Plan' => $worksheet->getCellByColumnAndRow(19, $row)->getValue(),
							 'No_of_Sectors' => $worksheet->getCellByColumnAndRow(20, $row)->getValue(),
							 'NTG_or_BUSINESS' => $worksheet->getCellByColumnAndRow(21, $row)->getValue(),
							 'Nomination_Driver' => $worksheet->getCellByColumnAndRow(22, $row)->getValue()
						);
					}
				}

				$geoarea = $this->session->userdata('geoarea');
				$table_name = 'pmo_'.$geoarea;

				$this->FileTrackModel->insertDataByBatch($data, $table_name);
				$json = array(
					'title' => 'Success',
					'msg'   => 'Successfully inserted.',
					'type'  => 'success',
					'data'  => $this->FileTrackModel->get_details($table_name)
				);
			} else {
				$json = array(
					'title' => 'Ooops!',
					'msg'   => 'Format excel import does not match to Database Format.',
					'type'  => 'error'
				);
			}
		} else {
			$json = array(
				'title' => 'Ooops!',
				'msg'   => 'Something went wrong.',
				'type'  => 'error'
			);
		}
		echo json_encode($json);
	}

	public function save_pmo_track() {
		$status = $this->input->post('pmo_status');
		$inputs = array();
		for ($count = 0; $count < count($_POST["pmo_site_name"]); $count++) {
				if ($status == 0) {
						$inputs[] = array(
							'Serial_Number'  		   => $_POST['pmo_serial_number'][$count],
							'SITE_NAME'    		     => $_POST['pmo_site_name'][$count],
							'REGION' 					 	   => $_POST['pmo_region'][$count],
							'Budget_Tagging'  		 => $_POST['pmo_budget_tagging'][$count],
							'Program'        			 => $_POST['pmo_program'][$count],
							'Project_Phase'        => $_POST['pmo_project_phase'][$count],
							'Lead_Vendor'       	 => $_POST['pmo_lead_vendor'][$count],
							'Solution'        		 => $_POST['pmo_solution'][$count],
							'Coverage'       			 => $_POST['pmo_coverage'][$count],
							'Scope'        				 => $_POST['pmo_scope'][$count],
							'Wireless_Plan'        => $_POST['pmo_wireless_plan'][$count],
							'No_of_Sectors'        => $_POST['pmo_sectors'][$count]
						);
				} else {
						$inputs[] = array(
							'Serial_Number'  		   => $_POST['pmo_serial_number'][$count],
							'SITE_NAME'    		     => $_POST['pmo_site_name'][$count],
							'REGION' 					 	   => $_POST['pmo_region'][$count],
							'Budget_Tagging'  		 => $_POST['pmo_budget_tagging'][$count],
							'Program'        			 => $_POST['pmo_program'][$count],
							'Project_Phase'        => $_POST['pmo_project_phase'][$count],
							'Lead_Vendor'       	 => $_POST['pmo_lead_vendor'][$count],
							'Solution'        		 => $_POST['pmo_solution'][$count],
							'Coverage'       			 => $_POST['pmo_coverage'][$count],
							'Scope'        				 => $_POST['pmo_scope'][$count],
							'Wireless_Plan'        => $_POST['pmo_wireless_plan'][$count],
							'No_of_Sectors'        => $_POST['pmo_sectors'][$count],
							'id'                   => $_POST['pmo_iid'][$count]
						);
				}
		}

		$data = $this->FileTrackModel->save_pmo_track($inputs, $status);
		echo json_encode($data);
	}

	public function del_pmo_track() {
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'pmo_'.$geoarea;
		$status = $this->input->post('status');
		for ($count = 0; $count < count($_POST["id"]); $count++) {
			$inputs[] = array(
				'id' => $_POST['id'][$count]
			);
		}

		$data = $this->FileTrackModel->remove_track($inputs, $status, $table_name);
		echo json_encode($data);
	}

	public function survey_upload() {
		$arrayHeader = array("A" => 'Search Ring Name',
												 "B" => 'Survey Date',
												 "C" => 'Project',
												 "D" => 'RRE Representative',
												 "E" => 'RFE Representative',
												 "F" => 'SAQ Representative',
												 "G" => 'Category',
												 "H" => 'Vendor',
												 "I" => 'Site Status',
												 "J" => 'Remarks',
												 "K" => 'Longtitude',
												 "L" => 'Latitude'
											 );

		if (isset($_FILES["survey_file"]["name"])) {

			$path = $_FILES["survey_file"]["tmp_name"];
			$object= PHPExcel_IOFactory::load($path);

			#GET FORMAT HEADER OF EXCEL
			$cell_collection = $object->getActiveSheet()->getCellCollection();
			foreach ($cell_collection as $cell) {
				$column = $object->getActiveSheet()->getCell($cell)->getColumn();
				$row = $object->getActiveSheet()->getCell($cell)->getRow();
				$data_value = $object->getActiveSheet()->getCell($cell)->getValue();
				//The header will/should be in row 1 only. of course, this can be modified to suit your need.
				if ($row == 1) {
					$header[$row][$column] = $data_value;
				}
			}

			#CHECK IF EXCEL IMPORT MATCHES TO DATABASE FORMAT OF EXCEL
			if ($arrayHeader == $header[1]) {
				foreach ($object->getWorksheetIterator() as $worksheet) {
					$highestRow = $worksheet->getHighestRow();
					$highestColumn = $worksheet->getHighestColumn();
					for ($row = 2; $row <= $highestRow; $row++) {
						$date = date("Y-m-d", strtotime($worksheet->getCellByColumnAndRow(1, $row)->getValue()));

						$data[] = array(
							 'SITE_NAME' => $worksheet->getCellByColumnAndRow(0, $row)->getValue(),
							 'Survey_Date' => $date,
							 'Project' => $worksheet->getCellByColumnAndRow(2, $row)->getValue(),
							 'RRE_Representative' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
							 'RFE_Representative' => $worksheet->getCellByColumnAndRow(4, $row)->getValue(),
							 'SAQ_Representative' => $worksheet->getCellByColumnAndRow(5, $row)->getValue(),
							 'Category' => $worksheet->getCellByColumnAndRow(6, $row)->getValue(),
							 'Vendor' => $worksheet->getCellByColumnAndRow(7, $row)->getValue(),
							 'Site_Status' => $worksheet->getCellByColumnAndRow(8, $row)->getValue(),
							 'Remarks' => $worksheet->getCellByColumnAndRow(9, $row)->getValue(),
							 'longtitude' => $worksheet->getCellByColumnAndRow(10, $row)->getValue(),
							 'latitude' => $worksheet->getCellByColumnAndRow(11, $row)->getValue()
						);
					}
				}

				$geoarea = $this->session->userdata('geoarea');
				$table_name = 'survey_'.$geoarea;

				$this->FileTrackModel->insertDataByBatch($data, $table_name);
				$json = array(
					'title' => 'Success',
					'msg'   => 'Successfully inserted.',
					'type'  => 'success',
					'data'  => $this->FileTrackModel->get_details($table_name)
				);
			} else {
				$json = array(
					'title' => 'Ooops!',
					'msg'   => 'Format excel import does not match to Database Format.',
					'type'  => 'error'
				);
			}
		} else {
			$json = array(
				'title' => 'Ooops!',
				'msg'   => 'Something went wrong.',
				'type'  => 'error'
			);
		}
		echo json_encode($json);
	}

	public function save_ibs_track() {
		$status = $this->input->post('ibs_status');
		$inputs = array();
		for ($count = 0; $count < count($_POST["ibs_site_name"]); $count++) {
				if ($status == 0) {
						$inputs[] = array(
							'SITE_NAME'    		     => $_POST['ibs_site_name'][$count],
							'Building_Name' 			 => $_POST['ibs_bldg_name'][$count],
							'Building_Type'  		   => $_POST['ibs_bldg_type'][$count],
							'PLA_ID'        			 => $_POST['ibs_pla_id'][$count],
							'Longitude'        		 => $_POST['ibs_longtitude'][$count],
							'Latitude'       	     => $_POST['ibs_latitude'][$count],
							'Region'        		   => $_POST['ibs_region'][$count],
							'Province'       			 => $_POST['ibs_province'][$count],
							'Municipality_City'    => $_POST['ibs_mun_city'][$count],
							'address'        			 => $_POST['ibs_address'][$count]
						);
				} else {
						$inputs[] = array(
							'SITE_NAME'    		     => $_POST['ibs_site_name'][$count],
							'Building_Name' 			 => $_POST['ibs_bldg_name'][$count],
							'Building_Type'  		   => $_POST['ibs_bldg_type'][$count],
							'PLA_ID'        			 => $_POST['ibs_pla_id'][$count],
							'Longitude'        		 => $_POST['ibs_longtitude'][$count],
							'Latitude'       	     => $_POST['ibs_latitude'][$count],
							'Region'        		   => $_POST['ibs_region'][$count],
							'Province'       			 => $_POST['ibs_province'][$count],
							'Municipality_City'    => $_POST['ibs_mun_city'][$count],
							'address'        			 => $_POST['ibs_address'][$count],
							'id'                   => $_POST['ibs_iid'][$count]
						);
				}
		}

		$data = $this->FileTrackModel->save_ibs_track($inputs, $status);
		echo json_encode($data);
	}

	public function ibs_upload() {
		$arrayHeader = array("A" => 'BCF Name',
												 "B" => 'Building Name',
												 "C" => 'Building Type',
												 "D" => 'PLA ID',
												 "E" => 'Longitude',
												 "F" => 'Latitude',
												 "G" => 'Region',
												 "H" => 'Province',
												 "I" => 'Municipality / City',
												 "J" => 'address'
											 );

		if (isset($_FILES["ibs_file"]["name"])) {

			$path = $_FILES["ibs_file"]["tmp_name"];
			$object= PHPExcel_IOFactory::load($path);

			#GET FORMAT HEADER OF EXCEL
			$cell_collection = $object->getActiveSheet()->getCellCollection();
			foreach ($cell_collection as $cell) {
				$column = $object->getActiveSheet()->getCell($cell)->getColumn();
				$row = $object->getActiveSheet()->getCell($cell)->getRow();
				$data_value = $object->getActiveSheet()->getCell($cell)->getValue();
				//The header will/should be in row 1 only. of course, this can be modified to suit your need.
				if ($row == 1) {
					$header[$row][$column] = $data_value;
				}
			}

			#CHECK IF EXCEL IMPORT MATCHES TO DATABASE FORMAT OF EXCEL
			if ($arrayHeader == $header[1]) {
				foreach ($object->getWorksheetIterator() as $worksheet) {
					$highestRow = $worksheet->getHighestRow();
					$highestColumn = $worksheet->getHighestColumn();
					for ($row = 2; $row <= $highestRow; $row++) {
						$data[] = array(
							 'SITE_NAME' => $worksheet->getCellByColumnAndRow(0, $row)->getValue(),
							 'Building_Name' => $worksheet->getCellByColumnAndRow(1, $row)->getValue(),
							 'Building_Type' => $worksheet->getCellByColumnAndRow(2, $row)->getValue(),
							 'PLA_ID' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
							 'Longitude' => $worksheet->getCellByColumnAndRow(4, $row)->getValue(),
							 'Latitude' => $worksheet->getCellByColumnAndRow(5, $row)->getValue(),
							 'Region' => $worksheet->getCellByColumnAndRow(6, $row)->getValue(),
							 'Province' => $worksheet->getCellByColumnAndRow(7, $row)->getValue(),
							 'Municipality_City' => $worksheet->getCellByColumnAndRow(8, $row)->getValue(),
							 'address' => $worksheet->getCellByColumnAndRow(9, $row)->getValue()
						);
					}
				}

				$geoarea = $this->session->userdata('geoarea');
				$table_name = 'ibs_'.$geoarea;

				$this->FileTrackModel->insertDataByBatch($data, $table_name);
				$json = array(
					'title' => 'Success',
					'msg'   => 'Successfully inserted.',
					'type'  => 'success',
					'data'  => $this->FileTrackModel->get_details($table_name)
				);
			} else {
				$json = array(
					'title' => 'Ooops!',
					'msg'   => 'Format excel import does not match to Database Format.',
					'type'  => 'error'
				);
			}
		} else {
			$json = array(
				'title' => 'Ooops!',
				'msg'   => 'Something went wrong.',
				'type'  => 'error'
			);
		}
		echo json_encode($json);
	}

	public function del_ibs_track() {
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'ibs_'.$geoarea;
		$status = $this->input->post('status');
		for ($count = 0; $count < count($_POST["id"]); $count++) {
			$inputs[] = array(
				'id' => $_POST['id'][$count]
			);
		}

		$data = $this->FileTrackModel->remove_track($inputs, $status, $table_name);
		echo json_encode($data);
	}

	public function save_tssr_track() {
		$status = $this->input->post('tssr_status');
		$inputs = array();
		for ($count = 0; $count < count($_POST["tssr_site_name"]); $count++) {
				if ($status == 0) {
						$inputs[] = array(
							'site_name'    		     => $_POST['tssr_site_name'][$count],
							'region' 			 				 => $_POST['tssr_region'][$count],
							'pla_id'  		   			 => $_POST['tssr_pla_id'][$count],
							'coverage'        		 => $_POST['tssr_coverage'][$count],
							'location'        		 => $_POST['tssr_address'][$count],
							'province'       	     => $_POST['tssr_province'][$count],
							'town'        		  	 => $_POST['tssr_town'][$count],
							'brgy'        		  	 => $_POST['tssr_brgy'][$count],
							'town_psgc'    				 => $_POST['tssr_psgc'][$count],
							'latitude'        		 => $_POST['tssr_latitude'][$count],
							'longitude'        		 => $_POST['tssr_longtitude'][$count],
							'baluarte_tagging'     => $_POST['tssr_baluarte_tagging'][$count],
							'clutter_type'         => $_POST['tssr_clutter_type'][$count]
						);
				} else {
						$inputs[] = array(
							'site_name'    		     => $_POST['tssr_site_name'][$count],
							'region' 			 				 => $_POST['tssr_region'][$count],
							'pla_id'  		   			 => $_POST['tssr_pla_id'][$count],
							'coverage'        		 => $_POST['tssr_coverage'][$count],
							'location'        		 => $_POST['tssr_address'][$count],
							'province'       	     => $_POST['tssr_province'][$count],
							'town'        		  	 => $_POST['tssr_town'][$count],
							'brgy'        		  	 => $_POST['tssr_brgy'][$count],
							'town_psgc'    				 => $_POST['tssr_psgc'][$count],
							'latitude'        		 => $_POST['tssr_latitude'][$count],
							'longitude'        		 => $_POST['tssr_longtitude'][$count],
							'baluarte_tagging'     => $_POST['tssr_baluarte_tagging'][$count],
							'clutter_type'         => $_POST['tssr_clutter_type'][$count],
							'id'                   => $_POST['tssr_iid'][$count]
						);
				}
		}

		$data = $this->FileTrackModel->save_tssr_track($inputs, $status);
		echo json_encode($data);
	}

	public function del_tssr_track() {
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'tssr_'.$geoarea;
		$status = $this->input->post('status');
		for ($count = 0; $count < count($_POST["id"]); $count++) {
			$inputs[] = array(
				'id' => $_POST['id'][$count]
			);
		}

		$data = $this->FileTrackModel->remove_track($inputs, $status, $table_name);
		echo json_encode($data);
	}

	public function tssr_upload() {
		$arrayHeader = array(
												"A" => 'region',
												 "B" => 'site name',
												 "C" => 'pla id',
												 "D" => 'coverage',
												 "E" => 'location',
												 "F" => 'province',
												 "G" => 'town',
												 "H" => 'brgy',
												 "I" => 'town psgc',
												 "J" => 'longitude',
												 "K" => 'latitude',
												 "L" => 'baluarte tagging',
												 "M" => 'clutter type',
											 );

		if (isset($_FILES["tssrs_file"]["name"])) {

			$path = $_FILES["tssrs_file"]["tmp_name"];
			$object= PHPExcel_IOFactory::load($path);

			#GET FORMAT HEADER OF EXCEL
			$cell_collection = $object->getActiveSheet()->getCellCollection();
			foreach ($cell_collection as $cell) {
				$column = $object->getActiveSheet()->getCell($cell)->getColumn();
				$row = $object->getActiveSheet()->getCell($cell)->getRow();
				$data_value = $object->getActiveSheet()->getCell($cell)->getValue();
				//The header will/should be in row 1 only. of course, this can be modified to suit your need.
				if ($row == 1) {
					$header[$row][$column] = $data_value;
				}
			}

			#CHECK IF EXCEL IMPORT MATCHES TO DATABASE FORMAT OF EXCEL
			if ($arrayHeader == $header[1]) {
				foreach ($object->getWorksheetIterator() as $worksheet) {
					$highestRow = $worksheet->getHighestRow();
					$highestColumn = $worksheet->getHighestColumn();
					for ($row = 2; $row <= $highestRow; $row++) {
						$data[] = array(
							 "region" => $worksheet->getCellByColumnAndRow(0, $row)->getValue(),
							 'site_name' => $worksheet->getCellByColumnAndRow(1, $row)->getValue(),
							 'pla_id' => $worksheet->getCellByColumnAndRow(2, $row)->getValue(),
							 'coverage' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
							 'location' => $worksheet->getCellByColumnAndRow(4, $row)->getValue(),
							 'province' => $worksheet->getCellByColumnAndRow(5, $row)->getValue(),
							 'town' => $worksheet->getCellByColumnAndRow(6, $row)->getValue(),
							 'brgy' => $worksheet->getCellByColumnAndRow(7, $row)->getValue(),
							 'town_psgc' => $worksheet->getCellByColumnAndRow(8, $row)->getValue(),
							 'longitude' => $worksheet->getCellByColumnAndRow(9, $row)->getValue(),
							 'latitude' => $worksheet->getCellByColumnAndRow(10, $row)->getValue(),
							 'baluarte_tagging' => $worksheet->getCellByColumnAndRow(11, $row)->getValue(),
							 'clutter_type' => $worksheet->getCellByColumnAndRow(12, $row)->getValue()
						);
					}
				}

				$geoarea = $this->session->userdata('geoarea');
				$table_name = 'tssr_'.$geoarea;

				$this->FileTrackModel->insertDataByBatch($data, $table_name);
				$json = array(
					'title' => 'Success',
					'msg'   => 'Successfully inserted.',
					'type'  => 'success',
					'data'  => $this->FileTrackModel->get_details($table_name)
				);
			} else {
				$json = array(
					'title' => 'Ooops!',
					'msg'   => 'Format excel import does not match to Database Format.',
					'type'  => 'error'
				);
			}
		} else {
			$json = array(
				'title' => 'Ooops!',
				'msg'   => 'Something went wrong.',
				'type'  => 'error'
			);
		}
		echo json_encode($json);
	}

	public function save_survey_track() {
		$data = $this->FileTrackModel->save_survey_track();
		$data['activities'] = $this->Admin_Model->getActivites();
		echo json_encode($data);
	}

	public function del_survey_track() {
		$geoarea = $this->session->userdata('geoarea');
		$table_name = 'survey_'.$geoarea;
		$status = $this->input->post('status');
		for ($count = 0; $count < count($_POST["id"]); $count++) {
			$inputs[] = array(
				'id' => $_POST['id'][$count]
			);
		}

		$data = $this->FileTrackModel->remove_track($inputs, $status, $table_name);
		echo json_encode($data);
	}
}
