<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bluebook extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', '512M');
		$this->load->model('Bluebook_Model');
	}

	public function bluebook_upload() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Bluebook_Model->bluebook_upload();
			echo json_encode($result);
		}
	}

	public function get_files($row_num = 0) {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Bluebook_Model->get_files($row_num);
			echo json_encode($res);
		}
	}

	public function get_categories($row_num = 0) {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Bluebook_Model->get_categories($row_num);
			echo json_encode($res);
		}
	}

	public function remove_file($row_num = 0) {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Bluebook_Model->remove_file($row_num);
			echo json_encode($res);
		}
	}

	public function edit_filename($row_num = 0) {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Bluebook_Model->edit_filename($row_num);
			echo json_encode($res);
		}
	}

	public function get_region($row_num = 0) {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Bluebook_Model->get_region($row_num);
			echo json_encode($res);
		}
	}

	public function get_main() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Bluebook_Model->get_main();
			echo json_encode($result);
		}
	}
}
