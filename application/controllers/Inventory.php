<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', '512M');
		$this->load->model('Inventory_Model');
		$this->load->model('Admin_Model');
	}

	public function inventory_page() {
		$data['title'] = 'Inventory';
		$dep = $this->session->userdata('department');
		$access_level = $this->session->userdata('access_level');
		$title = $access_level == 'Admin' ? $access_level : $dep;
		$data['menus'] = $this->Admin_Model->get_menus($title);
		$data['js'] = $access_level == 'Admin' ? array('Admin/inventory_admin') : array('inventory');
		$this->load->view('/layout/header', $data);
		$this->load->view('/layout/side_menu', $data);

		if ($access_level == 'Admin' || $dep == 'RRE') {
			$this->load->view('RRE/inventory_page', $data);
			$this->load->view('/layout/footer', $data);
		} else {
			redirect(base_url('restrictedpage'));
		}
	}

	public function save_item() {
		$id = $this->input->post('id');
		$name_file = $_FILES['image_file']['name'];
		$config['upload_path']   = './inventory/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['file_name']     = $name_file;
		$this->load->library('upload',$config);

		$image = !$this->upload->do_upload('image_file') ? '' : 'inventory/'. $name_file;

		$data = array(
			"image"   		=> $image,
			"item_code"     => $this->input->post('item_code'),
			"name"			=> Ucfirst($this->input->post('name')),
			"quantity"		=> $this->input->post('quantity'),
			"date_created"	=> date('Y-m-d'),
		);

		$this->Inventory_Model->save_item($data, $id);

		$res = array(
			"type"    => "success",
			"title"   => "Success!",
			"message" => "Item successfully updated"
		);

		echo json_encode($res);
	}

	public function get_items() {
		$search = $this->input->post('search');
		$data = $this->Inventory_Model->get_items($search);
		echo json_encode($data);
	}

	public function del_items() {
		$id = $this->input->post('id');
		$data = $this->Inventory_Model->del_items($id);
		echo json_encode($data);
	}

	public function save_to_cart() {
		for ($count = 0; $count < count($_POST["quantities"]); $count++) {
			$inputs[] = array(
				'uid'           => $this->session->userdata('id'),
				'iid'   		=> $_POST['id'][$count],
				'qty' 			=> $_POST['quantities'][$count],
				'stocks'     	=> $_POST['stocks'][$count]
			);
		}

		$data = $this->Inventory_Model->save_to_cart($inputs);
		echo json_encode($data);
	}

	public function get_item_cart() {
		$data = $this->Inventory_Model->get_item_cart();
		echo json_encode($data);
	}

	public function save_to_requests() {
		for ($count = 0; $count < count($_POST["id"]); $count++) {
			$inputs[] = array(
				'purpose'  => $this->input->post('purpose'),
				'uid'      => $this->session->userdata('id'),
				'id'   		 => $_POST['id'][$count]
			);
		}

		$data = $this->Inventory_Model->save_to_requests($inputs);
		echo json_encode($data);
	}

	public function get_requested_lists() {
		$search = $this->input->post('search');
		$date = $this->input->post('date');
		$data = $this->Inventory_Model->get_requested_lists($search, $date);
		echo json_encode($data);
	}

	public function get_requested_item() {
		$search = $this->input->post('search');
		$date = $this->input->post('date');
		$data = $this->Inventory_Model->get_requested_item($search, $date);
		echo json_encode($data);
	}

	public function accept_request() {
		for ($count = 0; $count < count($_POST["id"]); $count++) {
			$inputs[] = array(
				'id' => $_POST['id'][$count]
			);
		}

		$data = $this->Inventory_Model->accept_request($inputs);
		echo json_encode($data);
	}

	public function reject_request() {
		for ($count = 0; $count < count($_POST["id"]); $count++) {
			$inputs[] = array(
				'iid' => $_POST['id'][$count]
			);
		}

		$data = $this->Inventory_Model->reject_request($inputs);
		echo json_encode($data);
	}

	public function return_item() {
		foreach($this->input->post("data") as $rows){
				$inputs[] = array(
					'iid' => $rows['id'],
					'qty' => $rows['qty']
				);
		}

		$data = $this->Inventory_Model->return_item($inputs);
		echo json_encode($data);
	}

	public function get_users() {
		$data = $this->Inventory_Model->get_users();
		echo json_encode($data);
	}

	public function remove_cart_lists() {
		$id = $this->input->post('id');
		$data = $this->Inventory_Model->remove_cart_lists($id);
		echo json_encode($data);
	}
}
