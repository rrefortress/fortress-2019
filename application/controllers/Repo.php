<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Repo extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', '512M');
		$this->load->model('Repo_Model');
	}

	public function get_sites($row_num = 0) {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Repo_Model->get_sites($row_num);
			echo json_encode($res);
		}
	}

	public function get_region_count() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Repo_Model->get_region_count();
			echo json_encode($result);
		}
	}

	public function repo_upload() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Repo_Model->repo_upload();
			echo json_encode($result);
		}
	}

	public function get_content() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Repo_Model->get_content();
			echo json_encode($res);
		}
	}

	public function delete_content() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Repo_Model->delete_content();
			echo json_encode($res);
		}
	}

	public function get_file_count() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Repo_Model->get_file_count();
			echo json_encode($res);
		}
	}

	public function get_repo_chart() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Repo_Model->get_repo_chart();
			echo json_encode($res);
		}
	}
}
