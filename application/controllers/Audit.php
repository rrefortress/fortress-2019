<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Audit extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		ini_set('memory_limit', '512M');
		$this->load->model('Audit_Model');
	}

	public function get_audit($row_num = 0) {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$res = $this->Audit_Model->get_audit($row_num);
			echo json_encode($res);
		}
	}

    public function save_audit() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Audit_Model->save_audit();
			echo json_encode($result);
		}
	}

	public function export_audit() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Audit_Model->export_audit();
			echo json_encode($result);
		}
	}

	public function del_audit() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		} else {
			$result = $this->Audit_Model->del_audit();
			echo json_encode($result);
		}
	}
}
