<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Site_database');
		$this->load->model('Power_Model');
		$this->load->model('Tower_Model');
		$this->load->library('excel');
	}
	public function locateCoord()
	{
		// $longitude = $this->input->post('longitude');
		// $latitude = $this->input->post('latitude');

		// $sq = "SELECT ST_AsGeoJSON(`geom`) as 'geojson', brgymap_vis.psgc as 'Bgy_psgc',`Bgy_name`,`Town_name`,`Pro_name`,`Town_annex`,`Pro_annex`,`Reg_name`,`Reg_annex`  FROM `brgymap_vis` INNER JOIN `annex` on (brgymap_vis.psgc = annex.Bgy_psgc) WHERE ST_Contains(`geom`,POINT($longitude , $latitude))";
		// $numrows = $this->db->query('SELECT * FROM airports')->num_rows();
		$numrows = $this->db->query('SELECT * FROM awe_for_bcf')->num_rows();
		// $id = array("Bgy_psgc" => "175301001");
		// echo  json_encode($this->db->get_where('annex', $id)->row());
		// $sq = "SELECT brgymap_vis.psgc as 'Bgy_psgc',`Bgy_name`,`Town_name`,`Pro_name`,`Reg_name`  FROM `brgymap_vis` INNER JOIN `annex` on (brgymap_vis.psgc = annex.Bgy_psgc) WHERE ST_Contains(`geom`,POINT($longitude , $latitude))";

		// $gtpolesrow;
		// $numrows=2;
		// for($i=1;$i<$numrows+1;$i++)
		$i=235;
		while($i>215)
		{
			// echo $i;
			$this->db->select('*');
			$id = array("id" => $i);
			$awerow=$this->db->get_where('awe_for_bcf', $id)->row();
			// echo json_encode($airportrow);
			// $gtpolesrow=$this->db->query("SELECT 'Latitude','Longitude','id' FROM gt_poles WHERE 'id'=$i")->row();

			// if(is_array($airportrow) || is_object($airportrow)>0 ||$airportrow->psgc!==null || $airportrow->psgc!=="")
			if(is_array($awerow) || is_object($awerow)>0 || $awerow->serial!="" || $awerow->Bgy_psgc!="")
			{
				$longitude = floatval($awerow->long);
				$latitude =  floatval($awerow->lat);
				// $longitude = 124.5962;
				// $latitude =  12.06827;
				// echo  json_encode($longitude." , ".$latitude);
				$id =  $i;
				$sq = "SELECT brgymap_vis.psgc as 'Bgy_psgc',`Bgy_name`,`Town_name`,`Pro_name`,`Reg_name`,`Town_annex`,`Pro_annex`  FROM `brgymap_vis` INNER JOIN `annex` on (brgymap_vis.psgc = annex.Bgy_psgc) WHERE ST_Contains(`geom`,POINT($longitude, $latitude))";
				// $sq2 = "SELECT brgymap_vis.psgc as 'Bgy_psgc',`Bgy_name`,`Town_name`,`Pro_name`,`Reg_name`,`Town_annex`,`Pro_annex` FROM `brgymap_vis` INNER JOIN `annex` on (brgymap_vis.psgc = annex.Bgy_psgc) WHERE ST_Contains(`geom`,POINT($longitude, $latitude))";
				$ret = $this->db->query($sq)->row();
				//in Km
				// $search_distance=100;

				// $sq2 = "SELECT *, 3956 * 2 * ASIN(SQRT(
				// 	POWER(SIN(($latitude - abs(dest.latitude)) * pi()/180 / 2), 2) +  COS($latitude * pi()/180 ) * COS(abs(dest.latitude) * pi()/180) *  POWER(SIN(($longitude-dest.longitude) * pi()/180 / 2), 2) )) as  distance
				// 	FROM airports dest
				// 	having distance < $search_distance
				// 	ORDER BY distance 
				// 	limit 2";
				// $ret = $this->db->query($sq2)->result();
					 // $ret = $this->db->query($sq);
					// $ret = $this->db->query($sq)->result();
				// echo  json_encode($ret);
				// echo  json_encode($ret->Bgy_psgc);

				// $getdist = $this->db->query($sq)->row();
				if(is_array($ret) || is_object($ret)>0)
				{
					// $data = array(
			  //       "nearest1" 		=> $ret[0]->name,
			  //       "type1" 		=> $ret[0]->classification,
			  //       "distance1" 		=> $ret[0]->distance,
			  //       "airlat1" 		=> $ret[0]->latitude,
			  //       "airlong1" 		=> $ret[0]->longitude,
			  //       "nearest2" 		=> $ret[1]->name,
			  //       "type2" 		=> $ret[1]->classification,
			  //       "distance2" 		=> $ret[1]->distance,
			  //       "airlat2" 		=> $ret[1]->latitude,
			  //       "airlong2" 		=> $ret[1]->longitude,

					$data = array(
			        	"Bgy_psgc" => $ret->Bgy_psgc,
			  			"Bgy_name" => $ret->Bgy_name,
			  			"Town_name" => $ret->Town_name,
			  			"Pro_name" => $ret->Pro_name,
			  			"Reg_name"=> $ret->Reg_name,
			  			"Town_annex" => $ret->Town_annex,
			  			"Pro_annex"	=> $ret->Pro_annex,
					);
				$this->db->where('id', $id);
				// echo json_encode($ret[0]);
				// echo  json_encode($this->db->update('awe_for_bcf', $data));
				$this->db->update('awe_for_bcf', $data);

				}
				
			}
			$i--;
		}

			echo  json_encode($ret);
		
	}

	//ORIGINAL
	// 	public function locateCoord()
	// {
	// 	$longitude = $this->input->post('longitude');
	// 	$latitude = $this->input->post('latitude');

	// 	$sq = "SELECT ST_AsGeoJSON(`geom`) as 'geojson', brgymap_vis.psgc as 'Bgy_psgc',`Bgy_name`,`Town_name`,`Pro_name`,`Town_annex`,`Pro_annex`,`Reg_name`,`Reg_annex`  FROM `brgymap_vis` INNER JOIN `annex` on (brgymap_vis.psgc = annex.Bgy_psgc) WHERE ST_Contains(`geom`,POINT($longitude , $latitude))";
	// 	$ret = $this->db->query($sq)->row();
	// 	if (is_array($ret) || is_object($ret)>0)
	// 	{
	// 		echo  json_encode($ret);
	// 	}
	// }
	public function fileupload()
	{
		$dep = strtoupper($this->session->userdata('department'));
		$name_file = $_FILES['userfile']['name'];
		$database_table = $this->input->post('database_table');
		$geoarea = $this->session->userdata('geoarea');
		$database_table .= '_'.$geoarea;

		$config['upload_path']          = './upload/';
		$config['allowed_types']        = 'xlsx|csv|xls';
		$config['overwrite'] 			= true;
		$config['encrypt_name']         = FALSE;
		$config['remove_spaces']        = TRUE;
		$config['file_name']			= $name_file;
		$this->data['upload_error']		= ' ';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('userfile'))
		{
			$this->data['upload_error'] = $this->upload->display_errors();
			$res = array(
				"title" => "Oops!",
				"msg"   => "Something went wrong.",
				"type"  => "error"
			);
		}
		else
		{
			$this->data['upload_error']	= $this->upload->data();
			#IF UPLOAD IS FROM RRE
			if ($dep == 'RRE') {
				$res = $this->Site_database->batchInsertRRE($database_table, $name_file);
			} else if ($dep == 'RFE') {
				#IF UPLOAD IS FROM RFE
				$res = $this->Site_database->batchInsertRFE();
			} else if ($dep == 'RCNE' || $dep == 'RWE') {
				#IF UPLOAD IS FROM RCNE OR RWE
				$res = $this->Site_database->batchInsertRCNE();
			} else if ($dep == 'RGPM') {
				#IF UPLOAD IS FROM RGPM
				$res = $this->Site_database->batchInsertRGPM();
			}
		}

		echo json_encode($res);
	}
	public function checkduplicate()
	{
		$sitename = $this->input->post('sitename');
		$data = array(
			"sitename"		=> $sitename
		);
		$database_table = 'tower';
		$select = 'sitename';
		$geoarea = $this->session->userdata('geoarea');
		$database_table .= '_'.$geoarea;
		$result = ($this->Site_database->getSingle($database_table, $data, $select));
		if($result != "" || $result != null)
		{
			echo json_encode(1);
		}else
		{
			echo json_encode(0);
		}
	}
	//for autocomplete purpose when searching the site
	public function getSearch()
	{
		$data = array(
			"sitename"		=> $this->input->post('sitename'),
		);
		$database_table = 'tower';
		$select = 'sitename';
		$geoarea = $this->session->userdata('geoarea');
		$database_table .= '_'.$geoarea;
		echo json_encode($this->Site_database->getLike($database_table, $data, $select));
	}
	//get all towers sitename
	public function getAllTowers()
	{
		$database_table = 'tower';
		$select = '*';
		$geoarea = $this->session->userdata('geoarea');
		$database_table .= '_'.$geoarea;
		echo json_encode($this->Site_database->getAll($database_table, $select));
	}
	//get table row count
	public function getlastdbid()
	{
		$database_table = $this->input->post('table');
		$geoarea = $this->session->userdata('geoarea');
		$select = '*';
		$database_table .= '_'.$geoarea;
		echo json_encode($this->Site_database->getLastDBID($database_table, $select));
	}
	//get specific query data
	public function getquery($database_table)
	{	
		$data = array(
			"siteid"		=> $this->input->post('siteid'),
		);
		$select = '*';
		$geoarea = $this->session->userdata('geoarea');
		
		if ($database_table == 'tower')
		{
			$database_table .= '_'.$geoarea;
			echo json_encode($this->Site_database->getSingle($database_table, $data, $select));
		}
		else if ($database_table == '2g' || $database_table == '3g' || $database_table == '4g')
		{
			$database_table .= '_'.$geoarea;
			echo json_encode($this->Site_database->getSome($database_table, $data, $select));
		}
	}
	//add site to site DB after being approved
	public function getsite()
	{
		$database_table = 'mdbupdates';
		$approvalid = $this->input->post('approvalid');
		$geoarea = $this->session->userdata('geoarea');
		$database_table .= '_'.$geoarea;
		echo json_encode($this->Site_database->getSite($approvalid, $database_table));
	}
	public function insertsite()
	{
	//update approvals_vis and insert site to tower_vis
		$date = date("Y/m/d H:i:s");
		$geoarea = $this->session->userdata('geoarea');
		$database_table .= '_'.$geoarea;
		$database_table = 'mdbupdates';
		$approvalid = $this->input->post('approvalid');
		//update approvals
		$updatedata = array(
			'id'			=> $approvalid,
	    	'approvalstatus' => 'uploaded',
	    	'dateapproved' 	=> $date
		);
		$this->Site_database->updateSingleDB($updatedata, $database_table);

		///////get approval data using ID
		$filterarray = array('id' => $approvalid);
		$ret = $this->Site_database->getSite($filterarray, $database_table);
		$detailsafter = (object)(json_decode($ret->detailsafter, true));
		$detailsafter->audit = "YES";
		$database_table = 'tower';
		$database_table .= '_'.$geoarea;
		$this->Site_database->insertSingleDB($detailsafter, $database_table);
	}
	public function inserttodb()
	{
	//update approvals_vis and insert site to tower_vis
		$date = date("Y/m/d H:i:s");
		$geoarea = $this->session->userdata('geoarea');
		$database_table = 'mdbupdates';
		$database_table .= '_'.$geoarea;
		$approvalid = $this->input->post('approvalid');
		// $sitename = $this->input->post('sitename');

		// if($approvalid === '' || $approvalid === null){

		// }

		//update approvals
		$updatedata = array(
			'id'			=> $approvalid,
	    	'approvalstatus' => 'added',
	    	'dateapproved' 	=> $date
		);
		$this->Site_database->updateSingleDB($updatedata, $database_table);

		///////get approval data using ID
		$filterarray = array('id' => $approvalid);
		$ret = $this->Site_database->getSite($filterarray, $database_table);
		$db = $ret->db;
		$siteid = $ret->siteid;
		
		$database_table = $db;
		$database_table .= '_'.$geoarea;
		$arr = array();

		$toweridfilter = array('siteid' => $siteid, 'db' => 'tower');
		$towerret = $this->Site_database->getSite($toweridfilter, 'mdbupdates_'.$geoarea);
		$towerdetailsafter = (object)(json_decode($towerret->detailsafter, true));
		// $sitename = $towerdetailsafter->sitename;

		//////////insert to DB
		switch ($db) {
			case 'tower':
				//$detailsafter = (object)(json_decode($ret->detailsafter, true));
				$towerdetailsafter->audited = "YES"; //for tower only
				// echo json_encode( $towerdetailsafter);
				for($j=0;$j<sizeof($detailsafter[$i]);$j++)
				{
					$arrline = array(
						$fieldnames[$j]=$detailsafter[$i][$j]
					);
				}
				array_push($arr,$this->Site_database->insertSingleDB($towerdetailsafter, $database_table));
				break;
			case   '3g' ||  '2g' ||  '4g':
				
				$detailsafter = (json_decode($ret->detailsafter, true));
				$fieldnames = (json_decode($ret->fieldnames, true));
				//echo json_encode(sizeof($detailsafter));
				switch ($db) {
					case '2g' || '3g':
						for($i=0;$i<sizeof($detailsafter);$i++){
							$arrline = array();
							for($j=0;$j<sizeof($detailsafter[$i]);$j++)
							{
								$arrline = array(
									$fieldnames[$j]=$detailsafter[$i][$j]
								);
							}
							array_push($arr,$this->Site_database->insertSingleDB($detailsafter[$i], $database_table));
						}
						break;

			if (is_array($detailsafter) && sizeof($detailsafter) > 0)
			{
				// for($i=0;$i<sizeof($detailsafter);$i++)
				foreach ($fieldnames as $id => $value) 
				{
					foreach ($value as $id2 => $value2) 
					{
						$eachdata[$value2]=$detailsafter[$id][$id2];
					}
					array_push($arr,$this->Site_database->updateSingleDB($eachdata, $database_table));
				}
			}



					// case  '3g':
					// 	for($i=0;$i<sizeof($detailsafter);$i++){
					// 			array_push($arr,$this->Site_database->insertSite($detailsafter[$i], $database_table));
					// 	}
					// 	break;
					// case  '4g':
					// 	break;	
				}
				break;
		}
		echo json_encode($arr);
	}

	public function updatesite()
	{	
		$filter = array(
			'title' => 'My title',
			'name'  => 'My Name',
			'date'  => 'My date'
		);

		$data = array(
	        "sitename" 		=> $this->input->post('sitename'),
	        "plaid" 		=> $this->input->post('plaid'),
	        "geoarea" 		=> $this->input->post('geoarea'),
	        "longitude" 	=> $this->input->post('longitude'),
	        "latitude" 		=> $this->input->post('latitude'),
	        "region" 		=> $this->input->post('region'),
	        "province" 		=> $this->input->post('province'),
	        "municipality" 	=> $this->input->post('municipality'),
	        "barangay" 		=> $this->input->post('barangay'),
	        "address" 		=> $this->input->post('address'),
	        "audited" 		=> "NO"
		);
		$geoarea = $data['geoarea'];
		$database_table = 'tower';
		$database_table .= '_'.$geoarea;
		echo json_encode($this->Site_database->insertSingleDB($filterdata, $data, $database_table));
		// $date = date("Y/m/d H:i:s");
		// $this->input->post('dateapproved');
	}
	// public function loadRecord($rowno=0){
 //    // Row per page
	// 	$rowperpage = 10;

 //    // Row position
	// 	if($rowno != 0){
	// 		$rowno = ($rowno-1) * $rowperpage;
	// 	}

	// 	//$data = NULL;
	// 	$data = array("audited" => "IBS");
	// 	// All records count
	// 	$allcount = $this->Site_database->getData(Null,Null,$data,1);
 //    // Get records
	// 	$users_record = $this->Site_database->getData($rowno,$rowperpage,$data,0);
    
 //    // Pagination Configuration
	// 	$config['base_url'] = base_url('loadRecord');
	// 	$config['use_page_numbers'] = TRUE;
	// 	$config['total_rows'] = $allcount;
	// 	$config['per_page'] = $rowperpage;

	// 	$config['full_tag_open']    = '<div class="padding text-right"><nav><ul class="pagination">';
 //        $config['full_tag_close']   = '</ul></nav></div>';
 //        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
 //        $config['num_tag_close']    = '</span></li>';
 //        $config['cur_tag_open']     = '<li class="page-item active "><span class="page-link">';
 //        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
 //        $config['next_tag_open']    = '<li class="page-item"><span class="page-link ">';
 //        $config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
 //        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link ">';
 //        $config['prev_tag_close']  = '</span></li>';
 //        $config['first_tag_open']   = '<li class="page-item"><span class="page-link ">';
 //        $config['first_tag_close'] = '</span></li>';
 //        $config['last_tag_open']    = '<li class="page-item"><span class="page-link ">';
 //        $config['last_tag_close']  = '</span></li>';
        


 //    // Initialize
	// 	$this->pagination->initialize($config);

 //    // Initialize $data Array
	// 	$data['pagination'] = $this->pagination->create_links();
	// 	$data['result'] = $users_record;
	// 	$data['row'] = $rowno;

	// 	echo json_encode($data);

	// }
	public function loadAnnex($option)
	{
		$province = $this->input->post('province');
		$town = $this->input->post('town');
		$database_table = 'annex';
		$select = 'province, town, barangay';
		
		$uniques = $this->Site_database->getAll($database_table, $select);
		$newlist = array();

		switch ($option) {
			case 'province':
				foreach ($uniques as $unique => $value)
				{
					array_push($newlist, $value->$option);
				}
				echo json_encode(array_unique($newlist));
				break;
			case 'town':
				foreach ($uniques as $unique => $value)
				{
					if($value->province==$province)
					{
						array_push($newlist, $value->$option);
					}
				}
				echo json_encode(array_unique($newlist));
				break;
			case 'barangay':
				foreach ($uniques as $unique => $value)
				{
					if($value->province==$province && $value->town==$town)
					{
						array_push($newlist, $value->$option);
					}
				}
				echo json_encode(array_unique($newlist));
				break;
			default:
				# code...
				break;
		}
	}

	public function generateSiteName()
	{
		$province = $this->input->post('province');
		$town = $this->input->post('town');

		$data = array(
			"province"	=>	$province,
			"town"		=>	$town
		);
		$database_table = 'annex';
		$select = '*';
		echo json_encode($this->Site_database->getSingle($database_table, $data, $select));
	}

	public function getmdbupdate()
	{
		$database_table = 'mdbupdates';
		$updateid = $this->input->post('updateid');
		$geoarea = $this->session->userdata('geoarea');
		$database_table .= '_'.$geoarea;
		$filterarray = array('id' => $updateid);
		echo json_encode($this->Site_database->getSite($filterarray, $database_table));
	}
	public function insertmdbupdate()
	{

		$date 	= date("Y/m/d H:i:s");
		$siteidarr = array(
			"siteid"		=> $this->input->post('siteid'),
		);
		$select = '*';
		$geoarea = "vis"; ///////geoarea data will be from annex
		$database_table = 'mdbupdates_'.$geoarea;
		// $geoarea = $this->session->userdata('geoarea');
		$siteid = $this->input->post('siteid');
		$operation = $this->input->post('operation');
		$ret = array();

		if($operation=='add')
		{
			$dbtoupdate = array('tower','2g','3g');
			foreach ($dbtoupdate as &$tech) 
			{
			    $data = $this->input->post('data'.$tech);
			    $fieldnames = $this->input->post('fieldnames'.$tech);
			    if($data == "")
			    {
			    	continue;
			    }
			    	// $data = $this->input->post('data'.$tech);
			    	$updatedata = array(
					"datecreated"	=> $date,
					//"dateapproved"	=> "",
					"siteid"		=> $siteid,
					"accesslevel"	=> $this->session->userdata('access_level'),
					//"remarks"		=> "",
					"userid"		=> $this->session->userdata('id'),
					"updatetype"	=> $operation,
					"db"			=> $tech,
					"approvalstatus"=> "to".$operation,
					"fieldnames"	=> json_encode($fieldnames),
					"detailsbefore"	=> json_encode(""),
					"detailsafter"	=> json_encode($data)
					);
					array_push($ret, $this->Site_database->insertSingleDB($updatedata, $database_table));
			    
			}
		}
		else
		{
			$dbtoupdate = array('tower','2g','3g','4g');
			foreach ($dbtoupdate as &$tech) 
			{
			    $data = $this->Site_database->getSome($tech.'_'.$geoarea, $siteidarr, $select);
			    if($data == "")
			    {
			    	continue;
			    }
			 //    $dbdetailsbefore=array();
				// $fieldnamesource=array();
			 //    foreach ($data as &$fieldnames)
				// {
				// 	array_push($ret,
				// }
			    $updatedata = array(
					"datecreated"	=> $date,
					//"dateapproved"	=> "",
					"siteid"		=> $siteid,
					"accesslevel"	=> $this->session->userdata('access_level'),
					//"remarks"		=> "",
					"userid"		=> $this->session->userdata('id'),
					"updatetype"	=> $operation,
					"db"			=> $tech,
					"approvalstatus"=> "to".$operation,
					"fieldnames"	=> json_encode($fieldnames),
					"detailsbefore"	=> json_encode($data),
					"detailsafter"	=> json_encode("")
				);
				array_push($ret, $this->Site_database->insertSingleDB($updatedata, $database_table));
			}
		}
		echo json_encode($ret);

	}

	public function insertsinglemdbupdate()
	{	
		$date 	= date("Y/m/d H:i:s");
		$siteid = $this->input->post('siteid');
		$tech = $this->input->post('tech');
		$detailsafter = $this->input->post('detailsafter');
		$detailsbefore = $this->input->post('detailsbefore');
		$fieldnames = $this->input->post('fieldnames');
		$operation = $this->input->post('operation');
		$geoarea = $this->session->userdata('geoarea');
		$database_table = "mdbupdates";
		$database_table .= '_'.$geoarea;
		// $data = array(
	 //        "sitename" 		=> $this->input->post('sitename'),
	 //        "plaid" 		=> $this->input->post('plaid'),
	 //        "geoarea" 		=> $this->input->post('geoarea'),
	 //        "longitude" 	=> $this->input->post('longitude'),
	 //        "latitude" 		=> $this->input->post('latitude'),
	 //        "region" 		=> $this->input->post('region'),
	 //        "province" 		=> $this->input->post('province'),
	 //        "municipality" 	=> $this->input->post('municipality'),
	 //        "barangay" 		=> $this->input->post('barangay'),
	 //        "address" 		=> $this->input->post('address'),
	 //        "audited" 		=> "NO"
		// );
		$dataappr = array(	
			"datecreated"	=> $date,
			//"dateapproved"	=> "",
			"siteid"		=> $siteid,
			"accesslevel"	=> $this->session->userdata('access_level'),
			"userid"		=> $this->session->userdata('id'),
			"updatetype"	=> $operation,
			"db"			=> $tech,
			"approvalstatus"=> "to".$operation,
			"fieldnames"	=> json_encode($fieldnames),
			"detailsbefore"	=> json_encode($detailsbefore),
			"detailsafter"	=> json_encode($detailsafter)
		);
		// echo json_encode($data);
		$ret = array();
		array_push($ret, $this->Site_database->insertSingleDB($dataappr, $database_table));
		echo json_encode($ret);
	}

/////////////////////////////////////////////////////////////////////////////////
	public function dbadjust()
	{
		$geoarea = $this->session->userdata('geoarea');
		$database_table = 'mdbupdates';
		$database_table .= '_'.$geoarea;
		$updatetype = $this->input->post('updatetype');
		$filterarray = array('updatetype' => $updatetype);
		$ret = $this->Site_database->getSome($database_table,$filterarray,'*');
		$fieldnames = array();
		$ids = array();
		$detailsafterarr = array();
		for($i=0;$i<sizeof($ret);$i++)
		{
			array_push($ids, $ret[$i]->id);
		}
		echo json_encode($ids);
		foreach ($ids as $idkey => $idvalue)
		{
			$filterarray = array('id' => $idvalue);
			// $filterarray = array('id' => '645');
			$ret2 = $this->Site_database->getSite($filterarray, $database_table);
			// $everydetailsafter = json_decode($ret2->detailsafter, true);
			$everydetailsafter = json_decode($ret2->fieldnames, true);
			// foreach ($everydetailsafter[0] as $key => $value)
			// echo json_encode($everydetailsafter);
			if (is_array($everydetailsafter) || is_object($everydetailsafter))
			{
				foreach ($everydetailsafter as $key => $value)
				{
					// array_push($fieldnames,$key);
					array_push($fieldnames,$value);
				}
			}
			$fieldnames2 = array();
			$firstfieldname = $fieldnames[0];

			echo json_encode($idvalue);
			for($i=1;$i<sizeof($fieldnames);$i++)
			{
				if($fieldnames[$i] == $firstfieldname)
				{
					break;
				}array_push($fieldnames2,$fieldnames[$i]);
			}

			// echo json_encode($fieldnames2);
			// for($i=0;$i<sizeof($everydetailsafter);$i++)
			// {	
			// 	// $detailsafterarr[$i]=array();
			// 	$xarray=[];
			// 	foreach ($everydetailsafter[$i] as $key => $value)
			// 	{
			// 		array_push($xarray,$value);
			// 	}
			// 	array_push($detailsafterarr,$xarray);
				
			// }
			// $data = array(
			// 	'id'=> $idvalue,
			// 	'fieldnames'=> json_encode($fieldnames)
			// );
			// $this->Site_database->updateSingleDB($data, $database_table);
			// $data = array(
			// 	'id'=> $idvalue,
			// 	'detailsafter'=> json_encode($detailsafterarr)
			// );
			// $this->Site_database->updateSingleDB($data, $database_table);
		}
		
		// echo json_encode($ids);

	}
/////////////////////////////////////////////////////////////////////////////
	public function insertsingletodb()
	{
	//update approvals_vis and insert site to tower_vis
		$date = date("Y/m/d H:i:s");
		$geoarea = $this->session->userdata('geoarea');
		$database_table = 'mdbupdates';
		$database_table .= '_'.$geoarea;
		$approvalid = $this->input->post('approvalid');
		$operation = $this->input->post('operation');
		//update approvals
		$updatedata = array(
			'id'			=> $approvalid,
	    	'approvalstatus' => $operation.'ed',
	    	'dateapproved' 	=> $date
		);
		$this->Site_database->updateSingleDB($updatedata, $database_table);

		/////get approval data using ID
		$filterarray = array('id' => $approvalid);
		$ret = $this->Site_database->getSite($filterarray, $database_table);
		$db = $ret->db;
		
		$database_table = $db.'_'.$geoarea;
		$arr = array();
		//////////insert to DB
		$detailsafter = (json_decode($ret->detailsafter, true));
		$fieldnames = (json_decode($ret->fieldnames, true));
		if($operation == 'edit')
		{
			if (is_array($detailsafter) && sizeof($detailsafter) > 0)
			{
				// for($i=0;$i<sizeof($detailsafter);$i++)
				foreach ($detailsafter as $id => $value) 
				{
					foreach ($value as $id2 => $value2) 
					{
						$eachdata[$fieldnames[$id][$id2]]=$value2;
					}
					array_push($arr,$this->Site_database->updateSingleDB($eachdata, $database_table));
				}
			}
		}
		else
		{
			if (is_array($detailsafter) && sizeof($detailsafter) > 0)
			{
				foreach ($detailsafter as $id => $value) 
				{
					foreach ($value as $id2 => $value2) 
					{
						$eachdata[$fieldnames[0][$id2]]=$value2;
					}
					array_push($arr,$this->Site_database->insertSingleDB($eachdata, $database_table));
				}
			}
		}
		echo json_encode($arr);
	}

	public function deletesinglefromdb()
	{
	//update approvals_vis and insert site to tower_vis
		$date = date("Y/m/d H:i:s");
		$geoarea = $this->session->userdata('geoarea');
		$database_table = 'mdbupdates';
		$database_table .= '_'.$geoarea;
		$approvalid = $this->input->post('approvalid');
		$operation = $this->input->post('operation');
		// $sitename = $this->input->post('sitename');

		// if($approvalid === '' || $approvalid === null){

		// }

		//update approvals
		$updatedata = array(
			'id'			=> $approvalid,
	    	'approvalstatus' => 'deleted',
	    	'dateapproved' 	=> $date
		);
		$this->Site_database->updateSingleDB($updatedata, $database_table);

		/////get approval data using ID
		$filterarray = array('id' => $approvalid);
		$ret = $this->Site_database->getSite($filterarray, $database_table);
		$db = $ret->db;
		
		$database_table = $db.'_'.$geoarea;
		$arr = array();
		//////////insert to DB
		$detailsbefore = (json_decode($ret->detailsbefore, true));
		$fieldnames = (json_decode($ret->fieldnames, true));
		// echo json_encode($detailsbefore[0]['dbcellid']);
		if (is_array($detailsbefore) && sizeof($detailsbefore) > 0)
		{
			for($i=0;$i<sizeof($detailsbefore);$i++)
			{
				foreach  ($fieldnames[0] as $id => $value) 
				{
					if($db=='tower')
					{
						$eachdata = array('siteid' => $detailsbefore[$i][$id]);
						array_push($arr,$this->Site_database->deleteSingleDB($eachdata, $database_table));
					}
					else //($value=='dbcellid')
					{
						$eachdata = array('dbcellid' => $detailsbefore[$i][$id]);
						array_push($arr,$this->Site_database->deleteSingleDB($eachdata, $database_table));
					}
				}
			}
		}

		echo json_encode($arr);
	}
	public function deletesitefromdb()
	{	
		$date = date("Y/m/d H:i:s");
		$geoarea = $this->session->userdata('geoarea');
		$approvalid = $this->input->post('approvalid');
		$operation = $this->input->post('operation');
		$arr = array();
		
		foreach ($approvalid as &$id) 
		{
			$database_table = 'mdbupdates_'.$geoarea;
			$updatedata = array(
				'id'			=> $id,
		    	'approvalstatus' => 'deleted',
		    	'dateapproved' 	=> $date
			);
			$this->Site_database->updateSingleDB($updatedata, $database_table);
			/////get approval data using ID
			$filterarray = array('id' => $id);
			$ret = $this->Site_database->getSite($filterarray, $database_table);
			$db = $ret->db;
			
			$database_table = $db.'_'.$geoarea;
			
			//////////insert to DB
			$detailsbefore = (json_decode($ret->detailsbefore, true));
			if($db == 'tower')
			{
				$dbidnow='siteid';
			}
			else
			{
				$dbidnow='dbcellid';
			}
			if (is_array($detailsbefore) && sizeof($detailsbefore) > 0)
			{
				for($i=0;$i<sizeof($detailsbefore);$i++)
				{
					$eachdata = array($dbidnow => $detailsbefore[$i][$dbidnow]);
					array_push($arr,$this->Site_database->deleteSingleDB($eachdata, $database_table));
				}
			}
		}
		echo json_encode($arr);

	}
	public function generatecellid($tech)
	{	
		switch ($tech) {
			case '2g':
				$database_table = '2g';
				break;
			case '3g':
				$database_table = '3g';
				break;
			case '4g':
				$database_table = '4g';
				break;
			default:
				# code...
				break;
		}
		
		$select = 'cid';
		$geoarea = $this->session->userdata('geoarea');
		$database_table .= '_'.$geoarea;
		$data_cid = $this->Site_database->getAll($database_table, $select);
		$pool = array();
		$pool_3g = array();
		$existingcid_list = array();
		// echo $data_cid[0]->cid;
		switch ($tech) 
		{
			case '2g':
				foreach ($data_cid as $id => $value) 
				{
					$temp = str_pad($value->cid, 5, '0', STR_PAD_LEFT);
					$temp = substr($temp, 0, -4);
					if($temp === '1' || $temp === '2' || $temp === '3')
					{
						array_push($existingcid_list, substr(str_pad($value->cid, 5, '0', STR_PAD_LEFT), 1));
					}
				}

				$uniquecid_list = array_unique($existingcid_list);

				for ($i=1; $i <= 9999; $i++) 
				{ 
					array_push($pool, str_pad($i, 4, '0', STR_PAD_LEFT));
				}

				$availablecid_list = array_diff($pool, $uniquecid_list);
				echo json_encode($availablecid_list);
				break;

			case '3g':
				foreach ($data_cid as $id => $value) 
				{
					$temp = str_pad($value->cid, 5, '0', STR_PAD_LEFT);
					$temp = substr($temp, 0, -4);
					if($temp === '0' ||$temp === '4' || $temp === '5')
					{
						array_push($existingcid_list, substr(str_pad($value->cid, 5, '0', STR_PAD_LEFT), 1));
					}
					
				}
				$uniquecid_list = array_unique($existingcid_list);
				for ($i=1; $i <= 9999; $i++) 
				{ 
					array_push($pool, str_pad($i, 4, '0', STR_PAD_LEFT));
				}
				$availablecid_list = array_diff($pool, $uniquecid_list);
				echo json_encode($availablecid_list);
				break;
			default:
				# code...
				break;
		}
	}

	public function getNumber()
	{
		$select = 'tech';
		$geoarea = $this->session->userdata('geoarea');

		$dashboard_data = array(
		'g900'	=>	$this->Site_database->getCount('2g'.'_'.$geoarea, array('tech' => 'G900','status' => 'active'), $select),
		'g1800'	=>	$this->Site_database->getCount('2g'.'_'.$geoarea, array('tech' => 'G1800','status' => 'active'), $select),
		'u900'	=>	$this->Site_database->getCount('3g'.'_'.$geoarea, array('tech' => 'U900','status' => 'active'), $select),
		'u2100'	=>	$this->Site_database->getCount('3g'.'_'.$geoarea, array('tech' => 'U2100','status' => 'active'), $select),
		'l700'	=>	$this->Site_database->getCount('4g'.'_'.$geoarea, array('tech' => 'L700','status' => 'active'), $select),
		'l1800'	=>	$this->Site_database->getCount('4g'.'_'.$geoarea, array('tech' => 'L1800','status' => 'active'), $select),
		'l2300'	=>	$this->Site_database->getCount('4g'.'_'.$geoarea, array('tech' => 'L2300','status' => 'active'), $select),
		'l2600'	=>	$this->Site_database->getCount('4g'.'_'.$geoarea, array('tech' => 'L2600','status' => 'active'), $select)
		);

		echo json_encode($dashboard_data);
	}

	//get neighbors excluding the target site
	public function getNeighbors($database_table, $radius)
	{
		$data = array(
			"siteid"		=> $this->input->post('siteid'),
		);

		$getNeighbors =array();

		$select = '*';
		$geoarea = $this->session->userdata('geoarea');
		$database_table .= '_'.$geoarea;
		$target = $this->Site_database->getSingle($database_table, $data, $select);
		
		$sites = $this->Site_database->getAll($database_table, $select);

		foreach ($sites as $site => $value) 
		{
			$distance = $this->distance(floatval($target->latitude), floatval($target->longitude), floatval($value->latitude), floatval($value->longitude), 'K');
			
			if( $radius >= $distance)
			{
				if($value->sitename!==$target->sitename){
				array_push($getNeighbors, array(
					'sitename'	=> $value->sitename,
					'longitude' => $value->longitude,
					'latitude'	=> $value->latitude
				));
			}
			}
		}
		echo json_encode($getNeighbors);
	}

	//Compute distance
	public function distance($lat1, $lon1, $lat2, $lon2, $unit)
	{
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);

	  	if ($unit == "K") 
	  	{
	  		//return kilometers
	    	return ($miles * 1.609344);
	  	} 
	  	else if ($unit == "N")
	  	{	
	  		//return nautical
	    	return ($miles * 0.8684);
	    } 
	    else 
	    {
	    	//return miles
	    	return $miles;
	    }
	}
		/// generate scrambling code

	public function generatepsc()
	{
		//get input
		$inputlongitude = $this->input->post("longitude");
		$inputlatitude = $this->input->post("latitude");
		$radius = $this->input->post("radius");
		$sitetype = $this->input->post("sitetype");
		$generatedlist=array();
		//generate list
		if($sitetype=="indoor"){
			for($y=0;$y<100;$y++)
			{
				array_push($generatedlist, $y);
			}
		}else if($sitetype=="outdoor")
		{
			for($y=100;$y<512;$y++)
			{
				array_push($generatedlist, $y);
			}
		}
		
		//get db
		$database_table = "3g";
		$geoarea = $this->session->userdata('geoarea');
		$database_table .= '_'.$geoarea;
		$select = 'cellname,longitude,latitude,psc';
		$existingsites = $this->Site_database->getAll($database_table, $select);
		$nearbysites =array();
		$existingpsc=array();
		//process
		$distanceradius = $radius / 1.609344;

		foreach($existingsites as $site)
		{
			$checkdistance = $this->distance(floatval($inputlatitude), floatval($inputlongitude), floatval($site->latitude), floatval($site->longitude), "M");

			if(is_nan($checkdistance))
			{
				$checkdistance=0;
			}
			if($checkdistance<=$distanceradius)
			{
				array_push($nearbysites, $site);
				array_push($existingpsc, $site->psc);
			}
		}
		$existingpsc = array_unique($existingpsc);

		$available_code= array_diff($generatedlist, $existingpsc);
		echo json_encode($available_code);

	}
	public function edit()
	{	
		$dep = strtolower($this->session->userdata('department'));
		$geoarea = $this->session->userdata('geoarea');
		$db = $this->input->post('db');
  		if($db == 'sites'){
  			$database_table = $dep.'_'.$geoarea;
		} else $database_table = $db.'_'.$geoarea;
		// $database_table = $dep.'_'.$geoarea;
		
		$detailsafter = $this->input->post("data");
		$eachdata= array();
		$ids= array();
		if (is_array($detailsafter) && sizeof($detailsafter) > 0)
			{
				// for($i=0;$i<sizeof($detailsafter);$i++)
				foreach ($detailsafter as $value) 
				{
					// $sub_array = array();
					array_push($ids,$value[0]);
					array_push($eachdata,array(
					'id'		=> $value[0],
					$value[1] 	=> $value[2]
					));
				}
			}

		$this->Tower_Model->update_completion_data($ids);
		echo json_encode($this->Site_database->update($database_table,$eachdata));
			
	}
	public function delete()
	{	
		$dep = strtolower($this->session->userdata('department'));
		$geoarea = $this->session->userdata('geoarea');
		$db = $this->input->post('db');
  		if($db == 'sites'){
  			$database_table = $dep.'_'.$geoarea;
		} else $database_table = $db.'_'.$geoarea;
		// $database_table = $dep.'_'.$geoarea;
		$selections = $this->input->post("selections");

		if($selections[0]=="all"){
			echo json_encode($this->Site_database->truncate($database_table));
		}else{
			echo json_encode($this->Site_database->delete($database_table,$selections));
		}	
	}
	
	public function getSection()
	{	
		echo json_encode(strtolower($this->session->userdata('department')));
	}
	public function generatedatatable(){
			$dep = strtolower($this->session->userdata('department'));
  			$geoarea = $this->session->userdata('geoarea');
  			$displayto = $this->input->post('displayto');///////use this to limit fields to display depending on page
  			$database_table = '';
  			$request = $this->input->post('request');
  			// $db = $this->input->post('db');
  			if($displayto == 'sites'){
  				$database_table = $dep.'_'.$geoarea;
			} else {
  				$database_table = $displayto.'_'.$geoarea;
			}
			$table_fields = $this->Site_database->getTableFields($database_table);
			
			switch($request)
			{
				case 'fields': 
					echo json_encode($table_fields);
					break;
				case 'data':
					$data = array(); 
					$fetch_data = $this->Site_database->makeDatatables($database_table);
					foreach($fetch_data as $row)  
					{  
						$sub_array = array();
						foreach($table_fields as $colname)
						{
							$sub_array[] = $row->$colname;
						}
				                // $sub_array[] = '<img src="'.base_url().'upload/'.$row->image.'" class="img-thumbnail" width="50" height="35" />';  
				                // $sub_array[] = '<button type="button" name="editdelete" id="'.$row->sitename.'" class="editdelete btn btn-warning btn-sm">Edit/Delete</button>';  
						$data[] = $sub_array;
					}
					$output = array(  
						"draw"				=>     intval($_POST["draw"]),  
						"recordsTotal"		=>      $this->Site_database->getAllData($database_table),  
						"recordsFiltered"	=>     $this->Site_database->getFilteredData($database_table),  
						"data"				=>     $data,
						// "mk"				=>     $this->Site_database->makeQuery($database_table)
					); 
					echo json_encode($output);
					break;
				case 'all_data':
					$data = array(); 
					$fetch_data = $this->Site_database->getAll($database_table, "*");
					foreach($fetch_data as $row)  
					{  
						$sub_array = array();
						foreach($table_fields as $value)
						{
							$sub_array[] = $row->$value;
						}
				                // $sub_array[] = '<img src="'.base_url().'upload/'.$row->image.'" class="img-thumbnail" width="50" height="35" />';  
				                // $sub_array[] = '<button type="button" name="editdelete" id="'.$row->sitename.'" class="editdelete btn btn-warning btn-sm">Edit/Delete</button>';  
						$data[] = $sub_array;
					}
					echo json_encode($data);
					break;
				case 'search':
					$selectedtr = $this->input->post('selectedtr');
					$results = array();
					$select = 'PLAID,LATITUDE,LONGITUDE';
					if (is_array($selectedtr) && sizeof($selectedtr) > 0)
					{
						foreach ($selectedtr as $value) {
							$this->db->select($select);
							array_push($results, $this->db->get_where($database_table,  array('id'=>$value))->row());
						}
					}
					
					echo json_encode($results);
					// echo json_encode($id);
					break;
					///////////////////////////
					case 'select_sites':
						$proposed_sites = $this->input->post('proposed_sites');
						// $results = array();
						$select= $this->input->post('select_fields');
						$data=[];
						foreach ($proposed_sites as $value) {
							$this->db->select($select);
							if($geoarea=="nat"){
								$database_table=$displayto.'_'.$value[2];
							}else
							{
								$database_table = $displayto.'_'.$geoarea;	
							}
							$database_table = strtolower($database_table);
							$tempData = $this->db->get_where($database_table, array('PLAID'=>$value[0],'SITENAME'=>$value[1]))->row();
							if($tempData !=null){
								array_push($data, $tempData);
							}
						}
						// $list = array('PLAID' => $updatetype);
						// $data = $this->Site_database->getSome($database_table, $list, $select);
						
						echo json_encode($data);
					break;
				default:
					echo json_encode("error");
					break;
			}

    }  


}

/* End of file Site.php */
/* Location: ./application/controllers/Site.php */
