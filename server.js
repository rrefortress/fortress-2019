let socket = require( 'socket.io' );
let express = require('express');
let app = express();
let server = require('http').createServer(app);
let io = socket.listen(server);
let port = process.env.PORT || 3308;
server.listen(port, function () {
	console.log('Server listening at port %d', port);
});
io.on('connection', function (socket) {
	socket.on( 'logs', function( data ) {
		io.sockets.emit( 'logs', {
			id: data.id,
			action: data.action,
			ago: data.ago,
			department: data.department,
			geoarea: data.geoarea,
			name: data.name,
			profile: data.profile
		});
	});

	socket.on( 'active_users', function( data ) {
		io.sockets.emit( 'active_users', {
			active: data.active
		});
	});

	socket.on( 'activities', function( data ) {
		io.sockets.emit( 'activities', {
			active: data
		});
	});
});
